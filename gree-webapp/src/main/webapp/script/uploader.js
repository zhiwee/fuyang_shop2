(function ($) {
    var defaultOptions = {
        auto: true,
        swf: '/lib/webuploader/0.1.5/Uploader.swf',
        server: '',
        pick: {
            id: '#upload-container',
            innerHTML: '<i class="Hui-iconfont">&#xe642;</i> 浏览文件',
        }
    };
    function Uploader(options){
        var opts = $.extend({}, defaultOptions, options);
        var uploader = WebUploader.Uploader.call(this, opts);
        return uploader;
    }

    $.uploader = Uploader;

}(jQuery));