(function ($) {

    $.fn.extend({
        self: function(){
            return this.prop('outerHTML');
        }
    });

}(jQuery));