(function ($) {

    $.Huitextarealength = function(target){
        var target = target || document;
        var options = {};
        var arg1 = arguments[1], arg2 = arguments[2];
        if(arguments.length == 2){
            if($.isNumeric(arg1)){
                options.maxlength = arg1;
            }else if($.isPlainObject(arg1)){
                $.extend(options, arg1);
            }
        } else {
            if($.isNumeric(arg1) && $.isNumeric(arg2)){
                options.minlength = arg1;
                options.maxlength = arg2;
            }else if($.isNumeric(arg1) && $.isPlainObject(arg2)){
                $.extend(options, {minlength: arg1}, arg2);
            }else if($.isPlainObject(arg1) && $.isPlainObject(arg2)){
                $.extend(options, arg1, arg2);
            }else if($.isPlainObject(arg1) && $.isNumeric(arg2)){
                $.extend(options, arg1, {maxlength: arg2});
            }
        }
        $(target).Huitextarealength(options);
    }

    $.fn.Huifold = function(options){
        var defaults = {
            titCell:'.item .Huifold-header',
            mainCell:'.item .Huifold-body',
            type:1,//1	只打开一个，可以全部关闭;2	必须有一个打开;3	可打开多个
            trigger:'click',
            className:"selected",
            speed:'first',
        }
        var options = $.extend(defaults, options);
        this.each(function(){
            var that = $(this);
            that.on(options.trigger, options.titCell, function(){
                if ($(this).next().is(":visible")) {
                    if (options.type == 2) {
                        return false;
                    } else {
                        $(this).next().slideUp(options.speed).end().removeClass(options.className);
                        if ($(this).find("b")) {
                            $(this).find("b").html("+");
                        }
                    }
                }else {
                    if (options.type == 3) {
                        $(this).next().slideDown(options.speed).end().addClass(options.className);
                        if ($(this).find("b")) {
                            $(this).find("b").html("-");
                        }
                    } else {
                        that.find(options.mainCell).slideUp(options.speed);
                        that.find(options.titCell).removeClass(options.className);
                        if (that.find(options.titCell).find("b")) {
                            that.find(options.titCell).find("b").html("+");
                        }
                        $(this).next().slideDown(options.speed).end().addClass(options.className);
                        if ($(this).find("b")) {
                            $(this).find("b").html("-");
                        }
                    }
                }
            });
        });
    }

}(jQuery));