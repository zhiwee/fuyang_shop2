(function () {

    function safeOverride(container, name, fn){
        if(arguments.length === 2 && typeof name === 'function'){
            fn = name;
            name = fn.name;
        }
        if(!container[name]) {
            container[name] = fn;
        }
    }

    safeOverride(window, function of(key, value) {
        if(arguments.length === 1 && typeof key == 'function'){
            value = key;
            key = key.name;
        }
        window[key] = value;
    });

    var EMPTY = '',
        NOOP = function () {},
        EMPTY_ARRAY = [],
        EMPTY_OBJECT = {};

    var StrProto = String.prototype,
        ArrProto = Array.prototype,
        ObjProto = Object.prototype,
        FunProto = Function.prototype;

    /*
    * Object
    * **************************/
    safeOverride(Object, 'isPlainObject', $.isPlainObject);
    safeOverride(Object, function keys(obj) {
        if(Object.isPlainObject(obj)){
            var keys = [], k, tmp = {};
            for(k in obj){
                if(obj.hasOwnProperty(k) && !tmp[k]){
                    keys.push(k);
                    tmp[k] = 1;
                }
            }
            return keys;
        }
        return [];
    });

    safeOverride(Object, function querystring(obj){
        if(Object.isPlainObject(obj)){
            var qs = [];
            for(k in obj){
                if(obj.hasOwnProperty(k)){
                    qs.push('{0}={1}'.format(k, obj[k]));
                }
            }
            return qs.join('&');
        }
        return '';
    });

    /*
    * Function
    * **************************/
    safeOverride(FunProto, function bind(thisArgs) {
            var fn = this;
            return function () {
                return fn.apply(thisArgs, arguments);
            }
        });

    /*
    * Array
    * *************************/

    safeOverride(Array, 'isArray', $.isArray);

    safeOverride(ArrProto, function min() {
        var min = this[0];

        var len = this.length, i = 1;
        for(; i < len; i++) {
            if(this[i] < min) {
                min = this[i];
            }
        }

        return min;
    });

    safeOverride(ArrProto, function max() {
        var max = this[0];

        var len = this.length, i = 1;
        for(; i < len; i++) {
            if(this[i] > max) {
                max = this[i];
            }
        }

        return max;
    });

    /*
    * String
    * **********************/

    function multiline(fn){
        var tmpl = fn.toString().split('\n').slice(1, -1).join('\n') + '\n';
        var args = [].slice.call(arguments, 1);
        if(args && args.length){
            var model;
            if(args.length == 1){
                model = args[0];
            }else{
                model = args;
            }
            if(Array.isArray(model)){
                tmpl = tmpl.format.apply(tmpl, model);
            }else{
                tmpl = tmpl.format(model);
            }
        }
        return tmpl;
    }

    /**
     * 定义模板，模板中变量可以被替换，参见String.prototype.format
     * @param fn
     * @returns {function(): (string | *)}
     */
    safeOverride(String, function template(fn){
        return function(){
            var args = [fn].concat([].slice.call(arguments, 0));
            return multiline.apply(null, args);
        }
    });

    /**
     * 获取一个GUID
     * xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx
     */
    safeOverride(String, function guid(){
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = Math.random() * 16|0, v = c == 'x' ? r : (r&0x3|0x8);
            return v.toString(16);
        });
    });

    /**
     * 两种调用方式
     * var template1="我是{0}，今年{1}了";
     * var template2="我是{name}，今年{age}了";
     * var result1=template1.format("loogn",22);
     * var result2=template2.format({name:"loogn",age:22});
     * 两个结果都是"我是loogn，今年22了"
     */
    safeOverride(StrProto, function format(args) {
        var result = this;
        if (arguments.length > 0) {
            if (arguments.length == 1 && typeof (args) == "object") {
                for (var key in args) {
                    var reg = new RegExp('({' + key + '})', 'g');
                    result = result.replace(reg, args[key] !== undefined ? args[key] : EMPTY);
                }
            } else {
                for (var i = 0; i < arguments.length; i++) {
                    var reg= new RegExp('({)' + i + '(})', 'g');
                    result = result.replace(reg, arguments[i] !== undefined ? arguments[i] : EMPTY);
                }
            }
        }
        return result;
    });

    /**
     * 首字母大写
     */
    safeOverride(StrProto, function capitalize(){
        if(this.length === 0){
            return this;
        }
        return this.charAt(0).toUpperCase() + this.slice(1);
    });

    /**
     * 首字母小写
     */
    safeOverride(StrProto, function uncapitalize(){
        if(this.length === 0){
            return this;
        }
        return this.charAt(0).toLowerCase() + this.slice(1);
    });

}());