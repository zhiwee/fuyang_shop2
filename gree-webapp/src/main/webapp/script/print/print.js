function PrinterObj(){
// 打印  http://blog.csdn.net/wf1154439/article/details/44562739
    var LODOP, WIDTH = 400, HEIGHT = 300, LEFT = 100, ITEM_TOP = 10, LINE_MARGIN = 15, UNIT_WIDTH = 10;
    function init(){
        if(!LODOP){
            LODOP = getLodop();
            LODOP.SET_LICENSES("","AFAE0B8BC8FC0A439649699786415AE9","C94CEE276DB2187AE6B65D56B3FC2848","");
        }

    }

    function print(payment){
        resolve(payment, 1);
    }

    function preview(payment){
        resolve(payment, 0);
    }

    function resolve(payment, type){
        if(payment){
            if(type == 1){
                printSingle(payment,type);

            }else{
                previewSingle(payment,type);
            }
        }
    }
    function singleOrder(payment,type){

        LODOP.PRINT_INIT("打印控件功能演示_Lodop功能_内容旋转");
      // LODOP.SET_PRINT_PAGESIZE(1,2310,1300,"");
        var xk=1;
        var s=0;
        var totalPrice=0;
        $(payment).each(function (i, v) {
            if (i % 7 == 0) {
                if (i) {
                    xk=xk+1;
                    LODOP.NewPageA();
                }
                //券号、订单号、支付时间
                var ticket,orderNumber,createtime;
                //客户信息、姓名、电话、安装时间、安装地址
                var recognizeUserName,recognizePhone,remarkTime,recognizeAddress;
                //导购员、收银员、订单备注
                var shoppingGuidename,cashierName,remark;

                $(payment).each(function (i, v) {
                    ticket=v.ticket.valueOf();
                    orderNumber=v.orderNumber.valueOf();
                    recognizeUserName=v.recognizeUserName.valueOf();
                    recognizePhone=v.recognizePhone.valueOf();
                    remarkTime=v.remarkTime;
                    recognizeAddress=v.recognizeAddress;
                    shoppingGuidename=v.shoppingGuidename;
                    //时间
                    var today=new Date();
                    var s=today.getFullYear()+"-"+(today.getMonth()+1 < 10 ? '0'+(today.getMonth()+1) : today.getMonth()+1)+ '-'+today.getDate()+" "+today.getHours()+":"+today.getMinutes()+":"+today.getSeconds();
                    createtime=s;
                    //备注
                    if(v.remark != null || v.remark!=""){
                        remark=v.remark;
                    }else{
                        remark="";
                    }
                    cashierName=v.cashierName;
                });
                if(type!=1) {
                    /*  LODOP.ADD_PRINT_LINE(35,270,35,450,0,1);
                      <!--20长度-->
                      LODOP.ADD_PRINT_LINE(70, 20, 70, 750, 0, 1);
                      LODOP.ADD_PRINT_LINE(105, 20, 105, 750, 0, 1);
                      LODOP.ADD_PRINT_LINE(130, 20, 130, 750, 0, 1);
                      LODOP.ADD_PRINT_LINE(165, 20, 165, 750, 0, 1);
                      LODOP.ADD_PRINT_LINE(200, 20, 200, 750, 0, 1);
                      LODOP.ADD_PRINT_LINE(235, 20, 235, 750, 0, 1);
                      LODOP.ADD_PRINT_LINE(270, 20, 270, 750, 0, 1);
                      LODOP.ADD_PRINT_LINE(305, 20, 305, 750, 0, 1);
                      LODOP.ADD_PRINT_LINE(340, 20, 340, 750, 0, 1);
                      LODOP.ADD_PRINT_LINE(375, 20, 375, 750, 0, 1);
                      LODOP.ADD_PRINT_LINE(410, 20, 410, 750, 0, 1);
                      LODOP.ADD_PRINT_LINE(430, 20, 430, 750, 0, 1);

                      LODOP.ADD_PRINT_LINE(70, 20, 430, 20, 0, 1);
                      LODOP.ADD_PRINT_LINE(70, 750, 430, 750, 0, 1);

                      LODOP.ADD_PRINT_LINE(130, 90, 410, 90, 0, 1);

                      LODOP.ADD_PRINT_LINE(130,310, 410, 310, 0, 1);
                      LODOP.ADD_PRINT_LINE(130, 610, 410, 610, 0, 1);
                      LODOP.ADD_PRINT_LINE(130, 650, 410, 650, 0, 1);
                      LODOP.ADD_PRINT_LINE(130, 700, 410, 700, 0, 1);


                      LODOP.ADD_PRINT_LINE(70, 310, 105, 310, 0, 1);
                      LODOP.ADD_PRINT_LINE(70, 610, 105, 610, 0, 1);
  */

                    <!-- 35居上 120居左 320长度 35高度-->
                    // LODOP.ADD_PRINT_TEXT(15,270,320,35,"格力电器 工厂巡展");
                    // LODOP.SET_PRINT_STYLEA(0,"FontSize",15);
                    // LODOP.ADD_PRINT_TEXT(37,330,220,57,"销售单");
                    // LODOP.SET_PRINT_STYLEA(0,"FontSize",13);
                    // LODOP.ADD_PRINT_TEXT(15,520,320,35,"No:");
                    //LODOP.SET_PRINT_STYLEA(0,"FontSize",15);
                    // LODOP.ADD_PRINT_TEXT(55,30,70,25,"认筹券号：");

                    // LODOP.ADD_PRINT_TEXT(55,200,70,25,"支付时间：");
                    // LODOP.ADD_PRINT_TEXT(55,590,110,25,"订单流水号：");
                    //LODOP.ADD_PRINT_TEXT(80,30,75,25,"送装联系人:");
                    // LODOP.ADD_PRINT_TEXT(80,310,75,25,"送装电话:");
                    //LODOP.ADD_PRINT_TEXT(80,610,100,25,"预约安装时间:");
                    //LODOP.ADD_PRINT_TEXT(110,30,75,25,"收货地址：");
                    //LODOP.ADD_PRINT_TEXT(145,30,75,25,"编  码");
                    // LODOP.ADD_PRINT_TEXT(145,185,75,25,"商品名称");
                    /*LODOP.ADD_PRINT_TEXT(180,220,75,25,"商品型号");*/
                    /*LODOP.ADD_PRINT_TEXT(145,420,75,25,"商品型号");
                    LODOP.ADD_PRINT_TEXT(145,613,75,25,"数量");
                    LODOP.ADD_PRINT_TEXT(145,650,75,25,"单价");
                    LODOP.ADD_PRINT_TEXT(145,700,75,25,"金额小计");
                    LODOP.ADD_PRINT_TEXT(415,30,75,25,"合计金额：");
                    LODOP.ADD_PRINT_TEXT(430,30,170,25,"备注："+remark);
                    LODOP.ADD_PRINT_TEXT(430,480,650,25,"格力售后派工电话： 400-365-315");
                    LODOP.ADD_PRINT_TEXT(450,30,79,25,"导购员：");
                    LODOP.ADD_PRINT_TEXT(450,200,79,25,"收款员：");

                    LODOP.ADD_PRINT_TEXT(450,480,94,25,"顾客确认：");
*/
                }
                LODOP.SET_PRINT_STYLE("FontName","黑体");
                //LODOP.SET_PRINT_STYLE("FontSize", 13);

                LODOP.ADD_PRINT_TEXT(70,105,75,25,ticket);

                LODOP.ADD_PRINT_TEXT(70,295,150,25,createtime);

                LODOP.ADD_PRINT_TEXT(70,675,110,25,orderNumber+"-"+xk);


                LODOP.ADD_PRINT_TEXT(100,130,75,25,recognizeUserName);

                LODOP.ADD_PRINT_TEXT(100,375,75,25,recognizePhone);

                LODOP.ADD_PRINT_TEXT(100,695,100,25,remarkTime.substring(0,10));


                LODOP.ADD_PRINT_TEXT(135,115,450,25,recognizeAddress);


                LODOP.ADD_PRINT_TEXT(470,95,79,25,shoppingGuidename);
				
				LODOP.ADD_PRINT_TEXT(450,100,600,25,remark);

                LODOP.ADD_PRINT_TEXT(470,310,79,25,cashierName);

                s=0;
            }
            totalPrice=totalPrice+parseInt(v.totalPrice);
            if(s==6){
                LODOP.ADD_PRINT_TEXT(415,120,200,25,smalltoBIG(totalPrice));

                LODOP.ADD_PRINT_TEXT(415,675,76,25,totalPrice);
                totalPrice=0;
            }
            else if(i==(payment.length-1)){
                LODOP.ADD_PRINT_TEXT(415,120,200,25,smalltoBIG(totalPrice));

                LODOP.ADD_PRINT_TEXT(415,675,76,25,totalPrice);
                totalPrice=0;
            }else{

            }
            LODOP.ADD_PRINT_TEXT(195+ s * 30, 45, 75, 25, v.goodCode);
            LODOP.ADD_PRINT_TEXT(195+ s * 30, 133, 220, 25, v.goodName);
            LODOP.ADD_PRINT_TEXT(195+ s * 30, 305, 350, 25, v.goodModel);
            LODOP.ADD_PRINT_TEXT(195+ s * 30, 610, 60, 25, v.goodNum);
            LODOP.ADD_PRINT_TEXT(195+ s * 30, 650, 60, 25, v.goodPrice);
            LODOP.ADD_PRINT_TEXT(195+ s * 30, 723, 60, 25, v.totalPrice);
            s=s+1;
        });
    }
    function printSingle(payment,type){
        init();
        singleOrder(payment,type);
        LODOP.PRINT();
    }
    function previewSingle(payment,type){
        init();
        singleOrder(payment,type);
        LODOP.PREVIEW();
    }

    function formatTicketSeqno(val){
        return "G" + "000000".substring(("" + val).length) + val;
    }

    function smalltoBIG(n) {
        var fraction = ['角', '分'];
        var digit = ['零', '壹', '贰', '叁', '肆', '伍', '陆', '柒', '捌', '玖'];
        var unit = [ ['元', '万', '亿'], ['', '拾', '佰', '仟']  ];
        var head = n < 0? '欠': '';
        n = Math.abs(n);

        var s = '';

        for (var i = 0; i < fraction.length; i++)
        {
            s += (digit[Math.floor(n * 10 * Math.pow(10, i)) % 10] + fraction[i]).replace(/零./, '');
        }
        s = s || '整';
        n = Math.floor(n);

        for (var i = 0; i < unit[0].length && n > 0; i++)
        {
            var p = '';
            for (var j = 0; j < unit[1].length && n > 0; j++)
            {
                p = digit[n % 10] + unit[1][j] + p;
                n = Math.floor(n / 10);
            }
            s = p.replace(/(零.)*零$/, '').replace(/^$/, '零')  + unit[0][i] + s;
        }
        return head + s.replace(/(零.)*零元/, '元').replace(/(零.)+/g, '零').replace(/^整$/, '零元整');
    }

    return {
        print: print,
        preview: preview,
        resolve: resolve
    }
}

window.printerObj = new PrinterObj();
