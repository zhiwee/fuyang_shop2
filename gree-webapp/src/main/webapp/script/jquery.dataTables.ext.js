(function($){

    var defaultDataTableOptions = {
        lengthChange: false,
        ordering: false,
        paging: true,
        processing: true,
        select: 'single',
        searching: false,
        serverSide: true,
        language: {
            sProcessing: "处理中...",
            sLengthMenu: "显示 _MENU_ 项结果",
            sZeroRecords: "没有匹配结果",
            sInfo: "第 _PAGE_ 页，共 _PAGES_ 项",
            sInfoEmpty: "第 0 页，共 0 项",
            sInfoFiltered: "",
            sInfoPostFix: "",
            sSearch: "搜索:",
            sUrl: "",
            sEmptyTable: "表中数据为空",
            sLoadingRecords: "载入中...",
            sInfoThousands: ",",
            oPaginate: {
                sFirst: "首页",
                sPrevious: "上页",
                sNext: "下页",
                sLast: "末页"
            },
            oAria: {
                sSortAscending: ": 以升序排列此列",
                sSortDescending: ": 以降序排列此列"
            },
            select: {
                rows: {
                    _: "",
                    1: ""
                },
                columns: {
                    _: "",
                    0: "",
                    1: ""
                },
                cells: {
                    _: "",
                    0: "",
                    1: ""
                }
            }
        }
    };

    var defaultColumnOptions = {
        searchable: false,
        orderable: false,
    };

    var $dataTable = $.fn.dataTable;
    var $DataTable = $.fn.DataTable;

    function DataTable(options){
        var self = this;
        var tableOpts = collectTableOpts(self);
        var columnsOpts = collectColumnOpts(self);
        var opts = $.extend({}, options, columnsOpts, tableOpts, defaultDataTableOptions);
        self.on('preXhr.dt', function(e, settings, data){
            if(data){
                delete data.columns;
                delete data.order;
                delete data.search;
                data.pageSize = data.length;
                data.offset = data.start || 0;
                data.pageNum = ~~(data.offset / data.pageSize);
                settings._draw = data.draw;
            }
        });
        self.on('xhr.dt', function (e, settings, res, xhr) {
            res.draw = settings._draw;
            if(res.code == 200){
                //成功
                res._data = res.data;
                if(res._data){
                    res.recordsTotal = res._data.totalCount || 0;
                    res.recordsFiltered = res.recordsTotal;
                    res.data = res._data.data;
                }
            }else{
                //失败
                res.error = res.message || '获取数据失败';
                res.data = [];
                res.recordsTotal = 0;
                res.recordsFiltered = 0;

                layer.msg(res.error);
            }
        });

        if(!opts.ajax){
            opts.serverSide = false;
        }

        return $dataTable.call(self, opts);
    }

    $.extend(DataTable, $dataTable);
    DataTable.prototype = $dataTable;

    function collectColumnOpts($table){
        var $columns = $table.find('thead th');
        var columns = [];
        $columns.each(function (i, item) {
            var $item = $(item), datas = $item.data();
            var columnOpt = $.extend(datas, defaultColumnOptions);
            if(columnOpt.render){
                columnOpt.render = window[columnOpt.render].bind(columnOpt);
            }else if(columnOpt.consts && !columnOpt.render){
                columnOpt.render = window.formatConsts.bind(columnOpt);
            }
            if(columnOpt.data === undefined && columnOpt.render === undefined){
                columnOpt.render = window.noop.bind(columnOpt);
            }
            columns.push(columnOpt);
        });
        return {columns: columns};
    }

    function collectTableOpts($table){
        var opts  = {};
        return opts;
    }

    $.fn.extend({
        DataTable: $DataTable,
        dataTable: DataTable
    });

}(jQuery));