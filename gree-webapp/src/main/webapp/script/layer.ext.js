(function ($) {

    var defaultLayerParam = {
        type: 2,
        title: '',
        shadeClose: true,
        shade: 0.2,
        area: ['90%', '90%'],
        maxmin: true,
    };

    $.layer = {
        open: function (title, url, width, height, param) {
            var params = $.extend({}, defaultLayerParam, {
                content: url,
                title: title || '',
                area: [width || '90%', height || '90%'],
            }, param);
            return layer.open(params);
        },
    };

}(jQuery));

// jQuery(function($){
// //你的js代码放在这里（例如第二个方案提到的ready函数和子函数）
// //如果是js文件，其实就是在文件头部和尾部各加一行代码
// }
