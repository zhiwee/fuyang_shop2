(function ($) {

    /**
     *  通用的formatter
     *  formatter参数如下：
     *
     *  value: 该单元格的具体值
     *  rowData: 该行的值
     *  rowIndex: 该行的索引
     *
     * */

    var EMPTY = '',
        NOOP = function () {};

    of(function formatEmpty() {
        return EMPTY;
    });

    of(function formatConsts(value, row, rowIndex) {
        var key = this, formatter = 'format' + key.capitalize();
        return top[formatter] && top[formatter](value) || EMPTY;
    });


}(jQuery));