/**
 * 系统默认处理
 */
(function ($) {

    function noop(){}
    var _ajax = $.ajax;
    $.extend({
        ajax: function(options){
            var headers = {
                // Nonce: String.guid(),
            };
            options.headers = $.extend(headers, options.headers);

            //兼容h-ui.js的ajaxSubmit
            if(options.data && options.iframeSrc){
                options.data = options.formData;
            }

            if(options.data && ($.isPlainObject(options.data) || $.isArray(options.data))){
                options.data = JSON.stringify(options.data);
            }
            return _ajax(options);
        }
    });

    /**
     * ajax默认设置
     * contentType默认为application/json
     * dataType默认为json
     * type默认为post
     */
    $.ajaxSetup({
        type: 'POST',
        contentType: 'application/json;charset=utf-8',
        dataType: 'json',
        error: function (res) {
            layer.msg(res.message);
        }
    });


    /*
    *
    * 默认去加载系统配置
    *
    * */
    if(top.config) {
        window.config = top.config;
    } else {
        $.ajax({
            url: '/runtime/config',
            async: false
        }).then(function (res) {
            if(res && res.data){
                window.config = top.config = res.data;
                return;
            }
            throw new Error('获取系统运行时配置失败');
        });
    }


    function createFromatter(consts) {
        if(consts) {
            var key;
            for(key in consts){
                if(consts.hasOwnProperty(key)){
                    of('format' + key.capitalize(), (function(key){
                        return function (value) {
                            return top.consts[key][value] || ''
                        };
                    }(key)));
                }
            }
        }
    }

    /*
     * 加载常量，并生成对应的formatter
     * */
    if(top.consts) {
        window.consts = top.consts;
        createFromatter(top.consts);
    }else {
        $.ajax({
            url: '/runtime/consts',
            async: false
        }).then(function(res){
            if(res && res.data){
                window.consts = top.consts = res.data;
                createFromatter(top.consts);
                return;
            }
            throw new Error('获取系统运行时常量失败');
        });
    }

    /*
    * 默认formatter
    * */
    of(function formatDateTime(value) {
        if(value) {
            return moment(value).format('YYYY-MM-DD HH:mm:ss')
        }
        return '';
    });
    of(function formatTime(value) {
        if(value) {
            return moment(value).format('HH:mm:ss')
        }
        return '';
    });
    of(function formatDate(value) {
        if(value) {
            return moment(value).format('YYYY-MM-DD')
        }
        return '';
    });
    of(function textarealength(obj,length) {
        var num = 0;
        if ($(obj).val()) {
            num = $(obj).val().length;
            if (num > length) {
                num = length;
                $(obj).val($(obj).val().substr(0, length));
            }
            $(".textarea-length").text(num);
        }
    });

    /*
    * 刷新datagrid时，加载附近参数
    * */
    var _datagridRefresh = $.datagrid.refresh;
    $.datagrid.refresh = function(params, init){
        var domParmas = {};
        $('.datagrid-param').each(function(i, item){
            var $item = $(item), name = $item.attr('name'), val = $item.val();
            if(name){
                domParmas[name] = val;
            }
        });
        params = $.extend({}, domParmas, params);
        _datagridRefresh(params, init);
    };

    $(function () {
        /*
        * 默认去加载datagrid
        * */
        $.datagrid.refresh({}, true);

        /*
        * 对datagrid的条件过滤监听
        * */
        $('.datagrid-refresh').click(function () {
            $.datagrid.refresh();
        });

        /*
        * 表单中checkbox radio美化
        * */
        $('.skin-minimal input').iCheck({
            checkboxClass: 'icheckbox-blue',
            radioClass: 'iradio-blue',
            increaseArea: '20%'
        });

        /*
        * 对表单上面的按钮增加自定义点击事件：
        * .btn-open   直接打开一个页面
        * .btn-open-selected 获取选中行 把id作为参数传入到打开的个页面
        * .btn-confirm 弹出confirm框，进行操作
        * */
        $('.btn-open').click(function(){
            var $this = $(this), params = $.extend({}, $this.data());

            var url = params.content;
            var additionParam = {};
            if(params.yes){
                additionParam.yes = params.yes;
                delete params.yes;
            }
            if(params.cancel){
                additionParam.cancel = params.cancel;
                delete params.cancel;
            }
            if(params.success){
                params.success = window[params.success];
            }
            if(params.end){
                params.end = window[params.end];
            }
            var qs = Object.querystring(additionParam);
            if(qs){
                if(url.indexOf('?') !== -1){
                    url += ('&' + qs);
                }else{
                    url += ('?' + qs);
                }
            }
            params.content = url;
            $.layer.open(params.title, url, params.width, params.height, params);
        });
        $('.btn-open-selected').click(function(){
            var $this = $(this), params = $.extend({}, $this.data());
            var row = $.datagrid.getSelected();
            if(!row){
                layer.msg('请选中要操作的数据');
            }else{
                var url = params.content;
                var additionParam = {
                    id: row.id
                };
                if(params.yes){
                    additionParam.yes = params.yes;
                    delete params.yes;
                }
                if(params.cancel){
                    additionParam.cancel = params.cancel;
                    delete params.cancel;
                }
                if(params.success){
                    params.success = window[params.success];
                }
                if(params.end){
                    params.end = window[params.end];
                }
                var qs = Object.querystring(additionParam);
                if(qs){
                    if(url.indexOf('?') !== -1){
                        url += ('&' + qs);
                    }else{
                        url += ('?' + qs);
                    }
                }
                params.content = url;
                $.layer.open(params.title, url, params.width, params.height, params);
            }
        });
        $('.btn-confirm').click(function(){
            var $this = $(this), params = $.extend({}, $this.data()), url = params.content, msg = params.message;
            if(!url){
                return;
            }
            var rows = $.datagrid.getChecked();
            if(rows && rows.length){
                var ids = [], i, len;
                for(i = 0, len = rows.length; i < len; i++){
                    ids.push(rows[i].id);
                }
                layer.confirm(msg || '确认要进行该操作吗？', function () {
                    $.ajax({
                        url: url,
                        data: {
                            ids: ids
                        },
                        success: function(res){
                            if(res.ok){
                                layer.msg('操作成功');
                                $.datagrid.reload();
                            }else{
                                layer.msg(res.message || '操作失败');
                            }
                        }
                    });
                });
            }else{
                layer.msg('请选中待操作的数据');
            }
        });
        $('.btn-confirm-selected').click(function(){
            var $this = $(this), params = $.extend({}, $this.data()), url = params.content, msg = params.message;
            if(!url){
                return;
            }
            var row = $.datagrid.getSelected();
            if(row){
                layer.confirm(msg || '确认要进行该操作吗？', function () {
                    $.ajax({
                        url: url,
                        data: {
                            id: row.id
                        },
                        success: function(res){
                            if(res.ok){
                                layer.msg('操作成功');
                                $.datagrid.reload();
                            }else{
                                layer.msg(res.message || '操作失败');
                            }
                        }
                    });
                });
            }else{
                layer.msg('请选中一行待操作的数据');
            }
        });

        /*
        * 默认给form表单加提交事件
        * */
        var $formAjax = $("form.form-ajax");
        if($formAjax.length){
            $formAjax.formSubmit();
        }

        /*
        * 对select进行默认填充
        * .select-consts
        * */
        var $select = $('select.select-consts');
        if($select.length) {
            $select.each(function(i, item){
                var $this = $(item),
                group = $this.data('constsName'),
                nullField = $this.data('constsNull'),
                value = $this.data('constsValue'),
                data = consts[group],
                k;
                if(data) {
                    if(nullField) {
                        $this.append('<option value>{0}</option>'.format(nullField));
                    }
                    for (k in data) {
                        if(data.hasOwnProperty(k)) {
                            $this.append('<option value="{0}">{1}</option>'.format(k, data[k]));
                        }
                    }
                    if(value) {
                        $this.val(value);
                    }
                }
            })
        }

        /*
        * 表单默认填充
        * */
        var $form = $('form[data-fill-url]');
        if($form.length){
            var url = $form.data('fillUrl');
            if(url){
                var $params = $form.find('.fill-param');
                var param = {};
                $params.each(function (i, item) {
                    var $item = $(item), name = $item.attr('name'), value = $item.val();
                    param[name] = value;
                });
                $.ajax({
                    url: url,
                    data: param,
                    success: function (res) {
                        if(res && res.data){
                            $form.fillData(res.data);
                        }else{
                            layer.msg('获取信息失败');
                        }
                    }
                });
            }
        }
    });

}(jQuery));