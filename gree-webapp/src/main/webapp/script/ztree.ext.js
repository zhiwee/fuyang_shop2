(function ($) {



    var defaultSettings = {

        // selectedMulti: true, //设置是否能够同时选中多个节点
        //     showIcon: true,  //设置是否显示节点图标
        //     showLine: true,  //设置是否显示节点与节点之间的连线
        //     showTitle: true,  //设置是否显示节点的title提示信息

        view: {
            dblClickExpand: true,
            showLine: false,
            selectedMulti: false
        },
        // enable: false, //设置是否启用简单数据格式（zTree支持标准数据格式跟简单数据格式，上面例子中是标准数据格式）
        // idKey: "id",  //设置启用简单数据格式时id对应的属性名称
        // pidKey: "pId" //设置启用简单数据格式时parentId对应的属性名称,ztree根据id及pid层级关系构建树结构

        data: {
            simpleData: {
                enable:true,
                idKey: 'id',
                pIdKey: 'pId',
                rootPId: 'ROOT'
            }
        },
        // check:{
        //     enable: true   //设置是否显示checkbox复选框
        // },
        // callback: {
        //     onClick: onClick,    //定义节点单击事件回调函数
        //     onRightClick: OnRightClick, //定义节点右键单击事件回调函数
        //     beforeRename: beforeRename, //定义节点重新编辑成功前回调函数，一般用于节点编辑时判断输入的节点名称是否合法
        //     onDblClick: onDblClick,  //定义节点双击事件回调函数
        //     onCheck: onCheck    //定义节点复选框选中或取消选中事件的回调函数
        // },
        // async: {
        //     enable: true,      //设置启用异步加载
        //     type: "get",      //异步加载类型:post和get
        //     contentType: "application/json", //定义ajax提交参数的参数类型，一般为json格式
        //     url: "/Design/Get",    //定义数据请求路径
        //     autoParam: ["id=id", "name=name"] //定义提交时参数的名称，=号前面标识节点属性，后面标识提交时json数据中参数的名称
        // }

    };

    /*
    * 参数分三种：
    * 1. 没有参数，则获取已经生成的数对象
    * 2. 多个参数，第一个为字符串（url）,第二个可选，对象，ajax的参数
    * 3. 多个参数，第一个为对象，参数对照zTree的初始化参数
    *
    * */
    $.fn.extend({
        ztree: function () {
            return factory(this, arguments);
        },
    });


    function factory($obj, args){

        $._ztree_ = $._ztree_ || {};

        if(args.length === 0){
            return getTree($obj);
        }
        var ztree;
        if(typeof args[0] === 'string'){

            ztree = ajaxTree($obj, args);
        }else{

            ztree = initTree($obj, mergeSettings(args[0]), args[1]);
        }



        $._ztree_[getTreeId($obj)] = ztree;

        return ztree;
    }

    function ajaxTree($obj, args){

        var url = args[0],
            ajaxData = args[1],
            treeSettings = args[2];

        var ztree;
        //同步
        doAjax({
            url: url,
            data: ajaxData
        }, function (nodes) {
            if(nodes && nodes.length){
                nodes[0].open = true;
            }
            ztree = initTree($obj, mergeSettings(treeSettings), nodes);
            ztree.__ajaxArgs = {
                url: url,
                data: ajaxData
            };
            if(treeSettings.onInited) {
                treeSettings.onInited.call(ztree);
            }
        });
        return ztree;
    }

    //同步
    function doAjax(ajaxArgs, fn){
        $.ajax({
            url: ajaxArgs.url,
            data: ajaxArgs.data,
            async: true,
            success: function (res) {
                var nodes = res.data || [];
                fn && fn(nodes);
            }
        });
    }

    function initTree($obj, settings, nodes){

        var ztree = $.fn.zTree.init($obj, settings, nodes);

        ztree.__refresh = ztree.refresh;
        ztree.__hideNode = ztree.hideNode;
        ztree.__hideNodes = ztree.hideNodes;
        ztree.__showNode = ztree.showNode;
        ztree.__showNodes = ztree.showNodes;

        ztree.hideNode = function(node, cascade){
            node && this.hideNodes([node], cascade);
        };

        ztree.hideNodes = function(nodes, cascade){
            var self = this;
            if(cascade === false){
                self.__hideNodes(nodes);
            }else{
                _hideNodes(nodes);
            }
            self.__refresh();

            function _hideNodes(nodes){
                if(nodes && nodes.length){
                    self.__hideNodes(nodes);
                    var i, len;
                    for(i = 0, len = nodes.length; i < len; i++){
                        _hideNodes(nodes[i].children || []);
                    }
                }
            }
        };

        ztree.showNode = function(node, cascade){
            node && (this.showNodes([node], cascade));
        };

        ztree.showNodes = function(nodes, cascade){
            var self = this;

            if(cascade === false){
                self.__showNodes(nodes);
            }else{
                _showNodes(nodes);
            }
            self.__refresh();

            function _showNodes(nodes){
                if(nodes && nodes.length){
                    self.__showNodes(nodes);
                    var i, len;
                    for(i = 0, len = nodes.length; i < len; i++){
                        var node = nodes[i].getParentNode();
                        node && _showNodes([node]);
                    }
                }
            }
        };


        ztree.search = function () {
            var self = this;
            var args = arguments, nodes;
            if(args.length === 0){
                return;
            }
            if(args.length == 2){
                nodes = self.getNodesByParamFuzzy(args[0], args[1]);
            }else if(args.length == 1 && $.isFunction(args[0])){
                //filter
                nodes = self.getNodesByFilter(args[0]);
            }else if(args.length == 1 && typeof args[0] === 'string'){
                nodes = self.getNodesByParamFuzzy('name', args[0]);
            }else{
                nodes = self.getNodesByParamFuzzy('name', '');
            }
            self.hideNodes(self.getNodes(), true);
            self.showNodes(nodes, true);
        };

        ztree.refresh = function(){
            var self = this,
                ajaxArgs = self.__ajaxArgs;

            if(ajaxArgs && ajaxArgs.url){
                self.destroy();
                factory($obj, [ajaxArgs.url, ajaxArgs.data, self.setting])
            }else{
                self.__refresh();
            }

        };
        ztree.ztreeCheck = function (ids) {
            var self = this;
            ids = ';' + ids.join(';') + ';';
            var zTree = self.getCheckedNodes(false);
            for (var i = 0; i < zTree.length; i++) {
                if (ids.indexOf(';' + zTree[i].id + ';') > -1) {
                    self.expandNode(zTree[i], true); // 展开选中的
                    self.checkNode(zTree[i], true);
                }
            }
        }

        return ztree;
    }

    function getTree($obj){
        return $.fn.zTree.getZTreeObj(getTreeId($obj));
    }

    function getTreeId($obj){
        return $obj.attr('id') + '';
    }

    function mergeSettings(treeSetting){
        return $.extend({}, defaultSettings, treeSetting);
    }

}(jQuery));