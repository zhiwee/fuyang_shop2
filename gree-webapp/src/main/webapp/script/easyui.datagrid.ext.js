(function ($) {
    var _defaults = $.fn.datagrid.defaults;
    var _methods = $.fn.datagrid.methods;

    //禁用自动解析
    $.parser.auto = false;

    $.fn.datagrid.defaults = $.extend({}, _defaults, {
        loader: function (param, rsv, rjt) {
            var opts = $(this).datagrid('options');
            if(!opts.url){
                return false;
            };
            var page = param.page - 1;
            var rows = param.rows;
            delete param.page;
            delete param.rows;
            var url = '{0}?page={1}&size={2}'.format(opts.url, page, rows)
            $.ajax({
                url: url,
                data: param,
                success: function(res) {
                    if(res && res.data){
                        var pager = res.data;
                        rsv({
                            total: pager.totalCount || 0,
                            rows: pager.data || []
                        });
                    }else{
                        rsv({
                            total: 0,
                            rows: []
                        });
                    }
                },
                error: function() {
                    rjt.apply(this, arguments);
                }
            });
        },
    });

    $.datagrid = {};

    _methods.refresh = function(jq, params, init){
        if(jq.length === 0){
            return;
        }
        if(init === true){
            var url = jq.attr('url');
            jq.removeAttr('url');
            jq.datagrid();
            if(url !== undefined){
                jq.datagrid("options").url = url;
                jq.attr('url', url);
            }

        }
        jq.datagrid("options").pageNumber = 1;
        jq.datagrid({
            queryParams: params || {}
        });
    };

    var name;
    for(name in _methods){
        if(_methods.hasOwnProperty(name)){
            $.datagrid[name] = (function (name) {
                return function () {
                    var args = [$('.easyui-datagrid')].concat([].slice.call(arguments, 0));
                    return _methods[name].apply(null, args);
                }
            }(name));
        }
    }

}(jQuery));