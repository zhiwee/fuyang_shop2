(function(w) {

    /**
     * 参数：
     * data 当前cell的值
     * type 数据类型-有这些值： filter display type sort (啥意思，不懂)
     * row 整个row的数据
     * meta 对象 包含row(行索引), col（列索引），settings（配置对象）
     */

    var DEFAULT_STR = '';

    of(function noop() {
        return DEFAULT_STR;
    });

    of(function formatConsts(data, type, row, meta) {
        var self = this;
        if(self.consts && top.consts[self.consts]){
            return top.consts[self.consts][data];
        }
        return DEFAULT_STR;
    });

}(window));