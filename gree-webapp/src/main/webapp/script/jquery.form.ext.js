(function ($) {

    function DEFAULT_FORMAT(value) {
        return value;
    }

    function FormData(data){
        var self = this;
        self.data = data || {};
        self.stringify = function(){
            return JSON.stringify(self.data);
        };
        self.valueOf = function() {
            return self.data;
        }
    }

    function formData() {
        var $self = this;
        var root = {};
        collectFormData($self, root);
        return new FormData(root);
    }

    function collectFormData($group, objContainer){
        var name = $group.data('formGroup'), $items = $group.find('input:not([type="button"],[type="submit"],[type="reset"]), select, textarea');
        var data;
        if(!name){
            data = objContainer;
        }else{
            data = objContainer[name];
            if(data){
                var tmp = data;
                data = {};
                if($.isArray(tmp)){
                    tmp.push(data);
                }else{
                    objContainer[name] = [];
                    objContainer[name].push(tmp);
                    objContainer[name].push(data);
                }
            }else{
                data = objContainer[name] = {};
            }
        }
        $items.filter(function (i, item) {
            return !!$(item).attr('name');
        }).filter(function (i, item) {
            return isDirectChild($(item), $group);
        }).each(function (i, item) {
            var $item = $(item);
            var fieldName = $item.data('formField'), itemName = $item.attr('name');
            if($item.is('[type="radio"]') || $item.is('[type="checkbox"]')){
                var itemValue;
                if(fieldName){
                    itemValue = $group.find('input[data-form-field="' + fieldName + '"]:checked').val();
                }else{
                    itemValue = $group.find('input[name="' + itemName + '"]:checked').val()
                }
                data[fieldName || itemName] = itemValue;
            }else{
                data[itemName] = $item.val();
            }
        });
        $group.find('[data-form-group]').filter(function (i, item) {
            return isDirectChild($(item), $group);
        }).each(function (i, item) {
            collectFormData($(item), data);
        });
    }

    function isDirectChild($item, $group){
        var isGroup = $group.is('[data-form-group]');
        var $parentGroup = $item.parents('[data-form-group]');
        return (!isGroup && !$parentGroup.length) || (isGroup && $group.is($parentGroup));
    }

    function fillData(data){
        if(!data) {
            return;
        }
        var $self = this;
        $self.find('.form-param,input:not([type="button"],[type="submit"],[type="reset"]),textarea,select')
            .each(function(i, item){
                var $item = $(item), name = $item.attr('name'), formatterName = $item.data('fillFormat'), formatter = formatterName && window[formatterName] || DEFAULT_FORMAT;

                var formattedValue = formatter(name && data[name] || '', data);

                if(!name && formattedValue === null || formattedValue === undefined){
                    return;
                }
                if($item.attr('type') == 'checkbox'){
                    if(typeof formattedValue == 'string'){
                        formattedValue = formattedValue.split(',');
                    }
                    formattedValue = formattedValue || [];

                    var checked = false;
                    for(var i = 0; i < formattedValue.length; i ++){
                        var formattedValue = formatter(formattedValue[i], data);
                        if(formattedValue == $item.val()){
                            checked = true;
                            break;
                        }
                    }
                    checked && $item.iCheck('check');
                }else if($item.attr('type') == 'number'){
                    $item.val(formattedValue);
                } else if($item.attr('type') == 'radio'){
                    if(formattedValue == $item.val()){
                        $item.iCheck('check');
                    }
                } else if($item.is('select')){
                    var $opts = $item.children('option'), i, len = $opts.length;
                    for(i = 0; i < len; i++){
                        if($opts.eq(i).val() == formattedValue){
                            // $item.find("option[value="+formattedValue+"]").attr("selected",true);
                            $opts.eq(i).attr("selected",true);
                            // $($opts.eq(i)).attr('selected',true);
                            // alert(JSON.stringify($opts.eq(i))+"aaaaaaa"+ JSON.stringify($opts.eq(i).val()))
                        }
                    }
                }else if($item.is('textarea')){
                    $item.html(formattedValue);
                }else if($item.is('input')){
                    if(formattedValue && !isNaN(formattedValue)){
                        if($item.hasClass("formatDateTime")){
                            $item.val(moment(formattedValue).format('YYYY-MM-DD HH:mm:ss'));
                        }else if($item.hasClass("formatDate")){
                            $item.val(moment(formattedValue).format('YYYY-MM-DD'));
                        }else if($item.hasClass("formatTime")){
                            $item.val(moment(formattedValue).format('HH:mm:ss'));
                        }else{
                            $item.val(formattedValue);
                        }
                    }else{
                        $item.val(formattedValue);
                    }
                }else{
                    $item.html(formattedValue);
                }
            });
        $self.find(".textarea-length").each(function(i,item){
            var id= $(item).attr("for-id");
            if(id){
                $(".textarea-length").text($("#"+id).val().length)
            }
        });
    }

    /**
     * @param options
     * - validate: true
     * - submitHandler: function(){}
     * - refresh: false
     * - refreshDatagrid: true
     * - refreshTree；false
     * - success: function() {},
     * - error: function() {}
     *
     */
    var defaultSubmitOptions = {
        validate: true,
        refresh: false,
        refreshDatagrid: true,
        refreshTree: false,
        success: function (res) {
            if(res.ok){
                parent && parent.layer && parent.layer.msg('操作成功');
            }else{
                parent && parent.layer && parent.layer.msg(res.message || '系统请求异常');
            }
        },
        error: function (e) {
            parent && parent.layer && parent.layer.msg('系统请求异常');
        }
    };

    function submit(options){
        var $form = this;
        options = $.extend({}, defaultSubmitOptions, options);
        if(options.validate){
            $form.validate({
                onkeyup:false,
                focusCleanup:true,
                success:"valid",
                submitHandler: options.submitHandler || function (form) {
                    options.formData = $.extend({}, $form.formData().data, options.data);
                    doSumbit($form, options);
                }
            });
        }
    }

    function doRefreshDatagrid(){
        if(parent && parent.$ && parent.$.datagrid){
            parent.$.datagrid.reload();
        }
    }

    function doRefreshTree(treeId){
        if(parent && parent.$ && parent.$._ztree_){
            var ztrees = parent.$._ztree_;
            if(treeId === true){
                var treeId;
                for(treeId in ztrees){
                    ztrees[treeId] && ztrees[treeId].refresh();
                }
            }else{
                ztrees[treeId] && ztrees[treeId].refresh();
            }
        }
    }

    function doSumbit($form, options){
        options = options || {};
        var _success = options.success;
        var _error = options.error;
        options.success = function(res){
            _success && _success.call($form, res);
            if(res.ok){
                if(options.refresh === true){
                    doRefreshDatagrid();
                    doRefreshTree(true);
                }else {
                    if(options.refreshTree){
                        doRefreshTree(options.refreshTree);
                    }
                    if(options.refreshDatagrid){
                        doRefreshDatagrid();
                    }
                }
                if(parent && parent.layer){
                    var index = parent.layer.getFrameIndex(window.name);
                    parent.layer.close(index);
                }
            }
        };
        options.error = function(){
            _error && _error.call($form);
        };
        $form.ajaxSubmit(options);
    }

    $.fn.extend({
        formData: formData,
        fillData: fillData,
        formSubmit: submit
    });

}(jQuery));