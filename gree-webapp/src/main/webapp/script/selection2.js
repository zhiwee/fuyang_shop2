//todo 仅供新增优惠券中，选择分类使用，即判断不能选择上下级的分类


(function ($) {

        var selectedDOMItem = String.template(function () {/*
        <div data-id="{__id__}" class="panel-body selected-container" style="border: none; overflow: hidden;">
            <input class="btn btn-default radius" type="button" value="{__name__}">
            <i class="Hui-iconfont Hui-iconfont-del" style="font-size: 22px; cursor: pointer;"></i>
        </div>
    */});

        var ID_FIELD = 'selectionIdField';
        var NAME_FIELD = 'selectionNameField';

        var $selectedContainer = $('.selected-container');

        function addDOMItem(data) {
            $selectedContainer.append(selectedDOMItem(data));
        }

        function removeDOMItem(data) {
            $selectedContainer.find('[data-id="{0}"]'.format(data.__id__)).remove();
        }

        function removeAllDOMItem() {
            $selectedContainer.find('[data-id]').remove();
        }

        /*
        * 多选
        *
        * **************************************************/

        function MultiSelection($datagrid, idField, nameField) {
            var _idField = idField;
            var _nameField = nameField;
            var selectedData = {};

            function add(index,data) {
                if (!$.isArray(data)) {
                    data = [data];
                }
                $(data).each(function (i, item) {
                        item.__id__ = item[_idField];
                        item.__name__ = item[_nameField];

                        for (var key in selectedData) {
                            if (item.__id__ == key.substring(0, key.length - 2)) {
                                $datagrid.datagrid('uncheckRow', index);
                                layer.msg("已经选择过了该分类下的子类！");
                                return;
                            }
                            for (var k = 0; k < item.__id__.length - 1; k++) {
                                if (key == item.__id__.substring(0, k + 1) ) {
                                    $datagrid.datagrid('uncheckRow', index);
                                    layer.msg("已经选择过了该分类的父类！");
                                    return;
                                }
                            }
                        }

                        if (!selectedData[item.__id__]) {
                            selectedData[item.__id__] = item;
                            addDOMItem(item);
                        }
                    }
                );
            }

            function remove(data) {
                if (data) {
                    if (!$.isArray(data)) {
                        data = [data];
                    }
                    $(data).each(function (i, item) {
                        item.__id__ = item[_idField];
                        item.__name__ = item[_nameField];
                        delete selectedData[item.__id__];
                        removeDOMItem(item);
                    });
                }
            }

            function get() {
                var datas = [], k;
                for (k in selectedData) {
                    if (selectedData.hasOwnProperty(k)) {
                        datas.push(selectedData[k]);
                    }
                }
                return datas;
            }

            function getSelectedObj() {
                return $.extend({}, selectedData);
            }

            function clear() {
                selectedData = {};
                removeAllDOMItem();
            }

            function init(data) {
                if (data) {
                    data = $.isArray(data) ? data : [data];
                    //这里避免datagrid还没获取到数据，所以进行两次处理
                    add(data);
                    $(data).each(function (i, item) {
                        item.__id__ = item[_idField];
                        $datagrid.datagrid('selectRecord', item.__id__);
                    });
                }
            }

            return {
                $datagrid: $datagrid,
                init: init,
                add: add,
                remove: remove,
                get: get,
                getSelectedObj: getSelectedObj,
                clear: clear
            }
        }

        $(function () {
            var $datagrid = $('.easyui-datagrid.selection-multi');
            if ($datagrid.length) {
                var idField = $datagrid.attr(ID_FIELD) || 'id';
                var nameField = $datagrid.attr(NAME_FIELD) || 'name';
                var selection = $.selection = new MultiSelection($datagrid, idField, nameField);
                $datagrid.datagrid({
                    onSelect: function (index, row) {
                        selection.add(index,row);
                    },
                    onUnselect: function (index, row) {
                        selection.remove(row);
                    },
                    onSelectAll: function (rows) {
                        selection.add(rows);
                    },
                    onUnselectAll: function (rows) {
                        selection.remove(rows);
                    },
                    onLoadSuccess: function (data) {
                        var selectedObj = selection.getSelectedObj();
                        var rows = $datagrid.datagrid('getRows');
                        var idField = $datagrid.attr(ID_FIELD) || 'id';
                        $(rows).each(function (i, item) {
                            if (selectedObj[item[idField]]) {
                                $datagrid.datagrid('selectRow', i);
                            }
                        });
                    }
                });
            }
        });

        /*
        * 单选
        * **********************************************/
        function SingleSelection($datagrid, idField, nameField) {
            var _idField = idField, _nameField = nameField;
            var selectedData;

            function set(data) {
                data.__id__ = data[_idField];
                data.__name__ = data[_nameField];
                selectedData && removeDOMItem(selectedData);
                selectedData = data;
                selectedData && addDOMItem(selectedData);
            }

            function get() {
                return selectedData && $.extend({}, selectedData) || null;
            }

            function clear() {
                set(null);
            }

            function init(data) {
                if (data) {
                    set(data);
                    $datagrid.datagrid('selectRecord', data.__id__);
                }
            }

            return {
                $datagrid: $datagrid,
                set: set,
                get: get,
                clear: clear,
                init: init
            }
        }

        $(function () {
            var $datagrid = $('.easyui-datagrid.selection-single');
            if ($datagrid.length) {
                var idField = $datagrid.attr(ID_FIELD) || 'id';
                var nameField = $datagrid.attr(NAME_FIELD) || 'name';
                var selection = $.selection = new SingleSelection($datagrid, idField, nameField);
                $datagrid.datagrid({
                    onSelect: function (index, row) {
                        selection.set(row);
                    },
                    onUnselect: function (index, row) {
                        selection.set(null);
                    },
                    onAfterRender: function () {
                        var selectedObj = selection.get();
                        if (selectedObj) {
                            var rows = $datagrid.datagrid('getRows');
                            $(rows).each(function (i, item) {
                                if (selectedObj[item.id]) {
                                    $datagrid.datagrid('selectRow', i);
                                }
                            });
                        }
                    }
                });
            }
        });

        $(function () {
            var $datagrid = $('.easyui-datagrid');
            $('.selected-container').on('click', '.Hui-iconfont-del', function () {
                var $this = $(this);
                var idField = $datagrid.attr(ID_FIELD) || 'id';
                var id = $this.parent('[data-id]').data('id');
                var rows = $datagrid.datagrid('getRows');

                // 删除选中数据
                var data = {};
                data[idField] = id;
                $.selection.remove(data);

                //取消选择表格
                $(rows).each(function (i, item) {
                    if (item[idField] === id) {
                        $datagrid.datagrid('unselectRow', i);
                    }
                });
            });
        });

        $(function () {
            $('.btn-cancel').click(function () {
                if (parent && parent.layer) {
                    param.cancel && parent[param.cancel] && parent[param.cancel]();
                    var index = parent.layer.getFrameIndex(window.name);
                    parent.layer.close(index);
                }
            });
            $('.btn-save').click(function () {
                if (parent && parent.layer) {
                    param.yes && parent[param.yes] && parent[param.yes]($.selection.get(), param);
                    var index = parent.layer.getFrameIndex(window.name);
                    parent.layer.close(index);
                }
            });
        });

    }
    (jQuery)
)
;