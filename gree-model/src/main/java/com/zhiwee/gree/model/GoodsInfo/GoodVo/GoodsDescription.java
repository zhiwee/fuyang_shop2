package com.zhiwee.gree.model.GoodsInfo.GoodVo;

import com.zhiwee.gree.model.GoodsInfo.GoodsDetail;
import com.zhiwee.gree.model.GoodsInfo.ShopGoodsInfo;
import com.zhiwee.gree.model.GoodsInfo.ShopGoodsPicture;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author sun on 2019/5/9
 */
public class GoodsDescription implements Serializable {

    private List<ShopGoodsInfo> goodsInfos = new ArrayList<>();
    private List<ShopGoodsPicture> goodsPictures = new ArrayList<>();

    private GoodsDetail goodsDetail;

    public List<ShopGoodsPicture> getGoodsPictures() {
        return goodsPictures;
    }

    public void setGoodsPictures(List<ShopGoodsPicture> goodsPictures) {
        this.goodsPictures = goodsPictures;
    }

    public List<ShopGoodsInfo> getGoodsInfos() {
        return goodsInfos;
    }

    public void setGoodsInfos(List<ShopGoodsInfo> goodsInfos) {
        this.goodsInfos = goodsInfos;
    }

    public GoodsDetail getGoodsDetail() {
        return goodsDetail;
    }

    public void setGoodsDetail(GoodsDetail goodsDetail) {
        this.goodsDetail = goodsDetail;
    }
}
