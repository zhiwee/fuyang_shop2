package com.zhiwee.gree.model.ret;

public enum RoleReturnMsg {

	NAME_EMPTY("角色名称不能为空"),
	NAME_MANY("角色名称不能重复"),
	USERID_EMPTY("用户id不能为空"),
	ROLEIDS_EMPTY("角色id不能为空"),
	;

	private String message;

	RoleReturnMsg(String message) {
		this.message = message;
	}


	public String message() {
		return message;
	}

}
