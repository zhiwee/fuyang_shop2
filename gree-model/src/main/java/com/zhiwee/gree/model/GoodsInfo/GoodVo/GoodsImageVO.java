package com.zhiwee.gree.model.GoodsInfo.GoodVo;

public class GoodsImageVO {

    private String id;

    private String image;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

}
