package com.zhiwee.gree.model.OrderInfo.vo;

import java.io.Serializable;

/**
 * @author sun on 2019/7/1
 * 字段的顺序以及字段名不能变，这个是按echarts的参数的要求整理的
 */
public class GoodNameTopTen  implements Serializable {
    private Integer value;

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }
}
