package com.zhiwee.gree.model.GoodsInfo.GoodVo;

import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecgframework.poi.excel.annotation.ExcelTarget;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by jick on 2019/8/29.
 */
@ExcelTarget("GoodsFyExo")
public class GoodsFyExo implements Serializable {
    @Excel(name = "商品名称")
    private  String  name;
    @Excel(name = "商品描述")
    private  String describes;
    @Excel(name = "商品编码")
    private  String  goodsNum;
    @Excel(name = "商品分类名称")
    private    String categoryName;
    @Excel(name = "商品品牌名称")
    private  String   brandName;
    @Excel(name = "商品售价")
    private    BigDecimal shopPrice;
    @Excel(name = "标签1")
    private  String   label1parentName;
    @Excel(name = "标签1值")
    private  String   label1value;
    @Excel(name = "标签2")
    private  String   label2parentName;
    @Excel(name = "标签2值")
    private  String   label2value;
    @Excel(name = "标签3")
    private  String   label3parentName;
    @Excel(name = "标签3值")
    private  String   label3value;
    @Excel(name = "库存")
    private  Integer   storeCount;

    public Integer getStoreCount() {
        return storeCount;
    }

    public void setStoreCount(Integer storeCount) {
        this.storeCount = storeCount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescribes() {
        return describes;
    }

    public void setDescribes(String describes) {
        this.describes = describes;
    }

    public String getGoodsNum() {
        return goodsNum;
    }

    public void setGoodsNum(String goodsNum) {
        this.goodsNum = goodsNum;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public BigDecimal getShopPrice() {
        return shopPrice;
    }

    public void setShopPrice(BigDecimal shopPrice) {
        this.shopPrice = shopPrice;
    }

    public String getLabel1parentName() {
        return label1parentName;
    }

    public void setLabel1parentName(String label1parentName) {
        this.label1parentName = label1parentName;
    }

    public String getLabel1value() {
        return label1value;
    }

    public void setLabel1value(String label1value) {
        this.label1value = label1value;
    }

    public String getLabel2parentName() {
        return label2parentName;
    }

    public void setLabel2parentName(String label2parentName) {
        this.label2parentName = label2parentName;
    }

    public String getLabel2value() {
        return label2value;
    }

    public void setLabel2value(String label2value) {
        this.label2value = label2value;
    }

    public String getLabel3parentName() {
        return label3parentName;
    }

    public void setLabel3parentName(String label3parentName) {
        this.label3parentName = label3parentName;
    }

    public String getLabel3value() {
        return label3value;
    }

    public void setLabel3value(String label3value) {
        this.label3value = label3value;
    }
}
