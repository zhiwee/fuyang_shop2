package com.zhiwee.gree.model.ShopDistributor;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import xyz.icrab.common.util.jackson.CustomJsonDateDeserializer;

import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Transient;
/**
 * @author sun on 2019/5/21
 */
@Table(name = "s_distributor_info")
public class ShopDistributor implements Serializable {

    @Id
    private String id;

    private String dealerId;

    private String dealerName;

    private String phone;

    private String parentName;

    private String managerName;
    //地址
    private String userRediss;
    //面积
    private String userSum;

    //1 启用  2 禁用
    private Integer state;

    //1 门店配送商 2 非门店配送商
    private Integer type;

    //1 默认配送商 2 非默认配送商
    private Integer defaultDistributor;

    @Transient
    private String categoryIdPath;

    @Transient
    private String areaId;

    @Transient
    private String areaName;

    @Transient
    private String gradeName;

    @Transient
    private String categoryName;

    @JsonDeserialize(using = CustomJsonDateDeserializer.class)
    private Date createTime;
    @JsonDeserialize(using = CustomJsonDateDeserializer.class)
    private Date updateTime;

    private String createMan;

    private String updateMan;

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getGradeName() {
        return gradeName;
    }

    public void setGradeName(String gradeName) {
        this.gradeName = gradeName;
    }


    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public Integer getDefaultDistributor() {
        return defaultDistributor;
    }

    public void setDefaultDistributor(Integer defaultDistributor) {
        this.defaultDistributor = defaultDistributor;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDealerId() {
        return dealerId;
    }

    public void setDealerId(String dealerId) {
        this.dealerId = dealerId;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getCreateMan() {
        return createMan;
    }

    public void setCreateMan(String createMan) {
        this.createMan = createMan;
    }

    public String getUpdateMan() {
        return updateMan;
    }

    public void setUpdateMan(String updateMan) {
        this.updateMan = updateMan;
    }

    public String getCategoryIdPath() {
        return categoryIdPath;
    }

    public void setCategoryIdPath(String categoryIdPath) {
        this.categoryIdPath = categoryIdPath;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }


    public String getUserRediss() {
        return userRediss;
    }

    public void setUserRediss(String userRediss) {
        this.userRediss = userRediss;
    }

    public String getUserSum() {
        return userSum;
    }

    public void setUserSum(String userSum) {
        this.userSum = userSum;
    }
}
