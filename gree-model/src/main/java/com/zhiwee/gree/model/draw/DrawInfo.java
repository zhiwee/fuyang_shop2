package com.zhiwee.gree.model.draw;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import xyz.icrab.common.model.BaseModel;
import xyz.icrab.common.util.jackson.CustomJsonDateDeserializer;

import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Table(name = "s_draw_info")
public class DrawInfo   extends BaseModel {

    @Id
    private String id;

//名称
    private String name;
//抽奖总量
    private Integer allSum;
//已经抽奖
    private  Integer useSum;

    @JsonDeserialize(using = CustomJsonDateDeserializer.class)
    private Date beginTime;

    @JsonDeserialize(using = CustomJsonDateDeserializer.class)
    private Date endTime;
//类型 1 下单 2 认筹
    private Integer type;
//单人抽奖次数
    private Integer oneSum;

    private Integer state;

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getOneSum() {
        return oneSum;
    }

    public void setOneSum(Integer oneSum) {
        this.oneSum = oneSum;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAllSum() {
        return allSum;
    }

    public void setAllSum(Integer allSum) {
        this.allSum = allSum;
    }

    public Integer getUseSum() {
        return useSum;
    }

    public void setUseSum(Integer useSum) {
        this.useSum = useSum;
    }

    public Date getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(Date beginTime) {
        this.beginTime = beginTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }


}
