package com.zhiwee.gree.model.HomePage.PageV0;

import java.util.List;

public class PageChannelVO {

    private String id;

    private String name;

    private Integer type;

    private List<PageChannelAdimgVO> imageList;

    private List<PageChannelGoodsVO> goodsList;

    private List<PageSearchVO> wordList;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public List<PageChannelAdimgVO> getImageList() {
        return imageList;
    }

    public void setImageList(List<PageChannelAdimgVO> imageList) {
        this.imageList = imageList;
    }

    public List<PageChannelGoodsVO> getGoodsList() {
        return goodsList;
    }

    public void setGoodsList(List<PageChannelGoodsVO> goodsList) {
        this.goodsList = goodsList;
    }

    public List<PageSearchVO> getWordList() {
        return wordList;
    }

    public void setWordList(List<PageSearchVO> wordList) {
        this.wordList = wordList;
    }
}
