package com.zhiwee.gree.model.FyGoods;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Table(name = "t_fy_orderitem")
public class FyOrderItem implements Serializable{
    @Id
    private String id;
    private String userId;
    private String orderNum;
    private String brandId;
    private String brandName;
    private String categorysId;
    private String categorysName;
    @Transient
    private String fyGoodsId;

    private String goodsName;

    private  String  goodsId;

    private   String goodsCover;
    private Date createTime;
    //单价
   // @Transient
    private  BigDecimal  singlePrice;
    private  BigDecimal  giftPrice;
     @Transient
    private  BigDecimal  lowPrice;
    //数量
   // @Transient
    private  Integer  count;
    private  Integer  sendType;
    private  Integer  type;
    private  Integer  jiadian;

    private BigDecimal payPrice;
    private BigDecimal discount;
    private BigDecimal activityCost;

    //抽奖获取

    private  String   draw;
@Transient
    private  String   cover;
    //是否删除
    private  Integer isdelete;
    //1 折扣 2 佣金 3礼品卡 4 现金 5 混合
    private  Integer payType;

    //是否申请退款  1  表示 是  2 表示 否
    private  Integer isrefound;

    //是否已经退款  1  表示是  2 表示否
    private  Integer refoundState;

    //确认操作退款的管理员账号
    private  String operatorName;
    //家电门店
    private  String dealerId;
    private  String dealerName;

@Transient
private BargainGoods bargainGoods;

    @Transient
    private List<BargainOrder> bargainOrders;
    public BargainGoods getBargainGoods() {
        return bargainGoods;
    }

    public void setBargainGoods(BargainGoods bargainGoods) {
        this.bargainGoods = bargainGoods;
    }

    public BigDecimal getLowPrice() {
        return lowPrice;
    }

    public void setLowPrice(BigDecimal lowPrice) {
        this.lowPrice = lowPrice;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public void setType(Integer type) {
        this.type = type;
    }


    public Integer getType() {
        return type;
    }

    public String getDealerId() {
        return dealerId;
    }

    public BigDecimal getGiftPrice() {
        return giftPrice;
    }

    public List<BargainOrder> getBargainOrders() {
        return bargainOrders;
    }

    public void setBargainOrders(List<BargainOrder> bargainOrders) {
        this.bargainOrders = bargainOrders;
    }

    public void setGiftPrice(BigDecimal giftPrice) {
        this.giftPrice = giftPrice;
    }

    public void setDealerId(String dealerId) {
        this.dealerId = dealerId;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getBrandName() {
        return brandName;
    }

    public Integer getPayType() {
        return payType;
    }

    public void setPayType(Integer payType) {
        this.payType = payType;
    }

    public Integer getSendType() {
        return sendType;
    }

    public void setSendType(Integer sendType) {
        this.sendType = sendType;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getCategorysId() {
        return categorysId;
    }

    public void setCategorysId(String categorysId) {
        this.categorysId = categorysId;
    }

    public String getCategorysName() {
        return categorysName;
    }

    public void setCategorysName(String categorysName) {
        this.categorysName = categorysName;
    }

    public String getId() {
        return id;
    }


    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public BigDecimal getActivityCost() {
        return activityCost;
    }

    public void setActivityCost(BigDecimal activityCost) {
        this.activityCost = activityCost;
    }

    public Integer getJiadian() {
        return jiadian;
    }

    public void setJiadian(Integer jiadian) {
        this.jiadian = jiadian;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrderNum() {
        return orderNum;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getFyGoodsId() {
        return fyGoodsId;
    }

    public void setFyGoodsId(String fyGoodsId) {
        this.fyGoodsId = fyGoodsId;
    }

    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }

    public String getGoodsCover() {
        return goodsCover;
    }

    public void setGoodsCover(String goodsCover) {
        this.goodsCover = goodsCover;
    }

    public BigDecimal getPayPrice() {
        return payPrice;
    }

    public void setPayPrice(BigDecimal payPrice) {
        this.payPrice = payPrice;
    }

    public BigDecimal getSinglePrice() {
        return singlePrice;
    }

    public void setSinglePrice(BigDecimal singlePrice) {
        this.singlePrice = singlePrice;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getDraw() {
        return draw;
    }

    public void setDraw(String draw) {
        this.draw = draw;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getIsdelete() {
        return isdelete;
    }

    public void setIsdelete(Integer isdelete) {
        this.isdelete = isdelete;
    }

    public Integer getIsrefound() {
        return isrefound;
    }

    public void setIsrefound(Integer isrefound) {
        this.isrefound = isrefound;
    }

    public Integer getRefoundState() {
        return refoundState;
    }

    public void setRefoundState(Integer refoundState) {
        this.refoundState = refoundState;
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }



   /*@Override
    public int compareTo(FyOrderItem o) {
        int i = o.getSinglePrice().subtract(o.getSinglePrice()).intValue();

        if(i==0){
            i=this.getPayPrice().subtract(o.getPayPrice()).intValue();
            if(i==0){
                i =this.getCount()-o.getCount();
            }

        }
        return i;
    }*/
}