package com.zhiwee.gree.model.GoodsInfo;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by jick on 2019/8/29.
 */
@Table(name="s_fygoods_info")
public class Goods implements Serializable {

    @Id
    private   String  id;

    private  String  name;
    private  String  goodsId;

    private  String describes;

    private  String  goodsNum;

    private  String goodsMaterial;

    private  String categoryId;
    //二级分类用来展示商品
    private  String parentCategory;
    //楼层
    private  String floor;
    //直播商品地址
    private  String liveUrl;

    @Transient
    private BigDecimal price;
    @Transient
    private BigDecimal secKillPrice;
    @Transient
    private  Integer seckillstate;
    @Transient
    private  String activityId;
    @Transient
    private  String activityName;

    @Transient
    private Date beginTime;

    @Transient
    private Date ebdTime;

    @Transient
    private Date canCoupon;

    @Transient
    private BigDecimal seckillprice;

    private    String categoryName;

    @Transient
    private  String   backPicture;

    @Transient
    private  String   lastPicture;

    private  String   brandName;
    private BigDecimal activePrice;

    private    BigDecimal shopPrice;
    @Transient
    private GoodsBase goodsBase;

    private    String goodCover;

    private    String weight;

    private   Integer  storeCount;
    private   Integer  sendType;

    private   Integer  saleCount;
    private   Integer  jiadian;

    private   Integer   giveIntegral;

    private   Integer  exchangeIntegral;

    private    String   words;

    @Transient
    private List<String> urlList;
    private   Integer sort;

    private    BigDecimal commission;

    private    Integer   state;

    private  String label1id ;
    private  String label1parentId ;
    private  String label1parentName ;
    private  String label2parentName ;
    private  String label3parentName ;
    private  String label2parentId;
    private  String label3parentId ;

    private  String label2id ;
    private  String label3id ;
    private  String label1value;
    private  String label2value;
    private  String label3value;


    public String getLabel1parentId() {
        return label1parentId;
    }

    public void setLabel1parentId(String label1parentId) {
        this.label1parentId = label1parentId;
    }

    public String getLabel1parentName() {
        return label1parentName;
    }

    public void setLabel1parentName(String label1parentName) {
        this.label1parentName = label1parentName;
    }

    public String getLabel2parentName() {
        return label2parentName;
    }

    public void setLabel2parentName(String label2parentName) {
        this.label2parentName = label2parentName;
    }

    public String getLabel3parentName() {
        return label3parentName;
    }

    public void setLabel3parentName(String label3parentName) {
        this.label3parentName = label3parentName;
    }

    public String getLabel2parentId() {
        return label2parentId;
    }


    public Integer getSendType() {
        return sendType;
    }

    public void setSendType(Integer sendType) {
        this.sendType = sendType;
    }

    public void setLabel2parentId(String label2parentId) {
        this.label2parentId = label2parentId;
    }

    public String getLabel3parentId() {
        return label3parentId;
    }

    public void setLabel3parentId(String label3parentId) {
        this.label3parentId = label3parentId;
    }

    public String getLabel1id() {
        return label1id;
    }

    public void setLabel1id(String label1id) {
        this.label1id = label1id;
    }

    public String getLabel2id() {
        return label2id;
    }

    public void setLabel2id(String label2id) {
        this.label2id = label2id;
    }

    public String getLabel3id() {
        return label3id;
    }

    public void setLabel3id(String label3id) {
        this.label3id = label3id;
    }

    public String getLabel1value() {
        return label1value;
    }

    public void setLabel1value(String label1value) {
        this.label1value = label1value;
    }

    public String getLabel2value() {
        return label2value;
    }

    public void setLabel2value(String label2value) {
        this.label2value = label2value;
    }

    public String getLabel3value() {
        return label3value;
    }

    public void setLabel3value(String label3value) {
        this.label3value = label3value;
    }

    public String getParentCategory() {
        return parentCategory;
    }

    public GoodsBase getGoodsBase() {
        return goodsBase;
    }

    public void setGoodsBase(GoodsBase goodsBase) {
        this.goodsBase = goodsBase;
    }

    public void setParentCategory(String parentCategory) {
        this.parentCategory = parentCategory;
    }

    public List<String> getUrlList() {
        return urlList;
    }

    public Integer getJiadian() {
        return jiadian;
    }

    public void setJiadian(Integer jiadian) {
        this.jiadian = jiadian;
    }

    public void setUrlList(List<String> urlList) {
        this.urlList = urlList;
    }

    //评论数量
    private Integer commentNum;


    //购买的数量
  /*  @Transient
    private  Integer  count;*/




    private String brandId;


    public String getLiveUrl() {
        return liveUrl;
    }

    public void setLiveUrl(String liveUrl) {
        this.liveUrl = liveUrl;
    }

    public String getFloor() {
        return floor;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public Date getCanCoupon() {
        return canCoupon;
    }

    public void setCanCoupon(Date canCoupon) {
        this.canCoupon = canCoupon;
    }

    public BigDecimal getSecKillPrice() {
        return secKillPrice;
    }

    public void setSecKillPrice(BigDecimal secKillPrice) {
        this.secKillPrice = secKillPrice;
    }

    public String getActivityId() {
        return activityId;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public Date getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(Date beginTime) {
        this.beginTime = beginTime;
    }

    public Date getEbdTime() {
        return ebdTime;
    }

    public void setEbdTime(Date ebdTime) {
        this.ebdTime = ebdTime;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public Integer getSeckillstate() {
        return seckillstate;
    }

    public void setSeckillstate(Integer seckillstate) {
        this.seckillstate = seckillstate;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescribes() {
        return describes;
    }

    public void setDescribes(String describes) {
        this.describes = describes;
    }

    public String getGoodsNum() {
        return goodsNum;
    }

    public void setGoodsNum(String goodsNum) {
        this.goodsNum = goodsNum;
    }

    public String getGoodsMaterial() {
        return goodsMaterial;
    }

    public void setGoodsMaterial(String goodsMaterial) {
        this.goodsMaterial = goodsMaterial;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public BigDecimal getActivePrice() {
        return activePrice;
    }

    public void setActivePrice(BigDecimal activePrice) {
        this.activePrice = activePrice;
    }

    public BigDecimal getShopPrice() {
        return shopPrice;
    }

    public void setShopPrice(BigDecimal shopPrice) {
        this.shopPrice = shopPrice;
    }

    public String getGoodCover() {
        return goodCover;
    }

    public void setGoodCover(String goodCover) {
        this.goodCover = goodCover;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public Integer getStoreCount() {
        return storeCount;
    }

    public void setStoreCount(Integer storeCount) {
        this.storeCount = storeCount;
    }

    public Integer getSaleCount() {
        return saleCount;
    }

    public void setSaleCount(Integer saleCount) {
        this.saleCount = saleCount;
    }

    public Integer getGiveIntegral() {
        return giveIntegral;
    }

    public void setGiveIntegral(Integer giveIntegral) {
        this.giveIntegral = giveIntegral;
    }

    public Integer getExchangeIntegral() {
        return exchangeIntegral;
    }

    public void setExchangeIntegral(Integer exchangeIntegral) {
        this.exchangeIntegral = exchangeIntegral;
    }

    public String getWords() {
        return words;
    }

    public void setWords(String words) {
        this.words = words;
    }



    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public BigDecimal getCommission() {
        return commission;
    }

    public void setCommission(BigDecimal commission) {
        this.commission = commission;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public BigDecimal getSeckillprice() {
        return seckillprice;
    }

    public void setSeckillprice(BigDecimal seckillprice) {
        this.seckillprice = seckillprice;
    }

    public Integer getCommentNum() {
        return commentNum;
    }

    public void setCommentNum(Integer commentNum) {
        this.commentNum = commentNum;
    }


    public String getBackPicture() {
        return backPicture;
    }

    public void setBackPicture(String backPicture) {
        this.backPicture = backPicture;
    }

    public String getLastPicture() {
        return lastPicture;
    }

    public void setLastPicture(String lastPicture) {
        this.lastPicture = lastPicture;
    }

    public String getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }

    /* public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }*/
}
