package com.zhiwee.gree.model.ShopUserComment;

import java.io.Serializable;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import xyz.icrab.common.util.jackson.CustomJsonDateDeserializer;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Date;
/**
 * @author sun on 2019/6/4
 */
@Table(name="s_comment_picture")
public class CommentPicture implements Serializable {

    @Id
    private String id;

    private String commentId;

    private String picture;

    @JsonDeserialize(
            using = CustomJsonDateDeserializer.class
    )
    private Date createTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCommentId() {
        return commentId;
    }

    public void setCommentId(String commentId) {
        this.commentId = commentId;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
