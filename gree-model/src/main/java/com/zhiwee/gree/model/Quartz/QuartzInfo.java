package com.zhiwee.gree.model.Quartz;

import java.io.Serializable;
import java.io.Serializable;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import xyz.icrab.common.util.jackson.CustomJsonDateDeserializer;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * @author sun on 2019/7/3
 */
@Table(name="s_sys_quartz")
public class QuartzInfo implements Serializable {

    @Id
    private String id;


    /**
     * 触发器组名
     */
    private String  triggerGroupName;



    /**
     * 任务组名
     */
    private String  jobGroupName;


    @JsonDeserialize(
            using = CustomJsonDateDeserializer.class
    )
    private  Date createTime;

    @JsonDeserialize(
            using = CustomJsonDateDeserializer.class
    )
    private Date updateTime;

    /**
     * 执行小时时间
     */
    private Integer  hour;


    /**
     * 执行分钟时间
     */
    private Integer  min;


    /**
     * 描述
     */
    private String  description;


    /**
     * 任务名
     */
    private String  jobName;


    /**
     * 触发器名
     */
    private String triggerName;

    /**
     *   1 启用 2 禁用
     */
    private Integer state;


    public Integer getHour() {
        return hour;
    }

    public void setHour(Integer hour) {
        this.hour = hour;
    }

    public Integer getMin() {
        return min;
    }

    public void setMin(Integer min) {
        this.min = min;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTriggerGroupName() {
        return triggerGroupName;
    }

    public void setTriggerGroupName(String triggerGroupName) {
        this.triggerGroupName = triggerGroupName;
    }

    public String getJobGroupName() {
        return jobGroupName;
    }

    public void setJobGroupName(String jobGroupName) {
        this.jobGroupName = jobGroupName;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }



    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getTriggerName() {
        return triggerName;
    }

    public void setTriggerName(String triggerName) {
        this.triggerName = triggerName;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }
}
