package com.zhiwee.gree.model.ret;

public enum InterfaceReturnMsg {

	NAME_EMPTY("接口名称不能为空"),
	URL_EMPTY("接口地址不能为空"),
	CHECKAUTH_EMPTY("是否建权不能为空"),
	MODULEID_EMPTY("模块名称不能为空"),
	USERID_EMPTY("用户id不能为空"),
	INTERFACEIDS_EMPTY("接口id不能为空"),
	ROLEIDS_EMPTY("角色id不能为空"),
	;

	private String message;

	InterfaceReturnMsg(String message) {
		this.message = message;
	}


	public String message() {
		return message;
	}

}
