package com.zhiwee.gree.model.MemberGrade;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import xyz.icrab.common.util.jackson.CustomJsonDateDeserializer;
import javax.persistence.Transient;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * @author sun on 2019/5/11
 */
@Table(name="s_membergrade_change")
public class MemberGradeChange implements Serializable {

    @Id
    private String id;

    private Double nowIntegral;

    private Double oldIntegral;

    private Integer nowGrade;

    private Integer oldGrade;


    private Double nowAmount;

    private Double oldAmount;
    @JsonDeserialize(
            using = CustomJsonDateDeserializer.class
    )
    private Date createTime;

    private String   userId;

    @Transient
    private String   userName;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getNowGrade() {
        return nowGrade;
    }

    public void setNowGrade(Integer nowGrade) {
        this.nowGrade = nowGrade;
    }

    public Integer getOldGrade() {
        return oldGrade;
    }

    public void setOldGrade(Integer oldGrade) {
        this.oldGrade = oldGrade;
    }

    public Double getNowAmount() {
        return nowAmount;
    }

    public void setNowAmount(Double nowAmount) {
        this.nowAmount = nowAmount;
    }

    public Double getOldAmount() {
        return oldAmount;
    }

    public void setOldAmount(Double oldAmount) {
        this.oldAmount = oldAmount;
    }

    public Double getNowIntegral() {
        return nowIntegral;
    }

    public void setNowIntegral(Double nowIntegral) {
        this.nowIntegral = nowIntegral;
    }

    public Double getOldIntegral() {
        return oldIntegral;
    }

    public void setOldIntegral(Double oldIntegral) {
        this.oldIntegral = oldIntegral;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
