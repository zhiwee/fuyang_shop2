package com.zhiwee.gree.model.AchieveGift.vo;

import java.io.Serializable;

/**
 * @author sun on 2019/7/5
 */
public class GiftCount  implements Serializable {
private String giftname;
    private Long num;


    public String getGiftname() {
        return giftname;
    }

    public void setGiftname(String giftname) {
        this.giftname = giftname;
    }

    public Long getNum() {
        return num;
    }

    public void setNum(Long num) {
        this.num = num;
    }
}
