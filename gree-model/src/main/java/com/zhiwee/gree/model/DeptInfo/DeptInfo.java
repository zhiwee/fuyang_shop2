package com.zhiwee.gree.model.DeptInfo;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import xyz.icrab.common.model.annotation.Like;
import xyz.icrab.common.util.jackson.CustomJsonDateDeserializer;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by jick on 2019/8/23.
 */
@Table(name="s_sys_deptInfo")
public class DeptInfo  implements Serializable {

    @Id
    private String id;

    @Like
    private String idPath;

    private String partementId;
    @Transient
   private String partement;

    @JsonDeserialize(
            using = CustomJsonDateDeserializer.class
    )
    private  Date createTime;

    @JsonDeserialize(
            using = CustomJsonDateDeserializer.class
    )
    private Date updateTime;

    //1 公司部门  2 格力工厂
    private Integer type;

    private String parentId;
    private String parentName;

  private String userId;


    //1 启用 2 禁用
    private Integer state;


    //验证信息


    //门店名称
    private String dealerName;
    //
    private String shop;
    //所在城市
    private String city;
    //真实姓名
    private String name;
    //手机号
    private String tel;
    //是否技术安装工
    private String install;
    //部门分支
    private String branch;
    //工厂
    private String factory;

    // 等级
    private  Double rank1;
    private  Double rank2;
    private  Double rank3;
    private  Double rank4;
    private  Integer rank;
    private Double integral;


    public String getPartement() {
        return partement;
    }

    public void setPartement(String partement) {
        this.partement = partement;
    }

    public Double getIntegral() {
        return integral;
    }

    public void setIntegral(Double integral) {
        this.integral = integral;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }


    public Double getRank1() {
        return rank1;
    }

    public void setRank1(Double rank1) {
        this.rank1 = rank1;
    }

    public Double getRank2() {
        return rank2;
    }

    public void setRank2(Double rank2) {
        this.rank2 = rank2;
    }

    public Double getRank3() {
        return rank3;
    }

    public void setRank3(Double rank3) {
        this.rank3 = rank3;
    }

    public Double getRank4() {
        return rank4;
    }

    public void setRank4(Double rank4) {
        this.rank4 = rank4;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdPath() {
        return idPath;
    }

    public void setIdPath(String idPath) {
        this.idPath = idPath;
    }

    public String getPartementId() {
        return partementId;
    }

    public void setPartementId(String partementId) {
        this.partementId = partementId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public String getShop() {
        return shop;
    }

    public void setShop(String shop) {
        this.shop = shop;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getInstall() {
        return install;
    }

    public void setInstall(String install) {
        this.install = install;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getFactory() {
        return factory;
    }

    public void setFactory(String factory) {
        this.factory = factory;
    }
}
