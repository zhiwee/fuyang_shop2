package com.zhiwee.gree.model.card;

import xyz.icrab.common.model.BaseModel;

import javax.persistence.Id;
import javax.persistence.Table;

@Table(name="t_gift_card")
public class GiftCard extends BaseModel {
    @Id
    private String id;
    private String name;
    private String giftUrl;
    //说明
    private String describes;
    //预售金额
    private Double giftMoney;
    //实际抵用金额
    private Double realityMoney;
    //可以购买次数
    private Integer numbers;
    private Integer limitNum;

    //状态（1、正常，2删除）
    private Integer state;


    public String getGiftUrl() {
        return giftUrl;
    }

    public void setGiftUrl(String giftUrl) {
        this.giftUrl = giftUrl;
    }

    public Double getGiftMoney() {
        return giftMoney;
    }

    public void setGiftMoney(Double giftMoney) {
        this.giftMoney = giftMoney;
    }

    public String getDescribes() {
        return describes;
    }

    public void setDescribes(String describes) {
        this.describes = describes;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getNumbers() {
        return numbers;
    }

    public void setNumbers(Integer numbers) {
        this.numbers = numbers;
    }


    public Double getRealityMoney() {
        return realityMoney;
    }

    public void setRealityMoney(Double realityMoney) {
        this.realityMoney = realityMoney;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getLimitNum() {
        return limitNum;
    }

    public void setLimitNum(Integer limitNum) {
        this.limitNum = limitNum;
    }
}
