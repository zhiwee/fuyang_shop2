package com.zhiwee.gree.model.card;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecgframework.poi.excel.annotation.ExcelTarget;
import xyz.icrab.common.util.jackson.CustomJsonDateDeserializer;

import javax.persistence.Id;
import java.util.Date;

/**
 * TODO
 *
 * @author Chensong
 * @Company http://www.zhiwee.com
 * date 2020-02-22
 */
@ExcelTarget("BookingCardsExrt")
public class BookingCardsExrt {
    //状态
    //-1：超时未支付，0：待支付，1：已支付,未领取，2：已退款 3 已经领取
    @Excel(name = "状态", replace ={"待支付_0","已支付_1","已领取_3"})
    private Integer state;

    //商品名称
    @Excel(name = "商品名称", needMerge = true)
    private String goodName;

    //商品详情
    @Excel(name = "商品详情", needMerge = true)
    private String goodDetail;

    //客户手机号
    @Excel(name = "客户手机号", needMerge = true)
    private String phone;

    //客户姓名
    @Excel(name = "客户姓名", needMerge = true)
    private String userName;

    //预售金额
    @Excel(name = "预售金额", needMerge = true)
    private Double bookingMoney;

    //实际抵用金额
    @Excel(name = "抵用金额", needMerge = true)
    private Double realityMoney;

    //支付时间
    @Excel(name = "购买时间", needMerge = true,exportFormat = "yyyy-MM-dd HH:mm:ss")
    private Date paytime;

    //阜阳商厦推荐号
    @Excel(name = "阜阳商厦推荐号", needMerge = true)
    private String fyCode;

    //阜阳商厦核销人
    @Excel(name = "阜阳商厦核销人", needMerge = true)
    private String confirmMan;

    //阜阳商厦核销时间
    @Excel(name = "核销时间", needMerge = true,exportFormat = "yyyy-MM-dd HH:mm:ss")
    private Date confirmTime;

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getGoodName() {
        return goodName;
    }

    public void setGoodName(String goodName) {
        this.goodName = goodName;
    }

    public String getGoodDetail() {
        return goodDetail;
    }

    public void setGoodDetail(String goodDetail) {
        this.goodDetail = goodDetail;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Double getBookingMoney() {
        return bookingMoney;
    }

    public void setBookingMoney(Double bookingMoney) {
        this.bookingMoney = bookingMoney;
    }

    public Double getRealityMoney() {
        return realityMoney;
    }

    public void setRealityMoney(Double realityMoney) {
        this.realityMoney = realityMoney;
    }

    public Date getPaytime() {
        return paytime;
    }

    public void setPaytime(Date paytime) {
        this.paytime = paytime;
    }

    public String getFyCode() {
        return fyCode;
    }

    public void setFyCode(String fyCode) {
        this.fyCode = fyCode;
    }

    public String getConfirmMan() {
        return confirmMan;
    }

    public void setConfirmMan(String confirmMan) {
        this.confirmMan = confirmMan;
    }

    public Date getConfirmTime() {
        return confirmTime;
    }

    public void setConfirmTime(Date confirmTime) {
        this.confirmTime = confirmTime;
    }
}
