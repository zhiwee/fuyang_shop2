package com.zhiwee.gree.model.ret;

public enum ModuleReturnMsg {

	NAME_EMPTY("模块名称不能为空"),
	;

	private String message;

	ModuleReturnMsg(String message) {
		this.message = message;
	}


	public String message() {
		return message;
	}

}
