package com.zhiwee.gree.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections4.CollectionUtils;
/**
 * Description:
 * @author: sun
 * @Date 下午2:19 2019/5/26
 * @param:
 * @return:
 */
public class IdentifiedUser implements Serializable {


        private User user;
        private List<Role> roles;
        @JsonIgnore
        private Map<String, Role> roleMap;
        private List<Interface> interfaces;
        @JsonIgnore
        private Map<String, Interface> interfaceMap;
        private List<Menu> menus;
        @JsonIgnore
        private Map<String, Menu> menuMap;
        private List<Menu> buttons;
        @JsonIgnore
        private Map<String, Menu> buttonMap;
        private Object bizData;

        public IdentifiedUser() {
        }

        public IdentifiedUser(User user) {
            this.user = user;
        }

        public User getUser() {
            return this.user;
        }

        public void setUser(User user) {
            this.user = user;
        }

        public List<Role> getRoles() {
            return this.roles;
        }

        @JsonIgnore
        public Map<String, Role> getRoleMap() {
            return this.getRoleMap0();
        }

        public boolean hasRole(String code) {
            return this.getRoleMap().containsKey(code);
        }

        @JsonIgnore
        public Map<String, Interface> getInterfaceMap() {
            return this.getInterfaceMap0();
        }

        public boolean hasInterface(String url) {
            return this.getInterfaceMap().containsKey(url);
        }

        @JsonIgnore
        public Map<String, Menu> getMenuMap() {
            return this.getMenuMap0();
        }

        public boolean hasMenu(String code) {
            return this.getMenuMap().containsKey(code);
        }

        @JsonIgnore
        public Map<String, Menu> getButtonMap() {
            return this.getButtonMap0();
        }

        public boolean hasButton(String code) {
            return this.getButtonMap().containsKey(code);
        }

        @JsonIgnore
        private Map<String, Menu> getButtonMap0() {
            if (this.buttonMap != null) {
                return this.buttonMap;
            } else {
                if (CollectionUtils.isNotEmpty(this.buttons)) {
                    Map<String, Menu> buttonMap = new HashMap();
                    Iterator var2 = this.buttons.iterator();

                    while(var2.hasNext()) {
                        Menu menu = (Menu)var2.next();
                        buttonMap.put(menu.getCode(), menu);
                    }

                    this.buttonMap = buttonMap;
                } else {
                    this.buttonMap = Collections.emptyMap();
                }

                return this.buttonMap;
            }
        }

        @JsonIgnore
        private Map<String, Menu> getMenuMap0() {
            if (this.menuMap != null) {
                return this.menuMap;
            } else {
                if (CollectionUtils.isNotEmpty(this.menus)) {
                    Map<String, Menu> menuMap = new HashMap();
                    Iterator var2 = this.menus.iterator();

                    while(var2.hasNext()) {
                        Menu menu = (Menu)var2.next();
                        menuMap.put(menu.getCode(), menu);
                    }

                    this.menuMap = menuMap;
                } else {
                    this.menuMap = Collections.emptyMap();
                }

                return this.menuMap;
            }
        }

        @JsonIgnore
        private Map<String, Interface> getInterfaceMap0() {
            if (this.interfaceMap != null) {
                return this.interfaceMap;
            } else {
                if (CollectionUtils.isNotEmpty(this.interfaces)) {
                    Map<String, Interface> interfaceMap = new HashMap();
                    Iterator var2 = this.interfaces.iterator();

                    while(var2.hasNext()) {
                        Interface i = (Interface)var2.next();
                        interfaceMap.put(i.getUrl(), i);
                    }

                    this.interfaceMap = interfaceMap;
                } else {
                    this.interfaceMap = Collections.emptyMap();
                }

                return this.interfaceMap;
            }
        }

        @JsonIgnore
        private Map<String, Role> getRoleMap0() {
            if (this.roleMap != null) {
                return this.roleMap;
            } else {
                if (CollectionUtils.isNotEmpty(this.roles)) {
                    Map<String, Role> roleMap = new HashMap();
                    Iterator var2 = this.roles.iterator();

                    while(var2.hasNext()) {
                        Role role = (Role)var2.next();
                        roleMap.put(role.getCode(), role);
                    }

                    this.roleMap = roleMap;
                } else {
                    this.roleMap = Collections.emptyMap();
                }

                return this.roleMap;
            }
        }

        public void setRoles(List<Role> roles) {
            this.roles = roles;
        }

        public List<Interface> getInterfaces() {
            return this.interfaces;
        }

        public void setInterfaces(List<Interface> interfaces) {
            this.interfaces = interfaces;
        }

        public List<Menu> getMenus() {
            return this.menus;
        }

        public void setMenus(List<Menu> menus) {
            this.menus = menus;
        }

        public List<Menu> getButtons() {
            return this.buttons;
        }

        public void setButtons(List<Menu> buttons) {
            this.buttons = buttons;
        }

        public Object getBizData() {
            return this.bizData;
        }

        public void setBizData(Object bizData) {
            this.bizData = bizData;
        }


}
