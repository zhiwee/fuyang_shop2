package com.zhiwee.gree.model.Coupon;


import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.springframework.format.annotation.DateTimeFormat;
import xyz.icrab.common.util.jackson.CustomJsonDateDeserializer;

import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Table(name="s_coupon_subject")

public class CouponSubject  implements Serializable {
    @Id
    private String id;

    private Integer state;
   // 类型1 全品类优惠券 2 非全品类优惠券 3指定某些商品的优惠券  4全品类代金券  5 非全品类代金券 6指定某些商品的代金券
    private Integer type;

    private String name;

    private Date createTime;
    private Double integral;

    private String createMan;

    private Integer allNum;

    private Date updateTime;

    private  String updateMan;

    @JsonDeserialize(using = CustomJsonDateDeserializer.class)
    private Date beginTime;

    @JsonDeserialize(using = CustomJsonDateDeserializer.class)
    private  Date endTime;
//价值
    private Double  forMoney;
//标注
private Double lowMoney;
//价格标准
    private Integer typePrice;

    public Integer getTypePrice() {
        return typePrice;
    }

    public void setTypePrice(Integer typePrice) {
        this.typePrice = typePrice;
    }

    public Double getLowMoney() {
        return lowMoney;
    }

    public void setLowMoney(Double lowMoney) {
        this.lowMoney = lowMoney;
    }

    public Double getForMoney() {
        return forMoney;
    }

    public void setForMoney(Double forMoney) {
        this.forMoney = forMoney;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Date getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(Date beginTime) {
        this.beginTime = beginTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreateMan() {
        return createMan;
    }

    public void setCreateMan(String createMan) {
        this.createMan = createMan;
    }

    public Integer getAllNum() {
        return allNum;
    }

    public void setAllNum(Integer allNum) {
        this.allNum = allNum;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getUpdateMan() {
        return updateMan;
    }

    public Double getIntegral() {
        return integral;
    }

    public void setIntegral(Double integral) {
        this.integral = integral;
    }

    public void setUpdateMan(String updateMan) {
        this.updateMan = updateMan;
    }
}
