package com.zhiwee.gree.model.GoodsInfo;
import org.apache.solr.client.solrj.beans.Field;

import java.io.Serializable;

/**
 * @author sun on 2019/4/1
 */
public class  SolrGoodsInfo implements Serializable {

    @Field
    private String id;
    // 商品名称
    @Field
    private String name;

    // 原来的名称
    @Field
    private String orignalName;
    // 品牌
    @Field
    private String brandId;
    @Field
    private String brandName;

    @Field
    private String energyEfficiencyGrade;
    // 规格型号
    @Field
    private String specId;

    // 商品分类
    @Field
    private String category;

    // 商品大分类
    @Field
    private String categoryParent;


    //商品分类名称
    @Field
    private String categoryName;

    // 单价
    @Field
    private Double price;


    // 市场价
    @Field
    private Double marketPrice;

    // 以及代理价格
    @Field
    private Double onePrice;


    // 二级代理价格
    @Field
    private Double twoPrice;


    // 三级代理
    @Field
    private Double threePrice;

    // 单价
    @Field
    private Double activityPrice;

    // 基本计量单位
    @Field
    private String unit;


    //状态： 1：正常 2：下架 3：删除
    @Field
    private Integer state;


    //状态：是否特价
    @Field
    private Integer special;

    @Field
    private String picture;

//  特价
    @Field
    private Double specialPrice;


    @Field
    private String bp;

    @Field
    private String gb;

    @Field
    private String space;


    @Field
    private String ps;


    //是否推荐 1 推荐 2 不是
    @Field
    private Integer recommend;


    //是否是新品 1 是 2 不是
    @Field
    private Integer news;

    //排序
    @Field
    private  Integer sort;
//介绍
    @Field
    private String description;

//标题
    @Field
    private String title;


    public String getCategoryParent() {
        return categoryParent;
    }

    public void setCategoryParent(String categoryParent) {
        this.categoryParent = categoryParent;
    }

    public Integer getSpecial() {
        return special;
    }

    public void setSpecial(Integer special) {
        this.special = special;
    }

    public Double getOnePrice() {
        return onePrice;
    }

    public void setOnePrice(Double onePrice) {
        this.onePrice = onePrice;
    }

    public Double getTwoPrice() {
        return twoPrice;
    }

    public void setTwoPrice(Double twoPrice) {
        this.twoPrice = twoPrice;
    }

    public Double getThreePrice() {
        return threePrice;
    }

    public void setThreePrice(Double threePrice) {
        this.threePrice = threePrice;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Double getSpecialPrice() {
        return specialPrice;
    }

    public void setSpecialPrice(Double specialPrice) {
        this.specialPrice = specialPrice;
    }

    public String getOrignalName() {
        return orignalName;
    }

    public void setOrignalName(String orignalName) {
        this.orignalName = orignalName;
    }

    public Double getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(Double marketPrice) {
        this.marketPrice = marketPrice;
    }

    public Integer getRecommend() {
        return recommend;
    }

    public void setRecommend(Integer recommend) {
        this.recommend = recommend;
    }

    public Integer getNews() {
        return news;
    }

    public void setNews(Integer news) {
        this.news = news;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getEnergyEfficiencyGrade() {
        return energyEfficiencyGrade;
    }

    public void setEnergyEfficiencyGrade(String energyEfficiencyGrade) {
        this.energyEfficiencyGrade = energyEfficiencyGrade;
    }

    public String getSpecId() {
        return specId;
    }

    public void setSpecId(String specId) {
        this.specId = specId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getActivityPrice() {
        return activityPrice;
    }

    public void setActivityPrice(Double activityPrice) {
        this.activityPrice = activityPrice;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getBp() {
        return bp;
    }

    public void setBp(String bp) {
        this.bp = bp;
    }

    public String getGb() {
        return gb;
    }

    public void setGb(String gb) {
        this.gb = gb;
    }

    public String getSpace() {
        return space;
    }

    public void setSpace(String space) {
        this.space = space;
    }

    public String getPs() {
        return ps;
    }

    public void setPs(String ps) {
        this.ps = ps;
    }
}
