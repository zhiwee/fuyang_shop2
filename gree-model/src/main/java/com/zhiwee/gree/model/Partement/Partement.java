package com.zhiwee.gree.model.Partement;

import java.io.Serializable;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import xyz.icrab.common.model.annotation.Like;
import xyz.icrab.common.util.jackson.CustomJsonDateDeserializer;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * @author sun on 2019/6/13
 */
@Table(name="s_sys_partement")
public class Partement implements Serializable {

    @Id
    private String id;

    @Like
    private String idPath;

    private String partement;

    @JsonDeserialize(
            using = CustomJsonDateDeserializer.class
    )
    private  Date createTime;

    @JsonDeserialize(
            using = CustomJsonDateDeserializer.class
    )
    private Date updateTime;

//1 公司部门  2 格力工厂
    private Integer type;

private String parentId;
private String parentName;




    //1 启用 2 禁用
    private Integer state;


    //验证信息


//门店名称
    private Integer dealerName;
//
    private Integer isopenShop;
//所在城市
    private Integer isopenCity;
//真实姓名
    private Integer isopenName;
//手机号
    private Integer  isopenTel;
//是否技术安装工
    private Integer isopenInstall;
//部门分支
    private Integer isopenBranch;
//工厂
    private Integer isopenFactory;


    public Integer getDealerName() {
        return dealerName;
    }

    public void setDealerName(Integer dealerName) {
        this.dealerName = dealerName;
    }

    public Integer getIsopenShop() {
        return isopenShop;
    }

    public void setIsopenShop(Integer isopenShop) {
        this.isopenShop = isopenShop;
    }

    public Integer getIsopenCity() {
        return isopenCity;
    }

    public void setIsopenCity(Integer isopenCity) {
        this.isopenCity = isopenCity;
    }

    public Integer getIsopenName() {
        return isopenName;
    }

    public void setIsopenName(Integer isopenName) {
        this.isopenName = isopenName;
    }

    public Integer getIsopenTel() {
        return isopenTel;
    }

    public void setIsopenTel(Integer isopenTel) {
        this.isopenTel = isopenTel;
    }

    public Integer getIsopenInstall() {
        return isopenInstall;
    }

    public void setIsopenInstall(Integer isopenInstall) {
        this.isopenInstall = isopenInstall;
    }

    public Integer getIsopenBranch() {
        return isopenBranch;
    }

    public void setIsopenBranch(Integer isopenBranch) {
        this.isopenBranch = isopenBranch;
    }

    public Integer getIsopenFactory() {
        return isopenFactory;
    }

    public void setIsopenFactory(Integer isopenFactory) {
        this.isopenFactory = isopenFactory;
    }

    public String getIdPath() {
        return idPath;
    }

    public void setIdPath(String idPath) {
        this.idPath = idPath;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPartement() {
        return partement;
    }

    public void setPartement(String partement) {
        this.partement = partement;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}
