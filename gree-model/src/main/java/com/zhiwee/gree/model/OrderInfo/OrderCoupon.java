package com.zhiwee.gree.model.OrderInfo;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import xyz.icrab.common.util.jackson.CustomJsonDateDeserializer;

import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * @author sun on 2019/5/11
 */
@Table(name="s_order_coupon")
public class OrderCoupon implements Serializable {

    @Id
    private String id;

    private String couponId;

    private String couponSeqno;
    @JsonDeserialize(
            using = CustomJsonDateDeserializer.class
    )
    private Date createTime;
    @JsonDeserialize(
            using = CustomJsonDateDeserializer.class
    )
    private Date updateTime;

    private String orderId;

    private Double couponMoney;

     private Integer couponType;

    public Integer getCouponType() {
        return couponType;
    }

    public void setCouponType(Integer couponType) {
        this.couponType = couponType;
    }

    public String getCouponSeqno() {
        return couponSeqno;
    }

    public void setCouponSeqno(String couponSeqno) {
        this.couponSeqno = couponSeqno;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCouponId() {
        return couponId;
    }

    public void setCouponId(String couponId) {
        this.couponId = couponId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Double getCouponMoney() {
        return couponMoney;
    }

    public void setCouponMoney(Double couponMoney) {
        this.couponMoney = couponMoney;
    }
}
