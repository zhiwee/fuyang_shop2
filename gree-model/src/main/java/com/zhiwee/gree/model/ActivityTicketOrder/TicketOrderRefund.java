package com.zhiwee.gree.model.ActivityTicketOrder;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import xyz.icrab.common.util.jackson.CustomJsonDateDeserializer;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Date;

/**
 * @author sun on 2019/5/11
 */
@Table(name="s_ticketorder_refund")
public class TicketOrderRefund implements Serializable {

    @Id
    private String id;

    private Double totalPrice;

    private Double  refundPrice;


    @JsonDeserialize(
            using = CustomJsonDateDeserializer.class
    )
    private Date  createTime;

    @JsonDeserialize(
            using = CustomJsonDateDeserializer.class
    )
    private Date  updateTime;

    private String updateMan;

    private String createMan;

    private String orderId;

    //退款流水号
    private String  refundNum;

    //所属订单流水号
    @Transient
    private String  orderNum;


//退款的状态 1 全额退款 2 部分退款
    private Integer state;



    public String getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Double getRefundPrice() {
        return refundPrice;
    }

    public void setRefundPrice(Double refundPrice) {
        this.refundPrice = refundPrice;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getUpdateMan() {
        return updateMan;
    }

    public void setUpdateMan(String updateMan) {
        this.updateMan = updateMan;
    }

    public String getCreateMan() {
        return createMan;
    }

    public void setCreateMan(String createMan) {
        this.createMan = createMan;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getRefundNum() {
        return refundNum;
    }

    public void setRefundNum(String refundNum) {
        this.refundNum = refundNum;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

}
