package com.zhiwee.gree.model.FyGoods;

import org.jeecgframework.poi.excel.annotation.Excel;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author DELL
 */
public class FygoodsExo implements Serializable {

    @Excel(name = "商品名称")
    private String goodsName;
    @Excel(name = "原价")
    private BigDecimal price;
    @Excel(name = "秒杀价格")
    private BigDecimal killPrice;
    @Excel(name = "限购数量")
    private Integer  limitCount;
    @Excel(name = "库存")
    private Integer storeCount;
    @Excel(name = "直播地址")
    private String liveUrl;
    @Excel(name = "秒杀开始时间")
    private Date killStartTimeStr;
    @Excel(name = "秒杀结束时间")
    private  Date killEndTimeStr;
    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getKillPrice() {
        return killPrice;
    }

    public void setKillPrice(BigDecimal killPrice) {
        this.killPrice = killPrice;
    }

    public Integer getLimitCount() {
        return limitCount;
    }

    public void setLimitCount(Integer limitCount) {
        this.limitCount = limitCount;
    }

    public Integer getStoreCount() {
        return storeCount;
    }

    public void setStoreCount(Integer storeCount) {
        this.storeCount = storeCount;
    }

    public String getLiveUrl() {
        return liveUrl;
    }

    public void setLiveUrl(String liveUrl) {
        this.liveUrl = liveUrl;
    }

    public Date getKillStartTimeStr() {
        return killStartTimeStr;
    }

    public void setKillStartTimeStr(Date killStartTimeStr) {
        this.killStartTimeStr = killStartTimeStr;
    }

    public Date getKillEndTimeStr() {
        return killEndTimeStr;
    }

    public void setKillEndTimeStr(Date killEndTimeStr) {
        this.killEndTimeStr = killEndTimeStr;
    }
}