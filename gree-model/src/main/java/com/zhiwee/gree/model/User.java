package com.zhiwee.gree.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.zhiwee.gree.model.enums.UserType;
import xyz.icrab.common.model.enums.Gender;
import xyz.icrab.common.model.enums.YesOrNo;
import xyz.icrab.common.model.type.OrderBy;
import javax.persistence.Transient;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * @author sun
 * @since 2018/3/6 22:03
 */
@Table(name = "s_sys_user")
public class User implements Serializable {

    @Id
    @OrderBy(OrderBy.Sort.DESC)
    private String id;

    /**
     * 用户名
     */
    private String username;

    /**
     * 昵称
     */
    private String nickname;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 性别
     */
    private Gender gender;

    /**
     * 用户类型 1 超级管理员 2 系统用户  3 配送商用户
     */
    private UserType type;

    /**
     * 启用状态
     */
    private YesOrNo state;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 密码
     *    @JsonIgnore 代表不穿给前台
     */

    private String password;

    @Transient
    private String roleNames;



    private String distributorName;


    private String distributorId;

    /**
     *  角色id
     */
    @Transient
    private String addRoleIds;

    public String getAddRoleIds() {
        return addRoleIds;
    }

    public void setAddRoleIds(String addRoleIds) {
        this.addRoleIds = addRoleIds;
    }

    public String getDistributorName() {
        return distributorName;
    }

    public void setDistributorName(String distributorName) {
        this.distributorName = distributorName;
    }

    public String getDistributorId() {
        return distributorId;
    }

    public void setDistributorId(String distributorId) {
        this.distributorId = distributorId;
    }

    public String getRoleNames() {
        return roleNames;
    }

    public void setRoleNames(String roleNames) {
        this.roleNames = roleNames;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public UserType getType() {
        return type;
    }

    public void setType(UserType type) {
        this.type = type;
    }

    public YesOrNo getState() {
        return state;
    }

    public void setState(YesOrNo state) {
        this.state = state;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @JsonIgnore
    public boolean isSuperUser() {
        return this.type == UserType.Super;
    }
}
