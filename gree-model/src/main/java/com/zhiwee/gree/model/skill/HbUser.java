package com.zhiwee.gree.model.skill;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import xyz.icrab.common.util.jackson.CustomJsonDateDeserializer;

import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;
@Table(name = "t_fy_hb_user")
public class HbUser {
    @Id
    private String id;

    private String hbNumber;

    private String userId;

    private String nickName;

    private  String phoneNum;

    @JsonDeserialize(using = CustomJsonDateDeserializer.class)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date getTime;


    @JsonDeserialize(using = CustomJsonDateDeserializer.class)
    @JsonFormat(pattern = "yyyy-MM-dd ", timezone = "GMT+8")
    private  Date startTime;

    @JsonDeserialize(using = CustomJsonDateDeserializer.class)
    @JsonFormat(pattern = "yyyy-MM-dd ", timezone = "GMT+8")
    private   Date endTime;



    private Double price;

    private   String distributorName;

    private   String distributorNickName;

    private Integer isUsed;




    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHbNumber() {
        return hbNumber;
    }

    public void setHbNumber(String hbNumber) {
        this.hbNumber = hbNumber;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    public Date getGetTime() {
        return getTime;
    }

    public void setGetTime(Date getTime) {
        this.getTime = getTime;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getDistributorName() {
        return distributorName;
    }

    public void setDistributorName(String distributorName) {
        this.distributorName = distributorName;
    }

    public String getDistributorNickName() {
        return distributorNickName;
    }

    public void setDistributorNickName(String distributorNickName) {
        this.distributorNickName = distributorNickName;
    }

    public Integer getIsUsed() {
        return isUsed;
    }

    public void setIsUsed(Integer isUsed) {
        this.isUsed = isUsed;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }
}