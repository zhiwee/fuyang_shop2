package com.zhiwee.gree.model.GoodsInfo.GoodVo;

import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;


/**
 * @author tzj
 * @description  内购信息导出
 * @date 2019/9/24 21:28
 * @return
*/
public class PeopleExr implements Serializable {
    @Excel(name = "顾客姓名")
    private  String  customer;
    @Excel(name = "顾客电话")
    private  String  phone;
    @Excel(name = "顾客密码")
    private  String  passWord;
    @Excel(name = "参加活动")
    private  String  activityName;


    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }
}
