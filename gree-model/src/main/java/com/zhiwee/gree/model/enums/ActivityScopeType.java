package com.zhiwee.gree.model.enums;

import xyz.icrab.common.model.Represent;

/**
 * 活动范围类型
 * @author Knight
 * @since 2018/7/6 13:27
 */
public enum ActivityScopeType implements Represent<Integer> {

    /**
     * 所有经销商/店面均可参与活动
     */
    All(1, "全部经销商/门店"),

    /**
     * 传递性，指定的经销商及其下级均可参与活动
     */
    Transitivity(2, "指定经销商/门店及其子级"),

    /**
     * 特定的，只有显示指定的经销商才可以参加活动
     */
    Specified(3, "指定经销商/门店");

    private final int value;

    private final String message;

    ActivityScopeType(int value, String message) {
        this.value = value;
        this.message = message;
    }

    @Override
    public Integer value() {
        return this.value;
    }


    @Override
    public String message() {
        return this.message;
    }
}
