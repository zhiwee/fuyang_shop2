package com.zhiwee.gree.model.ShopUser;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import xyz.icrab.common.util.jackson.CustomJsonDateDeserializer;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Date;

/**
 * @author sun on 2019/5/8
 */
@Table(name="s_shop_user")
public class ShopUser implements Serializable {

    @Id
    private String id ;
    private  String  username;
    //微信openId或者其他的登录标识
    private String openId;
    private String password;
    @JsonDeserialize(
            using = CustomJsonDateDeserializer.class
    )
    private Date createTime;

    @JsonDeserialize(
            using = CustomJsonDateDeserializer.class
    )
    private Date  updateTime;

    @JsonDeserialize(
            using = CustomJsonDateDeserializer.class
    )
    private Date  checkTime;

    private String phone;

    private Integer state;

     // 1 男 2 女
    private Integer gender;

    //昵称
    private String nickname;

    //头像的url地址
    private String headUrl;

    //积分
    private Double integral;


    //注册会员等级id
    private Integer grade;

    @Transient
    private  String gradeName;


    //累计金额
    private Double amount;

    //账户余额
    private Double balance;


    // 1 商城正常的用户但信息未完善  2 商城信息完善的用户
    private Integer type;

    //分销提成
    private Double activityCost;

    //城市编码
    private String cityCode;


    //城市名
    private String cityName;

    // 1  申请门店推销员  2 申请公司推销员 3 申请格力工厂推销员 4 未申请
    private Integer applyRetail;

    //部门，或者 工厂
    private String partementId;

    //部门或者工厂名称
    @Transient
    private  String partementName;
    //门店id
    private String dealerId;

    //门店名称
    @Transient
    private  String dealerName;
    //邮箱
    private String email;

   //验证码
    @Transient
    private  String captcha;


    @Transient
    private  String confirmPassword;

    @Transient
    private  String oldPassword;



  //  经销商等级
    private Integer distributorGrade;

    @Transient
    private  String distributorGradeName;

// 1 用户自己注册  不是经销商类型  2 后台系统人员添加是经销商类型用户
    private Integer registerType;

//经销商等级修改累计金额
    private Double distributorAmount;

    // 1 皖南 2 皖北
    private Integer wan;

    //是否分销 0不是 1 是
    private Integer isRetail;

    public Integer getIsRetail() {
        return isRetail;
    }

    public void setIsRetail(Integer isRetail) {
        this.isRetail = isRetail;
    }

    public Integer getWan() {
        return wan;
    }

    public void setWan(Integer wan) {
        this.wan = wan;
    }

    public String getDistributorGradeName() {
        return distributorGradeName;
    }

    public void setDistributorGradeName(String distributorGradeName) {
        this.distributorGradeName = distributorGradeName;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public Integer getDistributorGrade() {
        return distributorGrade;
    }

    public void setDistributorGrade(Integer distributorGrade) {
        this.distributorGrade = distributorGrade;
    }

    public Integer getRegisterType() {
        return registerType;
    }

    public void setRegisterType(Integer registerType) {
        this.registerType = registerType;
    }

    public Double getDistributorAmount() {
        return distributorAmount;
    }

    public void setDistributorAmount(Double distributorAmount) {
        this.distributorAmount = distributorAmount;
    }

    public Integer getApplyRetail() {
        return applyRetail;
    }

    public void setApplyRetail(Integer applyRetail) {
        this.applyRetail = applyRetail;
    }

    public String getPartementName() {
        return partementName;
    }

    public void setPartementName(String partementName) {
        this.partementName = partementName;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Double getActivityCost() {
        return activityCost;
    }

    public void setActivityCost(Double activityCost) {
        this.activityCost = activityCost;
    }

    public Date getCheckTime() {
        return checkTime;
    }

    public void setCheckTime(Date checkTime) {
        this.checkTime = checkTime;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getPartementId() {
        return partementId;
    }

    public void setPartementId(String partementId) {
        this.partementId = partementId;
    }

    public String getDealerId() {
        return dealerId;
    }

    public Double getIntegral() {
        return integral;
    }

    public void setIntegral(Double integral) {
        this.integral = integral;
    }

    public void setDealerId(String dealerId) {
        this.dealerId = dealerId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getCaptcha() {
        return captcha;
    }

    public void setCaptcha(String captcha) {
        this.captcha = captcha;
    }

    public Double getAmount() {
        return amount;
    }

    public String getGradeName() {
        return gradeName;
    }

    public void setGradeName(String gradeName) {
        this.gradeName = gradeName;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }


    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public String getHeadUrl() {
        return headUrl;
    }

    public void setHeadUrl(String headUrl) {
        this.headUrl = headUrl;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    @Override
    public String toString() {
        return "ShopUser{" +
                "id='" + id + '\'' +
                ", username='" + username + '\'' +
                ", openId='" + openId + '\'' +
                ", password='" + password + '\'' +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                ", phone='" + phone + '\'' +
                ", state=" + state +
                ", gender=" + gender +
                ", nickname='" + nickname + '\'' +
                ", headUrl='" + headUrl + '\'' +
                ", integral=" + integral +
                ", grade=" + grade +
                ", gradeName='" + gradeName + '\'' +
                ", amount=" + amount +
                ", balance=" + balance +
                ", type=" + type +
                ", activityCost=" + activityCost +
                ", cityCode='" + cityCode + '\'' +
                ", cityName='" + cityName + '\'' +
                ", applyRetail=" + applyRetail +
                ", partementId='" + partementId + '\'' +
                ", partementName='" + partementName + '\'' +
                ", dealerId='" + dealerId + '\'' +
                ", dealerName='" + dealerName + '\'' +
                ", email='" + email + '\'' +
                ", captcha='" + captcha + '\'' +
                ", confirmPassword='" + confirmPassword + '\'' +
                ", oldPassword='" + oldPassword + '\'' +
                ", distributorGrade=" + distributorGrade +
                ", distributorGradeName='" + distributorGradeName + '\'' +
                ", registerType=" + registerType +
                ", distributorAmount=" + distributorAmount +
                ", wan=" + wan +
                '}';
    }
}
