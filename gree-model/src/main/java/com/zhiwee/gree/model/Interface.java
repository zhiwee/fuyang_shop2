package com.zhiwee.gree.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import xyz.icrab.common.model.enums.YesOrNo;

/**
 * @author gll
 * @since 2018/4/21 22:03
 */
@Table(name = "s_sys_interface")
public class Interface implements Serializable {

    /**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Id
    private String id;

    /**
     * 接口名称
     */
	@Column(name="name_")
    private String name;

    /**
     * 接口地址
     */
    private String url;

    /**
     * 接口模块ID
     */
    private String moduleid;

    /**
     * 接口模块名称
     */
    @Transient
    private String modulename;

    /**
     * 是否建权 1是2否
     */
    private YesOrNo checkauth;

    
	public String getModulename() {
		return modulename;
	}

	public void setModulename(String modulename) {
		this.modulename = modulename;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getModuleid() {
		return moduleid;
	}

	public void setModuleid(String moduleid) {
		this.moduleid = moduleid;
	}

	public YesOrNo getCheckauth() {
		return checkauth;
	}

	public void setCheckauth(YesOrNo checkauth) {
		this.checkauth = checkauth;
	}

	

}
