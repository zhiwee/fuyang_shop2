package com.zhiwee.gree.model.OrderInfo;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import xyz.icrab.common.util.jackson.CustomJsonDateDeserializer;
import javax.persistence.Transient;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * @author sun on 2019/5/11
 */
@Table(name="s_order_item")
public class OrderItem  implements Serializable {
    @Id
    private String id;

    private String orderId;

    private String goodsId;

    private String goodsName;

    private Integer  buyBun;


    //单价
    private Double unitPrice;

    //实际金额
    private Double realAmount;

    //调价金额
    private Double subtractPrice;

    private  Double amount;

    private Double activityCost;

    private Double promotionCosts;

    @JsonDeserialize(
            using = CustomJsonDateDeserializer.class
    )
    private Date updateTime;

    private String updateMan;

    private String createMan;

    @JsonDeserialize(
            using = CustomJsonDateDeserializer.class
    )
    private Date createTime;

    private  String specId;

    private  String couponId;

    private  String couponSeqno;

    private Double couponMoney;

    private  String categoryId;

    private Integer  isRetail;

    @Transient
    private  String picture;


    @Transient
    private  String distributorName;


    @JsonDeserialize(
            using = CustomJsonDateDeserializer.class
    )
    @Transient
    private Date deliveryTime;


    public Date getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(Date deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public String getDistributorName() {
        return distributorName;
    }

    public void setDistributorName(String distributorName) {
        this.distributorName = distributorName;
    }


    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Integer getIsRetail() {
        return isRetail;
    }

    public void setIsRetail(Integer isRetail) {
        this.isRetail = isRetail;
    }



    public String getCategoryId() {
        return categoryId;
    }
    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCouponId() {
        return couponId;
    }

    public void setCouponId(String couponId) {
        this.couponId = couponId;
    }

    public String getCouponSeqno() {
        return couponSeqno;
    }

    public void setCouponSeqno(String couponSeqno) {
        this.couponSeqno = couponSeqno;
    }

    public Double getCouponMoney() {
        return couponMoney;
    }

    public void setCouponMoney(Double couponMoney) {
        this.couponMoney = couponMoney;
    }

    public Double getRealAmount() {
        return realAmount;
    }

    public void setRealAmount(Double realAmount) {
        this.realAmount = realAmount;
    }

    public Double getSubtractPrice() {
        return subtractPrice;
    }

    public void setSubtractPrice(Double subtractPrice) {
        this.subtractPrice = subtractPrice;
    }

    public String getSpecId() {
        return specId;
    }

    public void setSpecId(String specId) {
        this.specId = specId;
    }

    public Double getActivityCost() {
        return activityCost;
    }

    public void setActivityCost(Double activityCost) {
        this.activityCost = activityCost;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public Integer getBuyBun() {
        return buyBun;
    }

    public void setBuyBun(Integer buyBun) {
        this.buyBun = buyBun;
    }

    public Double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getPromotionCosts() {
        return promotionCosts;
    }

    public void setPromotionCosts(Double promotionCosts) {
        this.promotionCosts = promotionCosts;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getUpdateMan() {
        return updateMan;
    }

    public void setUpdateMan(String updateMan) {
        this.updateMan = updateMan;
    }

    public String getCreateMan() {
        return createMan;
    }

    public void setCreateMan(String createMan) {
        this.createMan = createMan;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

}
