package com.zhiwee.gree.model;

import xyz.icrab.common.model.enums.YesOrNo;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * @author gll
 * @since 2018/4/21 22:03
 */
@Table(name = "s_sys_role")
public class Role implements Serializable {

    /**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Id
    private String id;

    /**
     * 角色名称
     */
	@Column(name="name_")
    private String name;

    /**
     * 是否启用 1是2否
     */
    private YesOrNo state;

    /**
     * 创建时间
     */
    private Date createtime;

    /**
     * 更新人
     */
    private String createman;

    /**
     * 更新时间
     */
    private Date updatetime;

    /**
     * 更新人
     */
    private String updateman;

    /**
     * 描述
     */
    private String remark;


	/**
	 * 编码
	 */
	private String code;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

    public YesOrNo getState() {
        return state;
    }

    public void setState(YesOrNo state) {
        this.state = state;
    }

    public Date getCreatetime() {
		return createtime;
	}
	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}
	public String getCreateman() {
		return createman;
	}
	public void setCreateman(String createman) {
		this.createman = createman;
	}
	public Date getUpdatetime() {
		return updatetime;
	}
	public void setUpdatetime(Date updatetime) {
		this.updatetime = updatetime;
	}
	public String getUpdateman() {
		return updateman;
	}
	public void setUpdateman(String updateman) {
		this.updateman = updateman;
	}

}
