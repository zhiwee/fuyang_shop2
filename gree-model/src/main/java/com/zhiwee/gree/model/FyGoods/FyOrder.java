package com.zhiwee.gree.model.FyGoods;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.jeecgframework.poi.excel.annotation.Excel;
import xyz.icrab.common.util.jackson.CustomJsonDateDeserializer;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Table(name = "t_fyorder")
public class FyOrder implements Serializable{

    @Id

    private String id;
    private String fyCode;

    @Excel(name = "订单号")
    private String orderNum;

    //分享者ID
    private String shareId;
    private String giftCardId;

    @JsonDeserialize(using = CustomJsonDateDeserializer.class)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
   // @DateTimeFormat(pattern = "yyyy-MM-dd ")
    @Excel(name = "下单时间",format="yyyy-MM-dd HH:mm:ss")
    private Date orderTime;
    @JsonDeserialize(using = CustomJsonDateDeserializer.class)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    // @DateTimeFormat(pattern = "yyyy-MM-dd ")
    @Excel(name = "支付时间",format="yyyy-MM-dd HH:mm:ss")
    private Date payTime;
    @JsonDeserialize(using = CustomJsonDateDeserializer.class)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date refoundTime;
    @Excel(name = "下单用户", needMerge = true)
    private  String userName;

    private String userId;

    @Excel(name = "用户手机号")
    private String phoneNum;

    @Excel(name = "订单总金额")
    private BigDecimal allPrice;

    private   String goodsBody;
    @Excel(name = "折扣")
    private BigDecimal discount;
        //折扣方式
    private String discountId;

    @Excel(name = "实际支付金额")
    private BigDecimal payPrice;

    private   String distributorName;

    private   String distributorNickName;
    private   String couponsId;

    @JsonDeserialize(using = CustomJsonDateDeserializer.class)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date claimTime;//认领时间
    @Excel(name = "状态", replace={"未支付_1","已支付_2","部分退款_3","全额退款_4"})
    private Integer state;

    //是否删除 1表示未删除  2表示删除
    private  Integer isdelete;
    private  Integer payType;
    //是否申请退款  1  表示有   2  表示没有
    @Excel(name = "是否申请退款",replace={"否_1","是_2"})
    private  Integer isrefound;
    @Excel(name = "是否发货",replace={"否_1","是_2"})
    private  Integer deliver ;
    @Excel(name = "购买渠道",replace={"普通下单_1","直播购买_2","秒杀订单_3"})
    private  Integer type ;
    private  Integer jiadian ;
    //是否已经退款   1  表示  是   2  表示否
   // private  Integer  refoundState;
    //退款金额
    @Excel(name = "退款金额")
    private  BigDecimal  refoundPrice;
    private  BigDecimal  activityCost;
   // private  Integer isAllrefound;

    private  Integer ishasRefound;


    //订单详细地址
    @Transient
    @Excel(name = "省")
    private String  province;
    @Transient
    @Excel(name = "市")
    private String  city;
    @Transient
    @Excel(name = "区")
    private String  area;
    @Transient
    @Excel(name = "收货地址")
    private String  detailAddress;



    @Transient
    private  String     goodsNameStr;
    @Transient
    private  String      singlePriceStr;

    public Integer getPayType() {
        return payType;
    }

    public void setPayType(Integer payType) {
        this.payType = payType;
    }

    @Transient
    private  String       countStr;

    @Transient
    private  String       payPriceStr;

    //退款订单商品详情
    @Transient
    private String   refGoodsNameDetail;


    //退款金额商品详情
    @Transient
    private  String  refPriceDetail;

    public Date getPayTime() {
        return payTime;
    }

    public void setPayTime(Date payTime) {
        this.payTime = payTime;
    }

    public String getDiscountId() {
        return discountId;
    }

    public void setDiscountId(String discountId) {
        this.discountId = discountId;
    }

    public String getFyCode() {
        return fyCode;
    }

    public void setFyCode(String fyCode) {
        this.fyCode = fyCode;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrderNum() {
        return orderNum;
    }

    public String getCouponsId() {
        return couponsId;
    }

    public void setCouponsId(String couponsId) {
        this.couponsId = couponsId;
    }

    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }

    public Date getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(Date orderTime) {
        this.orderTime = orderTime;
    }

    public Date getRefoundTime() {
        return refoundTime;
    }

    public void setRefoundTime(Date refoundTime) {
        this.refoundTime = refoundTime;
    }

    public String getUserName() {
        return userName;
    }

    public String getGiftCardId() {
        return giftCardId;
    }

    public void setGiftCardId(String giftCardId) {
        this.giftCardId = giftCardId;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public BigDecimal getActivityCost() {
        return activityCost;
    }

    public void setActivityCost(BigDecimal activityCost) {
        this.activityCost = activityCost;
    }

    public Integer getJiadian() {
        return jiadian;
    }

    public void setJiadian(Integer jiadian) {
        this.jiadian = jiadian;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    public BigDecimal getAllPrice() {
        return allPrice;
    }

    public void setAllPrice(BigDecimal allPrice) {
        this.allPrice = allPrice;
    }

    public String getGoodsBody() {
        return goodsBody;
    }

    public void setGoodsBody(String goodsBody) {
        this.goodsBody = goodsBody;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public BigDecimal getPayPrice() {
        return payPrice;
    }

    public void setPayPrice(BigDecimal payPrice) {
        this.payPrice = payPrice;
    }

    public String getDistributorName() {
        return distributorName;
    }

    public void setDistributorName(String distributorName) {
        this.distributorName = distributorName;
    }

    public String getDistributorNickName() {
        return distributorNickName;
    }

    public void setDistributorNickName(String distributorNickName) {
        this.distributorNickName = distributorNickName;
    }

    public Date getClaimTime() {
        return claimTime;
    }

    public String getShareId() {
        return shareId;
    }

    public void setShareId(String shareId) {
        this.shareId = shareId;
    }

    public void setClaimTime(Date claimTime) {
        this.claimTime = claimTime;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getIsdelete() {
        return isdelete;
    }

    public void setIsdelete(Integer isdelete) {
        this.isdelete = isdelete;
    }

    public Integer getIsrefound() {
        return isrefound;
    }

    public void setIsrefound(Integer isrefound) {
        this.isrefound = isrefound;
    }

    public BigDecimal getRefoundPrice() {
        return refoundPrice;
    }

    public void setRefoundPrice(BigDecimal refoundPrice) {
        this.refoundPrice = refoundPrice;
    }

    public Integer getIshasRefound() {
        return ishasRefound;
    }

    public void setIshasRefound(Integer ishasRefound) {
        this.ishasRefound = ishasRefound;
    }

    public String getGoodsNameStr() {
        return goodsNameStr;
    }

    public void setGoodsNameStr(String goodsNameStr) {
        this.goodsNameStr = goodsNameStr;
    }

    public String getSinglePriceStr() {
        return singlePriceStr;
    }

    public void setSinglePriceStr(String singlePriceStr) {
        this.singlePriceStr = singlePriceStr;
    }

    public String getCountStr() {
        return countStr;
    }

    public void setCountStr(String countStr) {
        this.countStr = countStr;
    }

    public String getPayPriceStr() {
        return payPriceStr;
    }

    public void setPayPriceStr(String payPriceStr) {
        this.payPriceStr = payPriceStr;
    }

    public String getRefGoodsNameDetail() {
        return refGoodsNameDetail;
    }

    public void setRefGoodsNameDetail(String refGoodsNameDetail) {
        this.refGoodsNameDetail = refGoodsNameDetail;
    }

    public String getRefPriceDetail() {
        return refPriceDetail;
    }

    public void setRefPriceDetail(String refPriceDetail) {
        this.refPriceDetail = refPriceDetail;
    }

    public String getDetailAddress() {
        return detailAddress;
    }

    public void setDetailAddress(String detailAddress) {
        this.detailAddress = detailAddress;
    }

    public Integer getType() {
        return type;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getDeliver() {
        return deliver;
    }

    public void setDeliver(Integer deliver) {
        this.deliver = deliver;
    }
/*@Override
    public int compareTo(FyOrder o) {

        Long orderNum1 =  Long.parseLong(this.getOrderNum());
        Long orderNum2 = Long.parseLong(o.getOrderNum());

        if(orderNum1>orderNum2){
            return 1;
        }else{
            return  -1;
        }

    }*/
}