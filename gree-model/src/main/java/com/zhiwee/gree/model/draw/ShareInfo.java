package com.zhiwee.gree.model.draw;

import xyz.icrab.common.model.BaseModel;

import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author DELL
 */
@Table(name = "l_shopuser_share")
public class ShareInfo extends BaseModel {

        @Id
        private String id;
        private String shopUserId;
        private Integer shareSum;
        private String acceptOpenId;
        private String lockKey;


    public String getLockKey() {
        return lockKey;
    }

    public void setLockKey(String lockKey) {
        this.lockKey = lockKey;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getShopUserId() {
        return shopUserId;
    }

    public void setShopUserId(String shopUserId) {
        this.shopUserId = shopUserId;
    }

    public Integer getShareSum() {
        return shareSum;
    }

    public String getAcceptOpenId() {
        return acceptOpenId;
    }

    public void setAcceptOpenId(String acceptOpenId) {
        this.acceptOpenId = acceptOpenId;
    }

    public void setShareSum(Integer shareSum) {
        this.shareSum = shareSum;
    }
}
