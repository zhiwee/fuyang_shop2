package com.zhiwee.gree.model.GoodsInfo.GoodVo;

import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecgframework.poi.excel.annotation.ExcelTarget;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by jick on 2019/8/29.
 */
@ExcelTarget("GoodsExo")
public class GoodsExo implements Serializable {

    @Excel(name = "商品id")
    private   String  id;
    @Excel(name = "商品名称")
    private  String  name;
    @Excel(name = "商品描述")
    private  String describes;
    @Excel(name = "商品编码")
    private  String  goodsNum;

    @Excel(name = "直播地址")
    private  String liveUrl;


    @Excel(name = "商品分类名称")
    private    String categoryName;
    @Excel(name = "商品品牌名称")
    private  String   brandName;
    @Excel(name = "关键词")
    private    String words;
    @Excel(name = "商品售价")
    private    BigDecimal basePrice;
    @Excel(name = "发货方式",replace={"线上发货_1","线下自提_2","用户自选_3"})
    private    Integer sendType;
    @Excel(name = "是否家电",replace={"否_1","是_2"})
    private Integer jiadian;
    @Excel(name = "是否推荐",replace={"是_1","否_2"})
    private   Integer isRecommend;
    @Excel(name = "是否新品",replace={"是_1","否_2"})
    private    Integer isNew;
    @Excel(name = "是否热卖",replace={"是_1","否_2"})
    private    Integer  isHot;
    @Excel(name = "商品活动价格")
    private BigDecimal activePrice;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public Integer getSendType() {
        return sendType;
    }

    public void setSendType(Integer sendType) {
        this.sendType = sendType;
    }

    public Integer getJiadian() {
        return jiadian;
    }

    public void setJiadian(Integer jiadian) {
        this.jiadian = jiadian;
    }

    public Integer getIsRecommend() {
        return isRecommend;
    }

    public void setIsRecommend(Integer isRecommend) {
        this.isRecommend = isRecommend;
    }

    public Integer getIsNew() {
        return isNew;
    }

    public void setIsNew(Integer isNew) {
        this.isNew = isNew;
    }

    public Integer getIsHot() {
        return isHot;
    }

    public void setIsHot(Integer isHot) {
        this.isHot = isHot;
    }

    public String getWords() {
        return words;
    }

    public void setWords(String words) {
        this.words = words;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescribes() {
        return describes;
    }

    public void setDescribes(String describes) {
        this.describes = describes;
    }

    public String getGoodsNum() {
        return goodsNum;
    }

    public void setGoodsNum(String goodsNum) {
        this.goodsNum = goodsNum;
    }

    public String getLiveUrl() {
        return liveUrl;
    }

    public void setLiveUrl(String liveUrl) {
        this.liveUrl = liveUrl;
    }


    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public BigDecimal getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(BigDecimal basePrice) {
        this.basePrice = basePrice;
    }

    public BigDecimal getActivePrice() {
        return activePrice;
    }

    public void setActivePrice(BigDecimal activePrice) {
        this.activePrice = activePrice;
    }

}
