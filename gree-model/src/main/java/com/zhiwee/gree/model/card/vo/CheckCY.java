package com.zhiwee.gree.model.card.vo;


public class CheckCY {
    private String id;
    private Double totalPrice;
    private String departmentCode;
    private String categoryCode;
    private Integer canUseMoney;
    private String msg;

    /**
     *
     *  0 请求成功   1 合格
     * @date 2019/12/18 13:00
    */
    private Integer state;
    private String couponId;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getCouponId() {
        return couponId;
    }

    public void setCouponId(String couponId) {
        this.couponId = couponId;
    }

    public String getDepartmentCode() {
        return departmentCode;
    }

    public void setDepartmentCode(String departmentCode) {
        this.departmentCode = departmentCode;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Integer getCanUseMoney() {
        return canUseMoney;
    }



    public void setCanUseMoney(Integer canUseMoney) {
        this.canUseMoney = canUseMoney;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getCategoryCode() {
        return categoryCode;
    }

    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }
}
