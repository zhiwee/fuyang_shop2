package com.zhiwee.gree.model.draw;

import xyz.icrab.common.model.BaseModel;

import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "l_shopuser_draw")
public class CanDraws extends BaseModel {

    @Id
    private String id;
        private String shopUserId;
        private Integer drawsSum;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getShopUserId() {
        return shopUserId;
    }

    public void setShopUserId(String shopUserId) {
        this.shopUserId = shopUserId;
    }

    public Integer getDrawsSum() {
        return drawsSum;
    }

    public void setDrawsSum(Integer drawsSum) {
        this.drawsSum = drawsSum;
    }
}
