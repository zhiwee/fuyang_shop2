package com.zhiwee.gree.model.GoodsInfo;


import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 商品信息实体
 * @version 1.0
 */
@Table(name="l_activity_goods")
public class LAGoodsInfo implements  Serializable{

    // ID
    @Id
    private String id;

    private String goodsId;

    private String activityId;

    private Integer stockNum;

    private BigDecimal secKillPrice;

    private Integer state;

    private Integer limitNum;

    @Transient
    private String name;
    private String killActivityId;
    private Date beginTime;
    private Date ebdTime;

    @Transient
    private  String  activeName;
    @Transient
    private   String goodsName;
    @Transient
    private String  categoryName;
    @Transient
    private BigDecimal shopPrice;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }

    public String getActivityId() {
        return activityId;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    public Integer getStockNum() {
        return stockNum;
    }

    public void setStockNum(Integer stockNum) {
        this.stockNum = stockNum;
    }

    public BigDecimal getSecKillPrice() {
        return secKillPrice;
    }

    public void setSecKillPrice(BigDecimal secKillPrice) {
        this.secKillPrice = secKillPrice;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getLimitNum() {
        return limitNum;
    }

    public void setLimitNum(Integer limitNum) {
        this.limitNum = limitNum;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKillActivityId() {
        return killActivityId;
    }

    public void setKillActivityId(String killActivityId) {
        this.killActivityId = killActivityId;
    }

    public Date getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(Date beginTime) {
        this.beginTime = beginTime;
    }

    public Date getEbdTime() {
        return ebdTime;
    }

    public void setEbdTime(Date ebdTime) {
        this.ebdTime = ebdTime;
    }

    public String getActiveName() {
        return activeName;
    }

    public void setActiveName(String activeName) {
        this.activeName = activeName;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public BigDecimal getShopPrice() {
        return shopPrice;
    }

    public void setShopPrice(BigDecimal shopPrice) {
        this.shopPrice = shopPrice;
    }
}
