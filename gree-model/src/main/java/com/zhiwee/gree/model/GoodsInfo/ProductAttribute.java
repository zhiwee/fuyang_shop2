package com.zhiwee.gree.model.GoodsInfo;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;

/**
 * Created by jick on 2019/8/28.
 */
//@Table(name = "s_product_attribute")
@Table(name = "s_goods_attribute")
public class ProductAttribute  implements Serializable {


    @Id
    private   String   id;

    private   String  mode;

    private   String  modeId;

    private  String   atrName;

    private  Integer    atrType;

    private  Integer search;

    private  Integer inputType;

    private String  optional;

    private String  number;

    @Transient
    private String itemName;

    @Transient
    private  String  specsName;

    @Transient
    private  String   specsItemName;

    @Transient
    private    String   name ;




    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }


    public String getModeId() {
        return modeId;
    }

    public void setModeId(String modeId) {
        this.modeId = modeId;
    }

    public String getAtrName() {
        return atrName;
    }

    public void setAtrName(String atrName) {
        this.atrName = atrName;
    }

    public Integer getAtrType() {
        return atrType;
    }

    public void setAtrType(Integer atrType) {
        this.atrType = atrType;
    }

    public Integer getSearch() {
        return search;
    }

    public void setSearch(Integer search) {
        this.search = search;
    }

    public Integer getInputType() {
        return inputType;
    }

    public void setInputType(Integer inputType) {
        this.inputType = inputType;
    }

    public String getOptional() {
        return optional;
    }

    public void setOptional(String optional) {
        this.optional = optional;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }


    public String getSpecsName() {
        return specsName;
    }

    public void setSpecsName(String specsName) {
        this.specsName = specsName;
    }

    public String getSpecsItemName() {
        return specsItemName;
    }

    public void setSpecsItemName(String specsItemName) {
        this.specsItemName = specsItemName;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
