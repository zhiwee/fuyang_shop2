package com.zhiwee.gree.model.ActivityTicketOrder;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import xyz.icrab.common.model.annotation.Like;
import xyz.icrab.common.util.jackson.CustomJsonDateDeserializer;

import javax.persistence.Table;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Transient;

@Table(name = "t_sys_ticket")
public class TicketInfo  implements Serializable {

    @Id
    private String id;

    private String seqno;

    private String code;

    private String qrimg;

    private String aSeqno;

    private String gSeqno;

    private TicketState ticketState;

    private Integer type;//劵的类型

    private String dealerId;

    private String dealerName;

    private String activityId;

    private String activityName;

    private String consumerId;

    private String consumerName;

    @JsonDeserialize(using = CustomJsonDateDeserializer.class)
    private Date outtime;

    @JsonDeserialize(using = CustomJsonDateDeserializer.class)
    private Date usetime;

    @JsonDeserialize(using = CustomJsonDateDeserializer.class)
    private Date createtime;

    @Like
    private  String salesman;

    private String consumerMobile;

    private String url;


    public  TicketInfo(){

    }

    public TicketInfo(TicketOrder order){
        this.type = order.getType();
        this.seqno = order.getTicketSeqno();
    }
    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public String getSalesman() {
        return salesman;
    }

    public void setSalesman(String salesman) {
        this.salesman = salesman;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
    public String getQrimg() {
        return qrimg;
    }

    public void setQrimg(String qrimg) {
        this.qrimg = qrimg;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public String getConsumerMobile() {
        return consumerMobile;
    }

    public void setConsumerMobile(String consumerMobile) {
        this.consumerMobile = consumerMobile;
    }

    public String getConsumerName() {
        return consumerName;
    }

    public void setConsumerName(String consumerName) {
        this.consumerName = consumerName;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public String getConsumerId() {
        return consumerId;
    }

    public void setConsumerId(String consumerId) {
        this.consumerId = consumerId;
    }

    public String getActivityId() {
        return activityId;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getOuttime() {
        return outtime;
    }

    public void setOuttime(Date outtime) {
        this.outtime = outtime;
    }

    public Date getUsetime() {
        return usetime;
    }

    public void setUsetime(Date usetime) {
        this.usetime = usetime;
    }

    public String getSeqno() {
        return seqno;
    }

    public void setSeqno(String seqno) {
        this.seqno = seqno;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public TicketState getTicketState() {
        return ticketState;
    }

    public void setTicketState(TicketState ticketState) {
        this.ticketState = ticketState;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getaSeqno() {
        return aSeqno;
    }

    public void setaSeqno(String aSeqno) {
        this.aSeqno = aSeqno;
    }

    public String getGSeqno() {
        return gSeqno;
    }

    public void setGSeqno(String GSeqno) {
        this.gSeqno = GSeqno;
    }

    public String getDealerId() {
        return dealerId;
    }

    public void setDealerId(String dealerId) {
        this.dealerId = dealerId;
    }


}
