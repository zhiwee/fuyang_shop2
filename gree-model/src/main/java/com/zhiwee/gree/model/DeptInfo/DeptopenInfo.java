package com.zhiwee.gree.model.DeptInfo;

import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by jick on 2019/8/23.
 */
@Table(name = "s_sys_deptopenInfo")
public class DeptopenInfo {

    @Id
    private Integer id;

    private String deptName;

    private Integer isopenShop;

    private Integer isopenCity;

    private Integer isopenName;

    private Integer  isopenTel;

    private Integer isopenInstall;

    private Integer isopenBranch;

    private Integer isopenFactory;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public Integer getIsopenShop() {
        return isopenShop;
    }

    public void setIsopenShop(Integer isopenShop) {
        this.isopenShop = isopenShop;
    }

    public Integer getIsopenCity() {
        return isopenCity;
    }

    public void setIsopenCity(Integer isopenCity) {
        this.isopenCity = isopenCity;
    }

    public Integer getIsopenName() {
        return isopenName;
    }

    public void setIsopenName(Integer isopenName) {
        this.isopenName = isopenName;
    }

    public Integer getIsopenTel() {
        return isopenTel;
    }

    public void setIsopenTel(Integer isopenTel) {
        this.isopenTel = isopenTel;
    }

    public Integer getIsopenInstall() {
        return isopenInstall;
    }

    public void setIsopenInstall(Integer isopenInstall) {
        this.isopenInstall = isopenInstall;
    }

    public Integer getIsopenBranch() {
        return isopenBranch;
    }

    public void setIsopenBranch(Integer isopenBranch) {
        this.isopenBranch = isopenBranch;
    }

    public Integer getIsopenFactory() {
        return isopenFactory;
    }

    public void setIsopenFactory(Integer isopenFactory) {
        this.isopenFactory = isopenFactory;
    }
}
