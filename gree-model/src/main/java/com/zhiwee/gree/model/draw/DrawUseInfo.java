package com.zhiwee.gree.model.draw;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import xyz.icrab.common.model.BaseModel;
import xyz.icrab.common.util.jackson.CustomJsonDateDeserializer;

import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Table(name = "s_drawuse_info")
public class DrawUseInfo  extends BaseModel {

    @Id
    private String id;

    //顾客信息
    private String customer;
    private String customerPhone;

    //奖池名称
    private String drawId;
    private String drawName;

    //奖项
    private Integer prize;
    private String drawsId;
    private String drawsName;

    // 1-未领奖、2-已领奖
    private Integer state;

    private String customerid;

    private String sysuserid;

    private String sysusername;

    public String getCustomerid() {
        return customerid;
    }

    public void setCustomerid(String customerid) {
        this.customerid = customerid;
    }

    public String getSysuserid() {
        return sysuserid;
    }

    public void setSysuserid(String sysuserid) {
        this.sysuserid = sysuserid;
    }

    public String getSysusername() {
        return sysusername;
    }

    public void setSysusername(String sysusername) {
        this.sysusername = sysusername;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public String getDrawId() {
        return drawId;
    }

    public void setDrawId(String drawId) {
        this.drawId = drawId;
    }

    public String getDrawName() {
        return drawName;
    }

    public void setDrawName(String drawName) {
        this.drawName = drawName;
    }

    public Integer getPrize() {
        return prize;
    }

    public void setPrize(Integer prize) {
        this.prize = prize;
    }

    public String getDrawsId() {
        return drawsId;
    }

    public void setDrawsId(String drawsId) {
        this.drawsId = drawsId;
    }

    public String getDrawsName() {
        return drawsName;
    }

    public void setDrawsName(String drawsName) {
        this.drawsName = drawsName;
    }

}
