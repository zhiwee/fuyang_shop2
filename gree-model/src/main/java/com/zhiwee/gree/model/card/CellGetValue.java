package com.zhiwee.gree.model.card;

import org.apache.poi.xssf.usermodel.XSSFCell;

/**
 * @author DELL
 */
public class CellGetValue {

    public static Integer YesOrNo(XSSFCell hssfCell){
        String state = String.valueOf(hssfCell.getStringCellValue());
        if("是".equals(state)){
            return 1;
        }else{
            return 2;
        }
    }
    public static String getValue(XSSFCell hssfCell) {
        if (hssfCell.getCellType() == hssfCell.CELL_TYPE_BOOLEAN) {
            // 返回布尔类型的值
            return String.valueOf(hssfCell.getBooleanCellValue());
        } else if (hssfCell.getCellType() == hssfCell.CELL_TYPE_NUMERIC) {
            // 返回数值类型的值
            return String.valueOf(hssfCell.getNumericCellValue());
        } else {
            // 返回字符串类型的值
            return String.valueOf(hssfCell.getStringCellValue());
        }
    }
}
