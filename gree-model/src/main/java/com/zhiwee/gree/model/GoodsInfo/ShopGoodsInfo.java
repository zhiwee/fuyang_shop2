package com.zhiwee.gree.model.GoodsInfo;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import xyz.icrab.common.model.BaseModel;
import xyz.icrab.common.util.jackson.CustomJsonDateDeserializer;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Date;

/**
 * 商品信息实体
 * @version 1.0
 */
@Table(name="s_goods_info")
public class ShopGoodsInfo extends BaseModel {
    // ID
    @Id
    private String id;

    private String name;
    // 品牌
    private String brandId;
    // 品牌名称
    @Transient
    private String brandName;
    // 规格型号
    private String specId;
    // 商品分类
    private String category;

    //商品分类名称
    @Transient
    private String categoryName;


    // 销售价格
    private Double price;


    // 市场价
    private Double marketPrice;


    // 基本计量单位
    private String unit;

    // 库存
    private Integer stockNum;

    // 是否允许使用优惠券
    private Integer useCoupon;

    //状态： 1：正常 2：下架
    private Integer state;


    //活动价
    private Double activityPrice;



    private String createMan;

    private String updateMan;


    //特价结束时间
    @JsonDeserialize(
            using = CustomJsonDateDeserializer.class
    )
    private Date specialEndTime;



    //商品父类Id
    @Transient
    private String categoryParent;

    //卖出去的数量
    private Integer sellNum;

//图片地址
   @Transient
    private String picture;

    //商品描述id
    private String detailId;

    //商品介绍
    private String description;

    //商品标题
    private String title;

    //商品描述内容
    @Transient
    private String detailName;

    //是否推荐 1 推荐 2 不是
    private Integer recommend;


    //是否是新品 1 是 2 不是
    private Integer news;

    //是否是热销 1 是 2 不是
    private Integer hotSell;

    //排序
    private  Integer sort;

    //购物车数量
    @Transient
    private Integer amount;

    //基础库商品id
    private String goodId;

    //分销价格
    private Double retailPrice;

    //是否分销 1 分销 2 不分销
    private Integer retail;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getSpecId() {
        return specId;
    }

    public void setSpecId(String specId) {
        this.specId = specId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(Double marketPrice) {
        this.marketPrice = marketPrice;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Integer getStockNum() {
        return stockNum;
    }

    public void setStockNum(Integer stockNum) {
        this.stockNum = stockNum;
    }

    public Integer getUseCoupon() {
        return useCoupon;
    }

    public void setUseCoupon(Integer useCoupon) {
        this.useCoupon = useCoupon;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Double getActivityPrice() {
        return activityPrice;
    }

    public void setActivityPrice(Double activityPrice) {
        this.activityPrice = activityPrice;
    }

    public String getCreateMan() {
        return createMan;
    }

    public void setCreateMan(String createMan) {
        this.createMan = createMan;
    }

    public String getUpdateMan() {
        return updateMan;
    }

    public void setUpdateMan(String updateMan) {
        this.updateMan = updateMan;
    }

    public Date getSpecialEndTime() {
        return specialEndTime;
    }

    public void setSpecialEndTime(Date specialEndTime) {
        this.specialEndTime = specialEndTime;
    }

    public String getCategoryParent() {
        return categoryParent;
    }

    public void setCategoryParent(String categoryParent) {
        this.categoryParent = categoryParent;
    }

    public Integer getSellNum() {
        return sellNum;
    }

    public void setSellNum(Integer sellNum) {
        this.sellNum = sellNum;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getDetailId() {
        return detailId;
    }

    public void setDetailId(String detailId) {
        this.detailId = detailId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetailName() {
        return detailName;
    }

    public void setDetailName(String detailName) {
        this.detailName = detailName;
    }

    public Integer getRecommend() {
        return recommend;
    }

    public void setRecommend(Integer recommend) {
        this.recommend = recommend;
    }

    public Integer getNews() {
        return news;
    }

    public void setNews(Integer news) {
        this.news = news;
    }

    public Integer getHotSell() {
        return hotSell;
    }

    public void setHotSell(Integer hotSell) {
        this.hotSell = hotSell;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getGoodId() {
        return goodId;
    }

    public void setGoodId(String goodId) {
        this.goodId = goodId;
    }

    public Double getRetailPrice() {
        return retailPrice;
    }

    public void setRetailPrice(Double retailPrice) {
        this.retailPrice = retailPrice;
    }

    public Integer getRetail() {
        return retail;
    }

    public void setRetail(Integer retail) {
        this.retail = retail;
    }
}
