package com.zhiwee.gree.model.ret;

/**
 * @author Knight
 * @since 2018/4/30 13:30
 */
public enum UserReturnMsg {

    EMPTY_USERNAME("用户名不能为空"),
    EMPTY_NICKNAME("用户姓名不能为空"),
    VALID_USERTYPE("非法的用户类型"),
    EMPTY_ROLE("角色不能为空"),
    EMPTY_PASSWORD("密码不能为空");

    private final String message;

    UserReturnMsg(String message) {
        this.message = message;
    }

    public String message() {
        return message;
    }
}
