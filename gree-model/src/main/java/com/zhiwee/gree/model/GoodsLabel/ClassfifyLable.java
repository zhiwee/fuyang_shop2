package com.zhiwee.gree.model.GoodsLabel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author sun on 2019/5/8
 */
public class ClassfifyLable implements Serializable {

    private List<Bp> bpList = new ArrayList<>();

    private List<Efficiency> efficiencyList = new ArrayList<>();

    private List<Gb> gbList = new ArrayList<>();

    private List<Ps> psList = new ArrayList<>();

    private List<Space> spaceList = new ArrayList<>();

    public List<Bp> getBpList() {
        return bpList;
    }

    public void setBpList(List<Bp> bpList) {
        this.bpList = bpList;
    }

    public List<Efficiency> getEfficiencyList() {
        return efficiencyList;
    }

    public void setEfficiencyList(List<Efficiency> efficiencyList) {
        this.efficiencyList = efficiencyList;
    }

    public List<Gb> getGbList() {
        return gbList;
    }

    public void setGbList(List<Gb> gbList) {
        this.gbList = gbList;
    }

    public List<Ps> getPsList() {
        return psList;
    }

    public void setPsList(List<Ps> psList) {
        this.psList = psList;
    }

    public List<Space> getSpaceList() {
        return spaceList;
    }

    public void setSpaceList(List<Space> spaceList) {
        this.spaceList = spaceList;
    }
}
