package com.zhiwee.gree.model.FyGoods;

import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "t_fygoodsdetailtext")
public class FyGoodsDetailText {

    @Id
    private String id;

    private String goodsId;

    private String detail;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}