package com.zhiwee.gree.model.Coupon;

import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;
import java.util.Date;

/**
 * @author sun on 2019/5/10
 */
public class CouponExport implements Serializable {

    @Excel(name = "专题名称")
    private  String  subjectName;

    @Excel(name = "优惠券号")
    private  String  couponSeqno;

    @Excel(name = "密码")
    private String  password;

    @Excel(name = "价值金额")
    private Double  couponMoney;

    @Excel(name = "使用标准")
    private Double  reachMoney;

    @Excel(name = "状态",replace={"启用_1","禁用_2"})
    private Integer state;

    @Excel(name = "是否使用",replace={"未使用_1","已使用_2","已失效_3"})
    private Integer couponState;

    @Excel(name = "使用价格类型",replace={"市场价_1","非市场价_2"})
    private Integer priceType;

    @Excel(name = "领取状态",replace={" 未领取_1","已领取_2"})
    private Integer sendState;

    @Excel(name = "类型",replace={"全品类优惠券_1","非全品类优惠券_2","指定某个商品的优惠券_3","全品类代金券_4","非全品类代金券_5","指定某个商品的代金券_6"},orderNum = "20")
    private Integer type;

    @Excel(name = "使用开始时间", needMerge = true,exportFormat = "yyyy-MM-dd HH:mm:ss")
    private Date startTime;

    @Excel(name = "使用结束时间", needMerge = true,exportFormat = "yyyy-MM-dd HH:mm:ss")
    private Date  endTime;


    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public Double getReachMoney() {
        return reachMoney;
    }

    public void setReachMoney(Double reachMoney) {
        this.reachMoney = reachMoney;
    }

    public Integer getPriceType() {
        return priceType;
    }

    public void setPriceType(Integer priceType) {
        this.priceType = priceType;
    }

    public Integer getSendState() {
        return sendState;
    }

    public void setSendState(Integer sendState) {
        this.sendState = sendState;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Integer getCouponState() {
        return couponState;
    }

    public void setCouponState(Integer couponState) {
        this.couponState = couponState;
    }

    public String getCouponSeqno() {
        return couponSeqno;
    }

    public void setCouponSeqno(String couponSeqno) {
        this.couponSeqno = couponSeqno;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }



    public Double getCouponMoney() {
        return couponMoney;
    }

    public void setCouponMoney(Double couponMoney) {
        this.couponMoney = couponMoney;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }


}
