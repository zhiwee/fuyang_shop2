package com.zhiwee.gree.model.Activity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import xyz.icrab.common.model.BaseModel;
import xyz.icrab.common.model.annotation.Like;
import xyz.icrab.common.model.type.OrderBy;
import xyz.icrab.common.util.jackson.CustomJsonDateDeserializer;

import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * 活动信息实体
 * @author sun
 * @since 2018/7/6 13:03
 */
@Table(name = "s_activity_info")
public class ActivityInfo extends BaseModel {
    @Id
    @OrderBy(OrderBy.Sort.DESC)
    private String id;

    @Like
    private String name;

    private String description;

    @JsonDeserialize(using = CustomJsonDateDeserializer.class)
    private Date begintime;

    @JsonDeserialize(using = CustomJsonDateDeserializer.class)
    private Date endtime;



    private Integer state;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getBegintime() {
        return begintime;
    }

    public void setBegintime(Date begintime) {
        this.begintime = begintime;
    }

    public Date getEndtime() {
        return endtime;
    }

    public void setEndtime(Date endtime) {
        this.endtime = endtime;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }
}
