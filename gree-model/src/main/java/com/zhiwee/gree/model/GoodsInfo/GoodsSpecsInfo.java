package com.zhiwee.gree.model.GoodsInfo;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.List;

@Table(name = "s_goods_specsinfo")
public class GoodsSpecsInfo {

    @Id
    private String id;

    private String name;

    private String remake;

    private Integer thisSort;

    @Transient
    private List<GoodsSpecsItem> itemList;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRemake() {
        return remake;
    }

    public void setRemake(String remake) {
        this.remake = remake;
    }

    public Integer getThisSort() {
        return thisSort;
    }

    public void setThisSort(Integer thisSort) {
        this.thisSort = thisSort;
    }

    public List<GoodsSpecsItem> getItemList() {
        return itemList;
    }

    public void setItemList(List<GoodsSpecsItem> itemList) {
        this.itemList = itemList;
    }
}
