package com.zhiwee.gree.model.Questionnaire;

import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecgframework.poi.excel.annotation.ExcelTarget;

import java.io.Serializable;

/**
 * TODO
 *
 * @author Chensong
 * @Company http://www.zhiwee.com
 * date 2019-12-12
 */
@ExcelTarget("AnswerInfoExrt")
public class AnswerInfoExrt implements Serializable {

    @Excel(name = "姓名", needMerge = true)
    private String nickname;

    @Excel(name = "您家在使用智能门锁吗？", replace={"是_1","否_2"})
    private String aquest;

    @Excel(name = "您有亲戚朋友家在使用智能门锁吗？", replace={"是_1","否_2"})
    private String bquest;

    @Excel(name = "您了解智能门锁吗？", replace={"了解_1","不了解_2","听说过_3"})
    private String cquest;

    @Excel(name = "你会去考虑使用智能门锁吗？", replace={"是_1","否_2"})
    private String dquest;

    @Excel(name = "如果你去选择购买智能门锁，你会考虑哪些因素？", replace={"产品外观_1","产品质量_2","产品价格_3","售后服务_4","安全保障_5"})
    private String equest;

    @Excel(name = "如果你购买智能门锁，你会选择什么价格段的智能门锁？", replace={"一千元一下_1","1000-1500_2","1500-2000_3","2000-3000_4"})
    private String fquest;

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getAquest() {
        return aquest;
    }

    public void setAquest(String aquest) {
        this.aquest = aquest;
    }

    public String getBquest() {
        return bquest;
    }

    public void setBquest(String bquest) {
        this.bquest = bquest;
    }

    public String getCquest() {
        return cquest;
    }

    public void setCquest(String cquest) {
        this.cquest = cquest;
    }

    public String getDquest() {
        return dquest;
    }

    public void setDquest(String dquest) {
        this.dquest = dquest;
    }

    public String getEquest() {
        return equest;
    }

    public void setEquest(String equest) {
        this.equest = equest;
    }

    public String getFquest() {
        return fquest;
    }

    public void setFquest(String fquest) {
        this.fquest = fquest;
    }
}
