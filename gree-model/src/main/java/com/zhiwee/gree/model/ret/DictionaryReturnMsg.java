package com.zhiwee.gree.model.ret;

public enum DictionaryReturnMsg {

	KEY_EMPTY("字典key不能为空"),
	KEY_MANY("同分组下字典key不能重复"),
	VALUE_EMPTY("字典value不能为空"),
	GROUP_EMPTY("分组名称不能为空"),
	GROUP_MANY("分组名称不能重复"),
	;

	private String message;

	DictionaryReturnMsg(String message) {
		this.message = message;
	}


	public String message() {
		return message;
	}

}
