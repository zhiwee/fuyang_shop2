package com.zhiwee.gree.model.HomePage;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * 商城Url库
 * 周广
 */
@Table(name = "s_page_url")
public class PageUrl {

    @Id
    private String id;

    private String name;

    private Integer type;

    private String url;

    private String modelId;

    private String modelName;

    private String addModelId;

    private String addModelName;

    private Integer state;

    private String remake;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    public String getAddModelId() {
        return addModelId;
    }

    public void setAddModelId(String addModelId) {
        this.addModelId = addModelId;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getRemake() {
        return remake;
    }

    public void setRemake(String remake) {
        this.remake = remake;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getAddModelName() {
        return addModelName;
    }

    public void setAddModelName(String addModelName) {
        this.addModelName = addModelName;
    }
}
