package com.zhiwee.gree.model.draw;

import java.util.List;

public class OrderDraws {

    private String id;

    private String seqno;

    private Integer prizeNum;

    private List<DrawsInfo> drawsList;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSeqno() {
        return seqno;
    }

    public void setSeqno(String seqno) {
        this.seqno = seqno;
    }

    public Integer getPrizeNum() {
        return prizeNum;
    }

    public void setPrizeNum(Integer prizeNum) {
        this.prizeNum = prizeNum;
    }

    public List<DrawsInfo> getDrawsList() {
        return drawsList;
    }

    public void setDrawsList(List<DrawsInfo> drawsList) {
        this.drawsList = drawsList;
    }
}
