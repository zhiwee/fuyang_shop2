package com.zhiwee.gree.model.OnlineActivity.vo;

import com.zhiwee.gree.model.HomePage.PageV0.PageChannelGoodsVO;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class OnlineActivityInfoVo  implements Serializable {

    private String id;

    private String name;

    private Date beginTime;

    private Date endTime;

    private Integer canCoupon;

    private  String  activityId;

    private Integer activityType;

    private BigDecimal secKillPrice;

    private Integer stockNum;

    private   String  listId;

    private  String  listName;

    private  String describes;

    private BigDecimal shopPrice;

    private   BigDecimal marketPrice;

    private  String goodCover;

    private  Integer  isSkill;



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(Date beginTime) {
        this.beginTime = beginTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Integer getCanCoupon() {
        return canCoupon;
    }

    public void setCanCoupon(Integer canCoupon) {
        this.canCoupon = canCoupon;
    }

    public String getActivityId() {
        return activityId;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    public Integer getActivityType() {
        return activityType;
    }

    public void setActivityType(Integer activityType) {
        this.activityType = activityType;
    }

    public BigDecimal getSecKillPrice() {
        return secKillPrice;
    }

    public void setSecKillPrice(BigDecimal secKillPrice) {
        this.secKillPrice = secKillPrice;
    }

    public Integer getStockNum() {
        return stockNum;
    }

    public void setStockNum(Integer stockNum) {
        this.stockNum = stockNum;
    }

    public String getListId() {
        return listId;
    }

    public void setListId(String listId) {
        this.listId = listId;
    }

    public String getListName() {
        return listName;
    }

    public void setListName(String listName) {
        this.listName = listName;
    }

    public String getDescribes() {
        return describes;
    }

    public void setDescribes(String describes) {
        this.describes = describes;
    }

    public BigDecimal getShopPrice() {
        return shopPrice;
    }

    public void setShopPrice(BigDecimal shopPrice) {
        this.shopPrice = shopPrice;
    }

    public BigDecimal getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(BigDecimal marketPrice) {
        this.marketPrice = marketPrice;
    }

    public String getGoodCover() {
        return goodCover;
    }

    public void setGoodCover(String goodCover) {
        this.goodCover = goodCover;
    }

    public Integer getIsSkill() {
        return isSkill;
    }

    public void setIsSkill(Integer isSkill) {
        this.isSkill = isSkill;
    }

}
