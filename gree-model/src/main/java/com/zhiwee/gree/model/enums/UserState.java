package com.zhiwee.gree.model.enums;

import xyz.icrab.common.model.Valueable;

/**
 * @author Knight
 * @since 2018/4/11 17:33
 */
public enum UserState implements Valueable<Integer> {

    ENABLE;

    @Override
    public Integer value() {
        return null;
    }
}
