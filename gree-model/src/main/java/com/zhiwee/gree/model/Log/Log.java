package com.zhiwee.gree.model.Log;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.StringUtils;

import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;
import java.util.Map;

/**
 * @author sun on 2019/3/28
 */
@Table(name="s_sys_log")
public class Log implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    private String logId;           //日志主键
    private String type;            //日志类型
    private String title;           //日志标题
    private String remoteAddr;          //请求地址
    private String requestUri;          //URI
    private String method;          //请求方式
    private String params;          //提交参数
    private String exception;           //异常
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date operateDate;           //开始时间
    private String timeout;         //结束时间
    private String userId;          //系统用户id
    private String nickname;          //账号

    private Integer state;          //1 商城后台日志  2 商城前台记录

    private String shopUserId;          //商城用户的id

    private String shouUserName;          //商城用户的账号

    private Integer requestMethod;          // 1 商城前台登录接口 2 不是商城前台登录接口


    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getShopUserId() {
        return shopUserId;
    }

    public void setShopUserId(String shopUserId) {
        this.shopUserId = shopUserId;
    }

    public String getShouUserName() {
        return shouUserName;
    }

    public void setShouUserName(String shouUserName) {
        this.shouUserName = shouUserName;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getRequestMethod() {
        return requestMethod;
    }

    public void setRequestMethod(Integer requestMethod) {
        this.requestMethod = requestMethod;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getLogId() {
        return StringUtils.isBlank(logId) ? logId : logId.trim();
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }


    public String getType() {
        return StringUtils.isBlank(type) ? type : type.trim();
    }

    public void setType(String type) {
        this.type = type;
    }


    public String getTitle() {
        return StringUtils.isBlank(title) ? title : title.trim();
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public String getRemoteAddr() {
        return StringUtils.isBlank(remoteAddr) ? remoteAddr : remoteAddr.trim();
    }

    public void setRemoteAddr(String remoteAddr) {
        this.remoteAddr = remoteAddr;
    }


    public String getRequestUri() {
        return StringUtils.isBlank(requestUri) ? requestUri : requestUri.trim();
    }

    public void setRequestUri(String requestUri) {
        this.requestUri = requestUri;
    }


    public String getMethod() {
        return StringUtils.isBlank(method) ? method : method.trim();
    }

    public void setMethod(String method) {
        this.method = method;
    }


    public String getParams() {
        return StringUtils.isBlank(params) ? params : params.trim();
    }

    public void setParams(String params) {
        this.params = params;
    }



    public String getException() {
        return StringUtils.isBlank(exception) ? exception : exception.trim();
    }

    public void setException(String exception) {
        this.exception = exception;
    }


    public Date getOperateDate() {
        return operateDate;
    }

    public void setOperateDate(Date operateDate) {
        this.operateDate = operateDate;
    }


    public String getTimeout() {
        return StringUtils.isBlank(timeout) ? timeout : timeout.trim();
    }

    public void setTimeout(String timeout) {
        this.timeout = timeout;
    }


    public String getUserId() {
        return StringUtils.isBlank(userId) ? userId : userId.trim();
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
