package com.zhiwee.gree.model.GoodsInfo.GoodVo;
import com.zhiwee.gree.model.GoodsInfo.GoodsCategoryAdImg;
import java.util.ArrayList;
import java.util.List;

public class GategoryVo {

    private String id;

    private String name;

    private Integer type;

    private String imgTopUrl;

    private String parentsId;

    private String imgBomUrl;

    private List<GategoryAdimgVO>  adImgs= new ArrayList<>();

    private List<GategoryVo> children = new ArrayList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImgTopUrl() {
        return imgTopUrl;
    }

    public void setImgTopUrl(String imgTopUrl) {
        this.imgTopUrl = imgTopUrl;
    }

    public String getImgBomUrl() {
        return imgBomUrl;
    }

    public void setImgBomUrl(String imgBomUrl) {
        this.imgBomUrl = imgBomUrl;
    }

    public List<GategoryAdimgVO> getAdImgs() {
        return adImgs;
    }

    public void setAdImgs(List<GategoryAdimgVO> adImgs) {
        this.adImgs = adImgs;
    }

    public List<GategoryVo> getChildren() {
        return children;
    }

    public void setChildren(List<GategoryVo> children) {
        this.children = children;
    }

    public String getParentsId() {
        return parentsId;
    }

    public void setParentsId(String parentsId) {
        this.parentsId = parentsId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}
