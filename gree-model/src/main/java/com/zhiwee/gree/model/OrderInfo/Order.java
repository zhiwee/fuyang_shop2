package com.zhiwee.gree.model.OrderInfo;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import xyz.icrab.common.util.jackson.CustomJsonDateDeserializer;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author sun on 2019/5/11
 */
@Table(name="s_order_info")
public class Order implements Serializable,Cloneable {

    @Id
    private String id;

    //支付状态 1 未支付 2 已支付 3 退单 4 退款
    private Integer orderState;

    private String orderNum;


    private String payId;


    // 1支付宝 2 微信 3银联 4 转账 5 余额
    private Integer payType;


    private Double totalPrice;


    @JsonDeserialize(
            using = CustomJsonDateDeserializer.class
    )
    private Date  payTime;


    @JsonDeserialize(
            using = CustomJsonDateDeserializer.class
    )
    private Date  deliveryTime;

    private Double promotionCosts;

    @JsonDeserialize(
            using = CustomJsonDateDeserializer.class
    )
    private Date  updateTime;

    private String  updateMan;

    private  String  createMan;
    @JsonDeserialize(
            using = CustomJsonDateDeserializer.class
    )
    private Date createTime;



    private String remark;

    private String  activityId;


    private Double realPrice;

    //是否使用优惠券 1 使用 2 未使用
    private Integer isCoupon;

    //1商城正常下单 2线下活动下单
    private Integer isOffline;

    //退单号
    private  String out_refund_no;

    // 支付生成的单号
    private  String transaction_id;

    //订单状态 1 待确认 2 已确认
    private Integer state;

    //申请退款状态  1 未申请退换货  2 申请退货中  3已允许退货，4拒绝退货 5 申请换货中 6允许换货 7 拒绝换货  8申请退款中  9 同意退款 10 拒绝退款
    private Integer refundState;
    //优惠券的id
    @Transient
    private String  couponId;

    @Transient
    private String  addressId;

    /**
     * 物流单号
     */

    private String  loggisticsNum;

    /**
     * 开单流水号
     */

    private String  drawNum;

    @Transient
    private List<OrderItem> orderItems = new ArrayList<>();

    @Transient
    private  OrderAddress orderAddress = new OrderAddress();


@Override
public Object clone()  {
    Object order = null;
    try {
        order = super.clone();
    } catch (CloneNotSupportedException e) {
        e.printStackTrace();
    }
    return order;
}


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getOrderState() {
        return orderState;
    }

    public void setOrderState(Integer orderState) {
        this.orderState = orderState;
    }

    public String getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }

    public String getPayId() {
        return payId;
    }

    public void setPayId(String payId) {
        this.payId = payId;
    }

    public Integer getPayType() {
        return payType;
    }

    public void setPayType(Integer payType) {
        this.payType = payType;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Date getPayTime() {
        return payTime;
    }

    public void setPayTime(Date payTime) {
        this.payTime = payTime;
    }

    public Date getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(Date deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public Double getPromotionCosts() {
        return promotionCosts;
    }

    public void setPromotionCosts(Double promotionCosts) {
        this.promotionCosts = promotionCosts;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getUpdateMan() {
        return updateMan;
    }

    public void setUpdateMan(String updateMan) {
        this.updateMan = updateMan;
    }

    public String getCreateMan() {
        return createMan;
    }

    public void setCreateMan(String createMan) {
        this.createMan = createMan;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getActivityId() {
        return activityId;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    public Double getRealPrice() {
        return realPrice;
    }

    public void setRealPrice(Double realPrice) {
        this.realPrice = realPrice;
    }

    public Integer getIsCoupon() {
        return isCoupon;
    }

    public void setIsCoupon(Integer isCoupon) {
        this.isCoupon = isCoupon;
    }

    public Integer getIsOffline() {
        return isOffline;
    }

    public void setIsOffline(Integer isOffline) {
        this.isOffline = isOffline;
    }

    public String getOut_refund_no() {
        return out_refund_no;
    }

    public void setOut_refund_no(String out_refund_no) {
        this.out_refund_no = out_refund_no;
    }

    public String getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(String transaction_id) {
        this.transaction_id = transaction_id;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getRefundState() {
        return refundState;
    }

    public void setRefundState(Integer refundState) {
        this.refundState = refundState;
    }

    public String getCouponId() {
        return couponId;
    }

    public void setCouponId(String couponId) {
        this.couponId = couponId;
    }

    public String getAddressId() {
        return addressId;
    }

    public void setAddressId(String addressId) {
        this.addressId = addressId;
    }

    public String getLoggisticsNum() {
        return loggisticsNum;
    }

    public void setLoggisticsNum(String loggisticsNum) {
        this.loggisticsNum = loggisticsNum;
    }

    public String getDrawNum() {
        return drawNum;
    }

    public void setDrawNum(String drawNum) {
        this.drawNum = drawNum;
    }

    public List<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    public OrderAddress getOrderAddress() {
        return orderAddress;
    }

    public void setOrderAddress(OrderAddress orderAddress) {
        this.orderAddress = orderAddress;
    }
}
