package com.zhiwee.gree.model.Questionnaire;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import xyz.icrab.common.model.BaseModel;
import xyz.icrab.common.util.jackson.CustomJsonDateDeserializer;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Date;

//问卷调查
@Table(name="s_answer_info")
public class AnswerInfo extends BaseModel {
    @Id
    private String id;
    private String userId;
    private String aquest;
    private String bquest;
    private String cquest;
    private String dquest;
    private String equest;
    private String fquest;
    private String gquest;
    private String hquest;
    private String iquest;
    @Transient
    private String userName;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAquest() {
        return aquest;
    }

    public void setAquest(String aquest) {
        this.aquest = aquest;
    }

    public String getBquest() {
        return bquest;
    }

    public void setBquest(String bquest) {
        this.bquest = bquest;
    }

    public String getCquest() {
        return cquest;
    }

    public void setCquest(String cquest) {
        this.cquest = cquest;
    }

    public String getDquest() {
        return dquest;
    }

    public void setDquest(String dquest) {
        this.dquest = dquest;
    }

    public String getEquest() {
        return equest;
    }

    public void setEquest(String equest) {
        this.equest = equest;
    }

    public String getFquest() {
        return fquest;
    }

    public void setFquest(String fquest) {
        this.fquest = fquest;
    }

    public String getGquest() {
        return gquest;
    }

    public void setGquest(String gquest) {
        this.gquest = gquest;
    }

    public String getHquest() {
        return hquest;
    }

    public void setHquest(String hquest) {
        this.hquest = hquest;
    }

    public String getIquest() {
        return iquest;
    }

    public void setIquest(String iquest) {
        this.iquest = iquest;
    }







}
