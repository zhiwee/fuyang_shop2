package com.zhiwee.gree.model.ret;

public enum AreaReturnMsg {
    AREA_NAME_EMPTY("地区名称不能为空"),
            ;

    private String message;

    AreaReturnMsg(String message) {
        this.message = message;
    }


    public String message() {
        return message;
    }
}
