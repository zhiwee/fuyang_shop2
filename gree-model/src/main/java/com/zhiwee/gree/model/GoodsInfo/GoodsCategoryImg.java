package com.zhiwee.gree.model.GoodsInfo;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * 分类图标  表  周广
 *
  */
@Table(name="s_goods_categoryimg")
public class GoodsCategoryImg {

    @Id
    private String id;

    private String categoryId;

    @Transient
    private String categoryName;

//    private String goodsId;
//
//    @Transient
//    private String goodsName;

    private String remake;

    private String imgTopUrl;

    private String imgBomUrl;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

//    public String getGoodsId() {
//        return goodsId;
//    }
//
//    public void setGoodsId(String goodsId) {
//        this.goodsId = goodsId;
//    }
//
//    public String getGoodsName() {
//        return goodsName;
//    }
//
//    public void setGoodsName(String goodsName) {
//        this.goodsName = goodsName;
//    }

    public String getRemake() {
        return remake;
    }

    public void setRemake(String remake) {
        this.remake = remake;
    }

    public String getImgTopUrl() {
        return imgTopUrl;
    }

    public void setImgTopUrl(String imgTopUrl) {
        this.imgTopUrl = imgTopUrl;
    }

    public String getImgBomUrl() {
        return imgBomUrl;
    }

    public void setImgBomUrl(String imgBomUrl) {
        this.imgBomUrl = imgBomUrl;
    }
}
