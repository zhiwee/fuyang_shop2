package com.zhiwee.gree.model.enums;

import xyz.icrab.common.model.Represent;

/**
 * 活动范围类型
 * @author Knight
 * @since 2018/7/6 13:27
 */
public enum OnlineActivityType implements Represent<Integer> {

    /**
     * 所有经销商/店面均可参与活动
     */
    secKill(1, "秒杀"),

    /**
     * 传递性，指定的经销商及其下级均可参与活动
     */
    NGH(2, "内购会"),

    /**
     * 特定的，只有显示指定的经销商才可以参加活动
     */
    GroupBuying(3, "团购");

    private final int value;

    private final String message;

    OnlineActivityType(int value, String message) {
        this.value = value;
        this.message = message;
    }

    @Override
    public Integer value() {
        return this.value;
    }


    @Override
    public String message() {
        return this.message;
    }
}
