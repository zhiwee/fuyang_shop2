package com.zhiwee.gree.model.Analysis;

import java.io.Serializable;

/**
 * @author sun on 2019/7/16
 */
public class ItemAnalysis  implements Serializable {

    private String areaName;

    private String businessType;

    private Integer itemAmount;

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public Integer getItemAmount() {
        return itemAmount;
    }

    public void setItemAmount(Integer itemAmount) {
        this.itemAmount = itemAmount;
    }
}
