package com.zhiwee.gree.model.UnionPay;

import java.io.Serializable;

/** 支付请求参数
 * @author sun on 2019/5/12
 */
public class UnionPay implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 商户网站唯一订单号
     */
    private String out_trade_no;
    /**
     * 付款金额
     */
    private String total_amount;

    /**
     * 退款金额
     */
    private Double refund_amount;


    private String refundPassword;


    public String getRefundPassword() {
        return refundPassword;
    }

    public void setRefundPassword(String refundPassword) {
        this.refundPassword = refundPassword;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Double getRefund_amount() {
        return refund_amount;
    }

    public void setRefund_amount(Double refund_amount) {
        this.refund_amount = refund_amount;
    }


    public String getOut_trade_no() {
        return out_trade_no;
    }

    public void setOut_trade_no(String out_trade_no) {
        this.out_trade_no = out_trade_no;
    }


    public String getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(String total_amount) {
        this.total_amount = total_amount;
    }

}
