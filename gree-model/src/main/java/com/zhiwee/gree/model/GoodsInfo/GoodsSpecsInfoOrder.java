package com.zhiwee.gree.model.GoodsInfo;

import javax.persistence.Id;
import javax.persistence.Table;

@Table(name ="s_goods_specsinfo_order")
public class GoodsSpecsInfoOrder {

    @Id
    private String id;

    private String goodsId;

    private String specsInfoId;

    private String remake;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }

    public String getSpecsInfoId() {
        return specsInfoId;
    }

    public void setSpecsInfoId(String specsInfoId) {
        this.specsInfoId = specsInfoId;
    }

    public String getRemake() {
        return remake;
    }

    public void setRemake(String remake) {
        this.remake = remake;
    }
}
