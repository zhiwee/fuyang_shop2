package com.zhiwee.gree.model.OrderInfo;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import xyz.icrab.common.util.jackson.CustomJsonDateDeserializer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author sun on 2019/6/28
 */
public class OrderDetail implements Serializable {

    private String orderId;


    private String orderNum;

    @JsonDeserialize(
            using = CustomJsonDateDeserializer.class
    )
    private Date orderTime;



    private Integer orderState;

    @JsonDeserialize(
            using = CustomJsonDateDeserializer.class
    )
    private Date payTime;

//订单总额
    private Double totalPrice;

    //实际付款金额
    private  Double realPrice;

    //折扣金额
    private  Double discountPrice;


    //物流单号
    private  String loggisticsNum;

    private List<OrderItem> orderItemList = new ArrayList<>();


    private OrderAddress orderAddress = new OrderAddress();

public OrderDetail(){

}

    public OrderDetail(Order order){
    this.orderId = order.getId();
      this.orderNum = order.getOrderNum();
      this.orderTime = order.getCreateTime();
      this.payTime = order.getPayTime();
       this.totalPrice = order.getTotalPrice();
       this.realPrice = order.getRealPrice();
        this.orderState = order.getOrderState();
        this.loggisticsNum = order.getLoggisticsNum();


    }


    public String getLoggisticsNum() {
        return loggisticsNum;
    }

    public void setLoggisticsNum(String loggisticsNum) {
        this.loggisticsNum = loggisticsNum;
    }

    public Integer getOrderState() {
        return orderState;
    }

    public void setOrderState(Integer orderState) {
        this.orderState = orderState;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Double getRealPrice() {
        return realPrice;
    }

    public void setRealPrice(Double realPrice) {
        this.realPrice = realPrice;
    }

    public Double getDiscountPrice() {
        return discountPrice;
    }

    public void setDiscountPrice(Double discountPrice) {
        this.discountPrice = discountPrice;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }

    public Date getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(Date orderTime) {
        this.orderTime = orderTime;
    }

    public Date getPayTime() {
        return payTime;
    }

    public void setPayTime(Date payTime) {
        this.payTime = payTime;
    }

    public List<OrderItem> getOrderItemList() {
        return orderItemList;
    }

    public void setOrderItemList(List<OrderItem> orderItemList) {
        this.orderItemList = orderItemList;
    }

    public OrderAddress getOrderAddress() {
        return orderAddress;
    }

    public void setOrderAddress(OrderAddress orderAddress) {
        this.orderAddress = orderAddress;
    }
}
