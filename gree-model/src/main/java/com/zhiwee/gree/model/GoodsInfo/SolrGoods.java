package com.zhiwee.gree.model.GoodsInfo;

import org.apache.solr.client.solrj.beans.Field;

import javax.persistence.Id;
import javax.persistence.Transient;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author: jick
 * @Date: 2019/10/9 18:35
 */
public class SolrGoods implements Serializable {

    @Field
    private   String  id;
    @Field
    private  String  name;
    @Field
    private  String  goodsNum;
    @Field
    private  Double activePrice;
    @Field
    private  Double shopPrice;
    @Field
    private    Double marketPrices;
    @Field
    private     Double costPrice;
    @Field
    private    Double   innerPrice;
    @Field
    private    Double   firstPrice;
    @Field
    private     Double   secondPrice;
    @Field
    private    Double   distributePrice;
    @Field
    private     Double  profit;
    @Field
    private    String goodCover;
    @Field
    private    Double weight;
    @Field
    private   Integer  storeCount;
    @Field
    private   Integer  saleCount;
    @Field
    private   Integer   giveIntegral;
    @Field
    private   Integer  exchangeIntegral;
    @Field
    private  String  describes;
    @Field
    private   Integer  commentNum;




    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getGoodsNum() {
        return goodsNum;
    }

    public void setGoodsNum(String goodsNum) {
        this.goodsNum = goodsNum;
    }

    public Double getActivePrice() {
        return activePrice;
    }

    public void setActivePrice(Double activePrice) {
        this.activePrice = activePrice;
    }

    public Double getShopPrice() {
        return shopPrice;
    }

    public void setShopPrice(Double shopPrice) {
        this.shopPrice = shopPrice;
    }

    public Double getMarketPrices() {
        return marketPrices;
    }

    public void setMarketPrices(Double marketPrices) {
        this.marketPrices = marketPrices;
    }

    public Double getCostPrice() {
        return costPrice;
    }

    public void setCostPrice(Double costPrice) {
        this.costPrice = costPrice;
    }

    public Double getInnerPrice() {
        return innerPrice;
    }

    public void setInnerPrice(Double innerPrice) {
        this.innerPrice = innerPrice;
    }

    public Double getFirstPrice() {
        return firstPrice;
    }

    public void setFirstPrice(Double firstPrice) {
        this.firstPrice = firstPrice;
    }

    public Double getSecondPrice() {
        return secondPrice;
    }

    public void setSecondPrice(Double secondPrice) {
        this.secondPrice = secondPrice;
    }

    public Double getDistributePrice() {
        return distributePrice;
    }

    public void setDistributePrice(Double distributePrice) {
        this.distributePrice = distributePrice;
    }

    public Double getProfit() {
        return profit;
    }

    public void setProfit(Double profit) {
        this.profit = profit;
    }

    public String getGoodCover() {
        return goodCover;
    }

    public void setGoodCover(String goodCover) {
        this.goodCover = goodCover;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Integer getStoreCount() {
        return storeCount;
    }

    public void setStoreCount(Integer storeCount) {
        this.storeCount = storeCount;
    }

    public Integer getSaleCount() {
        return saleCount;
    }

    public void setSaleCount(Integer saleCount) {
        this.saleCount = saleCount;
    }

    public Integer getGiveIntegral() {
        return giveIntegral;
    }

    public void setGiveIntegral(Integer giveIntegral) {
        this.giveIntegral = giveIntegral;
    }

    public Integer getExchangeIntegral() {
        return exchangeIntegral;
    }

    public void setExchangeIntegral(Integer exchangeIntegral) {
        this.exchangeIntegral = exchangeIntegral;
    }

    public String getDescribes() {
        return describes;
    }

    public void setDescribes(String describes) {
        this.describes = describes;
    }

    public Integer getCommentNum() {
        return commentNum;
    }

    public void setCommentNum(Integer commentNum) {
        this.commentNum = commentNum;
    }
}
