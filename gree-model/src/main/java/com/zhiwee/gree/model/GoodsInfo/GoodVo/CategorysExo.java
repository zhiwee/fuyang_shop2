package com.zhiwee.gree.model.GoodsInfo.GoodVo;

import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecgframework.poi.excel.annotation.ExcelTarget;

import java.io.Serializable;

/**
 * @author DELL
 */
@ExcelTarget("CategorysExo")
public class CategorysExo  implements Serializable {
    @Excel(name = "分类id")
    private String id;
    @Excel(name = "分类楼层")
    private Integer floor;
    @Excel(name = "分类名称")
    private String name;
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getFloor() {
        return floor;
    }

    public void setFloor(Integer floor) {
        this.floor = floor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
