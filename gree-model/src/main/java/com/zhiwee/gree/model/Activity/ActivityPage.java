package com.zhiwee.gree.model.Activity;

import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.zhiwee.gree.model.enums.ActivityScopeType;
import org.springframework.beans.factory.annotation.Autowired;
import xyz.icrab.common.model.BaseModel;
import xyz.icrab.common.model.annotation.Like;
import xyz.icrab.common.model.enums.YesOrNo;
import xyz.icrab.common.model.type.OrderBy;
import xyz.icrab.common.util.jackson.CustomJsonDateDeserializer;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.math.BigDecimal;
import java.util.Date;
/**
 * @author sun on 2019/7/8
 */
@Table(name = "s_activity_page")
public class ActivityPage  implements Serializable {

    @Id
    private String id;


    /**
     * 菜单名称
     */
    private String name;
    /**
     * 页面对应的url
     */
    private String activityUrl;

    /**
     * 创建时间
     */
    @JsonDeserialize(using = CustomJsonDateDeserializer.class)
    private Date createTime;




    /**
     * 更新时间
     */
    @JsonDeserialize(using = CustomJsonDateDeserializer.class)
    private Date updateTime;


    /**
     * 1 启用  2 禁用
     */
    private Integer state;


    /**
     * 1 pc  2 手机
     */
    private Integer type;


    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getActivityUrl() {
        return activityUrl;
    }

    public void setActivityUrl(String activityUrl) {
        this.activityUrl = activityUrl;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }
}
