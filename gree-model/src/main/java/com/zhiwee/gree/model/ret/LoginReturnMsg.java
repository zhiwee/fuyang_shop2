package com.zhiwee.gree.model.ret;

/**
 * @author Knight
 * @since 2018/4/23 10:49
 */
public class LoginReturnMsg {

    public static final String EMPTY_USERNAME = "用户名不能为空";
    public static final String EMPTY_PASSWORD = "密码不能为空";
    public static final String UNKONWN_IDENTIFY_TYPE = "未知的登录类型";
    public static final String FAILED = "用户名或密码不正确";
}
