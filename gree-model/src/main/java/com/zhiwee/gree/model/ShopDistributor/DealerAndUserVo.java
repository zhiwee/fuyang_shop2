package com.zhiwee.gree.model.ShopDistributor;

/**
 * @author 周广
 */
public class DealerAndUserVo{

    //门店
    private String dealerId;

    private String dealerName;

    private String parentName;

    private String managerName;

    private String userTelephone;

    //用户
    private String userId;

    private String userName;

    private String userPassword;

    //共用
    private String phone;

    private String userQq;

    private String userEml;

    private String userRediss;

    private String userSum;


    public String getDealerId() {
        return dealerId;
    }

    public void setDealerId(String dealerId) {
        this.dealerId = dealerId;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    public String getUserTelephone() {
        return userTelephone;
    }

    public void setUserTelephone(String userTelephone) {
        this.userTelephone = userTelephone;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUserQq() {
        return userQq;
    }

    public void setUserQq(String userQq) {
        this.userQq = userQq;
    }

    public String getUserEml() {
        return userEml;
    }

    public void setUserEml(String userEml) {
        this.userEml = userEml;
    }

    public String getUserRediss() {
        return userRediss;
    }

    public void setUserRediss(String userRediss) {
        this.userRediss = userRediss;
    }

    public String getUserSum() {
        return userSum;
    }

    public void setUserSum(String userSum) {
        this.userSum = userSum;
    }
}
