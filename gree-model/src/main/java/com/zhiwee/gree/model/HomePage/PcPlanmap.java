package com.zhiwee.gree.model.HomePage;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import xyz.icrab.common.util.jackson.CustomJsonDateDeserializer;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Date;

@Table(name = "s_page_planmap")
public class PcPlanmap {
    @Id
    private  String id;
    //说明
    private String name;
    //图片
    private String  picture;
    //1-启用   2-禁用
    private Integer state;
    //预留字段  1-指向商品  2-指向页面
    private Integer type;
    //指向
    private String url;
    //指向名
    @Transient
    private String urlName;
    //排序
    private Integer thisSort;

    @JsonDeserialize(using = CustomJsonDateDeserializer.class)
    private Date begintime;

    @JsonDeserialize(using = CustomJsonDateDeserializer.class)
    private Date  endtime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getThisSort() {
        return thisSort;
    }

    public void setThisSort(Integer thisSort) {
        this.thisSort = thisSort;
    }

    public Date getBegintime() {
        return begintime;
    }

    public void setBegintime(Date begintime) {
        this.begintime = begintime;
    }

    public Date getEndtime() {
        return endtime;
    }

    public void setEndtime(Date endtime) {
        this.endtime = endtime;
    }

    public String getUrlName() {
        return urlName;
    }

    public void setUrlName(String urlName) {
        this.urlName = urlName;
    }
}
