package com.zhiwee.gree.model.GoodsInfo;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.List;

/**
 * @author DELL
 */
@Table(name = "t_goods_category")
public class Categorys {
    @Id
    private String id;
    private String code;
    private String goodsCategoryId;

    private Integer rank;
    private Integer floor;

    private String name;

    private String parentId;
    @Transient
    private String parentName;
    @Transient
    private String goodsCategoryName;

    private String topName;

    private Integer state;

    private Integer type;

    private Integer isAfter;

    private Integer isLook;

    private String thisSort;

    private String seoTitle;

    private String seoKeyword;
    private Double integral;
    private String remake;
    private String categoryUrl;

    @Transient
    List<Categorys> list;


    public Integer getFloor() {
        return floor;
    }

    public void setFloor(Integer floor) {
        this.floor = floor;
    }

    public String getCategoryUrl() {
        return categoryUrl;
    }

    public void setCategoryUrl(String categoryUrl) {
        this.categoryUrl = categoryUrl;
    }

    public List<Categorys> getList() {
        return list;
    }

    public void setList(List<Categorys> list) {
        this.list = list;
    }

    public String getCode() {
        return code;
    }

    public Double getIntegral() {
        return integral;
    }

    public void setIntegral(Double integral) {
        this.integral = integral;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getIsLook() {
        return isLook;
    }

    public void setIsLook(Integer isLook) {
        this.isLook = isLook;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public String getGoodsCategoryId() {
        return goodsCategoryId;
    }

    public void setGoodsCategoryId(String goodsCategoryId) {
        this.goodsCategoryId = goodsCategoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getTopName() {
        return topName;
    }

    public void setTopName(String topName) {
        this.topName = topName;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getIsAfter() {
        return isAfter;
    }

    public void setIsAfter(Integer isAfter) {
        this.isAfter = isAfter;
    }

    public String getThisSort() {
        return thisSort;
    }

    public void setThisSort(String thisSort) {
        this.thisSort = thisSort;
    }

    public String getSeoTitle() {
        return seoTitle;
    }

    public void setSeoTitle(String seoTitle) {
        this.seoTitle = seoTitle;
    }

    public String getSeoKeyword() {
        return seoKeyword;
    }

    public void setSeoKeyword(String seoKeyword) {
        this.seoKeyword = seoKeyword;
    }

    public String getRemake() {
        return remake;
    }

    public void setRemake(String remake) {
        this.remake = remake;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public String getGoodsCategoryName() {
        return goodsCategoryName;
    }

    public void setGoodsCategoryName(String goodsCategoryName) {
        this.goodsCategoryName = goodsCategoryName;
    }
}
