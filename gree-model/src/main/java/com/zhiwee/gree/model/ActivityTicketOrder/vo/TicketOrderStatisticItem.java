package com.zhiwee.gree.model.ActivityTicketOrder.vo;

import java.io.Serializable;

/**
 * @author sun on 2019/7/5
 */
public class TicketOrderStatisticItem implements Serializable {

    private int count;

    private String date;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}
