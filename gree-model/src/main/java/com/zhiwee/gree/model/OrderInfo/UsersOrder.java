package com.zhiwee.gree.model.OrderInfo;


import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class UsersOrder implements Serializable {

    //主键
    private   String  id;

    //订单号
    private  String  orderNum;

    //支付人信息
    private   String  payId;

    //活动id
    private  String activityId;

    //商品的id
    private String goodsId;

    //创建时间
    private Date createTime;

    //总金额
    private BigDecimal totalPrice;
    //实际支付金额
    private BigDecimal realPrice;

    private  Integer  state;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }

    public String getPayId() {
        return payId;
    }

    public void setPayId(String payId) {
        this.payId = payId;
    }

    public String getActivityId() {
        return activityId;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    public String getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public BigDecimal getRealPrice() {
        return realPrice;
    }

    public void setRealPrice(BigDecimal realPrice) {
        this.realPrice = realPrice;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }
}
