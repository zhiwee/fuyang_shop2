package com.zhiwee.gree.model.OrderInfo.vo;

import java.io.Serializable;

/**
 * @author sun on 2019/7/5
 */
public class OrderStatisticItem implements Serializable {



    private double count;

    private String date;


    public double getCount() {
        return count;
    }

    public void setCount(double count) {
        this.count = count;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }


}
