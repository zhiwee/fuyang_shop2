package com.zhiwee.gree.model.OnlineActivity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.zhiwee.gree.model.GoodsInfo.Goods;
import com.zhiwee.gree.model.enums.OnlineActivityType;
import xyz.icrab.common.model.annotation.Like;
import xyz.icrab.common.model.type.OrderBy;
import xyz.icrab.common.util.jackson.CustomJsonDateDeserializer;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author sun on 2019/5/16
 */
@Table(name = "s_online_ActivityInfo")
public class OnlineActivityInfo implements Serializable {

    @Id
    @OrderBy(OrderBy.Sort.DESC)
    private String id;

    //名称
    @Like
    private String name;
    //描述
    private String description;

    @JsonDeserialize(using = CustomJsonDateDeserializer.class)
    private Date beginTime;

    @JsonDeserialize(using = CustomJsonDateDeserializer.class)
    private Date endTime;

    private Integer state;

    private OnlineActivityType activityType;

    private String updateman;
    private String createman;

    @JsonDeserialize(using = CustomJsonDateDeserializer.class)
    private Date updatetime;
    @JsonDeserialize(using = CustomJsonDateDeserializer.class)
    private Date createtime;
    //线下活动ID
    private String activityId;
    private String activityName;
    //是否可以使用优惠券
    private Integer canCoupon;


    @Transient
    private String idPath;

    @Transient
    private List<Goods> list;


    public List<Goods> getList() {
        return list;
    }

    public void setList(List<Goods> list) {
        this.list = list;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(Date beginTime) {
        this.beginTime = beginTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public OnlineActivityType getActivityType() {
        return activityType;
    }

    public void setActivityType(OnlineActivityType activityType) {
        this.activityType = activityType;
    }

    public String getUpdateman() {
        return updateman;
    }

    public void setUpdateman(String updateman) {
        this.updateman = updateman;
    }

    public String getCreateman() {
        return createman;
    }

    public void setCreateman(String createman) {
        this.createman = createman;
    }

    public Date getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Date updatetime) {
        this.updatetime = updatetime;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public String getActivityId() {
        return activityId;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public Integer getCanCoupon() {
        return canCoupon;
    }

    public void setCanCoupon(Integer canCoupon) {
        this.canCoupon = canCoupon;
    }

    public String getIdPath() {
        return idPath;
    }

    public void setIdPath(String idPath) {
        this.idPath = idPath;
    }
}
