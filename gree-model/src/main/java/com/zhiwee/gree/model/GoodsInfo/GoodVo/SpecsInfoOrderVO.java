package com.zhiwee.gree.model.GoodsInfo.GoodVo;

import com.zhiwee.gree.model.GoodsInfo.GoodsSpecsInfo;
import com.zhiwee.gree.model.GoodsInfo.GoodsSpecsItem;

import java.util.List;

public class SpecsInfoOrderVO {
    private String id;

    private String name;

    private List<GoodsSpecsItem> itemList;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<GoodsSpecsItem> getItemList() {
        return itemList;
    }

    public void setItemList(List<GoodsSpecsItem> itemList) {
        this.itemList = itemList;
    }
}
