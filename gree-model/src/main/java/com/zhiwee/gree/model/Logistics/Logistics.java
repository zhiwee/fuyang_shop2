package com.zhiwee.gree.model.Logistics;

import java.io.Serializable;

/**
 * @author sun on 2019/7/11
 */
public class Logistics implements Serializable {
    /**
     * 物流单号
     *
     * 	快递单号 【顺丰请输入运单号 : 收件人或寄件人手机号后四位。例如：123456789:1234】
     */
    private String no;

    /**
     * 快递公司代码: 可不填自动识别，填了查询更快【代码见附表
     */
    private     String  type;


    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
