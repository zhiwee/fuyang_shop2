package com.zhiwee.gree.model.draw;

import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecgframework.poi.excel.annotation.ExcelTarget;

import java.io.Serializable;
import java.util.Date;

@ExcelTarget("DrawUseInfoExrt")
public class DrawUseInfoExrt implements Serializable {

    @Excel(name = "顾客姓名", needMerge = true)
    private String customer;

    @Excel(name = "顾客电话", needMerge = true)
    private String customerPhone;

    @Excel(name = "奖池名称", needMerge = true)
    private String drawName;

    //奖项
    @Excel(name = "奖项",replace={"一等奖_1","二等奖_2","三等奖_3","四等奖_4","五等奖_5","六等奖_6","七等奖_7","八等奖_8","九等奖_9"})
    private Integer prize;

    @Excel(name = "奖品", needMerge = true)
    private String draws;

    @Excel(name = "中奖时间", needMerge = true,exportFormat = "yyyy-MM-dd HH:mm:ss")
    private Date createtime;

    // 1-未领奖、2-已领奖
    @Excel(name = "是否领奖",replace={"未领奖_1","已领奖_2"})
    private Integer state;

    @Excel(name = "核销人姓名", needMerge = true)
    private String sysusername;

    @Excel(name = "核销时间", needMerge = true,exportFormat = "yyyy-MM-dd HH:mm:ss")
    private Date updatetime;


    public Date getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Date updatetime) {
        this.updatetime = updatetime;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public String getDrawName() {
        return drawName;
    }

    public void setDrawName(String drawName) {
        this.drawName = drawName;
    }

//    public String getActivityName() {
//        return activityName;
//    }
//
//    public void setActivityName(String activityName) {
//        this.activityName = activityName;
//    }

    public Integer getPrize() {
        return prize;
    }

    public void setPrize(Integer prize) {
        this.prize = prize;
    }

    public String getDraws() {
        return draws;
    }

    public void setDraws(String draws) {
        this.draws = draws;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public String getSysusername() {
        return sysusername;
    }

    public void setSysusername(String sysusername) {
        this.sysusername = sysusername;
    }
}
