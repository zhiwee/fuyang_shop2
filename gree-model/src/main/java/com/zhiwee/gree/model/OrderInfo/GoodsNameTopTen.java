package com.zhiwee.gree.model.OrderInfo;

import java.math.BigDecimal;

/**
 * @author DELL
 */
public class GoodsNameTopTen {
    private String goodsName;
    private BigDecimal num;

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public BigDecimal getNum() {
        return num;
    }

    public void setNum(BigDecimal num) {
        this.num = num;
    }
}
