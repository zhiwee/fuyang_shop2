package com.zhiwee.gree.model.Coupon;

import java.io.Serializable;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.zhiwee.gree.model.FyGoods.FygoodsCart;
import com.zhiwee.gree.model.OrderInfo.OrderItem;
import xyz.icrab.common.util.jackson.CustomJsonDateDeserializer;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.criteria.CriteriaBuilder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author sun on 2019/5/10
 */
@Table(name="s_sys_coupon")
public class Coupon  implements Serializable {

    @Id
    private String id;

    private String subjectId;

    private String subjectName;

    private String couponSeqno;

    private String orderId;


    private Double couponMoney;

    private Double reachMoney;

    @JsonDeserialize(
            using = CustomJsonDateDeserializer.class
    )
    private Date createTime;

    @JsonDeserialize(
            using = CustomJsonDateDeserializer.class
    )
    private Date updateTime;



    @JsonDeserialize(
            using = CustomJsonDateDeserializer.class
    )
    private Date startTime;
    @Transient
    private String startTimeStr;

    @JsonDeserialize(
            using = CustomJsonDateDeserializer.class
    )
    private Date endTime;
    @Transient
    private String endTimeStr;



    private Integer sort;

//类型1 全品类优惠券 2 非全品类优惠券 3指定某个商品的优惠券  4全品类代金券  5 非全品类代金券 6指定某个商品的代金券
    private Integer type;


    private String idPath;

    private String createMan;



    private String  updateMan;


    private String  userId;
    private String  userName;
    private String  userPhone;

    private String  password;

    private Integer state;

    private Integer couponState;

    private Integer sendState;

   // 1市场价 2 非市场价
    private Integer priceType;
    @Transient
    private String categoryName;

    @Transient
        private Integer canUse;
   @Transient
    private Integer num;
    @Transient
    private Double orderMoney;
    @Transient
    private List<OrderItem> orderItems = new ArrayList<>();


    public Integer getCanUse() {
        return canUse;
    }

    public void setCanUse(Integer canUse) {
        this.canUse = canUse;
    }

    public Double getOrderMoney() {
        return orderMoney;
    }

    public void setOrderMoney(Double orderMoney) {
        this.orderMoney = orderMoney;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public List<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    public String getStartTimeStr() {
        return startTimeStr;
    }

    public void setStartTimeStr(String startTimeStr) {
        this.startTimeStr = startTimeStr;
    }

    public String getEndTimeStr() {
        return endTimeStr;
    }

    public void setEndTimeStr(String endTimeStr) {
        this.endTimeStr = endTimeStr;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getSendState() {
        return sendState;
    }

    public void setSendState(Integer sendState) {
        this.sendState = sendState;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }



    public Integer getPriceType() {
        return priceType;
    }

    public void setPriceType(Integer priceType) {
        this.priceType = priceType;
    }

    public Double getReachMoney() {
        return reachMoney;
    }

    public void setReachMoney(Double reachMoney) {
        this.reachMoney = reachMoney;
    }

    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    public Integer getCouponState() {
        return couponState;
    }

    public void setCouponState(Integer couponState) {
        this.couponState = couponState;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCouponSeqno() {
        return couponSeqno;
    }

    public void setCouponSeqno(String couponSeqno) {
        this.couponSeqno = couponSeqno;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Double getCouponMoney() {
        return couponMoney;
    }

    public void setCouponMoney(Double couponMoney) {
        this.couponMoney = couponMoney;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getIdPath() {
        return idPath;
    }

    public void setIdPath(String idPath) {
        this.idPath = idPath;
    }

    public String getCreateMan() {
        return createMan;
    }

    public void setCreateMan(String createMan) {
        this.createMan = createMan;
    }

    public String getUpdateMan() {
        return updateMan;
    }

    public void setUpdateMan(String updateMan) {
        this.updateMan = updateMan;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }
}
