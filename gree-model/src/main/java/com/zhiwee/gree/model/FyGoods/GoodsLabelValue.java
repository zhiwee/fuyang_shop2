package com.zhiwee.gree.model.FyGoods;

import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author DELL
 */
@Table(name="t_sys_labelvalue")
public class GoodsLabelValue  implements Serializable {
    @Id
    private String id;
    private String name;
    private String labelId;
    private Integer state;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLabelId() {
        return labelId;
    }

    public void setLabelId(String labelId) {
        this.labelId = labelId;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }
}
