package com.zhiwee.gree.model.livePlay;

import java.io.Serializable;
import java.util.List;

public class LivePlay implements Serializable {
    private String name;
    private String cover_img;
    private Long start_time;
    private String start_timeStr;
    private Long end_time;
    private String end_timeStr;
    private String anchor_name;
    private Integer roomid;
    private List<LivePlayGoods> goods;

    private Integer live_status;
    private String share_img;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCover_img() {
        return cover_img;
    }

    public void setCover_img(String cover_img) {
        this.cover_img = cover_img;
    }

    public Long getStart_time() {
        return start_time;
    }

    public void setStart_time(Long start_time) {
        this.start_time = start_time;
    }

    public Long getEnd_time() {
        return end_time;
    }

    public void setEnd_time(Long end_time) {
        this.end_time = end_time;
    }

    public String getStart_timeStr() {
        return start_timeStr;
    }

    public void setStart_timeStr(String start_timeStr) {
        this.start_timeStr = start_timeStr;
    }

    public String getEnd_timeStr() {
        return end_timeStr;
    }

    public void setEnd_timeStr(String end_timeStr) {
        this.end_timeStr = end_timeStr;
    }




    public String getAnchor_name() {
        return anchor_name;
    }

    public void setAnchor_name(String anchor_name) {
        this.anchor_name = anchor_name;
    }

    public Integer getRoomid() {
        return roomid;
    }

    public void setRoomid(Integer roomid) {
        this.roomid = roomid;
    }

    public Integer getLive_status() {
        return live_status;
    }

    public void setLive_status(Integer live_status) {
        this.live_status = live_status;
    }

    public List<LivePlayGoods> getGoods() {
        return goods;
    }

    public void setGoods(List<LivePlayGoods> goods) {
        this.goods = goods;
    }


    public String getShare_img() {
        return share_img;
    }

    public void setShare_img(String share_img) {
        this.share_img = share_img;
    }


}
