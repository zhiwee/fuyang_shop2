package com.zhiwee.gree.model.FyGoods;


import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "t_fygoodsimg")
public class FyGoodsImg {

    @Id
    private String id;

    private String goodsId;

    private String url;

    private   Integer  sort;

    private  String  remark;



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url == null ? null : url.trim();
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}