package com.zhiwee.gree.model.OrderInfo;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.zhiwee.gree.model.ShopUserAddress.ShopUserAddress;
import xyz.icrab.common.util.jackson.CustomJsonDateDeserializer;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author sun on 2019/5/11
 */
@Table(name="s_order_address")
public class OrderAddress implements Serializable {

    @Id
    private String id;


    private String province;


    private String provinceCode;


    private String city;

    private String cityCode;

    private String area;

    private String areaCode;
    /**
     * 详细地址
     */

    private String detailAddress;


    private String installAddress;

    /**
     * 订单id
     */
    private  String orderId;
    /**
     * 收货人
     */

    private String deliveryUser;

    /**
     * 收货号码
     */
    private String deliveryPhone;

    /**
     * 座机号码
     */
    private String mobile;

    /**
     * 邮政编码
     */
    private String emsCode;
    /**
     * 工程团购单位名称
     */

    private String dealerName;

    /**
     * 订单号
     */
    @Transient
    private String orderNum;

    @Transient
    private Integer sendType;
    @Transient
    private Integer type;

    /**
     * 认领时间
     */
    @Transient
    private Date claimTime;

    public OrderAddress(){}

    public OrderAddress(ShopUserAddress shopUserAddress){
        this.province = shopUserAddress.getProvince();
        this.provinceCode = shopUserAddress.getProvinceCode();
        this.city = shopUserAddress.getCity();
        this.cityCode = shopUserAddress.getCityCode();
        this.area = shopUserAddress.getArea();
        this.areaCode = shopUserAddress.getAreaCode();
        this.detailAddress = shopUserAddress.getAddress();
        this.deliveryUser = shopUserAddress.getDeliveryUser();
        this.deliveryPhone = shopUserAddress.getDeliveryPhone();
        this.mobile = shopUserAddress.getMobile();
        this.emsCode = shopUserAddress.getEmsCode();


    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmsCode() {
        return emsCode;
    }

    public void setEmsCode(String emsCode) {
        this.emsCode = emsCode;
    }

    public String getDeliveryUser() {
        return deliveryUser;
    }

    public void setDeliveryUser(String deliveryUser) {
        this.deliveryUser = deliveryUser;
    }

    public String getDeliveryPhone() {
        return deliveryPhone;
    }

    public void setDeliveryPhone(String deliveryPhone) {
        this.deliveryPhone = deliveryPhone;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getProvinceCode() {
        return provinceCode;
    }

    public void setProvinceCode(String provinceCode) {
        this.provinceCode = provinceCode;
    }

    public String getCity() {
        return city;
    }

    public Integer getSendType() {
        return sendType;
    }

    public void setSendType(Integer sendType) {
        this.sendType = sendType;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getDetailAddress() {
        return detailAddress;
    }

    public void setDetailAddress(String detailAddress) {
        this.detailAddress = detailAddress;
    }

    public String getInstallAddress() {
        return installAddress;
    }

    public void setInstallAddress(String installAddress) {
        this.installAddress = installAddress;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }

    public Date getClaimTime() {
        return claimTime;
    }

    public void setClaimTime(Date claimTime) {
        this.claimTime = claimTime;
    }
}
