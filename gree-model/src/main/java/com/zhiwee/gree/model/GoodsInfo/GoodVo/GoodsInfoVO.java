package com.zhiwee.gree.model.GoodsInfo.GoodVo;

import java.math.BigDecimal;
import java.util.List;

public class GoodsInfoVO {
    //商品信息
    private   String  id;

    private  String  name;

    private  String describes;

    private BigDecimal shopPrice;

    private   BigDecimal marketPrice;

    private  String goodCover;

    private   Integer  storeCount;  //库存

    private   Integer  saleCount;  //销量

    private Integer commentNum;  //评论数量
    //商品规格信息
    private List<GoodsSpecsItemOrderVO> specsList;

    //商品相册
    private List<GoodsImageVO> imageList;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescribes() {
        return describes;
    }

    public void setDescribes(String describes) {
        this.describes = describes;
    }

    public BigDecimal getShopPrice() {
        return shopPrice;
    }

    public void setShopPrice(BigDecimal shopPrice) {
        this.shopPrice = shopPrice;
    }

    public BigDecimal getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(BigDecimal marketPrice) {
        this.marketPrice = marketPrice;
    }

    public String getGoodCover() {
        return goodCover;
    }

    public void setGoodCover(String goodCover) {
        this.goodCover = goodCover;
    }

    public List<GoodsSpecsItemOrderVO> getSpecsList() {
        return specsList;
    }

    public void setSpecsList(List<GoodsSpecsItemOrderVO> specsList) {
        this.specsList = specsList;
    }

    public List<GoodsImageVO> getImageList() {
        return imageList;
    }

    public void setImageList(List<GoodsImageVO> imageList) {
        this.imageList = imageList;
    }

    public Integer getStoreCount() {
        return storeCount;
    }

    public void setStoreCount(Integer storeCount) {
        this.storeCount = storeCount;
    }

    public Integer getSaleCount() {
        return saleCount;
    }

    public void setSaleCount(Integer saleCount) {
        this.saleCount = saleCount;
    }
}
