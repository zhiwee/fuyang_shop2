package com.zhiwee.gree.model.ActivityTicketOrder;
import xyz.icrab.common.model.Represent;
/**
 * created by
 */
public enum TicketState implements Represent<Integer> {



    /**
     * 未分发
     */
        cerate(0, "未分发"),
        /**
         * 已下单
         */
        Init(1, "未激活"),
        /**
         * 已支付
         */
        Paid(2, "已激活"),
        /**
         * 已退款
         */
        Back(3, "已下单"),
        /**
         * 已失效
         */
        Expired(4, "已退款");

        private final int value;

        private final String message;

    TicketState(int value, String message) {
            this.value = value;
            this.message = message;
        }

        @Override
        public Integer value() {
            return this.value;
        }

        @Override
        public String message() {
            return this.message;
        }
    }


