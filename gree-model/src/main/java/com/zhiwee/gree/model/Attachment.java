package com.zhiwee.gree.model;

import xyz.icrab.common.model.annotation.GreaterEqual;
import xyz.icrab.common.model.annotation.LessEqual;
import xyz.icrab.common.model.annotation.Like;

import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * 附件
 */
@Table(name="s_sys_attachment")
public class Attachment implements Serializable {
    @Id
    private String id;
    private String refid;

    private String originalname;
    private String logicname;
    private String path;
    private Integer category;
    private Long size;
    private String type;

    private Date createtime;
    private String createman;
    private String createname;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRefid() {
        return refid;
    }

    public void setRefid(String refid) {
        this.refid = refid;
    }

    public String getOriginalname() {
        return originalname;
    }

    public void setOriginalname(String originalname) {
        this.originalname = originalname;
    }

    public String getLogicname() {
        return logicname;
    }

    public void setLogicname(String logicname) {
        this.logicname = logicname;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Integer getCategory() {
        return category;
    }

    public void setCategory(Integer category) {
        this.category = category;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public String getCreateman() {
        return createman;
    }

    public void setCreateman(String createman) {
        this.createman = createman;
    }

    public String getCreatename() {
        return createname;
    }

    public void setCreatename(String createname) {
        this.createname = createname;
    }
}
