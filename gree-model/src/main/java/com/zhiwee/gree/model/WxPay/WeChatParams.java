package com.zhiwee.gree.model.WxPay;

import java.io.Serializable;

/**
 * @author sun on 2019/5/15
 */
public class WeChatParams implements Serializable {
    public String total_fee;//订单金额【备注：以分为单位】
    public String body;//商品名称
    public String out_trade_no;//商户订单号
    public String attach;//附加参数
    public String memberid;//会员ID
    public String refundPassword;//退款密码
    public String code;//用户code

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getRefundPassword() {
        return refundPassword;
    }

    public void setRefundPassword(String refundPassword) {
        this.refundPassword = refundPassword;
    }

    public String getTotal_fee() {
        return total_fee;
    }

    public void setTotal_fee(String total_fee) {
        this.total_fee = total_fee;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getOut_trade_no() {
        return out_trade_no;
    }

    public void setOut_trade_no(String out_trade_no) {
        this.out_trade_no = out_trade_no;
    }

    public String getAttach() {
        return attach;
    }

    public void setAttach(String attach) {
        this.attach = attach;
    }

    public String getMemberid() {
        return memberid;
    }

    public void setMemberid(String memberid) {
        this.memberid = memberid;
    }
}
