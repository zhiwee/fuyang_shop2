package com.zhiwee.gree.model.GoodsInfo;

import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 商品品牌
 */
@Table(name="s_goods_brand")
public class GoodsBand {
    //编码
    @Id
    private String id;
    //品牌名
    private String name;
    private String cover;
    //品牌备注
    private String remake;
    // 状态1：正常 2、停用 3、删除
    private Integer state;
    //品牌排序
    private Integer thisSort;
    //楼层
    private Integer floor;


    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public Integer getFloor() {
        return floor;
    }

    public void setFloor(Integer floor) {
        this.floor = floor;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRemake() {
        return remake;
    }

    public void setRemake(String remake) {
        this.remake = remake;
    }

    public Integer getThisSort() {
        return thisSort;
    }

    public void setThisSort(Integer thisSort) {
        this.thisSort = thisSort;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }
}
