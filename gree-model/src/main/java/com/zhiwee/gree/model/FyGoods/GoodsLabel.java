package com.zhiwee.gree.model.FyGoods;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.List;

/**
 * @author DELL
 */
@Table(name="t_sys_label")
public class GoodsLabel implements Serializable {
    @Id
    private String id;
    private String name;
    private String parentId;
    private Integer state;
    private Integer rank;

    @Transient
    private List<GoodsLabel> list;
    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public List<GoodsLabel> getList() {
        return list;
    }

    public void setList(List<GoodsLabel> list) {
        this.list = list;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }
}
