package com.zhiwee.gree.model.GoodsInfo.GoodVo;

import java.math.BigDecimal;

public class GoodsSpecsItemOrderVO {

    private   String  id;  //id

    private String name;    //规格名

    private BigDecimal activePrice; //活动价

    private    BigDecimal shopPrice; //门店价

    private   BigDecimal marketPrice;//市场价

    private   BigDecimal   innerPrice; //员工内部价

    private   BigDecimal   firstPrice; //一级代理价

    private   BigDecimal   secondPrice; //二级代理价

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getActivePrice() {
        return activePrice;
    }

    public void setActivePrice(BigDecimal activePrice) {
        this.activePrice = activePrice;
    }

    public BigDecimal getShopPrice() {
        return shopPrice;
    }

    public void setShopPrice(BigDecimal shopPrice) {
        this.shopPrice = shopPrice;
    }

    public BigDecimal getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(BigDecimal marketPrice) {
        this.marketPrice = marketPrice;
    }

    public BigDecimal getInnerPrice() {
        return innerPrice;
    }

    public void setInnerPrice(BigDecimal innerPrice) {
        this.innerPrice = innerPrice;
    }

    public BigDecimal getFirstPrice() {
        return firstPrice;
    }

    public void setFirstPrice(BigDecimal firstPrice) {
        this.firstPrice = firstPrice;
    }

    public BigDecimal getSecondPrice() {
        return secondPrice;
    }

    public void setSecondPrice(BigDecimal secondPrice) {
        this.secondPrice = secondPrice;
    }

}
