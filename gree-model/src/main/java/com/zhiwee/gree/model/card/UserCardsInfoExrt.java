package com.zhiwee.gree.model.card;

import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecgframework.poi.excel.annotation.ExcelTarget;

import java.io.Serializable;
import java.util.Date;

/**
 * TODO
 *
 * @author Chensong
 * @Company http://www.zhiwee.com
 * date 2019-12-13
 */
@ExcelTarget("UserCardsInfoExrt")
public class UserCardsInfoExrt  implements Serializable {

    @Excel(name = "使用状态", replace ={"未支付_1","未使用_2","已使用_3"})
    private Integer state;

    @Excel(name = "优惠券名称")
    private String name;

    @Excel(name = "最低消费金额")
    private Double minMoney;

    @Excel(name = "抵用金额")
    private Double useMoney;

    @Excel(name = "顾客姓名")
    private String nickName;

    @Excel(name = "顾客电话")
    private String phone;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Double getMinMoney() {
        return minMoney;
    }

    public void setMinMoney(Double minMoney) {
        this.minMoney = minMoney;
    }

    public Double getUseMoney() {
        return useMoney;
    }

    public void setUseMoney(Double useMoney) {
        this.useMoney = useMoney;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }
}
