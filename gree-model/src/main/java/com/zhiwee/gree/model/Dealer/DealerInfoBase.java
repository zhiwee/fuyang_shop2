package com.zhiwee.gree.model.Dealer;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import xyz.icrab.common.model.annotation.Like;
import xyz.icrab.common.model.enums.YesOrNo;
import xyz.icrab.common.model.type.OrderBy;
import xyz.icrab.common.util.jackson.CustomJsonDateDeserializer;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 经销商信息表实体
 *
 * @version 1.0
 */
@Table(name = "t_dealer_info_base")
public class DealerInfoBase implements Serializable {

    @Id
    private String id;

    private YesOrNo state;

    private String center;

    private String district;

    private String area;

    private String town;

    private String code;

    private String name;

    private String address;

    private String mobile;

    private String parentId;

    private String parentName;

    @Like
    private String idPath;

    private String typeCode;

    private String typeName;

    private String managerName;//负责人

    private String smCallNumber;//业务员联系电话
    private String serviceManager;//格力业务

    private String createman;

    @OrderBy(OrderBy.Sort.ASC)
    @JsonDeserialize(using = CustomJsonDateDeserializer.class)
    private Date createTime;

    private String updateman;
    @JsonDeserialize(using = CustomJsonDateDeserializer.class)
    private Date updateTime;

    private Integer isShop;

    private Integer type;

    private List<DealerInfoBase> childrenList;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public YesOrNo getState() {
        return state;
    }

    public void setState(YesOrNo state) {
        this.state = state;
    }

    public String getCenter() {
        return center;
    }

    public void setCenter(String center) {
        this.center = center;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public String getIdPath() {
        return idPath;
    }

    public void setIdPath(String idPath) {
        this.idPath = idPath;
    }

    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    public String getSmCallNumber() {
        return smCallNumber;
    }

    public void setSmCallNumber(String smCallNumber) {
        this.smCallNumber = smCallNumber;
    }

    public String getServiceManager() {
        return serviceManager;
    }

    public void setServiceManager(String serviceManager) {
        this.serviceManager = serviceManager;
    }

    public String getCreateman() {
        return createman;
    }

    public void setCreateman(String createman) {
        this.createman = createman;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getUpdateman() {
        return updateman;
    }

    public void setUpdateman(String updateman) {
        this.updateman = updateman;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getIsShop() {
        return isShop;
    }

    public void setIsShop(Integer isShop) {
        this.isShop = isShop;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public List<DealerInfoBase> getChildrenList() {
        return childrenList;
    }

    public void setChildrenList(List<DealerInfoBase> childrenList) {
        this.childrenList = childrenList;
    }
}


