package com.zhiwee.gree.model.WithdrawalRecord;

import java.io.Serializable;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import xyz.icrab.common.util.jackson.CustomJsonDateDeserializer;

import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;
import javax.persistence.Transient;

/**
 * @author sun on 2019/6/14
 */
@Table(name="s_withdrawal_record")
public class WithdrawalRecord  implements Serializable {

    @Id
    private String id ;

    private  String  userId;

    private Double money;
    //1 申请中  2 同意  3 拒绝
    private Integer state;

    @JsonDeserialize(
            using = CustomJsonDateDeserializer.class
    )
    private Date createTime;


    @JsonDeserialize(
            using = CustomJsonDateDeserializer.class
    )
    private Date updateTime;


    @Transient
    private  Double activityCost;



    @Transient
    private  Double balance;

    @Transient
    private  String nickname;


    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public Double getActivityCost() {
        return activityCost;
    }

    public void setActivityCost(Double activityCost) {
        this.activityCost = activityCost;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Double getMoney() {
        return money;
    }

    public void setMoney(Double money) {
        this.money = money;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
