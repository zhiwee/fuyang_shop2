package com.zhiwee.gree.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

import xyz.icrab.common.model.enums.YesOrNo;

/**
 * @author gll
 * @since 2018/4/21 22:03
 */
@Table(name = "s_sys_dictionary")
public class Dictionary implements Serializable {

    /**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Id
    private String id;

    /**
     * key
     */
	@Column(name="key_")
    private String key;

    /**
     * value
     */
	@Column(name="value_")
    private String value;

    /**
     * 分组
     */
	@Column(name="group_")
    private String group;

    /**
     * 是否启用 1是2否
     */
    private YesOrNo state;

    /**
     * 创建时间
     */
    private Date createtime;

    /**
     * 创建人
     */
    private String createman;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

    public YesOrNo getState() {
        return state;
    }

    public void setState(YesOrNo state) {
        this.state = state;
    }

    public Date getCreatetime() {
		return createtime;
	}

	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}

	public String getCreateman() {
		return createman;
	}

	public void setCreateman(String createman) {
		this.createman = createman;
	}

}
