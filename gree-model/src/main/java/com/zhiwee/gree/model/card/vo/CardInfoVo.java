package com.zhiwee.gree.model.card.vo;

public class CardInfoVo {

    private String categoryCode;
    private String msg;
    private Integer state;
    private String departmentCode;
    private double canUseMoney;
    private String  couponId;

    public String getCouponId() {
        return couponId;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public void setCouponId(String couponId) {
        this.couponId = couponId;
    }

    public String getCategoryCode() {
        return categoryCode;
    }

    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    public String getDepartmentCode() {
        return departmentCode;
    }

    public void setDepartmentCode(String departmentCode) {
        this.departmentCode = departmentCode;
    }

    public double getCanUseMoney() {
        return canUseMoney;
    }

    public void setCanUseMoney(double canUseMoney) {
        this.canUseMoney = canUseMoney;
    }
}
