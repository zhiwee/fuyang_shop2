package com.zhiwee.gree.model.GoodsInfo;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import xyz.icrab.common.util.jackson.CustomJsonDateDeserializer;

import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Id;
/**
 * @author sun on 2019/5/5
 */
@Table(name="t_goods_detail")
public class GoodsDetail implements Serializable {
    @Id
    private String id;

    private String detail;
    @JsonDeserialize(
            using = CustomJsonDateDeserializer.class
    )
    private Date createtime;
    @JsonDeserialize(
            using = CustomJsonDateDeserializer.class
    )
    private Date updatetime;
    private String createMan;
    private String updateMan;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public Date getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Date updatetime) {
        this.updatetime = updatetime;
    }

    public String getCreateMan() {
        return createMan;
    }

    public void setCreateMan(String createMan) {
        this.createMan = createMan;
    }

    public String getUpdateMan() {
        return updateMan;
    }

    public void setUpdateMan(String updateMan) {
        this.updateMan = updateMan;
    }
}
