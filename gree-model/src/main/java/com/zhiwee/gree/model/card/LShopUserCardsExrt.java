package com.zhiwee.gree.model.card;

import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecgframework.poi.excel.annotation.ExcelTarget;

import java.util.Date;

/**
 * TODO
 *
 * @author Chensong
 * @Company http://www.zhiwee.com
 * date 2019-12-17
 */
@ExcelTarget("LShopUserCardsExrt")
public class LShopUserCardsExrt {

    @Excel(name = "支付状态", replace ={"未支付_1","已支付_2"})
    private Integer state;

    @Excel(name = "顾客电话", needMerge = true)
    private String phone;

    @Excel(name = "顾客姓名", needMerge = true)
    private String username;

    @Excel(name = "购买金额", needMerge = true)
    private Double money;

    @Excel(name = "购买时间", needMerge = true,exportFormat = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @Excel(name = "支付时间", needMerge = true,exportFormat = "yyyy-MM-dd HH:mm:ss")
    private Date payTime;

    @Excel(name = "支付专员账号", needMerge = true)
    private String operatorUsername;

    @Excel(name = "支付专员姓名", needMerge = true)
    private String operatorNickname;

    @Excel(name = "推荐券号", needMerge = true)
    private String ticket;

    @Excel(name = "卡号", needMerge = true)
    private String cardcode;

    public String getCardcode() {
        return cardcode;
    }

    public void setCardcode(String cardcode) {
        this.cardcode = cardcode;
    }

    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getOperatorUsername() {
        return operatorUsername;
    }

    public void setOperatorUsername(String operatorUsername) {
        this.operatorUsername = operatorUsername;
    }

    public String getOperatorNickname() {
        return operatorNickname;
    }

    public void setOperatorNickname(String operatorNickname) {
        this.operatorNickname = operatorNickname;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Double getMoney() {
        return money;
    }

    public void setMoney(Double money) {
        this.money = money;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getPayTime() {
        return payTime;
    }

    public void setPayTime(Date payTime) {
        this.payTime = payTime;
    }
}
