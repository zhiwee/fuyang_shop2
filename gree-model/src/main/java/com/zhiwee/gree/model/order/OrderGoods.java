package com.zhiwee.gree.model.order;


import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

@Table(name = "order_goods_info")
public class OrderGoods implements Serializable {

    @Id
    private  String id;

    //订单id
    private String  orderId;

    //商品id
    private  String goodsId;

    //商品名称
    private String  goodsName;

    //商品的规格参数
    private String sepc;

    //商品单价
    private BigDecimal shopPrice;

    //商品佣金
    private BigDecimal  commission;

    //商品数量
    private Integer amount;

    //需要支付金额
    private BigDecimal  payPrice;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getSepc() {
        return sepc;
    }

    public void setSepc(String sepc) {
        this.sepc = sepc;
    }

    public BigDecimal getShopPrice() {
        return shopPrice;
    }

    public void setShopPrice(BigDecimal shopPrice) {
        this.shopPrice = shopPrice;
    }

    public BigDecimal getCommission() {
        return commission;
    }

    public void setCommission(BigDecimal commission) {
        this.commission = commission;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public BigDecimal getPayPrice() {
        return payPrice;
    }

    public void setPayPrice(BigDecimal payPrice) {
        this.payPrice = payPrice;
    }
}
