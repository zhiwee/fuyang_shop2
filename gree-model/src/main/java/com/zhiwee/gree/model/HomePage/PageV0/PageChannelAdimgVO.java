package com.zhiwee.gree.model.HomePage.PageV0;

public class PageChannelAdimgVO {

    private String id;

    private String image;

    private Integer type;

    private String urlId;

    private String urlType;

    private String urlItem;

    private String modelId;

    private String addModelId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getUrlId() {
        return urlId;
    }

    public void setUrlId(String urlId) {
        this.urlId = urlId;
    }

    public String getUrlType() {
        return urlType;
    }

    public void setUrlType(String urlType) {
        this.urlType = urlType;
    }

    public String getUrlItem() {
        return urlItem;
    }

    public void setUrlItem(String urlItem) {
        this.urlItem = urlItem;
    }

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    public String getAddModelId() {
        return addModelId;
    }

    public void setAddModelId(String addModelId) {
        this.addModelId = addModelId;
    }
}
