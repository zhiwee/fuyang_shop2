package com.zhiwee.gree.model.ActivityTicketOrder;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import xyz.icrab.common.model.BaseModel;
import xyz.icrab.common.model.type.OrderBy;
import xyz.icrab.common.util.jackson.CustomJsonDateDeserializer;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Date;

/**
 * Description: 购买认筹的代码
 * @author: sun
 * @Date 下午4:57 2019/6/23
 * @param:
 * @return:
 */
@Table(name = "s_ticket_order")
public class TicketOrder implements Serializable {

    @Id
    private String id;


    private String userId;

    //门店id
    private String dealerId;


    //门店活动id
    private String activityId;


    private String ticketSeqno;

    private String orderPassword;


    //下单时间
    @JsonDeserialize(
            using = CustomJsonDateDeserializer.class
    )
    private Date createTime;

    // 支付时间
    @JsonDeserialize(
            using = CustomJsonDateDeserializer.class
    )
    private Date payTime;

    //更新时间
    private Date updateTime;

//订单状态 1 未支付 2 已支付 3 已退单 4 已退款
    private Integer orderState;


    //支付方式 1 支付宝 2 微信 3 银联 4 转账
    private Integer payType;

    //省
    private String deliveryProvince;
    //市
    private String deliveryCity;
    //区
    private String deliveryArea;

    //省编码
    private String provinceCode;
    //市编码
    private String cityCode;
    //区编码
    private String areaCode;
    //详细地址
    private String detailAddress;

    //实际金额
    private Double realPrice;
    //订单流水号
    private String  orderNum;

    //退款标识
    private String refundId;

    // 用户名
    @Transient
    private String userName;


    // 认筹人姓名
    private String orderName;

    //手机号
    private String phone;

    //1 活动认筹 2 当天认筹
    private Integer type;

    //退款金额
    private Double refundMoney;

    //1 未申请退款 2 申请退款中  3 拒绝申请退款  4 同意申请退款
    private Integer refundState;

// 下单购买数量
    @Transient
    private Integer amount;


    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getOrderPassword() {
        return orderPassword;
    }

    public void setOrderPassword(String orderPassword) {
        this.orderPassword = orderPassword;
    }

    public String getActivityId() {
        return activityId;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    public String getDealerId() {
        return dealerId;
    }

    public void setDealerId(String dealerId) {
        this.dealerId = dealerId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getDeliveryProvince() {
        return deliveryProvince;
    }

    public void setDeliveryProvince(String deliveryProvince) {
        this.deliveryProvince = deliveryProvince;
    }

    public String getDeliveryCity() {
        return deliveryCity;
    }

    public void setDeliveryCity(String deliveryCity) {
        this.deliveryCity = deliveryCity;
    }

    public String getDeliveryArea() {
        return deliveryArea;
    }

    public void setDeliveryArea(String deliveryArea) {
        this.deliveryArea = deliveryArea;
    }

    public String getProvinceCode() {
        return provinceCode;
    }

    public void setProvinceCode(String provinceCode) {
        this.provinceCode = provinceCode;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getDetailAddress() {
        return detailAddress;
    }

    public void setDetailAddress(String detailAddress) {
        this.detailAddress = detailAddress;
    }

    public Integer getRefundState() {
        return refundState;
    }

    public void setRefundState(Integer refundState) {
        this.refundState = refundState;
    }

    public Double getRefundMoney() {
        return refundMoney;
    }

    public void setRefundMoney(Double refundMoney) {
        this.refundMoney = refundMoney;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Double getRealPrice() {
        return realPrice;
    }

    public void setRealPrice(Double realPrice) {
        this.realPrice = realPrice;
    }

    public Integer getPayType() {
        return payType;
    }

    public void setPayType(Integer payType) {
        this.payType = payType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTicketSeqno() {
        return ticketSeqno;
    }

    public void setTicketSeqno(String ticketSeqno) {
        this.ticketSeqno = ticketSeqno;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getPayTime() {
        return payTime;
    }

    public void setPayTime(Date payTime) {
        this.payTime = payTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getOrderState() {
        return orderState;
    }

    public void setOrderState(Integer orderState) {
        this.orderState = orderState;
    }

    public String getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }

    public String getRefundId() {
        return refundId;
    }

    public void setRefundId(String refundId) {
        this.refundId = refundId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
