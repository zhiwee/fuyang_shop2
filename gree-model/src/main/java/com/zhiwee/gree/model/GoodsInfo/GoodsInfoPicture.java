package com.zhiwee.gree.model.GoodsInfo;


import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Date;

@Table(name="s_goods_info_picture")
public class GoodsInfoPicture {

    @Id
    private  String id;

    private String goodsId;

    private String  imgUrl;
    private Date createTime;
    private Integer sortBy;

    @Transient
    private String goodsName;


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public Integer getSortBy() {
        return sortBy;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public void setSortBy(Integer sortBy) {
        this.sortBy = sortBy;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }
}
