package com.zhiwee.gree.model.FyGoods;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.zhiwee.gree.model.GoodsInfo.Goods;
import xyz.icrab.common.util.jackson.CustomJsonDateDeserializer;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Table(name="t_fygoods")
public class Fygoods implements Serializable {

    @Id
    private String id;

    private String goodsName;

    private BigDecimal price;

    private BigDecimal killPrice;

    private String cover;

    private Integer  limitCount;

    private Integer storeCount;

    private Integer iskill;
    private String goodsId;
    private String liveUrl;
    @Transient
    private String    skillId;
    @Transient
    private Goods goods;
    @Transient
    private String    describes;;
    @Transient
    private  String categoryId;
    @Transient
    private    String categoryName;
    @Transient
    private  String   brandName;
    @Transient
    private String brandId;
    @Transient
    private Integer buyBun;

    @Transient
    private String goodCover;
    public String getSkillId() {
        return skillId;
    }

    public void setSkillId(String skillId) {
        this.skillId = skillId;
    }

    @JsonDeserialize(using = CustomJsonDateDeserializer.class)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date killStartTime;

    @JsonDeserialize(using = CustomJsonDateDeserializer.class)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private  Date killEndTime;

    @JsonDeserialize(using = CustomJsonDateDeserializer.class)
    @Transient
    private   Date date;



   // @JsonDeserialize(using = CustomJsonDateDeserializer.class)
    // private  Date   killStartTime2;

  //  @JsonDeserialize(using = CustomJsonDateDeserializer.class)
   //  private  Date  killEndTime2;


    private Integer state;

    public Goods getGoods() {
        return goods;
    }

    public void setGoods(Goods goods) {
        this.goods = goods;
    }

    public String getId() {
        return id;
    }

    public Integer getBuyBun() {
        return buyBun;
    }

    public void setBuyBun(Integer buyBun) {
        this.buyBun = buyBun;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getKillPrice() {
        return killPrice;
    }

    public void setKillPrice(BigDecimal killPrice) {
        this.killPrice = killPrice;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public Integer getLimitCount() {
        return limitCount;
    }

    public void setLimitCount(Integer limitCount) {
        this.limitCount = limitCount;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getStoreCount() {
        return storeCount;
    }

    public void setStoreCount(Integer storeCount) {
        this.storeCount = storeCount;
    }

    public Integer getIskill() {
        return iskill;
    }

    public void setIskill(Integer iskill) {
        this.iskill = iskill;
    }


    public Date getKillStartTime() {
        return killStartTime;
    }

    public void setKillStartTime(Date killStartTime) {
        this.killStartTime = killStartTime;
    }

    public Date getKillEndTime() {
        return killEndTime;
    }

    public void setKillEndTime(Date killEndTime) {
        this.killEndTime = killEndTime;
    }

    public String getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getDescribes() {
        return describes;
    }

    public void setDescribes(String describes) {
        this.describes = describes;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getBrandId() {
        return brandId;
    }

    public String getGoodCover() {
        return goodCover;
    }

    public void setGoodCover(String goodCover) {
        this.goodCover = goodCover;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getLiveUrl() {
        return liveUrl;
    }

    public void setLiveUrl(String liveUrl) {
        this.liveUrl = liveUrl;
    }
}