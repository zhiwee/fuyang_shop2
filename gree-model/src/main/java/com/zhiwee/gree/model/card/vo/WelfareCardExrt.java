package com.zhiwee.gree.model.card.vo;

import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecgframework.poi.excel.annotation.ExcelTarget;

import java.util.Date;

/**
 * TODO
 *
 * @author Chensong
 * @Company http://www.zhiwee.com
 * date 2020-02-22
 */
@ExcelTarget("BookingCardsExrt")
public class WelfareCardExrt {
    //状态
    //-1：超时未支付，0：待支付，1：已支付,未领取，2：已退款 3 已经领取
    @Excel(name = "状态", replace ={"未领取_0","已领取_1"})
    private Integer state;

    //商品名称
    @Excel(name = "用户名称", needMerge = true)
    private String userName;
    //客户手机号
    @Excel(name = "客户手机号", needMerge = true)
    private String phone;

    //商品详情
    @Excel(name = "卡号", needMerge = true)
    private String cardCode;

    //支付时间
    @Excel(name = "领取时间", needMerge = true,exportFormat = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    //阜阳商厦推荐号
    @Excel(name = "阜阳商厦推荐号", needMerge = true)
    private String fyCode;

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCardCode() {
        return cardCode;
    }

    public void setCardCode(String cardCode) {
        this.cardCode = cardCode;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getFyCode() {
        return fyCode;
    }

    public void setFyCode(String fyCode) {
        this.fyCode = fyCode;
    }
}
