package com.zhiwee.gree.model.HomePage;



import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author sun on 2019/5/6
 */
public class HomeContent  implements Serializable {

    private List<HomePage>   rotation = new ArrayList<>();

    private List<HomePage>   infomation = new ArrayList<>();

    public List<HomePage> getRotation() {
        return rotation;
    }

    public void setRotation(List<HomePage> rotation) {
        this.rotation = rotation;
    }

    public List<HomePage> getInfomation() {
        return infomation;
    }

    public void setInfomation(List<HomePage> infomation) {
        this.infomation = infomation;
    }
}
