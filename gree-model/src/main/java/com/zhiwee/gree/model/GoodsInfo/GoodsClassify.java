package com.zhiwee.gree.model.GoodsInfo;

import xyz.icrab.common.model.BaseModel;
import xyz.icrab.common.model.annotation.Like;
import xyz.icrab.common.model.type.OrderBy;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 商品分类实体
 * @version 1.0
 */
@Table(name="t_goods_classify")
//@Table(name="s_goods_classify")
public class GoodsClassify extends BaseModel{
    // 上级分类 顶级分类默认为ROOT
    @Id
    private String id;
    //
    private String parentID;
    @Transient
    private String parentName;
    // IdPath 以英文逗号分隔默认顶级分类ID为ROOT
    @Like
    private String idPath;
    // 分类名称
    @Like
    private String name;
    // 状态1：正常 2、停用 3、删除
    private Integer state;
    // 说明
    private String detailed;

    @OrderBy(order=1)
    private Integer sort;

    private String code;

    private String sign;

    @Transient
    private String areaName;

    @Transient
    private List<GoodsClassify> children = new ArrayList<>();


    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public List<GoodsClassify> getChildren() {
        return children;
    }

    public void setChildren(List<GoodsClassify> children) {
        this.children = children;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    /**
     * 获取上级分类 顶级分类默认为ROOT
     */
    public String getId()
    {
        return id;
    }
    /**
     * 设置上级分类 顶级分类默认为ROOT
     */
    public void setId(String id)
    {
        this.id = id;
    }
    /**
     * 获取
     */
    public String getParentID()
    {
        return parentID;
    }
    /**
     * 设置
     */
    public void setParentID(String parentID)
    {
        this.parentID = parentID;
    }
    /**
     * 获取IdPath 以英文逗号分隔默认顶级分类ID为ROOT
     */
    public String getIdPath()
    {
        return idPath;
    }
    /**
     * 设置IdPath 以英文逗号分隔默认顶级分类ID为ROOT
     */
    public void setIdPath(String idPath)
    {
        this.idPath = idPath;
    }
    /**
     * 获取分类名称
     */
    public String getName()
    {
        return name;
    }
    /**
     * 设置分类名称
     */
    public void setName(String name)
    {
        this.name = name;
    }
    /**
     * 获取状态1：正常 2、停用 3、删除
     */
    public Integer getState()
    {
        return state;
    }
    /**
     * 设置状态1：正常 2、停用 3、删除
     */
    public void setState(Integer state)
    {
        this.state = state;
    }
    /**
     * 获取说明
     */
    public String getDetailed()
    {
        return detailed;
    }
    /**
     * 设置说明
     */
    public void setDetailed(String detailed)
    {
        this.detailed = detailed;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }






}
