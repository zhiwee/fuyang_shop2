package com.zhiwee.gree.model.ShopUser;

public class ShopUserCy {
    private String id;
    private Long times;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getTimes() {
        return times;
    }

    public void setTimes(Long times) {
        this.times = times;
    }
}
