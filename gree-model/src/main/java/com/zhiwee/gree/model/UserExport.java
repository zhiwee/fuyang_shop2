package com.zhiwee.gree.model;

import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;

public class UserExport implements Serializable {
    @Excel(name = "登录名")
    private String userName;

    @Excel(name = "密码")
    private String password;

    @Excel(name = "真实姓名")
    private String nickname;

    @Excel(name="角色")
    private String roles;

    @Excel(name="配送商名称")
    private String distributorName;

    @Excel(name = "用户类型",replace={"超级管理员_1","系统用户_2","配送商用户_3"})
    private String type;

    @Excel(name = "状态",replace={"启用_1","禁用_2"})
    private String state;


    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDistributorName() {
        return distributorName;
    }

    public void setDistributorName(String distributorName) {
        this.distributorName = distributorName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }
}
