package com.zhiwee.gree.model.card.vo;


import java.util.List;

public class CheckCard {
    private String id;
    private List<CheckCY> list;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<CheckCY> getList() {
        return list;
    }

    public void setList(List<CheckCY> list) {
        this.list = list;
    }
}
