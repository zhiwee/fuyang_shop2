package com.zhiwee.gree.model.HomePage;


import xyz.icrab.common.model.annotation.Like;

import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "s_page_channel")
public class PageChannel {

    @Id
    private String id;

    @Like
    private String name;

    private Integer state;

    private Integer type;

    private String remake;

    private Integer thisSort;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getRemake() {
        return remake;
    }

    public void setRemake(String remake) {
        this.remake = remake;
    }

    public Integer getThisSort() {
        return thisSort;
    }

    public void setThisSort(Integer thisSort) {
        this.thisSort = thisSort;
    }
}
