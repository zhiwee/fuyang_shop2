package com.zhiwee.gree.model.Alipay;

import java.io.Serializable;

/**
 * @author sun on 2019/5/13
 */
public class AliRefund implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 订单号
     */
    private String out_trade_no;
    /**
     * 需要退款的金额，该金额不能大于订单金额,单位为元，支持两位小数
     */
    private String refund_amount;
    /**
     *  退款的原因说明
     */
    private String refund_reason;

    /**
     * 退款密码
     */
    public String refundPassword;

    /**
     * 不是全额退款，部分退款必须使用该参数，同一个订单，不同的out_request_no代表部分退款
     */
    public String out_request_no;

    public String getOut_request_no() {
        return out_request_no;
    }

    public void setOut_request_no(String out_request_no) {
        this.out_request_no = out_request_no;
    }

    public String getRefundPassword() {
        return refundPassword;
    }

    public void setRefundPassword(String refundPassword) {
        this.refundPassword = refundPassword;
    }

    public String getOut_trade_no() {
        return out_trade_no;
    }

    public void setOut_trade_no(String out_trade_no) {
        this.out_trade_no = out_trade_no;
    }

    public String getRefund_amount() {
        return refund_amount;
    }

    public void setRefund_amount(String refund_amount) {
        this.refund_amount = refund_amount;
    }

    public String getRefund_reason() {
        return refund_reason;
    }

    public void setRefund_reason(String refund_reason) {
        this.refund_reason = refund_reason;
    }
}
