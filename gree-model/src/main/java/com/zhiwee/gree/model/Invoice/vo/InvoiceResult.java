package com.zhiwee.gree.model.Invoice.vo;

import com.zhiwee.gree.model.Invoice.Invoice;

import java.io.Serializable;
import java.util.List;

/**
 * Created by jick on 2019/7/16.
 */
public class InvoiceResult implements Serializable {
    private  Object object;
    private List<Invoice> list;

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }

    public List<Invoice> getList() {
        return list;
    }

    public void setList(List<Invoice> list) {
        this.list = list;
    }
}
