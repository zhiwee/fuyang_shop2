package com.zhiwee.gree.model.card;

import java.io.Serializable;

/**
 * TODO
 *
 * @author Chensong
 * @Company http://www.zhiwee.com
 * date 2019-12-15
 */
public class UseCouponBean implements Serializable {

    private String id ;
    private Double costMoney ;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getCostMoney() {
        return costMoney;
    }

    public void setCostMoney(double costMoney) {
        this.costMoney = costMoney;
    }
}
