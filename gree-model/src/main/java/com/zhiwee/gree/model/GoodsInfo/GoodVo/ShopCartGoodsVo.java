package com.zhiwee.gree.model.GoodsInfo.GoodVo;

import com.zhiwee.gree.model.GoodsInfo.ShopCartGoods;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class ShopCartGoodsVo implements Serializable {

    private List<ShopCartGoods> cartGoodsList;

    private BigDecimal allPrice;

    public List<ShopCartGoods> getCartGoodsList() {
        return cartGoodsList;
    }

    public void setCartGoodsList(List<ShopCartGoods> cartGoodsList) {
        this.cartGoodsList = cartGoodsList;
    }

    public BigDecimal getAllPrice() {
        return allPrice;
    }

    public void setAllPrice(BigDecimal allPrice) {
        this.allPrice = allPrice;
    }
}
