package com.zhiwee.gree.model.FyGoods.vo;

import com.zhiwee.gree.model.FyGoods.FygoodsCart;
import com.zhiwee.gree.model.OrderInfo.OrderAddress;

import javax.persistence.criteria.CriteriaBuilder;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


public class FygoodsCartVo implements Serializable {

   // private BigDecimal allPay

    private List<FygoodsCart>  list;

    private   BigDecimal      payAll;
    //折扣
    private   BigDecimal  discount;
    //佣金
    private   BigDecimal  activityCost;
    // 1 使用佣金  2 未使用
    private   Integer  costType;
    //认领时间
    private  Date  claimTime;
    //1 普通下单 2 直播下单 3秒杀下单 4拼团下单
    private Integer type;
    //1 代表购物车
    private   Integer byCar;
    private  String  couponsId;
    private  String  giftCardUser;
    //推荐号
    private  String  fyCode;
    //分享者ID
    private  String  shareId;
    //地址
    private OrderAddress orderAddress;


    public String getShareId() {
        return shareId;
    }

    public void setShareId(String shareId) {
        this.shareId = shareId;
    }

    public BigDecimal getActivityCost() {
        return activityCost;
    }

    public void setActivityCost(BigDecimal activityCost) {
        this.activityCost = activityCost;
    }

    public String getFyCode() {
        return fyCode;
    }

    public void setFyCode(String fyCode) {
        this.fyCode = fyCode;
    }

    public String getCouponsId() {
        return couponsId;
    }

    public void setCouponsId(String couponsId) {
        this.couponsId = couponsId;
    }

    public List<FygoodsCart> getList() {
        return list;
    }

    public void setList(List<FygoodsCart> list) {
        this.list = list;
    }

    public BigDecimal getPayAll() {
        return payAll;
    }

    public void setPayAll(BigDecimal payAll) {
        this.payAll = payAll;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getGiftCardUser() {
        return giftCardUser;
    }

    public void setGiftCardUser(String giftCardUser) {
        this.giftCardUser = giftCardUser;
    }

    public Integer getByCar() {
        return byCar;
    }

    public void setByCar(Integer byCar) {
        this.byCar = byCar;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public Date getClaimTime() {
        return claimTime;
    }

    public void setClaimTime(Date claimTime) {
        this.claimTime = claimTime;
    }

    public Integer getCostType() {
        return costType;
    }

    public void setCostType(Integer costType) {
        this.costType = costType;
    }

    public OrderAddress getOrderAddress() {
        return orderAddress;
    }

    public void setOrderAddress(OrderAddress orderAddress) {
        this.orderAddress = orderAddress;
    }
}
