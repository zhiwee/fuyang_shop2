package com.zhiwee.gree.model.FyGoods;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.LineNumberReader;
import java.io.Serializable;
import java.math.BigDecimal;

@Table(name = "t_fygoodsCart")
public class FygoodsCart implements Serializable ,Comparable<FygoodsCart >{

    @Id
    private String id;
    @Transient
    private String skillId;

    private String userId;

    private String phoneNum;

    private String goodsId;
    private String dealerId;
    private String dealerName;

    private  String  goodsName;

    private  String  cover;


    private BigDecimal price;
    private BigDecimal giftPrice;
    @Transient
    private BigDecimal  discount;
    @Transient
    private BigDecimal  activityCost;
    private Integer  count;
    private Integer  sendType;

    private String fyGoodsId;
    private String brandName;
    private String brandUrl;
    //单价乘数量
    private  BigDecimal totalPrice;
    private  Integer iskill;
    private  Integer type;
    @Transient
    private    String   orderNum;


    public Integer getSendType() {
        return sendType;
    }

    public void setSendType(Integer sendType) {
        this.sendType = sendType;
    }


    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    @Transient
    private  String label1id ;
    @Transient
    private  String label1parentId ;
    @Transient
    private  String label1parentName ;
    @Transient
    private  String label2parentName ;
    @Transient
    private  String label3parentName ;
    @Transient
    private  String label2parentId;
    @Transient
    private  String label3parentId ;
    @Transient
    private  String label2id ;
    @Transient
    private  String label3id ;
    @Transient
    private  String label1value;
    @Transient
    private  String label2value;
    @Transient
    private  String label3value;
    @Transient
    private  Integer jiadian ;


    public BigDecimal getActivityCost() {
        return activityCost;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getBrandUrl() {
        return brandUrl;
    }

    public void setBrandUrl(String brandUrl) {
        this.brandUrl = brandUrl;
    }

    public void setActivityCost(BigDecimal activityCost) {
        this.activityCost = activityCost;
    }

    public BigDecimal getGiftPrice() {
        return giftPrice;
    }

    public void setGiftPrice(BigDecimal giftPrice) {
        this.giftPrice = giftPrice;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getLabel1id() {
        return label1id;
    }

    public Integer getJiadian() {
        return jiadian;
    }

    public void setJiadian(Integer jiadian) {
        this.jiadian = jiadian;
    }

    public void setLabel1id(String label1id) {
        this.label1id = label1id;
    }

    public String getLabel1parentId() {
        return label1parentId;
    }

    public void setLabel1parentId(String label1parentId) {
        this.label1parentId = label1parentId;
    }

    public String getLabel1parentName() {
        return label1parentName;
    }

    public void setLabel1parentName(String label1parentName) {
        this.label1parentName = label1parentName;
    }

    public String getLabel2parentName() {
        return label2parentName;
    }

    public void setLabel2parentName(String label2parentName) {
        this.label2parentName = label2parentName;
    }

    public String getLabel3parentName() {
        return label3parentName;
    }

    public void setLabel3parentName(String label3parentName) {
        this.label3parentName = label3parentName;
    }

    public String getLabel2parentId() {
        return label2parentId;
    }

    public void setLabel2parentId(String label2parentId) {
        this.label2parentId = label2parentId;
    }

    public String getLabel3parentId() {
        return label3parentId;
    }

    public void setLabel3parentId(String label3parentId) {
        this.label3parentId = label3parentId;
    }

    public String getLabel2id() {
        return label2id;
    }

    public void setLabel2id(String label2id) {
        this.label2id = label2id;
    }

    public String getLabel3id() {
        return label3id;
    }

    public void setLabel3id(String label3id) {
        this.label3id = label3id;
    }

    public String getLabel1value() {
        return label1value;
    }

    public void setLabel1value(String label1value) {
        this.label1value = label1value;
    }

    public String getLabel2value() {
        return label2value;
    }

    public void setLabel2value(String label2value) {
        this.label2value = label2value;
    }

    public String getLabel3value() {
        return label3value;
    }

    public void setLabel3value(String label3value) {
        this.label3value = label3value;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public String getFyGoodsId() {
        return fyGoodsId;
    }


    public String getDealerId() {
        return dealerId;
    }

    public void setDealerId(String dealerId) {
        this.dealerId = dealerId;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public void setFyGoodsId(String fyGoodsId) {
        this.fyGoodsId = fyGoodsId;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    public String getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }


    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Integer getIskill() {
        return iskill;
    }

    public void setIskill(Integer iskill) {
        this.iskill = iskill;
    }

    public String getSkillId() {
        return skillId;
    }

    public void setSkillId(String skillId) {
        this.skillId = skillId;
    }

    public String getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }

    @Override
    public int compareTo(FygoodsCart o) {

     int  i =   this.getPrice().subtract(o.getPrice()).intValue();

         Long   id1 =  Long.parseLong(this.getId());
         Long  id2 =  Long.parseLong(o.getId());

         if(id1>id2){
             return 1;
         }else {
             return  -1;
         }



    }


    @Override
    public String toString() {
        return "FygoodsCart{" +
                "id='" + id + '\'' +
                ", userId='" + userId + '\'' +
                ", phoneNum='" + phoneNum + '\'' +
                ", goodsId='" + goodsId + '\'' +
                ", goodsName='" + goodsName + '\'' +
                ", cover='" + cover + '\'' +
                ", price=" + price +
                ", count=" + count +
                ", totalPrice=" + totalPrice +
                ", iskill=" + iskill +
                ", orderNum='" + orderNum + '\'' +
                '}';
    }
}