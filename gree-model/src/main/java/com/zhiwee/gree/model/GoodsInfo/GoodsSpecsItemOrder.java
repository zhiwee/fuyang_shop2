package com.zhiwee.gree.model.GoodsInfo;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.math.BigDecimal;

@Table(name = "s_goods_specsitem_order")
public class GoodsSpecsItemOrder {

    @Id
    private   String  id;  //id

    private  String goodsNum;  //编码

    private  String  dealerGoodsId;  //会销系统商品id

    @Transient
    private String dealerGoodsName;  //会销系统商品名称

    private String goodsId;  //商城商品id

    private String name;    //规格名

    private BigDecimal activePrice; //活动价

    private    BigDecimal shopPrice; //门店价

    private   BigDecimal marketPrice;//市场价

    private    BigDecimal costPrice; //成本价

    private   BigDecimal   innerPrice; //员工内部价

    private   BigDecimal   firstPrice; //一级代理价

    private   BigDecimal   secondPrice; //二级代理价

    private   BigDecimal   distributePrice;  //分销价

    private    BigDecimal  profit;  //经销商利润

    private   Integer  storeCount;  //库存

    private   Integer  saleCount;  //销量

    private    BigDecimal commission;  //佣金

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGoodsNum() {
        return goodsNum;
    }

    public void setGoodsNum(String goodsNum) {
        this.goodsNum = goodsNum;
    }

    public String getDealerGoodsId() {
        return dealerGoodsId;
    }

    public void setDealerGoodsId(String dealerGoodsId) {
        this.dealerGoodsId = dealerGoodsId;
    }

    public String getDealerGoodsName() {
        return dealerGoodsName;
    }

    public void setDealerGoodsName(String dealerGoodsName) {
        this.dealerGoodsName = dealerGoodsName;
    }

    public String getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getActivePrice() {
        return activePrice;
    }

    public void setActivePrice(BigDecimal activePrice) {
        this.activePrice = activePrice;
    }

    public BigDecimal getShopPrice() {
        return shopPrice;
    }

    public void setShopPrice(BigDecimal shopPrice) {
        this.shopPrice = shopPrice;
    }

    public BigDecimal getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(BigDecimal marketPrice) {
        this.marketPrice = marketPrice;
    }

    public BigDecimal getCostPrice() {
        return costPrice;
    }

    public void setCostPrice(BigDecimal costPrice) {
        this.costPrice = costPrice;
    }

    public BigDecimal getInnerPrice() {
        return innerPrice;
    }

    public void setInnerPrice(BigDecimal innerPrice) {
        this.innerPrice = innerPrice;
    }

    public BigDecimal getFirstPrice() {
        return firstPrice;
    }

    public void setFirstPrice(BigDecimal firstPrice) {
        this.firstPrice = firstPrice;
    }

    public BigDecimal getSecondPrice() {
        return secondPrice;
    }

    public void setSecondPrice(BigDecimal secondPrice) {
        this.secondPrice = secondPrice;
    }

    public BigDecimal getDistributePrice() {
        return distributePrice;
    }

    public void setDistributePrice(BigDecimal distributePrice) {
        this.distributePrice = distributePrice;
    }

    public BigDecimal getProfit() {
        return profit;
    }

    public void setProfit(BigDecimal profit) {
        this.profit = profit;
    }

    public Integer getStoreCount() {
        return storeCount;
    }

    public void setStoreCount(Integer storeCount) {
        this.storeCount = storeCount;
    }

    public Integer getSaleCount() {
        return saleCount;
    }

    public void setSaleCount(Integer saleCount) {
        this.saleCount = saleCount;
    }

    public BigDecimal getCommission() {
        return commission;
    }

    public void setCommission(BigDecimal commission) {
        this.commission = commission;
    }
}
