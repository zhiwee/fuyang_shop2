package com.zhiwee.gree.model.OrderInfo;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.zhiwee.gree.model.ShopUserAddress.ShopUserAddress;
import xyz.icrab.common.util.jackson.CustomJsonDateDeserializer;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
/**
 * @author sun on 2019/7/9
 */
@Table(name="s_change_order")
public class OrderChange implements  Serializable{

    @Id
    private String id ;

    /**
     *记录流水号
     */
    private String orderNum;

    /**
     *订单id
     */
    private String orderId;

    /**
     *原来订单金额
     */
    private Double oldRealPrice;
    /**
     *现在订单金额
     */
    private Double newRealPrice;
    /**
     *原来的商品id
     */

    private String oldGood;

    /**
     *现在的商品id
     */
    private String newGood;
    /**
     *原来的数量
     */

    private Integer oldNum;

    /**
     *现在的数量
     */
    private Integer newNum;

    @JsonDeserialize(
            using = CustomJsonDateDeserializer.class
    )
    private Date createTime;


    @JsonDeserialize(
            using = CustomJsonDateDeserializer.class
    )
    private Date payTime;
    /**
     * 差价
     * 正数为 系统需要补给客户的钱
     *
     * 负数 是客户需要补给系统的钱
     */
    private Double changePrice;


    /**
     * 换货订单 状态
     *
     * orderState
     */
    private Integer orderState;


    /**订单补充余额的方式
     *1 支付宝  2 微信  3银联 4 转账 5 余额
     */
    private Integer payType;


    /**
     * 订单明细id
     */
    private String orderItemId;


    /**
     * 用户id
     */
    private String userId;



    /**
     * 申请记录id
     */
    private String refundId;

    /**
     * 原来商品名
     */
    @Transient
    private  String oldGoodName;
    /**
     * 现在商品名
     */
    @Transient
    private  String newGoodName;


    public String getRefundId() {
        return refundId;
    }

    public void setRefundId(String refundId) {
        this.refundId = refundId;
    }

    public Date getPayTime() {
        return payTime;
    }

    public void setPayTime(Date payTime) {
        this.payTime = payTime;
    }

    public String getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }

    public Integer getPayType() {
        return payType;
    }

    public void setPayType(Integer payType) {
        this.payType = payType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Double getOldRealPrice() {
        return oldRealPrice;
    }

    public void setOldRealPrice(Double oldRealPrice) {
        this.oldRealPrice = oldRealPrice;
    }

    public Double getNewRealPrice() {
        return newRealPrice;
    }

    public void setNewRealPrice(Double newRealPrice) {
        this.newRealPrice = newRealPrice;
    }

    public String getOldGood() {
        return oldGood;
    }

    public void setOldGood(String oldGood) {
        this.oldGood = oldGood;
    }

    public String getNewGood() {
        return newGood;
    }

    public void setNewGood(String newGood) {
        this.newGood = newGood;
    }

    public Integer getOldNum() {
        return oldNum;
    }

    public void setOldNum(Integer oldNum) {
        this.oldNum = oldNum;
    }

    public Integer getNewNum() {
        return newNum;
    }

    public void setNewNum(Integer newNum) {
        this.newNum = newNum;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Double getChangePrice() {
        return changePrice;
    }

    public void setChangePrice(Double changePrice) {
        this.changePrice = changePrice;
    }

    public Integer getOrderState() {
        return orderState;
    }

    public void setOrderState(Integer orderState) {
        this.orderState = orderState;
    }

    public String getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(String orderItemId) {
        this.orderItemId = orderItemId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOldGoodName() {
        return oldGoodName;
    }

    public void setOldGoodName(String oldGoodName) {
        this.oldGoodName = oldGoodName;
    }

    public String getNewGoodName() {
        return newGoodName;
    }

    public void setNewGoodName(String newGoodName) {
        this.newGoodName = newGoodName;
    }
}
