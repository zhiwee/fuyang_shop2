package com.zhiwee.gree.model.BaseInfo;


import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author sun on 2019/5/10
 */
@Table(name="s_base_info")
public class BaseInfo implements Serializable {

    @Id
    private String id;
// 分享赠送积分
    private Double sendBalance;


    private String refundPassword;


    private Integer type;


    private Integer sendIntegral;

//    @JsonDeserialize(
//            using = CustomJsonDateDeserializer.class
//    )
//    private Date specialEndTime;


    private  String initPassword;



    private  Double realPrice;



    private  Integer couponNum;



//是否允许直接体现 1 允许  2 不允许
    private Integer isApply;


    public Double getSendBalance() {
        return sendBalance;
    }

    public void setSendBalance(Double sendBalance) {
        this.sendBalance = sendBalance;
    }

    public Integer getIsApply() {
        return isApply;
    }

    public void setIsApply(Integer isApply) {
        this.isApply = isApply;
    }

    public Integer getCouponNum() {
        return couponNum;
    }

    public void setCouponNum(Integer couponNum) {
        this.couponNum = couponNum;
    }

    public Double getRealPrice() {
        return realPrice;
    }

    public void setRealPrice(Double realPrice) {
        this.realPrice = realPrice;
    }

    public String getInitPassword() {
        return initPassword;
    }

    public void setInitPassword(String initPassword) {
        this.initPassword = initPassword;
    }

//    public Date getSpecialEndTime() {
//        return specialEndTime;
//    }
//
//    public void setSpecialEndTime(Date specialEndTime) {
//        this.specialEndTime = specialEndTime;
//    }

    public Integer getSendIntegral() {
        return sendIntegral;
    }

    public void setSendIntegral(Integer sendIntegral) {
        this.sendIntegral = sendIntegral;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

//    public String getSystemPhone() {
//        return systemPhone;
//    }
//
//    public void setSystemPhone(String systemPhone) {
//        this.systemPhone = systemPhone;
//    }

    public String getRefundPassword() {
        return refundPassword;
    }

    public void setRefundPassword(String refundPassword) {
        this.refundPassword = refundPassword;
    }
}
