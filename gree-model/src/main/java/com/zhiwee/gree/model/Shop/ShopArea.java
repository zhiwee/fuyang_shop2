package com.zhiwee.gree.model.Shop;

import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author jiake
 * @date 2020-02-19 12:12
 */
@Table(name = "t_area")
public class ShopArea {

    @Id
    private Integer id;

    private String name;

    private Integer parentId;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }
}
