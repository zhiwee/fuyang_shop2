package com.zhiwee.gree.model.order;

import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Table(name = "order_base_info")
public class BaseOrder implements Serializable {
    @Id
    private  String id;

    //订单号
    private  String orderNum;

    //总金额
    private BigDecimal totalPrice;

    //实际支付金额
    private  BigDecimal realPay;

    //1 使用优惠券 2 使用代金券 3  不使用
    private  Integer Coupon;

    //折扣金额
    private BigDecimal discount;

    //付款人id
    private  String  payerId;

    //订单创建时间
    private Date createTime;

    //状态  1、表示未支付 2表示已支付
    private  Integer state;

    //积分
    private double integral;

    //订单类型 1表示普通订单 2 表示分销订单
    private  Integer type;

    //分销员id
    private String distributorId;

    //收货地址id
    private String addressId;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public BigDecimal getRealPay() {
        return realPay;
    }

    public void setRealPay(BigDecimal realPay) {
        this.realPay = realPay;
    }

    public Integer getCoupon() {
        return Coupon;
    }

    public void setCoupon(Integer coupon) {
        Coupon = coupon;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public String getPayerId() {
        return payerId;
    }

    public void setPayerId(String payerId) {
        this.payerId = payerId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public double getIntegral() {
        return integral;
    }

    public void setIntegral(double integral) {
        this.integral = integral;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getDistributorId() {
        return distributorId;
    }

    public void setDistributorId(String distributorId) {
        this.distributorId = distributorId;
    }

    public String getAddressId() {
        return addressId;
    }

    public void setAddressId(String addressId) {
        this.addressId = addressId;
    }
}
