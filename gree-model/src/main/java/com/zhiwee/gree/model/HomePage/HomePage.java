package com.zhiwee.gree.model.HomePage;

import javax.persistence.Id;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import xyz.icrab.common.model.type.OrderBy;
import xyz.icrab.common.util.jackson.CustomJsonDateDeserializer;

import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Date;

/**
 * @author sun on 2019/5/6
 */
@Table(name="s_shop_homepage")
public class HomePage implements Serializable {

    @Id
    private  String id;

    private String  picture;

    private Integer state;

    private String url;

    @JsonDeserialize(
            using = CustomJsonDateDeserializer.class
    )
    private Date createtime;

    @JsonDeserialize(
            using = CustomJsonDateDeserializer.class
    )
    private Date  updatetime;

    private String createMan;

    private String  updateMan;

    // 1pc端的轮播图 2 手机端的轮播图
    private Integer type;


    @OrderBy(OrderBy.Sort.ASC)
    private Integer sort;

    private String name;



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public Date getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Date updatetime) {
        this.updatetime = updatetime;
    }

    public String getCreateMan() {
        return createMan;
    }

    public void setCreateMan(String createMan) {
        this.createMan = createMan;
    }

    public String getUpdateMan() {
        return updateMan;
    }

    public void setUpdateMan(String updateMan) {
        this.updateMan = updateMan;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
