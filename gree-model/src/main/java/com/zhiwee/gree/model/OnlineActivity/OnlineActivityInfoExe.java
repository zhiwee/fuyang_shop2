package com.zhiwee.gree.model.OnlineActivity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.zhiwee.gree.model.enums.OnlineActivityType;
import org.jeecgframework.poi.excel.annotation.Excel;
import xyz.icrab.common.model.annotation.Like;
import xyz.icrab.common.model.type.OrderBy;
import xyz.icrab.common.util.jackson.CustomJsonDateDeserializer;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Date;


/**
 * @author tzj
 * @description
 * @date 2019/9/24 22:07
 * @return
*/
public class OnlineActivityInfoExe implements Serializable {

//    @Excel(name = "是否使用",replace={"未使用_1","已使用_2","已失效_3"})
//    private Integer couponState;


    @Excel(name = "活动ID")
    private String id;

    @Excel(name = "活动名称")
    private String name;

    @Excel(name = "活动描述")
    private String description;

    @Excel(name = "开始时间", needMerge = true,exportFormat = "yyyy-MM-dd HH:mm:ss")
    private Date beginTime;

    @Excel(name = "结束时间", needMerge = true,exportFormat = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;
    @Excel(name = "是否启用",replace={"启用_1","未启用_2"})
    private Integer state;
    @Excel(name = "活动类型",replace={"秒杀_1","内购会_2","团购_3"})
    private OnlineActivityType activityType;

    @Excel(name = "创建人")
    private String createman;
    @Excel(name = "更新人")
    private String updateman;
    @Excel(name = "创建时间", needMerge = true,exportFormat = "yyyy-MM-dd HH:mm:ss")
    private Date createtime;
    @Excel(name = "更新时间", needMerge = true,exportFormat = "yyyy-MM-dd HH:mm:ss")
    private Date updatetime;
    //线下活动ID

    @Excel(name = "关联线下活动")
    private String activityName;
    //是否可以使用优惠券
    @Excel(name = "否可以使用优惠券",replace={"可以_1","不可以_2"})
    private Integer canCoupon;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(Date beginTime) {
        this.beginTime = beginTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public OnlineActivityType getActivityType() {
        return activityType;
    }

    public void setActivityType(OnlineActivityType activityType) {
        this.activityType = activityType;
    }

    public String getUpdateman() {
        return updateman;
    }

    public void setUpdateman(String updateman) {
        this.updateman = updateman;
    }

    public String getCreateman() {
        return createman;
    }

    public void setCreateman(String createman) {
        this.createman = createman;
    }

    public Date getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Date updatetime) {
        this.updatetime = updatetime;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }



    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public Integer getCanCoupon() {
        return canCoupon;
    }

    public void setCanCoupon(Integer canCoupon) {
        this.canCoupon = canCoupon;
    }

}
