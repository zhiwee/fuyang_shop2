package com.zhiwee.gree.model.card;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecgframework.poi.excel.annotation.ExcelTarget;
import xyz.icrab.common.util.jackson.CustomJsonDateDeserializer;

import java.util.Date;

/**
 * TODO
 *
 * @author Chensong
 * @Company http://www.zhiwee.com
 * date 2020-01-10
 */
@ExcelTarget("LShopUserCardWxPayInfoExrt")
public class LShopUserCardWxPayInfoExrt {

    @Excel(name = "订单状态", replace ={"已失效_-1","待支付_1","已支付_2","已退款_3"})
    private Integer state;
    @Excel(name = "顾客姓名", needMerge = true)
    private String username;
    @Excel(name = "顾客电话", needMerge = true)
    private String phone;
    @Excel(name = "订单名称", needMerge = true)
    private String goodName;
    @Excel(name = "订单详情", needMerge = true)
    private String goodDetail;
    @Excel(name = "购买时间", needMerge = true,exportFormat = "yyyy-MM-dd HH:mm:ss")
    private Date createtime;

    @Excel(name = "订单号", needMerge = true)
    private String outTradeNo;
    @Excel(name = "支付金额（精确到分）", needMerge = true)
    private String totalFee;
    @Excel(name = "支付时间", needMerge = true,exportFormat = "yyyy-MM-dd HH:mm:ss")
    private Date paytime;

    @Excel(name = "退款单号", needMerge = true)
    private String outRefundNo;
    @Excel(name = "退款金额（精确到分）", needMerge = true)
    private String refundFee;
    @Excel(name = "退款时间", needMerge = true,exportFormat = "yyyy-MM-dd HH:mm:ss")
    private Date canceltime;

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getGoodName() {
        return goodName;
    }

    public void setGoodName(String goodName) {
        this.goodName = goodName;
    }

    public String getGoodDetail() {
        return goodDetail;
    }

    public void setGoodDetail(String goodDetail) {
        this.goodDetail = goodDetail;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public String getOutTradeNo() {
        return outTradeNo;
    }

    public void setOutTradeNo(String outTradeNo) {
        this.outTradeNo = outTradeNo;
    }

    public String getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(String totalFee) {
        this.totalFee = totalFee;
    }

    public Date getPaytime() {
        return paytime;
    }

    public void setPaytime(Date paytime) {
        this.paytime = paytime;
    }

    public String getOutRefundNo() {
        return outRefundNo;
    }

    public void setOutRefundNo(String outRefundNo) {
        this.outRefundNo = outRefundNo;
    }

    public String getRefundFee() {
        return refundFee;
    }

    public void setRefundFee(String refundFee) {
        this.refundFee = refundFee;
    }

    public Date getCanceltime() {
        return canceltime;
    }

    public void setCanceltime(Date canceltime) {
        this.canceltime = canceltime;
    }
}
