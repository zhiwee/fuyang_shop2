package com.zhiwee.gree.model.card;

import xyz.icrab.common.model.BaseModel;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Table(name="s_card_info")
public class CardInfo extends BaseModel {
    @Id
    private String id;
    private String name;
    private String categoryIds;
    private String categoryCodes;
    private String departmentIds;
    private String departmentCodes;
    //最小金额
    private Double minMoney;
    //抵用金额
    private Integer useMoney;

    @Transient
    private String categoryNames;

    @Transient
    private String departmentNames;


    public String getDepartmentCodes() {
        return departmentCodes;
    }

    public void setDepartmentCodes(String departmentCodes) {
        this.departmentCodes = departmentCodes;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getCategoryCodes() {
        return categoryCodes;
    }

    public void setCategoryCodes(String categoryCodes) {
        this.categoryCodes = categoryCodes;
    }

    public String getDepartmentIds() {
        return departmentIds;
    }

    public void setDepartmentIds(String departmentIds) {
        this.departmentIds = departmentIds;
    }

    public String getDepartmentNames() {
        return departmentNames;
    }

    public void setDepartmentNames(String departmentNames) {
        this.departmentNames = departmentNames;
    }

    public Integer getUseMoney() {
        return useMoney;
    }

    public void setUseMoney(Integer useMoney) {
        this.useMoney = useMoney;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategoryIds() {
        return categoryIds;
    }

    public void setCategoryIds(String categoryIds) {
        this.categoryIds = categoryIds;
    }

    public String getCategoryNames() {
        return categoryNames;
    }

    public void setCategoryNames(String categoryNames) {
        this.categoryNames = categoryNames;
    }

    public Double getMinMoney() {
        return minMoney;
    }

    public void setMinMoney(Double minMoney) {
        this.minMoney = minMoney;
    }
}
