package com.zhiwee.gree.model.FyGoods.vo;

import com.zhiwee.gree.model.FyGoods.FyOrder;
import com.zhiwee.gree.model.FyGoods.FyOrderItem;

import java.io.Serializable;
import java.util.List;

public class FyOrderResultVo  implements Serializable {

    private FyOrder fyOrder;

    private List<FyOrderItem> fyOrderItemList;


    public FyOrder getFyOrder() {
        return fyOrder;
    }

    public void setFyOrder(FyOrder fyOrder) {
        this.fyOrder = fyOrder;
    }

    public void setFyOrderItemList(List<FyOrderItem> fyOrderItemList) {
        this.fyOrderItemList = fyOrderItemList;
    }

    public List<FyOrderItem> getFyOrderItemList() {
        return fyOrderItemList;
    }

    @Override
    public String toString() {
        return "FyOrderResultVo{" +
                "fyOrder=" + fyOrder +
                ", fyOrderItemList=" + fyOrderItemList +
                '}';
    }
}
