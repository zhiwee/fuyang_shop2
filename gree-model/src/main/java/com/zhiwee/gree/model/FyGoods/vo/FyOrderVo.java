package com.zhiwee.gree.model.FyGoods.vo;

import org.jeecgframework.poi.excel.annotation.Excel;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author DELL
 */
public class FyOrderVo implements Serializable{

    @Excel(name = "订单号")
    private String orderNum;

    @Excel(name = "下单时间")
    private String orderTimeStr;

    @Excel(name = "支付时间")
    private String payTimeStr;

    @Excel(name = "退款时间")
    private String refoundTimeStr;

    @Excel(name = "下单用户", needMerge = true)
    private  String userName;

    @Excel(name = "用户手机号")
    private String phoneNum;

    @Excel(name = "订单总金额")
    private BigDecimal allPrice;

    @Excel(name = "商品")
    private   String goodsBody;
    @Excel(name = "数量")
    private   Integer count;
    @Excel(name = "折扣")
    private BigDecimal discount;

    @Excel(name = "实际支付金额")
    private BigDecimal payPrice;
    @Excel(name = "状态", replace={"未支付_1","已支付_2","部分退款_3","全额退款_4"})
    private Integer state;

    @Excel(name = "是否申请退款",replace={"否_1","是_2"})
    private  Integer isrefound;

    @Excel(name = "是否发货",replace={"否_1","是_2"})
    private  Integer deliver ;

    @Excel(name = "购买渠道",replace={"普通下单_1","直播购买_2","秒杀订单_3"})
    private  Integer type ;


    @Excel(name = "退款金额")
    private  BigDecimal  refoundPrice;
    @Excel(name = "省")
    private String  province;
    @Excel(name = "市")
    private String  city;
    @Excel(name = "区")
    private String  area;
    @Excel(name = "收货地址")
    private String  detailAddress;

    public String getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }

    public String getOrderTimeStr() {
        return orderTimeStr;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public void setOrderTimeStr(String orderTimeStr) {
        this.orderTimeStr = orderTimeStr;
    }

    public String getPayTimeStr() {
        return payTimeStr;
    }

    public void setPayTimeStr(String payTimeStr) {
        this.payTimeStr = payTimeStr;
    }

    public String getRefoundTimeStr() {
        return refoundTimeStr;
    }

    public void setRefoundTimeStr(String refoundTimeStr) {
        this.refoundTimeStr = refoundTimeStr;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    public BigDecimal getAllPrice() {
        return allPrice;
    }

    public void setAllPrice(BigDecimal allPrice) {
        this.allPrice = allPrice;
    }

    public String getGoodsBody() {
        return goodsBody;
    }

    public void setGoodsBody(String goodsBody) {
        this.goodsBody = goodsBody;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public BigDecimal getPayPrice() {
        return payPrice;
    }

    public void setPayPrice(BigDecimal payPrice) {
        this.payPrice = payPrice;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getIsrefound() {
        return isrefound;
    }

    public void setIsrefound(Integer isrefound) {
        this.isrefound = isrefound;
    }

    public Integer getDeliver() {
        return deliver;
    }

    public void setDeliver(Integer deliver) {
        this.deliver = deliver;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public BigDecimal getRefoundPrice() {
        return refoundPrice;
    }

    public void setRefoundPrice(BigDecimal refoundPrice) {
        this.refoundPrice = refoundPrice;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getDetailAddress() {
        return detailAddress;
    }

    public void setDetailAddress(String detailAddress) {
        this.detailAddress = detailAddress;
    }
}