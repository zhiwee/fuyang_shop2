package com.zhiwee.gree.model.OrderInfo;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.jeecgframework.poi.excel.annotation.Excel;
import xyz.icrab.common.util.jackson.CustomJsonDateDeserializer;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author sun on 2019/5/11
 */
public class OrderExport implements Serializable,Cloneable {

    @Excel(name = "收货人")
    private String deliveryUser;

    @Excel(name = "收货手机号")
    private String deliveryPhone;

    @Excel(name = "订单状态",replace={"未支付_1","已支付_2","退单_3","退款_4"})
    private Integer orderState;


    @Excel(name = "订单流水号")
    private String orderNum;

    @Excel(name = "下单时间", needMerge = true,exportFormat = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;


    @Excel(name = "支付时间", needMerge = true,exportFormat = "yyyy-MM-dd HH:mm:ss")
    private Date payTime;

    @Excel(name = "订单总额")
    private Double totalPrice;

    @Excel(name = "实际金额")
    private Double realPrice;

    @Excel(name = "收货地址")
    private String installAddress;


    @Excel(name = "订单促销总费用")
    private Double promotionCosts;


    @Excel(name = "商品名")
    private String goodsName;

    @Excel(name = "商品型号")
    private String specId;

    @Excel(name = "商品单价")
    private Double unitPrice;

    @Excel(name = "商品总额")
    private Double amount;

    @Excel(name = "商品实际总额")
    private Double realAmount;


    @Excel(name = "购买数量")
    private Integer buyBun;

    @Excel(name = "促销费用")
    private Double activityCost;

    @Excel(name = "是否使用优惠券",replace={"使用_1","未使用_2"})
    private Integer isCoupon;

    @Excel(name = "订单类型",replace={"商城正常下单_1", "线下活动下单_2" ,"分销订单_3","工程团购订单_4"})
    private Integer isOffline;

    @Excel(name = "订单备注")
    private String remark;

    @Excel(name = "工程单位名称")
    private String dealerName;


    public Double getRealAmount() {
        return realAmount;
    }

    public void setRealAmount(Double realAmount) {
        this.realAmount = realAmount;
    }

    public String getDeliveryUser() {
        return deliveryUser;
    }

    public void setDeliveryUser(String deliveryUser) {
        this.deliveryUser = deliveryUser;
    }

    public String getDeliveryPhone() {
        return deliveryPhone;
    }

    public void setDeliveryPhone(String deliveryPhone) {
        this.deliveryPhone = deliveryPhone;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Integer getBuyBun() {
        return buyBun;
    }

    public void setBuyBun(Integer buyBun) {
        this.buyBun = buyBun;
    }


    public Integer getOrderState() {
        return orderState;
    }

    public void setOrderState(Integer orderState) {
        this.orderState = orderState;
    }

    public String getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getPayTime() {
        return payTime;
    }

    public void setPayTime(Date payTime) {
        this.payTime = payTime;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Double getRealPrice() {
        return realPrice;
    }

    public void setRealPrice(Double realPrice) {
        this.realPrice = realPrice;
    }

    public String getInstallAddress() {
        return installAddress;
    }

    public void setInstallAddress(String installAddress) {
        this.installAddress = installAddress;
    }


    public Double getPromotionCosts() {
        return promotionCosts;
    }

    public void setPromotionCosts(Double promotionCosts) {
        this.promotionCosts = promotionCosts;
    }



    public String getSpecId() {
        return specId;
    }

    public void setSpecId(String specId) {
        this.specId = specId;
    }

    public Double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Double getActivityCost() {
        return activityCost;
    }

    public void setActivityCost(Double activityCost) {
        this.activityCost = activityCost;
    }

    public Integer getIsCoupon() {
        return isCoupon;
    }

    public void setIsCoupon(Integer isCoupon) {
        this.isCoupon = isCoupon;
    }


    public Integer getIsOffline() {
        return isOffline;
    }

    public void setIsOffline(Integer isOffline) {
        this.isOffline = isOffline;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
