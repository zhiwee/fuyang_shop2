package com.zhiwee.gree.model.card;


import xyz.icrab.common.model.enums.YesOrNo;

public class PhoneCard {

        //抵用金额
     private Double useMoney;

     private String categoryName;

    private String userName;


    private YesOrNo yesOrNo;

    public Double getUseMoney() {
        return useMoney;
    }

    public void setUseMoney(Double useMoney) {
        this.useMoney = useMoney;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public YesOrNo getYesOrNo() {
        return yesOrNo;
    }

    public void setYesOrNo(YesOrNo yesOrNo) {
        this.yesOrNo = yesOrNo;
    }
}
