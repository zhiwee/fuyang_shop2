package com.zhiwee.gree.model.GoodsInfo.GoodVo;

import com.zhiwee.gree.model.GoodsInfo.ShopGoodsInfo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author sun on 2019/6/5
 */
public class GoodsHomePage implements Serializable {

    List<ShopGoodsInfo> news = new ArrayList<>();
    List<ShopGoodsInfo> recommend = new ArrayList<>();
    List<ShopGoodsInfo> hotSell = new ArrayList<>();

    public  GoodsHomePage( List<ShopGoodsInfo> news , List<ShopGoodsInfo> recommend,List<ShopGoodsInfo> hotSell){
        this.news = news;
        this.recommend = recommend;
        this.hotSell = hotSell;
    }

    public  GoodsHomePage(){}
    public List<ShopGoodsInfo> getNews() {
        return news;
    }

    public void setNews(List<ShopGoodsInfo> news) {
        this.news = news;
    }

    public List<ShopGoodsInfo> getRecommend() {
        return recommend;
    }

    public void setRecommend(List<ShopGoodsInfo> recommend) {
        this.recommend = recommend;
    }

    public List<ShopGoodsInfo> getHotSell() {
        return hotSell;
    }

    public void setHotSell(List<ShopGoodsInfo> hotSell) {
        this.hotSell = hotSell;
    }
}
