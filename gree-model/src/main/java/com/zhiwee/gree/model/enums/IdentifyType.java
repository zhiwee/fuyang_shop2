package com.zhiwee.gree.model.enums;

import xyz.icrab.common.model.Valueable;

/**
 * @author Knight
 * @since 2018/4/23 10:56
 */
public enum IdentifyType implements Valueable<Integer> {

    USERNAME(1);

    private final int value;

    IdentifyType(int value) {
        this.value = value;
    }

    @Override
    public Integer value() {
        return value;
    }
}
