package com.zhiwee.gree.model.HomePage;


import xyz.icrab.common.model.annotation.Like;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Table(name="s_page_channel_keyword")
public class PageChannelKeyword {

    @Id
    private String id;
    @Like
    private String name;

    private String channelId;

    @Transient
    private String channelName;

    private String url;
    @Transient
    private String urlName;

    private Integer thisSort;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrlName() {
        return urlName;
    }

    public void setUrlName(String urlName) {
        this.urlName = urlName;
    }

    public Integer getThisSort() {
        return thisSort;
    }

    public void setThisSort(Integer thisSort) {
        this.thisSort = thisSort;
    }
}
