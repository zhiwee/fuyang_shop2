package com.zhiwee.gree.model.OrderInfo.vo;

import java.io.Serializable;

/**
 * @author sun on 2019/7/5
 */
public class OrderBaseInfo implements Serializable {

    /**
     * 订单号
     */
    private String orderNum;
    /**
     * 收货人
     */
    private String deliveryUser;
    /**
     * 收货地址
     */
    private String deliveryAddress;
    /**
     * 收货号码
     */
    private String deliveryPhone;
    /**
     * 金额
     */
    private Double realPrice;

    public Double getRealPrice() {
        return realPrice;
    }

    public void setRealPrice(Double realPrice) {
        this.realPrice = realPrice;
    }

    public String getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }

    public String getDeliveryUser() {
        return deliveryUser;
    }

    public void setDeliveryUser(String deliveryUser) {
        this.deliveryUser = deliveryUser;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public String getDeliveryPhone() {
        return deliveryPhone;
    }

    public void setDeliveryPhone(String deliveryPhone) {
        this.deliveryPhone = deliveryPhone;
    }
}
