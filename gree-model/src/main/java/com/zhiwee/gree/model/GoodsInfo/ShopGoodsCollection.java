package com.zhiwee.gree.model.GoodsInfo;

import java.io.Serializable;
import java.math.BigDecimal;

public class ShopGoodsCollection  implements Serializable {

    private  String  goodsId;

    private  String  cover;

    private  String  name;

    private BigDecimal price;

    private  String  storeInfo;


    public String getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getStoreInfo() {
        return storeInfo;
    }

    public void setStoreInfo(String storeInfo) {
        this.storeInfo = storeInfo;
    }
}
