package com.zhiwee.gree.model.HomePage.PageV0;

import java.util.List;

public class PageChannelAllVO {
    private List<PageChannelVO> sonlist;

    public List<PageChannelVO> getSonlist() {
        return sonlist;
    }

    public void setSonlist(List<PageChannelVO> sonlist) {
        this.sonlist = sonlist;
    }
}
