package com.zhiwee.gree.model.ShopUser;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import xyz.icrab.common.util.jackson.CustomJsonDateDeserializer;

import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * TODO
 *
 * @author Chensong
 * @Company http://www.zhiwee.com
 * date 2020-02-10
 */
@Table(name="s_shop_user_signlog")
public class ShopUserSignLog implements Serializable {

    @Id
    private String id ;
    private String userId;

    @JsonDeserialize(using = CustomJsonDateDeserializer.class)
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss", timezone = "GMT+8")
    private Date signDate;

    private Integer signDays;

    public ShopUserSignLog() {
    }

    public ShopUserSignLog(String id) {
        this.id = id;
    }

    public ShopUserSignLog(String id, String userId) {
        this.id = id;
        this.userId = userId;
    }

    public ShopUserSignLog(String id, String userId, Date signDate) {
        this.id = id;
        this.userId = userId;
        this.signDate = signDate;
    }

    public Integer getSignDays() {
        return signDays;
    }

    public void setSignDays(Integer signDays) {
        this.signDays = signDays;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Date getSignDate() {
        return signDate;
    }

    public void setSignDate(Date signDate) {
        this.signDate = signDate;
    }
}
