package com.zhiwee.gree.model.GoodsInfo;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 购物车实体类信息
 */
public class ShopCartGoods implements Serializable {

   private   String  id;


   private String  shopUserId;

   private String  getShopUserName;

   //商品的id
   private   String  goodsId;

   //商品名称
   private  String  name;

   //规格属性
    private  String  spec;

   //单价
   private   BigDecimal   price;

   //数量
   private  Integer  count;

    //总价
    private  BigDecimal  totalPrice;



   //封面
    private   String   cover;


    //商品库存
    private   Integer   storeCount;



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getShopUserId() {
        return shopUserId;
    }

    public void setShopUserId(String shopUserId) {
        this.shopUserId = shopUserId;
    }

    public String getGetShopUserName() {
        return getShopUserName;
    }

    public void setGetShopUserName(String getShopUserName) {
        this.getShopUserName = getShopUserName;
    }

    public String getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }


    public String getSpec() {
        return spec;
    }

    public void setSpec(String spec) {
        this.spec = spec;
    }

    public Integer getStoreCount() {
        return storeCount;
    }

    public void setStoreCount(Integer storeCount) {
        this.storeCount = storeCount;
    }

}
