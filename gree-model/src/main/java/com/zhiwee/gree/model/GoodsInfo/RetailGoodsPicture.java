package com.zhiwee.gree.model.GoodsInfo;

import javax.persistence.Transient;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;


/**
 * @author sun on 2019/5/28
 */
@Table(name="s_retailgood_picture")
public class RetailGoodsPicture implements Serializable {

    @Id
    private String id;

    private String lastPicture;

    private String goodId;

    private Double leftPercent;

    private Double topPercent;


    private Double widthPercent;

    private Double heightPercent;

    private String backPicture;

    @Transient
    private String redirectUrl;

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    public String getBackPicture() {
        return backPicture;
    }

    public void setBackPicture(String backPicture) {
        this.backPicture = backPicture;
    }

    public Double getTopPercent() {
        return topPercent;
    }

    public void setTopPercent(Double topPercent) {
        this.topPercent = topPercent;
    }

    public Double getWidthPercent() {
        return widthPercent;
    }

    public void setWidthPercent(Double widthPercent) {
        this.widthPercent = widthPercent;
    }

    public Double getHeightPercent() {
        return heightPercent;
    }

    public void setHeightPercent(Double heightPercent) {
        this.heightPercent = heightPercent;
    }

    public Double getLeftPercent() {
        return leftPercent;
    }

    public void setLeftPercent(Double leftPercent) {
        this.leftPercent = leftPercent;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLastPicture() {
        return lastPicture;
    }

    public void setLastPicture(String lastPicture) {
        this.lastPicture = lastPicture;
    }

    public String getGoodId() {
        return goodId;
    }

    public void setGoodId(String goodId) {
        this.goodId = goodId;
    }
}
