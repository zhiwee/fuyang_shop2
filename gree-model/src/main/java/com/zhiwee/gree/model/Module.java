package com.zhiwee.gree.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author gll
 * @since 2018/4/21 22:03
 */
@Table(name = "s_sys_module")
public class Module implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    private String id;

    /**
     * 接口模块名称
     */
	@Column(name="name_")
    private String name;

    /**
     * 父级Id
     */
    private String parentid;

    /**
     * 路径
     */
    private String parentids;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getParentid() {
		return parentid;
	}

	public void setParentid(String parentid) {
		this.parentid = parentid;
	}

	public String getParentids() {
		return parentids;
	}

	public void setParentids(String parentids) {
		this.parentids = parentids;
	}

	
}
