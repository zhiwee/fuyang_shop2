package com.zhiwee.gree.model.livePlay;

import java.io.Serializable;
import java.util.List;

public class LivePlayStr implements Serializable {

   private Integer errcode;
    private String     errmsg;
    private Integer total;
        private List<LivePlay> room_info;
    private List<liveReplay> live_replay;

    public Integer getErrcode() {
        return errcode;
    }

    public void setErrcode(Integer errcode) {
        this.errcode = errcode;
    }

    public String getErrmsg() {
        return errmsg;
    }

    public void setErrmsg(String errmsg) {
        this.errmsg = errmsg;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public List<LivePlay> getRoom_info() {
        return room_info;
    }

    public void setRoom_info(List<LivePlay> room_info) {
        this.room_info = room_info;
    }

    public List<liveReplay> getLive_replay() {
        return live_replay;
    }

    public void setLive_replay(List<liveReplay> live_replay) {
        this.live_replay = live_replay;
    }
}
