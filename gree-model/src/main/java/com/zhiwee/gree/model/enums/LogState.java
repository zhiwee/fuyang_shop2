package com.zhiwee.gree.model.enums;

import xyz.icrab.common.model.Represent;

/**
 * @author sun on 2019/4/29
 */
public enum LogState implements Represent<String> {
    /**
     * 信息
     */
    info("info", "信息"),
    /**
     * 异常
     */
    error("error", "异常");

    private final String value;

    private final String message;


    LogState(String  value, String message) {
        this.value = value;
        this.message = message;
    }
    @Override
    public String value() {
        return this.value;
    }

    @Override
    public String message() {
        return this.message;
    }
}
