package com.zhiwee.gree.model.ret;

public enum MenuReturnMsg {
    MENU_NAME_EMPTY("菜单名称不能为空"),
    MENU_TYPE_EMPTY("菜单类型不能为空"),
    MENU_ID_EMPTY("菜单id不能为空"),
    ROLE_ID_EMPTY("角色id不能为空"),
    USER_ID_EMPTY("用户id不能为空"),
    ;

    private String message;

    MenuReturnMsg(String message) {
        this.message = message;
    }


    public String message() {
        return message;
    }
}
