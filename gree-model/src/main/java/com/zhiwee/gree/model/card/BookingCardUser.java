package com.zhiwee.gree.model.card;

import xyz.icrab.common.model.BaseModel;

import javax.persistence.Id;
import javax.persistence.Table;

@Table(name="s_bookingcard_user")
public class BookingCardUser extends BaseModel {
    @Id
    private String id;
    private String phone;
    private String userName;
    private String userId;
    private String bookingCardId;
    //预售金额
    private Double bookingMoney;
    //实际抵用金额
    private Double realityMoney;

    //状态（1、正常，2删除）
    private Integer state;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getBookingCardId() {
        return bookingCardId;
    }

    public void setBookingCardId(String bookingCardId) {
        this.bookingCardId = bookingCardId;
    }

    public Double getBookingMoney() {
        return bookingMoney;
    }

    public void setBookingMoney(Double bookingMoney) {
        this.bookingMoney = bookingMoney;
    }

    public Double getRealityMoney() {
        return realityMoney;
    }

    public void setRealityMoney(Double realityMoney) {
        this.realityMoney = realityMoney;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }
}
