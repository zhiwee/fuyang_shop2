package com.zhiwee.gree.model.draw;

import xyz.icrab.common.model.BaseModel;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;

@Table(name = "s_draws_info")
public class DrawsInfo  extends BaseModel {

    @Id
    private String id;

    //奖池名称
    private String drawId;


    //奖品
    private String name;

//可中数量
    private  Integer drawSum;

//以中数量
    private Integer useDrawSum;

    //奖项
    private Integer prize;
    //不限数量
    private Integer state;
    //所占百分比
    private Double chance;



    @Transient
    private String customer;
    @Transient
    private String customerPhone;
    @Transient
    private String drawName;
    public Integer getState() {
        return state;
    }

    public Double getChance() {
        return chance;
    }

    public void setChance(Double chance) {
        this.chance = chance;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }


    public String getDrawId() {
        return drawId;
    }

    public void setDrawId(String drawId) {
        this.drawId = drawId;
    }

    public Integer getPrize() {
        return prize;
    }

    public void setPrize(Integer prize) {
        this.prize = prize;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDrawName() {
        return drawName;
    }

    public void setDrawName(String drawName) {
        this.drawName = drawName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }




    public Integer getDrawSum() {
        return drawSum;
    }

    public void setDrawSum(Integer drawSum) {
        this.drawSum = drawSum;
    }

    public Integer getUseDrawSum() {
        return useDrawSum;
    }

    public void setUseDrawSum(Integer useDrawSum) {
        this.useDrawSum = useDrawSum;
    }
}
