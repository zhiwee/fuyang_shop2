package com.zhiwee.gree.model.ActivityTicketOrder;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.jeecgframework.poi.excel.annotation.Excel;
import xyz.icrab.common.util.jackson.CustomJsonDateDeserializer;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Date;

/**
 * Description: 认筹劵的购买信息导出
 * @author: sun
 * @Date 下午4:57 2019/6/23
 * @param:
 * @return:
 */
public class TicketOrderExport implements Serializable {

    @Excel(name = "劵号")
    private String ticketSeqno;

    @Excel(name = "下单时间", needMerge = true,exportFormat = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @Excel(name = "支付时间", needMerge = true,exportFormat = "yyyy-MM-dd HH:mm:ss")
    private Date payTime;



    //订单状态 1 未支付 2 已支付 3 已退单 4 已退款
    @Excel(name = "订单状态",replace={"未支付1","已支付_2","退单_3","退款_4"})
    private Integer orderState;


    //支付方式 1 支付宝 2 微信 3 银联 4 转账
    @Excel(name = "支付方式",replace={"支付宝_1","微信_2","银联_3","转账_4"})
    private Integer payType;

    //实际金额
    @Excel(name = "金额")
    private Double realPrice;

    //订单流水号
    @Excel(name = "订单流水号")
    private String  orderNum;

    // 用户名
    @Excel(name = "用户名")
    private String userName;

    //手机号
    @Excel(name = "手机号")
    private String phone;


    //退款金额
    @Excel(name = "退款金额")
    private Double refundMoney;

    //1 未申请退款 2 申请退款中  3 拒绝申请退款  4 同意申请退款
    @Excel(name = "退款状态",replace={"未申请退款_1","申请退款中_2","拒绝申请退款_3","同意申请退款_4"})
    private Integer refundState;


    public String getTicketSeqno() {
        return ticketSeqno;
    }

    public void setTicketSeqno(String ticketSeqno) {
        this.ticketSeqno = ticketSeqno;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getPayTime() {
        return payTime;
    }

    public void setPayTime(Date payTime) {
        this.payTime = payTime;
    }

    public Integer getOrderState() {
        return orderState;
    }

    public void setOrderState(Integer orderState) {
        this.orderState = orderState;
    }

    public Integer getPayType() {
        return payType;
    }

    public void setPayType(Integer payType) {
        this.payType = payType;
    }

    public Double getRealPrice() {
        return realPrice;
    }

    public void setRealPrice(Double realPrice) {
        this.realPrice = realPrice;
    }

    public String getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Double getRefundMoney() {
        return refundMoney;
    }

    public void setRefundMoney(Double refundMoney) {
        this.refundMoney = refundMoney;
    }

    public Integer getRefundState() {
        return refundState;
    }

    public void setRefundState(Integer refundState) {
        this.refundState = refundState;
    }
}
