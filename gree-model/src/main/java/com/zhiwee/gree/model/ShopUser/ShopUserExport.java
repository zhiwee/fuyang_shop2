package com.zhiwee.gree.model.ShopUser;

import org.jeecgframework.poi.excel.annotation.Excel;


import java.io.Serializable;
import java.util.Date;

/**
 * @author sun on 2019/5/8
 */
public class ShopUserExport implements Serializable {

    @Excel(name = "登录账号")
    private  String  username;

    @Excel(name = "真实姓名")
    private String nickname;

    @Excel(name = "手机号")
    private String phone;


    @Excel(name = "状态",replace={"启用_1","禁用_2"})
    private Integer state;

    @Excel(name = "注册时间", needMerge = true,exportFormat = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @Excel(name = "OPENID")
    private String openId;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }
}
