package com.zhiwee.gree.model.Dealer;


import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import xyz.icrab.common.model.annotation.Like;
import xyz.icrab.common.model.enums.YesOrNo;
import xyz.icrab.common.model.type.OrderBy;
import xyz.icrab.common.util.jackson.CustomJsonDateDeserializer;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
/**
 * @author sun on 2019/6/26
 */
@Table(name="t_dealer_info")
public class DealerInfo implements  Serializable{
    @Id
    private String id;

    private YesOrNo state;

    private String center;
    /*@Transient*/
    private String district;

    private String area;

    private String town;

    private String code;

    private String name;

    private String address;

    private String mobile;

    private String parentId;

    private String parentName;

    @Like
    private String idPath;

    private String typeCode;

    private String typeName;

    private String managerName;//负责人

    private String smCallNumber;//业务员联系电话

    private String createman;

    @OrderBy(OrderBy.Sort.ASC)
    @JsonDeserialize(using = CustomJsonDateDeserializer.class)
    private Date createTime;

    private String updateman;
    @JsonDeserialize(using = CustomJsonDateDeserializer.class)
    private Date updateTime;
    private String serviceManager;//格力业务

    private Integer isShop;

    private String qrImg;

    private String activityId;

    private String centerId;

    private Integer type;

    private String distributorId;

    @Transient
    private String distributorName;

    private String meetingId;

    //销售类型
    private Integer marketType;

    //营业情况
    private String business;

    //渠道类型
    private String channel;

    //综合面积
    private String comprehensiveArea;

    //实际面积
    private String actualArea;

    //店面性质
    private String storeNature;

    //冰箱导购人数
    private Integer personnelNumberB;

    //空调提成类型
    @Transient
    private Integer percentageTypeK;

    //冰箱提成类型
    @Transient
    private Integer percentageTypeB;

    //空调导购人数
    private Integer personnelNumberK;

    //生活电器导购人数
    private Integer personnelNumberS;

    @Transient
    private String meetingName;

    //0为一般门店1为通用卷门店
    private Integer isDefault;

    private List<DealerInfo> childrenList;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public YesOrNo getState() {
        return state;
    }

    public void setState(YesOrNo state) {
        this.state = state;
    }

    public String getCenter() {
        return center;
    }

    public void setCenter(String center) {
        this.center = center;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public String getIdPath() {
        return idPath;
    }

    public void setIdPath(String idPath) {
        this.idPath = idPath;
    }

    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    public String getSmCallNumber() {
        return smCallNumber;
    }

    public void setSmCallNumber(String smCallNumber) {
        this.smCallNumber = smCallNumber;
    }

    public String getCreateman() {
        return createman;
    }

    public void setCreateman(String createman) {
        this.createman = createman;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getUpdateman() {
        return updateman;
    }

    public void setUpdateman(String updateman) {
        this.updateman = updateman;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getServiceManager() {
        return serviceManager;
    }

    public void setServiceManager(String serviceManager) {
        this.serviceManager = serviceManager;
    }

    public Integer getIsShop() {
        return isShop;
    }

    public void setIsShop(Integer isShop) {
        this.isShop = isShop;
    }

    public String getQrImg() {
        return qrImg;
    }

    public void setQrImg(String qrImg) {
        this.qrImg = qrImg;
    }

    public String getActivityId() {
        return activityId;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    public String getCenterId() {
        return centerId;
    }

    public void setCenterId(String centerId) {
        this.centerId = centerId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getDistributorId() {
        return distributorId;
    }

    public void setDistributorId(String distributorId) {
        this.distributorId = distributorId;
    }

    public String getDistributorName() {
        return distributorName;
    }

    public void setDistributorName(String distributorName) {
        this.distributorName = distributorName;
    }

    public String getMeetingId() {
        return meetingId;
    }

    public void setMeetingId(String meetingId) {
        this.meetingId = meetingId;
    }

    public Integer getMarketType() {
        return marketType;
    }

    public void setMarketType(Integer marketType) {
        this.marketType = marketType;
    }

    public String getBusiness() {
        return business;
    }

    public void setBusiness(String business) {
        this.business = business;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getComprehensiveArea() {
        return comprehensiveArea;
    }

    public void setComprehensiveArea(String comprehensiveArea) {
        this.comprehensiveArea = comprehensiveArea;
    }

    public String getActualArea() {
        return actualArea;
    }

    public void setActualArea(String actualArea) {
        this.actualArea = actualArea;
    }

    public String getStoreNature() {
        return storeNature;
    }

    public void setStoreNature(String storeNature) {
        this.storeNature = storeNature;
    }

    public Integer getPersonnelNumberB() {
        return personnelNumberB;
    }

    public void setPersonnelNumberB(Integer personnelNumberB) {
        this.personnelNumberB = personnelNumberB;
    }

    public Integer getPercentageTypeK() {
        return percentageTypeK;
    }

    public void setPercentageTypeK(Integer percentageTypeK) {
        this.percentageTypeK = percentageTypeK;
    }

    public Integer getPercentageTypeB() {
        return percentageTypeB;
    }

    public void setPercentageTypeB(Integer percentageTypeB) {
        this.percentageTypeB = percentageTypeB;
    }

    public Integer getPersonnelNumberK() {
        return personnelNumberK;
    }

    public void setPersonnelNumberK(Integer personnelNumberK) {
        this.personnelNumberK = personnelNumberK;
    }

    public Integer getPersonnelNumberS() {
        return personnelNumberS;
    }

    public void setPersonnelNumberS(Integer personnelNumberS) {
        this.personnelNumberS = personnelNumberS;
    }

    public String getMeetingName() {
        return meetingName;
    }

    public void setMeetingName(String meetingName) {
        this.meetingName = meetingName;
    }

    public Integer getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(Integer isDefault) {
        this.isDefault = isDefault;
    }

    public List<DealerInfo> getChildrenList() {
        return childrenList;
    }

    public void setChildrenList(List<DealerInfo> childrenList) {
        this.childrenList = childrenList;
    }
}
