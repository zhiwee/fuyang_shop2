package com.zhiwee.gree.model.ActivityTicketOrder;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import xyz.icrab.common.model.BaseModel;
import xyz.icrab.common.model.type.OrderBy;
import xyz.icrab.common.util.jackson.CustomJsonDateDeserializer;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Date;

/**
 * 活动票券认筹记录
 * @author Knight
 * @since 2018/7/6 13:39
 */
@Table(name = "t_activity_ticket_order")
public class ActivityTicketOrder extends BaseModel {


    @Id
    private String id;
   // 1 未支付  2 已支付  3退款
    private Integer state;

    private String activityId;

    private String activityName;

    private String dealerId;

    private String dealerName;



    private String activityAddress;

    private String salesman;


    private String ticketId;

    private String ticketCode;


    private String ticketSeqno;

    private String  orderPassWord;



    private String consumerId;

    private String consumerMobile;

    private String deliveryMobile;

    private String deliveryReciever;

    private String deliveryProvince;

    private String deliveryCity;

    private String deliveryArea;

    private String deliveryAddress;

    @OrderBy(OrderBy.Sort.DESC)
    @JsonDeserialize(
            using = CustomJsonDateDeserializer.class
    )
    private Date orderTime;

    private Date payTime;

    private Date backTime;

    private String installAddress;//安装地址

    private String  rcAddress;//认筹地址
    /**
     * 认筹类型：1 活动认筹 2 当日认筹
     */
    private Integer type;

    /**
     * 认筹类型：1 线上认筹 2 线下认筹
     */
    private Integer online;


    private String provinceCode;

    private String cityCode;

    private String areaCode;

    private String recieverMAN;

    private String  recieverPhone;

    private String model;


    public ActivityTicketOrder(){

    }

    public ActivityTicketOrder(TicketOrder order){
        this.deliveryReciever = order.getOrderName();
        this.type = order.getType();
        this.ticketSeqno = order.getTicketSeqno();
        this.deliveryCity = order.getDeliveryCity();
        this.deliveryArea = order.getDeliveryArea();
        this.deliveryProvince = order.getDeliveryProvince();
        this.consumerMobile= order.getPhone();
        this.provinceCode = order.getProvinceCode();
        this.cityCode = order.getCityCode();
        this.areaCode = order.getAreaCode();
        this.deliveryAddress = order.getDetailAddress();

    }

    public String getOrderPassWord() {
        return orderPassWord;
    }

    public void setOrderPassWord(String orderPassWord) {
        this.orderPassWord = orderPassWord;
    }

    public Integer getOnline() {
        return online;
    }

    public void setOnline(Integer online) {
        this.online = online;
    }

    public String getProvinceCode() {
        return provinceCode;
    }

    public void setProvinceCode(String provinceCode) {
        this.provinceCode = provinceCode;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }


    public String getRecieverMAN() {
        return recieverMAN;
    }

    public void setRecieverMAN(String recieverMAN) {
        this.recieverMAN = recieverMAN;
    }

    public String getRecieverPhone() {
        return recieverPhone;
    }

    public void setRecieverPhone(String recieverPhone) {
        this.recieverPhone = recieverPhone;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }


    public String getInstallAddress() {
        return installAddress;
    }

    public void setInstallAddress(String installAddress) {
        this.installAddress = installAddress;
    }

    public String getRcAddress() {
        return rcAddress;
    }

    public void setRcAddress(String rcAddress) {
        this.rcAddress = rcAddress;
    }


    public String getTicketSeqno() {
        return ticketSeqno;
    }

    public void setTicketSeqno(String ticketSeqno) {
        this.ticketSeqno = ticketSeqno;
    }

    public String getActivityAddress() {
        return activityAddress;
    }

    public void setActivityAddress(String activityAddress) {
        this.activityAddress = activityAddress;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }



    public String getActivityId() {
        return activityId;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public String getDealerId() {
        return dealerId;
    }

    public void setDealerId(String dealerId) {
        this.dealerId = dealerId;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public String getSalesman() {
        return salesman;
    }

    public void setSalesman(String salesman) {
        this.salesman = salesman;
    }

    public String getTicketId() {
        return ticketId;
    }

    public void setTicketId(String ticketId) {
        this.ticketId = ticketId;
    }

    public String getTicketCode() {
        return ticketCode;
    }

    public void setTicketCode(String ticketCode) {
        this.ticketCode = ticketCode;
    }


    public String getConsumerId() {
        return consumerId;
    }

    public void setConsumerId(String consumerId) {
        this.consumerId = consumerId;
    }

    public String getConsumerMobile() {
        return consumerMobile;
    }

    public void setConsumerMobile(String consumerMobile) {
        this.consumerMobile = consumerMobile;
    }

    public String getDeliveryMobile() {
        return deliveryMobile;
    }

    public void setDeliveryMobile(String deliveryMobile) {
        this.deliveryMobile = deliveryMobile;
    }

    public String getDeliveryProvince() {
        return deliveryProvince;
    }

    public void setDeliveryProvince(String deliveryProvince) {
        this.deliveryProvince = deliveryProvince;
    }

    public String getDeliveryCity() {
        return deliveryCity;
    }

    public void setDeliveryCity(String deliveryCity) {
        this.deliveryCity = deliveryCity;
    }

    public String getDeliveryArea() {
        return deliveryArea;
    }

    public void setDeliveryArea(String deliveryArea) {
        this.deliveryArea = deliveryArea;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public Date getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(Date orderTime) {
        this.orderTime = orderTime;
    }

    public Date getPayTime() {
        return payTime;
    }

    public void setPayTime(Date payTime) {
        this.payTime = payTime;
    }

    public Date getBackTime() {
        return backTime;
    }

    public void setBackTime(Date backTime) {
        this.backTime = backTime;
    }

    public String getDeliveryReciever() {
        return deliveryReciever;
    }

    public void setDeliveryReciever(String deliveryReciever) {
        this.deliveryReciever = deliveryReciever;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

}
