package com.zhiwee.gree.model.WxPay;

/**
 * @author sun on 2019/5/15
 */

import java.io.Serializable;

/**
 * 微信支付配置文件
 * @author sun
 *
 */
public class WeChatConfig implements Serializable {
    public static final String SIGNTYPE = "MD5";
    /**
     * 扫码支付 类型
     */
    public static String scanPay="NATIVE";

    /**
     * h5调起微信客户端支付
     */
    public static String wxPay="MWEB";


    /**
     * JSAPI调起微信客户端支付
     */
    public static String Pay="JSAPI";


    /**
     * 公众号微信服务号APPID
     *  安徽格力 wxce622c5e0ba45a83
     *  zhiwee wx8df6c21ac062c68c
     *  阜阳 小程序 wx575dba0ef8ff034f
     *
     */
    public static String APPID="wx8df6c21ac062c68c";
    public static String APPIDFuYang="wx575dba0ef8ff034f";
    /**
     * 公众号微信支付的商户号
     *  安徽格力 1490317082
     *  zhiwee 1502086231
     *  阜阳 1232511302
     */
    public static String MCHID="1502086231";
    public static String MCHIDFuYang="1232511302";
    /**
     * 公众号微信支付的API密钥
     * zhiwee xHNnpiSugr8E5axFvjbZyyuEOhUVUJfv
     * 安徽格力  A1L0TMIKCJZ4O86XHB5PF29E3UWVDSGR
     * 阜阳 fuyangshangshashazhifumiyao2019g
     */
    public static String APIKEY="xHNnpiSugr8E5axFvjbZyyuEOhUVUJfv";
    public static String APIKEYFuYang="fuyangshangshashazhifumiyao2019g";
    public static String APIKEYFuYang2="7f7f89ad5023ce314a4ba5b6496201b8";
//    public static String WxPayRefundConfigCertPath="D:/1232511302_20200103_cert/apiclient_cert.p12";
    public static String WxPayRefundConfigCertPath="/home/cert/WXCertUtil/cert/1232511302_20200103_cert/apiclient_cert.p12";
    /**
     * 公众号微信支付订单成功之后的回调地址【注意：当前回调地址必须是公网能够访问的地址】
     */
    public static String WECHAT_NOTIFYURL="http://zx.zhiwee.com:8030/wxPay/wechat_notifurl";
    /**
     * 小程序支付订单成功之后的回调地址【注意：当前回调地址必须是公网能够访问的地址】
     */
    public static String WECHAT_NOTIFYURLFuYang="http://fyss.zhiwee.com:8090/fyGoods/wxPay/wxNotify";

    /**
     * 小程序珍藏卡支付回调地址
     */
    public static String WECHAT_ZHENGCANGKANOTIFYURLFuYang="";

    /**
     *todo 小程序支付预售卡的回调地址
     */
    public static String WECHAT_NOTIFYURLBookingCard="https://zw.huijiang.sh.cn/bookingCards/wxPayNotify";
    /**
     *todo 小程序退款预售卡的回调地址【注意：当前回调地址必须是公网能够访问的地址】
     */
    public static String WECHAT_NOTIFYURLBookingCardRefund="https://zw.huijiang.sh.cn/bookingCards/wxPayRefundNotify";

    /**
     *todo 小程序支付珍藏卡订单成功之后的回调地址【注意：当前回调地址必须是公网能够访问的地址】
     */
    public static String WECHAT_NOTIFYURLFuYangCard="https://zw.huijiang.sh.cn/cardInfo/wxPayNotify";
    /**
     *todo 小程序支付珍藏卡订单成功之后的回调地址【注意：当前回调地址必须是公网能够访问的地址】
     */
    public static String WECHAT_NOTIFYURLFuYangCardRefund="https://zw.huijiang.sh.cn/cardInfo/wxPayRefundNotify";
    /**
     * 公众号微信支付认筹劵成功之后的回调地址【注意：当前回调地址必须是公网能够访问的地址】
     */
    public static String WECHAT_TICKETNOTIFYURL="http://zx.zhiwee.com:8030/ticketOrder/wechat_notifurl";

    /**
     * 公众号微信充值成功之后的回调地址【注意：当前回调地址必须是公网能够访问的地址】
     */
    public static String WECHAT_RECHARGENOTIFYURL="http://zx.zhiwee.com:8030/balanceReCharge/wechat_notifurl";
    /**
     * 公众号微信统一下单API地址 正式版本
     */
    public static String UFDODER_URL="https://api.mch.weixin.qq.com/pay/unifiedorder";
    /**
     * 公众号微信订单退款回调地址
     */
   // public static String REFUND_URL="http://zx.zhiwee.com:8030/wxPay/refundUrl";
    public static String REFUND_URL="http://fyss.zhiwee.com:8091/wxPay/refUrl";


    /**
     * 公众号微信认筹劵退款回调地址
     */
    public static String REFUND_TICKETURL="http://zx.zhiwee.com:8030/ticketOrder/refundUrl";
    /**
     * 公众号开发者密钥
     *公司 8aef06b8b56c59abc8ee02d3ccff2a
     * 安徽 5c2b578056ecf398aa20d4a58b9425ab
     */
    public static String appSecret ="5c2b578056ecf398aa20d4a58b9425ab";
//    /**
//     * true为使用真实金额支付，false为使用测试金额支付（1分）
//     */
//    public static String WXPAY="false";

    /**
     * 微信开放平台开发者密钥
     */
    public static String kfAppSecret ="8aef06b8b56c59abc8ee02d3ccff2a";



    /**
     * 微信开放平台APPID
     *
     */
    public static String KFAPPID="wx8df6c21ac062c68c";

//湖南
    /**
     * 微信服务号APPID
     *
     */
    public static String HNAPPID="wx297aaeb8278014b3";
    /**
     * 微信小程序APPID
     */
    public static String MINAPPID="wx575dba0ef8ff034f";
    /**
     * 微信支付的商户号
     */
    public static String HNMCHID="1393552502";
    /**
     * 湖南微信支付的API密钥
     */
    public static String HNAPIKEY="hngree19A7aa9e41F9bF5945Beaf9941";
    /**
     * 微信小程序密钥
     */
    public static String AppSecret="7f7f89ad5023ce314a4ba5b6496201b8";

    /**
     * 湖南微信名称
     */
    public static String HNMCHIDNAME ="湖南格力";

    /**
     * 湖南开发者密码
     */
    public static String hnappSecret ="00fb450a0a169cc4a32ed472457af5dc";



    /**
     *    通过code换取access_token 以及openId
     */
    public static String access_tokenUrL ="https://api.weixin.qq.com/sns/oauth2/access_token?appid=AppId&secret=AppSecret&code=CODE&grant_type=authorization_code";
    /**
     *    通过code换取access_token 以及openId
     */
    public static String wxApiCode2Session ="https://api.weixin.qq.com/sns/jscode2session?appid=AppId&secret=AppSecret&js_code=CODE&grant_type=authorization_code";

    /**
     *    通过access_token 以及openId拉取用户信息
     */
    public static String UserInfoUrL ="https://api.weixin.qq.com/sns/userinfo?access_token=ACCESS_TOKEN&openid=OPENID&lang=zh_CN";



    //微信统一下单接口地址
    public static final String pay_url = "https://api.mch.weixin.qq.com/pay/unifiedorder";

    //微信获取token地址
    public static final String getAccess_tokenUrL ="https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET";



}