package com.zhiwee.gree.model.enums;

public enum AreaType {
    PROVINCE(1,"省"),
    CITY(2,"市"),
    COUNTY(3,"区/县"),
    TOWN(4,"镇"),
    VILLAGE(5,"村")
    ;

    private int value;
    private String name;

    AreaType(int value,String name) {
        this.value = value;
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
