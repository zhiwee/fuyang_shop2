package com.zhiwee.gree.model.OrderInfo;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import xyz.icrab.common.util.jackson.CustomJsonDateDeserializer;

import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * @author sun on 2019/5/11
 */
@Table(name="s_order_payment")
public class OrderPayment implements Serializable {

    @Id
    private String id;

    private String flowNum;

    private Double totalPaymentl;



    private Double creditCard;

    private Double unionay;

    private Double alipay;

    private Double weixin;

    @JsonDeserialize(
            using = CustomJsonDateDeserializer.class
    )
    private Date updateTime;

    private String updateMan;

    private String createMan;

    @JsonDeserialize(
            using = CustomJsonDateDeserializer.class
    )
    private Date createTime;


    //支付方式  1 支付宝   2 微信   3 转账支付  4 银联
    private Integer payType;

    private String orderId;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Integer getPayType() {
        return payType;
    }

    public void setPayType(Integer payType) {
        this.payType = payType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFlowNum() {
        return flowNum;
    }

    public void setFlowNum(String flowNum) {
        this.flowNum = flowNum;
    }

    public Double getTotalPaymentl() {
        return totalPaymentl;
    }

    public void setTotalPaymentl(Double totalPaymentl) {
        this.totalPaymentl = totalPaymentl;
    }

    public Double getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(Double creditCard) {
        this.creditCard = creditCard;
    }

    public Double getUnionay() {
        return unionay;
    }

    public void setUnionay(Double unionay) {
        this.unionay = unionay;
    }

    public Double getAlipay() {
        return alipay;
    }

    public void setAlipay(Double alipay) {
        this.alipay = alipay;
    }

    public Double getWeixin() {
        return weixin;
    }

    public void setWeixin(Double weixin) {
        this.weixin = weixin;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getUpdateMan() {
        return updateMan;
    }

    public void setUpdateMan(String updateMan) {
        this.updateMan = updateMan;
    }

    public String getCreateMan() {
        return createMan;
    }

    public void setCreateMan(String createMan) {
        this.createMan = createMan;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
