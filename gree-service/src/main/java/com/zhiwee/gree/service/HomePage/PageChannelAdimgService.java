package com.zhiwee.gree.service.HomePage;

import com.zhiwee.gree.model.HomePage.PageChannelAdimg;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.Map;

public interface PageChannelAdimgService extends BaseService<PageChannelAdimg,String> {
    Pageable<PageChannelAdimg> getPage(Map<String, Object> param, Pagination pagination);
}
