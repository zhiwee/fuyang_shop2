package com.zhiwee.gree.service.GoodsInfo;


import com.zhiwee.gree.model.GoodsInfo.GoodsCategoryAdImg;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.Map;

public interface GoodsCategoryAdImgService extends BaseService<GoodsCategoryAdImg,String> {

    Pageable<GoodsCategoryAdImg> getPage(Map<String, Object> param, Pagination pagination);

    GoodsCategoryAdImg selectById(String id);
}
