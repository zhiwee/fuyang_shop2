package com.zhiwee.gree.service.skill;

import com.zhiwee.gree.model.skill.Skillhb;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.List;
import java.util.Map;

public interface SkillhbService extends BaseService<Skillhb,String> {
    // extends BaseService<CommentPicture,String>
    // Pageable<ShopUserComment> achieveComment(Map<String, Object> params, Pagination pagination);
    Pageable<Skillhb>   queryList(Map<String, Object> params, Pagination pagination);
    List<Skillhb> queryHbByTime(Map<String,Object> params);
}
