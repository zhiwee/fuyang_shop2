package com.zhiwee.gree.service.support.ShopUser;


import com.zhiwee.gree.dao.ShopUser.ShopUserDao;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.ShopUser.ShopUserExport;
import com.zhiwee.gree.service.ShopUser.ShopUserService;
import com.zhiwee.gree.util.CodesUtils;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;
import xyz.icrab.common.web.param.IdsParam;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;


@Service
public class ShopUserServiceImpl extends BaseServiceImpl<ShopUser,String,ShopUserDao> implements ShopUserService {
    @Value("${ZHENCANGKA_URL}")
    private String cardUrl;
    @Value("${server.ticket.qrimg.dir}")
    private String imgDir;

    @Override
    public List<ShopUserExport> exportShopUser(Map<String,Object> param) {
        return this.dao.exportShopUser(param);
    }



    @Override
    public Pageable<ShopUser> pageInfo(Map<String, Object> param, Pagination pagination) {
        return dao.pageInfo(param, pagination);
    }

    @Override
    public void deleteUser(IdsParam param) {
        dao.deleteUser(param);
    }

    @Override
    public ShopUser achieveByUser(ShopUser user) {
        return this.dao.achieveByUser(user);
    }
    @Transactional
    @Override
    public void updateAndDelete(ShopUser shopUser, ShopUser su) {
        this.dao.update(shopUser);
        this.dao.delete(su);
    }

    @Override
    public Integer registerAmount(Map<String, Object> params) {
       return  this.dao.registerAmount(params);
    }

    @Override
    public String refreshUrl(String id, String cardUrl) {

        ShopUser shopUser=new ShopUser();
        shopUser.setId(id);
        String redirectUrl =String.format(cardUrl,id);
        shopUser.setCheckTime(new Date());
        shopUser.setHeadUrl(redirectUrl);
        dao.updateSelective(shopUser);
        return redirectUrl;
    }

    @Override
    public void generateImg(ShopUser shopUser) {
        try {
            FileOutputStream fos = new FileOutputStream(imgDir+shopUser.getId()+".png");
            String redirectUrl =String.format(cardUrl,shopUser.getId());
            fos.write(Base64.decodeBase64(CodesUtils.generate(redirectUrl)));
            fos.flush();
            fos.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

