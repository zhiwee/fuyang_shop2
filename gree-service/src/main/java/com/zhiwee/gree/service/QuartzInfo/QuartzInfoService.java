package com.zhiwee.gree.service.QuartzInfo;


import com.zhiwee.gree.model.Partement.Partement;
import com.zhiwee.gree.model.Quartz.QuartzInfo;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.Map;


public interface QuartzInfoService extends BaseService<QuartzInfo,String> {


    Pageable<QuartzInfo> pageInfo(Map<String, Object> params, Pagination pagination);
}
