package com.zhiwee.gree.service.GoodsInfo;

import com.zhiwee.gree.model.GoodsInfo.GoodsSpecsItemOrder;
import xyz.icrab.common.service.BaseService;

import java.util.List;
import java.util.Map;

public interface GoodsSpecsItemOrderService  extends BaseService<GoodsSpecsItemOrder,String> {
    List<GoodsSpecsItemOrder> getListBYGoodsId(Map<String, Object> param);
}
