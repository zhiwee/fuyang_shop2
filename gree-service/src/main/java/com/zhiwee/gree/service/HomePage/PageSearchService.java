package com.zhiwee.gree.service.HomePage;

import com.zhiwee.gree.model.HomePage.PageSearch;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;
import java.util.List;
import java.util.Map;

public interface PageSearchService extends BaseService<PageSearch,String> {
    Pageable<PageSearch> getPage(Map<String, Object> param, Pagination pagination);

    PageSearch getById(String id);

    List<PageSearch> getList();
}
