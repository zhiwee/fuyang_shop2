package com.zhiwee.gree.service.support.goods;


import com.zhiwee.gree.dao.goods.GoodsInfoDao;
import com.zhiwee.gree.dao.goods.ShopGoodsInfoDao;
import com.zhiwee.gree.model.GoodsInfo.GoodsInfo;
import com.zhiwee.gree.model.GoodsInfo.ShopGoodsInfo;
import com.zhiwee.gree.model.GoodsInfo.SolrGoodsInfo;
import com.zhiwee.gree.service.GoodsInfo.GoodsInfoService;
import com.zhiwee.gree.service.GoodsInfo.ShopGoodsInfoService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.List;
import java.util.Map;


@Service
public class ShopGoodsInfoServiceImpl extends BaseServiceImpl<ShopGoodsInfo,String,ShopGoodsInfoDao> implements ShopGoodsInfoService {

    @Override
    public Pageable<ShopGoodsInfo> achieveSmallGoods(Map<String, Object> param, Pagination pagination) {
        return this.dao.achieveSmallGoods(param,pagination);
    }

    @Override
    public List<ShopGoodsInfo> achieveGoodsByName(Map<String, Object> param) {
        return this.dao.achieveGoodsByName(param);
    }

    @Override
    public List<SolrGoodsInfo> getAllGoods(Map<String,Object> param) {
       return this.dao.getAllGoods(param);
    }

    @Override
    public Pageable<ShopGoodsInfo> searchAll(Map<String, Object> params, Pagination pagination) {
        return this.dao.searchAll(params,pagination);
    }

    @Override
    public ShopGoodsInfo achieveGoodsById(Map<String, Object> params) {
        return this.dao.achieveGoodsById(params);
    }

    @Override
    public List<ShopGoodsInfo> achieveSpecialGoods(Map<String,Object> params) {
        return this.dao.achieveSpecialGoods(params);
    }

    @Override
    public List<ShopGoodsInfo> achieveRecommend(Map<String, Object> param) {
        return this.dao.achieveRecommend(param);
    }

    @Override
    public Pageable<ShopGoodsInfo> searchEngineerGood(Map<String, Object> params, Pagination pagination) {
        return this.dao.searchEngineerGood(params,pagination);
    }

    @Override
    public ShopGoodsInfo searchOrderGoods(String goodId) {
        return this.dao.searchOrderGoods(goodId);
    }

    @Override
    public List<ShopGoodsInfo> achieveHotSellGood(Map<String,Object> param) {
        return this.dao.achieveHotSellGood(param);
    }


}
