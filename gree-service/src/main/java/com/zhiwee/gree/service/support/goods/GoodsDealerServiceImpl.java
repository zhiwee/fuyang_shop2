package com.zhiwee.gree.service.support.goods;

import com.zhiwee.gree.dao.goods.GoodsDealerDao;
import com.zhiwee.gree.model.GoodsInfo.GoodsDealer;
import com.zhiwee.gree.service.Dealer.DealerInfoService;
import com.zhiwee.gree.service.GoodsInfo.GoodsDealerService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.service.support.BaseServiceImpl;


/**
 *
 * @author jick
 * @date 2019/8/30
 */
@Service
public class GoodsDealerServiceImpl extends BaseServiceImpl<GoodsDealer,String, GoodsDealerDao> implements GoodsDealerService {


}

