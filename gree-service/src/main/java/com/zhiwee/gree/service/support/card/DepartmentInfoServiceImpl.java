package com.zhiwee.gree.service.support.card;

import com.zhiwee.gree.dao.crad.CardInfoDao;
import com.zhiwee.gree.dao.crad.DepartmentInfoDao;
import com.zhiwee.gree.model.card.CardInfo;
import com.zhiwee.gree.model.card.DepartmentInfo;
import com.zhiwee.gree.model.card.DepartmentInfoCY;
import com.zhiwee.gree.service.card.CardInfoService;
import com.zhiwee.gree.service.card.DepartmentInfoService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.List;
import java.util.Map;


@Service
public class DepartmentInfoServiceImpl extends BaseServiceImpl<DepartmentInfo, String, DepartmentInfoDao> implements DepartmentInfoService {


    @Override
    public Pageable<DepartmentInfo> pageInfo(Map<String, Object> params, Pagination pagination) {
        return dao.pageInfo(params,pagination);
    }

    @Override
    public List<DepartmentInfoCY> listAll() {
        return dao.listAll();
    }

}
