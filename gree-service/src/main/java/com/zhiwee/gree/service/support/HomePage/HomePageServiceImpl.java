package com.zhiwee.gree.service.support.HomePage;


import com.zhiwee.gree.dao.HomePage.HomePageDao;
import com.zhiwee.gree.dao.goods.GoodsInfoDao;
import com.zhiwee.gree.model.GoodsInfo.GoodsInfo;
import com.zhiwee.gree.model.GoodsInfo.SolrGoodsInfo;
import com.zhiwee.gree.model.HomePage.HomePage;
import com.zhiwee.gree.model.HomePage.PcPlanmap;
import com.zhiwee.gree.service.GoodsInfo.GoodsInfoService;
import com.zhiwee.gree.service.HomePage.HomePageService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.List;
import java.util.Map;


@Service
public class HomePageServiceImpl extends BaseServiceImpl<HomePage,String,HomePageDao> implements HomePageService {


    @Override
    public Pageable<HomePage> achieveHomePage(Map<String, Object> params, Pagination pagination) {
        return this.dao.achieveHomePage(params,pagination);
    }

}
