package com.zhiwee.gree.service.support.ShopDistributor;


import com.zhiwee.gree.dao.OrderInfo.OrderDao;
import com.zhiwee.gree.dao.ShopDistributor.DistributorAreaDao;
import com.zhiwee.gree.dao.ShopDistributor.DistributorOrderDao;
import com.zhiwee.gree.model.OrderInfo.Order;
import com.zhiwee.gree.model.ShopDistributor.DistributorArea;
import com.zhiwee.gree.model.ShopDistributor.DistributorOrder;
import com.zhiwee.gree.service.ShopDistributor.DistributorAreaService;
import com.zhiwee.gree.service.ShopDistributor.DistributorOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class DistributorOrderServiceImpl extends BaseServiceImpl<DistributorOrder, String, DistributorOrderDao> implements DistributorOrderService {
    @Autowired
    private OrderDao orderDao;

    @Override
    public Pageable<DistributorOrder> infoPage(Map<String, Object> param, Pagination pagination) {
        return this.dao.infoPage(param, pagination);
    }



    @Override
    public void updateDistributorOrder(DistributorOrder distributorOrder) {
        String orderId = distributorOrder.getOrderId();
        Map<String, Object> params = new HashMap<>();
        params.put("orderId", orderId);
        params.put("deliveryState", 1);
        Boolean flag = false;
        /* 查找这个订单已经发货的*/
        List<DistributorOrder> distributorOrders = this.dao.selectList(params);
        if (!distributorOrders.isEmpty() && distributorOrders.size() > 0) {
            for (DistributorOrder order : distributorOrders) {
                if (!order.getId().equals(distributorOrder.getId())) {
                    /**
                     * 说明有其他的订单也是已经发货的
                     */
                    flag = true;
                }

            }
        }
        if (!flag) {
            Order order = orderDao.select(orderId);
            orderDao.update(order);
        }

    }
}
