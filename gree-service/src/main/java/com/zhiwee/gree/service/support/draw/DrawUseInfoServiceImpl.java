package com.zhiwee.gree.service.support.draw;

import com.zhiwee.gree.dao.draw.DrawUseInfoDao;
import com.zhiwee.gree.model.draw.DrawUseInfo;
import com.zhiwee.gree.model.draw.DrawUseInfoExrt;
import com.zhiwee.gree.service.draw.DrawUseInfoService;
import xyz.icrab.common.model.Pagination;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.List;
import java.util.Map;


@Service("drawUseInfoService")
public class DrawUseInfoServiceImpl extends BaseServiceImpl<DrawUseInfo, String, DrawUseInfoDao> implements DrawUseInfoService {


    @Override
    public Pageable<DrawUseInfo> pageInfo(Map<String, Object> params, Pagination pagination) {
        return dao.pageInfo(params,pagination);
    }

    @Override
    public List<DrawUseInfoExrt> queryDrawUseInfoExport(Map<String, Object> params) {
        return dao.queryDrawUseInfoExport(params);
    }
}
