package com.zhiwee.gree.service.support.QuartzInfo;


import com.zhiwee.gree.dao.Partement.PartementDao;
import com.zhiwee.gree.dao.QuartzInfo.QuartzInfoDao;
import com.zhiwee.gree.model.Partement.Partement;
import com.zhiwee.gree.model.Quartz.QuartzInfo;
import com.zhiwee.gree.service.Partement.PartementService;
import com.zhiwee.gree.service.QuartzInfo.QuartzInfoService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.Map;


@Service
public class QuartzInfoServiceImpl extends BaseServiceImpl<QuartzInfo,String,QuartzInfoDao> implements QuartzInfoService {


    @Override
    public Pageable<QuartzInfo> pageInfo(Map<String, Object> params, Pagination pagination) {
       return  this.dao.pageInfo(params,pagination);
    }
}
