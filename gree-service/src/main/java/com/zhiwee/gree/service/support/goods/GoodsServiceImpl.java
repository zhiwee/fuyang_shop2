package com.zhiwee.gree.service.support.goods;

import com.zhiwee.gree.dao.goods.GoodsDao;
import com.zhiwee.gree.model.FyGoods.FygoodsExo;
import com.zhiwee.gree.model.GoodsInfo.GoodVo.GoodsExo;
import com.zhiwee.gree.model.GoodsInfo.GoodVo.GoodsFyExo;
import com.zhiwee.gree.model.GoodsInfo.GoodVo.GoodsInfoVO;
import com.zhiwee.gree.model.GoodsInfo.Goods;
import com.zhiwee.gree.model.GoodsInfo.LAGoodsInfo;
import com.zhiwee.gree.model.GoodsInfo.ProductAttribute;
import com.zhiwee.gree.model.OnlineActivity.OnlineActivityInfo;
import com.zhiwee.gree.model.OnlineActivity.vo.OnlineActivityInfoVo;
import com.zhiwee.gree.service.GoodsInfo.GoodsService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.List;
import java.util.Map;

/**
 * Created by jick on 2019/8/30.
 */
@Service
public class GoodsServiceImpl   extends BaseServiceImpl<Goods,String,GoodsDao> implements GoodsService{
    @Override
    public Pageable<Goods> queryInfoList(Map<String, Object> params, Pagination pagination) {
        return this.dao.queryInfoList(params,pagination);
        //   return    this.dao.queryList(params,pagination);
    }

    @Override
    public Goods queryInfoListById(Map<String, Object> params) {
        //   return this.dao.getAllGoods(param);
        return  this.dao.queryInfoListById(params);
    }
    //extends BaseServiceImpl<GoodsInfo,String,GoodsInfoDao> implements GoodsInfoService {


  /*  @Override
    public Pageable<Goods> secKillGoods(Map<String, Object> params, Pagination pagination) {
        return this.dao.secKillGoods(params, pagination);
    }
*/
    @Override
    public void upSecKillGoods(Map<String, Object> params) {
         this.dao.upSecKillGoods(params);
    }

    @Override
    public void upSecKillPeople(Map<String, Object> params) {
        this.dao.upSecKillPeople(params);
    }

    @Override
    public Goods getInfo(Map<String, Object> params) {
        return         this.dao.getInfo(params);
    }

    @Override
    public List<OnlineActivityInfoVo> getCurrentSceKillGoods(Map<String, Object> params) {
        return  this.dao.getCurrentSceKillGods(params);
    }

    @Override
    public GoodsInfoVO getOneByGoods(Map<String, Object> params) {
        return this.dao.getOneByGoods(params);
    }


    @Override
    public List<ProductAttribute> querySpecsById(Map<String, Object> param) {
        return   this.dao.querySpecsById(param);
    }

    @Override
    public List<ProductAttribute> queryAttrById(Map<String, Object> param) {
        return  this.dao.queryAttrById(param);
    }

    @Override
    public List<Goods> listAll(Map<String, Object> params) {
        return dao.listAll(params);
    }

    @Override
    public List<GoodsExo> listExo() {
        return dao.listExo();
    }

    @Override
    public List<GoodsFyExo> listExo2() {
        return dao.listExo2();
    }

    @Override
    public List<FygoodsExo> listall() {
        return dao.listall();
    }

    @Override
    public  List<Goods>  getCount(String id) {
        return dao.getCount(id);
    }

}

