package com.zhiwee.gree.service.support.OnlineActivity;

import com.zhiwee.gree.dao.OnlineActivity.PeopleInfoDao;
import com.zhiwee.gree.model.GoodsInfo.GoodVo.PeopleExr;
import com.zhiwee.gree.model.GoodsInfo.LAPeople;
import com.zhiwee.gree.service.OnlineActivity.PeopleInfoService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.List;
import java.util.Map;

@Service
public class PeopleInfoServiceImpl extends BaseServiceImpl<LAPeople, String, PeopleInfoDao> implements PeopleInfoService {

    @Override
    public Pageable<LAPeople> nghPeople(Map<String, Object> params, Pagination pagination) {
        return this.dao.nghPeople(params,pagination);
    }

    @Override
    public List<LAPeople> queryInfo(Map<String, Object> params) {
        return this.dao.queryInfo(params);
    }

    @Override
    public void upPeople(Map<String, Object> params) {
         this.dao.upPeople(params);
    }

    @Override
    public List<PeopleExr> exportPeople(Map<String, Object> params) {
        return          this.dao.exportPeople(params);
    }

    @Override
    public void updateInfo(Map<String, Object> params) {
        this.dao.updateInfo(params);
    }
}
