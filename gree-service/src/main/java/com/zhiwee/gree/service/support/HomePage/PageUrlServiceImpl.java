package com.zhiwee.gree.service.support.HomePage;

import com.zhiwee.gree.dao.HomePage.PageUrlDao;
import com.zhiwee.gree.model.HomePage.PageUrl;
import com.zhiwee.gree.service.HomePage.PageUrlService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.Map;

@Service
public class PageUrlServiceImpl  extends BaseServiceImpl<PageUrl,String, PageUrlDao> implements PageUrlService {
    @Override
    public Pageable<PageUrl> getPage(Map<String, Object> param, Pagination pagination) {
        return this.dao.getPage(param,pagination);
    }
}
