package com.zhiwee.gree.service.support.ShopUserComment;


import com.zhiwee.gree.dao.HomePage.HomePageDao;
import com.zhiwee.gree.dao.OrderInfo.OrderDao;
import com.zhiwee.gree.dao.ShopDistributor.DistributorOrderDao;
import com.zhiwee.gree.dao.ShopUserComment.CommentPictureDao;
import com.zhiwee.gree.dao.ShopUserComment.ShopUserCommentDao;
import com.zhiwee.gree.dao.goods.ShopGoodsInfoDao;
import com.zhiwee.gree.model.GoodsInfo.ShopGoodsInfo;
import com.zhiwee.gree.model.HomePage.HomePage;
import com.zhiwee.gree.model.OrderInfo.Order;
import com.zhiwee.gree.model.ShopDistributor.DistributorOrder;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.ShopUserComment.Comment;
import com.zhiwee.gree.model.ShopUserComment.CommentPicture;
import com.zhiwee.gree.model.ShopUserComment.ShopUserComment;
import com.zhiwee.gree.service.HomePage.HomePageService;
import com.zhiwee.gree.service.ShopUserComment.ShopUserCommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;
import xyz.icrab.common.web.session.Session;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class ShopUserCommentServiceImpl extends BaseServiceImpl<ShopUserComment,String,ShopUserCommentDao> implements ShopUserCommentService {


    @Autowired
    private OrderDao orderDao;

    @Autowired
    private CommentPictureDao commentPictureDao;

    @Autowired
    private DistributorOrderDao distributorOrderDao;

    @Autowired
    private ShopGoodsInfoDao shopGoodsInfoDao;
    @Override
    public Pageable<ShopUserComment> achieveComment(Map<String, Object> params, Pagination pagination) {
        return this.dao.achieveComment(params,pagination);
    }

    @Override
    public Pageable<ShopUserComment> achieveCommentByGoodsId(List<String> ids, Pagination pagination, Session user) {
        return this.dao.achieveCommentByGoodsId(ids,pagination,user);
    }

    @Transactional
    @Override
    public void saveAndUpdate(Order order, ShopUserComment shopUserComment, List<CommentPicture> commentPictures, ShopGoodsInfo shopGoodsInfo,List<DistributorOrder> distributorOrders) {
      if(distributorOrders.isEmpty() && distributorOrders.size() > 0){
          for(DistributorOrder distributorOrder : distributorOrders){
//              distributorOrder.setDeliveryState(3);
//              distributorOrder.setDeliveryTime(new Date());
//              distributorOrder.setCommentState(2);
              distributorOrderDao.update(distributorOrder);
          }
      }
//        Map<String,Object> params = new HashMap<>();
//        params.put("orderItemId",shopUserComment.getOrderItemId());
//        DistributorOrder distributorOrder = distributorOrderDao.selectOne(params);
//        distributorOrder.setCommentState(2);

        //更新商品的评论数量,这里的处理方式存在问题，事务隔离的影响，已消息队列的方式或者redis的队列实现不错

        shopGoodsInfoDao.update(shopGoodsInfo);

//        distributorOrderDao.update(distributorOrder);
         this.dao.insert(shopUserComment);
        orderDao.update(order);
        commentPictureDao.insertBatch(commentPictures);
    }

    @Override
    public Integer todayAmount(Map<String, Object> params) {
        return  this.dao.todayAmount(params);
    }

    @Override
    public   Pageable<ShopUserComment>  achieveMyComment(Map<String, Object> param, Pagination pagination) {
        return  this.dao.achieveMyComment(param,pagination);
    }
}
