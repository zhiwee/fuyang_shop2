package com.zhiwee.gree.service.support.Questionnaire;

import com.zhiwee.gree.dao.Questionnaire.QuestionnaireDao;
import com.zhiwee.gree.model.Questionnaire.Questionnaire;
import com.zhiwee.gree.service.Questionnaire.QuestionnaireService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.Map;

@Service
public class QuestionnaireServiceImpl  extends BaseServiceImpl<Questionnaire,String, QuestionnaireDao> implements QuestionnaireService {
    @Override
    public Pageable<Questionnaire> pageInfo(Map<String, Object> params, Pagination pagination) {
        return dao.pageInfo(params,pagination);
    }
}
