package com.zhiwee.gree.service.draw;

import com.zhiwee.gree.model.draw.DrawInfo;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface DrawInfoService extends BaseService<DrawInfo,String> {


    List<DrawInfo> checkTime(Map<String, Object> param);

    Pageable<DrawInfo> pageInfo(Map<String, Object> params, Pagination pagination);

    List<DrawInfo> getCurrent(Date date);

}
