package com.zhiwee.gree.service.draw;

import com.zhiwee.gree.model.draw.DrawUseInfo;
import com.zhiwee.gree.model.draw.DrawUseInfoExrt;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.service.BaseService;

import java.util.List;
import java.util.Map;

public interface DrawUseInfoService extends BaseService<DrawUseInfo,String> {

    Pageable<DrawUseInfo> pageInfo(Map<String, Object> params, Pagination pagination);

    /**
     * 导出
     * Chensong
     * @param params
     * @return
     */
    List<DrawUseInfoExrt> queryDrawUseInfoExport(Map<String,Object> params);


}
