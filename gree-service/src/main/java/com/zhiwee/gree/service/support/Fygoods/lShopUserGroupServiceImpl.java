package com.zhiwee.gree.service.support.Fygoods;

import com.zhiwee.gree.dao.FyGoods.LShopUserGroupDao;
import com.zhiwee.gree.model.FyGoods.LShopUserGroup;
import com.zhiwee.gree.service.FyGoods.LShopUserGroupService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.List;
import java.util.Map;

@Service
public class lShopUserGroupServiceImpl
        extends BaseServiceImpl<LShopUserGroup,String, LShopUserGroupDao> implements LShopUserGroupService {
    @Override
    public Pageable<LShopUserGroup> queryGoodsList(Map<String, Object> param, Pagination pagination) {
        return  this.dao.queryGoodsList(param,pagination);
    }

    @Override
    public List<LShopUserGroup> listAll(Map<String, Object> param) {
        return dao.listAll(param);
    }


}
