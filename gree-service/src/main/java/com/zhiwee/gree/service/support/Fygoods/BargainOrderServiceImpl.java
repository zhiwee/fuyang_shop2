package com.zhiwee.gree.service.support.Fygoods;

import com.zhiwee.gree.dao.FyGoods.BargainOrderDao;
import com.zhiwee.gree.model.FyGoods.BargainOrder;
import com.zhiwee.gree.service.FyGoods.BargainOrderService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.service.support.BaseServiceImpl;


@Service
public class BargainOrderServiceImpl
        extends BaseServiceImpl<BargainOrder,String, BargainOrderDao> implements BargainOrderService {


}
