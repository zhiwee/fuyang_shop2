package com.zhiwee.gree.service.support.tgoods;

import com.zhiwee.gree.dao.tgoods.GoodsLabelDao;
import com.zhiwee.gree.model.FyGoods.GoodsLabel;
import com.zhiwee.gree.service.tgoods.GoodsLabelService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.service.support.BaseServiceImpl;


/**
 * @author DELL
 */
@Service
public class GoodsLabelServiceImpl extends BaseServiceImpl<GoodsLabel, String, GoodsLabelDao> implements GoodsLabelService {


}
