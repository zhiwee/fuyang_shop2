package com.zhiwee.gree.service.support.goods;

import com.zhiwee.gree.dao.goods.GoodsCategoryDao;
import com.zhiwee.gree.dao.goods.GoodsCategoryImgDao;
import com.zhiwee.gree.model.GoodsInfo.GoodsCategory;
import com.zhiwee.gree.model.GoodsInfo.GoodsCategoryImg;
import com.zhiwee.gree.service.GoodsInfo.GoodsCategoryImgService;
import com.zhiwee.gree.service.GoodsInfo.GoodsCategoryService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.Map;

@Service
public class GoodsGategoryImgServiceImpl extends BaseServiceImpl<GoodsCategoryImg,String, GoodsCategoryImgDao> implements GoodsCategoryImgService {

    @Override
    public Pageable<GoodsCategoryImg> getPage(Map<String, Object> param, Pagination pagination) {
        return this.dao.getPage(param,pagination);
    }

    @Override
    public GoodsCategoryImg selectById(String id) {
        return this.dao.selectById(id);
    }

    @Override
    public GoodsCategoryImg getOneByCategoryId(String categoryId) {
        return this.dao.getOneByCategoryId(categoryId);
    }
}
