package com.zhiwee.gree.service.support.goods;

import com.zhiwee.gree.dao.goods.GoodsBandDao;
import com.zhiwee.gree.model.GoodsInfo.GoodsBand;
import com.zhiwee.gree.service.GoodsInfo.GoodsBandService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.Map;

@Service
public class GoodsBandServiceImpl extends BaseServiceImpl<GoodsBand,String, GoodsBandDao> implements GoodsBandService {
    @Override
    public Pageable<GoodsBand> getPage(Map<String, Object> param, Pagination pagination) {
        return this.dao.getPage(param,pagination);
    }
}
