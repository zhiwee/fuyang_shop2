package com.zhiwee.gree.service.GoodsInfo;


import com.zhiwee.gree.model.GoodsInfo.GoodsImage;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.List;
import java.util.Map;

public interface GoodsImageService extends BaseService<GoodsImage,String> {
    List<GoodsImage> getWebList(Map<String, Object> params);

    Pageable<GoodsImage> getPage(Map<String, Object> param, Pagination pagination);
}
