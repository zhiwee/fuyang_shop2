package com.zhiwee.gree.service.MemberGrade;


import com.zhiwee.gree.model.MemberGrade.MemberGrade;
import com.zhiwee.gree.model.MemberGrade.MemberGradeChange;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.Map;


public interface MemberGradeChangeService extends BaseService<MemberGradeChange,String> {


    Pageable<MemberGradeChange> searchAll(Map<String,Object> params, Pagination pagination);

}
