package com.zhiwee.gree.service.Dept;

import com.zhiwee.gree.model.DeptInfo.DeptInfo;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.Map;


/**
 * Created by jick on 2019/8/26.
 */
public interface DeptInfoService   extends BaseService<DeptInfo,String> {
    //extends BaseService<DeptopenInfo,String>
    // Pageable<DeptopenInfo>  queryDeptOpenInfo(Map<String,Object> param, Pagination pagination);

    Pageable<DeptInfo> pageInfo(Map<String, Object> param, Pagination pagination);
}
