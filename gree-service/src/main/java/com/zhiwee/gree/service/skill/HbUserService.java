package com.zhiwee.gree.service.skill;

import com.zhiwee.gree.model.skill.HbUser;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.List;
import java.util.Map;

public interface HbUserService extends BaseService<HbUser,String> {
    List<HbUser> queryGetHbList(Map<String,Object> param);
    //   Pageable<Skillhb>   queryList(Map<String, Object> params, Pagination pagination);
    Pageable<HbUser> queryList(Map<String, Object> params, Pagination pagination);
}
