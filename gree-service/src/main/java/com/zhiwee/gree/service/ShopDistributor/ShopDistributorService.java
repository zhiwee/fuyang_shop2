package com.zhiwee.gree.service.ShopDistributor;

import com.zhiwee.gree.model.Dealer.DealerInfo;
import com.zhiwee.gree.model.Dealer.DealerInfoBase;
import com.zhiwee.gree.model.ShopDistributor.ShopDistributor;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.List;
import java.util.Map;


public interface ShopDistributorService extends BaseService<ShopDistributor,String> {


    Pageable<ShopDistributor> infoPage(Map<String, Object> param, Pagination pagination);


    void saveAndUpdate(ShopDistributor shopDistributor,DealerInfoBase dealerInfo);

    void updateDealerInfo(ShopDistributor shopDistributor, DealerInfoBase dealerInfo);

    ShopDistributor defaultDistributor(Map<String,Object> mapkey);
}
