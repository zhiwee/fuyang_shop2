package com.zhiwee.gree.service.support.Information;

import com.zhiwee.gree.dao.Information.InformationDao;
import com.zhiwee.gree.model.Information.Information;
import com.zhiwee.gree.model.Invoice.Invoice;
import com.zhiwee.gree.service.Information.InformationService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.List;
import java.util.Map;

/**
 * Created by jick on 2019/7/18.
 */
@Service
public class InformationServiceImpl extends BaseServiceImpl<Information,String,InformationDao> implements InformationService {
   /* @Override
    public List<Information> queryInfoList() {
        return  this.dao.queryInfoList();
    }*/


   /*@Override
    public Pageable<Invoice> queryBaseInvoice(Map<String, Object> param, Pagination pagination) {
        return  this.dao.queryBaseInvoice(param,pagination);
    }*/

    @Override
    public Pageable<Information> queryInfoList(Pagination pagination,Map<String, Object> param) {
        return  this.dao.queryInfoList(pagination,param);
    }
    // extends BaseServiceImpl<Invoice,String,InvoiceDao> implements InvoiceService
}
