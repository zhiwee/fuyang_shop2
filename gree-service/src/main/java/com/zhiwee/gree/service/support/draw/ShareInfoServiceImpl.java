package com.zhiwee.gree.service.support.draw;

import com.zhiwee.gree.dao.draw.CanDrawsDao;
import com.zhiwee.gree.dao.draw.ShareInfoDao;
import com.zhiwee.gree.model.draw.CanDraws;
import com.zhiwee.gree.model.draw.ShareInfo;
import com.zhiwee.gree.service.draw.CanDrawsService;
import com.zhiwee.gree.service.draw.ShareInfoService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.service.support.BaseServiceImpl;


@Service
public class ShareInfoServiceImpl extends BaseServiceImpl<ShareInfo, String, ShareInfoDao> implements ShareInfoService {


}
