package com.zhiwee.gree.service.support.shopBase;

import com.zhiwee.gree.dao.shopBase.MenuDao;
import com.zhiwee.gree.model.Menu;
import com.zhiwee.gree.service.shopBase.MenuService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.List;
import java.util.Map;

@Service("menuService")
public class MenuServiceImpl extends BaseServiceImpl<Menu, String, MenuDao> implements MenuService {

    @Override
    public Pageable getMenuByUser(Map<String, Object> param, Pagination pagination) {
        return this.getDao().getMenuByUser(param, pagination);
    }

    @Override
    public List<Menu> getListMenuByUser(Map<String, Object> param) {
        return this.getDao().getMenuByUser(param);
    }

    @Override
    public List<Menu> getMenuByUserId(Map<String, Object> param) {
        return this.getDao().getMenuByUserId(param);
    }

    @Override
    public List<Menu> getMenuByRoleId(String roleid) {
        return this.getDao().getMenuByRoleId(roleid);
    }


    @Override
    public void addMenuRoleRef(Map<String, Object> param) {
        this.getDao().addMenuRoleRef(param);
    }

    @Override
    public void addMenuUserRef(Map<String, Object> param) {
        this.getDao().addMenuUserRef(param);
    }
}
