package com.zhiwee.gree.service.FyGoods;

import com.zhiwee.gree.model.FyGoods.PictureInfo;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.Map;

public interface PictureInfoService extends BaseService<PictureInfo,String> {
    Pageable<PictureInfo> queryGoodsList(Map<String, Object> param, Pagination pagination);
}
