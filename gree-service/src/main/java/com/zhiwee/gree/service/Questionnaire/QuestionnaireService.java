package com.zhiwee.gree.service.Questionnaire;

import com.zhiwee.gree.model.Questionnaire.Questionnaire;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.Map;

public interface QuestionnaireService extends BaseService<Questionnaire,String> {
    Pageable<Questionnaire> pageInfo(Map<String, Object> params, Pagination pagination);
}
