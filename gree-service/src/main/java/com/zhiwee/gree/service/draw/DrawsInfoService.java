package com.zhiwee.gree.service.draw;

import com.zhiwee.gree.model.draw.DrawsInfo;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.List;
import java.util.Map;

public interface DrawsInfoService extends BaseService<DrawsInfo,String> {


    List<DrawsInfo> checkAllNum(Map<String, Object> param);

    Pageable<DrawsInfo> pageInfo(Map<String, Object> params, Pagination pagination);

    List<DrawsInfo> listAll(Map<String, Object> param);
}
