package com.zhiwee.gree.service.OrderInfo;


import com.zhiwee.gree.model.OrderInfo.OrderAddress;
import com.zhiwee.gree.model.OrderInfo.OrderRefund;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.Map;


public interface OrderRefundService extends BaseService<OrderRefund,String> {


    Pageable<OrderRefund> pageInfo(Map<String,Object> params, Pagination pagination);
}
