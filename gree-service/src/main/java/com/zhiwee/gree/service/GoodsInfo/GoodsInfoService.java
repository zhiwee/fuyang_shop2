package com.zhiwee.gree.service.GoodsInfo;


import com.zhiwee.gree.model.GoodsInfo.GoodsInfo;
import com.zhiwee.gree.model.GoodsInfo.LAGoodsInfo;
import com.zhiwee.gree.model.GoodsInfo.LAPeople;
import com.zhiwee.gree.model.GoodsInfo.SolrGoodsInfo;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;
import java.util.List;
import java.util.Map;


public interface GoodsInfoService extends BaseService<GoodsInfo,String> {


    Pageable<GoodsInfo> searchAll(Map<String, Object> params, Pagination pagination);


    void updateActivityGoodsInfo(List<LAGoodsInfo> list);

    void updateActivityPeople(List<LAPeople> list);

    void updateActivityGoodsInfo2(List<LAGoodsInfo> list);
}
