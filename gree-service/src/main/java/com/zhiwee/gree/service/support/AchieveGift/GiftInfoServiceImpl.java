package com.zhiwee.gree.service.support.AchieveGift;


import com.zhiwee.gree.dao.AchieveGift.AchieveGiftDao;
import com.zhiwee.gree.dao.AchieveGift.GiftInfoDao;
import com.zhiwee.gree.model.AchieveGift.AchieveGift;
import com.zhiwee.gree.model.AchieveGift.GiftInfo;
import com.zhiwee.gree.service.AchieveGift.AchieveGiftService;
import com.zhiwee.gree.service.AchieveGift.GiftInfoService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.Map;


@Service
public class GiftInfoServiceImpl extends BaseServiceImpl<GiftInfo,String,GiftInfoDao> implements GiftInfoService {


    @Override
    public Pageable<GiftInfo> pageInfo(Map<String, Object> params, Pagination pagination) {

        return this.dao.pageInfo(params,pagination);
    }
}
