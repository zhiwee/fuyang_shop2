package com.zhiwee.gree.service.support.ShopDistributor;


import com.zhiwee.gree.dao.ShopDistributor.DistributorGradeDao;
import com.zhiwee.gree.dao.ShopDistributor.ShopDistributorDao;
import com.zhiwee.gree.model.ShopDistributor.DistributorGrade;
import com.zhiwee.gree.model.ShopDistributor.ShopDistributor;
import com.zhiwee.gree.service.ShopDistributor.DistributorGradeService;
import com.zhiwee.gree.service.ShopDistributor.ShopDistributorService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.Map;


@Service
public class DistributorGradeServiceImpl extends BaseServiceImpl<DistributorGrade, String, DistributorGradeDao> implements DistributorGradeService {




    @Override
    public Pageable<DistributorGrade> infoPage(Map<String, Object> param, Pagination pagination) {
        return dao.infoPage(param, pagination);
    }

    @Override
    public DistributorGrade selectMax(Map<String, Object> params) {
        return dao.selectMax(params);
    }


}
