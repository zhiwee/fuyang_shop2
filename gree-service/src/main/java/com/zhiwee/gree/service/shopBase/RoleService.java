package com.zhiwee.gree.service.shopBase;

import java.util.List;
import java.util.Map;

import com.zhiwee.gree.model.Role;
import com.zhiwee.gree.model.User;
import xyz.icrab.common.model.Relation;
import xyz.icrab.common.service.BaseService;

/**
 * @author gll
 * @since 2018/4/21 17:19
 */
public interface RoleService extends BaseService<Role, String> {
	/*
	 * 检查角色名有没有重名
	 */
	boolean checkName(String group);
    /*
	 * 停用启用
	 */
	void updateState(Map<String, Object> map);
	/*
	 * 添加用户角色数据
	 */
	void resetUserRole(String var1, List<String> var2);

    void saveUserRole(List<Relation> entities, User user);
	/*
	 * 根据userid获取用户角色
	 */
	List<Role> getRoleByUserId(String userid);
}
