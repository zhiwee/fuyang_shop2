package com.zhiwee.gree.service.FyGoods;

import com.zhiwee.gree.model.FyGoods.Fygoods;
import com.zhiwee.gree.model.FyGoods.FygoodsExo;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.List;
import java.util.Map;

public interface FygoodsService extends BaseService<Fygoods,String> {
    //extends BaseService<GoodCollect,String>
    Pageable<Fygoods> queryGoodsList(Map<String,Object> param, Pagination pagination);
    List<Fygoods>   portalQueryGoodsList(Map<String,Object> param);

    List<Fygoods> queryList(Map<String, Object> param);

    Fygoods queryInfoListById(Map<String, Object> param);

    List<FygoodsExo> listall();
}
