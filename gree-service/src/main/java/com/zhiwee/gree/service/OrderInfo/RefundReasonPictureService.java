package com.zhiwee.gree.service.OrderInfo;


import com.zhiwee.gree.model.OrderInfo.Order;
import com.zhiwee.gree.model.OrderInfo.OrderRefundReason;
import com.zhiwee.gree.model.OrderInfo.RefundReasonPicture;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.Map;


public interface RefundReasonPictureService extends BaseService<RefundReasonPicture,String> {


}
