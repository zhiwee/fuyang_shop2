package com.zhiwee.gree.service.support.HomePage;

import com.zhiwee.gree.dao.HomePage.PageChannelGoodsDao;
import com.zhiwee.gree.model.HomePage.PageChannelGoods;
import com.zhiwee.gree.service.HomePage.PageChannelGoodsService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.Map;

@Service
public class PageChannelGoodsServiceImpl extends BaseServiceImpl<PageChannelGoods,String, PageChannelGoodsDao> implements PageChannelGoodsService {
    @Override
    public Pageable<PageChannelGoods> getPage(Map<String, Object> param, Pagination pagination) {
        return this.dao.getPage(param,pagination);
    }
}
