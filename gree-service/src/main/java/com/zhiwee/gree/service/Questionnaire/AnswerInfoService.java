package com.zhiwee.gree.service.Questionnaire;

import com.zhiwee.gree.model.Questionnaire.AnswerInfo;
import com.zhiwee.gree.model.Questionnaire.AnswerInfoExrt;
import com.zhiwee.gree.model.Questionnaire.Questionnaire;
import com.zhiwee.gree.model.draw.DrawUseInfoExrt;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.List;
import java.util.Map;

public interface AnswerInfoService extends BaseService<AnswerInfo,String> {
    Pageable<AnswerInfo> pageInfo(Map<String, Object> params, Pagination pagination);

    /**
     * 导出
     * Chensong
     * @param params
     * @return
     */
    List<AnswerInfoExrt> queryAnswerInfoExport(Map<String,Object> params);

}
