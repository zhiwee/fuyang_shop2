package com.zhiwee.gree.service.GoodsInfo;

import com.zhiwee.gree.model.GoodsInfo.GoodsSpecsItem;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.Map;

public interface GoodsSpecsItemService extends BaseService<GoodsSpecsItem,String> {
    Pageable<GoodsSpecsItem> getPage(Map<String, Object> param, Pagination pagination);
}
