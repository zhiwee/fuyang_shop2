package com.zhiwee.gree.service.support.draw;

import com.zhiwee.gree.dao.draw.DrawInfoDao;
import com.zhiwee.gree.model.draw.DrawInfo;
import com.zhiwee.gree.service.draw.DrawInfoService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.Date;
import java.util.List;
import java.util.Map;


@Service("drawInfoService")
public class DrawInfoServiceImpl extends BaseServiceImpl<DrawInfo, String, DrawInfoDao> implements DrawInfoService {
    @Override
    public List<DrawInfo> checkTime(Map<String, Object> param) {
        return dao.checkTime(param);
    }

    @Override
    public Pageable<DrawInfo> pageInfo(Map<String, Object> params, Pagination pagination) {
     return     this.dao.pageInfo(params,pagination);
    }

    @Override
    public List<DrawInfo> getCurrent(Date date) {
        return dao.getCurrent(date);
    }


}
