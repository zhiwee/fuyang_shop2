package com.zhiwee.gree.service.support.HomePage;

import com.zhiwee.gree.dao.HomePage.PageSearchDao;
import com.zhiwee.gree.model.HomePage.PageSearch;
import com.zhiwee.gree.service.HomePage.PageSearchService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.List;
import java.util.Map;

@Service
public class PageSearchServiceImpl  extends BaseServiceImpl<PageSearch,String, PageSearchDao> implements PageSearchService {
    @Override
    public Pageable<PageSearch> getPage(Map<String, Object> param, Pagination pagination) {
        return this.dao.getPage(param,pagination);
    }

    @Override
    public PageSearch getById(String id) {
        return this.dao.getById(id);
    }

    @Override
    public List<PageSearch> getList() {
        return this.dao.getList();
    }
}
