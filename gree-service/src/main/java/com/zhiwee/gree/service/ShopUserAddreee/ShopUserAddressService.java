package com.zhiwee.gree.service.ShopUserAddreee;


import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.ShopUserAddress.ShopUserAddress;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;
import xyz.icrab.common.web.param.IdsParam;

import java.util.List;
import java.util.Map;


public interface ShopUserAddressService extends BaseService<ShopUserAddress,String> {

    void saveAndUpdate(ShopUserAddress shopUserAddress,ShopUserAddress defaultAddress);

    void updateAddress(ShopUserAddress shopUserAddress, ShopUser shopUser);
}
