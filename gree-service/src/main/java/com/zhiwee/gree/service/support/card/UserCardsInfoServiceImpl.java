package com.zhiwee.gree.service.support.card;

import com.zhiwee.gree.dao.crad.UserCardsInfoDao;
import com.zhiwee.gree.model.card.UserCardsInfo;
import com.zhiwee.gree.model.card.UserCardsInfoExrt;
import com.zhiwee.gree.service.card.UserCardsInfoService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.List;
import java.util.Map;


@Service
public class UserCardsInfoServiceImpl extends BaseServiceImpl<UserCardsInfo, String, UserCardsInfoDao> implements UserCardsInfoService {


    @Override
    public Pageable<UserCardsInfo> pageInfo(Map<String, Object> params, Pagination pagination) {
        return dao.pageInfo(params,pagination);
    }
    @Override
    public Pageable<UserCardsInfo> pageInfoNoCategory(Map<String, Object> params, Pagination pagination) {
        return dao.pageInfoNoCategory(params,pagination);
    }

    @Override
    public UserCardsInfo getCurrentOne(Map<String,Object> param) {
        return dao.getCurrentOne(param);
    }


    @Override
    public List<UserCardsInfoExrt> queryUserCardsInfoExport(Map<String, Object> params) {
        return dao.queryUserCardsInfoExport(params);
    }
    @Override
    public List<UserCardsInfoExrt> queryUserCardsInfoNoCategoryExport(Map<String, Object> params) {
        return dao.queryUserCardsInfoNoCategoryExport(params);
    }

    @Override
    public List<UserCardsInfo> queryUserCardsInfo(Map<String, Object> params) {
        return dao.queryUserCardsInfo(params);
    }
}
