package com.zhiwee.gree.service.support.GoodsLabel;


import com.zhiwee.gree.dao.GoodsLabel.PsDao;
import com.zhiwee.gree.dao.GoodsLabel.SpaceDao;
import com.zhiwee.gree.model.GoodsLabel.Ps;
import com.zhiwee.gree.model.GoodsLabel.Space;
import com.zhiwee.gree.service.GoodsLabel.PsService;
import com.zhiwee.gree.service.GoodsLabel.SpaceService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.service.support.BaseServiceImpl;


@Service
public class SpaceServiceImpl extends BaseServiceImpl<Space,String,SpaceDao> implements SpaceService {



}
