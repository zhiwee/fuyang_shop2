package com.zhiwee.gree.service.support.Fygoods;

import com.zhiwee.gree.dao.FyGoods.FyOrderItemDao;
import com.zhiwee.gree.model.FyGoods.FyOrderItem;
import com.zhiwee.gree.service.FyGoods.FyOrderItemService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.Map;

@Service
public class FyOrderItemServiceImpl extends
        BaseServiceImpl<FyOrderItem,String, FyOrderItemDao> implements FyOrderItemService {
    @Override
    public Pageable<FyOrderItem> queryOrderItemByNum(Map<String, Object> param, Pagination pagination) {
        return  this.dao.queryOrderItemByNum(param,pagination);
    }

    @Override
    public void deleteByGoodsId(Map<String, Object> map) {
         this.dao.deleteByGoodsId(map);
    }

    @Override
    public void deleteAll(Map<String, Object> params) {
        dao.deleteAll(params);
    }
}
