package com.zhiwee.gree.service.HomePage;



import com.zhiwee.gree.model.HomePage.HomePage;
import com.zhiwee.gree.model.HomePage.PcPlanmap;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;
import java.util.Map;


public interface HomePageService extends BaseService<HomePage,String> {

    Pageable<HomePage> achieveHomePage(Map<String,Object> params, Pagination pagination);


}
