package com.zhiwee.gree.service.support.card;

import com.zhiwee.gree.dao.crad.BookingCardsDao;
import com.zhiwee.gree.dao.crad.GiftCardUserDao;
import com.zhiwee.gree.model.card.BookingCards;
import com.zhiwee.gree.model.card.BookingCardsExrt;
import com.zhiwee.gree.model.card.GiftCardUser;
import com.zhiwee.gree.service.card.BookingCardsService;
import com.zhiwee.gree.service.card.GiftCardUserService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.List;
import java.util.Map;


@Service
public class GiftCardUserServiceImpl extends BaseServiceImpl<GiftCardUser, String, GiftCardUserDao> implements GiftCardUserService {


    @Override
    public Pageable<GiftCardUser> pageInfo(Map<String, Object> params, Pagination pagination) {
        return dao.pageInfo(params,pagination);
    }



}
