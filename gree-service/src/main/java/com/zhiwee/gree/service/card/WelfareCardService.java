package com.zhiwee.gree.service.card;


import com.zhiwee.gree.model.card.WelfareCard;

import com.zhiwee.gree.model.card.vo.WelfareCardExrt;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.List;
import java.util.Map;


public interface WelfareCardService extends BaseService<WelfareCard,String> {


    Pageable<WelfareCard> pageInfo(Map<String, Object> params, Pagination pagination);

    List<WelfareCardExrt> listAll(Map<String, Object> params);
}
