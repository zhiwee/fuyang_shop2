package com.zhiwee.gree.service.Activity;

import com.zhiwee.gree.model.Activity.ActivityInfo;
import com.zhiwee.gree.model.Activity.ActivityPage;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.Map;


/**
 * @author sun
 * @since 2019/7/6 14:24
 */
public interface ActivityPageService extends BaseService<ActivityPage, String> {

    Pageable<ActivityPage> pageInfo(Map<String,Object> param, Pagination pagination);
}
