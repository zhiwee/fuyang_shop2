package com.zhiwee.gree.service.support.OrderInfo;


import com.zhiwee.gree.dao.OrderInfo.OrderCouponDao;
import com.zhiwee.gree.dao.OrderInfo.OrderDao;
import com.zhiwee.gree.model.OrderInfo.Order;
import com.zhiwee.gree.model.OrderInfo.OrderCoupon;
import com.zhiwee.gree.service.OrderInfo.OrderCouponService;
import com.zhiwee.gree.service.OrderInfo.OrderService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.service.support.BaseServiceImpl;


@Service
public class OrderCouponServiceImpl extends BaseServiceImpl<OrderCoupon,String,OrderCouponDao> implements OrderCouponService {

}
