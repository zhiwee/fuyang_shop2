package com.zhiwee.gree.service.support.goods;

import com.zhiwee.gree.dao.goods.GoodsInfoPictureDao;
import com.zhiwee.gree.model.GoodsInfo.GoodsInfoPicture;
import com.zhiwee.gree.service.GoodsInfo.GoodsInfoPictureService;
import com.zhiwee.gree.service.GoodsInfo.GoodsInfoService;
import com.zhiwee.gree.service.support.BaseInfo.BaseInfoServiceImpl;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.List;
import java.util.Map;

@Service
public class GoodsInfoPictureServiceImpl  extends BaseServiceImpl<GoodsInfoPicture,String, GoodsInfoPictureDao>
        implements GoodsInfoPictureService {
    @Override
    public List<GoodsInfoPicture> listAll(Map<String, Object> param) {
        return dao.listAll(param);
    }

    @Override
    public Pageable<GoodsInfoPicture> pageInfo(Map<String, Object> params, Pagination pagination) {
        return dao.pageInfo(params,pagination);
    }

    @Override
    public GoodsInfoPicture getInfo(String id) {
        return dao.getInfo(id);
    }
    //extends BaseServiceImpl<GoodsImage,String, GoodsImageDao> implements GoodsImageService
}
