package com.zhiwee.gree.service.GoodsInfo;

import com.zhiwee.gree.model.GoodsInfo.GoodsCategoryImg;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.Map;

public interface GoodsCategoryImgService extends BaseService<GoodsCategoryImg,String> {

    Pageable<GoodsCategoryImg> getPage(Map<String, Object> param, Pagination pagination);

    GoodsCategoryImg selectById(String id);

    GoodsCategoryImg getOneByCategoryId(String categoryId);
}
