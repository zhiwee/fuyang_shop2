package com.zhiwee.gree.service.support.goods;


import com.zhiwee.gree.dao.goods.RetailGoodsPictureDao;
import com.zhiwee.gree.dao.goods.ShopGoodsPictureDao;
import com.zhiwee.gree.model.GoodsInfo.RetailGoodsPicture;
import com.zhiwee.gree.model.GoodsInfo.ShopGoodsPicture;
import com.zhiwee.gree.service.GoodsInfo.RetailGoodsPictureService;
import com.zhiwee.gree.service.GoodsInfo.ShopGoodsPictureService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.service.support.BaseServiceImpl;


@Service
public class RetailGoodsPictureServiceImpl extends BaseServiceImpl<RetailGoodsPicture,String,RetailGoodsPictureDao> implements RetailGoodsPictureService {




}
