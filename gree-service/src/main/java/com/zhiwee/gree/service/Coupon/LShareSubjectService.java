package com.zhiwee.gree.service.Coupon;


import com.zhiwee.gree.model.Coupon.Coupon;
import com.zhiwee.gree.model.Coupon.CouponExport;
import com.zhiwee.gree.model.Coupon.CouponSubject;
import com.zhiwee.gree.model.Coupon.LShareSubject;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.User;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.List;
import java.util.Map;


public interface LShareSubjectService extends BaseService<LShareSubject,String> {


}
