package com.zhiwee.gree.service.card;

import com.zhiwee.gree.model.card.GiftCard;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.Map;

public interface GiftCardService extends BaseService<GiftCard,String> {

    Pageable<GiftCard> pageInfo(Map<String, Object> params, Pagination pagination);

    GiftCard getInfo(GiftCard cardInfo);
}
