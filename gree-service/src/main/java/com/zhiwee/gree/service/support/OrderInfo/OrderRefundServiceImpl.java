package com.zhiwee.gree.service.support.OrderInfo;


import com.zhiwee.gree.dao.OrderInfo.OrderDao;
import com.zhiwee.gree.dao.OrderInfo.OrderItemDao;
import com.zhiwee.gree.dao.OrderInfo.OrderRefundDao;
import com.zhiwee.gree.model.OrderInfo.Order;
import com.zhiwee.gree.model.OrderInfo.OrderItem;
import com.zhiwee.gree.model.OrderInfo.OrderRefund;
import com.zhiwee.gree.service.OrderInfo.OrderItemService;
import com.zhiwee.gree.service.OrderInfo.OrderRefundService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.Map;


@Service
public class OrderRefundServiceImpl extends BaseServiceImpl<OrderRefund,String,OrderRefundDao> implements OrderRefundService {

    @Override
    public Pageable<OrderRefund> pageInfo(Map<String, Object> params, Pagination pagination) {
        return this.dao.pageInfo(params,pagination);
    }
}
