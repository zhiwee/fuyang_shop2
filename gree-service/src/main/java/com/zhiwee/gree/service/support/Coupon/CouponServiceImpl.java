package com.zhiwee.gree.service.support.Coupon;

import com.zhiwee.gree.dao.BaseInfo.BaseInfoDao;
import com.zhiwee.gree.dao.Coupon.CouponSubjectDao;
import com.zhiwee.gree.dao.goods.ShopGoodsInfoDao;
import com.zhiwee.gree.model.BaseInfo.BaseInfo;
import com.zhiwee.gree.model.Coupon.CouponSubject;
import com.zhiwee.gree.model.GoodsInfo.ShopGoodsInfo;
import com.zhiwee.gree.model.OrderInfo.OrderItem;
import com.zhiwee.gree.service.ShopUser.ShopUserService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import com.zhiwee.gree.dao.Coupon.CouponDao;
import com.zhiwee.gree.model.Coupon.Coupon;
import com.zhiwee.gree.model.Coupon.CouponExport;

import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.User;
import com.zhiwee.gree.service.Coupon.CouponService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;
import xyz.icrab.common.util.IdGenerator;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;


@Service
public class CouponServiceImpl extends BaseServiceImpl<Coupon, String, CouponDao> implements CouponService {

    @Resource
    private BaseInfoDao baseInfoDao;
    @Resource
    private ShopUserService shopUserService;
    @Autowired
    private ShopGoodsInfoDao shopGoodsInfoDao;

    @Autowired
    private CouponSubjectDao couponSubjectDao;


    @Transactional
    @Override
    public void generate(Coupon coupon, User user, CouponSubject couponSubject) {
        Random ra = new Random();
        List<Coupon> couponList = new ArrayList<>();
        for (int i = 0; i < coupon.getNum(); i++) {
            Coupon coupons = new Coupon();
            coupons.setId(IdGenerator.objectId());
            coupons.setType(coupon.getType());
            coupons.setCouponState(1);
            coupons.setPriceType(coupon.getPriceType());
            coupons.setReachMoney(coupon.getReachMoney());
            coupons.setStartTime(coupon.getStartTime());
            coupons.setEndTime(coupon.getEndTime());
            coupons.setIdPath(coupon.getIdPath());
            coupons.setSubjectId(couponSubject.getId());
            coupons.setSubjectName(couponSubject.getName());
            coupons.setCouponMoney(coupon.getCouponMoney());
            String day = DateFormatUtils.format(new Date(), "yyyyMMdd");
            coupons.setCouponSeqno(day + "" + ra.nextInt(9999999));
            coupons.setPassword(ra.nextInt(9999999) + "");
            coupons.setCreateTime(new Date());
            coupons.setCreateMan(user.getId());
            coupons.setState(1);
            coupons.setSendState(1);
            couponList.add(coupons);
        }
        dao.insertBatch(couponList);
        couponSubjectDao.insert(couponSubject);

    }

    @Override
    public Pageable<Coupon> pageInfo(Map<String, Object> param, Pagination pagination) {
        return dao.pageInfo(param, pagination);
    }

    @Override
    public List<CouponExport> exportCoupon(Map<String, Object> params) {
        return dao.exportCoupon(params);
    }

    @Override
    public Coupon getCouponById(String id) {
        return dao.getCouponById(id);
    }

    @Transactional
    @Override
    public void getCoupon(ShopUser user, Coupon coupon,double integral) {
        Map<String, Object> params = new HashMap<>();
        params.put("couponState", new Object[]{1});
        params.put("type", coupon.getType());
        params.put("sendState", "1");
        List<Coupon> coupons = this.dao.selectList(params);
        if (coupons != null && coupons.size() > 0) {
            Coupon userCoupon = coupons.get(0);
            userCoupon.setSendState(2);
            userCoupon.setUserId(user.getId());
            userCoupon.setUserName(user.getNickname());
            userCoupon.setUserPhone(user.getPhone());
            this.dao.update(userCoupon);
            user.setIntegral(user.getIntegral()-integral);
            shopUserService.update(user);
        }

    }



    @Override
    public void deleteList(Map<String, Object> delId) {
        dao.deleteList(delId);
    }


    @Override
    public void updateAll(Map<String, Object> param) {
        dao.updateAll(param);
    }

    @Override
    public List<Coupon> listAll(Map<String, Object> params) {
        return dao.listAll(params);
    }
}