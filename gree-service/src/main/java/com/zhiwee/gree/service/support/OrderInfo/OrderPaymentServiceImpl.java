package com.zhiwee.gree.service.support.OrderInfo;


import com.zhiwee.gree.dao.OrderInfo.OrderDao;
import com.zhiwee.gree.dao.OrderInfo.OrderPaymentDao;
import com.zhiwee.gree.model.OrderInfo.Order;
import com.zhiwee.gree.model.OrderInfo.OrderPayment;
import com.zhiwee.gree.service.OrderInfo.OrderPaymentService;
import com.zhiwee.gree.service.OrderInfo.OrderService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.service.support.BaseServiceImpl;


@Service
public class OrderPaymentServiceImpl extends BaseServiceImpl<OrderPayment,String,OrderPaymentDao> implements OrderPaymentService {

}
