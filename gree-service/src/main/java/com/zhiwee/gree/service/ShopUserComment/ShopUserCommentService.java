package com.zhiwee.gree.service.ShopUserComment;


import com.zhiwee.gree.model.GoodsInfo.ShopGoodsInfo;
import com.zhiwee.gree.model.OrderInfo.Order;
import com.zhiwee.gree.model.ShopDistributor.DistributorOrder;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.ShopUserComment.Comment;
import com.zhiwee.gree.model.ShopUserComment.CommentPicture;
import com.zhiwee.gree.model.ShopUserComment.ShopUserComment;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;
import xyz.icrab.common.web.session.Session;

import java.util.List;
import java.util.Map;


public interface ShopUserCommentService extends BaseService<ShopUserComment,String> {

    Pageable<ShopUserComment> achieveComment(Map<String, Object> params, Pagination pagination);

    Pageable<ShopUserComment>achieveCommentByGoodsId(List<String> ids, Pagination pagination, Session user);

    void saveAndUpdate(Order order, ShopUserComment shopUserComment, List<CommentPicture> commentPictures, ShopGoodsInfo shopGoodsInfo, List<DistributorOrder> distributorOrders);

    Integer todayAmount(Map<String,Object> params);

    Pageable<ShopUserComment>  achieveMyComment(Map<String,Object> param, Pagination pagination);
}
