package com.zhiwee.gree.service.Balance;


import com.zhiwee.gree.model.Balance.BalanceReCharge;
import com.zhiwee.gree.model.Balance.BalanceWithdrawal;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.List;
import java.util.Map;


public interface BalanceWithdrawalService extends BaseService<BalanceWithdrawal,String> {

    Pageable<BalanceWithdrawal> pageInfo(Map<String, Object> param, Pagination pagination);


    List<BalanceWithdrawal> balanceRecord(Map<String,Object> params);
}
