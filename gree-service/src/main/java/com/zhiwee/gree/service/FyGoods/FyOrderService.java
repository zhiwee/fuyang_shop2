package com.zhiwee.gree.service.FyGoods;

import com.zhiwee.gree.model.FyGoods.FyOrder;
import com.zhiwee.gree.model.FyGoods.vo.FyOrderVo;
import com.zhiwee.gree.model.OrderInfo.vo.OrderStatisticItem;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.List;
import java.util.Map;

public interface FyOrderService  extends BaseService<FyOrder,String> {
    //extends BaseService<Fygoods,String>
    //  Pageable<Fygoods> queryGoodsList(Map<String,Object> param, Pagination pagination);
    Pageable<FyOrder> queryOrderList(Map<String,Object> param, Pagination pagination);

    void updateByOrderNum(Map<String, Object> params);

    void deleteByOrderNum(Map<String, Object> params);

    void updateSelective(Map<String, Object> map);

    Pageable<FyOrder> refoundOrderList(Map<String, Object> param, Pagination pagination);

    List<FyOrder> queryPayOrderList(Map<String, Object> map);

    List<FyOrder> queryList(Map<String, Object> map);

    List<FyOrderVo> queryExportOrderList(Map<String, Object> params);

    List<OrderStatisticItem> statisticAllAmountByDate(Map<String, Object> param);

    Object qidoGoodsNameNumTopTen(Map<String, Object> param);

    Object qidoGoodsNameMoneyTopTen(Map<String, Object> param);

    Object orderAmount(Map<String, Object> params);

    Double totalPrice(Map<String, Object> param);

    List<FyOrder> queryPayOrderList2(Map<String, Object> map);
}
