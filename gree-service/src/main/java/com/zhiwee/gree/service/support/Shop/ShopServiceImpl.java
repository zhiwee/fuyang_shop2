package com.zhiwee.gree.service.support.Shop;

import com.zhiwee.gree.dao.Shop.ShopDao;
import com.zhiwee.gree.model.Shop.Shop;
import com.zhiwee.gree.service.Shop.ShopService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.Map;

/**
 * @author jiake
 * @date 2020-02-19 15:03
 */
@Service
public class ShopServiceImpl extends BaseServiceImpl<Shop,String, ShopDao>
        implements ShopService {
    @Override
    public Pageable<Shop> queryList(Map<String, Object> param, Pagination pagination) {
        //return null;
        // return  this.dao.queryList(params,pagination);
        return  this.dao.queryList(param,pagination);
    }
    // BaseServiceImpl<AnswerInfo,String, AnswerInfoDao> implements AnswerInfoServic




}
