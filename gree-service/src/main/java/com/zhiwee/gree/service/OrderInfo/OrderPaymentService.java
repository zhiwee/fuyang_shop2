package com.zhiwee.gree.service.OrderInfo;


import com.zhiwee.gree.model.OrderInfo.Order;
import com.zhiwee.gree.model.OrderInfo.OrderPayment;
import xyz.icrab.common.service.BaseService;


public interface OrderPaymentService extends BaseService<OrderPayment,String> {

}
