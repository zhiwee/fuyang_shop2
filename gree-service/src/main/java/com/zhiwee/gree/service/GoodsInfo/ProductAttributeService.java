package com.zhiwee.gree.service.GoodsInfo;

import com.zhiwee.gree.model.GoodsInfo.ProductAttribute;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.List;
import java.util.Map;

/**
 * Created by jick on 2019/8/28.
 */
public interface ProductAttributeService   extends BaseService<ProductAttribute,String> {
    //GoodsInfoService extends BaseService<GoodsInfo,String> {
    //queryList
    //    Pageable<GoodsInfo> searchAll(Map<String, Object> params, Pagination pagination);
    Pageable<ProductAttribute>   queryList(Map<String, Object> params, Pagination pagination);

    List<ProductAttribute> selectBycategoryId(Map<String, Object> param);



}
