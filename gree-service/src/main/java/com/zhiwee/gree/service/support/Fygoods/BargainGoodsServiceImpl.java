package com.zhiwee.gree.service.support.Fygoods;

import com.zhiwee.gree.dao.FyGoods.BargainGoodsDao;
import com.zhiwee.gree.dao.FyGoods.ShareGoodsDao;
import com.zhiwee.gree.model.FyGoods.BargainGoods;
import com.zhiwee.gree.model.FyGoods.ShareGoods;
import com.zhiwee.gree.service.FyGoods.BargainGoodsService;
import com.zhiwee.gree.service.FyGoods.ShareGoodsService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.List;
import java.util.Map;

@Service
public class BargainGoodsServiceImpl
        extends BaseServiceImpl<BargainGoods,String, BargainGoodsDao> implements BargainGoodsService {
    @Override
    public List<BargainGoods> queryGoodsList(Map<String, Object> param, Pagination pagination) {
        return  this.dao.queryGoodsList(param,pagination);
    }


}
