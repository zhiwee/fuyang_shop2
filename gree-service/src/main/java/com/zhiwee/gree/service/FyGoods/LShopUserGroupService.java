package com.zhiwee.gree.service.FyGoods;

import com.zhiwee.gree.model.FyGoods.LShopUserGroup;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.List;
import java.util.Map;

public interface LShopUserGroupService extends BaseService<LShopUserGroup,String> {
    Pageable<LShopUserGroup> queryGoodsList(Map<String, Object> param, Pagination pagination);

    List<LShopUserGroup> listAll(Map<String, Object> param);
}
