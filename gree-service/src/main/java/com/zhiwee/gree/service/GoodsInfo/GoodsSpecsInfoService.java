package com.zhiwee.gree.service.GoodsInfo;

import com.zhiwee.gree.model.GoodsInfo.GoodsSpecsInfo;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.Map;

public interface GoodsSpecsInfoService extends BaseService<GoodsSpecsInfo,String> {
    Pageable<GoodsSpecsInfo> getPage(Map<String, Object> param, Pagination pagination);

    Pageable<GoodsSpecsInfo> getItemPage(Map<String, Object> param, Pagination pagination);
}
