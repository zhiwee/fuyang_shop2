package com.zhiwee.gree.service.HomePage;

import com.zhiwee.gree.model.HomePage.PageChannelKeyword;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.Map;

public interface PageChannelKeywordService extends BaseService<PageChannelKeyword,String> {
    Pageable<PageChannelKeyword> getPage(Map<String, Object> param, Pagination pagination);
}
