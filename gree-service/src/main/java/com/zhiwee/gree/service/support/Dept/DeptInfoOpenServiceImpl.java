package com.zhiwee.gree.service.support.Dept;

import com.zhiwee.gree.dao.Dept.DeptopenInfoDao;
import com.zhiwee.gree.model.DeptInfo.DeptopenInfo;
import com.zhiwee.gree.service.Dept.DeptopenInfoService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.Map;

/**
 * Created by jick on 2019/8/23.
 */
@Service
public class DeptInfoOpenServiceImpl extends BaseServiceImpl<DeptopenInfo,String,DeptopenInfoDao> implements DeptopenInfoService {
    @Override
    public Pageable<DeptopenInfo> queryDeptOpenInfo(Map<String, Object> param, Pagination pagination) {
        //
        // return  this.dao.queryBaseInvoice(param,pagination);
        return    this.dao.queryDeptOpenInfo(param,pagination);
    }
}
