package com.zhiwee.gree.service.support.Balance;


import com.zhiwee.gree.dao.Balance.BalanceReChargeDao;
import com.zhiwee.gree.dao.Balance.BalanceWithdrawalDao;
import com.zhiwee.gree.model.Balance.BalanceReCharge;
import com.zhiwee.gree.model.Balance.BalanceWithdrawal;
import com.zhiwee.gree.service.Balance.BalanceReChargeService;
import com.zhiwee.gree.service.Balance.BalanceWithdrawalService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.List;
import java.util.Map;


@Service
public class BalanceWithdrawalServiceImpl extends BaseServiceImpl<BalanceWithdrawal,String,BalanceWithdrawalDao> implements BalanceWithdrawalService {




    @Override
    public Pageable<BalanceWithdrawal> pageInfo(Map<String, Object> param, Pagination pagination) {
        return dao.pageInfo(param, pagination);
    }

    @Override
    public List<BalanceWithdrawal> balanceRecord(Map<String, Object> params) {
        return dao.balanceRecord(params);
    }


}
