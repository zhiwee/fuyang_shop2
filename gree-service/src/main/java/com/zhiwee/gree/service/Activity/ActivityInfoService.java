package com.zhiwee.gree.service.Activity;

import com.zhiwee.gree.model.Activity.ActivityInfo;
import xyz.icrab.common.service.BaseService;

import java.util.List;
import java.util.Map;


/**
 * @author sun
 * @since 2018/7/6 14:24
 */
public interface ActivityInfoService extends BaseService<ActivityInfo, String> {

    List<ActivityInfo> checkTime(Map<String, Object> param);

    ActivityInfo getCurrent(Map<String, Object> param);
}
