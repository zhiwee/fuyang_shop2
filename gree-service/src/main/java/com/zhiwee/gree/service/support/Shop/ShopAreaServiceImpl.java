package com.zhiwee.gree.service.support.Shop;

import com.zhiwee.gree.dao.Shop.ShopAreaDao;
import com.zhiwee.gree.model.Shop.ShopArea;
import com.zhiwee.gree.service.Shop.ShopAreaService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.service.support.BaseServiceImpl;

/**
 * @author jiake
 * @date 2020-02-19 12:57
 */
@Service
public class ShopAreaServiceImpl extends BaseServiceImpl<ShopArea,Integer, ShopAreaDao> implements ShopAreaService {


    // BaseServiceImpl<AnswerInfo,String, AnswerInfoDao> implements AnswerInfoServic

}
