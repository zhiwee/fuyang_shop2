package com.zhiwee.gree.service.support.Dealer;


import com.zhiwee.gree.dao.Dealer.DealerInfoBaseDao;

import com.zhiwee.gree.model.Dealer.DealerInfoBase;
import com.zhiwee.gree.service.Dealer.DealerInfoBaseService;

import org.springframework.stereotype.Service;

import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.List;
import java.util.Map;


@Service
public class DealerInfoBaseServiceImpl extends BaseServiceImpl<DealerInfoBase, String, DealerInfoBaseDao> implements DealerInfoBaseService {


    @Override
    public List<DealerInfoBase> simpleList(Map<String, Object> param) {
        return dao.simpleList(param);
    }

    @Override
    public Pageable<DealerInfoBase> infoPage(Map<String, Object> param, Pagination pagination) {
        return dao.infoPage(param, pagination);
    }
}
