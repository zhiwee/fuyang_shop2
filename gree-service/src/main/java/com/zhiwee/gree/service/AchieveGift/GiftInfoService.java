package com.zhiwee.gree.service.AchieveGift;


import com.zhiwee.gree.model.AchieveGift.AchieveGift;
import com.zhiwee.gree.model.AchieveGift.GiftInfo;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.Map;


public interface GiftInfoService extends BaseService<GiftInfo,String> {

    Pageable<GiftInfo> pageInfo(Map<String, Object> params, Pagination pagination);
}
