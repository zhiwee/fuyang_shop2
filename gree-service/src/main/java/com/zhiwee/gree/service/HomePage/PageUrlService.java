package com.zhiwee.gree.service.HomePage;

import com.zhiwee.gree.model.HomePage.PageUrl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.Map;

public interface PageUrlService  extends BaseService<PageUrl,String> {
    Pageable<PageUrl> getPage(Map<String, Object> param, Pagination pagination);
}
