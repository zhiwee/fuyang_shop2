package com.zhiwee.gree.service.OrderInfo;


import com.zhiwee.gree.model.Activity.ActivityInfo;
import com.zhiwee.gree.model.ActivityTicketOrder.ActivityTicketOrder;
import com.zhiwee.gree.model.Analysis.ItemAnalysis;
import com.zhiwee.gree.model.Coupon.Coupon;
import com.zhiwee.gree.model.GoodsInfo.GoodsInfo;
import com.zhiwee.gree.model.GoodsInfo.SolrGoodsInfo;
import com.zhiwee.gree.model.OnlineActivity.OnlineActivityInfo;
import com.zhiwee.gree.model.OrderInfo.Order;
import com.zhiwee.gree.model.OrderInfo.OrderAddress;
import com.zhiwee.gree.model.OrderInfo.OrderExport;
import com.zhiwee.gree.model.OrderInfo.vo.OrderStatisticItem;
import com.zhiwee.gree.model.ShopDistributor.DistributorOrder;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.service.BaseService;

import java.util.List;
import java.util.Map;


public interface OrderService extends BaseService<Order,String> {

 

    void createFyOrder(Order order, ShopUser shopUser);

    Pageable<Order> pageInfo(Map<String, Object> params, Pagination pagination);
}
