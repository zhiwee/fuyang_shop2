package com.zhiwee.gree.service.FyGoods;

import com.zhiwee.gree.model.FyGoods.FyGoodsImg;
import xyz.icrab.common.service.BaseService;

import java.util.List;
import java.util.Map;

public interface FygoodsImgService extends BaseService<FyGoodsImg,String> {
    List<FyGoodsImg> queryImgList(Map<String, Object> param);
}
