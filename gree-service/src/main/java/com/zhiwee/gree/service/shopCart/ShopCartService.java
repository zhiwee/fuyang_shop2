package com.zhiwee.gree.service.shopCart;

import com.zhiwee.gree.model.GoodsInfo.GoodsInfo;
import com.zhiwee.gree.model.GoodsInfo.ShopGoodsInfo;
import com.zhiwee.gree.model.Log.Log;
import xyz.icrab.common.service.BaseService;

import java.util.List;

/**
 * @author sun on 2019/3/28
 */
public interface ShopCartService {


    Boolean addCart(String  userId, String itemId, int num);
    Boolean mergeCart(String userId, List<ShopGoodsInfo> itemList);
    List<ShopGoodsInfo> getCartList(String userId);
    Boolean updateCartNum(String userId, String itemId, int num);
    Boolean deleteCartItem(String userId, String itemId);
    Boolean clearCartItem(String  userId);
}
