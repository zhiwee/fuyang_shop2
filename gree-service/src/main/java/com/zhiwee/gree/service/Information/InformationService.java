package com.zhiwee.gree.service.Information;

import com.zhiwee.gree.model.Information.Information;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.List;
import java.util.Map;

/**
 * Created by jick on 2019/7/18.
 */
public interface InformationService  extends BaseService<Information,String> {
    //extends BaseService<Invoice,String>
   // List<Information> queryInfoList();
    //Pageable<Invoice> queryBaseInvoice(Map<String,Object> param, Pagination pagination);
    Pageable<Information> queryInfoList(Pagination pagination,Map<String,Object> param);
}
