package com.zhiwee.gree.service.support.OrderInfo;


import com.zhiwee.gree.dao.OrderInfo.*;
import com.zhiwee.gree.dao.ShopDistributor.DistributorAreaDao;
import com.zhiwee.gree.dao.ShopDistributor.DistributorOrderDao;
import com.zhiwee.gree.dao.ShopDistributor.ShopDistributorDao;
import com.zhiwee.gree.model.Analysis.ItemAnalysis;
import com.zhiwee.gree.model.OrderInfo.*;
import com.zhiwee.gree.model.OrderInfo.vo.GoodNameTopTen;
import com.zhiwee.gree.model.ShopDistributor.DistributorArea;
import com.zhiwee.gree.model.ShopDistributor.DistributorOrder;
import com.zhiwee.gree.model.ShopDistributor.ShopDistributor;
import com.zhiwee.gree.service.OrderInfo.OrderItemService;
import com.zhiwee.gree.service.OrderInfo.OrderService;
import com.zhiwee.gree.util.SequenceUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;
import xyz.icrab.common.util.IdGenerator;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class OrderItemServiceImpl extends BaseServiceImpl<OrderItem,String,OrderItemDao> implements OrderItemService {
    @Autowired
    private OrderDao orderDao;

    @Autowired
    private OrderAddressDao orderAddressDao;


    @Autowired
    private DistributorOrderDao distributorOrderDao;


    @Autowired
    private ShopDistributorDao shopDistributorDao;

    @Autowired
    private DistributorAreaDao distributorAreaDao;

    @Autowired
    private SequenceUtils sequenceUtils;


    @Autowired
    private OrderChangeDao orderChangeDao;


    @Autowired
    private OrderRefundReasonDao orderRefundReasonDao;


    @Override
    public Pageable<OrderItem> orderItemList(Map<String, Object> params, Pagination pagination) {
        return dao.orderItemList(params,pagination);
    }
   @Transactional
    @Override
    public void updateOrderAndItem(OrderItem item, Order order,Boolean flag,OrderChange orderChange) {
       OrderAddress  orderAddress = new OrderAddress();
        if(flag == true){
            Map<String,Object> param = new HashMap<>();
            param.put("orderId",order.getId());
            orderAddress =orderAddressDao.selectOne(param);
            DistributorOrder distributorOrder = new DistributorOrder();
            distributorOrder.setOrderItemId(item.getId());
            distributorOrderDao.delete(distributorOrder);

            Map<String,Object> mapkey = new HashMap<>();
            mapkey.put("defaultDistributor",1);
            ShopDistributor shopDistributor = shopDistributorDao.defaultDistributor(mapkey);
            if(shopDistributor != null && shopDistributor.getCategoryIdPath() != null && !"".equals(shopDistributor.getCategoryIdPath())){
                if(StringUtils.contains(shopDistributor.getCategoryIdPath(), item.getCategoryId())){
                    distributorOrder =  new DistributorOrder();
                    distributorOrder.setId(IdGenerator.objectId());
                    distributorOrder.setDistributorId(shopDistributor.getId());
                    distributorOrder.setOrderId(order.getId());
                    distributorOrder.setDeliveryState(2);
                    distributorOrder.setOrderItemId(item.getId());
                    distributorOrder.setState(1);
                    distributorOrder.setSendNum(sequenceUtils.sendNumByDay());
                    distributorOrder.setCreateTime(new Date());
                    distributorOrderDao.insert(distributorOrder);
                }else{
                    Map<String,Object> params = new HashMap<>();
                    params.put("category",item.getCategoryId());
                    params.put("areaId",orderAddress.getAreaCode());
                    params.put("state",1);
                    List<DistributorArea> shopDistributors = distributorAreaDao.searchByCategory(params);
                    if(shopDistributors != null && shopDistributors.size() > 0){
                        distributorOrder =  new DistributorOrder();
                        distributorOrder.setId(IdGenerator.objectId());
                        distributorOrder.setDistributorId(shopDistributors.get(0).getDistributorId());
                        distributorOrder.setDeliveryState(2);
                        distributorOrder.setOrderId(order.getId());
                        distributorOrder.setOrderItemId(item.getId());
                        distributorOrder.setState(1);
                        distributorOrder.setSendNum(sequenceUtils.sendNumByDay());
                        distributorOrder.setCreateTime(new Date());
                        distributorOrderDao.insert(distributorOrder);
                    }else{
                        distributorOrder =  new DistributorOrder();
                        distributorOrder.setId(IdGenerator.objectId());
                        distributorOrder.setDistributorId(shopDistributor.getId());
                        distributorOrder.setOrderId(order.getId());
                        distributorOrder.setDeliveryState(2);
                        distributorOrder.setOrderItemId(item.getId());
                        distributorOrder.setSendNum(sequenceUtils.sendNumByDay());
                        distributorOrder.setState(1);
                        distributorOrder.setCreateTime(new Date());
                        distributorOrderDao.insert(distributorOrder);
                    }
                }
            }

        }
       /**
        * 获取申请记录,修改申请记录，改成同意
        *
        */
       Map<String, Object> refundParams = new HashMap<>();
       refundParams.put("orderId", order.getId());
       refundParams.put("state", 1);
       OrderRefundReason orderRefundReasonCopy = null;
       List<OrderRefundReason> orderRefundReasons = orderRefundReasonDao.selectList(refundParams);
       if (!orderRefundReasons.isEmpty() && orderRefundReasons.size() > 0) {
           orderRefundReasonCopy = orderRefundReasons.get(0);
           for (OrderRefundReason orr : orderRefundReasons) {
               orr.setState(3);
               orr.setUpdateTime(new Date());
               orderRefundReasonDao.update(orr);
           }
       }


       /**
        * 差价
        * 正数为 系统需要补给客户的钱
        *
        * 负数 是客户需要补给系统的钱
        * 如果是商品的的种类或者商品的数量改变就去插入记录
        * 前提是订单支付了
        *
        * 申请退款状态  1 未申请退换货  2 申请退货中  3已允许退货，4拒绝退货 5 申请换货中 6允许换货 7 拒绝换货  8申请退款中  9 同意退款 10 拒绝退款
        */
        if(order.getOrderState() == 2){
            if(!orderChange.getNewGood().equals(orderChange.getOldGood()) || orderChange.getOldNum() != orderChange.getNewNum()){
                order.setRefundState(6);
                if(orderRefundReasonCopy != null){
                    orderChange.setRefundId(orderRefundReasonCopy.getId());
                }
                orderChange.setId(IdGenerator.objectId());
                orderChange.setOrderNum(sequenceUtils.orderChangeByDay());
                orderChange.setChangePrice(orderChange.getOldRealPrice() - orderChange.getNewRealPrice());
                orderChangeDao.insert(orderChange);
            }

        }






       this.dao.update(item);
        orderDao.update(order);

    }

    @Override
    public Pageable<OrderRecord> achieveOrderRecord(List<String> ids, Pagination pagination) {
        return dao.achieveOrderRecord(ids,pagination);
    }

    @Override
    public List<OrderItem> achieveItemsByOrderId(Map<String, Object> orderId) {
        return dao.achieveItemsByOrderId(orderId);
    }

    @Override
    public List<GoodNameTopTen> topTen() {
        return dao.topTen();
    }

    /**
     * 如果订单中的商品明细已经不存在了，连同地址以及订单全部删除
     * @param params
     */
    @Transactional
    @Override
    public void deleteAll(Map<String, Object> params) {
        OrderAddress orderAddress = new OrderAddress();
        orderAddress.setOrderId((String)params.get("orderId"));
        Order order = new Order();
        order.setId((String)params.get("orderId"));
        orderAddressDao.delete(orderAddress);
        orderDao.delete(order);
    }

    @Override
    public ItemAnalysis itemAnalysis(Map<String,Object> params) {
        return this.dao.itemAnalysis(params);
    }
}
