package com.zhiwee.gree.service.OrderInfo;


import com.zhiwee.gree.model.Analysis.ItemAnalysis;
import com.zhiwee.gree.model.OrderInfo.Order;
import com.zhiwee.gree.model.OrderInfo.OrderChange;
import com.zhiwee.gree.model.OrderInfo.OrderItem;
import com.zhiwee.gree.model.OrderInfo.OrderRecord;
import com.zhiwee.gree.model.OrderInfo.vo.GoodNameTopTen;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.List;
import java.util.Map;


public interface OrderItemService extends BaseService<OrderItem,String> {

    Pageable<OrderItem> orderItemList(Map<String,Object> params, Pagination pagination);

    void updateOrderAndItem(OrderItem item, Order order,Boolean flag,OrderChange orderChange);

    Pageable<OrderRecord> achieveOrderRecord(List<String> ids, Pagination pagination);

    List<OrderItem> achieveItemsByOrderId(Map<String,Object> orderId);

    List<GoodNameTopTen> topTen();

    void deleteAll(Map<String,Object> params);

    ItemAnalysis itemAnalysis(Map<String,Object> params);
}
