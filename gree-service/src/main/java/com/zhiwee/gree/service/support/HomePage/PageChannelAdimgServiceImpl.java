package com.zhiwee.gree.service.support.HomePage;

import com.zhiwee.gree.dao.HomePage.PageChannelAdimgDao;
import com.zhiwee.gree.model.HomePage.PageChannelAdimg;
import com.zhiwee.gree.service.HomePage.PageChannelAdimgService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.Map;

@Service
public class PageChannelAdimgServiceImpl extends BaseServiceImpl<PageChannelAdimg,String, PageChannelAdimgDao> implements PageChannelAdimgService {
    @Override
    public Pageable<PageChannelAdimg> getPage(Map<String, Object> param, Pagination pagination) {
        return this.dao.getPage(param,pagination);
    }
}
