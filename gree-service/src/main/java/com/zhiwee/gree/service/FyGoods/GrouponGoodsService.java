package com.zhiwee.gree.service.FyGoods;

import com.zhiwee.gree.model.FyGoods.GrouponGoods;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.List;
import java.util.Map;

public interface GrouponGoodsService extends BaseService<GrouponGoods,String> {
    List<GrouponGoods> queryGoodsList(Map<String, Object> param, Pagination pagination);
}
