package com.zhiwee.gree.service.support.goods;


import com.zhiwee.gree.dao.goods.GoodsInfoDao;
import com.zhiwee.gree.model.GoodsInfo.GoodsInfo;
import com.zhiwee.gree.model.GoodsInfo.LAGoodsInfo;
import com.zhiwee.gree.model.GoodsInfo.LAPeople;
import com.zhiwee.gree.model.GoodsInfo.SolrGoodsInfo;
import com.zhiwee.gree.service.GoodsInfo.GoodsInfoService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;
import java.util.List;
import java.util.Map;


@Service
public class GoodsInfoServiceImpl extends BaseServiceImpl<GoodsInfo,String,GoodsInfoDao> implements GoodsInfoService {

//    @Override
//    public List<SolrGoodsInfo> getAllGoods() {
//
//        return this.dao.getAllGoods();
//    }

    @Override
    public Pageable<GoodsInfo> searchAll(Map<String, Object> params, Pagination pagination) {
        return this.dao.searchAll(params,pagination);
    }

    @Override
    public void updateActivityGoodsInfo(List<LAGoodsInfo> list) {
        this.dao.updateActivityGoodsInfo(list);
    }

    @Override
    public void updateActivityGoodsInfo2(List<LAGoodsInfo> list) {
        this.dao.updateActivityGoodsInfo2(list);
    }

    @Override
    public void updateActivityPeople(List<LAPeople> list) {
        this.dao.updateActivityPeople(list);
    }
}
