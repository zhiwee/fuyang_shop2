package com.zhiwee.gree.service.support.goods;

import com.zhiwee.gree.dao.goods.GoodsCategoryDao;
import com.zhiwee.gree.model.GoodsInfo.GoodVo.GategoryVo;
import com.zhiwee.gree.model.GoodsInfo.GoodVo.GoodsCategoryCY;
import com.zhiwee.gree.model.GoodsInfo.GoodsCategory;
import com.zhiwee.gree.model.HomePage.PageV0.PageChannelGoodsVO;
import com.zhiwee.gree.model.ShopDistributor.CategoryVo;
import com.zhiwee.gree.service.GoodsInfo.GoodsCategoryService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.List;
import java.util.Map;

@Service
public class GoodsGategoryServiceImpl extends BaseServiceImpl<GoodsCategory,String, GoodsCategoryDao> implements GoodsCategoryService {
    @Override
    public Pageable<GoodsCategory> getPage(Map<String, Object> param, Pagination pagination) {
        return this.dao.getPage(param,pagination);
    }

    @Override
    public GoodsCategory selectByid(String id) {
        return this.dao.selectByid(id);
    }

    @Override
    public List<GategoryVo> getVoList(Map<String, Object> params) {
        return this.dao.getVoList(params);
    }

    @Override
    public List<PageChannelGoodsVO> getCategoryGoods(Map<String, Object> params) {
        return this.dao.getCategoryGoods(params);
    }

    @Override
    public List<GoodsCategoryCY> listInfo() {
        return dao.listInfo();
    }

    @Override
    public List<GoodsCategory>listChildren(Map<String, Object> params) {
        return dao.listChildren(params);
    }
}
