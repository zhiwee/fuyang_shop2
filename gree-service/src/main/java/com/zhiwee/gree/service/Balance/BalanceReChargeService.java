package com.zhiwee.gree.service.Balance;


import com.zhiwee.gree.model.Balance.BalanceReCharge;
import com.zhiwee.gree.model.BaseInfo.BaseInfo;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.List;
import java.util.Map;


public interface BalanceReChargeService extends BaseService<BalanceReCharge,String> {

    Pageable<BalanceReCharge> pageInfo(Map<String, Object> param, Pagination pagination);


    void updateBalance(BalanceReCharge order);

    List<BalanceReCharge> balanceRecord(Map<String,Object> params);
}
