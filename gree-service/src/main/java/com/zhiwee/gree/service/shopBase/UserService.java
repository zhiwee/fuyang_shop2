package com.zhiwee.gree.service.shopBase;


import com.zhiwee.gree.model.IdentifiedUser;
import com.zhiwee.gree.model.User;
import com.zhiwee.gree.model.UserExport;
import com.zhiwee.gree.model.enums.UserType;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;
import xyz.icrab.common.web.param.IdsParam;

import java.util.List;
import java.util.Map;

/**
 * @author Knight
 * @since 2018/4/11 17:19
 */
public interface UserService extends BaseService<User, String> {

    /**
     * 根据用户认证凭证获取用户信息
     * @param username 用户名
     * @param password 密码
     * @param userTypes 用户类型
     * @return 如果凭证校验成功返回该用户，否则返回null
     */
    IdentifiedUser login(String username, String password, UserType... userTypes);
    Pageable<User> page(Map<String, Object> param, Pagination pagination);

    List<UserExport> exportUser(Map<String,Object> param);

    User getMyRoleName(Map<String,Object> param);

    void deleteUser(IdsParam param);

    User searchOne(Map<String,Object> param);


    void updateAndDelete(User user);
}
