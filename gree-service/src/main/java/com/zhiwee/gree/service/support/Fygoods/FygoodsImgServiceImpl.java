package com.zhiwee.gree.service.support.Fygoods;

import com.zhiwee.gree.dao.FyGoods.FygoodsImgDao;
import com.zhiwee.gree.model.FyGoods.FyGoodsImg;
import com.zhiwee.gree.service.FyGoods.FygoodsImgService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.List;
import java.util.Map;

@Service
public class FygoodsImgServiceImpl
        extends BaseServiceImpl<FyGoodsImg,String, FygoodsImgDao> implements FygoodsImgService {
    @Override
    public List<FyGoodsImg> queryImgList(Map<String, Object> param) {
        return this.dao.queryImgList(param);
    }
}
