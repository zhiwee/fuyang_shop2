package com.zhiwee.gree.service.ShopDistributor;

import com.zhiwee.gree.model.ShopDistributor.DistributorArea;
import com.zhiwee.gree.model.ShopDistributor.DistributorCategory;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.List;
import java.util.Map;


public interface DistributorCategoryService extends BaseService<DistributorCategory,String> {


    Pageable<DistributorCategory> achieveSmallCategory(Map<String,Object> param, Pagination pagination);
}
