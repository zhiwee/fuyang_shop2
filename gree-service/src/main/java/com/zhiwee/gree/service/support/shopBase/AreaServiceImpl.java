package com.zhiwee.gree.service.support.shopBase;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.zhiwee.gree.dao.shopBase.AreaDao;
import com.zhiwee.gree.model.Area;
import com.zhiwee.gree.service.shopBase.AreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.List;
import java.util.Map;


@Service("areaService")
public class AreaServiceImpl extends BaseServiceImpl<Area,String,AreaDao> implements AreaService {
    @Override
    public List<Area> ahtree(Map<String, Object> params) {
        return this.dao.ahtree(params);
    }

    @Override
    public Pageable<Area> ahPage(Map<String, Object> params, Pagination pagination) {
        return this.dao.ahPage(params, pagination);
    }

    @Override
    public Pageable<Area> allProvince(Map<String, Object> params, Pagination pagination) {
        return this.dao.allProvince(params, pagination);
    }
}
