package com.zhiwee.gree.service.card;

import com.zhiwee.gree.model.card.CardInfo;
import com.zhiwee.gree.model.card.DepartmentInfo;
import com.zhiwee.gree.model.card.DepartmentInfoCY;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.List;
import java.util.Map;

public interface DepartmentInfoService extends BaseService<DepartmentInfo,String> {


    Pageable<DepartmentInfo> pageInfo(Map<String, Object> params, Pagination pagination);

    List<DepartmentInfoCY> listAll();
}
