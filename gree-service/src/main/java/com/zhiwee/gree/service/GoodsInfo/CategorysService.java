package com.zhiwee.gree.service.GoodsInfo;

import com.zhiwee.gree.model.GoodsInfo.Categorys;
import com.zhiwee.gree.model.GoodsInfo.GoodVo.CategorysExo;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.List;
import java.util.Map;


/**
 * @author DELL
 */
public interface CategorysService extends BaseService<Categorys,String> {

    Pageable<Categorys> getPage(Map<String, Object> param, Pagination pagination);

    List<CategorysExo> listExo();
}
