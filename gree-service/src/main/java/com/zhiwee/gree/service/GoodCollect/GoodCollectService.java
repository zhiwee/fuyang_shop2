package com.zhiwee.gree.service.GoodCollect;


import com.zhiwee.gree.model.Dealer.DealerInfoBase;
import com.zhiwee.gree.model.GoodCollect.GoodCollect;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.List;
import java.util.Map;


public interface GoodCollectService extends BaseService<GoodCollect,String> {




    Pageable<GoodCollect> infoPage(Map<String, Object> param, Pagination pagination);


}
