package com.zhiwee.gree.service.support.GoodsLabel;


import com.zhiwee.gree.dao.GoodsLabel.BpDao;
import com.zhiwee.gree.dao.GoodsLabel.SpaceDao;
import com.zhiwee.gree.model.GoodsLabel.Bp;
import com.zhiwee.gree.model.GoodsLabel.Space;
import com.zhiwee.gree.service.GoodsLabel.BpService;
import com.zhiwee.gree.service.GoodsLabel.SpaceService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.service.support.BaseServiceImpl;


@Service
public class BpServiceImpl extends BaseServiceImpl<Bp,String,BpDao> implements BpService {



}
