package com.zhiwee.gree.service.support.HomePage;

import com.zhiwee.gree.dao.HomePage.PageNavDao;
import com.zhiwee.gree.model.HomePage.PageNav;
import com.zhiwee.gree.service.HomePage.PageNavService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.List;
import java.util.Map;

@Service
public class PageNavServiceImpl extends BaseServiceImpl<PageNav,String, PageNavDao> implements PageNavService {
    @Override
    public PageNav getById(String id) {
        return this.dao.getById(id);
    }

    @Override
    public Pageable<PageNav> getPage(Map<String, Object> param, Pagination pagination) {
        return this.dao.getPage(param,pagination);
    }

    @Override
    public List<PageNav> getlist() {
        return this.dao.getList();
    }
}
