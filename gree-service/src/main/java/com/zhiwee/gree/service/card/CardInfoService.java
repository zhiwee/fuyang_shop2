package com.zhiwee.gree.service.card;

import com.zhiwee.gree.model.card.CardInfo;
import com.zhiwee.gree.model.card.LShopUserCards;
import com.zhiwee.gree.model.card.LShopUserCardsExrt;
import com.zhiwee.gree.model.draw.DrawUseInfo;
import com.zhiwee.gree.model.draw.DrawsInfo;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.List;
import java.util.Map;

public interface CardInfoService extends BaseService<CardInfo,String> {


    Pageable<CardInfo> pageInfo(Map<String, Object> params, Pagination pagination);

    CardInfo getInfo(CardInfo cardInfo);

}
