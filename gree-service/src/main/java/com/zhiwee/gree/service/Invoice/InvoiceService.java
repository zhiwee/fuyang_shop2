package com.zhiwee.gree.service.Invoice;

import com.zhiwee.gree.model.Invoice.Invoice;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.List;
import java.util.Map;

/**
 * Created by jick on 2019/7/16.
 */
public interface InvoiceService extends BaseService<Invoice,String> {
    List<Invoice> queryInvoice(Map<String,Object> param);
    Pageable<Invoice> queryBaseInvoice(Map<String,Object> param, Pagination pagination);
    //Pageable<HomePage> achieveHomePage(Map<String,Object> params, Pagination pagination);
    Invoice queryBaseInvoiceById(Map<String,Object> param);

}
