package com.zhiwee.gree.service.support.GoodCollect;


import com.zhiwee.gree.dao.Dealer.DealerInfoBaseDao;
import com.zhiwee.gree.dao.GoodCollect.GoodCollectDao;
import com.zhiwee.gree.model.Dealer.DealerInfoBase;
import com.zhiwee.gree.model.GoodCollect.GoodCollect;
import com.zhiwee.gree.service.Dealer.DealerInfoBaseService;
import com.zhiwee.gree.service.GoodCollect.GoodCollectService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.List;
import java.util.Map;


@Service
public class GoodCollectServiceImpl extends BaseServiceImpl<GoodCollect, String, GoodCollectDao> implements GoodCollectService {



    @Override
    public Pageable<GoodCollect> infoPage(Map<String, Object> param, Pagination pagination) {
        return dao.infoPage(param, pagination);
    }
}
