package com.zhiwee.gree.service.support.OrderInfo;


import com.zhiwee.gree.dao.BaseInfo.BaseInfoDao;
import com.zhiwee.gree.dao.Coupon.CouponDao;
import com.zhiwee.gree.dao.Integral.IntegralDao;
import com.zhiwee.gree.dao.MemberGrade.MemberGradeChangeDao;
import com.zhiwee.gree.dao.MemberGrade.MemberGradeDao;
import com.zhiwee.gree.dao.OrderInfo.*;
import com.zhiwee.gree.dao.ShopDistributor.DistributorGradeChangeDao;
import com.zhiwee.gree.dao.ShopDistributor.DistributorGradeDao;
import com.zhiwee.gree.dao.ShopDistributor.DistributorOrderDao;
import com.zhiwee.gree.dao.ShopDistributor.ShopDistributorDao;
import com.zhiwee.gree.dao.ShopUser.ShopUserDao;
import com.zhiwee.gree.dao.ShopUserAddress.ShopUserAddressDao;
import com.zhiwee.gree.model.ActivityTicketOrder.ActivityTicketOrder;
import com.zhiwee.gree.model.BaseInfo.BaseInfo;
import com.zhiwee.gree.model.Coupon.Coupon;
import com.zhiwee.gree.model.GoodsInfo.ShopGoodsInfo;
import com.zhiwee.gree.model.Integral.Integral;
import com.zhiwee.gree.model.MemberGrade.MemberGrade;
import com.zhiwee.gree.model.MemberGrade.MemberGradeChange;
import com.zhiwee.gree.model.OrderInfo.*;
import com.zhiwee.gree.model.OrderInfo.vo.OrderStatisticItem;
import com.zhiwee.gree.model.ShopDistributor.DistributorGrade;
import com.zhiwee.gree.model.ShopDistributor.DistributorGradeChange;
import com.zhiwee.gree.model.ShopDistributor.DistributorOrder;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.ShopUserAddress.ShopUserAddress;
import com.zhiwee.gree.service.GoodsInfo.ShopGoodsInfoService;
import com.zhiwee.gree.service.OrderInfo.OrderChangeService;
import com.zhiwee.gree.service.OrderInfo.OrderService;
import com.zhiwee.gree.util.SequenceUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;
import xyz.icrab.common.util.IdGenerator;

import java.math.BigDecimal;
import java.util.*;


@Service
public class OrderChangeServiceImpl extends BaseServiceImpl<OrderChange, String, OrderChangeDao> implements OrderChangeService {


    @Override
    public Pageable<OrderChange> pageInfo(Map<String, Object> params, Pagination pagination) {
        return this.dao.pageInfo(params, pagination);
    }


}

