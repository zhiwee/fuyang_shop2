package com.zhiwee.gree.service.support.card;


import com.zhiwee.gree.dao.crad.WelfareCardDao;
import com.zhiwee.gree.model.card.WelfareCard;
import com.zhiwee.gree.model.card.vo.WelfareCardExrt;
import com.zhiwee.gree.service.card.WelfareCardService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.List;
import java.util.Map;


@Service
public class WelfareCardServiceImpl extends BaseServiceImpl<WelfareCard, String, WelfareCardDao> implements WelfareCardService {


    @Override
    public Pageable<WelfareCard> pageInfo(Map<String, Object> params, Pagination pagination) {
        return dao.pageInfo(params,pagination);
    }

    @Override
    public List<WelfareCardExrt> listAll(Map<String, Object> params) {
        return dao.listAll(params);
    }
}
