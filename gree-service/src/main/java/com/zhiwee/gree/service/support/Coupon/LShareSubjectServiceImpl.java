package com.zhiwee.gree.service.support.Coupon;

import com.zhiwee.gree.dao.BaseInfo.BaseInfoDao;
import com.zhiwee.gree.dao.Coupon.CouponDao;
import com.zhiwee.gree.dao.Coupon.CouponSubjectDao;
import com.zhiwee.gree.dao.Coupon.LShareSubjectDao;
import com.zhiwee.gree.dao.goods.ShopGoodsInfoDao;
import com.zhiwee.gree.model.Coupon.Coupon;
import com.zhiwee.gree.model.Coupon.CouponExport;
import com.zhiwee.gree.model.Coupon.CouponSubject;
import com.zhiwee.gree.model.Coupon.LShareSubject;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.User;
import com.zhiwee.gree.service.Coupon.CouponService;
import com.zhiwee.gree.service.Coupon.LShareSubjectService;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;
import xyz.icrab.common.util.IdGenerator;

import java.util.*;


@Service
public class LShareSubjectServiceImpl extends BaseServiceImpl<LShareSubject, String, LShareSubjectDao> implements LShareSubjectService {


}