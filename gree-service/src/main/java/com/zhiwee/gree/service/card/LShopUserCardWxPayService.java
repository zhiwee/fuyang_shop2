package com.zhiwee.gree.service.card;

import com.zhiwee.gree.model.card.LShopUserCardWxPayInfo;
import com.zhiwee.gree.model.card.LShopUserCardWxPayInfoExrt;
import com.zhiwee.gree.model.card.LShopUserCards;
import com.zhiwee.gree.model.card.LShopUserCardsExrt;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.List;
import java.util.Map;

/**
 * TODO
 *
 * @author Chensong
 * @Company http://www.zhiwee.com
 * date 2020-01-03
 */
public interface LShopUserCardWxPayService  extends BaseService<LShopUserCardWxPayInfo,String> {

    Pageable<LShopUserCardWxPayInfo> getLShopUserCardWxPayList(Map<String, Object> params, Pagination pagination);

    List<LShopUserCardWxPayInfoExrt> queryLShopUserCardWxPayExport(Map<String, Object> params);

}
