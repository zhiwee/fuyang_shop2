package com.zhiwee.gree.service.support.Dealer;


import com.zhiwee.gree.dao.Dealer.DealerInfoBaseDao;
import com.zhiwee.gree.dao.Dealer.DealerInfoDao;
import com.zhiwee.gree.model.Dealer.DealerInfo;
import com.zhiwee.gree.model.Dealer.DealerInfoBase;
import com.zhiwee.gree.service.Dealer.DealerInfoBaseService;
import com.zhiwee.gree.service.Dealer.DealerInfoService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.List;
import java.util.Map;


@Service
public class DealerInfoServiceImpl extends BaseServiceImpl<DealerInfo, String, DealerInfoDao> implements DealerInfoService {


}
