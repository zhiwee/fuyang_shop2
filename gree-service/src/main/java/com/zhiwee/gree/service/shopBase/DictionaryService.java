package com.zhiwee.gree.service.shopBase;

import com.zhiwee.gree.model.Dictionary;
import xyz.icrab.common.service.BaseService;

import java.util.Map;


/**
 * @author gll
 * @since 2018/4/21 17:19
 */
public interface DictionaryService extends BaseService<Dictionary, String> {
	/*
	 * 检查分组有没有重名
	 */
	boolean checkGroup(String group);
    /*
	 * 检查key有没有重名
	 */
	boolean checkKey(String key, String group);
    /*
	 * 停用启用
	 */
	void updateState(Map<String, Object> map);
}
