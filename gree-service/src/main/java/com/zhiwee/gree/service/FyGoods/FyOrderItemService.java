package com.zhiwee.gree.service.FyGoods;

import com.zhiwee.gree.model.FyGoods.FyOrderItem;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.Map;

public interface FyOrderItemService extends BaseService<FyOrderItem,String> {
    Pageable<FyOrderItem> queryOrderItemByNum(Map<String,Object> param, Pagination pagination);

    void deleteByGoodsId(Map<String, Object> map);

    void deleteAll(Map<String, Object> params);
    // List<FyOrderItem> queryOrderItemByNum(Map<String,Object> param);
}
