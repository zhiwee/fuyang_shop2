package com.zhiwee.gree.service.support.ShopUserAddress;


import com.zhiwee.gree.dao.ShopUser.ShopUserDao;
import com.zhiwee.gree.dao.ShopUserAddress.ShopUserAddressDao;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.ShopUserAddress.ShopUserAddress;
import com.zhiwee.gree.service.ShopUser.ShopUserService;
import com.zhiwee.gree.service.ShopUserAddreee.ShopUserAddressService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;
import xyz.icrab.common.web.param.IdsParam;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class ShopUserAddressServiceImpl extends BaseServiceImpl<ShopUserAddress,String,ShopUserAddressDao> implements ShopUserAddressService {

   @Transactional
    @Override
    public void saveAndUpdate(ShopUserAddress shopUserAddress, ShopUserAddress defaultAddress) {
       if(shopUserAddress.getIsDefault() != 1 || defaultAddress == null){
           this.dao.insert(shopUserAddress);
       }else if(shopUserAddress.getIsDefault() == 1 && defaultAddress != null){
           defaultAddress.setIsDefault(2);
           this.dao.update(defaultAddress);
           this.dao.insert(shopUserAddress);
       }else{
           this.dao.insert(shopUserAddress);
       }


    }
    @Transactional
    @Override
    public void updateAddress(ShopUserAddress shopUserAddress, ShopUser shopUser) {
       //本身原来的值
       ShopUserAddress address =  this.dao.select(shopUserAddress.getId());
        Map<String,Object> params = new HashMap<>();
        params.put("userId",shopUser.getId());
        params.put("isDefault",1);
        ShopUserAddress defaultAddress = this.dao.selectOne(params);
       if(address.getIsDefault() == 1){
           this.dao.update(shopUserAddress);
       }else if(defaultAddress == null){
           this.dao.update(shopUserAddress);
       }else if(shopUserAddress.getIsDefault() == 1 && defaultAddress != null  && !address.getId().equals(defaultAddress.getId())){
           defaultAddress.setIsDefault(2);
           this.dao.update(defaultAddress);
           this.dao.update(shopUserAddress);
       }else{
           this.dao.update(shopUserAddress);
       }

    }
}

