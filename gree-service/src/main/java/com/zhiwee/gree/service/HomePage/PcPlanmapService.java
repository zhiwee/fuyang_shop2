package com.zhiwee.gree.service.HomePage;

import com.zhiwee.gree.model.HomePage.PcPlanmap;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.List;
import java.util.Map;

public interface PcPlanmapService extends BaseService<PcPlanmap,String> {
    Pageable<PcPlanmap> getPage(Map<String, Object> params, Pagination pagination);

    PcPlanmap getById(String id);

    List<PcPlanmap> getList(Map<String, Object> params);
}
