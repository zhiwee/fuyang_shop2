package com.zhiwee.gree.service.ActivityTicketOrder;

import com.zhiwee.gree.model.ActivityTicketOrder.ActivityTicketOrder;
import com.zhiwee.gree.model.ActivityTicketOrder.vo.TicketOrderStatisticItem;
import xyz.icrab.common.service.BaseService;

import java.util.List;
import java.util.Map;


/**
 * @author sun
 * @since 2018/7/9 14:33
 */
public interface ActivityTicketOrderService extends BaseService<ActivityTicketOrder, String> {

    List<TicketOrderStatisticItem> ticketOrderByDate(Map<String,Object> param);
}
