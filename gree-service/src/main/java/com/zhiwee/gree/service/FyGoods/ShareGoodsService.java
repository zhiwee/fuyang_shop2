package com.zhiwee.gree.service.FyGoods;

import com.zhiwee.gree.model.FyGoods.ShareGoods;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.List;
import java.util.Map;

public interface ShareGoodsService extends BaseService<ShareGoods,String> {
    List<ShareGoods> queryGoodsList(Map<String, Object> param, Pagination pagination);
}
