package com.zhiwee.gree.service.support.ActivityTicketOrder;
import com.zhiwee.gree.dao.ActivityTicketOrder.ActivityTicketOrderDao;
import com.zhiwee.gree.dao.ActivityTicketOrder.TicketInfoDao;
import com.zhiwee.gree.dao.ActivityTicketOrder.TicketOrderDao;
import com.zhiwee.gree.model.ActivityTicketOrder.ActivityTicketOrder;
import com.zhiwee.gree.model.ActivityTicketOrder.TicketInfo;
import com.zhiwee.gree.model.ActivityTicketOrder.TicketOrder;
import com.zhiwee.gree.model.ActivityTicketOrder.TicketOrderExport;
import com.zhiwee.gree.service.ActivityTicketOrder.TicketInfoService;
import com.zhiwee.gree.service.ActivityTicketOrder.TicketOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;
import xyz.icrab.common.util.IdGenerator;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author sun
 * @since 2019/6/23
 */
@Service
public class TicketInfoServiceImpl extends BaseServiceImpl<TicketInfo, String, TicketInfoDao> implements TicketInfoService {

}
