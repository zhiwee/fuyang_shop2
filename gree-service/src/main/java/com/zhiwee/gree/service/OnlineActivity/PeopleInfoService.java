package com.zhiwee.gree.service.OnlineActivity;

import com.zhiwee.gree.model.GoodsInfo.GoodVo.PeopleExr;
import com.zhiwee.gree.model.GoodsInfo.LAPeople;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.List;
import java.util.Map;



public interface PeopleInfoService  extends BaseService<LAPeople, String> {
    Pageable<LAPeople> nghPeople(Map<String, Object> params, Pagination pagination);

    List<LAPeople> queryInfo(Map<String, Object> params);

    void upPeople(Map<String, Object> params);

    List<PeopleExr> exportPeople(Map<String, Object> params);

    void updateInfo(Map<String, Object> params);
}
