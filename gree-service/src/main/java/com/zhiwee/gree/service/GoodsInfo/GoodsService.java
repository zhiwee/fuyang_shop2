package com.zhiwee.gree.service.GoodsInfo;

import com.zhiwee.gree.model.FyGoods.FygoodsExo;
import com.zhiwee.gree.model.GoodsInfo.GoodVo.GoodsExo;
import com.zhiwee.gree.model.GoodsInfo.GoodVo.GoodsFyExo;
import com.zhiwee.gree.model.GoodsInfo.GoodVo.GoodsInfoVO;
import com.zhiwee.gree.model.GoodsInfo.Goods;
import com.zhiwee.gree.model.GoodsInfo.LAGoodsInfo;
import com.zhiwee.gree.model.GoodsInfo.ProductAttribute;
import com.zhiwee.gree.model.OnlineActivity.OnlineActivityInfo;
import com.zhiwee.gree.model.OnlineActivity.vo.OnlineActivityInfoVo;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.List;
import java.util.Map;

/**
 * Created by jick on 2019/8/30.
 */
public interface GoodsService  extends BaseService<Goods,String> {
    //extends BaseService<GoodsInfo,String> {
    //queryInfoList
    // Pageable<ProductAttribute>   queryList(Map<String, Object> params, Pagination pagination);
    Pageable<Goods> queryInfoList(Map<String, Object> params, Pagination pagination);

    Goods queryInfoListById(Map<String,Object> params);

  //  Pageable<Goods> secKillGoods(Map<String, Object> params, Pagination pagination);

    void upSecKillGoods(Map<String, Object> params);

    void upSecKillPeople(Map<String, Object> params);

    Goods getInfo(Map<String, Object> params);

    List<OnlineActivityInfoVo> getCurrentSceKillGoods(Map<String, Object> params);

    GoodsInfoVO getOneByGoods(Map<String, Object> params);

    List<ProductAttribute> querySpecsById(Map<String,Object> param);

    List<ProductAttribute> queryAttrById(Map<String,Object> param);


    List<Goods> listAll(Map<String, Object> params);

    List<GoodsExo> listExo();

    List<GoodsFyExo> listExo2();

    List<FygoodsExo> listall();

    List<Goods>  getCount(String id);
}
