package com.zhiwee.gree.service.Coupon;


import com.zhiwee.gree.model.Coupon.Coupon;
import com.zhiwee.gree.model.Coupon.CouponExport;
import com.zhiwee.gree.model.Coupon.CouponSubject;
import com.zhiwee.gree.model.GoodsInfo.GoodsInfo;
import com.zhiwee.gree.model.GoodsInfo.SolrGoodsInfo;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.User;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.List;
import java.util.Map;


public interface CouponService extends BaseService<Coupon,String> {

    //,String sign
    void generate(Coupon coupon, User user, CouponSubject couponSubject);

    Pageable<Coupon> pageInfo(Map<String,Object> param, Pagination pagination);

    List<CouponExport> exportCoupon(Map<String,Object> params);

    Coupon getCouponById(String id);

    void getCoupon(ShopUser user,Coupon coupon,double integral);



    void deleteList(Map<String, Object> delId);

    void updateAll(Map<String, Object> param);

    List<Coupon> listAll(Map<String, Object> params);
}
