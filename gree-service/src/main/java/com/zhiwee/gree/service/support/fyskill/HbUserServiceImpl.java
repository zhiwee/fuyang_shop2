package com.zhiwee.gree.service.support.fyskill;

import com.zhiwee.gree.dao.skill.HbUserDao;
import com.zhiwee.gree.model.skill.HbUser;
import com.zhiwee.gree.service.skill.HbUserService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.List;
import java.util.Map;

@Service
public class HbUserServiceImpl extends  BaseServiceImpl<HbUser,String, HbUserDao> implements HbUserService {

    @Override
    public List<HbUser> queryGetHbList(Map<String, Object> param) {
        return  this.dao.queryGetHbList(param);
    }

    @Override
    public Pageable<HbUser> queryList(Map<String, Object> params, Pagination pagination) {
        return  this.dao.queryList(params,pagination);
    }
}
