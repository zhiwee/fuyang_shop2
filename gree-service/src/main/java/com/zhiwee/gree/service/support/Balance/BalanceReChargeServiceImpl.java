package com.zhiwee.gree.service.support.Balance;


import com.zhiwee.gree.dao.Balance.BalanceReChargeDao;
import com.zhiwee.gree.dao.BaseInfo.BaseInfoDao;
import com.zhiwee.gree.dao.ShopUser.ShopUserDao;
import com.zhiwee.gree.model.Balance.BalanceReCharge;
import com.zhiwee.gree.model.BaseInfo.BaseInfo;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.service.Balance.BalanceReChargeService;
import com.zhiwee.gree.service.BaseInfo.BaseInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.List;
import java.util.Map;


@Service
public class BalanceReChargeServiceImpl extends BaseServiceImpl<BalanceReCharge,String,BalanceReChargeDao> implements BalanceReChargeService {

@Autowired
private ShopUserDao shopUserDao;


    @Override
    public Pageable<BalanceReCharge> pageInfo(Map<String, Object> param, Pagination pagination) {
        return dao.pageInfo(param, pagination);
    }

    @Override
    public void updateBalance(BalanceReCharge order) {
        ShopUser shopUser = shopUserDao.select(order.getUserId());
        shopUser.setBalance(shopUser.getBalance()+order.getAmount());
        shopUserDao.update(shopUser);
        this.dao.update(order);
    }

    @Override
    public List<BalanceReCharge> balanceRecord(Map<String, Object> params) {
       return this.dao.balanceRecord(params);
    }


}
