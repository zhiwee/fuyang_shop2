package com.zhiwee.gree.service.support.goods;

import com.zhiwee.gree.dao.goods.ProductAttributeDao;
import com.zhiwee.gree.model.GoodsInfo.ProductAttribute;
import com.zhiwee.gree.service.GoodsInfo.ProductAttributeService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.List;
import java.util.Map;

/**
 * Created by jick on 2019/8/28.
 */
@Service
public class ProductAttributeServiceImpl  extends BaseServiceImpl<ProductAttribute,String,ProductAttributeDao>  implements ProductAttributeService {
    @Override
    public Pageable<ProductAttribute> queryList(Map<String, Object> params, Pagination pagination) {
        //return null;
        //  return this.dao.searchAll(params,pagination);
        return    this.dao.queryList(params,pagination);
    }

    @Override
    public List<ProductAttribute> selectBycategoryId(Map<String, Object> param) {
        return this.dao.selectBycategoryId(param);
    }


    //extends BaseServiceImpl<RetailGoodsPicture,String,RetailGoodsPictureDao> implements RetailGoodsPictureService {



}
