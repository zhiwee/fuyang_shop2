package com.zhiwee.gree.service.support.Order;

import com.zhiwee.gree.dao.order.OrderGoodsDao;
import com.zhiwee.gree.model.order.OrderGoods;
import com.zhiwee.gree.service.Order.OrderGoodsService;
import com.zhiwee.gree.service.OrderInfo.OrderService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.service.support.BaseServiceImpl;

@Service
public class OrderGoodsServiceImpl  extends BaseServiceImpl<OrderGoods,String, OrderGoodsDao> implements OrderGoodsService {
    //extends BaseServiceImpl<OrderAddress,String,OrderAddressDao> implements OrderAddressService
}
