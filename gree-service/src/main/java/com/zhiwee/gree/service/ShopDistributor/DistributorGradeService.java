package com.zhiwee.gree.service.ShopDistributor;

import com.zhiwee.gree.model.ShopDistributor.DistributorGrade;
import com.zhiwee.gree.model.ShopDistributor.ShopDistributor;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.Map;


public interface DistributorGradeService extends BaseService<DistributorGrade,String> {


    Pageable<DistributorGrade> infoPage(Map<String, Object> param, Pagination pagination);


    DistributorGrade selectMax(Map<String,Object> params);
}
