package com.zhiwee.gree.service.support.shopCart;



import com.zhiwee.gree.dao.goods.GoodsInfoDao;
import com.zhiwee.gree.dao.goods.ShopGoodsInfoDao;
import com.zhiwee.gree.dao.goods.ShopGoodsPictureDao;
import com.zhiwee.gree.dao.support.goods.GoodsInfoDaoImpl;
import com.zhiwee.gree.model.GoodsInfo.GoodsInfo;
import com.zhiwee.gree.model.GoodsInfo.ShopGoodsInfo;
import com.zhiwee.gree.model.GoodsInfo.ShopGoodsPicture;
import com.zhiwee.gree.service.shopCart.ShopCartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author sun on 2019/3/28
 */
@Service
public class ShopCartServiceImpl implements ShopCartService {


    @Autowired
    private RedisTemplate redisTemplate;
    @Value("${REDIS_SHOPCART}")
    private String REDIS_SHOPCART;
    @Autowired
    private GoodsInfoDao goodsInfoDao;

    @Autowired
    private ShopGoodsInfoDao shopGoodsInfoDao;


    @Autowired
    private ShopGoodsPictureDao shopGoodsPictureDao;
    @Override
    public Boolean addCart(String userId, String  itemId, int num) {
        Boolean flag= false;
        //向redis中添加购物车。
        //数据类型是hash key：用户id field：商品id value：商品信息
        //判断商品是否存在
        Boolean hexists = redisTemplate.opsForHash().hasKey(REDIS_SHOPCART + ":" + userId, itemId);
        //如果存在数量相加
        if (hexists) {
            Object o = redisTemplate.opsForHash().get(REDIS_SHOPCART + ":" + userId, itemId);
            ShopGoodsInfo item = (ShopGoodsInfo)o;

             item.setAmount(item.getAmount() + num);
            //写回redis
            redisTemplate.opsForHash().put(REDIS_SHOPCART + ":" + userId, itemId , item);
            flag=true;
            return flag;
        }
        //如果不存在，根据商品id取商品信息
        ShopGoodsInfo goodsInfo = shopGoodsInfoDao.select(itemId);
        //设置购物车数据量
        goodsInfo.setAmount(num);

        Map<String,Object> params = new HashMap<>();
        params.put("goodId",itemId);
        List<ShopGoodsPicture> shopGoodsPictures = shopGoodsPictureDao.selectList(params);
        //取一张图片
        if(shopGoodsPictures != null && shopGoodsPictures.size() > 0){
            goodsInfo.setPicture(shopGoodsPictures.get(0).getPicture());
        }

        //添加到购物车列表
        redisTemplate.opsForHash().put(REDIS_SHOPCART + ":" + userId, itemId , goodsInfo);
        flag=true;
        return flag;
    }

    @Override
    public Boolean mergeCart(String userId, List<ShopGoodsInfo> itemList) {
        //遍历商品列表
        //把列表添加到购物车。
        //判断购物车中是否有此商品
        //如果有，数量相加
        //如果没有添加新的商品
        if(itemList != null){
            for (ShopGoodsInfo tbItem : itemList) {
                addCart(userId, tbItem.getId(), tbItem.getAmount());
            }
        }
        //返回成功
        return true;
    }
//
    @Override
    public List<ShopGoodsInfo> getCartList(String userId) {
        //根据用户id查询购车列表
        List<ShopGoodsInfo> goodsInfos = redisTemplate.opsForHash().values(REDIS_SHOPCART + ":" + userId);
//        List<GoodsInfo> itemList = new ArrayList<>();
//        for (String string : jsonList) {
//            //创建一个TbItem对象
//            TbItem item = JsonUtils.jsonToPojo(string, TbItem.class);
//            //添加到列表
//            itemList.add(item);
//        }
        return goodsInfos;
    }
//
    @Override
    public Boolean updateCartNum(String userId, String itemId, int num) {
        //从redis中取商品信息
        ShopGoodsInfo goodsInfo = (ShopGoodsInfo) redisTemplate.opsForHash().get(REDIS_SHOPCART + ":" + userId, itemId);
        //更新商品数量
//        TbItem tbItem = JsonUtils.jsonToPojo(json, TbItem.class);
        goodsInfo.setAmount(num);
        //写入redis
        redisTemplate.opsForHash().put(REDIS_SHOPCART + ":" + userId, itemId, goodsInfo);
        return true;
    }
//
    @Override
    public Boolean deleteCartItem(String userId, String itemId) {
        String[] ids  = itemId.split(",");
        // 删除购物车商品
        for(int i = 0;i< ids.length;i++){
            redisTemplate.opsForHash().delete(REDIS_SHOPCART + ":" + userId,ids[i]);
        }

        return true;
    }
//
    @Override
    public Boolean clearCartItem(String  userId) {
        //删除购物车信息
        redisTemplate.delete(REDIS_SHOPCART + ":" + userId);
        return true;
    }

}
