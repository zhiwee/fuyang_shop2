package com.zhiwee.gree.service.ShopDistributor;

import com.zhiwee.gree.model.ShopDistributor.DistributorGrade;
import com.zhiwee.gree.model.ShopDistributor.DistributorGradeChange;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.Map;


public interface DistributorGradeChangeService extends BaseService<DistributorGradeChange,String> {


    Pageable<DistributorGradeChange> infoPage(Map<String, Object> param, Pagination pagination);


}
