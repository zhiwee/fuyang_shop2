package com.zhiwee.gree.service.support.HomePage;

import com.zhiwee.gree.dao.HomePage.PageChannelKeywordDao;
import com.zhiwee.gree.model.HomePage.PageChannelKeyword;
import com.zhiwee.gree.service.HomePage.PageChannelKeywordService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.Map;

@Service
public class PageChannelKeywordServiceImpl  extends BaseServiceImpl<PageChannelKeyword,String, PageChannelKeywordDao> implements PageChannelKeywordService {
    @Override
    public Pageable<PageChannelKeyword> getPage(Map<String, Object> param, Pagination pagination) {
        return this.dao.getPage(param,pagination);
    }
}
