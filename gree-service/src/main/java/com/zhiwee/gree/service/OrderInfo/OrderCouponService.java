package com.zhiwee.gree.service.OrderInfo;


import com.zhiwee.gree.model.OrderInfo.Order;
import com.zhiwee.gree.model.OrderInfo.OrderCoupon;
import xyz.icrab.common.service.BaseService;


public interface OrderCouponService extends BaseService<OrderCoupon,String> {

}
