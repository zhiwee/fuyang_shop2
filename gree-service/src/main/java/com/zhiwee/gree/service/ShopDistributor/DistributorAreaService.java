package com.zhiwee.gree.service.ShopDistributor;

import com.zhiwee.gree.model.ShopDistributor.DistributorArea;
import com.zhiwee.gree.model.ShopDistributor.ShopDistributor;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.List;
import java.util.Map;


public interface DistributorAreaService extends BaseService<DistributorArea,String> {

    Pageable<DistributorArea> pageArea(Map<String,Object> param, Pagination pagination);


    List<DistributorArea> searchByCategory(Map<String,Object> params);
}
