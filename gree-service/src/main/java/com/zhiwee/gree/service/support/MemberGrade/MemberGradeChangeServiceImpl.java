package com.zhiwee.gree.service.support.MemberGrade;


import com.zhiwee.gree.dao.MemberGrade.MemberGradeChangeDao;
import com.zhiwee.gree.dao.MemberGrade.MemberGradeDao;
import com.zhiwee.gree.model.MemberGrade.MemberGrade;
import com.zhiwee.gree.model.MemberGrade.MemberGradeChange;
import com.zhiwee.gree.service.MemberGrade.MemberGradeChangeService;
import com.zhiwee.gree.service.MemberGrade.MemberGradeService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.Map;


@Service
public class MemberGradeChangeServiceImpl extends BaseServiceImpl<MemberGradeChange,String,MemberGradeChangeDao> implements MemberGradeChangeService {


    @Override
    public Pageable<MemberGradeChange> searchAll(Map<String, Object> params, Pagination pagination) {
        return this.dao.searchAll(params,pagination);
    }
}
