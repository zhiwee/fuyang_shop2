package com.zhiwee.gree.service.card;

import com.zhiwee.gree.model.card.BookingCard;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.Map;

public interface BookingCardService extends BaseService<BookingCard,String> {

    Pageable<BookingCard> pageInfo(Map<String, Object> params, Pagination pagination);

    BookingCard getInfo(BookingCard cardInfo);
}
