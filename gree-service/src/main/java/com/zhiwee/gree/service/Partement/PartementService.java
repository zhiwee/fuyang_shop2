package com.zhiwee.gree.service.Partement;


import com.zhiwee.gree.model.OrderInfo.OrderAddress;
import com.zhiwee.gree.model.Partement.Partement;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.Map;


public interface PartementService extends BaseService<Partement,String> {


    Pageable<Partement> pageInfo(Map<String,Object> params, Pagination pagination);

    void disable(String id);

    void enable(String id);
}
