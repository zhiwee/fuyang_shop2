package com.zhiwee.gree.service.ShopDistributor;

import com.zhiwee.gree.model.ShopDistributor.DistributorArea;
import com.zhiwee.gree.model.ShopDistributor.DistributorOrder;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.Map;


public interface DistributorOrderService extends BaseService<DistributorOrder,String> {


    Pageable<DistributorOrder> infoPage(Map<String,Object> param, Pagination pagination);


    void updateDistributorOrder(DistributorOrder distributorOrder);
}
