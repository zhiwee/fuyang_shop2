package com.zhiwee.gree.service.GoodsInfo;

import com.zhiwee.gree.model.GoodsInfo.GoodVo.SpecsInfoOrderVO;
import com.zhiwee.gree.model.GoodsInfo.GoodsSpecsInfoOrder;
import xyz.icrab.common.service.BaseService;

import java.util.List;
import java.util.Map;

public interface GoodsSpecsInfoOrderService  extends BaseService<GoodsSpecsInfoOrder,String> {

    List<SpecsInfoOrderVO> selectOneList(Map<String, Object> params);
}
