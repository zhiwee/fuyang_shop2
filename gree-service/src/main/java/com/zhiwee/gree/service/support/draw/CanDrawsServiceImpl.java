package com.zhiwee.gree.service.support.draw;

import com.zhiwee.gree.dao.draw.CanDrawsDao;
import com.zhiwee.gree.model.draw.CanDraws;
import com.zhiwee.gree.service.draw.CanDrawsService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.service.support.BaseServiceImpl;


@Service
public class CanDrawsServiceImpl extends BaseServiceImpl<CanDraws, String, CanDrawsDao> implements CanDrawsService {


}
