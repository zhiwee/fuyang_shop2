package com.zhiwee.gree.service.support.Fygoods;

import com.zhiwee.gree.dao.FyGoods.FygoodsDao;
import com.zhiwee.gree.dao.FyGoods.GrouponGoodsDao;
import com.zhiwee.gree.model.FyGoods.Fygoods;
import com.zhiwee.gree.model.FyGoods.GrouponGoods;
import com.zhiwee.gree.service.FyGoods.FygoodsService;
import com.zhiwee.gree.service.FyGoods.GrouponGoodsService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.List;
import java.util.Map;

@Service
public class GrouponGoodsServiceImpl
        extends BaseServiceImpl<GrouponGoods,String, GrouponGoodsDao> implements GrouponGoodsService {
    @Override
    public List<GrouponGoods> queryGoodsList(Map<String, Object> param, Pagination pagination) {
        return  this.dao.queryGoodsList(param,pagination);
    }


}
