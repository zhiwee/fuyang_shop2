package com.zhiwee.gree.service.shopBase;

import java.util.List;
import java.util.Map;


import com.zhiwee.gree.model.Interface;
import com.zhiwee.gree.model.User;
import xyz.icrab.common.model.Relation;
import xyz.icrab.common.service.BaseService;


/**
 * @author gll
 * @since 2018/4/21 17:19
 */
public interface InterfaceService extends BaseService<Interface, String> {
	

    void saveUserInterface(List<Relation> entities, User user);
    void saveRoleInterface(List<Relation> entities, User user);
	/*
	 * 根据userid获取接口
	 */
	List<Interface> getIntByUserId(String userid);
	/*
	 * 根据roleid获取接口
	 */
	List<Interface> getIntByRoleIds(String... roleids);
	/*
	 * 根据userid获取所有接口
	 */
	Map<String,Interface> getAllIntByUserId(String userid);
	
}
