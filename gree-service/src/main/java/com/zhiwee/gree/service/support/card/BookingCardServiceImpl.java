package com.zhiwee.gree.service.support.card;

import com.zhiwee.gree.dao.crad.BookingCardDao;
import com.zhiwee.gree.dao.crad.CardInfoDao;
import com.zhiwee.gree.model.card.BookingCard;
import com.zhiwee.gree.model.card.CardInfo;
import com.zhiwee.gree.service.card.BookingCardService;
import com.zhiwee.gree.service.card.CardInfoService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.Map;


@Service
public class BookingCardServiceImpl extends BaseServiceImpl<BookingCard, String, BookingCardDao> implements BookingCardService {

    @Override
    public Pageable<BookingCard> pageInfo(Map<String, Object> params, Pagination pagination) {
        return dao.pageInfo(params,pagination);
    }

    @Override
    public BookingCard getInfo(BookingCard cardInfo) {
        return dao.getInfo(cardInfo);
    }


}
