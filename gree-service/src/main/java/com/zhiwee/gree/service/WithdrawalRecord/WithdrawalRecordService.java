package com.zhiwee.gree.service.WithdrawalRecord;


import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.WithdrawalRecord.WithdrawalRecord;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.Map;


public interface WithdrawalRecordService extends BaseService<WithdrawalRecord,String> {



    Pageable<WithdrawalRecord>  pageInfo(Map<String, Object> param, Pagination pagination);


    void updateWrAndUser(WithdrawalRecord wr, ShopUser shopUser);
}
