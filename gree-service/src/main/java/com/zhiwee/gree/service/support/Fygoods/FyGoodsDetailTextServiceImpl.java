package com.zhiwee.gree.service.support.Fygoods;

import com.zhiwee.gree.dao.FyGoods.FyGoodsDetailTextDao;
import com.zhiwee.gree.model.FyGoods.FyGoodsDetailText;
import com.zhiwee.gree.service.FyGoods.FyGoodsDetailTextService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.service.support.BaseServiceImpl;

@Service
public class FyGoodsDetailTextServiceImpl
        extends BaseServiceImpl<FyGoodsDetailText,String, FyGoodsDetailTextDao> implements FyGoodsDetailTextService
{
}
