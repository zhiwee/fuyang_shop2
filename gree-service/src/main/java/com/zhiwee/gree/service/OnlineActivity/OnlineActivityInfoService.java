package com.zhiwee.gree.service.OnlineActivity;

import com.zhiwee.gree.model.GoodsInfo.LAPeople;
import com.zhiwee.gree.model.OnlineActivity.OnlineActivityInfo;
import com.zhiwee.gree.model.OnlineActivity.OnlineActivityInfoExe;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.List;
import java.util.Map;


/**
 * @author sun
 * @since 2018/7/6 14:24
 */
public interface OnlineActivityInfoService extends BaseService<OnlineActivityInfo, String> {

    List<OnlineActivityInfo> onlineCurrent();

    List<OnlineActivityInfoExe> exportActivityInfo(Map<String, Object> params);
}
