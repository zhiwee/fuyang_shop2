package com.zhiwee.gree.service.shopBase;


import com.zhiwee.gree.model.Attachment;
import xyz.icrab.common.service.BaseService;

public interface AttachmentService extends BaseService<Attachment,String> {
}
