package com.zhiwee.gree.service.support.Integral;



import com.zhiwee.gree.dao.Integral.IntegralDao;
import com.zhiwee.gree.model.Integral.Integral;
import com.zhiwee.gree.model.MemberGrade.MemberGrade;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.service.Integral.IntegralService;
import com.zhiwee.gree.service.MemberGrade.MemberGradeService;
import com.zhiwee.gree.service.ShopUser.ShopUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.List;
import java.util.Map;


@Service
public class IntegralServiceImpl extends BaseServiceImpl<Integral,String,IntegralDao> implements IntegralService {

        @Autowired
        private ShopUserService shopUserService;
        @Autowired
        private IntegralService integralService;
        @Autowired
        private MemberGradeService memberGradeService;



    @Override
    public Pageable<Integral> achieveIntegral( Pagination pagination) {
        return this.dao.achieveIntegral(pagination);
    }

    @Override
    public Double changeIntegral(ShopUser shopUser, Double totalPrice) {
        //判断总价 错误返回-1
        if (totalPrice<=0){
            return -1.0;
        }
            //找到用户
        ShopUser user = shopUserService.getOne(shopUser);
        //无用户信息返回-1
        if (user==null){
            return -1.0;
        }
        //找到用户原来的积分
        Double integralOld = user.getIntegral();
        //计算出现在的积分
        int price = totalPrice.intValue();
        //取没多少钱 多少积分
        Integer dividend = integralService.list().get(0).getDividend();
        Integer divisor = integralService.list().get(0).getDivisor();
        //本次获得积分
        int integral = price / dividend * divisor;
        Double integralNow = integralOld + integral;

        MemberGrade memberGradeNow=new MemberGrade();
        //找到会员等级
        List<MemberGrade> list = memberGradeService.list();
            for (int i=0;i<list.size()-1;i++){
                //查找等级
                if (integralNow>list.get(i).getEndAmount()&&integralNow<list.get(i+1).getEndAmount()) {
                    memberGradeNow=list.get(i+1);
                }

            }
            shopUser.setIntegral(integralNow);
            shopUser.setGrade(memberGradeNow.getId());
            shopUserService.update(shopUser);
        return integralNow;
    }
}
