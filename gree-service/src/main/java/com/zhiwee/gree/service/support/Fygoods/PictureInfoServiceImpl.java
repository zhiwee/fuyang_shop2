package com.zhiwee.gree.service.support.Fygoods;

import com.zhiwee.gree.dao.FyGoods.GrouponGoodsDao;
import com.zhiwee.gree.dao.FyGoods.PictureInfoDao;
import com.zhiwee.gree.model.FyGoods.GrouponGoods;
import com.zhiwee.gree.model.FyGoods.PictureInfo;
import com.zhiwee.gree.service.FyGoods.GrouponGoodsService;
import com.zhiwee.gree.service.FyGoods.PictureInfoService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.Map;

@Service
public class PictureInfoServiceImpl
        extends BaseServiceImpl<PictureInfo,String, PictureInfoDao> implements PictureInfoService {
    @Override
    public Pageable<PictureInfo> queryGoodsList(Map<String, Object> param, Pagination pagination) {
        return  this.dao.queryGoodsList(param,pagination);
    }


}
