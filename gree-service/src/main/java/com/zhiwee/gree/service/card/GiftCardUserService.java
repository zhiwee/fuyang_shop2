package com.zhiwee.gree.service.card;

import com.zhiwee.gree.model.card.GiftCardUser;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.Map;

public interface GiftCardUserService extends BaseService<GiftCardUser,String> {
    Pageable<GiftCardUser> pageInfo(Map<String, Object> params, Pagination pagination);

}
