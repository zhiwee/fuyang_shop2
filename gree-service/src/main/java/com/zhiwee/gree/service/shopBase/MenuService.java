package com.zhiwee.gree.service.shopBase;

import com.zhiwee.gree.model.Menu;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.List;
import java.util.Map;

public interface MenuService  extends BaseService<Menu, String> {
    Pageable getMenuByUser(Map<String, Object> param, Pagination pagination);

    /**
     * 获取用户所有菜单，包含用户对应角色菜单
     * @param param
     * @return
     */
    List<Menu> getListMenuByUser(Map<String, Object> param);

    List<Menu> getMenuByRoleId(String roleid);

    void addMenuRoleRef(Map<String, Object> param);

    void addMenuUserRef(Map<String, Object> param);

    /**
     * 获取用户对应菜单，不包含用户对应角色菜单
     * @param param
     * @return
     */
    List<Menu> getMenuByUserId(Map<String, Object> param);
}
