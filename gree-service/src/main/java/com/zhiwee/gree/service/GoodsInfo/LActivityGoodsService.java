package com.zhiwee.gree.service.GoodsInfo;


import com.zhiwee.gree.model.GoodsInfo.LAGoodsInfo;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.List;
import java.util.Map;

public interface LActivityGoodsService  extends BaseService<LAGoodsInfo,String> {
    Pageable<LAGoodsInfo> secKillGoods(Map<String, Object> params, Pagination pagination);

    LAGoodsInfo queryKillInfoById(Map<String,Object> param);

    List<LAGoodsInfo> secKillGoods2(Map<String, Object> params);


}
