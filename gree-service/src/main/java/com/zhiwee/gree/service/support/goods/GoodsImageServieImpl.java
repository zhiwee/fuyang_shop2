package com.zhiwee.gree.service.support.goods;


import com.zhiwee.gree.dao.goods.GoodsImageDao;
import com.zhiwee.gree.model.GoodsInfo.GoodsImage;
import com.zhiwee.gree.service.GoodsInfo.GoodsImageService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.List;
import java.util.Map;


@Service
public class GoodsImageServieImpl extends BaseServiceImpl<GoodsImage,String, GoodsImageDao> implements GoodsImageService {
    @Override
    public List<GoodsImage> getWebList(Map<String, Object> params) {
        return this.dao.getWebList(params);
    }

    @Override
    public Pageable<GoodsImage> getPage(Map<String, Object> param, Pagination pagination) {
        return this.dao.getPage(param,pagination);
    }
}
