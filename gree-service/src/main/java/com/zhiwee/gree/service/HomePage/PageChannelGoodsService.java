package com.zhiwee.gree.service.HomePage;

import com.zhiwee.gree.model.HomePage.PageChannelGoods;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.Map;

public interface PageChannelGoodsService  extends BaseService<PageChannelGoods,String> {
    Pageable<PageChannelGoods> getPage(Map<String, Object> param, Pagination pagination);
}
