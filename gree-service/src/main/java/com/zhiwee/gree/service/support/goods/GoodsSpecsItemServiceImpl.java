package com.zhiwee.gree.service.support.goods;

import com.zhiwee.gree.dao.goods.GoodsSpecsItemDao;
import com.zhiwee.gree.model.GoodsInfo.GoodsSpecsItem;
import com.zhiwee.gree.service.GoodsInfo.GoodsSpecsItemService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.Map;

@Service
public class GoodsSpecsItemServiceImpl extends BaseServiceImpl<GoodsSpecsItem,String, GoodsSpecsItemDao> implements GoodsSpecsItemService {
    @Override
    public Pageable<GoodsSpecsItem> getPage(Map<String, Object> param, Pagination pagination) {
        return this.dao.getPage(param,pagination);
    }
}
