package com.zhiwee.gree.service.support.ActivityTicketOrder;
import com.zhiwee.gree.dao.ActivityTicketOrder.ActivityTicketOrderDao;
import com.zhiwee.gree.model.ActivityTicketOrder.ActivityTicketOrder;
import com.zhiwee.gree.model.ActivityTicketOrder.vo.TicketOrderStatisticItem;
import com.zhiwee.gree.service.ActivityTicketOrder.ActivityTicketOrderService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.List;
import java.util.Map;

/**
 * @author sun
 * @since 2018/7/6 14:27
 */
@Service
public class ActivityTicketOrderServiceImpl extends BaseServiceImpl<ActivityTicketOrder, String, ActivityTicketOrderDao> implements ActivityTicketOrderService {


    @Override
    public List<TicketOrderStatisticItem> ticketOrderByDate(Map<String, Object> param) {
      return this.dao.ticketOrderByDate(param);
    }
}
