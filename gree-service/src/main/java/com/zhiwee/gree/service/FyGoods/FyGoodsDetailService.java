package com.zhiwee.gree.service.FyGoods;

import com.zhiwee.gree.model.FyGoods.FyGoodsDetail;
import xyz.icrab.common.service.BaseService;

import java.util.List;
import java.util.Map;

public interface FyGoodsDetailService  extends BaseService<FyGoodsDetail,String> {
    //extends BaseService<FyGoodsImg,String>
    List<FyGoodsDetail> queryByGoodsId(Map<String,Object> param);
}
