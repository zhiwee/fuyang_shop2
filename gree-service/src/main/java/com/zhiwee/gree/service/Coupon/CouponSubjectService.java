package com.zhiwee.gree.service.Coupon;


import com.zhiwee.gree.model.Coupon.CouponSubject;
import xyz.icrab.common.model.Page;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.Map;

public interface CouponSubjectService  extends BaseService<CouponSubject,String> {


    Pageable<CouponSubject> pageInfo(Map<String, Object> param, Pagination pagination);
}
