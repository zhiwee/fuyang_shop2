package com.zhiwee.gree.service.support.ShopUser;

import com.zhiwee.gree.dao.ShopUser.ShopUserSignLogDao;
import com.zhiwee.gree.model.ShopUser.ShopUserSignLog;
import com.zhiwee.gree.service.ShopUser.ShopUserSignLogService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.Map;

/**
 * TODO
 *
 * @author Chensong
 * @Company http://www.zhiwee.com
 * date 2020-02-10
 */
@Service
public class ShopUserSignLogServiceImpl extends BaseServiceImpl<ShopUserSignLog,String, ShopUserSignLogDao> implements ShopUserSignLogService {

    @Override
    public Integer signAmount(Map<String, Object> params) {
        return this.dao.signAmount(params);
    }
    @Override
    public ShopUserSignLog getOneLog(Map<String, Object> params) {
        return this.dao.getOneLog(params);
    }

}
