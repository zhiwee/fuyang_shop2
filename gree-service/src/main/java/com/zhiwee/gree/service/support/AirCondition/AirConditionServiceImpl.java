package com.zhiwee.gree.service.support.AirCondition;


import com.zhiwee.gree.dao.AchieveGift.GiftInfoDao;
import com.zhiwee.gree.dao.AirCondition.AirConditionDao;
import com.zhiwee.gree.model.AchieveGift.GiftInfo;
import com.zhiwee.gree.model.AirCondition.AirCondition;
import com.zhiwee.gree.service.AchieveGift.GiftInfoService;
import com.zhiwee.gree.service.AirCondition.AirConditionService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.Map;


@Service
public class AirConditionServiceImpl extends BaseServiceImpl<AirCondition,String,AirConditionDao> implements AirConditionService {


    @Override
    public Pageable<AirCondition> pageInfo(Map<String, Object> params, Pagination pagination) {

        return this.dao.pageInfo(params,pagination);
    }
}
