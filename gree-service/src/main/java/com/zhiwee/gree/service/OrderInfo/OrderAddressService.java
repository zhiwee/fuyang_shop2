package com.zhiwee.gree.service.OrderInfo;


import com.zhiwee.gree.model.OrderInfo.Order;
import com.zhiwee.gree.model.OrderInfo.OrderAddress;
import com.zhiwee.gree.model.OrderInfo.OrderItem;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.Map;


public interface OrderAddressService extends BaseService<OrderAddress,String> {


    void updateOrders(OrderAddress orderAddress, OrderAddress address, Boolean flag);
}
