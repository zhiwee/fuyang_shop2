package com.zhiwee.gree.service.support.Order;

import com.zhiwee.gree.dao.order.BaseOrderDao;
import com.zhiwee.gree.model.order.BaseOrder;
import com.zhiwee.gree.service.Order.BaseOrderService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.service.support.BaseServiceImpl;

@Service
public class BaseOrderDaoServiceImpl  extends BaseServiceImpl<BaseOrder,String, BaseOrderDao> implements BaseOrderService {
    //extends BaseServiceImpl<OrderAddress,String,OrderAddressDao> implements OrderAddressService
}
