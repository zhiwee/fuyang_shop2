package com.zhiwee.gree.service.GoodsInfo;

import com.zhiwee.gree.model.GoodsInfo.GoodsInfoPicture;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.List;
import java.util.Map;


public interface GoodsInfoPictureService  extends BaseService<GoodsInfoPicture,String> {
    List<GoodsInfoPicture> listAll(Map<String, Object> param);

    Pageable<GoodsInfoPicture> pageInfo(Map<String, Object> params, Pagination pagination);

    GoodsInfoPicture getInfo(String id);
    //extends BaseService<GoodsImage,String>
}
