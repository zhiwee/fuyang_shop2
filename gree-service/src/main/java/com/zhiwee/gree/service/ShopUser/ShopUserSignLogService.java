package com.zhiwee.gree.service.ShopUser;

import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.ShopUser.ShopUserSignLog;
import xyz.icrab.common.service.BaseService;

import java.util.Map;

/**
 * TODO
 *
 * @author Chensong
 * @Company http://www.zhiwee.com
 * date 2020-02-10
 */
public interface ShopUserSignLogService  extends BaseService<ShopUserSignLog,String> {
    Integer signAmount(Map<String,Object> params);

    ShopUserSignLog getOneLog(Map<String,Object> params);
}
