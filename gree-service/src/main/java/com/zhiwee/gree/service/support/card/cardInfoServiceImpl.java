package com.zhiwee.gree.service.support.card;

import com.zhiwee.gree.dao.crad.CardInfoDao;
import com.zhiwee.gree.dao.draw.CanDrawsDao;
import com.zhiwee.gree.dao.draw.DrawUseInfoDao;
import com.zhiwee.gree.model.card.CardInfo;
import com.zhiwee.gree.model.card.LShopUserCards;
import com.zhiwee.gree.model.draw.DrawUseInfo;
import com.zhiwee.gree.model.draw.DrawsInfo;
import com.zhiwee.gree.service.card.CardInfoService;
import com.zhiwee.gree.service.draw.DrawUseInfoService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.Map;


@Service
public class cardInfoServiceImpl extends BaseServiceImpl<CardInfo, String, CardInfoDao> implements CardInfoService {


    @Override
    public Pageable<CardInfo> pageInfo(Map<String, Object> params, Pagination pagination) {
        return dao.pageInfo(params,pagination);
    }

    @Override
    public CardInfo getInfo(CardInfo cardInfo) {
        return dao.getInfo(cardInfo);
    }



}
