package com.zhiwee.gree.service.ActivityTicketOrder;

import com.zhiwee.gree.model.ActivityTicketOrder.TicketOrder;
import com.zhiwee.gree.model.ActivityTicketOrder.TicketOrderExport;
import com.zhiwee.gree.model.ActivityTicketOrder.TicketOrderRefund;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.List;
import java.util.Map;


/**
 * @author sun
 * @since 下午5:06 2019/6/23
 */
public interface TicketOrderRefundService extends BaseService<TicketOrderRefund, String> {

    Pageable<TicketOrderRefund> pageInfo(Map<String, Object> param, Pagination pagination);


}
