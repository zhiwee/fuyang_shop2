package com.zhiwee.gree.service.support.OnlineActivity;

import com.zhiwee.gree.dao.OnlineActivity.OnlineActivityInfoDao;
import com.zhiwee.gree.model.GoodsInfo.LAPeople;
import com.zhiwee.gree.model.OnlineActivity.OnlineActivityInfo;
import com.zhiwee.gree.model.OnlineActivity.OnlineActivityInfoExe;
import com.zhiwee.gree.service.OnlineActivity.OnlineActivityInfoService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author sun
 * @since 2018/7/6 14:27
 */
@Service
public class OnlineActivityInfoServiceImpl extends BaseServiceImpl<OnlineActivityInfo, String, OnlineActivityInfoDao> implements OnlineActivityInfoService {

    @Override
    public List<OnlineActivityInfo> onlineCurrent() {
        Map<String, Object> param = new HashMap<>();
        param.put("state", 1);
        return this.list(param);
    }

    @Override
    public List<OnlineActivityInfoExe> exportActivityInfo(Map<String, Object> params) {
        return dao.exportActivityInfo(params);

    }

}
