package com.zhiwee.gree.service.support.Questionnaire;

import com.zhiwee.gree.dao.Questionnaire.AnswerInfoDao;
import com.zhiwee.gree.dao.Questionnaire.QuestionnaireDao;
import com.zhiwee.gree.model.Questionnaire.AnswerInfo;
import com.zhiwee.gree.model.Questionnaire.AnswerInfoExrt;
import com.zhiwee.gree.model.Questionnaire.Questionnaire;
import com.zhiwee.gree.service.Questionnaire.AnswerInfoService;
import com.zhiwee.gree.service.Questionnaire.QuestionnaireService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.List;
import java.util.Map;

@Service
public class AnswerInfoServiceImpl extends BaseServiceImpl<AnswerInfo,String, AnswerInfoDao> implements AnswerInfoService {
    @Override
    public Pageable<AnswerInfo> pageInfo(Map<String, Object> params, Pagination pagination) {
        return dao.pageInfo(params,pagination);
    }

    @Override
    public List<AnswerInfoExrt> queryAnswerInfoExport(Map<String, Object> params) {
        return dao.queryAnswerInfoExport(params);
    }
}
