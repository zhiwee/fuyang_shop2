package com.zhiwee.gree.service.support.goods;

import com.zhiwee.gree.dao.goods.GoodsDao;
import com.zhiwee.gree.dao.goods.LActivityGoodsDao;
import com.zhiwee.gree.model.GoodsInfo.Goods;
import com.zhiwee.gree.model.GoodsInfo.LAGoodsInfo;
import com.zhiwee.gree.service.GoodsInfo.GoodsService;
import com.zhiwee.gree.service.GoodsInfo.LActivityGoodsService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.List;
import java.util.Map;

@Service
public class LActivityGoodsServiceImpl extends BaseServiceImpl<LAGoodsInfo,String, LActivityGoodsDao> implements LActivityGoodsService {
    @Override
    public Pageable<LAGoodsInfo> secKillGoods(Map<String, Object> params, Pagination pagination) {
        return this.dao.secKillGoods(params,pagination);
        // return this.dao.queryInfoList(params,pagination);
    }

    @Override
    public LAGoodsInfo queryKillInfoById(Map<String, Object> param) {
        return   this.dao.queryKillInfoById(param);
    }

    @Override
    public List<LAGoodsInfo> secKillGoods2(Map<String, Object> params) {
        return  this.dao.secKillGoods2(params);
    }


}
