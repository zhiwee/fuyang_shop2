package com.zhiwee.gree.service.FyGoods;

import com.zhiwee.gree.model.FyGoods.BargainGoods;
import com.zhiwee.gree.model.FyGoods.ShareGoods;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.List;
import java.util.Map;

public interface BargainGoodsService extends BaseService<BargainGoods,String> {
    List<BargainGoods> queryGoodsList(Map<String, Object> param, Pagination pagination);
}
