package com.zhiwee.gree.service.support.Fygoods;

import com.zhiwee.gree.dao.FyGoods.FygoodsCartDao;
import com.zhiwee.gree.model.FyGoods.FygoodsCart;
import com.zhiwee.gree.service.FyGoods.FygoodsCartService;
import com.zhiwee.gree.service.FyGoods.FygoodsService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.service.support.BaseServiceImpl;

@Service
public class FygoodsCartImpl
        extends BaseServiceImpl<FygoodsCart,String, FygoodsCartDao> implements FygoodsCartService {
}
