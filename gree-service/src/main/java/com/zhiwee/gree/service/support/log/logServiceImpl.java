package com.zhiwee.gree.service.support.log;

import com.zhiwee.gree.dao.log.LogDao;
import com.zhiwee.gree.model.Log.Log;
import com.zhiwee.gree.service.log.LogService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.service.support.BaseServiceImpl;

/**
 * @author sun on 2019/3/28
 */
@Service
public class logServiceImpl extends BaseServiceImpl<Log, String, LogDao> implements LogService {
}
