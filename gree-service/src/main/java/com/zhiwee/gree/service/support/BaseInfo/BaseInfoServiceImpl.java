package com.zhiwee.gree.service.support.BaseInfo;


import com.zhiwee.gree.dao.BaseInfo.BaseInfoDao;
import com.zhiwee.gree.dao.Coupon.CouponDao;
import com.zhiwee.gree.model.BaseInfo.BaseInfo;
import com.zhiwee.gree.model.Coupon.Coupon;
import com.zhiwee.gree.model.Coupon.CouponExport;
import com.zhiwee.gree.model.User;
import com.zhiwee.gree.service.BaseInfo.BaseInfoService;
import com.zhiwee.gree.service.Coupon.CouponService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;
import xyz.icrab.common.util.IdGenerator;

import java.util.*;


@Service
public class BaseInfoServiceImpl extends BaseServiceImpl<BaseInfo,String,BaseInfoDao> implements BaseInfoService {




    @Override
    public Pageable<BaseInfo> pageInfo(Map<String, Object> param, Pagination pagination) {
        return dao.pageInfo(param, pagination);
    }


}
