package com.zhiwee.gree.service.support.Invoice;

import com.zhiwee.gree.dao.Invoice.InvoiceDao;
import com.zhiwee.gree.model.Invoice.Invoice;
import com.zhiwee.gree.service.Invoice.InvoiceService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.List;
import java.util.Map;

/**
 * Created by jick on 2019/7/16.
 */
@Service
public class InvoiceServiceImpl extends BaseServiceImpl<Invoice,String,InvoiceDao> implements InvoiceService {
    @Override
    public List<Invoice> queryInvoice(Map<String, Object> param) {
        return  this.dao.queryInvoice(param);
    }

    @Override
    public Pageable<Invoice> queryBaseInvoice(Map<String, Object> param, Pagination pagination) {
        return  this.dao.queryBaseInvoice(param,pagination);
    }


    @Override
    public Invoice queryBaseInvoiceById(Map<String, Object> param) {
        return  this.dao.queryBaseInvoiceById(param);
    }
}
