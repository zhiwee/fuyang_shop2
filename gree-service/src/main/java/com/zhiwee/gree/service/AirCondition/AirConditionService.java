package com.zhiwee.gree.service.AirCondition;


import com.zhiwee.gree.model.AchieveGift.GiftInfo;
import com.zhiwee.gree.model.AirCondition.AirCondition;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.Map;


public interface AirConditionService extends BaseService<AirCondition,String> {

    Pageable<AirCondition> pageInfo(Map<String, Object> params, Pagination pagination);
}
