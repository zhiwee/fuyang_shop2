package com.zhiwee.gree.service.support.OrderInfo;


import com.zhiwee.gree.dao.OrderInfo.OrderDao;
import com.zhiwee.gree.dao.OrderInfo.OrderRefundDao;
import com.zhiwee.gree.dao.OrderInfo.OrderRefundReasonDao;
import com.zhiwee.gree.dao.OrderInfo.RefundReasonPictureDao;
import com.zhiwee.gree.model.OrderInfo.Order;
import com.zhiwee.gree.model.OrderInfo.OrderRefund;
import com.zhiwee.gree.model.OrderInfo.OrderRefundReason;
import com.zhiwee.gree.model.OrderInfo.RefundReasonPicture;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.service.OrderInfo.OrderRefundReasonService;
import com.zhiwee.gree.service.OrderInfo.OrderRefundService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;
import org.springframework.transaction.annotation.Transactional;
import xyz.icrab.common.util.IdGenerator;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class OrderRefundReasonServiceImpl extends BaseServiceImpl<OrderRefundReason, String, OrderRefundReasonDao> implements OrderRefundReasonService {
    @Autowired
    private OrderDao orderDao;


    @Autowired
    private RefundReasonPictureDao refundReasonPictureDao;


    @Transactional
    @Override
    public void backMoney(Order order, OrderRefundReason orderRefundReason, ShopUser user) {
        orderRefundReason.setId(IdGenerator.objectId());
        orderRefundReason.setState(1);
        orderRefundReason.setUserId(user.getId());
        orderRefundReason.setCreateTime(new Date());
        //申请退款
        orderRefundReason.setType(3);
        //申请退款中
        order.setRefundState(8);
        order.setUpdateTime(new Date());

        if (orderRefundReason.getPictures() != null && !orderRefundReason.getPictures().equals("")) {
            String[] pictures = orderRefundReason.getPictures().split(",");
            List<RefundReasonPicture> refundReasonPictures = new ArrayList<>();
            if (pictures.length > 0) {
                for (int i = 0; i < pictures.length; i++) {
                    RefundReasonPicture commentPicture = new RefundReasonPicture();
                    commentPicture.setId(IdGenerator.objectId());
                    commentPicture.setRefundReasonId(orderRefundReason.getId());
                    commentPicture.setCreateTime(new Date());
                    commentPicture.setPicture(pictures[i]);
                    refundReasonPictures.add(commentPicture);
                }
            }
            refundReasonPictureDao.insertBatch(refundReasonPictures);
        }

        orderDao.update(order);
        this.dao.insert(orderRefundReason);
    }

    @Transactional
    @Override
    public void backGood(Order order, OrderRefundReason orderRefundReason, ShopUser user) {
        orderRefundReason.setId(IdGenerator.objectId());
        orderRefundReason.setState(1);
        orderRefundReason.setUserId(user.getId());
        orderRefundReason.setCreateTime(new Date());
        //申请退款
        orderRefundReason.setType(1);
        //申请退货中
        order.setRefundState(2);
        order.setUpdateTime(new Date());
        if (orderRefundReason.getPictures() != null && !orderRefundReason.getPictures().equals("")) {
            String[] pictures = orderRefundReason.getPictures().split(",");
            List<RefundReasonPicture> refundReasonPictures = new ArrayList<>();
            if (pictures.length > 0) {
                for (int i = 0; i < pictures.length; i++) {
                    RefundReasonPicture commentPicture = new RefundReasonPicture();
                    commentPicture.setId(IdGenerator.objectId());
                    commentPicture.setRefundReasonId(orderRefundReason.getId());
                    commentPicture.setCreateTime(new Date());
                    commentPicture.setPicture(pictures[i]);
                    refundReasonPictures.add(commentPicture);
                }
            }
            refundReasonPictureDao.insertBatch(refundReasonPictures);
        }


        orderDao.update(order);
        this.dao.insert(orderRefundReason);
    }


    @Transactional
    @Override
    public void changeGood(Order order, OrderRefundReason orderRefundReason, ShopUser user) {
        orderRefundReason.setId(IdGenerator.objectId());
        orderRefundReason.setState(1);
        orderRefundReason.setUserId(user.getId());
        orderRefundReason.setCreateTime(new Date());
        //申请换货
        orderRefundReason.setType(2);
        //申请换货中
        order.setRefundState(5);
        order.setUpdateTime(new Date());
        if (orderRefundReason.getPictures() != null && !orderRefundReason.getPictures().equals("")) {
            String[] pictures = orderRefundReason.getPictures().split(",");
            List<RefundReasonPicture> refundReasonPictures = new ArrayList<>();
            if (pictures.length > 0) {
                for (int i = 0; i < pictures.length; i++) {
                    RefundReasonPicture commentPicture = new RefundReasonPicture();
                    commentPicture.setId(IdGenerator.objectId());
                    commentPicture.setRefundReasonId(orderRefundReason.getId());
                    commentPicture.setCreateTime(new Date());
                    commentPicture.setPicture(pictures[i]);
                    refundReasonPictures.add(commentPicture);
                }
            }
            refundReasonPictureDao.insertBatch(refundReasonPictures);
        }

        orderDao.update(order);
        this.dao.insert(orderRefundReason);
    }

    @Override
    public Pageable<OrderRefundReason> pageInfo(Map<String, Object> params, Pagination pagination) {
        return this.dao.pageInfo(params, pagination);
    }

    @Transactional
    @Override
    public void updateOrders(OrderRefundReason orderRefund, Order order) {
        orderDao.update(order);
        this.dao.update(orderRefund);

    }

    @Override
    public List<OrderRefundReason> achieveRefundInfo(Map<String, Object> param) {
        return this.dao.achieveRefundInfo(param);
    }

    @Override
    public List<OrderRefundReason> achieveChangeOrderInfo(Map<String, Object> param) {
        return this.dao.achieveChangeOrderInfo(param);
    }
}
