package com.zhiwee.gree.service.card;

import com.zhiwee.gree.model.card.BookingCard;
import com.zhiwee.gree.model.card.BookingCardUser;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.Map;

public interface BookingCardUserService extends BaseService<BookingCardUser,String> {

}
