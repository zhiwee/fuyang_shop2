package com.zhiwee.gree.service.GoodsInfo;

import com.zhiwee.gree.model.GoodsInfo.GoodsAttributeOrder;
import xyz.icrab.common.service.BaseService;

import java.util.Map;

public interface GoodsAttributeOrderService  extends BaseService<GoodsAttributeOrder,String> {
    void deleteByGoodsId(Map<String, Object> param);
}
