package com.zhiwee.gree.service.Shop;

import com.zhiwee.gree.model.Shop.Shop;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.Map;

public interface ShopService extends BaseService<Shop,String> {
    Pageable<Shop> queryList(Map<String, Object> param, Pagination pagination);

    //BaseService<BaseInfo,String>
}
