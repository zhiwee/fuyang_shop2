package com.zhiwee.gree.service.card;

import com.zhiwee.gree.model.card.UserCardsInfo;
import com.zhiwee.gree.model.card.UserCardsInfoExrt;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.List;
import java.util.Map;

public interface UserCardsInfoService extends BaseService<UserCardsInfo,String> {


    Pageable<UserCardsInfo> pageInfo(Map<String, Object> params, Pagination pagination);

    Pageable<UserCardsInfo> pageInfoNoCategory(Map<String, Object> params, Pagination pagination);

    UserCardsInfo getCurrentOne(Map<String,Object> param);



    /**
     * 导出
     * Chensong
     * @param params
     * @return
     */
    List<UserCardsInfoExrt> queryUserCardsInfoExport(Map<String,Object> params);
    List<UserCardsInfoExrt> queryUserCardsInfoNoCategoryExport(Map<String,Object> params);

    List<UserCardsInfo> queryUserCardsInfo(Map<String,Object> params);

}
