package com.zhiwee.gree.service.support.Dept;

import com.zhiwee.gree.dao.Dept.DeptInfoDao;
import com.zhiwee.gree.model.DeptInfo.DeptInfo;
import com.zhiwee.gree.service.Dept.DeptInfoService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.Map;

/**
 * Created by jick on 2019/8/26.
 */
@Service
public class DeptInfoServiceImpl extends BaseServiceImpl<DeptInfo,String,DeptInfoDao> implements DeptInfoService {
    @Override
    public Pageable<DeptInfo> pageInfo(Map<String, Object> param, Pagination pagination) {
        //    return null;
        //   return    this.dao.queryDeptOpenInfo(param,pagination);
        return   this.dao.pageInfo(param,pagination);
    }
    // extends BaseServiceImpl<DeptopenInfo,String,DeptopenInfoDao> implements DeptopenInfoService
}
