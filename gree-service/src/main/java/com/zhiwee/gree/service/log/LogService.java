package com.zhiwee.gree.service.log;

import com.zhiwee.gree.model.Log.Log;
import xyz.icrab.common.service.BaseService;

/**
 * @author sun on 2019/3/28
 */
public interface LogService extends BaseService<Log,String> {
}
