package com.zhiwee.gree.service.support.shopBase;

import com.zhiwee.gree.dao.shopBase.DictionaryDao;
import com.zhiwee.gree.dao.support.shopBase.DictionaryDaoImpl;
import com.zhiwee.gree.model.Dictionary;
import com.zhiwee.gree.service.shopBase.DictionaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xyz.icrab.common.service.support.BaseServiceImpl;


import java.util.HashMap;
import java.util.Map;

/**
 * @author gll
 * @since 2018/4/21 17:22
 */
@Service("dictionaryService")
public class DictionaryServiceImpl extends BaseServiceImpl<Dictionary, String, DictionaryDao> implements DictionaryService {

	@Autowired
	DictionaryDaoImpl dictionaryDaoImpl;

	@Override
	public boolean checkGroup(String group) {
		int i = dictionaryDaoImpl.checkGroup(group);
		if(i > 0){
			return true;
		}
	    return false;
	}

	@Override
	public boolean checkKey(String key,String group) {
		int i = dictionaryDaoImpl.checkKey(key,group);
		if(i > 0){
			return true;
		}
	    return false;
	}

	@Override
	public void updateState(Map<String, Object> map) {
		if(map == null){
			map = new HashMap<>();
		}
		dictionaryDaoImpl.updateState(map);
	}
}
