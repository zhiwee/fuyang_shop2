package com.zhiwee.gree.service.GoodsInfo;

import com.zhiwee.gree.model.GoodsInfo.Goods;
import com.zhiwee.gree.model.GoodsInfo.GoodsBase;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.List;
import java.util.Map;

/**
 * Created by jick on 2019/8/30.
 */
public interface GoodsBaseService extends BaseService<GoodsBase,String> {

    Pageable<GoodsBase> queryInfoList(Map<String, Object> params, Pagination pagination);


    GoodsBase queryInfoListById(Map<String, Object> params);

    List<GoodsBase> listAll(Map<String, Object> params);

    List<String> getList(String id);
}
