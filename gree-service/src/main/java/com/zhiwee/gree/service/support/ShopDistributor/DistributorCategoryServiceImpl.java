package com.zhiwee.gree.service.support.ShopDistributor;


import com.zhiwee.gree.dao.ShopDistributor.DistributorAreaDao;
import com.zhiwee.gree.dao.ShopDistributor.DistributorCategoryDao;
import com.zhiwee.gree.model.ShopDistributor.DistributorArea;
import com.zhiwee.gree.model.ShopDistributor.DistributorCategory;
import com.zhiwee.gree.service.ShopDistributor.DistributorAreaService;
import com.zhiwee.gree.service.ShopDistributor.DistributorCategoryService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.List;
import java.util.Map;


@Service
public class DistributorCategoryServiceImpl extends BaseServiceImpl<DistributorCategory, String, DistributorCategoryDao> implements DistributorCategoryService {


    @Override
    public Pageable<DistributorCategory> achieveSmallCategory(Map<String, Object> param, Pagination pagination) {
        return this.dao.achieveSmallCategory(param,pagination);
    }
}
