package com.zhiwee.gree.service.support.shopBase;

import com.zhiwee.gree.dao.shopBase.AttachmentDao;
import com.zhiwee.gree.model.Attachment;
import com.zhiwee.gree.service.shopBase.AttachmentService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.service.support.BaseServiceImpl;


@Service("attachmentService")
public class AttachmentServiceImpl extends BaseServiceImpl<Attachment,String,AttachmentDao> implements AttachmentService {

}
