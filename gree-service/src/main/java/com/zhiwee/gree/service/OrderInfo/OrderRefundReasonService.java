package com.zhiwee.gree.service.OrderInfo;


import com.zhiwee.gree.model.OrderInfo.Order;
import com.zhiwee.gree.model.OrderInfo.OrderRefund;
import com.zhiwee.gree.model.OrderInfo.OrderRefundReason;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.List;
import java.util.Map;


public interface OrderRefundReasonService extends BaseService<OrderRefundReason,String> {


    void backMoney(Order order, OrderRefundReason orderRefundReason,ShopUser user);

    void backGood(Order order, OrderRefundReason orderRefundReason,ShopUser user);

    void changeGood(Order order, OrderRefundReason orderRefundReason,ShopUser user);

    Pageable<OrderRefundReason> pageInfo(Map<String,Object> params, Pagination pagination);

    void updateOrders(OrderRefundReason orderRefund, Order order);

    List<OrderRefundReason> achieveRefundInfo(Map<String,Object> param);

    List<OrderRefundReason> achieveChangeOrderInfo(Map<String,Object> param);
}
