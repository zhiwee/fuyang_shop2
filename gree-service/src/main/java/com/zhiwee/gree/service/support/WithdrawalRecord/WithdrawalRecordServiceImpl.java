package com.zhiwee.gree.service.support.WithdrawalRecord;


import com.zhiwee.gree.dao.ShopUser.ShopUserDao;
import com.zhiwee.gree.dao.WithdrawalRecord.WithdrawalRecordDao;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.WithdrawalRecord.WithdrawalRecord;
import com.zhiwee.gree.service.WithdrawalRecord.WithdrawalRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.Map;


@Service
public class WithdrawalRecordServiceImpl extends BaseServiceImpl<WithdrawalRecord,String,WithdrawalRecordDao> implements WithdrawalRecordService {

@Autowired
private ShopUserDao shopUserDao;



    @Override
    public Pageable<WithdrawalRecord> pageInfo(Map<String, Object> param, Pagination pagination) {
        return dao.pageInfo(param, pagination);
    }

    @Override
    public void updateWrAndUser(WithdrawalRecord wr, ShopUser shopUser) {
        this.dao.update(wr);
        shopUserDao.update(shopUser);

    }


}

