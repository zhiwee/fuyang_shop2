package com.zhiwee.gree.service.support.card;

import com.zhiwee.gree.dao.crad.LShopUserCardWxPayDao;
import com.zhiwee.gree.dao.crad.LShopUserCardsDao;
import com.zhiwee.gree.model.card.LShopUserCardWxPayInfo;
import com.zhiwee.gree.model.card.LShopUserCardWxPayInfoExrt;
import com.zhiwee.gree.model.card.LShopUserCards;
import com.zhiwee.gree.service.card.LShopUserCardWxPayService;
import com.zhiwee.gree.service.card.LShopUserCardsService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.List;
import java.util.Map;

/**
 * TODO
 *
 * @author Chensong
 * @Company http://www.zhiwee.com
 * date 2020-01-03
 */
@Service
public class LShopUserCardWxPayServiceImpl  extends BaseServiceImpl<LShopUserCardWxPayInfo, String, LShopUserCardWxPayDao> implements LShopUserCardWxPayService{


    @Override
    public Pageable<LShopUserCardWxPayInfo> getLShopUserCardWxPayList(Map<String, Object> params, Pagination pagination) {
        return dao.getLShopUserCardWxPayInfoList(params,pagination);
    }

    @Override
    public List<LShopUserCardWxPayInfoExrt> queryLShopUserCardWxPayExport(Map<String, Object> params) {
        return dao.queryLShopUserCardWxPayInfoExport(params);
    }
}
