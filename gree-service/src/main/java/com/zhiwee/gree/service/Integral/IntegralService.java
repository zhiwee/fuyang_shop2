package com.zhiwee.gree.service.Integral;


import com.zhiwee.gree.model.HomePage.HomePage;
import com.zhiwee.gree.model.Integral.Integral;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.Map;


public interface IntegralService extends BaseService<Integral,String> {

    Pageable<Integral> achieveIntegral( Pagination pagination);


    Double changeIntegral(ShopUser shopUser, Double totalPrice);
}
