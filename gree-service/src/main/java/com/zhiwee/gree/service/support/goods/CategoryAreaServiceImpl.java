package com.zhiwee.gree.service.support.goods;

import com.zhiwee.gree.dao.goods.CategoryAreaDao;
import com.zhiwee.gree.dao.goods.GoodsClassifyDao;
import com.zhiwee.gree.model.GoodsInfo.CategoryArea;
import com.zhiwee.gree.model.GoodsInfo.GoodsClassify;
import com.zhiwee.gree.model.GoodsLabel.*;
import com.zhiwee.gree.service.GoodsInfo.CategoryAreaService;
import com.zhiwee.gree.service.GoodsInfo.GoodsClassifyService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.List;
import java.util.Map;


@Service
public class CategoryAreaServiceImpl extends BaseServiceImpl<CategoryArea,String,CategoryAreaDao> implements CategoryAreaService {



}
