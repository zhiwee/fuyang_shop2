package com.zhiwee.gree.service.support.goods;

import com.zhiwee.gree.dao.goods.CategorysDao;
import com.zhiwee.gree.model.GoodsInfo.Categorys;
import com.zhiwee.gree.model.GoodsInfo.GoodVo.CategorysExo;
import com.zhiwee.gree.service.GoodsInfo.CategorysService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.List;
import java.util.Map;


/**
 * @author DELL
 */
@Service
public class GategorysServiceImpl extends BaseServiceImpl<Categorys,String, CategorysDao> implements CategorysService {

    @Override
    public Pageable<Categorys> getPage(Map<String, Object> param, Pagination pagination) {
        return dao.getPage(param,pagination);
    }

    @Override
    public List<CategorysExo> listExo() {
        return dao.listExo();
    }
}
