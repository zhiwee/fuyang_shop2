package com.zhiwee.gree.service.MemberGrade;


import com.zhiwee.gree.model.GoodsInfo.GoodsInfo;
import com.zhiwee.gree.model.GoodsInfo.SolrGoodsInfo;
import com.zhiwee.gree.model.MemberGrade.MemberGrade;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.List;
import java.util.Map;


public interface MemberGradeService extends BaseService<MemberGrade,String> {


    Pageable<MemberGrade> searchAll(Map<String, Object> params, Pagination pagination);


    MemberGrade selectMax(Map<String, Object> params);
}
