package com.zhiwee.gree.service.support.card;

import com.zhiwee.gree.dao.crad.LShopUserCardsDao;
import com.zhiwee.gree.dao.draw.CanDrawsDao;
import com.zhiwee.gree.model.card.LShopUserCards;
import com.zhiwee.gree.model.card.LShopUserCardsExrt;
import com.zhiwee.gree.model.draw.CanDraws;
import com.zhiwee.gree.service.card.LShopUserCardsService;
import com.zhiwee.gree.service.draw.CanDrawsService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.List;
import java.util.Map;


@Service
public class LShopUserCardsServiceImpl extends BaseServiceImpl<LShopUserCards, String, LShopUserCardsDao> implements LShopUserCardsService {

    @Override
    public Pageable<LShopUserCards> getLShopUserCardsList(Map<String, Object> params, Pagination pagination) {
        return dao.getLShopUserCardsList(params,pagination);
    }


    @Override
    public List<LShopUserCardsExrt> queryLShopUserCardsExport(Map<String, Object> params) {
        return dao.queryLShopUserCardsExport(params);
    }
}
