package com.zhiwee.gree.service.ShopUser;


import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.ShopUser.ShopUserExport;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;
import xyz.icrab.common.web.param.IdsParam;

import java.util.List;
import java.util.Map;


public interface ShopUserService extends BaseService<ShopUser,String> {

    List<ShopUserExport> exportShopUser(Map<String,Object> param);

    Pageable<ShopUser>  pageInfo(Map<String,Object> param, Pagination pagination);

    void deleteUser(IdsParam param);

    ShopUser achieveByUser(ShopUser user);

    void updateAndDelete(ShopUser shopUser, ShopUser su);

    Integer registerAmount(Map<String,Object> params);

    String refreshUrl(String id, String cardUrl);

    void generateImg(ShopUser shopUser);

}
