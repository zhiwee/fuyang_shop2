package com.zhiwee.gree.service.support.goods;

import com.zhiwee.gree.dao.goods.GoodsSpecsInfoOrderDao;
import com.zhiwee.gree.model.GoodsInfo.GoodVo.SpecsInfoOrderVO;
import com.zhiwee.gree.model.GoodsInfo.GoodsSpecsInfoOrder;
import com.zhiwee.gree.service.GoodsInfo.GoodsSpecsInfoOrderService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.List;
import java.util.Map;

@Service
public class GoodsSpecsInfoOrderServiceImpl extends BaseServiceImpl<GoodsSpecsInfoOrder,String, GoodsSpecsInfoOrderDao> implements GoodsSpecsInfoOrderService {
    @Override
    public List<SpecsInfoOrderVO> selectOneList(Map<String, Object> params) {
        return this.dao.selectOneList(params);
    }
}
