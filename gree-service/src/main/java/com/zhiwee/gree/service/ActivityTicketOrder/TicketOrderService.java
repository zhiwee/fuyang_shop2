package com.zhiwee.gree.service.ActivityTicketOrder;

import com.zhiwee.gree.model.ActivityTicketOrder.ActivityTicketOrder;
import com.zhiwee.gree.model.ActivityTicketOrder.TicketOrder;
import com.zhiwee.gree.model.ActivityTicketOrder.TicketOrderExport;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.List;
import java.util.Map;


/**
 * @author sun
 * @since 下午5:06 2019/6/23
 */
public interface TicketOrderService extends BaseService<TicketOrder, String> {

    Pageable<TicketOrder> pageInfo(Map<String,Object> param, Pagination pagination);

    Pageable<TicketOrder> achieveMyOrder(Map<String,Object> params, Pagination pagination);

    List<TicketOrderExport> pageList(Map<String,Object> params);

    void updateAndSave(TicketOrder order);

    void updateTicketOrder(TicketOrder order);
}
