package com.zhiwee.gree.service.card;

import com.zhiwee.gree.model.card.BookingCards;
import com.zhiwee.gree.model.card.BookingCardsExrt;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.List;
import java.util.Map;

public interface BookingCardsService extends BaseService<BookingCards,String> {

    Pageable<BookingCards> pageInfo(Map<String, Object> params, Pagination pagination);

    List<BookingCardsExrt> queryBookingCardsExport(Map<String, Object> params);

    List<BookingCards> listBuy(Map<String, Object> qParam2);



}
