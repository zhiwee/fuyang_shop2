package com.zhiwee.gree.service.Dept;

import com.zhiwee.gree.model.DeptInfo.DeptopenInfo;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.Map;

/**
 * Created by jick on 2019/8/23.
 */
public interface DeptopenInfoService  extends BaseService<DeptopenInfo,String> {
    //  Pageable<Invoice> queryBaseInvoice(Map<String,Object> param, Pagination pagination);
  //Map<String,Object> param, Pagination pagination
    Pageable<DeptopenInfo>  queryDeptOpenInfo(Map<String,Object> param, Pagination pagination);
}
