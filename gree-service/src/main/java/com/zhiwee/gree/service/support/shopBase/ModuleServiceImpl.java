package com.zhiwee.gree.service.support.shopBase;

import com.zhiwee.gree.dao.shopBase.ModuleDao;
import com.zhiwee.gree.model.Module;
import com.zhiwee.gree.service.shopBase.ModuleService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.service.support.BaseServiceImpl;

/**
 * @author gll
 * @since 2018/4/21 17:22
 */
@Service("moduleService")
public class ModuleServiceImpl extends BaseServiceImpl<Module, String, ModuleDao> implements ModuleService {
}
