package com.zhiwee.gree.service.support.Activity;

import com.zhiwee.gree.dao.Activity.ActivityInfoDao;
import com.zhiwee.gree.dao.Activity.ActivityPageDao;
import com.zhiwee.gree.model.Activity.ActivityInfo;
import com.zhiwee.gree.model.Activity.ActivityPage;
import com.zhiwee.gree.service.Activity.ActivityInfoService;
import com.zhiwee.gree.service.Activity.ActivityPageService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.Map;


/**
 * @author sun
 * @since 2019/7/6 14:27
 */
@Service
public class ActivityPageServiceImpl extends BaseServiceImpl<ActivityPage, String, ActivityPageDao> implements ActivityPageService {

    @Override
    public Pageable<ActivityPage> pageInfo(Map<String, Object> param, Pagination pagination) {
        return dao.pageInfo(param, pagination);
    }
}
