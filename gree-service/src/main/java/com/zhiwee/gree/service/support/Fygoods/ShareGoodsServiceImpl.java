package com.zhiwee.gree.service.support.Fygoods;

import com.zhiwee.gree.dao.FyGoods.GrouponGoodsDao;
import com.zhiwee.gree.dao.FyGoods.ShareGoodsDao;
import com.zhiwee.gree.model.FyGoods.GrouponGoods;
import com.zhiwee.gree.model.FyGoods.ShareGoods;
import com.zhiwee.gree.service.FyGoods.GrouponGoodsService;
import com.zhiwee.gree.service.FyGoods.ShareGoodsService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.List;
import java.util.Map;

@Service
public class ShareGoodsServiceImpl
        extends BaseServiceImpl<ShareGoods,String, ShareGoodsDao> implements ShareGoodsService {
    @Override
    public List<ShareGoods> queryGoodsList(Map<String, Object> param, Pagination pagination) {
        return  this.dao.queryGoodsList(param,pagination);
    }


}
