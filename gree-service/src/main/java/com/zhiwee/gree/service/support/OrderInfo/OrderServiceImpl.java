package com.zhiwee.gree.service.support.OrderInfo;


import com.zhiwee.gree.dao.Coupon.CouponDao;
import com.zhiwee.gree.dao.Integral.IntegralDao;
import com.zhiwee.gree.dao.MemberGrade.MemberGradeChangeDao;
import com.zhiwee.gree.dao.MemberGrade.MemberGradeDao;
import com.zhiwee.gree.dao.OrderInfo.*;
import com.zhiwee.gree.dao.ShopDistributor.DistributorGradeChangeDao;
import com.zhiwee.gree.dao.ShopDistributor.DistributorGradeDao;
import com.zhiwee.gree.dao.ShopUser.ShopUserDao;
import com.zhiwee.gree.model.GoodsInfo.Goods;
import com.zhiwee.gree.model.OrderInfo.*;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.service.GoodsInfo.GoodsService;
import com.zhiwee.gree.service.OrderInfo.OrderService;
import com.zhiwee.gree.util.SequenceUtils;
import xyz.icrab.common.model.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.service.support.BaseServiceImpl;
import xyz.icrab.common.util.IdGenerator;

import java.math.BigDecimal;
import java.util.*;


@Service
public class OrderServiceImpl extends BaseServiceImpl<Order, String, OrderDao> implements OrderService {

    @Autowired
    private OrderItemDao orderItemDao;

    @Autowired
    private SequenceUtils sequenceUtils;

    @Autowired
    private GoodsService goodsService;



    @Autowired
    private OrderService orderService;

    @Autowired
    private DistributorGradeChangeDao distributorGradeChangeDao;


    @Autowired
    private OrderRefundReasonDao orderRefundReasonDao;

    @Transactional
    @Override
    @SuppressWarnings("all")
    public void createFyOrder(Order order, ShopUser shopUser) {
        Goods goodsInfo = null;
        order.setState(1);
        order.setRefundState(1);
        order.setPayId(shopUser.getId());
        order.setRealPrice(0.0);
        order.setOrderState(1);
        order.setOrderNum(sequenceUtils.sequenceByDay());
        order.setCreateTime(new Date());
        //商城正常下单
        order.setIsOffline(1);
        //未支付状态
        order.setOrderState(1);
        List<OrderItem> orderItemList = new ArrayList<>();
        Iterator<OrderItem> iterator = order.getOrderItems().iterator();
        while (iterator.hasNext()) {
            OrderItem orderItem = iterator.next();
            goodsInfo = goodsService.get(orderItem.getGoodsId());
            orderItem.setId(IdGenerator.objectId());
            orderItem.setActivityCost(0.0);
            orderItem.setPromotionCosts(0.0);
            orderItem.setCategoryId(goodsInfo.getCategoryId());
//            orderItem.setGoodsName(goodsInfo.getName());
            orderItem.setCreateTime(new Date());
            orderItem.setOrderId(order.getId());
            orderItem.setGoodsId(goodsInfo.getId());
            orderItem.setSubtractPrice(0.0);
            orderItem.setAmount(BigDecimal.valueOf(orderItem.getUnitPrice()).multiply(new BigDecimal(orderItem.getBuyBun())).doubleValue());
            orderItem.setRealAmount(orderItem.getAmount());
            orderItemDao.insert(orderItem);

        }
        orderService.save(order);
        }


    @Override
    public Pageable<Order> pageInfo(Map<String, Object> params, Pagination pagination) {
        return  dao.pageInfo(params,pagination);
    }
}

