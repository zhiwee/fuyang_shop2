package com.zhiwee.gree.service.support.card;

import com.zhiwee.gree.dao.crad.BookingCardsDao;
import com.zhiwee.gree.model.card.BookingCard;
import com.zhiwee.gree.model.card.BookingCards;
import com.zhiwee.gree.model.card.BookingCardsExrt;
import com.zhiwee.gree.model.card.LShopUserCardsExrt;
import com.zhiwee.gree.service.card.BookingCardsService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.List;
import java.util.Map;


@Service
public class BookingCardsServiceImpl extends BaseServiceImpl<BookingCards, String, BookingCardsDao> implements BookingCardsService {


    @Override
    public Pageable<BookingCards> pageInfo(Map<String, Object> params, Pagination pagination) {
        return dao.pageInfo(params,pagination);
    }


    @Override
    public List<BookingCardsExrt> queryBookingCardsExport(Map<String, Object> params) {
        return dao.queryBookingCardsExport(params);
    }

    @Override
    public List<BookingCards> listBuy(Map<String, Object> qParam2) {
        return dao.listBuy(qParam2);
    }
}
