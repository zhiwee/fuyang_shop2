package com.zhiwee.gree.service.support.fyskill;


import com.zhiwee.gree.dao.skill.SkillhbDao;
import com.zhiwee.gree.model.skill.Skillhb;
import com.zhiwee.gree.service.skill.SkillhbService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.List;
import java.util.Map;

@Service
public class SkillhbServiceImpl
        extends BaseServiceImpl<Skillhb,String, SkillhbDao> implements SkillhbService {
    @Override
    public Pageable<Skillhb> queryList(Map<String, Object> params, Pagination pagination) {
        return  this.dao.queryList(params,pagination);
    }

    @Override
    public List<Skillhb> queryHbByTime(Map<String, Object> params) {
        return  this.dao.queryHbByTime(params);
    }

    //extends BaseServiceImpl<DeptopenInfo,String,DeptopenInfoDao> implements DeptopenInfoService
}
