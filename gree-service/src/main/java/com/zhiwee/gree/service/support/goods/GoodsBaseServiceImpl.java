package com.zhiwee.gree.service.support.goods;

import com.zhiwee.gree.dao.goods.GoodsBaseDao;
import com.zhiwee.gree.dao.goods.GoodsDao;
import com.zhiwee.gree.model.GoodsInfo.GoodVo.GoodsExo;
import com.zhiwee.gree.model.GoodsInfo.GoodVo.GoodsInfoVO;
import com.zhiwee.gree.model.GoodsInfo.Goods;
import com.zhiwee.gree.model.GoodsInfo.GoodsBase;
import com.zhiwee.gree.model.GoodsInfo.ProductAttribute;
import com.zhiwee.gree.model.OnlineActivity.vo.OnlineActivityInfoVo;
import com.zhiwee.gree.service.GoodsInfo.GoodsBaseService;
import com.zhiwee.gree.service.GoodsInfo.GoodsService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.List;
import java.util.Map;

/**
 * Created by jick on 2019/8/30.
 */
@Service
public class GoodsBaseServiceImpl extends BaseServiceImpl<GoodsBase,String, GoodsBaseDao> implements GoodsBaseService {
    @Override
    public Pageable<GoodsBase> queryInfoList(Map<String, Object> params, Pagination pagination) {
        return this.dao.queryInfoList(params,pagination);
        //   return    this.dao.queryList(params,pagination);
    }

    @Override
    public GoodsBase queryInfoListById(Map<String, Object> params) {
        return dao.queryInfoListById(params);
    }

    @Override
    public List<GoodsBase> listAll(Map<String, Object> params) {
        return dao.listAll(params);
    }

    @Override
    public List<String> getList(String id) {
        return dao.getList(id);
    }
}

