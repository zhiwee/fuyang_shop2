package com.zhiwee.gree.service.GoodsInfo;

import com.zhiwee.gree.model.GoodsInfo.GoodsBand;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.Map;

public interface GoodsBandService extends BaseService<GoodsBand,String> {
    Pageable<GoodsBand> getPage(Map<String, Object> param, Pagination pagination);
}
