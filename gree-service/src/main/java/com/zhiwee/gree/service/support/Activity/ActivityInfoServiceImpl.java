package com.zhiwee.gree.service.support.Activity;

import com.zhiwee.gree.dao.Activity.ActivityInfoDao;

import com.zhiwee.gree.model.Activity.ActivityInfo;

import com.zhiwee.gree.service.Activity.ActivityInfoService;

import org.springframework.stereotype.Service;

import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.List;
import java.util.Map;


/**
 * @author sun
 * @since 2018/7/6 14:27
 */
@Service
public class ActivityInfoServiceImpl extends BaseServiceImpl<ActivityInfo, String, ActivityInfoDao> implements ActivityInfoService {

    @Override
    public List<ActivityInfo> checkTime(Map<String, Object> param) {
        return dao.checkTime(param);
    }

    @Override
    public ActivityInfo getCurrent(Map<String, Object> param) {
        return dao.getCurrent(param);
    }
}
