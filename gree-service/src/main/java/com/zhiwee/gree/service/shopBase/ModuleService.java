package com.zhiwee.gree.service.shopBase;


import com.zhiwee.gree.model.Module;
import xyz.icrab.common.service.BaseService;

/**
 * @author gll
 * @since 2018/4/21 17:19
 */
public interface ModuleService extends BaseService<Module, String> {
}
