package com.zhiwee.gree.service.shopBase;


import com.zhiwee.gree.model.Area;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.List;
import java.util.Map;

public interface AreaService extends BaseService<Area,String> {
    List<Area> ahtree(Map<String,Object> params);

    Pageable<Area> ahPage(Map<String,Object> params, Pagination pagination);

    Pageable<Area> allProvince(Map<String,Object> params, Pagination pagination);
}
