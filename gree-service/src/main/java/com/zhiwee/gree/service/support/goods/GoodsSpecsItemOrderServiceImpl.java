package com.zhiwee.gree.service.support.goods;

import com.zhiwee.gree.dao.goods.GoodsSpecsItemOrderDao;
import com.zhiwee.gree.model.GoodsInfo.GoodsSpecsItemOrder;
import com.zhiwee.gree.service.GoodsInfo.GoodsSpecsItemOrderService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.List;
import java.util.Map;

@Service
public class GoodsSpecsItemOrderServiceImpl extends BaseServiceImpl<GoodsSpecsItemOrder,String, GoodsSpecsItemOrderDao> implements GoodsSpecsItemOrderService {

    @Override
    public List<GoodsSpecsItemOrder> getListBYGoodsId(Map<String, Object> param) {
        return this.dao.getListBYGoodsId(param);
    }
}
