package com.zhiwee.gree.service.support.card;

import com.zhiwee.gree.dao.crad.GiftCardDao;
import com.zhiwee.gree.model.card.GiftCard;
import com.zhiwee.gree.service.card.GiftCardService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.Map;


@Service
public class GiftCardServiceImpl extends BaseServiceImpl<GiftCard, String, GiftCardDao> implements GiftCardService {

    @Override
    public Pageable<GiftCard> pageInfo(Map<String, Object> params, Pagination pagination) {
        return dao.pageInfo(params,pagination);
    }

    @Override
    public GiftCard getInfo(GiftCard cardInfo) {
        return dao.getInfo(cardInfo);
    }


}
