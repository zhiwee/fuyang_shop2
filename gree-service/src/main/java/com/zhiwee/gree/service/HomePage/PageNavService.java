package com.zhiwee.gree.service.HomePage;

import com.zhiwee.gree.model.HomePage.PageNav;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.List;
import java.util.Map;

public interface PageNavService extends BaseService<PageNav,String> {
    PageNav getById(String id);

    Pageable<PageNav> getPage(Map<String, Object> param, Pagination pagination);

    List<PageNav> getlist();
}
