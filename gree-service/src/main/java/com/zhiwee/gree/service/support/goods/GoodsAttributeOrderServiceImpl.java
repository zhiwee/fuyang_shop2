package com.zhiwee.gree.service.support.goods;

import com.zhiwee.gree.dao.goods.GoodsAttributeOrderDao;
import com.zhiwee.gree.model.GoodsInfo.GoodsAttributeOrder;
import com.zhiwee.gree.service.GoodsInfo.GoodsAttributeOrderService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.Map;

@Service
public class GoodsAttributeOrderServiceImpl extends BaseServiceImpl<GoodsAttributeOrder,String, GoodsAttributeOrderDao> implements GoodsAttributeOrderService {
    @Override
    public void deleteByGoodsId(Map<String, Object> param) {
        this.dao.deleteByGoodsId(param);
    }
}
