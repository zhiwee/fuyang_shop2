package com.zhiwee.gree.service.support.shopBase;

import com.zhiwee.gree.dao.shopBase.InterfaceDao;
import com.zhiwee.gree.dao.support.shopBase.InterfaceDaoImpl;
import com.zhiwee.gree.dao.support.shopBase.RoleDaoImpl;
import com.zhiwee.gree.model.Interface;
import com.zhiwee.gree.model.Role;
import com.zhiwee.gree.model.User;
import com.zhiwee.gree.model.enums.UserType;
import com.zhiwee.gree.service.shopBase.InterfaceService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Relation;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author gll
 * @since 2018/4/21 17:22
 */
@Service("interfaceService")
public class InterfaceServiceImpl extends BaseServiceImpl<Interface, String, InterfaceDao> implements InterfaceService {

	@Autowired
    InterfaceDaoImpl interfaceDaoImpl;
	@Autowired
    RoleDaoImpl roleDaoImpl;
	@Override
	public void saveUserInterface(List<Relation> entities , User user) {
		if(entities != null && entities.size() > 0){
			if(user.getType() == UserType.Super){
				//超级用户不用验证当前登录人权限
				Map<String,Object> map = new HashMap<>();
//				map.put("userid", entities.get(0).getMainid());
				interfaceDaoImpl.delUserIntByIds(map);
				interfaceDaoImpl.saveUserInterface(entities);
			}else{
				//第一步根据当前登录人id 获取 当前登录人拥有的角色列表
				List<Interface> userIntList = interfaceDaoImpl.getIntByUserId(user.getId());
				//第二步根据当前登录人所拥有的的roleids 查出 被赋权用户 所拥有的角色关联数据
				List<String> interids = new ArrayList<String>();
				for(Interface i : userIntList){
					interids.add(i.getId());
				}
				Map<String,Object> map = new HashMap<>();
//				map.put("userid", entities.get(0).getMainid());
				map.put("interfaceids", interids);
				interfaceDaoImpl.delUserIntByIds(map);
				//第三步添加新的关联数据
				interfaceDaoImpl.saveUserInterface(entities);
			}
		}
	}
	@Override
	public void saveRoleInterface(List<Relation> entities , User user) {
		if(entities != null && entities.size() > 0){
			if(user.getType() == UserType.Super){
				//超级用户不用验证当前登录人权限
				Map<String,Object> map = new HashMap<>();
//				map.put("roleid", entities.get(0).getMainid());
				interfaceDaoImpl.delRoleIntByIds(map);
				interfaceDaoImpl.saveRoleInterface(entities);
			}else{
				//第一步根据当前登录人id 获取 当前登录人拥有的角色列表
				List<Interface> userIntList = interfaceDaoImpl.getIntByUserId(user.getId());
				//第二步根据当前登录人所拥有的的roleids 查出 被赋权用户 所拥有的角色关联数据
				List<String> interids = new ArrayList<String>();
				for(Interface i : userIntList){
					interids.add(i.getId());
				}
				Map<String,Object> map = new HashMap<>();
//				map.put("roleid", entities.get(0).getMainid());
				map.put("interfaceids", interids);
				interfaceDaoImpl.delRoleIntByIds(map);
				//第三步添加新的关联数据
				interfaceDaoImpl.saveRoleInterface(entities);
			}

		}
	}
	@Override
	public List<Interface> getIntByUserId(String userid) {
		return interfaceDaoImpl.getIntByUserId(userid);
	}
	@Override
	public List<Interface> getIntByRoleIds(String... roleids) {
		return interfaceDaoImpl.getIntByRoleIds(roleids);
	}
	@Override
	public Map<String, Interface> getAllIntByUserId(String userid) {
		List<Role> rolelist = roleDaoImpl.getRoleByUserId(userid);
		String roleids = "";
		for(Role r : rolelist){
			roleids = roleids + r.getId() + ",";
		}
		if(StringUtils.isNotEmpty(roleids)){
			roleids = roleids.substring(0,roleids.length() - 1);
		}
		String[] roleidArr = roleids.split(",");
		List<Interface> list = new ArrayList<>();
		list.addAll(interfaceDaoImpl.getIntByUserId(userid));
		list.addAll(interfaceDaoImpl.getIntByRoleIds(roleidArr));

		Map<String,Interface> map = new HashMap<>();
		for(Interface i : list){
			map.put(i.getUrl(), i);
		}
		return map;
	}
}
