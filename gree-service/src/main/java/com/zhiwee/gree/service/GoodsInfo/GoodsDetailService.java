package com.zhiwee.gree.service.GoodsInfo;


import com.zhiwee.gree.model.GoodsInfo.GoodsDetail;
import com.zhiwee.gree.model.GoodsInfo.GoodsInfo;
import com.zhiwee.gree.model.GoodsInfo.SolrGoodsInfo;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.List;
import java.util.Map;


public interface GoodsDetailService extends BaseService<GoodsDetail,String> {

}
