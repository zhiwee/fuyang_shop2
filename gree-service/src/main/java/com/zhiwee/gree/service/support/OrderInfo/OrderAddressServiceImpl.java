package com.zhiwee.gree.service.support.OrderInfo;


import com.zhiwee.gree.dao.OrderInfo.OrderAddressDao;
import com.zhiwee.gree.dao.OrderInfo.OrderItemDao;
import com.zhiwee.gree.dao.OrderInfo.OrderRefundDao;
import com.zhiwee.gree.dao.ShopDistributor.DistributorAreaDao;
import com.zhiwee.gree.dao.ShopDistributor.DistributorOrderDao;
import com.zhiwee.gree.dao.ShopDistributor.ShopDistributorDao;
import com.zhiwee.gree.model.OrderInfo.OrderAddress;
import com.zhiwee.gree.model.OrderInfo.OrderItem;
import com.zhiwee.gree.model.OrderInfo.OrderRefund;
import com.zhiwee.gree.model.ShopDistributor.DistributorArea;
import com.zhiwee.gree.model.ShopDistributor.DistributorOrder;
import com.zhiwee.gree.model.ShopDistributor.ShopDistributor;
import com.zhiwee.gree.service.OrderInfo.OrderAddressService;
import com.zhiwee.gree.service.OrderInfo.OrderRefundService;
import com.zhiwee.gree.util.SequenceUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import xyz.icrab.common.service.support.BaseServiceImpl;
import xyz.icrab.common.util.IdGenerator;

import java.util.*;


@Service
public class OrderAddressServiceImpl extends BaseServiceImpl<OrderAddress,String,OrderAddressDao> implements OrderAddressService {

    @Autowired
    private OrderAddressDao orderAddressDao;



    @Autowired
    private DistributorOrderDao distributorOrderDao;

    @Autowired
    private OrderItemDao orderItemDao;

    @Autowired
    private ShopDistributorDao shopDistributorDao;

    @Autowired
    private DistributorAreaDao distributorAreaDao;

    @Autowired
    private SequenceUtils sequenceUtils;

    @Transactional
    @Override
    public void updateOrders(OrderAddress orderAddress, OrderAddress address, Boolean flag) {
        if(flag == true){
            DistributorOrder dorder = new DistributorOrder();
            dorder.setOrderId(orderAddress.getOrderId());
            distributorOrderDao.delete(dorder);

            Map<String,Object> mapkey = new HashMap<>();
            mapkey.put("defaultDistributor",1);
            ShopDistributor shopDistributor = shopDistributorDao.selectOne(mapkey);
            Map<String,Object> param = new HashMap<>();
            param.put("orderId",orderAddress.getOrderId());
            List<OrderItem> orderItemList = orderItemDao.selectList(param);
            DistributorOrder distributorOrder = null;
            List<DistributorOrder> distributorOrders = new ArrayList<>();
            if(!orderItemList.isEmpty() && orderItemList.size() > 0){
                for(OrderItem orderItem :orderItemList){
                    if(shopDistributor != null && shopDistributor.getCategoryIdPath() != null && !"".equals(shopDistributor.getCategoryIdPath())){
                        if(StringUtils.contains(shopDistributor.getCategoryIdPath(), orderItem.getCategoryId())){
                            distributorOrder =  new DistributorOrder();
                            distributorOrder.setId(IdGenerator.objectId());
                            distributorOrder.setDistributorId(shopDistributor.getId());
                            distributorOrder.setOrderId(orderAddress.getOrderId());
                            distributorOrder.setDeliveryState(2);
                            distributorOrder.setOrderItemId(orderItem.getId());
                            distributorOrder.setState(1);
                            distributorOrder.setSendNum(sequenceUtils.sendNumByDay());
                            distributorOrder.setCreateTime(new Date());
                            distributorOrders.add(distributorOrder);
                        }else{
                            Map<String,Object> params = new HashMap<>();
                            params.put("category",orderItem.getCategoryId());
                            params.put("areaId",orderAddress.getAreaCode());
                            params.put("state",1);
                            List<DistributorArea> shopDistributors = distributorAreaDao.searchByCategory(params);
                            if(shopDistributors != null && shopDistributors.size() > 0){
                                distributorOrder =  new DistributorOrder();
                                distributorOrder.setId(IdGenerator.objectId());
                                distributorOrder.setDistributorId(shopDistributors.get(0).getDistributorId());
                                distributorOrder.setDeliveryState(2);
                                distributorOrder.setOrderId(orderAddress.getOrderId());
                                distributorOrder.setOrderItemId(orderItem.getId());
                                distributorOrder.setState(1);
                                distributorOrder.setSendNum(sequenceUtils.sendNumByDay());
                                distributorOrder.setCreateTime(new Date());
                                distributorOrders.add(distributorOrder);
                            }else{
                                distributorOrder =  new DistributorOrder();
                                distributorOrder.setId(IdGenerator.objectId());
                                distributorOrder.setDistributorId(shopDistributor.getId());
                                distributorOrder.setOrderId(orderAddress.getOrderId());
                                distributorOrder.setDeliveryState(2);
                                distributorOrder.setOrderItemId(orderItem.getId());
                                distributorOrder.setSendNum(sequenceUtils.sendNumByDay());
                                distributorOrder.setState(1);

                                distributorOrder.setCreateTime(new Date());
                                distributorOrders.add(distributorOrder);
                            }
                        }
                    }else{
                        Map<String,Object> params = new HashMap<>();
                        params.put("category",orderItem.getCategoryId());
                        params.put("areaId",orderAddress.getAreaCode());
                        params.put("state",1);
                        List<DistributorArea> shopDistributors = distributorAreaDao.searchByCategory(params);
                        if(shopDistributors != null && shopDistributors.size() > 0){

                            distributorOrder =  new DistributorOrder();
                            distributorOrder.setId(IdGenerator.objectId());

                            distributorOrder.setDistributorId(shopDistributors.get(0).getDistributorId());
                            distributorOrder.setOrderId(orderAddress.getOrderId());
                            distributorOrder.setDeliveryState(2);
                            distributorOrder.setOrderItemId(orderItem.getId());
                            distributorOrder.setState(1);
                            distributorOrder.setSendNum(sequenceUtils.sendNumByDay());
                            distributorOrders.add(distributorOrder);
                            distributorOrder.setCreateTime(new Date());

                        }else{
                            distributorOrder =  new DistributorOrder();
                            distributorOrder.setId(IdGenerator.objectId());
                            distributorOrder.setDistributorId(shopDistributor.getId());
                            distributorOrder.setOrderId(orderAddress.getOrderId());
                            distributorOrder.setDeliveryState(2);
                            distributorOrder.setOrderItemId(orderItem.getId());
                            distributorOrder.setState(1);
                            distributorOrder.setSendNum(sequenceUtils.sendNumByDay());
                            distributorOrder.setCreateTime(new Date());
                            distributorOrders.add(distributorOrder);
                        }
                    }

                }
            }
            distributorOrderDao.insertBatch(distributorOrders);
        }
        address.setProvince(orderAddress.getProvince());

        address.setProvinceCode(orderAddress.getProvinceCode());

        address.setCity(orderAddress.getCity());

        address.setCityCode(orderAddress.getCityCode());

        address.setArea(orderAddress.getArea());

        address.setAreaCode(orderAddress.getAreaCode());

        address.setDetailAddress(orderAddress.getDetailAddress());

        address.setDeliveryPhone(orderAddress.getDeliveryPhone());

        address.setDeliveryUser(orderAddress.getDeliveryUser());

        address.setMobile(orderAddress.getMobile());

        address.setEmsCode(orderAddress.getEmsCode());

        orderAddressDao.update(address);
    }
}
