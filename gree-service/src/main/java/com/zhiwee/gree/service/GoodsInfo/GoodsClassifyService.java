package com.zhiwee.gree.service.GoodsInfo;

import com.zhiwee.gree.model.GoodsInfo.GoodsClassify;
import com.zhiwee.gree.model.GoodsLabel.*;
import com.zhiwee.gree.model.ShopDistributor.CategoryVo;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.List;
import java.util.Map;

public interface GoodsClassifyService extends BaseService<GoodsClassify,String> {


    List<Ps> getPsList(Map<String,Object> param);

    List<Space> getSpaceList(Map<String,Object> param);


    List<Gb> getGbList(Map<String,Object> param);


    List<Bp> getBpsList(Map<String,Object> param);


    List<Efficiency> getEfficienciesList(Map<String,Object> param);

    Pageable<GoodsClassify> achieveSmallCategory(Map<String, Object> param, Pagination pagination);


    Pageable<GoodsClassify> smallCategory(Map<String,Object> param, Pagination pagination);

    List<GoodsClassify>  queryListByRoot(Map<String ,Object> param);

    List<GoodsClassify>  queryListById(Map<String ,Object> param);

    Pageable<GoodsClassify> smallGoodsClassify(Map<String,Object> param, Pagination pagination);

    List<CategoryVo> getCategory(Map<String, Object> param);
}
