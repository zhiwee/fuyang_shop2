package com.zhiwee.gree.service.HomePage;

import com.zhiwee.gree.model.HomePage.PageChannel;
import com.zhiwee.gree.model.HomePage.PageV0.PageChannelVO;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.List;
import java.util.Map;

public interface PageChannelService extends BaseService<PageChannel,String> {
    Pageable<PageChannel> getPage(Map<String, Object> param, Pagination pagination);

    List<PageChannelVO> getlist(Map<String, Object> param);
}
