package com.zhiwee.gree.service.support.ActivityTicketOrder;
import com.zhiwee.gree.dao.ActivityTicketOrder.ActivityTicketOrderDao;
import com.zhiwee.gree.dao.ActivityTicketOrder.TicketInfoDao;
import com.zhiwee.gree.dao.ActivityTicketOrder.TicketOrderDao;
import com.zhiwee.gree.dao.ActivityTicketOrder.TicketOrderRefundDao;
import com.zhiwee.gree.model.ActivityTicketOrder.*;
import com.zhiwee.gree.model.OrderInfo.OrderRefund;
import com.zhiwee.gree.service.ActivityTicketOrder.ActivityTicketOrderService;
import com.zhiwee.gree.service.ActivityTicketOrder.TicketOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;
import xyz.icrab.common.util.IdGenerator;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author sun
 * @since 2019/6/23
 */
@Service
public class TicketOrderServiceImpl extends BaseServiceImpl<TicketOrder, String, TicketOrderDao> implements TicketOrderService {
      @Autowired
     private ActivityTicketOrderDao activityTicketOrderDao;


    @Autowired
    private TicketOrderRefundDao ticketOrderRefundDao;

       @Autowired
      private TicketInfoDao ticketInfoDao;

      @Value("${TICKET_URL}")
      private String ticketUrl;

    @Override
    public Pageable<TicketOrder> pageInfo(Map<String, Object> param, Pagination pagination) {
        return dao.pageInfo(param, pagination);
    }

    @Override
    public Pageable<TicketOrder> achieveMyOrder(Map<String, Object> params, Pagination pagination) {
        return dao.achieveMyOrder(params, pagination);
    }

    @Override
    public List<TicketOrderExport> pageList(Map<String, Object> params) {
        return dao.pageList(params);
    }

    @Transactional
    @Override
    public void updateAndSave(TicketOrder order) {
        ActivityTicketOrder activityTicketOrder = new ActivityTicketOrder(order);

        activityTicketOrder.setId(IdGenerator.objectId());
        activityTicketOrder.setSalesman("格力商城");
        activityTicketOrder.setOrderTime(new Date());
        activityTicketOrder.setActivityId(order.getActivityId());
        activityTicketOrder.setDealerId(order.getDealerId());
        activityTicketOrder.setState(2);
        activityTicketOrder.setCreatetime(new Date());
        activityTicketOrder.setOrderPassWord(order.getOrderPassword());
        //线上认筹
        activityTicketOrder.setOnline(1);
        activityTicketOrder.setPayTime(new Date());
        TicketInfo ticketInfo = new TicketInfo(order);
        ticketInfo.setId(IdGenerator.objectId());
        ticketInfo.setTicketState(TicketState.Paid);
        ticketInfo.setUrl(ticketUrl+order.getTicketSeqno()+"/view");
        ticketInfo.setCreatetime(new Date());
        ticketInfoDao.insert(ticketInfo);
        this.dao.update(order);
        activityTicketOrderDao.insert(activityTicketOrder);
    }

    @Override
    public void updateTicketOrder(TicketOrder order) {
        TicketOrderRefund ticketOrderRefund = new TicketOrderRefund();
        this.dao.update(order);
        Map<String,Object> params  = new HashMap<>();
        params.put("ticketSeqno",order.getTicketSeqno());
        ActivityTicketOrder activityTicketOrder = activityTicketOrderDao.selectOne(params);
        activityTicketOrder.setState(3);
        activityTicketOrder.setUpdatetime(new Date());
        activityTicketOrderDao.update(activityTicketOrder);
        ticketOrderRefund.setId(IdGenerator.objectId());
        ticketOrderRefund.setState(1);
        ticketOrderRefund.setCreateTime(new Date());
        ticketOrderRefund.setTotalPrice(order.getRealPrice());
        ticketOrderRefund.setRefundPrice(order.getRealPrice());
        ticketOrderRefund.setOrderId(order.getId());
        ticketOrderRefundDao.insert(ticketOrderRefund);


    }
}
