package com.zhiwee.gree.service.support.Activity;

import com.zhiwee.gree.dao.Activity.BindInfoDao;
import com.zhiwee.gree.model.Activity.BindInfo;
import com.zhiwee.gree.service.Activity.BindInfoService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.service.support.BaseServiceImpl;


@Service
public class BindInfoServiceImpl extends BaseServiceImpl<BindInfo, String, BindInfoDao> implements BindInfoService {


}
