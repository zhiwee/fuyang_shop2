package com.zhiwee.gree.service.support.goods;

import com.zhiwee.gree.dao.goods.GoodsClassifyDao;
import com.zhiwee.gree.model.GoodsInfo.GoodsClassify;
import com.zhiwee.gree.model.GoodsLabel.*;
import com.zhiwee.gree.model.ShopDistributor.CategoryVo;
import com.zhiwee.gree.service.GoodsInfo.GoodsClassifyService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.List;
import java.util.Map;


@Service
public class GoodsClassifyServiceImpl extends BaseServiceImpl<GoodsClassify,String,GoodsClassifyDao> implements GoodsClassifyService {


    @Override
    public List<Ps> getPsList(Map<String, Object> param) {
        return this.dao.getPsList(param);
    }

    @Override
    public List<Space>getSpaceList(Map<String, Object> param) {
        return this.dao.getSpaceList(param);
    }


    @Override
    public   List<Gb> getGbList(Map<String, Object> param) {
        return this.dao.getGbList(param);
    }


    @Override
    public   List<Bp> getBpsList(Map<String, Object> param) {
        return this.dao.getBpsList(param);
    }

    @Override
    public   List<Efficiency> getEfficienciesList(Map<String, Object> param) {
        return this.dao.getEfficienciesList(param);
    }

    @Override
    public Pageable<GoodsClassify> achieveSmallCategory(Map<String, Object> param, Pagination pagination) {
        return this.dao.achieveSmallCategory(param,pagination);
    }

    @Override
    public Pageable<GoodsClassify>  smallCategory(Map<String, Object> param,Pagination pagination) {
        return this.dao.smallCategory(param,pagination);
}

    @Override
    public List<GoodsClassify> queryListByRoot(Map<String, Object> param) {
        return  this.dao.queryListByRoot(param);
    }

    @Override
    public List<GoodsClassify> queryListById(Map<String ,Object> param) {
        return  this.dao.queryListById(param);
    }

    @Override
    public Pageable<GoodsClassify> smallGoodsClassify(Map<String, Object> param, Pagination pagination) {
        return this.dao.smallGoodsClassify(param,pagination);
    }

    @Override
    public List<CategoryVo> getCategory(Map<String, Object> param) {
        return this.dao.getCategory(param);
    }


}
