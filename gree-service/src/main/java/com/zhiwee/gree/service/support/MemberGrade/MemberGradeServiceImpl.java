package com.zhiwee.gree.service.support.MemberGrade;


import com.zhiwee.gree.dao.MemberGrade.MemberGradeDao;
import com.zhiwee.gree.dao.goods.GoodsInfoDao;
import com.zhiwee.gree.model.GoodsInfo.GoodsInfo;
import com.zhiwee.gree.model.GoodsInfo.SolrGoodsInfo;
import com.zhiwee.gree.model.MemberGrade.MemberGrade;
import com.zhiwee.gree.service.GoodsInfo.GoodsInfoService;
import com.zhiwee.gree.service.MemberGrade.MemberGradeService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.List;
import java.util.Map;


@Service
public class MemberGradeServiceImpl extends BaseServiceImpl<MemberGrade,String,MemberGradeDao> implements MemberGradeService {


    @Override
    public Pageable<MemberGrade> searchAll(Map<String, Object> params, Pagination pagination) {
        return this.dao.searchAll(params,pagination);
    }

    @Override
    public MemberGrade selectMax(Map<String, Object> params) {
        return this.dao.selectMax(params);
    }


}
