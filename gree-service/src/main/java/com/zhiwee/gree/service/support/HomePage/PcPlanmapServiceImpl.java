package com.zhiwee.gree.service.support.HomePage;

import com.zhiwee.gree.dao.HomePage.PcPlanmapDao;
import com.zhiwee.gree.model.HomePage.PcPlanmap;
import com.zhiwee.gree.service.HomePage.PcPlanmapService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.List;
import java.util.Map;

@Service
public class PcPlanmapServiceImpl extends BaseServiceImpl<PcPlanmap,String, PcPlanmapDao> implements PcPlanmapService {
    @Override
    public Pageable<PcPlanmap> getPage(Map<String, Object> params, Pagination pagination) {
        return this.dao.getPage(params,pagination);
    }

    @Override
    public PcPlanmap getById(String id) {
        return this.dao.selectById(id);
    }

    @Override
    public List<PcPlanmap> getList(Map<String, Object> params) {
        return this.dao.getList(params);
    }
}
