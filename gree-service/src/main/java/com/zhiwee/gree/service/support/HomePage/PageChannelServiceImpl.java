package com.zhiwee.gree.service.support.HomePage;

import com.zhiwee.gree.dao.HomePage.PageChannelDao;
import com.zhiwee.gree.model.HomePage.PageChannel;
import com.zhiwee.gree.model.HomePage.PageV0.PageChannelVO;
import com.zhiwee.gree.service.HomePage.PageChannelService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.List;
import java.util.Map;

@Service
public class PageChannelServiceImpl  extends BaseServiceImpl<PageChannel,String, PageChannelDao> implements PageChannelService {

    @Override
    public Pageable<PageChannel> getPage(Map<String, Object> param, Pagination pagination) {
        return this.dao.getPage(param,pagination);
    }

    @Override
    public List<PageChannelVO> getlist(Map<String, Object> param) {
        return this.dao.getList(param);
    }
}
