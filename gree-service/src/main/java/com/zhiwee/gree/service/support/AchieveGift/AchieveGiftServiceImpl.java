package com.zhiwee.gree.service.support.AchieveGift;


import com.zhiwee.gree.dao.AchieveGift.AchieveGiftDao;
import com.zhiwee.gree.model.AchieveGift.AchieveGift;
import com.zhiwee.gree.model.AchieveGift.vo.GiftCount;

import com.zhiwee.gree.service.AchieveGift.AchieveGiftService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.List;
import java.util.Map;


@Service
public class AchieveGiftServiceImpl extends BaseServiceImpl<AchieveGift,String,AchieveGiftDao> implements AchieveGiftService {


    @Override
    public Pageable<AchieveGift> searchGiftInfo(Map<String, Object> params, Pagination pagination) {

        return this.dao.searchGiftInfo(params,pagination);
    }

    @Override
    public List<AchieveGift> searchGift(Map<String, Object> param) {
        return this.dao.searchGift(param);
    }

    @Override
    public List<GiftCount> giftCount(Map<String, Object> param) {
        return this.dao.giftCount(param);
    }
}
