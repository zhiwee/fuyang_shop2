package com.zhiwee.gree.service.GoodsInfo;

import com.zhiwee.gree.model.GoodsInfo.GoodVo.GategoryVo;
import com.zhiwee.gree.model.GoodsInfo.GoodVo.GoodsCategoryCY;
import com.zhiwee.gree.model.GoodsInfo.GoodsCategory;
import com.zhiwee.gree.model.HomePage.PageV0.PageChannelGoodsVO;
import com.zhiwee.gree.model.ShopDistributor.CategoryVo;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.List;
import java.util.Map;

public interface GoodsCategoryService extends BaseService<GoodsCategory,String> {

    Pageable<GoodsCategory> getPage(Map<String, Object> param, Pagination pagination);

    GoodsCategory selectByid(String id);

    List<GategoryVo> getVoList(Map<String, Object> params);

    List<PageChannelGoodsVO> getCategoryGoods(Map<String, Object> params);

    List<GoodsCategoryCY> listInfo();

    List<GoodsCategory> listChildren(Map<String, Object> params);
}
