package com.zhiwee.gree.service.support.goods;


import com.zhiwee.gree.dao.goods.GoodsDetailDao;
import com.zhiwee.gree.dao.goods.GoodsInfoDao;
import com.zhiwee.gree.model.GoodsInfo.GoodsDetail;
import com.zhiwee.gree.model.GoodsInfo.GoodsInfo;
import com.zhiwee.gree.model.GoodsInfo.SolrGoodsInfo;
import com.zhiwee.gree.service.GoodsInfo.GoodsDetailService;
import com.zhiwee.gree.service.GoodsInfo.GoodsInfoService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.List;
import java.util.Map;


@Service
public class GoodsDetailServiceImpl extends BaseServiceImpl<GoodsDetail,String,GoodsDetailDao> implements GoodsDetailService {


}
