package com.zhiwee.gree.service.ShopUserComment;


import com.zhiwee.gree.model.ShopUserComment.CommentPicture;
import com.zhiwee.gree.model.ShopUserComment.ShopUserComment;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;
import xyz.icrab.common.web.session.Session;

import java.util.List;
import java.util.Map;


public interface CommentPictureService extends BaseService<CommentPicture,String> {


}
