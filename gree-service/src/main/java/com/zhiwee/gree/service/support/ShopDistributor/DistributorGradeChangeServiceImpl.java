package com.zhiwee.gree.service.support.ShopDistributor;


import com.zhiwee.gree.dao.ShopDistributor.DistributorGradeChangeDao;
import com.zhiwee.gree.dao.ShopDistributor.DistributorGradeDao;
import com.zhiwee.gree.model.ShopDistributor.DistributorGrade;
import com.zhiwee.gree.model.ShopDistributor.DistributorGradeChange;
import com.zhiwee.gree.service.ShopDistributor.DistributorGradeChangeService;
import com.zhiwee.gree.service.ShopDistributor.DistributorGradeService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.Map;


@Service
public class DistributorGradeChangeServiceImpl extends BaseServiceImpl<DistributorGradeChange, String, DistributorGradeChangeDao> implements DistributorGradeChangeService {




    @Override
    public Pageable<DistributorGradeChange> infoPage(Map<String, Object> param, Pagination pagination) {
        return dao.infoPage(param, pagination);
    }




}
