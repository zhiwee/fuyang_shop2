package com.zhiwee.gree.service.support.ShopDistributor;


import com.zhiwee.gree.dao.ShopDistributor.DistributorAreaDao;
import com.zhiwee.gree.dao.ShopDistributor.ShopDistributorDao;
import com.zhiwee.gree.model.ShopDistributor.DistributorArea;
import com.zhiwee.gree.model.ShopDistributor.ShopDistributor;
import com.zhiwee.gree.service.ShopDistributor.DistributorAreaService;
import com.zhiwee.gree.service.ShopDistributor.ShopDistributorService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.List;
import java.util.Map;


@Service
public class DistributorAreaServiceImpl extends BaseServiceImpl<DistributorArea, String, DistributorAreaDao> implements DistributorAreaService {


    @Override
    public Pageable<DistributorArea> pageArea(Map<String, Object> param, Pagination pagination) {
        return dao.pageArea(param, pagination);
    }

    @Override
    public List<DistributorArea> searchByCategory(Map<String, Object> params) {
        return dao.searchByCategory(params);
    }


}
