package com.zhiwee.gree.service.support.shopBase;

import com.zhiwee.gree.dao.shopBase.RoleDao;
import com.zhiwee.gree.dao.support.shopBase.RoleDaoImpl;
import com.zhiwee.gree.model.Role;
import com.zhiwee.gree.model.User;
import com.zhiwee.gree.model.enums.UserType;
import com.zhiwee.gree.service.shopBase.RoleService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import xyz.icrab.common.model.Relation;
import xyz.icrab.common.service.support.BaseServiceImpl;
import xyz.icrab.common.util.KeyUtils;

import java.util.*;

/**
 * @author gll
 * @since 2018/4/21 17:22
 */
@Service("roleService")
public class RoleServiceImpl extends BaseServiceImpl<Role, String, RoleDao> implements RoleService {

	@Autowired
    RoleDaoImpl roleDaoImpl;

	@Override
	public boolean checkName(String name) {
		int i = roleDaoImpl.checkName(name);
		if(i > 0){
			return true;
		}
	    return false;
	}

	@Override
	public void updateState(Map<String, Object> map) {
		if(map == null){
			map = new HashMap<>();
		}
		roleDaoImpl.updateState(map);
	}

	@Override
	public void saveUserRole(List<Relation> entities , User user) {
		if(entities != null && entities.size() > 0){
			if(user.getType() == UserType.Super){
				//超级用户不用验证当前登录人权限
				Map<String,Object> map = new HashMap<>();
//				map.put("userid", entities.get(0).getMainid());
				roleDaoImpl.delUserRoleByIds(map);
				roleDaoImpl.saveUserRole(entities);
			}else{
				//第一步根据当前登录人id 获取 当前登录人拥有的角色列表
				List<Role> userRoleList = roleDaoImpl.getRoleByUserId(user.getId());
				//第二步根据当前登录人所拥有的的roleids 查出 被赋权用户 所拥有的角色关联数据
				List<String> roleids = new ArrayList<String>();
				for(Role r : userRoleList){
					roleids.add(r.getId());
				}
				Map<String,Object> map = new HashMap<>();
//				map.put("userid", entities.get(0).getMainid());
				map.put("roleids", roleids);
				roleDaoImpl.delUserRoleByIds(map);
				//第三步添加新的关联数据
				roleDaoImpl.saveUserRole(entities);
			}

		}

	}

	@Override
	public List<Role> getRoleByUserId(String userid) {
		return roleDaoImpl.getRoleByUserId(userid);
	}

	@Transactional
	public void resetUserRole(String userId, List<String> addRoleIds) {
		if (!StringUtils.isBlank(userId)) {
			Map<String, Object> map = new HashMap();
			map.put("userid", userId);
			this.roleDaoImpl.delUserRoleByIds(map);
			if (CollectionUtils.isNotEmpty(addRoleIds)) {
				List<Relation> relations = new ArrayList();
				Iterator var5 = addRoleIds.iterator();

				while(var5.hasNext()) {
					String roleId = (String)var5.next();
					Relation relation = new Relation();
					relation.setId(KeyUtils.getKey());
					relation.setMainId(userId);
					relation.setResourceId(roleId);
					relations.add(relation);
				}

				this.roleDaoImpl.saveUserRole(relations);
			}

		}
	}
}
