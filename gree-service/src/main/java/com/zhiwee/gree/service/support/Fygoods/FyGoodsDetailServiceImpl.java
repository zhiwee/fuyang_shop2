package com.zhiwee.gree.service.support.Fygoods;

import com.zhiwee.gree.dao.FyGoods.FyGoodsDetailDao;
import com.zhiwee.gree.model.FyGoods.FyGoodsDetail;
import com.zhiwee.gree.service.FyGoods.FyGoodsDetailService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.List;
import java.util.Map;

@Service
public class FyGoodsDetailServiceImpl
        extends BaseServiceImpl<FyGoodsDetail,String, FyGoodsDetailDao> implements FyGoodsDetailService

{
    @Override
    public List<FyGoodsDetail> queryByGoodsId(Map<String, Object> param) {
        return  this.dao.queryByGoodsId(param);
    }
}
