package com.zhiwee.gree.service.support.Partement;


import com.zhiwee.gree.dao.OrderInfo.OrderAddressDao;
import com.zhiwee.gree.dao.OrderInfo.OrderItemDao;
import com.zhiwee.gree.dao.Partement.PartementDao;
import com.zhiwee.gree.dao.ShopDistributor.DistributorAreaDao;
import com.zhiwee.gree.dao.ShopDistributor.DistributorOrderDao;
import com.zhiwee.gree.dao.ShopDistributor.ShopDistributorDao;
import com.zhiwee.gree.model.OrderInfo.OrderAddress;
import com.zhiwee.gree.model.OrderInfo.OrderItem;
import com.zhiwee.gree.model.Partement.Partement;
import com.zhiwee.gree.model.ShopDistributor.DistributorArea;
import com.zhiwee.gree.model.ShopDistributor.DistributorOrder;
import com.zhiwee.gree.model.ShopDistributor.ShopDistributor;
import com.zhiwee.gree.service.OrderInfo.OrderAddressService;
import com.zhiwee.gree.service.Partement.PartementService;
import com.zhiwee.gree.util.SequenceUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.enums.YesOrNo;
import xyz.icrab.common.service.support.BaseServiceImpl;
import xyz.icrab.common.util.IdGenerator;

import java.util.*;


@Service
public class PartementServiceImpl extends BaseServiceImpl<Partement,String,PartementDao> implements PartementService {


    @Override
    public Pageable<Partement> pageInfo(Map<String, Object> params, Pagination pagination) {
       return  this.dao.pageInfo(params,pagination);
    }

    @Override
    public void disable(String id) {
        Partement partement = new Partement();
        partement.setId(id);
        partement.setState(2);
        dao.updateSelective(partement);
    }

    @Override
    public void enable(String id) {
        Partement partement = new Partement();
        partement.setId(id);
        partement.setState(1);
        dao.updateSelective(partement);
    }


}
