package com.zhiwee.gree.service.support.draw;

import com.zhiwee.gree.dao.draw.DrawsInfoDao;
import com.zhiwee.gree.model.draw.DrawsInfo;
import com.zhiwee.gree.service.draw.DrawsInfoService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.List;
import java.util.Map;

@Service("drawsInfoService")
public class DrawsInfoServiceImpl extends BaseServiceImpl<DrawsInfo, String, DrawsInfoDao> implements DrawsInfoService {


    @Override
    public List<DrawsInfo> checkAllNum(Map<String, Object> param) {
        return dao.checkAllNum(param);
    }

    @Override
    public Pageable<DrawsInfo> pageInfo(Map<String, Object> params, Pagination pagination) {
        return dao.pageInfo(params,pagination);
    }

    @Override
    public List<DrawsInfo> listAll(Map<String, Object> param) {
        return dao.listAll(param);
    }
}
