package com.zhiwee.gree.service.support.Coupon;

import com.zhiwee.gree.dao.Coupon.CouponSubjectDao;
import com.zhiwee.gree.model.Coupon.CouponSubject;
import com.zhiwee.gree.service.Coupon.CouponSubjectService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Page;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.Map;



@Service
public class CouponSubjectServiceImpl extends BaseServiceImpl<CouponSubject, String, CouponSubjectDao> implements CouponSubjectService {

    @Override
    public Pageable<CouponSubject> pageInfo(Map<String, Object> param, Pagination pagination) {
        return dao.pageInfo(param, pagination);
    }
}
