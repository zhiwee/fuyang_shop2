package com.zhiwee.gree.service.support.ShopDistributor;


import com.zhiwee.gree.dao.Dealer.DealerInfoBaseDao;
import com.zhiwee.gree.dao.ShopDistributor.ShopDistributorDao;

import com.zhiwee.gree.model.Dealer.DealerInfo;
import com.zhiwee.gree.model.Dealer.DealerInfoBase;
import com.zhiwee.gree.model.ShopDistributor.ShopDistributor;
import com.zhiwee.gree.service.ShopDistributor.ShopDistributorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.Map;


@Service
public class ShopDistributorServiceImpl extends BaseServiceImpl<ShopDistributor, String, ShopDistributorDao> implements ShopDistributorService {

@Autowired
private DealerInfoBaseDao dealerInfoBaseDao;


    @Override
    public Pageable<ShopDistributor> infoPage(Map<String, Object> param, Pagination pagination) {
        return dao.infoPage(param, pagination);
    }

    @Transactional
    @Override
    public void saveAndUpdate(ShopDistributor shopDistributor, DealerInfoBase dealerInfoBase) {
        this.dao.insert(shopDistributor);
        dealerInfoBaseDao.update(dealerInfoBase);
    }

    @Override
    public void updateDealerInfo(ShopDistributor shopDistributor, DealerInfoBase dealerInfo) {
        this.dao.update(shopDistributor);
        dealerInfoBaseDao.update(dealerInfo);

    }

    @Override
    public ShopDistributor defaultDistributor(Map<String, Object> mapkey) {
         return this.dao.defaultDistributor(mapkey);
    }


}
