package com.zhiwee.gree.service.support.OrderInfo;


import com.zhiwee.gree.dao.OrderInfo.OrderDao;
import com.zhiwee.gree.dao.OrderInfo.OrderRefundReasonDao;
import com.zhiwee.gree.dao.OrderInfo.RefundReasonPictureDao;
import com.zhiwee.gree.model.OrderInfo.Order;
import com.zhiwee.gree.model.OrderInfo.OrderRefundReason;
import com.zhiwee.gree.model.OrderInfo.RefundReasonPicture;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.service.OrderInfo.OrderRefundReasonService;
import com.zhiwee.gree.service.OrderInfo.RefundReasonPictureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;
import xyz.icrab.common.util.IdGenerator;

import java.util.Date;
import java.util.Map;

@Service
public class RefundReasonPictureServiceImpl extends BaseServiceImpl<RefundReasonPicture,String,RefundReasonPictureDao> implements RefundReasonPictureService {

}
