package com.zhiwee.gree.service.support.shopBase;

import com.zhiwee.gree.dao.shopBase.InterfaceDao;
import com.zhiwee.gree.dao.shopBase.MenuDao;
import com.zhiwee.gree.dao.shopBase.RoleDao;
import com.zhiwee.gree.dao.shopBase.UserDao;
import com.zhiwee.gree.model.*;
import com.zhiwee.gree.model.enums.UserType;
import com.zhiwee.gree.service.shopBase.UserService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.enums.MenuType;
import xyz.icrab.common.model.enums.YesOrNo;
import xyz.icrab.common.service.support.BaseServiceImpl;
import xyz.icrab.common.web.param.IdsParam;

import java.util.*;

/**
 * @author Knight
 * @since 2018/4/11 17:22
 */
@Service("userService")
public class UserServiceImpl extends BaseServiceImpl<User, String, UserDao> implements UserService {

    @Autowired
    private RoleDao roleDao;

    @Autowired
    private InterfaceDao interfaceDao;

    @Autowired
    private MenuDao menuDao;


    @Override
    public Pageable<User> page(Map<String, Object> param, Pagination pagination) {
        return dao.page(param, pagination);
    }


    @Override
    public IdentifiedUser login(String username, String password, UserType... userTypes) {
        if (StringUtils.isAnyBlank(new CharSequence[]{username, password})) {
            return null;
        } else if (userTypes == null) {
            return null;
        } else {
            User param = new User();
            param.setUsername(username);
            param.setPassword(password);
            User user = dao.selectOne(param);
            return user == null ? null : this.doLogin(user);
        }
}


    private IdentifiedUser doLogin(User user) {
        IdentifiedUser identifiedUser = new IdentifiedUser(user);
        List menuAndBtns;
        List interfaces;
        ArrayList buttons;
        if (UserType.Super.value().equals(user.getType().value())) {
            Map<String, Object> authParam = new HashMap();
            List<Interface> interfaceses = this.interfaceDao.selectList(authParam);
            identifiedUser.setInterfaces(interfaceses);
            //超级管理员可以查看全部菜单，包括禁用的
            authParam.put("state", YesOrNo.YES.value());
            List<Role> RoleList = this.roleDao.selectList(authParam);
            identifiedUser.setRoles(RoleList);
            menuAndBtns = this.menuDao.selectList(authParam);
        } else {
            List<Role> roles = this.roleDao.getRoleByUserId(user.getId());
            identifiedUser.setRoles(roles);
            buttons = new ArrayList();
            if (CollectionUtils.isNotEmpty(roles)) {
                List<String> roleIds = new ArrayList();
                Iterator var7 = roles.iterator();

                while(var7.hasNext()) {
                    Role role = (Role)var7.next();
                    roleIds.add(role.getId());
                }

                List<Interface> interfacec = this.interfaceDao.getIntByRoleIds((String[])roleIds.toArray(new String[roleIds.size()]));
                if (CollectionUtils.isNotEmpty(interfacec)) {
                    buttons.addAll(interfacec);
                }
            }

            interfaces = this.interfaceDao.getIntByRoleIds(new String[]{user.getId()});
            if (CollectionUtils.isNotEmpty(interfaces)) {
                buttons.addAll(interfaces);
            }

            identifiedUser.setInterfaces(buttons);
            Map<String, Object> menuParam = new HashMap();
            menuParam.put("userId", user.getId());
            menuParam.put("state", YesOrNo.YES);
            menuAndBtns = this.menuDao.getMenuByUser(menuParam);
        }

        List<Menu> menus = new ArrayList();
        buttons = new ArrayList();
        if (CollectionUtils.isNotEmpty(menuAndBtns)) {
            Iterator var13 = menuAndBtns.iterator();

            while(var13.hasNext()) {
                Menu menu = (Menu)var13.next();
                if (MenuType.MENU.value().equals(menu.getType())) {
                    menus.add(menu);
                } else if (MenuType.BUTTON.value().equals(menu.getType())) {
                    buttons.add(menu);
                }
            }
        }

        identifiedUser.setMenus(menus);
        identifiedUser.setButtons(buttons);
        return identifiedUser;
    }



    @Override
    public List<UserExport> exportUser(Map<String, Object> param) {
        return this.dao.exportUser(param);
    }
    @Override
    public User getMyRoleName(Map<String,Object> params) {
        return dao.getMyRoleName(params) ;
    }


    @Override
    @Transactional
    public void deleteUser(IdsParam param) {
        dao.deleteUser(param);
        dao.deleteUserRole(param);
    }

    @Override
    public User searchOne(Map<String, Object> param) {
      return   dao.searchOne(param);
    }

    @Override
    public void updateAndDelete(User user) {
        Map<String, Object> map = new HashMap();
        map.put("userid", user.getId());
        roleDao.delUserRoleByIds(map);
        this.dao.update(user);
    }


}
