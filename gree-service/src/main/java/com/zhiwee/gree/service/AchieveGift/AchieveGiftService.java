package com.zhiwee.gree.service.AchieveGift;


import com.zhiwee.gree.model.AchieveGift.AchieveGift;
import com.zhiwee.gree.model.AchieveGift.vo.GiftCount;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.List;
import java.util.Map;


public interface AchieveGiftService extends BaseService<AchieveGift,String> {

    Pageable<AchieveGift> searchGiftInfo(Map<String, Object> params, Pagination pagination);

    List<AchieveGift> searchGift(Map<String,Object> param);

    List<GiftCount> giftCount(Map<String,Object> param);
}
