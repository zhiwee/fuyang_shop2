package com.zhiwee.gree.service.support.Fygoods;

import com.zhiwee.gree.dao.FyGoods.FygoodsDao;
import com.zhiwee.gree.model.FyGoods.Fygoods;
import com.zhiwee.gree.model.FyGoods.FygoodsExo;
import com.zhiwee.gree.service.FyGoods.FygoodsService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.List;
import java.util.Map;

@Service
public class FygoodsServiceImpl
        extends BaseServiceImpl<Fygoods,String, FygoodsDao> implements FygoodsService {
    @Override
    public Pageable<Fygoods> queryGoodsList(Map<String, Object> param, Pagination pagination) {
        return  this.dao.queryGoodsList(param,pagination);
    }

    @Override
    public List<Fygoods> portalQueryGoodsList(Map<String, Object> param) {
        return  this.dao.portalQueryGoodsList(param);
    }

    //extends BaseServiceImpl<Skillhb,String, SkillhbDao> implements SkillhbService

    @Override
    public List<Fygoods> queryList(Map<String, Object> param) {
        return this.dao.queryList(param);
    }

    @Override
    public Fygoods queryInfoListById(Map<String, Object> param) {
        return dao.queryInfoListById(param);
    }

    @Override
    public List<FygoodsExo> listall() {
        return dao.listall();
    }
}
