package com.zhiwee.gree.service.support.goods;

import com.zhiwee.gree.dao.goods.GoodsSpecsInfoDao;
import com.zhiwee.gree.model.GoodsInfo.GoodsSpecsInfo;
import com.zhiwee.gree.service.GoodsInfo.GoodsSpecsInfoService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.Map;

@Service
public class GoodsSpecsInfoServiceImpl extends BaseServiceImpl<GoodsSpecsInfo,String, GoodsSpecsInfoDao> implements GoodsSpecsInfoService {
    @Override
    public Pageable<GoodsSpecsInfo> getPage(Map<String, Object> param, Pagination pagination) {
        return this.dao.getPage(param,pagination);
    }

    @Override
    public Pageable<GoodsSpecsInfo> getItemPage(Map<String, Object> param, Pagination pagination) {
        return this.dao.getItemPage(param,pagination);
    }
}
