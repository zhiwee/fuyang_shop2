package com.zhiwee.gree.service.card;

import com.zhiwee.gree.model.card.LShopUserCards;
import com.zhiwee.gree.model.card.LShopUserCardsExrt;
import com.zhiwee.gree.model.draw.CanDraws;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.List;
import java.util.Map;

public interface LShopUserCardsService extends BaseService<LShopUserCards,String> {


    Pageable<LShopUserCards> getLShopUserCardsList(Map<String, Object> params, Pagination pagination);

    List<LShopUserCardsExrt> queryLShopUserCardsExport(Map<String, Object> params);


}
