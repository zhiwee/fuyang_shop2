package com.zhiwee.gree.service.Dealer;


import com.zhiwee.gree.model.Dealer.DealerInfoBase;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.BaseService;

import java.util.List;
import java.util.Map;


public interface DealerInfoBaseService extends BaseService<DealerInfoBase,String> {


    /**
     * "id", "name", "parentId", "code"仅获取这几个字段
     * @param param
     * @return
     */
    List<DealerInfoBase> simpleList(Map<String, Object> param);

    Pageable<DealerInfoBase> infoPage(Map<String, Object> param, Pagination pagination);


}
