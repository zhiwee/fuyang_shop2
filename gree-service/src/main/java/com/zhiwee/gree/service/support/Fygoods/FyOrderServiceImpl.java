package com.zhiwee.gree.service.support.Fygoods;

import com.zhiwee.gree.dao.FyGoods.FyOrderDao;
import com.zhiwee.gree.model.FyGoods.FyOrder;
import com.zhiwee.gree.model.FyGoods.vo.FyOrderVo;
import com.zhiwee.gree.model.OrderInfo.GoodsNameTopTen;
import com.zhiwee.gree.model.OrderInfo.vo.OrderStatisticItem;
import com.zhiwee.gree.service.FyGoods.FyOrderService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.service.support.BaseServiceImpl;

import java.util.List;
import java.util.Map;

@Service
public class FyOrderServiceImpl
        extends BaseServiceImpl<FyOrder,String, FyOrderDao> implements FyOrderService {
    @Override
    public Pageable<FyOrder> queryOrderList(Map<String, Object> param, Pagination pagination) {
        return this.dao.queryOrderList(param,pagination);
    }

    @Override
    public void updateByOrderNum(Map<String, Object> params) {
         this.dao.updateByOrderNum(params);
    }

    @Override
    public void deleteByOrderNum(Map<String, Object> params) {
        this.dao.deleteByOrderNum(params);
    }

    @Override
    public void updateSelective(Map<String, Object> map) {
        this.dao.updateSelective(map);
    }

    @Override
    public Pageable<FyOrder> refoundOrderList(Map<String, Object> param, Pagination pagination) {
        return this.dao.refoundOrderList(param,pagination);
    }

    @Override
    public List<FyOrder> queryPayOrderList(Map<String, Object> map) {
        return this.dao. queryPayOrderList(map);
    }

    @Override
    public List<FyOrder> queryList(Map<String, Object> map) {
        return this.dao.queryList(map);
    }

    @Override
    public List<FyOrderVo> queryExportOrderList(Map<String, Object> params) {
        return this.dao.queryExportOrderList(params);
    }

    @Override
    public List<OrderStatisticItem> statisticAllAmountByDate(Map<String, Object> param) {
        return dao.statisticAllAmountByDate(param);
    }

    @Override
    public List<GoodsNameTopTen>  qidoGoodsNameNumTopTen(Map<String, Object> param) {
        return dao.qidoGoodsNameNumTopTen(param);
    }

    @Override
    public List<GoodsNameTopTen>  qidoGoodsNameMoneyTopTen(Map<String, Object> param) {
        return dao.qidoGoodsNameMoneyTopTen(param);
    }

    @Override
    public Object orderAmount(Map<String, Object> params) {
        return dao.orderAmount(params);
    }

    @Override
    public Double totalPrice(Map<String, Object> param) {
        return dao.totalPrice(param);
    }

    @Override
    public List<FyOrder> queryPayOrderList2(Map<String, Object> map) {
        return dao.queryPayOrderList2(map);
    }
}
