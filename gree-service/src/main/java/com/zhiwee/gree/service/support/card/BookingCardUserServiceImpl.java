package com.zhiwee.gree.service.support.card;

import com.zhiwee.gree.dao.crad.BookingCardUserDao;
import com.zhiwee.gree.model.card.BookingCardUser;
import com.zhiwee.gree.service.card.BookingCardUserService;
import org.springframework.stereotype.Service;
import xyz.icrab.common.service.support.BaseServiceImpl;



@Service
public class BookingCardUserServiceImpl extends BaseServiceImpl<BookingCardUser, String, BookingCardUserDao> implements BookingCardUserService {


}
