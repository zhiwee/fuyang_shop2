package com.zhiwee.gree.dao.AchieveGift;


import com.zhiwee.gree.model.AchieveGift.AchieveGift;
import com.zhiwee.gree.model.AchieveGift.vo.GiftCount;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

public interface AchieveGiftDao extends BaseDao<AchieveGift, String> {


    Pageable<AchieveGift> searchGiftInfo(Map<String, Object> params, Pagination pagination);

    List<AchieveGift> searchGift(Map<String,Object> param);

    List<GiftCount> giftCount(Map<String,Object> param);
}
