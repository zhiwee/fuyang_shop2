package com.zhiwee.gree.dao.support.FyGoods;

import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.FyGoods.BargainGoodsDao;
import com.zhiwee.gree.dao.FyGoods.BargainOrderDao;
import com.zhiwee.gree.dao.support.FyGoods.mapper.BargainGoodsMapper;
import com.zhiwee.gree.dao.support.FyGoods.mapper.BargainOrderMapper;
import com.zhiwee.gree.model.FyGoods.BargainGoods;
import com.zhiwee.gree.model.FyGoods.BargainOrder;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;


@Repository
public class BargainOrderDaoImpl
        extends BaseDaoImpl<BargainOrder, String, BargainOrderMapper> implements BargainOrderDao {


}
