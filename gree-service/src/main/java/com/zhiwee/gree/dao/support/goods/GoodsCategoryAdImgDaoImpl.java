package com.zhiwee.gree.dao.support.goods;

import com.github.pagehelper.PageRowBounds;
import com.zhiwee.gree.dao.goods.GoodsCategoryAdImgDao;
import com.zhiwee.gree.dao.support.goods.mapper.GoodsCategoryAdImgMapper;
import com.zhiwee.gree.model.GoodsInfo.GoodsCategoryAdImg;
import com.zhiwee.gree.model.GoodsInfo.GoodsCategoryImg;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

@Repository
public class GoodsCategoryAdImgDaoImpl extends BaseDaoImpl<GoodsCategoryAdImg,String, GoodsCategoryAdImgMapper> implements GoodsCategoryAdImgDao {

    @Override
    public Pageable<GoodsCategoryAdImg> getPage(Map<String, Object> param, Pagination pagination) {
        PageRowBounds rowBounds = new PageRowBounds(pagination.getOffset(), pagination.getPageSize());
        rowBounds.setCount(true);
        List<GoodsCategoryAdImg> data = this.mapper.getPage(param, rowBounds);
        return convertToPageable(rowBounds, data);
    }

    @Override
    public GoodsCategoryAdImg selectById(String id) {
        return this.mapper.selectById(id);
    }
}
