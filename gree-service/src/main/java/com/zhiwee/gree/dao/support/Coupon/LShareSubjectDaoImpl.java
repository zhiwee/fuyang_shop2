package com.zhiwee.gree.dao.support.Coupon;


import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.Coupon.CouponDao;
import com.zhiwee.gree.dao.Coupon.LShareSubjectDao;
import com.zhiwee.gree.dao.support.Coupon.mapper.CouponMapper;
import com.zhiwee.gree.dao.support.Coupon.mapper.LShareSubjectMapper;
import com.zhiwee.gree.model.Coupon.Coupon;
import com.zhiwee.gree.model.Coupon.CouponExport;
import com.zhiwee.gree.model.Coupon.LShareSubject;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;


@Repository
public class LShareSubjectDaoImpl extends BaseDaoImpl<LShareSubject, String, LShareSubjectMapper> implements LShareSubjectDao {

}
