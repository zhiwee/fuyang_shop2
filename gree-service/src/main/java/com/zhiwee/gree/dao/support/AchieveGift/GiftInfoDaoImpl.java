package com.zhiwee.gree.dao.support.AchieveGift;


import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.AchieveGift.AchieveGiftDao;
import com.zhiwee.gree.dao.AchieveGift.GiftInfoDao;
import com.zhiwee.gree.dao.support.AchieveGift.mapper.AchieveGiftMapper;
import com.zhiwee.gree.dao.support.AchieveGift.mapper.GiftInfoMapper;
import com.zhiwee.gree.model.AchieveGift.AchieveGift;
import com.zhiwee.gree.model.AchieveGift.GiftInfo;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.Map;


@Repository
public class GiftInfoDaoImpl extends BaseDaoImpl<GiftInfo, String,GiftInfoMapper> implements GiftInfoDao {


    @Override
    public Pageable<GiftInfo> pageInfo(Map<String, Object> params, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return convertToPageable(this.mapper.pageInfo(params));
    }
}
