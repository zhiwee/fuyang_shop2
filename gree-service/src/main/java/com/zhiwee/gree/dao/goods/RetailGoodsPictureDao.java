package com.zhiwee.gree.dao.goods;


import com.zhiwee.gree.model.GoodsInfo.RetailGoodsPicture;
import com.zhiwee.gree.model.GoodsInfo.ShopGoodsPicture;
import xyz.icrab.common.dao.BaseDao;

public interface RetailGoodsPictureDao extends BaseDao<RetailGoodsPicture, String> {


}
