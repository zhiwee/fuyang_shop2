package com.zhiwee.gree.dao.support.card.mapper;

import com.zhiwee.gree.model.card.BookingCard;
import com.zhiwee.gree.model.card.CardInfo;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

public interface BookingCardMapper extends BaseMapper<BookingCard> {
    List<BookingCard> pageInfo(Map<String, Object> params);
    BookingCard getInfo(BookingCard cardInfo);

}
