package com.zhiwee.gree.dao.support.OrderInfo;



import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.OrderInfo.OrderAddressDao;
import com.zhiwee.gree.dao.OrderInfo.OrderItemDao;
import com.zhiwee.gree.dao.support.OrderInfo.mapper.OrderAddressMapper;
import com.zhiwee.gree.dao.support.OrderInfo.mapper.OrderItemMapper;
import com.zhiwee.gree.model.OrderInfo.OrderAddress;
import com.zhiwee.gree.model.OrderInfo.OrderItem;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.Map;


@Repository
public class OrderAddressDaoImpl extends BaseDaoImpl<OrderAddress, String, OrderAddressMapper> implements OrderAddressDao {



}
