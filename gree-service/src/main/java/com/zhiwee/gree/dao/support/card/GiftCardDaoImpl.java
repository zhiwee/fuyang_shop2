package com.zhiwee.gree.dao.support.card;

import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.crad.GiftCardDao;
import com.zhiwee.gree.dao.support.card.mapper.GiftCardMapper;
import com.zhiwee.gree.model.card.GiftCard;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.Map;

@Repository
public class GiftCardDaoImpl extends BaseDaoImpl<GiftCard, String, GiftCardMapper> implements GiftCardDao {
    @Override
    public Pageable<GiftCard> pageInfo(Map<String, Object> params, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return convertToPageable(this.mapper.pageInfo(params));
    }


    @Override
    public GiftCard getInfo(GiftCard cardInfo) {
        return mapper.getInfo(cardInfo);
    }

}
