package com.zhiwee.gree.dao.support.goods;


import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.goods.GoodsDetailDao;
import com.zhiwee.gree.dao.goods.GoodsInfoDao;
import com.zhiwee.gree.dao.support.goods.mapper.GoodsDetailMapper;
import com.zhiwee.gree.dao.support.goods.mapper.GoodsInfoMapper;
import com.zhiwee.gree.model.GoodsInfo.GoodsDetail;
import com.zhiwee.gree.model.GoodsInfo.GoodsInfo;
import com.zhiwee.gree.model.GoodsInfo.SolrGoodsInfo;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;


@Repository
public class GoodsDetailDaoImpl extends BaseDaoImpl<GoodsDetail, String, GoodsDetailMapper> implements GoodsDetailDao {


}
