package com.zhiwee.gree.dao.support.draw.mapper;

import com.zhiwee.gree.model.draw.DrawsInfo;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

public interface DrawsInfoMapper extends BaseMapper<DrawsInfo> {

    List<DrawsInfo> checkAllNum(Map<String, Object> param);

    List<DrawsInfo> pageInfo(Map<String, Object> params);

    List<DrawsInfo> listAll(Map<String, Object> param);
}
