package com.zhiwee.gree.dao.support.card.mapper;

import com.zhiwee.gree.model.card.GiftCardUser;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

public interface GiftCardUserMapper extends BaseMapper<GiftCardUser> {
    List<GiftCardUser> pageInfo(Map<String, Object> params);

}
