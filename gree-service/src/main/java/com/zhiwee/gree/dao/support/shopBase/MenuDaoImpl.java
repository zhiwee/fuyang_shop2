package com.zhiwee.gree.dao.support.shopBase;

import com.github.pagehelper.PageRowBounds;
import com.zhiwee.gree.dao.shopBase.MenuDao;
import com.zhiwee.gree.dao.support.shopBase.mapper.MenuMapper;
import com.zhiwee.gree.model.Menu;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class MenuDaoImpl extends BaseDaoImpl<Menu, String, MenuMapper> implements MenuDao {

    @Override
    public Pageable getMenuByUser(Map<String, Object> param, Pagination pagination) {
        PageRowBounds rowBounds = new PageRowBounds(pagination.getOffset(), pagination.getPageSize());
        rowBounds.setCount(true);
        List<Menu> data = this.getMapper().getMenuByUser(param);
        return convertToPageable(rowBounds, data);
    }

    @Override
    public List<Menu> getMenuByUser(Map<String, Object> param) {
        return this.getMapper().getMenuByUser(param);
    }

    @Override
    public List<Menu> getMenuByUserId(Map<String, Object> param) {
        return this.getMapper().getMenuByUserId(param);
    }

    @Override
    public List<Menu> getMenuByRoleId(String roleid) {
        Map<String,Object> param=new HashMap<>();
        param.put("roleId",roleid);
        return this.getMapper().getMenuByRoleId(param);
    }

    @Override
    public void addMenuRoleRef(Map<String, Object> param) {
        super.getMapper().delMenuByRoleId(param);
        super.getMapper().addMenuRoleRef(param);
    }

    @Override
    public void addMenuUserRef(Map<String, Object> param) {
        super.getMapper().delMenuByUserId(param);
        super.getMapper().addMenuUserRef(param);
    }
}
