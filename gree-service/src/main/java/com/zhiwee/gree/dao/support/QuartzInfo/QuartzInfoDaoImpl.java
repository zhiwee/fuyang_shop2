package com.zhiwee.gree.dao.support.QuartzInfo;



import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.Partement.PartementDao;
import com.zhiwee.gree.dao.QuartzInfo.QuartzInfoDao;
import com.zhiwee.gree.dao.support.Partement.mapper.PartementMapper;
import com.zhiwee.gree.dao.support.QuartzInfo.mapper.QuartzInfoMapper;
import com.zhiwee.gree.model.Partement.Partement;
import com.zhiwee.gree.model.Quartz.QuartzInfo;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.Map;


@Repository
public class QuartzInfoDaoImpl extends BaseDaoImpl<QuartzInfo, String, QuartzInfoMapper> implements QuartzInfoDao {


    @Override
    public Pageable<QuartzInfo> pageInfo(Map<String, Object> params, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return convertToPageable(this.mapper.pageInfo(params));
    }
}
