package com.zhiwee.gree.dao.support.card.mapper;

import com.zhiwee.gree.model.card.BookingCard;
import com.zhiwee.gree.model.card.GiftCard;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

public interface GiftCardMapper extends BaseMapper<GiftCard> {
    List<GiftCard> pageInfo(Map<String, Object> params);
    GiftCard getInfo(GiftCard cardInfo);

}
