package com.zhiwee.gree.dao.support.goods.mapper;


import com.zhiwee.gree.model.GoodsInfo.RetailGoodsPicture;
import com.zhiwee.gree.model.GoodsInfo.ShopGoodsPicture;
import xyz.icrab.common.dao.support.mapper.BaseMapper;


public interface RetailGoodsPictureMapper extends BaseMapper<RetailGoodsPicture> {


}
