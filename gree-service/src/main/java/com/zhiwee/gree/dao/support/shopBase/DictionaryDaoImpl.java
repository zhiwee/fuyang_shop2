package com.zhiwee.gree.dao.support.shopBase;

import java.util.HashMap;
import java.util.Map;

import com.zhiwee.gree.dao.shopBase.DictionaryDao;
import com.zhiwee.gree.dao.support.shopBase.mapper.DictionaryMapper;
import com.zhiwee.gree.model.Dictionary;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;

/**
 * @author gll
 * @since 2018/4/21 22:35
 */
@Repository
public class DictionaryDaoImpl extends BaseDaoImpl<Dictionary, String, DictionaryMapper> implements DictionaryDao {
	
	@Override
	public int checkGroup(String group) {
		return mapper.checkGroup(group);
	}

	@Override
	public int checkKey(String key,String group) {
		Map<String,String> map = new HashMap<>();
		map.put("key",key);
		map.put("group",group);
		return mapper.checkKey(map);
	}

	@Override
	public int updateState(Map<String, Object> map) {
		if(map == null){
			map = new HashMap<>();
		}
		return mapper.updateState(map);
	}
	
}
