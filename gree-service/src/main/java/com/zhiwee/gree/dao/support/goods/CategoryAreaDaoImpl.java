package com.zhiwee.gree.dao.support.goods;

import com.github.pagehelper.PageRowBounds;
import com.zhiwee.gree.dao.goods.CategoryAreaDao;
import com.zhiwee.gree.dao.goods.GoodsClassifyDao;
import com.zhiwee.gree.dao.support.goods.mapper.CategoryAreaMapper;
import com.zhiwee.gree.dao.support.goods.mapper.GoodsClassifyMapper;
import com.zhiwee.gree.model.GoodsInfo.CategoryArea;
import com.zhiwee.gree.model.GoodsInfo.GoodsClassify;
import com.zhiwee.gree.model.GoodsLabel.*;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;


@Repository
public class CategoryAreaDaoImpl extends BaseDaoImpl<CategoryArea, String,CategoryAreaMapper> implements CategoryAreaDao {



}
