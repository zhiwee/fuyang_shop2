package com.zhiwee.gree.dao.support.FyGoods.mapper;

import com.zhiwee.gree.model.FyGoods.FygoodsCart;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

public interface FygoodsCartMapper  extends BaseMapper<FygoodsCart> {

}