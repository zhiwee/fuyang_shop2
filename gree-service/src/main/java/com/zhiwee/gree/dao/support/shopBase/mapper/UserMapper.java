package com.zhiwee.gree.dao.support.shopBase.mapper;


import com.zhiwee.gree.model.User;
import com.zhiwee.gree.model.UserExport;
import xyz.icrab.common.dao.support.mapper.BaseMapper;
import xyz.icrab.common.web.param.IdsParam;

import java.util.List;
import java.util.Map;

/**
 * @author Knight
 * @since 2018/3/6 22:35
 */
public interface UserMapper extends BaseMapper<User> {


    List<User> selectByParam(Map<String, Object> param);



    void deleteUser(IdsParam param);

    void deleteUserRole(IdsParam param);

    void updateUserPwd(Map<String,Object> params);

    List<UserExport> exportUser(Map<String,Object> param);

    User getMyRoleName(Map<String,Object> param);


    User searchOne(Map<String,Object> param);

}
