package com.zhiwee.gree.dao.goods;

import com.zhiwee.gree.model.GoodsInfo.ProductAttribute;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;


/**
 * Created by jick on 2019/8/28.
 */
public interface ProductAttributeDao extends BaseDao<ProductAttribute,String> {
    //extends BaseDao<GoodsInfo, String>
    // Pageable<GoodsInfo> searchAll(Map<String, Object> params, Pagination pagination);
    //queryList
    Pageable<ProductAttribute> queryList(Map<String, Object> params, Pagination pagination);

    List<ProductAttribute> selectBycategoryId(Map<String, Object> param);



}
