package com.zhiwee.gree.dao.support.Partement;



import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.OrderInfo.OrderAddressDao;
import com.zhiwee.gree.dao.Partement.PartementDao;
import com.zhiwee.gree.dao.support.OrderInfo.mapper.OrderAddressMapper;
import com.zhiwee.gree.dao.support.Partement.mapper.PartementMapper;
import com.zhiwee.gree.model.OrderInfo.OrderAddress;
import com.zhiwee.gree.model.Partement.Partement;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.Map;


@Repository
public class PartementDaoImpl extends BaseDaoImpl<Partement, String, PartementMapper> implements PartementDao {


    @Override
    public Pageable<Partement> pageInfo(Map<String, Object> params, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return convertToPageable(this.mapper.pageInfo(params));
    }
}
