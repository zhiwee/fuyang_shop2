package com.zhiwee.gree.dao.shopBase;

import com.zhiwee.gree.model.User;
import com.zhiwee.gree.model.UserExport;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.web.param.IdsParam;

import java.util.List;
import java.util.Map;

/**
 * @author Knight
 * @since 2018/3/6 22:31
 */
public interface UserDao extends BaseDao<User, String> {

    Pageable<User> page(Map<String, Object> param, Pagination pagination);

    List<UserExport> exportUser(Map<String,Object> param);

    User getMyRoleName(Map<String,Object> params);

    void deleteUser(IdsParam param);

    void deleteUserRole(IdsParam param);

    User searchOne(Map<String,Object> param);

}
