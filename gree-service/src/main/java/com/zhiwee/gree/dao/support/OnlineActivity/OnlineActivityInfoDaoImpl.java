package com.zhiwee.gree.dao.support.OnlineActivity;

import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.OnlineActivity.OnlineActivityInfoDao;
import com.zhiwee.gree.dao.support.OnlineActivity.mapper.OnlineActivityInfoMapper;
import com.zhiwee.gree.model.GoodsInfo.LAPeople;
import com.zhiwee.gree.model.OnlineActivity.OnlineActivityInfo;
import com.zhiwee.gree.model.OnlineActivity.OnlineActivityInfoExe;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

/**
 * @author Knight
 * @since 2018/7/6 14:26
 */
@Repository
public class OnlineActivityInfoDaoImpl extends BaseDaoImpl<OnlineActivityInfo, String, OnlineActivityInfoMapper> implements OnlineActivityInfoDao {


    @Override
    public List<OnlineActivityInfoExe> exportActivityInfo(Map<String, Object> params) {
        return mapper.exportActivityInfo(params);
    }
}
