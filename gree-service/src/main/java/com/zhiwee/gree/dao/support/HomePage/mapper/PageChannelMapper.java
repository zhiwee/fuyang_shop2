package com.zhiwee.gree.dao.support.HomePage.mapper;

import com.github.pagehelper.PageRowBounds;
import com.zhiwee.gree.model.HomePage.PageChannel;
import com.zhiwee.gree.model.HomePage.PageV0.PageChannelVO;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

public interface PageChannelMapper extends BaseMapper<PageChannel> {
    List<PageChannel> getPage(Map<String, Object> param, PageRowBounds rowBounds);

    List<PageChannelVO> getList(Map<String, Object> param);
}
