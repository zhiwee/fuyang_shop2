package com.zhiwee.gree.dao.support.card;

import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.crad.CardInfoDao;
import com.zhiwee.gree.dao.support.card.mapper.CardInfoMapper;
import com.zhiwee.gree.model.card.CardInfo;
import com.zhiwee.gree.model.card.LShopUserCards;
import com.zhiwee.gree.model.draw.DrawsInfo;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.Map;


@Repository
public class CardInfoDaoImpl extends BaseDaoImpl<CardInfo, String, CardInfoMapper> implements CardInfoDao {
    @Override
    public Pageable<CardInfo> pageInfo(Map<String, Object> params, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return convertToPageable(this.mapper.pageInfo(params));
    }

    @Override
    public CardInfo getInfo(CardInfo cardInfo) {
        return mapper.getInfo(cardInfo);
    }

}
