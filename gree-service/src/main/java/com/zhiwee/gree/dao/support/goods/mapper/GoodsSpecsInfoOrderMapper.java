package com.zhiwee.gree.dao.support.goods.mapper;

import com.zhiwee.gree.model.GoodsInfo.GoodVo.SpecsInfoOrderVO;
import com.zhiwee.gree.model.GoodsInfo.GoodsSpecsInfoOrder;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;


public interface GoodsSpecsInfoOrderMapper extends BaseMapper<GoodsSpecsInfoOrder> {
    List<SpecsInfoOrderVO> selectOneList(Map<String, Object> params);
}
