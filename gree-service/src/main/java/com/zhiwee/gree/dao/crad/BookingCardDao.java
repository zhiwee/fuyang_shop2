package com.zhiwee.gree.dao.crad;

import com.zhiwee.gree.model.card.BookingCard;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.Map;


public interface BookingCardDao extends BaseDao<BookingCard,String> {
    Pageable<BookingCard> pageInfo(Map<String, Object> params, Pagination pagination);

    BookingCard getInfo(BookingCard cardInfo);
}
