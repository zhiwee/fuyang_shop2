package com.zhiwee.gree.dao.HomePage;

import com.zhiwee.gree.model.HomePage.PageChannelGoods;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.Map;

public interface PageChannelGoodsDao extends BaseDao<PageChannelGoods,String> {
    Pageable<PageChannelGoods> getPage(Map<String, Object> param, Pagination pagination);
}
