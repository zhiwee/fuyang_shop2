package com.zhiwee.gree.dao.Dept;

import com.zhiwee.gree.model.DeptInfo.DeptopenInfo;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.Map;

/**
 * Created by jick on 2019/8/23.
 */
public interface DeptopenInfoDao  extends BaseDao<DeptopenInfo,String> {
    Pageable<DeptopenInfo> queryDeptOpenInfo(Map<String,Object> param, Pagination pagination);
   //    Pageable<Invoice>

}
