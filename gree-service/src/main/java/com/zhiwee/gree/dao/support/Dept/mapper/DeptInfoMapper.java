package com.zhiwee.gree.dao.support.Dept.mapper;

import com.zhiwee.gree.model.DeptInfo.DeptInfo;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * Created by jick on 2019/8/26.
 */
public interface DeptInfoMapper extends BaseMapper<DeptInfo> {
    //   List<DeptopenInfo> queryDeptOpenInfo(Map<String,Object> param);
    List<DeptInfo>  pageInfo(Map<String,Object> param);
}
