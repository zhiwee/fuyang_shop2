package com.zhiwee.gree.dao.support.WithdrawalRecord;


import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.WithdrawalRecord.WithdrawalRecordDao;
import com.zhiwee.gree.dao.support.WithdrawalRecord.mapper.WithdrawalRecordMapper;
import com.zhiwee.gree.model.WithdrawalRecord.WithdrawalRecord;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;


@Repository
public class WithdrawalRecordDaoImpl extends BaseDaoImpl<WithdrawalRecord, String, WithdrawalRecordMapper> implements WithdrawalRecordDao {




    @Override
    public Pageable<WithdrawalRecord> pageInfo(Map<String, Object> param, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum() + 1, pagination.getPageSize());
        List<WithdrawalRecord> withdrawalRecords = mapper.pageInfo(param);
        return convertToPageable(withdrawalRecords);
    }

}
