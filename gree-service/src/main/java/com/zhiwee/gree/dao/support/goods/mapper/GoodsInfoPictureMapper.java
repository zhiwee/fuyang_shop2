package com.zhiwee.gree.dao.support.goods.mapper;

import com.zhiwee.gree.model.GoodsInfo.GoodsInfoPicture;
import org.apache.ibatis.annotations.Param;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

public interface GoodsInfoPictureMapper  extends BaseMapper<GoodsInfoPicture> {
    List<GoodsInfoPicture> listAll(Map<String, Object> param);

    List<GoodsInfoPicture> pageInfo(Map<String, Object> params);

    GoodsInfoPicture getInfo(@Param("id") String id);
    //  extends BaseMapper<Goods>
}
