package com.zhiwee.gree.dao.support.shopBase.mapper;

import java.util.List;
import java.util.Map;

import com.zhiwee.gree.model.Role;
import xyz.icrab.common.dao.support.mapper.BaseMapper;
import xyz.icrab.common.model.Relation;

/**
 * @author gll
 * @since 2018/4/21 22:35
 */
public interface RoleMapper extends BaseMapper<Role> {
	/*
	 * 检查分组有没有重名
	 */
    int checkName(String group);
    /*
	 * 停用启用
	 */
    int updateState(Map<String, Object> map);
    /*
	 * 用户添加角色数据
	 */
	int saveUserRole(List<Relation> list);
	/*
	 * 根据模块userid删除用户角色关系数据
	 */
	int delUserRoleByIds(Map<String, Object> map);
	/*
	 * 根据userid获取用户角色
	 */
	List<Role> getRoleByUserId(String userid);
}
