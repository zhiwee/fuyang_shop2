package com.zhiwee.gree.dao.support.goods.mapper;


import com.github.pagehelper.PageRowBounds;
import com.zhiwee.gree.model.GoodsInfo.CategoryArea;
import com.zhiwee.gree.model.GoodsInfo.GoodsClassify;
import com.zhiwee.gree.model.GoodsLabel.*;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

public interface CategoryAreaMapper extends BaseMapper<CategoryArea> {

}
