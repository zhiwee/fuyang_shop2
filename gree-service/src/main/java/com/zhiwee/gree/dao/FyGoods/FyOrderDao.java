package com.zhiwee.gree.dao.FyGoods;

import com.zhiwee.gree.model.FyGoods.FyOrder;
import com.zhiwee.gree.model.FyGoods.vo.FyOrderVo;
import com.zhiwee.gree.model.OrderInfo.GoodsNameTopTen;
import com.zhiwee.gree.model.OrderInfo.vo.OrderStatisticItem;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;


public interface FyOrderDao   extends BaseDao<FyOrder,String> {
    //  extends BaseDao<FyGoodsImg,String>
 //   List<FyOrder>  queryOrderList(Map<String,Object> param);
    // Pageable<Skillhb> queryList(Map<String, Object> params, Pagination pagination);
    Pageable<FyOrder> queryOrderList(Map<String, Object> params, Pagination pagination);

    void updateByOrderNum(Map<String, Object> params);

    void deleteByOrderNum(Map<String, Object> params);

    void updateSelective(Map<String,Object> params);

    Pageable<FyOrder> refoundOrderList(Map<String, Object> param, Pagination pagination);

    List<FyOrder> queryPayOrderList(Map<String, Object> map);

    List<FyOrder> queryList(Map<String, Object> map);

    List<FyOrderVo> queryExportOrderList(Map<String, Object> params);

    List<OrderStatisticItem> statisticAllAmountByDate(Map<String, Object> param);

    List<GoodsNameTopTen>  qidoGoodsNameNumTopTen(Map<String, Object> param);

    List<GoodsNameTopTen>  qidoGoodsNameMoneyTopTen(Map<String, Object> param);

    Object orderAmount(Map<String, Object> params);

    Double totalPrice(Map<String, Object> param);

    List<FyOrder> queryPayOrderList2(Map<String, Object> map);
}
