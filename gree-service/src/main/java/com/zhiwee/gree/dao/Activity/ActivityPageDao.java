package com.zhiwee.gree.dao.Activity;

import com.zhiwee.gree.model.Activity.ActivityInfo;
import com.zhiwee.gree.model.Activity.ActivityPage;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.Map;

/**
 * @author sun
 * @since 2019/7/6 14:25
 */
public interface ActivityPageDao extends BaseDao<ActivityPage, String> {

    Pageable<ActivityPage> pageInfo(Map<String,Object> param, Pagination pagination);
}
