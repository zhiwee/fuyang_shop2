package com.zhiwee.gree.dao.support.OrderInfo;



import com.zhiwee.gree.dao.OrderInfo.OrderCouponDao;
import com.zhiwee.gree.dao.OrderInfo.OrderPaymentDao;
import com.zhiwee.gree.dao.support.OrderInfo.mapper.OrderCouponMapper;
import com.zhiwee.gree.dao.support.OrderInfo.mapper.OrderPaymentMapper;
import com.zhiwee.gree.model.OrderInfo.OrderCoupon;
import com.zhiwee.gree.model.OrderInfo.OrderPayment;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;


@Repository
public class OrderCouponDaoImpl extends BaseDaoImpl<OrderCoupon, String, OrderCouponMapper> implements OrderCouponDao {


}
