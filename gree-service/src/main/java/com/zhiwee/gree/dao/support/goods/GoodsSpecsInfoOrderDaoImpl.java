package com.zhiwee.gree.dao.support.goods;

import com.zhiwee.gree.dao.goods.GoodsSpecsInfoOrderDao;
import com.zhiwee.gree.dao.support.goods.mapper.GoodsSpecsInfoOrderMapper;
import com.zhiwee.gree.model.GoodsInfo.GoodVo.SpecsInfoOrderVO;
import com.zhiwee.gree.model.GoodsInfo.GoodsSpecsInfoOrder;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;

import java.util.List;
import java.util.Map;

@Repository
public class GoodsSpecsInfoOrderDaoImpl extends BaseDaoImpl<GoodsSpecsInfoOrder,String, GoodsSpecsInfoOrderMapper> implements GoodsSpecsInfoOrderDao {
    @Override
    public List<SpecsInfoOrderVO> selectOneList(Map<String, Object> params) {
        return this.mapper.selectOneList(params);
    }
}
