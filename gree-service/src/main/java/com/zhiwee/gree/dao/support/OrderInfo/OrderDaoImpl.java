package com.zhiwee.gree.dao.support.OrderInfo;



import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.OrderInfo.OrderDao;
import com.zhiwee.gree.dao.support.OrderInfo.mapper.OrderMapper;
import com.zhiwee.gree.model.OrderInfo.Order;
import com.zhiwee.gree.model.OrderInfo.OrderExport;
import com.zhiwee.gree.model.OrderInfo.vo.OrderStatisticItem;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;


@Repository
public class OrderDaoImpl extends BaseDaoImpl<Order, String, OrderMapper> implements OrderDao {


    @Override
    public Pageable<Order> pageInfo(Map<String, Object> params, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return convertToPageable(this.mapper.pageInfo(params));
    }

    @Override
    public List<OrderExport> queryOrderExport(Map<String, Object> params) {
        return this.mapper.queryOrderExport(params);
    }

    @Override
    public Pageable<Order> achieveMyOrder(Map<String, Object> params, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return convertToPageable(this.mapper.achieveMyOrder(params));
    }

    @Override
    public Integer orderAmount(Map<String, Object> params) {
        return this.mapper.orderAmount(params);
    }

    @Override
    public List<OrderStatisticItem> statisticAllAmountByDate(Map<String, Object> param) {
        return this.mapper.statisticAllAmountByDate(param);
    }

    @Override
    public Pageable<Order> achieveMyEngineerOrder(Map<String, Object> params, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return convertToPageable(this.mapper.achieveMyEngineerOrder(params));
    }

    @Override
    public Double totalPrice(Map<String,Object> param) {
        return this.mapper.totalPrice(param);
    }
}
