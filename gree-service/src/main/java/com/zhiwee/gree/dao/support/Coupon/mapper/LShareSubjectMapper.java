package com.zhiwee.gree.dao.support.Coupon.mapper;


import com.zhiwee.gree.model.Coupon.Coupon;
import com.zhiwee.gree.model.Coupon.CouponExport;
import com.zhiwee.gree.model.Coupon.LShareSubject;
import org.apache.ibatis.annotations.Param;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;


public interface LShareSubjectMapper extends BaseMapper<LShareSubject> {


}
