package com.zhiwee.gree.dao.ShopUserAddress;


import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.ShopUserAddress.ShopUserAddress;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.web.param.IdsParam;

import java.util.List;
import java.util.Map;

public interface ShopUserAddressDao extends BaseDao<ShopUserAddress, String> {


}
