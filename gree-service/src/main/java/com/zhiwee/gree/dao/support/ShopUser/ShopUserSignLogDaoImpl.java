package com.zhiwee.gree.dao.support.ShopUser;

import com.zhiwee.gree.dao.ShopUser.ShopUserDao;
import com.zhiwee.gree.dao.ShopUser.ShopUserSignLogDao;
import com.zhiwee.gree.dao.support.ShopUser.mapper.ShopUserMapper;
import com.zhiwee.gree.dao.support.ShopUser.mapper.ShopUserSignLogMapper;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.ShopUser.ShopUserSignLog;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;

import java.util.Map;

/**
 * TODO
 *
 * @author Chensong
 * @Company http://www.zhiwee.com
 * date 2020-02-10
 */
@Repository
public class ShopUserSignLogDaoImpl extends BaseDaoImpl<ShopUserSignLog, String, ShopUserSignLogMapper> implements ShopUserSignLogDao {

    @Override
    public Integer signAmount(Map<String, Object> params) {
        return mapper.signAmount(params);
    }
    @Override
    public ShopUserSignLog getOneLog(Map<String, Object> params) {
        return mapper.getOneLog(params);
    }
}
