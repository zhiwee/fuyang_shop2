package com.zhiwee.gree.dao.support.order;



import com.zhiwee.gree.dao.order.BaseOrderDao;
import com.zhiwee.gree.dao.support.order.mapper.BaseOrderMapper;
import com.zhiwee.gree.model.order.BaseOrder;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;

@Repository
public class BaseOrderDaoImpl extends  BaseDaoImpl<BaseOrder,String, BaseOrderMapper>
        implements BaseOrderDao {
    // extends BaseDaoImpl<OrderAddress, String, OrderAddressMapper> implements OrderAddressDao
}
