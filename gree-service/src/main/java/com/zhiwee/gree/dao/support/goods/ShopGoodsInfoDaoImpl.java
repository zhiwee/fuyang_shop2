package com.zhiwee.gree.dao.support.goods;


import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.goods.ShopGoodsInfoDao;

import com.zhiwee.gree.dao.support.goods.mapper.ShopGoodsInfoMapper;

import com.zhiwee.gree.model.GoodsInfo.ShopGoodsInfo;

import com.zhiwee.gree.model.GoodsInfo.SolrGoodsInfo;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;


@Repository
public class ShopGoodsInfoDaoImpl extends BaseDaoImpl<ShopGoodsInfo, String, ShopGoodsInfoMapper> implements ShopGoodsInfoDao {

    @Override
    public List<ShopGoodsInfo> achieveGoodsByName(Map<String, Object> param) {
        return mapper.achieveGoodsByName(param);
    }

    @Override
    public List<SolrGoodsInfo> getAllGoods(Map<String, Object> param) {
        return this.mapper.getAllGoods(param);
    }

    @Override
    public Pageable<ShopGoodsInfo> searchAll(Map<String, Object> params, Pagination pagination) {

        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return convertToPageable(this.mapper.searchAll(params));
    }

    @Override
    public ShopGoodsInfo achieveGoodsById(Map<String, Object> params) {
        return this.mapper.achieveGoodsById(params);
    }

    @Override
    public List<ShopGoodsInfo> achieveSpecialGoods(Map<String, Object> params) {
        return this.mapper.achieveSpecialGoods(params);
    }

    @Override
    public Pageable<ShopGoodsInfo> achieveSmallGoods(Map<String, Object> param, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return convertToPageable(this.mapper.achieveSmallGoods(param));
    }

    @Override
    public List<ShopGoodsInfo> achieveRecommend(Map<String, Object> param) {
        return this.mapper.achieveRecommend(param);
    }

    @Override
    public Pageable<ShopGoodsInfo> searchEngineerGood(Map<String, Object> params, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum()-1, pagination.getPageSize(), true);
        return convertToPageable(this.mapper.searchAll(params));
    }

    @Override
    public ShopGoodsInfo searchOrderGoods(String goodId) {
        return this.mapper.searchOrderGoods(goodId);
    }

    @Override
    public List<ShopGoodsInfo> achieveHotSellGood(Map<String,Object> param) {
        return this.mapper.achieveHotSellGood(param);
    }


}
