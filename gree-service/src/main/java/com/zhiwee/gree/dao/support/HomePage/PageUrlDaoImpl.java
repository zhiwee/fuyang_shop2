package com.zhiwee.gree.dao.support.HomePage;

import com.github.pagehelper.PageRowBounds;
import com.zhiwee.gree.dao.HomePage.PageUrlDao;
import com.zhiwee.gree.dao.support.HomePage.mapper.PageUrlMapper;
import com.zhiwee.gree.model.HomePage.PageSearch;
import com.zhiwee.gree.model.HomePage.PageUrl;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

@Repository
public class PageUrlDaoImpl  extends BaseDaoImpl<PageUrl,String, PageUrlMapper> implements PageUrlDao {
    @Override
    public Pageable<PageUrl> getPage(Map<String, Object> param, Pagination pagination) {
        PageRowBounds rowBounds = new PageRowBounds(pagination.getOffset(), pagination.getPageSize());
        rowBounds.setCount(true);
        List<PageUrl> data = this.mapper.getPage(param, rowBounds);
        return convertToPageable(rowBounds, data);
    }
}
