package com.zhiwee.gree.dao.support.Questionnaire.mapper;

import com.zhiwee.gree.model.Questionnaire.AnswerInfo;
import com.zhiwee.gree.model.Questionnaire.AnswerInfoExrt;
import com.zhiwee.gree.model.Questionnaire.Questionnaire;
import com.zhiwee.gree.model.draw.DrawUseInfo;
import com.zhiwee.gree.model.draw.DrawUseInfoExrt;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

public interface AnswerInfoMapper extends BaseMapper<AnswerInfo> {
    List<AnswerInfo> pageInfo(Map<String, Object> params);

    List<AnswerInfoExrt> queryAnswerInfoExport(Map<String, Object> params);
}
