package com.zhiwee.gree.dao.support.OrderInfo;



import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.OrderInfo.OrderRefundDao;
import com.zhiwee.gree.dao.OrderInfo.OrderRefundReasonDao;
import com.zhiwee.gree.dao.support.OrderInfo.mapper.OrderRefundMapper;
import com.zhiwee.gree.dao.support.OrderInfo.mapper.OrderRefundReasonMapper;
import com.zhiwee.gree.model.OrderInfo.Order;
import com.zhiwee.gree.model.OrderInfo.OrderRefund;
import com.zhiwee.gree.model.OrderInfo.OrderRefundReason;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;


@Repository
public class OrderRefundReasonDaoImpl extends BaseDaoImpl<OrderRefundReason, String, OrderRefundReasonMapper> implements OrderRefundReasonDao {


    @Override
    public Pageable<OrderRefundReason> pageInfo(Map<String, Object> params, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return convertToPageable(this.mapper.pageInfo(params));
    }

    @Override
    public List<OrderRefundReason> achieveRefundInfo(Map<String, Object> param) {
        return this.mapper.achieveRefundInfo(param);
    }

    @Override
    public List<OrderRefundReason> achieveChangeOrderInfo(Map<String, Object> param) {
        return this.mapper.achieveChangeOrderInfo(param);
    }
}
