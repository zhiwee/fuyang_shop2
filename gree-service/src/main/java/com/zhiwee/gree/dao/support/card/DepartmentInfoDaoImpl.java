package com.zhiwee.gree.dao.support.card;

import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.crad.DepartmentInfoDao;
import com.zhiwee.gree.dao.support.card.mapper.DepartmentInfoMapper;
import com.zhiwee.gree.model.card.CardInfo;
import com.zhiwee.gree.model.card.DepartmentInfo;
import com.zhiwee.gree.model.card.DepartmentInfoCY;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;


@Repository
public class DepartmentInfoDaoImpl extends BaseDaoImpl<DepartmentInfo, String, DepartmentInfoMapper> implements DepartmentInfoDao {
    @Override
    public Pageable<DepartmentInfo> pageInfo(Map<String, Object> params, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return convertToPageable(this.mapper.pageInfo(params));
    }

    @Override
    public List<DepartmentInfoCY> listAll() {
        return mapper.listAll();
    }


}
