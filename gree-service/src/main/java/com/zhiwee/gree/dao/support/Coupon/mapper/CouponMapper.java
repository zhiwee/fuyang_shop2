package com.zhiwee.gree.dao.support.Coupon.mapper;


import com.zhiwee.gree.model.Coupon.Coupon;
import com.zhiwee.gree.model.Coupon.CouponExport;
import com.zhiwee.gree.model.GoodsInfo.GoodsInfo;
import com.zhiwee.gree.model.GoodsInfo.SolrGoodsInfo;
import org.apache.ibatis.annotations.Param;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;


public interface CouponMapper extends BaseMapper<Coupon> {

    List<Coupon> pageInfo(Map<String,Object> param);

    List<CouponExport> exportCoupon(Map<String,Object> exportCoupon);

    Coupon getCouponById(@Param("id")String id);

    void deleteList(Map<String, Object> delId);

    void updateAll(Map<String, Object> param);

    List<Coupon> listAll(Map<String, Object> params);
}
