package com.zhiwee.gree.dao.FyGoods;

import com.zhiwee.gree.model.FyGoods.Fygoods;
import com.zhiwee.gree.model.FyGoods.GrouponGoods;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

public interface GrouponGoodsDao extends BaseDao<GrouponGoods,String> {
    List<GrouponGoods> queryGoodsList(Map<String, Object> param, Pagination pagination);

}
