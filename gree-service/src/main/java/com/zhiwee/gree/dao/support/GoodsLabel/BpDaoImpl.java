package com.zhiwee.gree.dao.support.GoodsLabel;


import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.GoodsLabel.BpDao;
import com.zhiwee.gree.dao.Integral.IntegralDao;
import com.zhiwee.gree.dao.support.GoodsLabel.mapper.BpMapper;
import com.zhiwee.gree.dao.support.Integral.mapper.IntegralMapper;
import com.zhiwee.gree.model.GoodsLabel.Bp;
import com.zhiwee.gree.model.Integral.Integral;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;


@Repository
public class BpDaoImpl extends BaseDaoImpl<Bp, String, BpMapper> implements BpDao {



}
