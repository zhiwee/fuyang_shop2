package com.zhiwee.gree.dao.OrderInfo;


import com.zhiwee.gree.model.OrderInfo.Order;
import com.zhiwee.gree.model.OrderInfo.OrderRefundReason;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

public interface OrderRefundReasonDao extends BaseDao<OrderRefundReason, String> {


    Pageable<OrderRefundReason> pageInfo(Map<String,Object> params, Pagination pagination);

    List<OrderRefundReason> achieveRefundInfo(Map<String,Object> param);

    List<OrderRefundReason> achieveChangeOrderInfo(Map<String,Object> param);
}
