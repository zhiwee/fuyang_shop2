package com.zhiwee.gree.dao.Questionnaire;

import com.zhiwee.gree.model.Questionnaire.AnswerInfo;
import com.zhiwee.gree.model.Questionnaire.AnswerInfoExrt;
import com.zhiwee.gree.model.Questionnaire.Questionnaire;
import com.zhiwee.gree.model.draw.DrawUseInfoExrt;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

public interface AnswerInfoDao extends BaseDao<AnswerInfo, String> {
    Pageable<AnswerInfo> pageInfo(Map<String, Object> params, Pagination pagination);


    List<AnswerInfoExrt> queryAnswerInfoExport(Map<String,Object> params);
}
