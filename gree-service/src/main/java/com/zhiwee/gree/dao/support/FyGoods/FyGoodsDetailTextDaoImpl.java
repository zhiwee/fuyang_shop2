package com.zhiwee.gree.dao.support.FyGoods;

import com.zhiwee.gree.dao.FyGoods.FyGoodsDetailTextDao;
import com.zhiwee.gree.dao.support.FyGoods.mapper.FyGoodsDetailMapper;
import com.zhiwee.gree.dao.support.FyGoods.mapper.FyGoodsDetailTextMapper;
import com.zhiwee.gree.model.FyGoods.FyGoodsDetailText;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;

@Repository
public class FyGoodsDetailTextDaoImpl extends BaseDaoImpl<FyGoodsDetailText, String, FyGoodsDetailTextMapper>
        implements FyGoodsDetailTextDao {
}
