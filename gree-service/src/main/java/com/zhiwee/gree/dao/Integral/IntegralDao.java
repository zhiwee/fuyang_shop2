package com.zhiwee.gree.dao.Integral;


import com.zhiwee.gree.model.Integral.Integral;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.Map;

public interface IntegralDao extends BaseDao<Integral, String> {


    Pageable<Integral> achieveIntegral( Pagination pagination);
}
