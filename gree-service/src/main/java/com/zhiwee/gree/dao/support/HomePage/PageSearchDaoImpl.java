package com.zhiwee.gree.dao.support.HomePage;

import com.github.pagehelper.PageRowBounds;
import com.zhiwee.gree.dao.HomePage.PageSearchDao;
import com.zhiwee.gree.dao.support.HomePage.mapper.PageSearchMapper;
import com.zhiwee.gree.model.HomePage.PageSearch;
import com.zhiwee.gree.model.HomePage.PcPlanmap;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

@Repository
public class PageSearchDaoImpl extends BaseDaoImpl<PageSearch,String, PageSearchMapper> implements PageSearchDao {

    @Override
    public PageSearch getById(String id) {
        return this.mapper.getById(id);
    }

    @Override
    public Pageable<PageSearch> getPage(Map<String, Object> param, Pagination pagination) {
        PageRowBounds rowBounds = new PageRowBounds(pagination.getOffset(), pagination.getPageSize());
        rowBounds.setCount(true);
        List<PageSearch> data = this.mapper.getPage(param, rowBounds);
        return convertToPageable(rowBounds, data);
    }

    @Override
    public List<PageSearch> getList() {
        return this.mapper.getList();
    }
}
