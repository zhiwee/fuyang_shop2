package com.zhiwee.gree.dao.support.HomePage;

import com.github.pagehelper.PageRowBounds;
import com.zhiwee.gree.dao.HomePage.PageChannelDao;
import com.zhiwee.gree.dao.support.HomePage.mapper.PageChannelMapper;
import com.zhiwee.gree.model.HomePage.PageChannel;
import com.zhiwee.gree.model.HomePage.PageNav;
import com.zhiwee.gree.model.HomePage.PageV0.PageChannelVO;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

@Repository
public class PageChannelDaoImpl extends BaseDaoImpl<PageChannel,String, PageChannelMapper> implements PageChannelDao {
    @Override
    public Pageable<PageChannel> getPage(Map<String, Object> param, Pagination pagination) {
        PageRowBounds rowBounds = new PageRowBounds(pagination.getOffset(), pagination.getPageSize());
        rowBounds.setCount(true);
        List<PageChannel> data = this.mapper.getPage(param, rowBounds);
        return convertToPageable(rowBounds, data);
    }

    @Override
    public List<PageChannelVO> getList(Map<String, Object> param) {
        return this.mapper.getList(param);
    }
}
