package com.zhiwee.gree.dao.FyGoods;

import com.zhiwee.gree.model.FyGoods.ShareGoods;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

public interface ShareGoodsDao extends BaseDao<ShareGoods,String> {
    List<ShareGoods> queryGoodsList(Map<String, Object> param, Pagination pagination);

}
