package com.zhiwee.gree.dao.support.OrderInfo;



import com.zhiwee.gree.dao.OrderInfo.OrderDao;
import com.zhiwee.gree.dao.OrderInfo.OrderPaymentDao;
import com.zhiwee.gree.dao.support.OrderInfo.mapper.OrderMapper;
import com.zhiwee.gree.dao.support.OrderInfo.mapper.OrderPaymentMapper;
import com.zhiwee.gree.model.OrderInfo.Order;
import com.zhiwee.gree.model.OrderInfo.OrderPayment;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;


@Repository
public class OrderPaymentDaoImpl extends BaseDaoImpl<OrderPayment, String, OrderPaymentMapper> implements OrderPaymentDao {


}
