package com.zhiwee.gree.dao.support.Coupon.mapper;

import com.zhiwee.gree.model.Coupon.CouponSubject;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

public interface CouponSubjectMapper extends BaseMapper<CouponSubject> {


    List<CouponSubject> pageInfo(Map<String, Object> param);
}
