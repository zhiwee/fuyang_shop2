package com.zhiwee.gree.dao.support.FyGoods.mapper;


import com.zhiwee.gree.model.FyGoods.PictureInfo;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;


public interface PictureInfoMapper extends BaseMapper<PictureInfo> {
    List<PictureInfo> queryGoodsList(Map<String, Object> param);
}