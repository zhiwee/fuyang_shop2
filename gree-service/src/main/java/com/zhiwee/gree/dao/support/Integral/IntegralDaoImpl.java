package com.zhiwee.gree.dao.support.Integral;


import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.Integral.IntegralDao;
import com.zhiwee.gree.dao.support.HomePage.mapper.HomePageMapper;
import com.zhiwee.gree.dao.support.Integral.mapper.IntegralMapper;
import com.zhiwee.gree.model.HomePage.HomePage;
import com.zhiwee.gree.model.Integral.Integral;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.Map;


@Repository
public class IntegralDaoImpl extends BaseDaoImpl<Integral, String, IntegralMapper> implements IntegralDao {


    @Override
    public Pageable<Integral> achieveIntegral( Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return convertToPageable(this.mapper.achieveIntegral());
    }
}
