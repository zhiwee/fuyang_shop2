package com.zhiwee.gree.dao.support.ShopDistributor.mapper;


import com.github.pagehelper.PageRowBounds;
import com.zhiwee.gree.model.ShopDistributor.DistributorArea;
import com.zhiwee.gree.model.ShopDistributor.DistributorCategory;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;


public interface DistributorCategoryMapper extends BaseMapper<DistributorCategory> {


    List<DistributorCategory> achieveSmallCategory(Map<String,Object> param, PageRowBounds rowBounds);
}
