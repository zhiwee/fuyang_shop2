package com.zhiwee.gree.dao.support.Activity;

import com.github.pagehelper.PageRowBounds;
import com.zhiwee.gree.dao.Activity.ActivityInfoDao;
import com.zhiwee.gree.dao.Activity.ActivityPageDao;
import com.zhiwee.gree.dao.support.Activity.mapper.ActivityInfoMapper;
import com.zhiwee.gree.dao.support.Activity.mapper.ActivityPageMapper;
import com.zhiwee.gree.model.Activity.ActivityInfo;
import com.zhiwee.gree.model.Activity.ActivityPage;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

/**
 * @author sun
 * @since 2018/7/6 14:26
 */
@Repository
public class ActivityPageDaoImpl extends BaseDaoImpl<ActivityPage, String, ActivityPageMapper> implements ActivityPageDao {

    @Override
    public Pageable<ActivityPage> pageInfo(Map<String, Object> param, Pagination pagination) {
        PageRowBounds rowBounds = new PageRowBounds(pagination.getOffset(), pagination.getPageSize());
        rowBounds.setCount(true);
        List<ActivityPage> data = this.mapper.pageInfo(param, rowBounds);
        return convertToPageable(rowBounds, data);
    }
}
