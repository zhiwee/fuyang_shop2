package com.zhiwee.gree.dao.support.goods;

import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.goods.GoodsInfoPictureDao;
import com.zhiwee.gree.dao.support.goods.mapper.GoodsInfoPictureMapper;
import com.zhiwee.gree.model.GoodsInfo.GoodsInfoPicture;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

@Repository
public class GoodsInfoPictureImpl  extends BaseDaoImpl<GoodsInfoPicture,String, GoodsInfoPictureMapper> implements  GoodsInfoPictureDao{
    @Override
    public List<GoodsInfoPicture> listAll(Map<String, Object> param) {
        return mapper.listAll(param);
    }

    @Override
    public Pageable<GoodsInfoPicture> pageInfo(Map<String, Object> params, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return     convertToPageable(this.mapper.pageInfo(params));
    }

    @Override
    public GoodsInfoPicture getInfo(String id) {
        return mapper.getInfo(id);
    }
    //extends BaseDaoImpl<GoodsInfo, String, GoodsInfoMapper> implements GoodsInfoDao
}
