package com.zhiwee.gree.dao.support.goods;


import com.zhiwee.gree.dao.goods.RetailGoodsPictureDao;
import com.zhiwee.gree.dao.goods.ShopGoodsPictureDao;
import com.zhiwee.gree.dao.support.goods.mapper.RetailGoodsPictureMapper;
import com.zhiwee.gree.dao.support.goods.mapper.ShopGoodsPictureMapper;
import com.zhiwee.gree.model.GoodsInfo.RetailGoodsPicture;
import com.zhiwee.gree.model.GoodsInfo.ShopGoodsPicture;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;


@Repository
public class RetailGoodsPictureDaoImpl extends BaseDaoImpl<RetailGoodsPicture, String, RetailGoodsPictureMapper> implements RetailGoodsPictureDao {



}
