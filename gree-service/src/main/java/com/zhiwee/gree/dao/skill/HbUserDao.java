package com.zhiwee.gree.dao.skill;

import com.zhiwee.gree.model.skill.HbUser;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

public interface HbUserDao extends BaseDao<HbUser,String> {
    // extends BaseDao<Skillhb,String>
    List<HbUser> queryGetHbList(Map<String,Object> param);
    // Pageable<Skillhb> queryList(Map<String, Object> params, Pagination pagination);queryList
    Pageable<HbUser> queryList(Map<String, Object> params, Pagination pagination);
}
