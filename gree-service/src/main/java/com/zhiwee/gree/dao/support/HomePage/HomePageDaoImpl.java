package com.zhiwee.gree.dao.support.HomePage;


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageRowBounds;
import com.zhiwee.gree.dao.HomePage.HomePageDao;
import com.zhiwee.gree.dao.support.HomePage.mapper.HomePageMapper;
import com.zhiwee.gree.model.GoodsInfo.GoodsInfo;
import com.zhiwee.gree.model.GoodsInfo.SolrGoodsInfo;
import com.zhiwee.gree.model.HomePage.HomePage;
import com.zhiwee.gree.model.HomePage.PcPlanmap;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Page;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;


@Repository
public class HomePageDaoImpl extends BaseDaoImpl<HomePage, String, HomePageMapper> implements HomePageDao {


    @Override
    public Pageable<HomePage> achieveHomePage(Map<String, Object> params, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return convertToPageable(this.mapper.achieveHomePage(params));
    }

}
