package com.zhiwee.gree.dao.order;


import com.zhiwee.gree.model.order.BaseOrder;
import xyz.icrab.common.dao.BaseDao;

public interface BaseOrderDao extends BaseDao<BaseOrder,String> {
    //extends BaseDao<OrderAddress, String>
}
