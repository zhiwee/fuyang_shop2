package com.zhiwee.gree.dao.support.ShopDistributor;

import com.github.pagehelper.PageRowBounds;
import com.zhiwee.gree.dao.ShopDistributor.DistributorAreaDao;
import com.zhiwee.gree.dao.ShopDistributor.ShopDistributorDao;
import com.zhiwee.gree.dao.support.ShopDistributor.mapper.DistributorAreaMapper;
import com.zhiwee.gree.dao.support.ShopDistributor.mapper.ShopDistributorMapper;
import com.zhiwee.gree.model.ShopDistributor.DistributorArea;
import com.zhiwee.gree.model.ShopDistributor.ShopDistributor;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;


@Repository
public class DistributorAreaDaoImpl extends BaseDaoImpl<DistributorArea, String, DistributorAreaMapper> implements DistributorAreaDao {




    @Override
    public Pageable<DistributorArea> pageArea(Map<String, Object> param, Pagination pagination) {
        PageRowBounds rowBounds = new PageRowBounds(pagination.getOffset(), pagination.getPageSize());
        rowBounds.setCount(true);
        List<DistributorArea> data = this.mapper.pageArea(param, rowBounds);
        return convertToPageable(rowBounds, data);
    }

    @Override
    public List<DistributorArea> searchByCategory(Map<String, Object> params) {
        return this.mapper.searchByCategory(params);
    }

}
