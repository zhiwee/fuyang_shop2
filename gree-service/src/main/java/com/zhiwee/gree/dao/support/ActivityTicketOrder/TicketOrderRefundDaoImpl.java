package com.zhiwee.gree.dao.support.ActivityTicketOrder;

import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.ActivityTicketOrder.TicketOrderDao;
import com.zhiwee.gree.dao.ActivityTicketOrder.TicketOrderRefundDao;
import com.zhiwee.gree.dao.support.ActivityTicketOrder.mapper.TicketOrderMapper;
import com.zhiwee.gree.dao.support.ActivityTicketOrder.mapper.TicketOrderRefundMapper;
import com.zhiwee.gree.model.ActivityTicketOrder.TicketOrder;
import com.zhiwee.gree.model.ActivityTicketOrder.TicketOrderExport;
import com.zhiwee.gree.model.ActivityTicketOrder.TicketOrderRefund;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;


/**
 * Description: 
 * @author: sun
 * @Date 下午5:06 2019/6/23
 * @param:
 * @return:
 */
@Repository
public class TicketOrderRefundDaoImpl extends BaseDaoImpl<TicketOrderRefund, String, TicketOrderRefundMapper> implements TicketOrderRefundDao {


    @Override
    public Pageable<TicketOrderRefund> pageInfo(Map<String, Object> param, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum() + 1, pagination.getPageSize());
        List<TicketOrderRefund> baseInfos = mapper.pageInfo(param);
        return convertToPageable(baseInfos);
    }


}
