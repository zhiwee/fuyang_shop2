package com.zhiwee.gree.dao.support.shopBase;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.zhiwee.gree.dao.shopBase.InterfaceDao;
import com.zhiwee.gree.dao.support.shopBase.mapper.InterfaceMapper;
import com.zhiwee.gree.model.Interface;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.github.pagehelper.PageRowBounds;

import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Relation;

/**
 * @author gll
 * @since 2018/4/21 22:35
 */
@Repository
public class InterfaceDaoImpl extends BaseDaoImpl<Interface, String, InterfaceMapper> implements InterfaceDao {

	@Autowired
	InterfaceMapper interfaceMapper;
	@Override
	public int saveUserInterface(List<Relation> list) {
		if(CollectionUtils.isNotEmpty(list)){
			return interfaceMapper.saveUserInterface(list);
		}
		return 0;
	}
	@Override
	public int saveRoleInterface(List<Relation> list) {
		if(CollectionUtils.isNotEmpty(list)){
			return interfaceMapper.saveRoleInterface(list);
		}
		return 0;
	}
	@Override
	public List<Interface> getIntByUserId(String userid) {
		if(StringUtils.isNotEmpty(userid)){
			return interfaceMapper.getIntByUserId(userid);
		}
		return null;
	}
	@Override
	public List<Interface> getIntByRoleIds(String... roleids) {
		if(ArrayUtils.isNotEmpty(roleids)){
			return interfaceMapper.getIntByRoleIds(roleids);
		}
		return null;
	}
	@Override
	public List<Interface> selectList(Map<String, Object> param) {
		if(param == null){
			param = new HashMap<String, Object>();
		}
		return interfaceMapper.getAll(param);
	}
	@Override
	public Pageable<Interface> selectPage(Map<String, Object> param, Pagination pagination) {
        PageRowBounds rowBounds = new PageRowBounds(pagination.getOffset(), pagination.getPageSize());
        rowBounds.setCount(true);
        List<Interface> data = interfaceMapper.getAll(param, rowBounds);
        return convertToPageable(rowBounds, data);
	}
	@Override
	public Interface select(String id) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("id", id);
		List<Interface> list = interfaceMapper.getAll(param );
		if(list.size() == 1){
			return list.get(0);
		}
		return null;
	}
	@Override
	public int delRoleIntByIds(Map<String,Object> map) {
		return interfaceMapper.delRoleIntByIds(map);
	}
	@Override
	public int delUserIntByIds(Map<String,Object> map) {
		return interfaceMapper.delUserIntByIds(map);
	}
}
