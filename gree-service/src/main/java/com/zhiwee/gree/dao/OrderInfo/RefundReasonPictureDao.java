package com.zhiwee.gree.dao.OrderInfo;


import com.zhiwee.gree.model.OrderInfo.OrderRefundReason;
import com.zhiwee.gree.model.OrderInfo.RefundReasonPicture;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.Map;

public interface RefundReasonPictureDao extends BaseDao<RefundReasonPicture, String> {


}
