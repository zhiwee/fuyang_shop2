package com.zhiwee.gree.dao.support.Balance.mapper;


import com.zhiwee.gree.model.Balance.BalanceReCharge;
import com.zhiwee.gree.model.BaseInfo.BaseInfo;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;


public interface BalanceReChargeMapper extends BaseMapper<BalanceReCharge> {

    List<BalanceReCharge> pageInfo(Map<String, Object> param);

    List<BalanceReCharge> balanceRecord(Map<String,Object> params);
}
