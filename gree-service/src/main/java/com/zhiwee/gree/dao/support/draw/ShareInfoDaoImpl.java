package com.zhiwee.gree.dao.support.draw;

import com.zhiwee.gree.dao.draw.CanDrawsDao;
import com.zhiwee.gree.dao.draw.ShareInfoDao;
import com.zhiwee.gree.dao.support.draw.mapper.CanDrawsMapper;
import com.zhiwee.gree.dao.support.draw.mapper.ShareInfoMapper;
import com.zhiwee.gree.model.draw.CanDraws;
import com.zhiwee.gree.model.draw.ShareInfo;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;

@Repository
public class ShareInfoDaoImpl extends BaseDaoImpl<ShareInfo, String, ShareInfoMapper> implements ShareInfoDao {
}
