package com.zhiwee.gree.dao.crad;

import com.zhiwee.gree.model.card.LShopUserCardWxPayInfo;
import com.zhiwee.gree.model.card.LShopUserCardWxPayInfoExrt;
import com.zhiwee.gree.model.card.LShopUserCards;
import com.zhiwee.gree.model.card.LShopUserCardsExrt;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

/**
 * TODO
 *
 * @author Chensong
 * @Company http://www.zhiwee.com
 * date 2020-01-03
 */
public interface LShopUserCardWxPayDao extends BaseDao<LShopUserCardWxPayInfo,String> {

    Pageable<LShopUserCardWxPayInfo> getLShopUserCardWxPayInfoList(Map<String, Object> params, Pagination pagination);

    List<LShopUserCardWxPayInfoExrt> queryLShopUserCardWxPayInfoExport(Map<String, Object> params);

}
