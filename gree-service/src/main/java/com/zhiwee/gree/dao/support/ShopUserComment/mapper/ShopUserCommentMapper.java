package com.zhiwee.gree.dao.support.ShopUserComment.mapper;



import com.zhiwee.gree.model.HomePage.HomePage;
import com.zhiwee.gree.model.ShopUserComment.Comment;
import com.zhiwee.gree.model.ShopUserComment.ShopUserComment;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;


public interface ShopUserCommentMapper extends BaseMapper<ShopUserComment> {


    List<ShopUserComment> achieveComment(Map<String, Object> params);

    List<ShopUserComment> achieveCommentByGoodsId(List<String> list);

    Integer todayAmount(Map<String,Object> params);

    List<ShopUserComment> achieveMyComment(Map<String,Object> param);
}
