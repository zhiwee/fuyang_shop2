package com.zhiwee.gree.dao.goods;

import com.zhiwee.gree.model.GoodsInfo.GoodsBand;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.Map;

public interface GoodsBandDao extends BaseDao<GoodsBand,String> {
    Pageable<GoodsBand> getPage(Map<String, Object> param, Pagination pagination);
}
