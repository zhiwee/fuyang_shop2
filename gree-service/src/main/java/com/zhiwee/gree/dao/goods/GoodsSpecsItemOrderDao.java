package com.zhiwee.gree.dao.goods;

import com.zhiwee.gree.model.GoodsInfo.GoodsSpecsItemOrder;
import xyz.icrab.common.dao.BaseDao;

import java.util.List;
import java.util.Map;

public interface GoodsSpecsItemOrderDao  extends BaseDao<GoodsSpecsItemOrder,String> {
    List<GoodsSpecsItemOrder> getListBYGoodsId( Map<String, Object> param);
}
