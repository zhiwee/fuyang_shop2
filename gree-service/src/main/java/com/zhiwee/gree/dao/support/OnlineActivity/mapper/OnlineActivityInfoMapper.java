package com.zhiwee.gree.dao.support.OnlineActivity.mapper;


import com.zhiwee.gree.model.GoodsInfo.LAPeople;
import com.zhiwee.gree.model.OnlineActivity.OnlineActivityInfo;
import com.zhiwee.gree.model.OnlineActivity.OnlineActivityInfoExe;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;


/**
 * @author SUN
 * @since 2018/7/6 14:26
 */
public interface OnlineActivityInfoMapper extends BaseMapper<OnlineActivityInfo> {

    List<OnlineActivityInfoExe> exportActivityInfo(Map<String, Object> params);
}
