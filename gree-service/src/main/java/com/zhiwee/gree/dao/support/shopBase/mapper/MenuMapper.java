package com.zhiwee.gree.dao.support.shopBase.mapper;

import com.zhiwee.gree.model.Menu;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

public interface MenuMapper extends BaseMapper<Menu> {
    List<Menu> getMenuByUser(Map<String, Object> param);

    List<Menu> getMenuByRoleId(Map<String, Object> param);

    void addMenuRoleRef(Map<String, Object> param);

    void addMenuUserRef(Map<String, Object> param);

    void delMenuByRoleId(Map<String, Object> param);

    void delMenuByUserId(Map<String, Object> param);

    List<Menu> getMenuByUserId(Map<String, Object> param);
}
