package com.zhiwee.gree.dao.support.skill;

import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.skill.SkillhbDao;
import com.zhiwee.gree.dao.support.skill.mapper.SkillhbMapper;
import com.zhiwee.gree.model.skill.Skillhb;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

@Repository
public class SkillhbDaoImpl   extends BaseDaoImpl<Skillhb, String, SkillhbMapper> implements SkillhbDao {
    @Override
    public Pageable<Skillhb>  queryList(Map<String, Object> params, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return convertToPageable(this.mapper.queryList(params));
    }

    @Override
    public List<Skillhb> queryHbByTime(Map<String, Object> params) {
        return  this.mapper.queryHbByTime(params);
    }
//extends BaseDaoImpl<CommentPicture, String,CommentPictureMapper> implements CommentPictureDao
}
