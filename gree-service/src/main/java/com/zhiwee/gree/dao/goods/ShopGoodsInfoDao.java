package com.zhiwee.gree.dao.goods;


import com.zhiwee.gree.model.GoodsInfo.GoodsInfo;
import com.zhiwee.gree.model.GoodsInfo.ShopGoodsInfo;
import com.zhiwee.gree.model.GoodsInfo.SolrGoodsInfo;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

public interface ShopGoodsInfoDao extends BaseDao<ShopGoodsInfo, String> {

    List<ShopGoodsInfo> achieveGoodsByName(Map<String,Object> param);

    List<SolrGoodsInfo> getAllGoods(Map<String,Object> param);

    Pageable<ShopGoodsInfo> searchAll(Map<String,Object> params, Pagination pagination);

    ShopGoodsInfo achieveGoodsById(Map<String,Object> params);

    List<ShopGoodsInfo> achieveSpecialGoods(Map<String,Object> params);

    Pageable<ShopGoodsInfo> achieveSmallGoods(Map<String,Object> param, Pagination pagination);

    List<ShopGoodsInfo> achieveRecommend(Map<String,Object> param);

    Pageable<ShopGoodsInfo> searchEngineerGood(Map<String,Object> params, Pagination pagination);

    ShopGoodsInfo searchOrderGoods(String goodId);

    List<ShopGoodsInfo> achieveHotSellGood(Map<String,Object> param);
}
