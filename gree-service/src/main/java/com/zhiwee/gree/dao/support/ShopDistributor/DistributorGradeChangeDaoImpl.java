package com.zhiwee.gree.dao.support.ShopDistributor;

import com.github.pagehelper.PageRowBounds;
import com.zhiwee.gree.dao.ShopDistributor.DistributorGradeChangeDao;
import com.zhiwee.gree.dao.ShopDistributor.DistributorGradeDao;
import com.zhiwee.gree.dao.support.ShopDistributor.mapper.DistributorGradeChangeMapper;
import com.zhiwee.gree.dao.support.ShopDistributor.mapper.DistributorGradeMapper;
import com.zhiwee.gree.model.ShopDistributor.DistributorGrade;
import com.zhiwee.gree.model.ShopDistributor.DistributorGradeChange;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;


@Repository
public class DistributorGradeChangeDaoImpl extends BaseDaoImpl<DistributorGradeChange, String, DistributorGradeChangeMapper> implements DistributorGradeChangeDao {



    @Override
    public Pageable<DistributorGradeChange> infoPage(Map<String, Object> param, Pagination pagination) {
        PageRowBounds rowBounds = new PageRowBounds(pagination.getOffset(), pagination.getPageSize());
        rowBounds.setCount(true);
        List<DistributorGradeChange> data = this.mapper.infoPage(param, rowBounds);
        return convertToPageable(rowBounds, data);
    }




}
