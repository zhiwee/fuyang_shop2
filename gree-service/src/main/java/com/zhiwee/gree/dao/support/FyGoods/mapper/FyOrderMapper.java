package com.zhiwee.gree.dao.support.FyGoods.mapper;

import com.zhiwee.gree.model.FyGoods.FyOrder;
import com.zhiwee.gree.model.FyGoods.vo.FyOrderVo;
import com.zhiwee.gree.model.OrderInfo.GoodsNameTopTen;
import com.zhiwee.gree.model.OrderInfo.vo.OrderStatisticItem;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

public interface FyOrderMapper  extends BaseMapper<FyOrder> {

    List<FyOrder>  queryOrderList(Map<String,Object> param);
    int  updateByOrderNum(Map<String,Object> param);
    int deleteByOrderNum(Map<String,Object> param);
    int updateSelective(Map<String,Object> param);
    List<FyOrder>  refoundOrderList(Map<String,Object> param);
    List<FyOrder> queryList(Map<String,Object> param);

    List<FyOrder>  queryPayOrderList(Map<String,Object> param);

    List<FyOrderVo> queryExportOrderList(Map<String, Object> params);

    List<OrderStatisticItem> statisticAllAmountByDate(Map<String, Object> param);

    List<GoodsNameTopTen>  qidoGoodsNameNumTopTen(Map<String, Object> param);

    List<GoodsNameTopTen>  qidoGoodsNameMoneyTopTen(Map<String, Object> param);

    Object orderAmount(Map<String, Object> params);

    Double totalPrice(Map<String, Object> param);

    List<FyOrder> queryPayOrderList2(Map<String, Object> map);
}