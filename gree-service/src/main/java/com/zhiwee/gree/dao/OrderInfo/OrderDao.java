package com.zhiwee.gree.dao.OrderInfo;


import com.zhiwee.gree.model.OrderInfo.Order;
import com.zhiwee.gree.model.OrderInfo.OrderExport;
import com.zhiwee.gree.model.OrderInfo.vo.OrderStatisticItem;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

public interface OrderDao extends BaseDao<Order, String> {


    Pageable<Order> pageInfo(Map<String,Object> params, Pagination pagination);

    List<OrderExport> queryOrderExport(Map<String,Object> params);

    Pageable<Order> achieveMyOrder(Map<String,Object> params, Pagination pagination);

    Integer orderAmount(Map<String,Object> params);

    List<OrderStatisticItem> statisticAllAmountByDate(Map<String,Object> param);

    Pageable<Order> achieveMyEngineerOrder(Map<String,Object> params, Pagination pagination);

    Double totalPrice(Map<String,Object> param);
}
