package com.zhiwee.gree.dao.tgoods;

import com.zhiwee.gree.model.FyGoods.GoodsLabel;
import xyz.icrab.common.dao.BaseDao;

/**
 * @author DELL
 */
public interface GoodsLabelDao extends BaseDao<GoodsLabel,String> {

}