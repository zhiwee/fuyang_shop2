package com.zhiwee.gree.dao.support.tgoods.mapper;

import com.zhiwee.gree.model.FyGoods.GoodsLabel;
import com.zhiwee.gree.model.draw.CanDraws;
import xyz.icrab.common.dao.support.mapper.BaseMapper;


/**
 * @author DELL
 */
public interface GoodsLabelMapper extends BaseMapper<GoodsLabel> {
}
