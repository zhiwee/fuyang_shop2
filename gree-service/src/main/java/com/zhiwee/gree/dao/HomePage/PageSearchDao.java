package com.zhiwee.gree.dao.HomePage;

import com.zhiwee.gree.model.HomePage.PageSearch;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

public interface PageSearchDao extends BaseDao<PageSearch,String> {
    PageSearch getById(String id);

    Pageable<PageSearch> getPage(Map<String, Object> param, Pagination pagination);

    List<PageSearch> getList();
}
