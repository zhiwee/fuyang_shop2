package com.zhiwee.gree.dao.OrderInfo;


import com.zhiwee.gree.model.OrderInfo.Order;
import com.zhiwee.gree.model.OrderInfo.OrderChange;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import java.util.Map;

public interface OrderChangeDao extends BaseDao<OrderChange, String> {


    Pageable<OrderChange> pageInfo(Map<String, Object> params, Pagination pagination);

}
