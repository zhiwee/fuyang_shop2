package com.zhiwee.gree.dao.support.HomePage.mapper;



import com.github.pagehelper.PageRowBounds;
import com.zhiwee.gree.model.HomePage.HomePage;
import com.zhiwee.gree.model.HomePage.PcPlanmap;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;


public interface HomePageMapper extends BaseMapper<HomePage> {


    List<HomePage> achieveHomePage(Map<String,Object> params);
}
