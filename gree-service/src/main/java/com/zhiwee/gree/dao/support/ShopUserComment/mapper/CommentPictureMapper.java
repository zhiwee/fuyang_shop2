package com.zhiwee.gree.dao.support.ShopUserComment.mapper;



import com.zhiwee.gree.model.ShopUserComment.CommentPicture;
import com.zhiwee.gree.model.ShopUserComment.ShopUserComment;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;


public interface CommentPictureMapper extends BaseMapper<CommentPicture> {


}
