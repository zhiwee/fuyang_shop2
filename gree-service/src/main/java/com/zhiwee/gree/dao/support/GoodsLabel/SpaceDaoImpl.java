package com.zhiwee.gree.dao.support.GoodsLabel;


import com.zhiwee.gree.dao.GoodsLabel.PsDao;
import com.zhiwee.gree.dao.GoodsLabel.SpaceDao;
import com.zhiwee.gree.dao.support.GoodsLabel.mapper.PsMapper;
import com.zhiwee.gree.dao.support.GoodsLabel.mapper.SpaceMapper;
import com.zhiwee.gree.model.GoodsLabel.Ps;
import com.zhiwee.gree.model.GoodsLabel.Space;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;


@Repository
public class SpaceDaoImpl extends BaseDaoImpl<Space, String, SpaceMapper> implements SpaceDao {



}
