package com.zhiwee.gree.dao.support.shopBase;

import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.shopBase.UserDao;
import com.zhiwee.gree.dao.support.shopBase.mapper.UserMapper;
import com.zhiwee.gree.model.User;
import com.zhiwee.gree.model.UserExport;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.util.Sqls;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.enums.YesOrNo;
import xyz.icrab.common.web.param.IdsParam;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @author sun
 * @since 2018/3/6 22:35
 */
@Repository
public class UserDaoImpl extends BaseDaoImpl<User, String, UserMapper> implements UserDao {

    @Override
    public int delete(String... ids) {
        if(ArrayUtils.isNotEmpty(ids)){
           return delete(Arrays.asList(ids));
        }
        return 0;
    }

    @Override
    public int delete(List<String> ids) {
        if(CollectionUtils.isNotEmpty(ids)){
            Example.Builder builder = Example.builder(User.class);
            builder.where(Sqls.custom().andIn("id", ids));
            User user = new User();
            user.setState(YesOrNo.NO);
            return getMapper().updateByExampleSelective(user, builder.build());
        }
        return 0;
    }

    @Override
    public int delete(User entity) {
        if(entity != null){
            User user = new User();
            user.setState(YesOrNo.NO);
            return getMapper().updateByExampleSelective(user, notNullOf(entity));
        }
        return 0;
    }



    @Override
    public Pageable<User> page(Map<String, Object> param, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum() + 1, pagination.getPageSize());
        List<User> users = mapper.selectByParam(param);
        return convertToPageable(users);
//        PageRowBounds rowBounds = new PageRowBounds(pagination.getOffset(), pagination.getPageSize());
//        rowBounds.setCount(true);
//        List<DealerInfo> data = this.mapper.infoPage(param, rowBounds);
//        return convertToPageable(rowBounds, data);
    }


    @Override
    public List<UserExport> exportUser(Map<String, Object> param) {
        return this.mapper.exportUser(param);
    }


    @Override
    public User getMyRoleName(Map<String,Object> param) {
        return this.mapper.getMyRoleName(param);
    }


    @Override
    public void deleteUser(IdsParam param) {
        mapper.deleteUser(param);
    }


    @Override
    public void deleteUserRole(IdsParam param) {
        mapper.deleteUserRole(param);
    }

    @Override
    public User searchOne(Map<String, Object> param) {
        return mapper.searchOne(param);
    }


}
