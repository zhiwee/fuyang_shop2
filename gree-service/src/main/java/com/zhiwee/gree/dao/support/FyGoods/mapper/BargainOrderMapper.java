package com.zhiwee.gree.dao.support.FyGoods.mapper;


import com.zhiwee.gree.model.FyGoods.BargainOrder;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;


public interface BargainOrderMapper extends BaseMapper<BargainOrder> {
}