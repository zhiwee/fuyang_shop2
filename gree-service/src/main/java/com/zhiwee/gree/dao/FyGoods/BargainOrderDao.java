package com.zhiwee.gree.dao.FyGoods;

import com.zhiwee.gree.model.FyGoods.BargainOrder;
import xyz.icrab.common.dao.BaseDao;

public interface BargainOrderDao extends BaseDao<BargainOrder,String> {

}
