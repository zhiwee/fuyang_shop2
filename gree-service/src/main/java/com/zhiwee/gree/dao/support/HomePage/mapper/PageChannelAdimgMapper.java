package com.zhiwee.gree.dao.support.HomePage.mapper;

import com.github.pagehelper.PageRowBounds;
import com.zhiwee.gree.model.HomePage.PageChannelAdimg;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

public interface PageChannelAdimgMapper extends BaseMapper<PageChannelAdimg> {
    List<PageChannelAdimg> getPage(Map<String, Object> param, PageRowBounds rowBounds);
}
