package com.zhiwee.gree.dao.support.order.mapper;

import com.zhiwee.gree.model.order.OrderGoods;
import xyz.icrab.common.dao.support.mapper.BaseMapper;


public  interface OrderGoodsMapper  extends BaseMapper<OrderGoods> {
    //extends BaseMapper<BaseOrder>
}
