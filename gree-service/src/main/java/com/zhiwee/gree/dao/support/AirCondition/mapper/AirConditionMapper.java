package com.zhiwee.gree.dao.support.AirCondition.mapper;



import com.zhiwee.gree.model.AchieveGift.GiftInfo;
import com.zhiwee.gree.model.AirCondition.AirCondition;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;


public interface AirConditionMapper extends BaseMapper<AirCondition> {

    List<AirCondition> pageInfo(Map<String, Object> params);
}
