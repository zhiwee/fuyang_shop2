package com.zhiwee.gree.dao.support.goods;


import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.goods.GoodsInfoDao;
import com.zhiwee.gree.dao.support.goods.mapper.GoodsInfoMapper;

import com.zhiwee.gree.model.GoodsInfo.GoodsInfo;
import com.zhiwee.gree.model.GoodsInfo.LAGoodsInfo;
import com.zhiwee.gree.model.GoodsInfo.LAPeople;
import com.zhiwee.gree.model.GoodsInfo.SolrGoodsInfo;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;


@Repository
public class GoodsInfoDaoImpl extends BaseDaoImpl<GoodsInfo, String, GoodsInfoMapper> implements GoodsInfoDao {



    @Override
    public Pageable<GoodsInfo> searchAll(Map<String, Object> params, Pagination pagination) {

        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return convertToPageable(this.mapper.searchAll(params));
    }

       @Override
    public void updateActivityGoodsInfo(List<LAGoodsInfo> list) {
        this.mapper.updateActivityGoodsInfo(list);
    }

    @Override
    public void updateActivityGoodsInfo2(List<LAGoodsInfo> list) {
        this.mapper.updateActivityGoodsInfo2(list);
    }


    @Override
    public void updateActivityPeople(List<LAPeople> list) {
        this.mapper.updateActivityPeople(list);

    }
}
