package com.zhiwee.gree.dao.support.ShopUserComment;


import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.HomePage.HomePageDao;
import com.zhiwee.gree.dao.ShopUserComment.ShopUserCommentDao;
import com.zhiwee.gree.dao.support.HomePage.mapper.HomePageMapper;
import com.zhiwee.gree.dao.support.ShopUserComment.mapper.ShopUserCommentMapper;
import com.zhiwee.gree.model.HomePage.HomePage;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.ShopUserComment.Comment;
import com.zhiwee.gree.model.ShopUserComment.ShopUserComment;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.web.session.Session;

import java.util.Iterator;
import java.util.List;
import java.util.Map;


@Repository
public class ShopUserCommentDaoImpl extends BaseDaoImpl<ShopUserComment, String,ShopUserCommentMapper> implements ShopUserCommentDao {


    @Override
    public Pageable<ShopUserComment> achieveComment(Map<String, Object> params, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return convertToPageable(this.mapper.achieveComment(params));
    }

    @Override
    public Pageable<ShopUserComment> achieveCommentByGoodsId(List<String> ids, Pagination pagination, Session session) {
        PageHelper.startPage(pagination.getPageNum() + 1, pagination.getPageSize(), true);
        List<ShopUserComment> shopUserComments = this.mapper.achieveCommentByGoodsId(ids);
        Iterator<ShopUserComment> iterator = shopUserComments.iterator();
        ShopUser user  = null;
        if (session != null) {
            user = (ShopUser) session.getAttribute("ShopUser");
        }
        while (iterator.hasNext()) {
            ShopUserComment shopUserComment = iterator.next();
            if (user == null) {
                //差评
                if (shopUserComment.getState() == 2) {
                    iterator.remove();
                }
            } else {
                //用户可以看到自己的差评
                if (shopUserComment.getState() == 2 && !shopUserComment.getUserId().equals(user.getId())){
                    iterator.remove();
                }
            }
        }
        return convertToPageable(shopUserComments);
    }

    @Override
    public Integer todayAmount(Map<String, Object> params) {
        return this.mapper.todayAmount(params);
    }

    @Override
    public Pageable<ShopUserComment> achieveMyComment(Map<String, Object> param, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum() + 1, pagination.getPageSize(), true);
        List<ShopUserComment> shopUserComments = this.mapper.achieveMyComment(param);
        return convertToPageable(shopUserComments);
    }
}
