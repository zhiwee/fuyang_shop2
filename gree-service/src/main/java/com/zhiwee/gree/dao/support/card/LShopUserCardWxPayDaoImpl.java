package com.zhiwee.gree.dao.support.card;

import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.crad.LShopUserCardWxPayDao;
import com.zhiwee.gree.dao.crad.LShopUserCardsDao;
import com.zhiwee.gree.dao.support.card.mapper.LShopUserCardWxPayMapper;
import com.zhiwee.gree.dao.support.card.mapper.LShopUserCardsMapper;
import com.zhiwee.gree.model.card.LShopUserCardWxPayInfo;
import com.zhiwee.gree.model.card.LShopUserCardWxPayInfoExrt;
import com.zhiwee.gree.model.card.LShopUserCards;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

/**
 * TODO
 *
 * @author Chensong
 * @Company http://www.zhiwee.com
 * date 2020-01-03
 */
@Repository
public class LShopUserCardWxPayDaoImpl extends BaseDaoImpl<LShopUserCardWxPayInfo, String, LShopUserCardWxPayMapper> implements LShopUserCardWxPayDao {
    @Override
    public Pageable<LShopUserCardWxPayInfo> getLShopUserCardWxPayInfoList(Map<String, Object> params, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return convertToPageable(this.mapper.getLShopUserCardWxPayInfoList(params));
    }

    @Override
    public List<LShopUserCardWxPayInfoExrt> queryLShopUserCardWxPayInfoExport(Map<String, Object> params) {
        return mapper.queryLShopUserCardWxPayInfoExport(params);
    }
}
