package com.zhiwee.gree.dao.support.card;

import com.zhiwee.gree.dao.crad.BookingCardUserDao;
import com.zhiwee.gree.dao.support.card.mapper.BookingCardUserMapper;
import com.zhiwee.gree.model.card.BookingCardUser;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;

@Repository
public class BookingCardUserDaoImpl extends BaseDaoImpl<BookingCardUser, String, BookingCardUserMapper> implements BookingCardUserDao {


}
