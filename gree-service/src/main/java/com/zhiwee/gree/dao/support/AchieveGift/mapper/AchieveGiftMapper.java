package com.zhiwee.gree.dao.support.AchieveGift.mapper;



import com.zhiwee.gree.model.AchieveGift.AchieveGift;
import com.zhiwee.gree.model.AchieveGift.vo.GiftCount;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;


public interface AchieveGiftMapper extends BaseMapper<AchieveGift> {

    List<AchieveGift> searchGiftInfo(Map<String,Object> params);

    List<AchieveGift> searchGift(Map<String,Object> param);

    List<GiftCount> giftCount(Map<String,Object> param);
}
