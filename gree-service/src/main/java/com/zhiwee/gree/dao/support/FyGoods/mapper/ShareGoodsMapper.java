package com.zhiwee.gree.dao.support.FyGoods.mapper;


import com.zhiwee.gree.model.FyGoods.ShareGoods;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;


public interface ShareGoodsMapper extends BaseMapper<ShareGoods> {
    List<ShareGoods> queryGoodsList(Map<String, Object> param);
}