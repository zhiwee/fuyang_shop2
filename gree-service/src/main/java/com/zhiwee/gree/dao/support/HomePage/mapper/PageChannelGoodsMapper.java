package com.zhiwee.gree.dao.support.HomePage.mapper;

import com.github.pagehelper.PageRowBounds;
import com.zhiwee.gree.model.HomePage.PageChannelGoods;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

public interface PageChannelGoodsMapper extends BaseMapper<PageChannelGoods> {
    List<PageChannelGoods> getPage(Map<String, Object> param, PageRowBounds rowBounds);
}
