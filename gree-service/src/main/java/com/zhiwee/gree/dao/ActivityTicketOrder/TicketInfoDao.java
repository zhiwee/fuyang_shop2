package com.zhiwee.gree.dao.ActivityTicketOrder;

import com.zhiwee.gree.model.ActivityTicketOrder.ActivityTicketOrder;
import com.zhiwee.gree.model.ActivityTicketOrder.TicketInfo;
import xyz.icrab.common.dao.BaseDao;

/**
 * @author sun
 */
public interface TicketInfoDao extends BaseDao<TicketInfo, String> {

}
