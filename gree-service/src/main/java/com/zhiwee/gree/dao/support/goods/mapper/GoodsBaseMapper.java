package com.zhiwee.gree.dao.support.goods.mapper;



import com.zhiwee.gree.model.GoodsInfo.GoodVo.GoodsExo;
import com.zhiwee.gree.model.GoodsInfo.GoodVo.GoodsInfoVO;
import com.zhiwee.gree.model.GoodsInfo.Goods;
import com.zhiwee.gree.model.GoodsInfo.GoodsBase;
import com.zhiwee.gree.model.GoodsInfo.ProductAttribute;
import com.zhiwee.gree.model.OnlineActivity.vo.OnlineActivityInfoVo;
import org.apache.ibatis.annotations.Param;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * Created by jick on 2019/8/30.
 */
public interface GoodsBaseMapper extends BaseMapper<GoodsBase> {

    List<GoodsBase> queryInfoList(Map<String, Object> params);

    GoodsBase queryInfoListById(Map<String, Object> params);

    List<GoodsBase> listAll(Map<String, Object> params);

    List<String> getList(@Param("id") String id);
}
