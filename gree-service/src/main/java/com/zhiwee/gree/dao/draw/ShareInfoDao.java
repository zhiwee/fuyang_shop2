package com.zhiwee.gree.dao.draw;


import com.zhiwee.gree.model.draw.ShareInfo;
import xyz.icrab.common.dao.BaseDao;


public interface ShareInfoDao extends BaseDao<ShareInfo,String> {
}
