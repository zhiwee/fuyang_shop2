package com.zhiwee.gree.dao.ShopDistributor;


import com.zhiwee.gree.model.ShopDistributor.DistributorArea;
import com.zhiwee.gree.model.ShopDistributor.DistributorOrder;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.Map;

public interface DistributorOrderDao extends BaseDao<DistributorOrder,String> {


    Pageable<DistributorOrder> infoPage(Map<String,Object> param, Pagination pagination);
}
