package com.zhiwee.gree.dao.support.ShopDistributor;

import com.github.pagehelper.PageRowBounds;
import com.zhiwee.gree.dao.ShopDistributor.ShopDistributorDao;
import com.zhiwee.gree.dao.support.ShopDistributor.mapper.ShopDistributorMapper;

import com.zhiwee.gree.model.ShopDistributor.ShopDistributor;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;


@Repository
public class ShopDistributorDaoImpl extends BaseDaoImpl<ShopDistributor, String, ShopDistributorMapper> implements ShopDistributorDao {



    @Override
    public Pageable<ShopDistributor> infoPage(Map<String, Object> param, Pagination pagination) {
        PageRowBounds rowBounds = new PageRowBounds(pagination.getOffset(), pagination.getPageSize());
        rowBounds.setCount(true);
        List<ShopDistributor> data = this.mapper.newPage(param, rowBounds);
        return convertToPageable(rowBounds, data);
    }

    @Override
    public ShopDistributor defaultDistributor(Map<String, Object> mapkey) {
        return this.mapper.defaultDistributor(mapkey);
    }


}
