package com.zhiwee.gree.dao.draw;

import com.zhiwee.gree.model.draw.DrawsInfo;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

public interface DrawsInfoDao extends BaseDao<DrawsInfo,String> {

    List<DrawsInfo> checkAllNum(Map<String, Object> param);

    Pageable<DrawsInfo> pageInfo(Map<String, Object> params, Pagination pagination);

    List<DrawsInfo> listAll(Map<String, Object> param);
}
