package com.zhiwee.gree.dao.support.Shop.mapper;

import com.zhiwee.gree.model.Shop.ShopArea;
import xyz.icrab.common.dao.support.mapper.BaseMapper;
;

public interface ShopAreaMapper extends BaseMapper<ShopArea> {
}
