package com.zhiwee.gree.dao.goods;

import com.zhiwee.gree.model.GoodsInfo.Categorys;
import com.zhiwee.gree.model.GoodsInfo.GoodVo.CategorysExo;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;


/**
 * @author DELL
 */
public interface CategorysDao extends BaseDao<Categorys,String> {

    Pageable<Categorys> getPage(Map<String, Object> param, Pagination pagination);

    List<CategorysExo> listExo();
}
