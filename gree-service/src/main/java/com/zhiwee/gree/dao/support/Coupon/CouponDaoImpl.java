package com.zhiwee.gree.dao.support.Coupon;


import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.Coupon.CouponDao;
import com.zhiwee.gree.dao.support.Coupon.mapper.CouponMapper;
import com.zhiwee.gree.model.Coupon.Coupon;
import com.zhiwee.gree.model.Coupon.CouponExport;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;


@Repository
public class CouponDaoImpl extends BaseDaoImpl<Coupon, String, CouponMapper> implements CouponDao {



    @Override
    public Pageable<Coupon> pageInfo(Map<String, Object> param, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum() + 1, pagination.getPageSize());
        List<Coupon> users = mapper.pageInfo(param);
        return convertToPageable(users);
    }

    @Override
    public List<CouponExport> exportCoupon(Map<String, Object> exportCoupon) {
        return mapper.exportCoupon(exportCoupon);
    }

    @Override
    public Coupon getCouponById(String id) {
        return mapper.getCouponById( id );
    }

    @Override
    public void deleteList(Map<String, Object> delId) {
        mapper.deleteList( delId );
    }

    @Override
    public void updateAll(Map<String, Object> param) {
        mapper.updateAll( param );
    }

    @Override
    public List<Coupon> listAll(Map<String, Object> params) {
        return mapper.listAll(params);
    }
}
