package com.zhiwee.gree.dao.Activity;

import com.zhiwee.gree.model.Activity.BindInfo;
import xyz.icrab.common.dao.BaseDao;


public interface BindInfoDao extends BaseDao<BindInfo,String> {
}
