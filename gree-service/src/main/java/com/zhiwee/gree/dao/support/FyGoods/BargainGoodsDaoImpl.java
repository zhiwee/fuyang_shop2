package com.zhiwee.gree.dao.support.FyGoods;

import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.FyGoods.BargainGoodsDao;
import com.zhiwee.gree.dao.FyGoods.ShareGoodsDao;
import com.zhiwee.gree.dao.support.FyGoods.mapper.BargainGoodsMapper;
import com.zhiwee.gree.dao.support.FyGoods.mapper.ShareGoodsMapper;
import com.zhiwee.gree.model.FyGoods.BargainGoods;
import com.zhiwee.gree.model.FyGoods.ShareGoods;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;


@Repository
public class BargainGoodsDaoImpl
        extends BaseDaoImpl<BargainGoods, String, BargainGoodsMapper> implements BargainGoodsDao {
    @Override
    public List<BargainGoods> queryGoodsList(Map<String, Object> param, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return (this.mapper.queryGoodsList(param));
    }

}
