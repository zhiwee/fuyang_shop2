package com.zhiwee.gree.dao.shopBase;

import java.util.List;
import java.util.Map;

import com.zhiwee.gree.model.Role;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Relation;

/**
 * @author gll
 * @since 2018/4/21 22:31
 */
public interface RoleDao extends BaseDao<Role, String> {
	/*
	 * 检查分组有没有重名
	 */
    int checkName(String group);
    /*
	 * 停用启用
	 */
    int updateState(Map<String, Object> map);
    /*
	 * 用户添加角色数据
	 */
	int saveUserRole(List<Relation> list);
	/*
	 * 根据userid获取用户角色
	 */
	List<Role> getRoleByUserId(String userid);
	

	/*
	 * 根据模块userid删除用户接口关系数据
	 */
	int delUserRoleByIds(Map<String, Object> map);
}
