package com.zhiwee.gree.dao.FyGoods;

import com.zhiwee.gree.model.FyGoods.PictureInfo;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.Map;

public interface PictureInfoDao extends BaseDao<PictureInfo,String> {
    Pageable<PictureInfo> queryGoodsList(Map<String, Object> param, Pagination pagination);

}
