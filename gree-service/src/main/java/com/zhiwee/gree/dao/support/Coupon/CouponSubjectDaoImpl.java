package com.zhiwee.gree.dao.support.Coupon;


import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.Coupon.CouponSubjectDao;
import com.zhiwee.gree.dao.support.Coupon.mapper.CouponSubjectMapper;
import com.zhiwee.gree.model.Coupon.CouponSubject;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Page;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

@Repository
public class CouponSubjectDaoImpl extends BaseDaoImpl<CouponSubject, String, CouponSubjectMapper> implements CouponSubjectDao {


    @Override
    public Pageable<CouponSubject> pageInfo(Map<String, Object> param, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum() + 1, pagination.getPageSize());
        List<CouponSubject> users = mapper.pageInfo(param);
        return convertToPageable(users);
    }
}
