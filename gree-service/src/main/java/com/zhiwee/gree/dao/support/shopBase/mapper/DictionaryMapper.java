package com.zhiwee.gree.dao.support.shopBase.mapper;

import java.util.Map;

import com.zhiwee.gree.model.Dictionary;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

/**
 * @author gll
 * @since 2018/4/21 22:35
 */
public interface DictionaryMapper extends BaseMapper<Dictionary> {
	/*
	 * 检查分组有没有重名
	 */
    int checkGroup(String group);
    /*
	 * 检查同group中key有没有重名
	 */
    int checkKey(Map<String, String> map);
    /*
	 * 停用启用
	 */
    int updateState(Map<String, Object> map);
}
