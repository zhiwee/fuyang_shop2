package com.zhiwee.gree.dao.support.goods.mapper;

import com.github.pagehelper.PageRowBounds;
import com.zhiwee.gree.model.GoodsInfo.GoodVo.GategoryVo;
import com.zhiwee.gree.model.GoodsInfo.GoodVo.GoodsCategoryCY;
import com.zhiwee.gree.model.GoodsInfo.GoodsCategory;
import com.zhiwee.gree.model.GoodsInfo.GoodsCategoryAdImg;
import com.zhiwee.gree.model.HomePage.PageV0.PageChannelGoodsVO;
import com.zhiwee.gree.model.ShopDistributor.CategoryVo;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

public interface GoodsCategoryMapper extends BaseMapper<GoodsCategory> {
    List<GoodsCategory> getPage(Map<String, Object> param, PageRowBounds rowBounds);


    GoodsCategory selectByid(String id);

    List<GategoryVo> getVoList(Map<String, Object> params);

    List<GategoryVo> getVoListCopy(Map<String, Object> params);

    List<PageChannelGoodsVO> getCategoryGoods(Map<String, Object> params);

    List<GoodsCategoryCY> listInfo();

    List<GoodsCategory>listChildren(Map<String, Object> params);
}
