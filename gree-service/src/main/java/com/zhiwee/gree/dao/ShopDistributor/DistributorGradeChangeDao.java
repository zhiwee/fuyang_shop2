package com.zhiwee.gree.dao.ShopDistributor;


import com.zhiwee.gree.model.ShopDistributor.DistributorGrade;
import com.zhiwee.gree.model.ShopDistributor.DistributorGradeChange;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.Map;

public interface DistributorGradeChangeDao extends BaseDao<DistributorGradeChange,String> {


    Pageable<DistributorGradeChange> infoPage(Map<String, Object> param, Pagination pagination);



}
