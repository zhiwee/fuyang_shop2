package com.zhiwee.gree.dao.support.goods.mapper;

import com.github.pagehelper.PageRowBounds;
import com.zhiwee.gree.model.GoodsInfo.GoodsSpecsItem;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;


public interface GoodsSpecsItemMapper extends BaseMapper<GoodsSpecsItem> {
    List<GoodsSpecsItem> getPage(Map<String, Object> param, PageRowBounds rowBounds);
}
