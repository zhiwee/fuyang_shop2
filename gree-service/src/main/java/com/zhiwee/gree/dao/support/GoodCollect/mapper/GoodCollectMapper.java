package com.zhiwee.gree.dao.support.GoodCollect.mapper;


import com.github.pagehelper.PageRowBounds;
import com.zhiwee.gree.model.Dealer.DealerInfoBase;
import com.zhiwee.gree.model.GoodCollect.GoodCollect;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;


public interface GoodCollectMapper extends BaseMapper<GoodCollect> {


    List<GoodCollect> infoPage(Map<String, Object> param, PageRowBounds rowBounds);
}
