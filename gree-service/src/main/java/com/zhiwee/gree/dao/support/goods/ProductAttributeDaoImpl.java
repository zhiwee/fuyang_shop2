package com.zhiwee.gree.dao.support.goods;

import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.goods.ProductAttributeDao;
import com.zhiwee.gree.dao.support.goods.mapper.ProductAttributeMapper;
import com.zhiwee.gree.model.GoodsInfo.ProductAttribute;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

/**
 * Created by jick on 2019/8/28.
 */
@Repository
public class ProductAttributeDaoImpl extends BaseDaoImpl<ProductAttribute,String,ProductAttributeMapper>  implements ProductAttributeDao{
    @Override
    public Pageable<ProductAttribute> queryList(Map<String, Object> params, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return     convertToPageable(this.mapper.queryList(params));
    }

    @Override
    public List<ProductAttribute> selectBycategoryId(Map<String, Object> param) {
        return this.mapper.selectBycategoryId(param);
    }



    //extends BaseDaoImpl<GoodsInfo, String, GoodsInfoMapper> implements GoodsInfoDao {

    //   PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
     //   return convertToPageable(this.mapper.searchAll(params));


}
