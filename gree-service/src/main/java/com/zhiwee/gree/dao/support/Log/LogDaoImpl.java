package com.zhiwee.gree.dao.support.Log;

import com.zhiwee.gree.dao.log.LogDao;
import com.zhiwee.gree.dao.support.Log.mapper.LogMapper;
import com.zhiwee.gree.model.Log.Log;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;

/**
 * @author sun on 2019/3/28
 */
@Repository
public class LogDaoImpl extends BaseDaoImpl<Log, String, LogMapper> implements LogDao {
}
