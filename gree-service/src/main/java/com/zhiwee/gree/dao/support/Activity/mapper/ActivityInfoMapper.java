package com.zhiwee.gree.dao.support.Activity.mapper;

import com.zhiwee.gree.model.Activity.ActivityInfo;

import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;


/**
 * @author SUN
 * @since 2018/7/6 14:26
 */
public interface ActivityInfoMapper extends BaseMapper<ActivityInfo> {

    List<ActivityInfo> checkTime(Map<String, Object> param);

    ActivityInfo getCurrent(Map<String, Object> param);
}
