package com.zhiwee.gree.dao.support.order.mapper;


import com.zhiwee.gree.model.order.BaseOrder;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

public interface BaseOrderMapper extends BaseMapper<BaseOrder> {
    //extends BaseMapper<OrderAddress>
}
