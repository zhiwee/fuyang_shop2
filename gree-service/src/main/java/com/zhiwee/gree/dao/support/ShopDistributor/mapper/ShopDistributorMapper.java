package com.zhiwee.gree.dao.support.ShopDistributor.mapper;


import com.github.pagehelper.PageRowBounds;
import com.zhiwee.gree.model.Dealer.DealerInfo;
import com.zhiwee.gree.model.ShopDistributor.ShopDistributor;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;


public interface ShopDistributorMapper extends BaseMapper<ShopDistributor> {


    List<ShopDistributor> infoPage(Map<String, Object> param, PageRowBounds rowBounds);


    List<ShopDistributor> newPage(Map<String, Object> param, PageRowBounds rowBounds);


    ShopDistributor defaultDistributor(Map<String,Object> mapkey);
}
