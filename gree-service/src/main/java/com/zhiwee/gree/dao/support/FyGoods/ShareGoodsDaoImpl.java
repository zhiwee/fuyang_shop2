package com.zhiwee.gree.dao.support.FyGoods;

import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.FyGoods.GrouponGoodsDao;
import com.zhiwee.gree.dao.FyGoods.ShareGoodsDao;
import com.zhiwee.gree.dao.support.FyGoods.mapper.GrouponGoodsMapper;
import com.zhiwee.gree.dao.support.FyGoods.mapper.ShareGoodsMapper;
import com.zhiwee.gree.model.FyGoods.GrouponGoods;
import com.zhiwee.gree.model.FyGoods.ShareGoods;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;


@Repository
public class ShareGoodsDaoImpl
        extends BaseDaoImpl<ShareGoods, String, ShareGoodsMapper> implements ShareGoodsDao {
    @Override
    public List<ShareGoods> queryGoodsList(Map<String, Object> param, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return (this.mapper.queryGoodsList(param));
    }

}
