package com.zhiwee.gree.dao.support.tgoods.mapper;

import com.zhiwee.gree.model.FyGoods.GoodsLabelValue;
import xyz.icrab.common.dao.support.mapper.BaseMapper;


/**
 * @author DELL
 */
public interface GoodsLabelValueMapper extends BaseMapper<GoodsLabelValue> {
}
