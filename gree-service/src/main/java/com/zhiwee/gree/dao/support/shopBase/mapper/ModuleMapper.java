package com.zhiwee.gree.dao.support.shopBase.mapper;

import com.zhiwee.gree.model.Module;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

/**
 * @author gll
 * @since 2018/4/21 22:35
 */
public interface ModuleMapper extends BaseMapper<Module> {
}
