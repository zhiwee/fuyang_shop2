package com.zhiwee.gree.dao.HomePage;

import com.zhiwee.gree.model.HomePage.PageNav;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

public interface PageNavDao extends BaseDao<PageNav,String> {
    Pageable<PageNav> getPage(Map<String, Object> param, Pagination pagination);

    PageNav getById(String id);

    List<PageNav> getList();
}
