package com.zhiwee.gree.dao.support.shopBase;

import java.util.List;

import com.zhiwee.gree.dao.shopBase.ModuleDao;
import com.zhiwee.gree.dao.support.shopBase.mapper.InterfaceMapper;
import com.zhiwee.gree.dao.support.shopBase.mapper.ModuleMapper;
import com.zhiwee.gree.model.Module;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import xyz.icrab.common.dao.support.BaseDaoImpl;


/**
 * @author gll
 * @since 2018/4/21 22:35
 */
@Repository
public class ModuleDaoImpl extends BaseDaoImpl<Module, String, ModuleMapper> implements ModuleDao {

	@Autowired
	InterfaceMapper interfaceMapper;
	
	@Override
	public int delete(List<String> ids) {
		for(String id : ids){
			interfaceMapper.deleteByModuleId(id);
		}
		return super.delete(ids);
	}
	
	
}
