package com.zhiwee.gree.dao.Coupon;

import com.zhiwee.gree.model.Coupon.CouponSubject;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Page;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.Map;

public interface CouponSubjectDao extends BaseDao<CouponSubject, String> {
    Pageable<CouponSubject> pageInfo(Map<String, Object> param, Pagination pagination);
}
