package com.zhiwee.gree.dao.FyGoods;

import com.zhiwee.gree.model.FyGoods.FyGoodsDetail;
import xyz.icrab.common.dao.BaseDao;

import java.util.List;
import java.util.Map;

public interface FyGoodsDetailDao   extends BaseDao<FyGoodsDetail,String> {

    List<FyGoodsDetail> queryByGoodsId(Map<String,Object> param);

}
