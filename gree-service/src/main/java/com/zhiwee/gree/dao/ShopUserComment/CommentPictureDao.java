package com.zhiwee.gree.dao.ShopUserComment;


import com.zhiwee.gree.model.ShopUserComment.CommentPicture;
import com.zhiwee.gree.model.ShopUserComment.ShopUserComment;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.web.session.Session;

import java.util.List;
import java.util.Map;

public interface CommentPictureDao extends BaseDao<CommentPicture, String> {



}
