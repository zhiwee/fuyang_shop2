package com.zhiwee.gree.dao.crad;

import com.zhiwee.gree.model.card.CardInfo;
import com.zhiwee.gree.model.card.LShopUserCards;
import com.zhiwee.gree.model.draw.DrawsInfo;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.Map;

public interface CardInfoDao extends BaseDao<CardInfo,String> {

    Pageable<CardInfo> pageInfo(Map<String, Object> params, Pagination pagination);

    CardInfo getInfo(CardInfo cardInfo);

}
