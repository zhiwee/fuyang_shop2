package com.zhiwee.gree.dao.goods;

import com.zhiwee.gree.model.GoodsInfo.GoodsSpecsItem;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.Map;

public interface GoodsSpecsItemDao extends BaseDao<GoodsSpecsItem,String> {
    Pageable<GoodsSpecsItem> getPage(Map<String, Object> param, Pagination pagination);
}
