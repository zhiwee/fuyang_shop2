package com.zhiwee.gree.dao.crad;

import com.zhiwee.gree.model.card.UserCardsInfo;
import com.zhiwee.gree.model.card.UserCardsInfoExrt;
import com.zhiwee.gree.model.draw.CanDraws;
import com.zhiwee.gree.model.draw.DrawUseInfoExrt;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;


public interface UserCardsInfoDao extends BaseDao<UserCardsInfo,String> {
    Pageable<UserCardsInfo> pageInfo(Map<String, Object> params, Pagination pagination);

    Pageable<UserCardsInfo> pageInfoNoCategory(Map<String, Object> params, Pagination pagination);

    UserCardsInfo getCurrentOne(Map<String,Object> param);

    List<UserCardsInfoExrt> queryUserCardsInfoExport(Map<String,Object> params);

    List<UserCardsInfoExrt> queryUserCardsInfoNoCategoryExport(Map<String,Object> params);

    List<UserCardsInfo> queryUserCardsInfo(Map<String,Object> params);

}
