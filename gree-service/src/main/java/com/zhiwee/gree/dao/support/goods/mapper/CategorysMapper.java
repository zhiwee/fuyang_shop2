package com.zhiwee.gree.dao.support.goods.mapper;

import com.github.pagehelper.PageRowBounds;
import com.zhiwee.gree.model.GoodsInfo.Categorys;
import com.zhiwee.gree.model.GoodsInfo.GoodVo.CategorysExo;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;


public interface CategorysMapper extends BaseMapper<Categorys> {

    List<Categorys> getPage(Map<String, Object> param, PageRowBounds rowBounds);

    List<CategorysExo> listExo();
}
