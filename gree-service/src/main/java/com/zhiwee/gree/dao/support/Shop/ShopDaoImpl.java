package com.zhiwee.gree.dao.support.Shop;

import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.Shop.ShopDao;
import com.zhiwee.gree.dao.support.Shop.mapper.ShopMapper;
import com.zhiwee.gree.model.Shop.Shop;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.Map;

/**
 * @author jiake
 * @date 2020-02-19 14:54
 */
@Repository
public class ShopDaoImpl extends BaseDaoImpl<Shop,String, ShopMapper> implements ShopDao {
    @Override
    public Pageable<Shop> queryList(Map<String, Object> params, Pagination pagination) {
        //return  this.mapper;
        //return null;
        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return convertToPageable(this.mapper.queryList(params));
    }
    //extends BaseDaoImpl<Area, String, AreaMapper> implements AreaDao
}
