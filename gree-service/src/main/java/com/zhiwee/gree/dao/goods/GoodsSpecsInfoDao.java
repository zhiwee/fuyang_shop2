package com.zhiwee.gree.dao.goods;

import com.zhiwee.gree.model.GoodsInfo.GoodsSpecsInfo;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.Map;

public interface GoodsSpecsInfoDao extends BaseDao<GoodsSpecsInfo ,String> {
    Pageable<GoodsSpecsInfo> getPage(Map<String, Object> param, Pagination pagination);

    Pageable<GoodsSpecsInfo> getItemPage(Map<String, Object> param, Pagination pagination);
}
