package com.zhiwee.gree.dao.goods;

import com.zhiwee.gree.model.GoodsInfo.GoodVo.SpecsInfoOrderVO;
import com.zhiwee.gree.model.GoodsInfo.GoodsSpecsInfoOrder;
import xyz.icrab.common.dao.BaseDao;

import java.util.List;
import java.util.Map;

public interface GoodsSpecsInfoOrderDao  extends BaseDao<GoodsSpecsInfoOrder,String> {
    List<SpecsInfoOrderVO> selectOneList(Map<String, Object> params);
}
