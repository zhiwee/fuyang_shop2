package com.zhiwee.gree.dao.support.AirCondition;


import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.AchieveGift.GiftInfoDao;
import com.zhiwee.gree.dao.AirCondition.AirConditionDao;
import com.zhiwee.gree.dao.support.AchieveGift.mapper.GiftInfoMapper;
import com.zhiwee.gree.dao.support.AirCondition.mapper.AirConditionMapper;
import com.zhiwee.gree.model.AchieveGift.GiftInfo;
import com.zhiwee.gree.model.AirCondition.AirCondition;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.Map;


@Repository
public class AirConditionDaoImpl extends BaseDaoImpl<AirCondition, String,AirConditionMapper> implements AirConditionDao {


    @Override
    public Pageable<AirCondition> pageInfo(Map<String, Object> params, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return convertToPageable(this.mapper.pageInfo(params));
    }
}
