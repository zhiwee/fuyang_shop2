package com.zhiwee.gree.dao.goods;

import com.zhiwee.gree.model.GoodsInfo.GoodsInfoPicture;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

public interface GoodsInfoPictureDao  extends BaseDao<GoodsInfoPicture,String> {
    List<GoodsInfoPicture> listAll(Map<String, Object> param);

    Pageable<GoodsInfoPicture> pageInfo(Map<String, Object> params, Pagination pagination);

    GoodsInfoPicture getInfo(String id);
    // extends BaseDao<GoodsInfo, String>
}
