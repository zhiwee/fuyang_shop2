package com.zhiwee.gree.dao.ShopDistributor;


import com.zhiwee.gree.model.ShopDistributor.DistributorGrade;
import com.zhiwee.gree.model.ShopDistributor.ShopDistributor;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.Map;

public interface DistributorGradeDao extends BaseDao<DistributorGrade,String> {


    Pageable<DistributorGrade> infoPage(Map<String, Object> param, Pagination pagination);


    DistributorGrade selectMax(Map<String,Object> params);
}
