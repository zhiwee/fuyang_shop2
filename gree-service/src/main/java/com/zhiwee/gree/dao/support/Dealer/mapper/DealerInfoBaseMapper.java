package com.zhiwee.gree.dao.support.Dealer.mapper;


import com.github.pagehelper.PageRowBounds;

import com.zhiwee.gree.model.Dealer.DealerInfoBase;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;


public interface DealerInfoBaseMapper extends BaseMapper<DealerInfoBase> {


    List<DealerInfoBase> infoPage(Map<String,Object> param, PageRowBounds rowBounds);
}
