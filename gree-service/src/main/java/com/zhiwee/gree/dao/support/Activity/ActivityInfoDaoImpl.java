package com.zhiwee.gree.dao.support.Activity;

import com.zhiwee.gree.dao.Activity.ActivityInfoDao;
import com.zhiwee.gree.dao.support.Activity.mapper.ActivityInfoMapper;
import com.zhiwee.gree.model.Activity.ActivityInfo;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;

import java.util.List;
import java.util.Map;

/**
 * @author sun
 * @since 2018/7/6 14:26
 */
@Repository
public class ActivityInfoDaoImpl extends BaseDaoImpl<ActivityInfo, String, ActivityInfoMapper> implements ActivityInfoDao {

    @Override
    public List<ActivityInfo> checkTime(Map<String, Object> param) {
        return mapper.checkTime(param);
    }

    @Override
    public ActivityInfo getCurrent(Map<String, Object> param) {
        return mapper.getCurrent(param);
    }
}
