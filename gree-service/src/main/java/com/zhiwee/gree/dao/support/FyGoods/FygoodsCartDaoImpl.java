package com.zhiwee.gree.dao.support.FyGoods;

import com.zhiwee.gree.dao.FyGoods.FygoodsCartDao;
import com.zhiwee.gree.dao.support.FyGoods.mapper.FygoodsCartMapper;
import com.zhiwee.gree.model.FyGoods.FygoodsCart;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;

@Repository
public class FygoodsCartDaoImpl
        extends BaseDaoImpl<FygoodsCart, String, FygoodsCartMapper> implements FygoodsCartDao {
}
