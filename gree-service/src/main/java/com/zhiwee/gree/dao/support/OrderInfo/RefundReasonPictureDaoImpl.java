package com.zhiwee.gree.dao.support.OrderInfo;



import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.OrderInfo.OrderRefundReasonDao;
import com.zhiwee.gree.dao.OrderInfo.RefundReasonPictureDao;
import com.zhiwee.gree.dao.support.OrderInfo.mapper.OrderRefundReasonMapper;
import com.zhiwee.gree.dao.support.OrderInfo.mapper.RefundReasonPictureMapper;
import com.zhiwee.gree.model.OrderInfo.OrderRefundReason;
import com.zhiwee.gree.model.OrderInfo.RefundReasonPicture;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.Map;


@Repository
public class RefundReasonPictureDaoImpl extends BaseDaoImpl<RefundReasonPicture, String, RefundReasonPictureMapper> implements RefundReasonPictureDao {


}
