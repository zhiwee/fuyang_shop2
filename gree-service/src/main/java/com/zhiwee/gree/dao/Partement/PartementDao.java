package com.zhiwee.gree.dao.Partement;


import com.zhiwee.gree.model.Partement.Partement;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.Map;

public interface PartementDao extends BaseDao<Partement, String> {


    Pageable<Partement> pageInfo(Map<String,Object> params, Pagination pagination);
}
