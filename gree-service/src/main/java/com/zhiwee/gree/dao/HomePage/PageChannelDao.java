package com.zhiwee.gree.dao.HomePage;

import com.zhiwee.gree.model.HomePage.PageChannel;
import com.zhiwee.gree.model.HomePage.PageV0.PageChannelVO;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

public interface PageChannelDao extends BaseDao<PageChannel,String> {
    Pageable<PageChannel> getPage(Map<String, Object> param, Pagination pagination);

    List<PageChannelVO> getList(Map<String, Object> param);
}
