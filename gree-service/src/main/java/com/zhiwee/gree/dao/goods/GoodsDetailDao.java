package com.zhiwee.gree.dao.goods;


import com.zhiwee.gree.model.GoodsInfo.GoodsDetail;
import com.zhiwee.gree.model.GoodsInfo.GoodsInfo;
import com.zhiwee.gree.model.GoodsInfo.SolrGoodsInfo;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

public interface GoodsDetailDao extends BaseDao<GoodsDetail, String> {


}
