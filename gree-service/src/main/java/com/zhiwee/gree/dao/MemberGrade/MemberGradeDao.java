package com.zhiwee.gree.dao.MemberGrade;


import com.zhiwee.gree.model.GoodsInfo.GoodsInfo;
import com.zhiwee.gree.model.GoodsInfo.SolrGoodsInfo;
import com.zhiwee.gree.model.MemberGrade.MemberGrade;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

public interface MemberGradeDao extends BaseDao<MemberGrade, String> {




    Pageable<MemberGrade> searchAll(Map<String, Object> params, Pagination pagination);


    MemberGrade selectMax(Map<String, Object> params);
}
