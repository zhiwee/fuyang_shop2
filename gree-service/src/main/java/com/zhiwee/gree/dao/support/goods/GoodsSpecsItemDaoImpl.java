package com.zhiwee.gree.dao.support.goods;

import com.github.pagehelper.PageRowBounds;
import com.zhiwee.gree.dao.goods.GoodsSpecsItemDao;
import com.zhiwee.gree.dao.support.goods.mapper.GoodsSpecsItemMapper;
import com.zhiwee.gree.model.GoodsInfo.GoodsSpecsInfo;
import com.zhiwee.gree.model.GoodsInfo.GoodsSpecsItem;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

@Repository
public class GoodsSpecsItemDaoImpl extends BaseDaoImpl<GoodsSpecsItem,String, GoodsSpecsItemMapper> implements GoodsSpecsItemDao {
    @Override
    public Pageable<GoodsSpecsItem> getPage(Map<String, Object> param, Pagination pagination) {

        PageRowBounds rowBounds = new PageRowBounds(pagination.getOffset(), pagination.getPageSize());
        rowBounds.setCount(true);
        List<GoodsSpecsItem> data = this.mapper.getPage(param, rowBounds);
        return convertToPageable(rowBounds, data);
    }
}
