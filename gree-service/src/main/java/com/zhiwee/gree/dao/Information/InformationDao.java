package com.zhiwee.gree.dao.Information;

import com.zhiwee.gree.model.Information.Information;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

/**
 * Created by jick on 2019/7/18.
 */
public interface InformationDao extends BaseDao<Information,String> {
    //queryInfoList
    // Pageable<HomePage> achieveHomePage(Map<String,Object> params, Pagination pagination);
    Pageable<Information>  queryInfoList(Pagination pagination,Map<String,Object> param);
    //List<Information> queryInfoList();
    //Pageable<Invoice> queryBaseInvoice(Map<String,Object> param,Pagination pagination);

}
