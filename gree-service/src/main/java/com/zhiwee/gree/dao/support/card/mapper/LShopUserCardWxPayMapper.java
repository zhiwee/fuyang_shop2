package com.zhiwee.gree.dao.support.card.mapper;

import com.zhiwee.gree.model.card.LShopUserCardWxPayInfo;
import com.zhiwee.gree.model.card.LShopUserCardWxPayInfoExrt;
import com.zhiwee.gree.model.card.LShopUserCards;
import com.zhiwee.gree.model.card.LShopUserCardsExrt;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * TODO
 *
 * @author Chensong
 * @Company http://www.zhiwee.com
 * date 2020-01-03
 */
public interface LShopUserCardWxPayMapper extends BaseMapper<LShopUserCardWxPayInfo> {

    List<LShopUserCardWxPayInfo> getLShopUserCardWxPayInfoList(Map<String, Object> params);

    List<LShopUserCardWxPayInfoExrt> queryLShopUserCardWxPayInfoExport(Map<String, Object> params);
}
