package com.zhiwee.gree.dao.support.Shop.mapper;

import com.zhiwee.gree.model.Shop.Shop;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;


public interface ShopMapper extends BaseMapper<Shop> {
    List<Shop> queryList(Map<String, Object> params);
    // extends BaseMapper<ShopArea>
}
