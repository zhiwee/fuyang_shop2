package com.zhiwee.gree.dao.support.GoodsLabel;


import com.zhiwee.gree.dao.GoodsLabel.EfficiencyDao;
import com.zhiwee.gree.dao.GoodsLabel.SpaceDao;
import com.zhiwee.gree.dao.support.GoodsLabel.mapper.EfficiencyMapper;
import com.zhiwee.gree.dao.support.GoodsLabel.mapper.SpaceMapper;
import com.zhiwee.gree.model.GoodsLabel.Efficiency;
import com.zhiwee.gree.model.GoodsLabel.Space;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;


@Repository
public class EfficiencyDaoImpl extends BaseDaoImpl<Efficiency, String, EfficiencyMapper> implements EfficiencyDao {



}
