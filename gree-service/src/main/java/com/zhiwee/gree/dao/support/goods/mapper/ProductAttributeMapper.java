package com.zhiwee.gree.dao.support.goods.mapper;

import com.zhiwee.gree.model.GoodsInfo.ProductAttribute;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * Created by jick on 2019/8/28.
 */
public interface ProductAttributeMapper   extends BaseMapper<ProductAttribute> {
    //extends BaseMapper<GoodsInfo>
    //queryList
    // List<GoodsInfo> searchAll(Map<String,Object> params);
    List<ProductAttribute> queryList(Map<String,Object> params);

    List<ProductAttribute> selectBycategoryId(Map<String, Object> param);

}
