package com.zhiwee.gree.dao.ShopUserComment;


import com.zhiwee.gree.model.HomePage.HomePage;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.ShopUserComment.Comment;
import com.zhiwee.gree.model.ShopUserComment.ShopUserComment;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.web.session.Session;

import java.util.List;
import java.util.Map;

public interface ShopUserCommentDao extends BaseDao<ShopUserComment, String> {


    Pageable<ShopUserComment> achieveComment(Map<String, Object> params, Pagination pagination);

    Pageable<ShopUserComment> achieveCommentByGoodsId(List<String> ids, Pagination pagination, Session user);

    Integer todayAmount(Map<String,Object> params);

    Pageable<ShopUserComment>  achieveMyComment(Map<String,Object> param,Pagination pagination);
}
