package com.zhiwee.gree.dao.support.draw.mapper;

import com.zhiwee.gree.model.draw.DrawUseInfo;
import com.zhiwee.gree.model.draw.DrawUseInfoExrt;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

public interface DrawUseInfoMapper extends BaseMapper<DrawUseInfo> {

    List<DrawUseInfo> pageInfo(Map<String, Object> params);
    List<DrawUseInfoExrt> queryDrawUseInfoExport(Map<String, Object> params);

}
