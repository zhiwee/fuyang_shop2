package com.zhiwee.gree.dao.support.card.mapper;

import com.zhiwee.gree.model.card.UserCardsInfo;
import com.zhiwee.gree.model.card.UserCardsInfoExrt;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;


public interface UserCardsInfoMapper extends BaseMapper<UserCardsInfo> {
    List<UserCardsInfo> pageInfo(Map<String, Object> params);

    List<UserCardsInfo> pageInfoNoCategory(Map<String, Object> params);

    UserCardsInfo getCurrentOne(Map<String, Object> param);

    List<UserCardsInfoExrt> queryUserCardsInfoExport(Map<String, Object> params);
    List<UserCardsInfoExrt> queryUserCardsInfoNoCategoryExport(Map<String, Object> params);

    List<UserCardsInfo>  queryUserCardsInfo(Map<String, Object> params);

}
