package com.zhiwee.gree.dao.OnlineActivity;

import com.zhiwee.gree.model.Activity.ActivityInfo;
import com.zhiwee.gree.model.GoodsInfo.LAPeople;
import com.zhiwee.gree.model.OnlineActivity.OnlineActivityInfo;
import com.zhiwee.gree.model.OnlineActivity.OnlineActivityInfoExe;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

/**
 * @author sun
 * @since 2018/7/6 14:25
 */
public interface OnlineActivityInfoDao extends BaseDao<OnlineActivityInfo, String> {

    List<OnlineActivityInfoExe> exportActivityInfo(Map<String, Object> params);
}
