package com.zhiwee.gree.dao.OrderInfo;


import com.zhiwee.gree.model.Analysis.ItemAnalysis;
import com.zhiwee.gree.model.OrderInfo.OrderItem;
import com.zhiwee.gree.model.OrderInfo.OrderRecord;
import com.zhiwee.gree.model.OrderInfo.vo.GoodNameTopTen;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

public interface OrderItemDao extends BaseDao<OrderItem, String> {


    Pageable<OrderItem> orderItemList(Map<String,Object> params, Pagination pagination);

    Pageable<OrderRecord> achieveOrderRecord(List<String> ids, Pagination pagination);

    List<OrderItem> achieveItemsByOrderId(Map<String,Object> orderId);

    List<GoodNameTopTen> topTen();

    ItemAnalysis itemAnalysis(Map<String,Object> params);
}
