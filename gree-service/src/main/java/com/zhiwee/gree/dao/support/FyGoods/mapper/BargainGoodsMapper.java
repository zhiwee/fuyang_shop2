package com.zhiwee.gree.dao.support.FyGoods.mapper;


import com.zhiwee.gree.model.FyGoods.BargainGoods;
import com.zhiwee.gree.model.FyGoods.ShareGoods;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;


public interface BargainGoodsMapper extends BaseMapper<BargainGoods> {
    List<BargainGoods> queryGoodsList(Map<String, Object> param);
}