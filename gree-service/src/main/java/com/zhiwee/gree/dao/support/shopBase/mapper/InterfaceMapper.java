package com.zhiwee.gree.dao.support.shopBase.mapper;

import java.util.List;
import java.util.Map;

import com.zhiwee.gree.model.Interface;
import org.apache.ibatis.session.RowBounds;

import xyz.icrab.common.dao.support.mapper.BaseMapper;
import xyz.icrab.common.model.Relation;

/**
 * @author gll
 * @since 2018/4/21 22:35
 */
public interface InterfaceMapper extends BaseMapper<Interface> {
	/*
	 * 根据模块id删除接口数据
	 */
	int deleteByModuleId(String id);
	/*
	 * 用户添加接口权限
	 */
	int saveUserInterface(List<Relation> list);
	/*
	 * 角色添加接口权限
	 */
	int saveRoleInterface(List<Relation> list);
	/*
	 * 根据模块roleid删除角色接口关系数据
	 */
	int delRoleIntByIds(Map<String, Object> map);
	/*
	 * 根据模块userid删除角色接口关系数据
	 */
	int delUserIntByIds(Map<String, Object> map);

	/*
	 * 根据userid获取接口
	 */
	List<Interface> getIntByUserId(String userid);
	/*
	 * 根据roleid获取接口
	 */
	List<Interface> getIntByRoleIds(String... roleids);
	List<Interface> getAll(Map<String, Object> map, RowBounds rowBounds);
	List<Interface> getAll(Map<String, Object> map);
}
