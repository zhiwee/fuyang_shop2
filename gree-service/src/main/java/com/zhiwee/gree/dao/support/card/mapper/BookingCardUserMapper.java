package com.zhiwee.gree.dao.support.card.mapper;

import com.zhiwee.gree.model.card.BookingCardUser;
import xyz.icrab.common.dao.support.mapper.BaseMapper;


public interface BookingCardUserMapper extends BaseMapper<BookingCardUser> {

}
