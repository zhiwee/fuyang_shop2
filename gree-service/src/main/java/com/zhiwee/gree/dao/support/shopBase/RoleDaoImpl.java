package com.zhiwee.gree.dao.support.shopBase;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.zhiwee.gree.dao.shopBase.RoleDao;
import com.zhiwee.gree.dao.support.shopBase.mapper.RoleMapper;
import com.zhiwee.gree.model.Role;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Relation;
/**
 * @author gll
 * @since 2018/4/21 22:35
 */
@Repository
public class RoleDaoImpl extends BaseDaoImpl<Role, String, RoleMapper> implements RoleDao {
	

	@Override
	public int checkName(String name) {
		return mapper.checkName(name);
	}
	
	@Override
	public int updateState(Map<String, Object> map) {
		if(map == null){
			map = new HashMap<>();
		}
		return mapper.updateState(map);
	}

	@Override
	public int saveUserRole(List<Relation> list) {
		if(CollectionUtils.isNotEmpty(list)){
			return mapper.saveUserRole(list);
		}
		return 0;
	}

	@Override
	public List<Role> getRoleByUserId(String userid) {
		if(StringUtils.isNotEmpty(userid)){
			return mapper.getRoleByUserId(userid);
		}
		return null;
	}


	@Override
	public int delUserRoleByIds(Map<String, Object> map) {
			return mapper.delUserRoleByIds(map);
	}
	
}
