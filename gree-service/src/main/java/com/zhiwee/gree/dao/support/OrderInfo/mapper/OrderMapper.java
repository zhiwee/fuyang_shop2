package com.zhiwee.gree.dao.support.OrderInfo.mapper;


import com.zhiwee.gree.model.OrderInfo.Order;
import com.zhiwee.gree.model.OrderInfo.OrderExport;
import com.zhiwee.gree.model.OrderInfo.vo.OrderStatisticItem;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;


public interface OrderMapper extends BaseMapper<Order> {


    List<Order> pageInfo(Map<String,Object> params);

    List<OrderExport> queryOrderExport(Map<String,Object> params);

    List<Order> achieveMyOrder(Map<String,Object> params);

    Integer orderAmount(Map<String,Object> params);

    List<OrderStatisticItem> statisticAllAmountByDate(Map<String,Object> param);

    List<Order> achieveMyEngineerOrder(Map<String,Object> params);

    Double totalPrice(Map<String,Object> params);
}
