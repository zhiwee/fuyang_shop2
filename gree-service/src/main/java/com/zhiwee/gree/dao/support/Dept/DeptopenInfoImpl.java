package com.zhiwee.gree.dao.support.Dept;

import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.Dept.DeptopenInfoDao;
import com.zhiwee.gree.dao.support.Dept.mapper.DeptopenInfoMapper;
import com.zhiwee.gree.model.DeptInfo.DeptopenInfo;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.Map;

/**
 * Created by jick on 2019/8/23.
 */
@Repository
public class DeptopenInfoImpl  extends BaseDaoImpl<DeptopenInfo,String,DeptopenInfoMapper>  implements DeptopenInfoDao {
    @Override
    public Pageable<DeptopenInfo> queryDeptOpenInfo(Map<String, Object> param, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return convertToPageable(this.mapper.queryDeptOpenInfo(param));
    }





}
