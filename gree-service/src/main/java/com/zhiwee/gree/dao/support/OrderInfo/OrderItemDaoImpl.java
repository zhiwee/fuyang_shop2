package com.zhiwee.gree.dao.support.OrderInfo;



import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.OrderInfo.OrderDao;
import com.zhiwee.gree.dao.OrderInfo.OrderItemDao;
import com.zhiwee.gree.dao.support.OrderInfo.mapper.OrderItemMapper;
import com.zhiwee.gree.dao.support.OrderInfo.mapper.OrderMapper;
import com.zhiwee.gree.model.Analysis.ItemAnalysis;
import com.zhiwee.gree.model.OrderInfo.Order;
import com.zhiwee.gree.model.OrderInfo.OrderItem;
import com.zhiwee.gree.model.OrderInfo.OrderRecord;
import com.zhiwee.gree.model.OrderInfo.vo.GoodNameTopTen;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;


@Repository
public class OrderItemDaoImpl extends BaseDaoImpl<OrderItem, String, OrderItemMapper> implements OrderItemDao {


    @Override
    public Pageable<OrderItem> orderItemList(Map<String, Object> params, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return convertToPageable(this.mapper.orderItemList(params));
    }

    @Override
    public Pageable<OrderRecord> achieveOrderRecord(List<String> ids, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return convertToPageable(this.mapper.achieveOrderRecord(ids));
    }

    @Override
    public List<OrderItem> achieveItemsByOrderId(Map<String, Object> orderId) {
        return this.mapper.achieveItemsByOrderId(orderId);
    }

    @Override
    public List<GoodNameTopTen> topTen() {
       return mapper.topTen();
    }

    @Override
    public ItemAnalysis itemAnalysis(Map<String, Object> params) {
        return this.mapper.itemAnalysis(params);
    }
}
