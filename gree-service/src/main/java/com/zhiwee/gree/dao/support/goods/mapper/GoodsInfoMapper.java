package com.zhiwee.gree.dao.support.goods.mapper;


import com.zhiwee.gree.model.GoodsInfo.GoodsInfo;
import com.zhiwee.gree.model.GoodsInfo.LAGoodsInfo;
import com.zhiwee.gree.model.GoodsInfo.LAPeople;
import com.zhiwee.gree.model.GoodsInfo.SolrGoodsInfo;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;


public interface GoodsInfoMapper extends BaseMapper<GoodsInfo> {

//
//    List<SolrGoodsInfo> getAllGoods();

    List<GoodsInfo> searchAll(Map<String,Object> params);


    void updateActivityGoodsInfo(List<LAGoodsInfo> list);

    void updateActivityPeople(List<LAPeople> list);

    void updateActivityGoodsInfo2(List<LAGoodsInfo> list);
}
