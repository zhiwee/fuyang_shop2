package com.zhiwee.gree.dao.support.goods;

import com.github.pagehelper.PageRowBounds;
import com.zhiwee.gree.dao.goods.GoodsCategoryDao;
import com.zhiwee.gree.dao.support.goods.mapper.GoodsCategoryMapper;
import com.zhiwee.gree.model.GoodsInfo.GoodVo.GategoryVo;
import com.zhiwee.gree.model.GoodsInfo.GoodVo.GoodsCategoryCY;
import com.zhiwee.gree.model.GoodsInfo.GoodsBand;
import com.zhiwee.gree.model.GoodsInfo.GoodsCategory;
import com.zhiwee.gree.model.HomePage.PageV0.PageChannelGoodsVO;
import com.zhiwee.gree.model.ShopDistributor.CategoryVo;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

@Repository
public class GoodsCategoryDaoImpl  extends BaseDaoImpl<GoodsCategory,String, GoodsCategoryMapper> implements GoodsCategoryDao {
    @Override
    public Pageable<GoodsCategory> getPage(Map<String, Object> param, Pagination pagination) {
        PageRowBounds rowBounds = new PageRowBounds(pagination.getOffset(), pagination.getPageSize());
        rowBounds.setCount(true);
        List<GoodsCategory> data = this.mapper.getPage(param, rowBounds);
        return convertToPageable(rowBounds, data);
    }

    @Override
    public GoodsCategory selectByid(String id) {
        return this.mapper.selectByid(id);
    }

    @Override
    public List<GategoryVo> getVoList(Map<String, Object> params) {
        return this.mapper.getVoList(params);
    }

    @Override
    public List<PageChannelGoodsVO> getCategoryGoods(Map<String, Object> params) {
        return this.mapper.getCategoryGoods(params);
    }

    @Override
    public List<GoodsCategoryCY> listInfo() {
        return mapper.listInfo();
    }

    @Override
    public List<GoodsCategory> listChildren(Map<String, Object> params) {
        return mapper.listChildren(params);
    }
}
