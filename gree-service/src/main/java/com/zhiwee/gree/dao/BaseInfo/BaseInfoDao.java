package com.zhiwee.gree.dao.BaseInfo;


import com.zhiwee.gree.model.BaseInfo.BaseInfo;
import com.zhiwee.gree.model.Coupon.Coupon;
import com.zhiwee.gree.model.Coupon.CouponExport;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

public interface BaseInfoDao extends BaseDao<BaseInfo, String> {


    Pageable<BaseInfo> pageInfo(Map<String, Object> param, Pagination pagination);


}
