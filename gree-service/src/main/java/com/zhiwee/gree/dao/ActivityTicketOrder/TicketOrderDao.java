package com.zhiwee.gree.dao.ActivityTicketOrder;

import com.zhiwee.gree.model.ActivityTicketOrder.ActivityTicketOrder;
import com.zhiwee.gree.model.ActivityTicketOrder.TicketOrder;
import com.zhiwee.gree.model.ActivityTicketOrder.TicketOrderExport;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

/**
 * @author sun
 * @since 下午5:06 2019/6/23
 */
public interface TicketOrderDao extends BaseDao<TicketOrder, String> {

    Pageable<TicketOrder> pageInfo(Map<String,Object> param, Pagination pagination);

    Pageable<TicketOrder> achieveMyOrder(Map<String,Object> params, Pagination pagination);

    List<TicketOrderExport> pageList(Map<String,Object> params);
}
