package com.zhiwee.gree.dao.support.tgoods;

import com.zhiwee.gree.dao.support.tgoods.mapper.GoodsLabelValueMapper;
import com.zhiwee.gree.dao.tgoods.GoodsLabelDao;
import com.zhiwee.gree.dao.tgoods.GoodsLabelValueDao;
import com.zhiwee.gree.model.FyGoods.GoodsLabel;
import com.zhiwee.gree.model.FyGoods.GoodsLabelValue;
import xyz.icrab.common.dao.support.BaseDaoImpl;

public class GoodsLabelValueDaoImpl extends BaseDaoImpl<GoodsLabelValue, String, GoodsLabelValueMapper> implements GoodsLabelValueDao {
        }
