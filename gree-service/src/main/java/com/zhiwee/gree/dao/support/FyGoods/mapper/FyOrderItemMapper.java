package com.zhiwee.gree.dao.support.FyGoods.mapper;


import com.aliyun.oss.model.LiveChannelStat;
import com.zhiwee.gree.model.FyGoods.FyOrderItem;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;


public interface FyOrderItemMapper  extends BaseMapper<FyOrderItem> {

    List<FyOrderItem> queryOrderItemByNum(Map<String,Object> param);
    int   deleteByGoodsId(Map<String,Object> param);


    void deleteAll(Map<String, Object> params);
}