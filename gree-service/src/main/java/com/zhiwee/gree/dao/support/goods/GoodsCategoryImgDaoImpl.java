package com.zhiwee.gree.dao.support.goods;

import com.github.pagehelper.PageRowBounds;
import com.zhiwee.gree.dao.goods.GoodsCategoryImgDao;
import com.zhiwee.gree.dao.support.goods.mapper.GoodsCategoryImgMapper;
import com.zhiwee.gree.model.GoodsInfo.GoodsCategory;
import com.zhiwee.gree.model.GoodsInfo.GoodsCategoryImg;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;


@Repository
public class GoodsCategoryImgDaoImpl extends BaseDaoImpl<GoodsCategoryImg,String, GoodsCategoryImgMapper> implements GoodsCategoryImgDao {

    @Override
    public Pageable<GoodsCategoryImg> getPage(Map<String, Object> param, Pagination pagination) {
        PageRowBounds rowBounds = new PageRowBounds(pagination.getOffset(), pagination.getPageSize());
        rowBounds.setCount(true);
        List<GoodsCategoryImg> data = this.mapper.getPage(param, rowBounds);
        return convertToPageable(rowBounds, data);
    }

    @Override
    public GoodsCategoryImg selectById(String id) {
        return this.mapper.selectById(id);
    }

    @Override
    public GoodsCategoryImg getOneByCategoryId(String categoryId) {
        return this.mapper.getOneByCategoryId(categoryId);
    }
}
