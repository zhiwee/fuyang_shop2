package com.zhiwee.gree.dao.crad;

import com.zhiwee.gree.model.card.BookingCardUser;
import xyz.icrab.common.dao.BaseDao;


public interface BookingCardUserDao extends BaseDao<BookingCardUser,String> {
}
