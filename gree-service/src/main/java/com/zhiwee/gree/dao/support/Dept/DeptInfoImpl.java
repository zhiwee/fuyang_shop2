package com.zhiwee.gree.dao.support.Dept;

import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.Dept.DeptInfoDao;
import com.zhiwee.gree.dao.support.Dept.mapper.DeptInfoMapper;
import com.zhiwee.gree.model.DeptInfo.DeptInfo;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.Map;

/**
 * Created by jick on 2019/8/26.
 */
@Repository
public class DeptInfoImpl   extends BaseDaoImpl<DeptInfo,String,DeptInfoMapper>   implements DeptInfoDao {
    @Override
    public Pageable<DeptInfo> pageInfo(Map<String, Object> param, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return convertToPageable(this.mapper.pageInfo(param));
    }
    //BaseDaoImpl<DeptopenInfo,String,DeptopenInfoMapper>  implements DeptopenInfoDao {
    //  PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
     //   return convertToPageable(this.mapper.queryDeptOpenInfo(param));
}
