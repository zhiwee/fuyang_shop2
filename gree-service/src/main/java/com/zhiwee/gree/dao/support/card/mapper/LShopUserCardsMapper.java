package com.zhiwee.gree.dao.support.card.mapper;

import com.zhiwee.gree.model.card.LShopUserCards;
import com.zhiwee.gree.model.card.LShopUserCardsExrt;
import com.zhiwee.gree.model.draw.CanDraws;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;


public interface LShopUserCardsMapper extends BaseMapper<LShopUserCards> {
    List<LShopUserCards> getLShopUserCardsList(Map<String, Object> params);

    List<LShopUserCardsExrt> queryLShopUserCardsExport(Map<String, Object> params);
}
