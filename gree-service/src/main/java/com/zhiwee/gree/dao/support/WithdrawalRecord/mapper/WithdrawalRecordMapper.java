package com.zhiwee.gree.dao.support.WithdrawalRecord.mapper;


import com.zhiwee.gree.model.WithdrawalRecord.WithdrawalRecord;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;


public interface WithdrawalRecordMapper extends BaseMapper<WithdrawalRecord> {




    List<WithdrawalRecord> pageInfo(Map<String, Object> param);


}
