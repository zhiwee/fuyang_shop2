package com.zhiwee.gree.dao.support.goods;

import com.github.pagehelper.PageRowBounds;
import com.zhiwee.gree.dao.goods.CategorysDao;
import com.zhiwee.gree.dao.support.goods.mapper.CategorysMapper;
import com.zhiwee.gree.model.GoodsInfo.Categorys;
import com.zhiwee.gree.model.GoodsInfo.GoodVo.CategorysExo;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

@Repository
public class CategorysDaoImpl extends BaseDaoImpl<Categorys,String, CategorysMapper> implements CategorysDao {


    @Override
    public Pageable<Categorys> getPage(Map<String, Object> param, Pagination pagination) {
        PageRowBounds rowBounds = new PageRowBounds(pagination.getOffset(), pagination.getPageSize());
        rowBounds.setCount(true);
        List<Categorys> data = this.mapper.getPage(param, rowBounds);
        return convertToPageable(rowBounds, data);
    }

    @Override
    public List<CategorysExo> listExo() {
        return mapper.listExo();
    }
}
