package com.zhiwee.gree.dao.support.ShopDistributor.mapper;


import com.github.pagehelper.PageRowBounds;
import com.zhiwee.gree.model.ShopDistributor.DistributorArea;
import com.zhiwee.gree.model.ShopDistributor.DistributorOrder;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;


public interface DistributorOrderMapper extends BaseMapper<DistributorOrder> {


    List<DistributorOrder> infoPage(Map<String,Object> param);
}
