package com.zhiwee.gree.dao.support.OrderInfo.mapper;


import com.zhiwee.gree.model.OrderInfo.Order;
import com.zhiwee.gree.model.OrderInfo.OrderPayment;
import xyz.icrab.common.dao.support.mapper.BaseMapper;


public interface OrderPaymentMapper extends BaseMapper<OrderPayment> {


}
