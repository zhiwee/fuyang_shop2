package com.zhiwee.gree.dao.support.FyGoods.mapper;

import com.zhiwee.gree.model.FyGoods.FyGoodsDetailText;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

public interface FyGoodsDetailTextMapper  extends BaseMapper<FyGoodsDetailText> {

}