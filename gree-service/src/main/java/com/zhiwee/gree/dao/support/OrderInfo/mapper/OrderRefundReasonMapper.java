package com.zhiwee.gree.dao.support.OrderInfo.mapper;


import com.zhiwee.gree.model.OrderInfo.OrderRefund;
import com.zhiwee.gree.model.OrderInfo.OrderRefundReason;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;


public interface OrderRefundReasonMapper extends BaseMapper<OrderRefundReason> {


    List<OrderRefundReason> pageInfo(Map<String,Object> params);

    List<OrderRefundReason> achieveRefundInfo(Map<String,Object> param);

    List<OrderRefundReason> achieveChangeOrderInfo(Map<String,Object> param);
}
