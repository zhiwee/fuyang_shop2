package com.zhiwee.gree.dao.draw;

import com.zhiwee.gree.model.draw.CanDraws;
import xyz.icrab.common.dao.BaseDao;


public interface CanDrawsDao extends BaseDao<CanDraws,String> {
}
