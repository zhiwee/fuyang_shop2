package com.zhiwee.gree.dao.goods;

import com.zhiwee.gree.model.GoodsInfo.GoodsAttributeOrder;
import xyz.icrab.common.dao.BaseDao;

import java.util.Map;

public interface GoodsAttributeOrderDao extends BaseDao<GoodsAttributeOrder,String> {

    void deleteByGoodsId(Map<String, Object> param);
}
