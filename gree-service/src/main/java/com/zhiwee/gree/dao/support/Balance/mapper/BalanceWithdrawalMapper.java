package com.zhiwee.gree.dao.support.Balance.mapper;


import com.zhiwee.gree.model.Balance.BalanceReCharge;
import com.zhiwee.gree.model.Balance.BalanceWithdrawal;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;


public interface BalanceWithdrawalMapper extends BaseMapper<BalanceWithdrawal> {

    List<BalanceWithdrawal> pageInfo(Map<String, Object> param);

    List<BalanceWithdrawal> balanceRecord(Map<String,Object> params);
}
