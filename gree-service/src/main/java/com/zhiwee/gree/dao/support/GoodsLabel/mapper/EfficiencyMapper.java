package com.zhiwee.gree.dao.support.GoodsLabel.mapper;


import com.zhiwee.gree.model.GoodsLabel.Efficiency;
import com.zhiwee.gree.model.GoodsLabel.Space;
import xyz.icrab.common.dao.support.mapper.BaseMapper;


public interface EfficiencyMapper extends BaseMapper<Efficiency> {


}
