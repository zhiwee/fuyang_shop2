package com.zhiwee.gree.dao.support.FyGoods;

import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.FyGoods.GrouponGoodsDao;
import com.zhiwee.gree.dao.FyGoods.PictureInfoDao;
import com.zhiwee.gree.dao.support.FyGoods.mapper.GrouponGoodsMapper;
import com.zhiwee.gree.dao.support.FyGoods.mapper.PictureInfoMapper;
import com.zhiwee.gree.model.FyGoods.GrouponGoods;
import com.zhiwee.gree.model.FyGoods.PictureInfo;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.Map;


@Repository
public class PictureInfoDaoImpl
        extends BaseDaoImpl<PictureInfo, String, PictureInfoMapper> implements PictureInfoDao {
    @Override
    public Pageable<PictureInfo> queryGoodsList(Map<String, Object> param, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return convertToPageable(this.mapper.queryGoodsList(param));
    }

}
