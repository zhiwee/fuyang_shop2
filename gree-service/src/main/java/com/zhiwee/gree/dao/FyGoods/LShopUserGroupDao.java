package com.zhiwee.gree.dao.FyGoods;

import com.zhiwee.gree.model.FyGoods.LShopUserGroup;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

public interface LShopUserGroupDao extends BaseDao<LShopUserGroup,String> {
    Pageable<LShopUserGroup> queryGoodsList(Map<String, Object> param, Pagination pagination);

    List<LShopUserGroup> listAll(Map<String, Object> param);
}
