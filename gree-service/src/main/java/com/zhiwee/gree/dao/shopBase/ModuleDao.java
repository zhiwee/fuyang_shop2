package com.zhiwee.gree.dao.shopBase;


import com.zhiwee.gree.model.Module;
import xyz.icrab.common.dao.BaseDao;

/**
 * @author gll
 * @since 2018/4/21 22:31
 */
public interface ModuleDao extends BaseDao<Module, String> {
}
