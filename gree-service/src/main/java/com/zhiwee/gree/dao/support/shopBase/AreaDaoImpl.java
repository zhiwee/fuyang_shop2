package com.zhiwee.gree.dao.support.shopBase;

import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.shopBase.AreaDao;
import com.zhiwee.gree.dao.support.shopBase.mapper.AreaMapper;
import com.zhiwee.gree.model.Area;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;


@Repository
public class AreaDaoImpl extends BaseDaoImpl<Area, String, AreaMapper> implements AreaDao {

    @Override
    public List<Area> ahtree(Map<String, Object> params) {
        return this.mapper.ahtree(params);
    }

    @Override
    public Pageable<Area> ahPage(Map<String, Object> params, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return convertToPageable(this.mapper.ahPage(params));
    }

    @Override
    public Pageable<Area> allProvince(Map<String, Object> params, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return convertToPageable(this.mapper.allProvince(params));
    }
}
