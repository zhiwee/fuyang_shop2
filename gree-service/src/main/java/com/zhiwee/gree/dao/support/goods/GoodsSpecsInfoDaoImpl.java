package com.zhiwee.gree.dao.support.goods;

import com.github.pagehelper.PageRowBounds;
import com.zhiwee.gree.dao.goods.GoodsSpecsInfoDao;
import com.zhiwee.gree.dao.support.goods.mapper.GoodsSpecsInfoMapper;
import com.zhiwee.gree.model.GoodsInfo.GoodsCategoryImg;
import com.zhiwee.gree.model.GoodsInfo.GoodsSpecsInfo;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

@Repository
public class GoodsSpecsInfoDaoImpl extends BaseDaoImpl<GoodsSpecsInfo,String, GoodsSpecsInfoMapper> implements GoodsSpecsInfoDao {
    @Override
    public Pageable<GoodsSpecsInfo> getPage(Map<String, Object> param, Pagination pagination) {
        PageRowBounds rowBounds = new PageRowBounds(pagination.getOffset(), pagination.getPageSize());
        rowBounds.setCount(true);
        List<GoodsSpecsInfo> data = this.mapper.getPage(param, rowBounds);
        return convertToPageable(rowBounds, data);
    }

    @Override
    public Pageable<GoodsSpecsInfo> getItemPage(Map<String, Object> param, Pagination pagination) {
        PageRowBounds rowBounds = new PageRowBounds(pagination.getOffset(), pagination.getPageSize());
        rowBounds.setCount(true);
        List<GoodsSpecsInfo> data = this.mapper.getItemPage(param, rowBounds);
        return convertToPageable(rowBounds, data);
    }
}
