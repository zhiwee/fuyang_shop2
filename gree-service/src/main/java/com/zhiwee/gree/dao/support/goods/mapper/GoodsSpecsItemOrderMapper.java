package com.zhiwee.gree.dao.support.goods.mapper;

import com.zhiwee.gree.model.GoodsInfo.GoodsSpecsItemOrder;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

public interface GoodsSpecsItemOrderMapper  extends BaseMapper<GoodsSpecsItemOrder> {
    List<GoodsSpecsItemOrder> getListBYGoodsId(Map<String, Object> param);
}
