package com.zhiwee.gree.dao.support.ShopUserAddree.mapper;


import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.ShopUserAddress.ShopUserAddress;
import xyz.icrab.common.dao.support.mapper.BaseMapper;
import xyz.icrab.common.web.param.IdsParam;

import java.util.List;
import java.util.Map;


public interface ShopUserAddressMapper extends BaseMapper<ShopUserAddress> {


}
