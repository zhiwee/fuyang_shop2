package com.zhiwee.gree.dao.support.FyGoods;

import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.FyGoods.FyOrderDao;
import com.zhiwee.gree.dao.support.FyGoods.mapper.FyOrderMapper;
import com.zhiwee.gree.model.FyGoods.FyOrder;
import com.zhiwee.gree.model.FyGoods.vo.FyOrderVo;
import com.zhiwee.gree.model.OrderInfo.GoodsNameTopTen;
import com.zhiwee.gree.model.OrderInfo.vo.OrderStatisticItem;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

@Repository
public class FyOrderDaoImpl extends BaseDaoImpl<FyOrder,String, FyOrderMapper> implements FyOrderDao {
    @Override
    public Pageable<FyOrder> queryOrderList(Map<String, Object> params, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return convertToPageable(this.mapper.queryOrderList(params));
    }

    @Override
    public void updateByOrderNum(Map<String, Object> params) {
        this.mapper.updateByOrderNum(params);
    }

    @Override
    public void deleteByOrderNum(Map<String, Object> params) {
          this.mapper.deleteByOrderNum(params);
    }

    @Override
    public void updateSelective(Map<String, Object> params) {
          this.mapper.updateSelective(params);
    }

    @Override
    public Pageable<FyOrder> refoundOrderList(Map<String, Object> param, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return convertToPageable(this.mapper.refoundOrderList(param));
    }

    @Override
    public List<FyOrder> queryPayOrderList(Map<String, Object> map) {
        return this.mapper.queryPayOrderList(map);
    }

    @Override
    public List<FyOrder> queryList(Map<String, Object> map) {
        return  this.mapper.queryList(map);
    }

    @Override
    public List<FyOrderVo> queryExportOrderList(Map<String, Object> params) {
        return this.mapper.queryExportOrderList(params);
    }
    //extends BaseDaoImpl<FyGoodsImg,String, FyGoodsImgMapper> implements FygoodsImgDao


    @Override
    public List<OrderStatisticItem> statisticAllAmountByDate(Map<String, Object> param) {
        return mapper.statisticAllAmountByDate(param);
    }

    @Override
    public List<GoodsNameTopTen>  qidoGoodsNameNumTopTen(Map<String, Object> param) {
        return mapper.qidoGoodsNameNumTopTen(param);
    }

    @Override
    public List<GoodsNameTopTen>  qidoGoodsNameMoneyTopTen(Map<String, Object> param) {
        return mapper.qidoGoodsNameMoneyTopTen(param);
    }

    @Override
    public Object orderAmount(Map<String, Object> params) {
        return mapper.orderAmount(params);
    }

    @Override
    public Double totalPrice(Map<String, Object> param) {
        return mapper.totalPrice(param);
    }

    @Override
    public List<FyOrder> queryPayOrderList2(Map<String, Object> map) {
        return mapper.queryPayOrderList2(map);
    }
}
