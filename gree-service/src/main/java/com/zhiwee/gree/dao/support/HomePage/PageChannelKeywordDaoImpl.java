package com.zhiwee.gree.dao.support.HomePage;

import com.github.pagehelper.PageRowBounds;
import com.zhiwee.gree.dao.HomePage.PageChannelKeywordDao;
import com.zhiwee.gree.dao.support.HomePage.mapper.PageChannelKeywordMapper;
import com.zhiwee.gree.model.HomePage.PageChannelAdimg;
import com.zhiwee.gree.model.HomePage.PageChannelKeyword;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

@Repository
public class PageChannelKeywordDaoImpl extends BaseDaoImpl<PageChannelKeyword,String, PageChannelKeywordMapper> implements PageChannelKeywordDao {
    @Override
    public Pageable<PageChannelKeyword> getPage(Map<String, Object> param, Pagination pagination) {
        PageRowBounds rowBounds = new PageRowBounds(pagination.getOffset(), pagination.getPageSize());
        rowBounds.setCount(true);
        List<PageChannelKeyword> data = this.mapper.getPage(param, rowBounds);
        return convertToPageable(rowBounds, data);
    }
}
