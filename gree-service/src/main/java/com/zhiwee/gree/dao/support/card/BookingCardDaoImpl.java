package com.zhiwee.gree.dao.support.card;

import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.crad.BookingCardDao;
import com.zhiwee.gree.dao.support.card.mapper.BookingCardMapper;
import com.zhiwee.gree.model.card.BookingCard;
import com.zhiwee.gree.model.card.CardInfo;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.Map;

@Repository
public class BookingCardDaoImpl extends BaseDaoImpl<BookingCard, String, BookingCardMapper> implements BookingCardDao {
    @Override
    public Pageable<BookingCard> pageInfo(Map<String, Object> params, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return convertToPageable(this.mapper.pageInfo(params));
    }


    @Override
    public BookingCard getInfo(BookingCard cardInfo) {
        return mapper.getInfo(cardInfo);
    }

}
