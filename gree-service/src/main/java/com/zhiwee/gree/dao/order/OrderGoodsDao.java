package com.zhiwee.gree.dao.order;

import com.zhiwee.gree.model.order.OrderGoods;
import xyz.icrab.common.dao.BaseDao;

public interface OrderGoodsDao extends BaseDao<OrderGoods,String> {
    // extends BaseDao<BaseOrder,String>
}
