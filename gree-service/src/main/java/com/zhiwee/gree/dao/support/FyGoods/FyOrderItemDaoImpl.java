package com.zhiwee.gree.dao.support.FyGoods;

import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.FyGoods.FyOrderItemDao;
import com.zhiwee.gree.dao.support.FyGoods.mapper.FyOrderItemMapper;
import com.zhiwee.gree.model.FyGoods.FyOrderItem;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.Map;

@Repository
public class FyOrderItemDaoImpl
        extends BaseDaoImpl<FyOrderItem,String, FyOrderItemMapper> implements FyOrderItemDao {
    @Override
    public Pageable<FyOrderItem> queryOrderItemByNum(Map<String, Object> param, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return convertToPageable(this.mapper.queryOrderItemByNum(param));
    }

    @Override
    public void deleteByGoodsId(Map<String, Object> map) {
            this.mapper.deleteByGoodsId(map);
    }

    @Override
    public void deleteAll(Map<String, Object> params) {
        mapper.deleteAll(params);
    }
}
