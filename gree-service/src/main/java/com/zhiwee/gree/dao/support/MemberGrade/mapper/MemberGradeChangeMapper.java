package com.zhiwee.gree.dao.support.MemberGrade.mapper;


import com.zhiwee.gree.model.MemberGrade.MemberGrade;
import com.zhiwee.gree.model.MemberGrade.MemberGradeChange;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;


public interface MemberGradeChangeMapper extends BaseMapper<MemberGradeChange> {


    List<MemberGradeChange> searchAll(Map<String,Object> params);
}
