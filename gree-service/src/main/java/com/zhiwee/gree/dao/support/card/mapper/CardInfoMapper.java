package com.zhiwee.gree.dao.support.card.mapper;

import com.zhiwee.gree.model.card.CardInfo;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

public interface CardInfoMapper extends BaseMapper<CardInfo> {
    List<CardInfo> pageInfo(Map<String, Object> params);

    CardInfo getInfo(CardInfo cardInfo);

}
