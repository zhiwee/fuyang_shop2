package com.zhiwee.gree.dao.Dept;

import com.zhiwee.gree.model.DeptInfo.DeptInfo;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.Map;


/**
 * Created by jick on 2019/8/26.
 */
public interface DeptInfoDao  extends BaseDao<DeptInfo,String> {
    // DeptopenInfoDao  extends BaseDao<DeptopenInfo,String>
       //List<DeptInfo> queryRetailUser();
       // Pageable<DeptopenInfo> queryDeptOpenInfo(Map<String,Object> param, Pagination pagination);
      Pageable<DeptInfo> pageInfo(Map<String,Object> param, Pagination pagination);
}
