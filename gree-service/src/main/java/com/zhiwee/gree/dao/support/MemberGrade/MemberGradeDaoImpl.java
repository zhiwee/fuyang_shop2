package com.zhiwee.gree.dao.support.MemberGrade;


import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.MemberGrade.MemberGradeDao;
import com.zhiwee.gree.dao.goods.GoodsInfoDao;
import com.zhiwee.gree.dao.support.MemberGrade.mapper.MemberGradeMapper;
import com.zhiwee.gree.dao.support.goods.mapper.GoodsInfoMapper;
import com.zhiwee.gree.model.GoodsInfo.GoodsInfo;
import com.zhiwee.gree.model.GoodsInfo.SolrGoodsInfo;
import com.zhiwee.gree.model.MemberGrade.MemberGrade;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;


@Repository
public class MemberGradeDaoImpl extends BaseDaoImpl<MemberGrade, String, MemberGradeMapper> implements MemberGradeDao {


    @Override
    public Pageable<MemberGrade> searchAll(Map<String, Object> params, Pagination pagination) {

        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return convertToPageable(this.mapper.searchAll(params));
    }

    @Override
    public MemberGrade selectMax(Map<String, Object> params) {
        return mapper.selectMax(params);
    }


}
