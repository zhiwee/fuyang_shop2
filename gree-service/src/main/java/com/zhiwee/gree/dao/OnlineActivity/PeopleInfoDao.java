package com.zhiwee.gree.dao.OnlineActivity;

import com.zhiwee.gree.model.GoodsInfo.GoodVo.PeopleExr;
import com.zhiwee.gree.model.GoodsInfo.LAPeople;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

public interface PeopleInfoDao  extends BaseDao<LAPeople, String> {
    Pageable<LAPeople> nghPeople(Map<String, Object> params, Pagination pagination);

    List<LAPeople> queryInfo(Map<String, Object> params);

    void upPeople(Map<String, Object> params);

    List<PeopleExr> exportPeople(Map<String, Object> params);

    void updateInfo(Map<String, Object> params);
}
