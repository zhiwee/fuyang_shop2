package com.zhiwee.gree.dao.AirCondition;


import com.zhiwee.gree.model.AchieveGift.GiftInfo;
import com.zhiwee.gree.model.AirCondition.AirCondition;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.Map;

public interface AirConditionDao extends BaseDao<AirCondition, String> {


    Pageable<AirCondition> pageInfo(Map<String, Object> params, Pagination pagination);
}
