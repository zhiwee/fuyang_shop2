package com.zhiwee.gree.dao.support.AchieveGift.mapper;



import com.zhiwee.gree.model.AchieveGift.AchieveGift;
import com.zhiwee.gree.model.AchieveGift.GiftInfo;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;


public interface GiftInfoMapper extends BaseMapper<GiftInfo> {

    List<GiftInfo> pageInfo(Map<String, Object> params);
}
