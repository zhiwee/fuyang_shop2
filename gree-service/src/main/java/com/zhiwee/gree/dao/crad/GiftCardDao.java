package com.zhiwee.gree.dao.crad;

import com.zhiwee.gree.model.card.GiftCard;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.Map;


public interface GiftCardDao extends BaseDao<GiftCard,String> {
    Pageable< GiftCard> pageInfo(Map<String, Object> params, Pagination pagination);

    GiftCard getInfo( GiftCard cardInfo);
}
