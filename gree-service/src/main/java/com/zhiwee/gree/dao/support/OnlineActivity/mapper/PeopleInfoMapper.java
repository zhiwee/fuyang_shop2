package com.zhiwee.gree.dao.support.OnlineActivity.mapper;

import com.zhiwee.gree.model.GoodsInfo.GoodVo.PeopleExr;
import com.zhiwee.gree.model.GoodsInfo.LAPeople;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

public interface PeopleInfoMapper extends BaseMapper<LAPeople> {
    List<LAPeople> nghPeople(Map<String, Object> params);

    List<LAPeople> queryInfo(Map<String, Object> params);

    void upPeople(Map<String, Object> params);

    List<PeopleExr> exportPeople(Map<String, Object> params);

    void updateInfo(Map<String, Object> params);
}
