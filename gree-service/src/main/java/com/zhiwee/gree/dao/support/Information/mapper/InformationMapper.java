package com.zhiwee.gree.dao.support.Information.mapper;

import com.zhiwee.gree.model.Information.Information;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * Created by jick on 2019/7/18.
 */
public interface InformationMapper  extends BaseMapper<Information>{
    // extends BaseMapper<Invoice>
    //
    List<Information> queryInfoList(Map<String,Object> param);
}
