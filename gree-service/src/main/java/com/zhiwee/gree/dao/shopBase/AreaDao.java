package com.zhiwee.gree.dao.shopBase;


import com.zhiwee.gree.model.Area;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

public interface AreaDao extends BaseDao<Area,String> {

    List<Area> ahtree(Map<String,Object> params);

    Pageable<Area> ahPage(Map<String,Object> params, Pagination pagination);

    Pageable<Area> allProvince(Map<String,Object> params, Pagination pagination);
}
