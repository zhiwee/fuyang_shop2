package com.zhiwee.gree.dao.support.ShopDistributor;

import com.github.pagehelper.PageRowBounds;
import com.zhiwee.gree.dao.ShopDistributor.DistributorAreaDao;
import com.zhiwee.gree.dao.ShopDistributor.DistributorCategoryDao;
import com.zhiwee.gree.dao.support.ShopDistributor.mapper.DistributorAreaMapper;
import com.zhiwee.gree.dao.support.ShopDistributor.mapper.DistributorCategoryMapper;
import com.zhiwee.gree.model.ShopDistributor.DistributorArea;
import com.zhiwee.gree.model.ShopDistributor.DistributorCategory;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;


@Repository
public class DistributorCategoryDaoImpl extends BaseDaoImpl<DistributorCategory, String, DistributorCategoryMapper> implements DistributorCategoryDao {


    @Override
    public Pageable<DistributorCategory> achieveSmallCategory(Map<String, Object> param, Pagination pagination) {
        PageRowBounds rowBounds = new PageRowBounds(pagination.getOffset(), pagination.getPageSize());
        rowBounds.setCount(true);
        List<DistributorCategory> data = this.mapper.achieveSmallCategory(param, rowBounds);
        return convertToPageable(rowBounds, data);
    }
}
