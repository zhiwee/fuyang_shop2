package com.zhiwee.gree.dao.support.GoodsLabel;


import com.zhiwee.gree.dao.GoodsLabel.BpDao;
import com.zhiwee.gree.dao.GoodsLabel.GbDao;
import com.zhiwee.gree.dao.support.GoodsLabel.mapper.BpMapper;
import com.zhiwee.gree.dao.support.GoodsLabel.mapper.GbMapper;
import com.zhiwee.gree.model.GoodsLabel.Bp;
import com.zhiwee.gree.model.GoodsLabel.Gb;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;


@Repository
public class GbDaoImpl extends BaseDaoImpl<Gb, String, GbMapper> implements GbDao {



}
