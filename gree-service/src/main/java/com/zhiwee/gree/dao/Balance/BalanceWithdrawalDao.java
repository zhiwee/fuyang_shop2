package com.zhiwee.gree.dao.Balance;


import com.zhiwee.gree.model.Balance.BalanceReCharge;
import com.zhiwee.gree.model.Balance.BalanceWithdrawal;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

public interface BalanceWithdrawalDao extends BaseDao<BalanceWithdrawal, String> {


    Pageable<BalanceWithdrawal> pageInfo(Map<String, Object> param, Pagination pagination);


    List<BalanceWithdrawal> balanceRecord(Map<String,Object> params);
}
