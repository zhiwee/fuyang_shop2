package com.zhiwee.gree.dao.FyGoods;

import com.zhiwee.gree.model.FyGoods.Fygoods;
import com.zhiwee.gree.model.FyGoods.FygoodsExo;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

public interface FygoodsDao extends BaseDao<Fygoods,String> {
    // extends BaseDao<GoodCollect,String>
    Pageable<Fygoods> queryGoodsList(Map<String,Object> param, Pagination pagination);
    // Pageable<Invoice> queryBaseInvoice(Map<String,Object> param,Pagination pagination);
    List<Fygoods>   portalQueryGoodsList(Map<String,Object> param);

    List<Fygoods> queryList(Map<String, Object> param);

    Fygoods queryInfoListById(Map<String, Object> param);

    List<FygoodsExo> listall();
}
