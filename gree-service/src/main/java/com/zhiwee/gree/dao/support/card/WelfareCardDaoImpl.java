package com.zhiwee.gree.dao.support.card;

import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.crad.BookingCardsDao;
import com.zhiwee.gree.dao.crad.WelfareCardDao;
import com.zhiwee.gree.dao.support.card.mapper.BookingCardsMapper;
import com.zhiwee.gree.dao.support.card.mapper.WelfareCardMapper;
import com.zhiwee.gree.model.card.BookingCards;
import com.zhiwee.gree.model.card.BookingCardsExrt;
import com.zhiwee.gree.model.card.WelfareCard;
import com.zhiwee.gree.model.card.vo.WelfareCardExrt;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;


@Repository
public class WelfareCardDaoImpl extends BaseDaoImpl<WelfareCard, String, WelfareCardMapper> implements WelfareCardDao {


    @Override
    public Pageable<WelfareCard> pageInfo(Map<String, Object> params, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return convertToPageable(this.mapper.pageInfo(params));
    }

    @Override
    public List<WelfareCardExrt> listAll(Map<String, Object> params) {
        return mapper.listAll(params);
    }
}
