package com.zhiwee.gree.dao.support.HomePage;

import com.github.pagehelper.PageRowBounds;
import com.zhiwee.gree.dao.HomePage.PcPlanmapDao;
import com.zhiwee.gree.dao.support.HomePage.mapper.PcPlanmapMapper;
import com.zhiwee.gree.model.HomePage.PcPlanmap;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Page;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

@Repository
public class PcPlanmapDaoImpl extends BaseDaoImpl<PcPlanmap,String, PcPlanmapMapper> implements PcPlanmapDao {
    @Override
    public Pageable<PcPlanmap> getPage(Map<String, Object> params, Pagination pagination) {
        PageRowBounds rowBounds = new PageRowBounds(pagination.getOffset(), pagination.getPageSize());
        rowBounds.setCount(true);
        List<PcPlanmap> data = this.mapper.getPage(params, rowBounds);
         return convertToPageable(rowBounds, data);
    }

    @Override
    public PcPlanmap selectById(String id) {
        return this.mapper.getOneByid(id);
    }

    @Override
    public List<PcPlanmap> getList(Map<String, Object> params) {
        return this.mapper.getList(params);
    }
}
