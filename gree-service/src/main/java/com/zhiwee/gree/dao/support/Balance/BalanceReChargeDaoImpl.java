package com.zhiwee.gree.dao.support.Balance;


import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.Balance.BalanceReChargeDao;
import com.zhiwee.gree.dao.BaseInfo.BaseInfoDao;
import com.zhiwee.gree.dao.support.Balance.mapper.BalanceReChargeMapper;
import com.zhiwee.gree.dao.support.BaseInfo.mapper.BaseInfoMapper;
import com.zhiwee.gree.model.Balance.BalanceReCharge;
import com.zhiwee.gree.model.BaseInfo.BaseInfo;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;


@Repository
public class BalanceReChargeDaoImpl extends BaseDaoImpl<BalanceReCharge, String, BalanceReChargeMapper> implements BalanceReChargeDao {



    @Override
    public Pageable<BalanceReCharge> pageInfo(Map<String, Object> param, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum() + 1, pagination.getPageSize());
        List<BalanceReCharge> baseInfos = mapper.pageInfo(param);
        return convertToPageable(baseInfos);
    }

    @Override
    public List<BalanceReCharge> balanceRecord(Map<String, Object> params) {
        return this.mapper.balanceRecord(params);
    }


}
