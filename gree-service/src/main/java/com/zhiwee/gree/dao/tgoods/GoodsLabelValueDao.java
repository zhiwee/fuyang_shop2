package com.zhiwee.gree.dao.tgoods;

import com.zhiwee.gree.model.FyGoods.GoodsLabelValue;
import xyz.icrab.common.dao.BaseDao;

/**
 * @author DELL
 */
public interface GoodsLabelValueDao extends BaseDao<GoodsLabelValue,String> {

}