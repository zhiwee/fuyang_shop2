package com.zhiwee.gree.dao.support.ShopDistributor.mapper;


import com.github.pagehelper.PageRowBounds;
import com.zhiwee.gree.model.ShopDistributor.DistributorArea;
import com.zhiwee.gree.model.ShopDistributor.ShopDistributor;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;


public interface DistributorAreaMapper extends BaseMapper<DistributorArea> {

    List<DistributorArea> pageArea(Map<String,Object> param, PageRowBounds rowBounds);

    List<DistributorArea> searchByCategory(Map<String,Object> params);
}
