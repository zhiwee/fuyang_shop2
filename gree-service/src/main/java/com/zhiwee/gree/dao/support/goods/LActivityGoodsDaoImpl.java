package com.zhiwee.gree.dao.support.goods;


import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.goods.LActivityGoodsDao;
import com.zhiwee.gree.dao.support.goods.mapper.LActivityGoodsMapper;
import com.zhiwee.gree.model.GoodsInfo.LAGoodsInfo;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

@Repository
public class LActivityGoodsDaoImpl extends BaseDaoImpl<LAGoodsInfo,String, LActivityGoodsMapper> implements LActivityGoodsDao {
    @Override
    public Pageable<LAGoodsInfo> secKillGoods(Map<String, Object> params, Pagination pagination) {
      //  return null;
        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return     convertToPageable(this.mapper.secKillGoods(params));
    }

    @Override
    public LAGoodsInfo queryKillInfoById(Map<String, Object> param) {
        return   this.mapper.queryKillInfoById(param);
    }

    @Override
    public List<LAGoodsInfo> secKillGoods2(Map<String, Object> params) {
        return this.mapper.secKillGoods2(params);
    }


}
