package com.zhiwee.gree.dao.support.ShopUser.mapper;

import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.ShopUser.ShopUserSignLog;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.Map;

/**
 * TODO
 *
 * @author Chensong
 * @Company http://www.zhiwee.com
 * date 2020-02-10
 */
public interface ShopUserSignLogMapper  extends BaseMapper<ShopUserSignLog> {
    Integer signAmount(Map<String,Object> params);
    ShopUserSignLog getOneLog(Map<String,Object> params);
}
