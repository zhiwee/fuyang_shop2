package com.zhiwee.gree.dao.support.goods;

import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.goods.GoodsDao;
import com.zhiwee.gree.dao.support.goods.mapper.GoodsMapper;
import com.zhiwee.gree.model.FyGoods.FygoodsExo;
import com.zhiwee.gree.model.GoodsInfo.GoodVo.GoodsExo;
import com.zhiwee.gree.model.GoodsInfo.GoodVo.GoodsFyExo;
import com.zhiwee.gree.model.GoodsInfo.GoodVo.GoodsInfoVO;
import com.zhiwee.gree.model.GoodsInfo.Goods;
import com.zhiwee.gree.model.GoodsInfo.ProductAttribute;
import com.zhiwee.gree.model.OnlineActivity.vo.OnlineActivityInfoVo;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

/**
 * Created by jick on 2019/8/30.
 */
@Repository
public class GoodsDaoImpl extends BaseDaoImpl<Goods,String,GoodsMapper> implements GoodsDao {
    @Override
    public Pageable<Goods> queryInfoList(Map<String, Object> params, Pagination pagination) {
      //  return null;
        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return     convertToPageable(this.mapper.queryInfoList(params));
    }

    @Override
    public Goods queryInfoListById(Map<String, Object> param) {
        //  return mapper.achieveGoodsByName(param);
        return mapper.queryInfoListById(param);
    }

   /* @Override
    public Pageable<SecKillGoods> secKillGoods(Map<String, Object> params, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return  convertToPageable(this.mapper.secKillGoods(params));
        return   null;
    }*/
    //extends BaseDaoImpl<GoodsDetail, String, GoodsDetailMapper> implements GoodsDetailDao {
    //  PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
   //     return     convertToPageable(this.mapper.queryList(params));




    @Override
    public void upSecKillGoods(Map<String, Object> params) {
         mapper.upSecKillGoods(params);
    }

    public void upSecKillPeople(Map<String, Object> params) {
        mapper.upSecKillPeople(params);
    }

    @Override
    public Goods getInfo(Map<String, Object> params) {
        return         mapper.getInfo(params);
    }

    @Override
    public List<OnlineActivityInfoVo> getCurrentSceKillGods(Map<String, Object> params) {
        return   mapper.getCurrentSceKillGods(params);
    }

    @Override
    public GoodsInfoVO getOneByGoods(Map<String, Object> params) {
        return this.mapper.getOneByGoods(params);
    }


    @Override
    public List<ProductAttribute> querySpecsById(Map<String, Object> param) {
        return   this.mapper.querySpecsById(param);
    }

    @Override
    public List<ProductAttribute> queryAttrById(Map<String, Object> param) {
        return    this.mapper.queryAttrById(param);
    }

    @Override
    public List<Goods> listAll(Map<String, Object> params) {
        return mapper.listAll(params);
    }

    @Override
    public List<GoodsExo> listExo() {
        return mapper.listExo();
    }

    @Override
    public List<GoodsFyExo> listExo2() {
        return mapper.listExo2();
    }

    @Override
    public List<FygoodsExo> listall() {
        return mapper.listall();
    }

    @Override
    public  List<Goods>  getCount(String id) {
        return mapper.getCount(id);
    }
}
