package com.zhiwee.gree.dao.shopBase;

import java.util.List;
import java.util.Map;

import com.zhiwee.gree.model.Interface;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Relation;

/**
 * @author gll
 * @since 2018/4/21 22:31
 */
public interface InterfaceDao extends BaseDao<Interface, String> {
	
	/*
	 * 用户添加接口数据
	 */
	int saveUserInterface(List<Relation> list);
	/*
	 * 角色添加接口数据
	 */
	int saveRoleInterface(List<Relation> list);
	/*
	 * 根据userid获取接口
	 */
	List<Interface> getIntByUserId(String userid);
	/*
	 * 根据roleid获取接口
	 */
	List<Interface> getIntByRoleIds(String... roleids);
	

	/*
	 * 根据模块roleid删除当前登录人拥有的角色所关联的角色接口关系数据
	 */
	int delRoleIntByIds(Map<String, Object> map);
	/*
	 * 根据模块userid删除删除当前登录人拥有的角色所关联的角色接口关系数据
	 */
	int delUserIntByIds(Map<String, Object> map);
}
