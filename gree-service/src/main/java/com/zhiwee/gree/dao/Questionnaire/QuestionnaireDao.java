package com.zhiwee.gree.dao.Questionnaire;

import com.zhiwee.gree.model.Questionnaire.Questionnaire;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.Map;

public interface QuestionnaireDao extends BaseDao<Questionnaire, String> {
    Pageable<Questionnaire> pageInfo(Map<String, Object> params, Pagination pagination);
}
