package com.zhiwee.gree.dao.support.HomePage.mapper;

import com.github.pagehelper.PageRowBounds;
import com.zhiwee.gree.model.HomePage.PageSearch;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

public interface PageSearchMapper extends BaseMapper<PageSearch> {
    List<PageSearch> getPage(Map<String, Object> param, PageRowBounds rowBounds);

    PageSearch getById(String id);

    List<PageSearch> getList();
}
