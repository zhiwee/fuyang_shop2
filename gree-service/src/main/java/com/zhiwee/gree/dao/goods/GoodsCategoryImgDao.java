package com.zhiwee.gree.dao.goods;

import com.zhiwee.gree.model.GoodsInfo.GoodsCategoryImg;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.Map;

public interface GoodsCategoryImgDao extends BaseDao<GoodsCategoryImg,String> {
    Pageable<GoodsCategoryImg> getPage(Map<String, Object> param, Pagination pagination);

    GoodsCategoryImg selectById(String id);

    GoodsCategoryImg getOneByCategoryId(String categoryId);
}
