package com.zhiwee.gree.dao.support.FyGoods.mapper;


import com.zhiwee.gree.model.FyGoods.GrouponGoods;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;


public interface GrouponGoodsMapper extends BaseMapper<GrouponGoods> {
    List<GrouponGoods> queryGoodsList(Map<String, Object> param);
}