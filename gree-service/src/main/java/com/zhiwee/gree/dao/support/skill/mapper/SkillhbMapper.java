package com.zhiwee.gree.dao.support.skill.mapper;

import com.zhiwee.gree.model.skill.Skillhb;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

public interface SkillhbMapper extends BaseMapper<Skillhb> {
    //extends BaseMapper<CommentPicture>
    // List<ShopUserComment> achieveComment(Map<String, Object> params);
    List<Skillhb> queryList(Map<String, Object> params);

    List<Skillhb> queryHbByTime(Map<String,Object> params);


}