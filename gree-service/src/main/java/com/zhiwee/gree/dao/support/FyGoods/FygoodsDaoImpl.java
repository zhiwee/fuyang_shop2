package com.zhiwee.gree.dao.support.FyGoods;

import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.FyGoods.FygoodsDao;
import com.zhiwee.gree.dao.support.FyGoods.mapper.FygoodsMapper;
import com.zhiwee.gree.model.FyGoods.Fygoods;
import com.zhiwee.gree.model.FyGoods.FygoodsExo;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;


@Repository
public class FygoodsDaoImpl
        extends BaseDaoImpl<Fygoods, String, FygoodsMapper> implements FygoodsDao {
    @Override
    public Pageable<Fygoods> queryGoodsList(Map<String, Object> param, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return convertToPageable(this.mapper.queryGoodsList(param));
    }

    @Override
    public List<Fygoods> portalQueryGoodsList(Map<String, Object> param) {
        return  this.mapper.portalQueryGoodsList(param);
    }

    @Override
    public List<Fygoods> queryList(Map<String, Object> param) {
        return this.mapper.queryList(param);
    }

    @Override
    public Fygoods queryInfoListById(Map<String, Object> param) {
        return mapper.queryInfoListById(param);
    }

    @Override
    public List<FygoodsExo> listall() {
        return mapper.listall();
    }
}
