package com.zhiwee.gree.dao.support.draw;

import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.draw.DrawUseInfoDao;
import com.zhiwee.gree.dao.support.draw.mapper.DrawUseInfoMapper;
import com.zhiwee.gree.model.draw.DrawUseInfo;
import com.zhiwee.gree.model.draw.DrawUseInfoExrt;
import xyz.icrab.common.model.Pagination;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;

import java.util.List;
import java.util.Map;


@Repository
public class DrawUseInfoDaoImpl extends BaseDaoImpl<DrawUseInfo, String, DrawUseInfoMapper> implements DrawUseInfoDao {

    @Override
    public Pageable<DrawUseInfo> pageInfo(Map<String, Object> params, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return convertToPageable(this.mapper.pageInfo(params));
    }

    @Override
    public List<DrawUseInfoExrt> queryDrawUseInfoExport(Map<String, Object> params) {
        return this.mapper.queryDrawUseInfoExport(params);
    }
}
