package com.zhiwee.gree.dao.ShopDistributor;


import com.zhiwee.gree.model.ShopDistributor.DistributorArea;
import com.zhiwee.gree.model.ShopDistributor.DistributorCategory;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

public interface DistributorCategoryDao extends BaseDao<DistributorCategory,String> {


    Pageable<DistributorCategory> achieveSmallCategory(Map<String,Object> param, Pagination pagination);
}
