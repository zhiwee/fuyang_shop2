package com.zhiwee.gree.dao.Dealer;



import com.zhiwee.gree.model.Dealer.DealerInfoBase;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

public interface DealerInfoBaseDao extends BaseDao<DealerInfoBase,String> {

    List<DealerInfoBase> simpleList(Map<String, Object> param);

    Pageable<DealerInfoBase> infoPage(Map<String,Object> param, Pagination pagination);
}
