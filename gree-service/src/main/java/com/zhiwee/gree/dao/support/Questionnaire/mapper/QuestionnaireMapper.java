package com.zhiwee.gree.dao.support.Questionnaire.mapper;

import com.zhiwee.gree.model.Questionnaire.Questionnaire;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

public interface QuestionnaireMapper extends BaseMapper<Questionnaire> {
    List<Questionnaire> pageInfo(Map<String, Object> params);
}
