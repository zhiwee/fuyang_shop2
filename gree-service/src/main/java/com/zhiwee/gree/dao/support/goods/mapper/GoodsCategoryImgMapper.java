package com.zhiwee.gree.dao.support.goods.mapper;

import com.github.pagehelper.PageRowBounds;
import com.zhiwee.gree.model.GoodsInfo.GoodsCategoryImg;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

public interface GoodsCategoryImgMapper extends BaseMapper<GoodsCategoryImg> {

    List<GoodsCategoryImg> getPage(Map<String, Object> param, PageRowBounds rowBounds);

    GoodsCategoryImg selectById(String id);

    GoodsCategoryImg getOneByCategoryId(String categoryId);
}
