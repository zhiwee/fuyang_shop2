package com.zhiwee.gree.dao.support.goods;

import com.github.pagehelper.PageRowBounds;
import com.zhiwee.gree.dao.goods.GoodsClassifyDao;
import com.zhiwee.gree.dao.support.goods.mapper.GoodsClassifyMapper;
import com.zhiwee.gree.model.GoodsInfo.GoodsClassify;

import com.zhiwee.gree.model.GoodsLabel.*;
import com.zhiwee.gree.model.ShopDistributor.CategoryVo;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;


@Repository
public class GoodsClassifyDaoImpl extends BaseDaoImpl<GoodsClassify, String, GoodsClassifyMapper> implements GoodsClassifyDao {


    @Override
    public List<Ps> getPsList(Map<String, Object> param) {
        return this.mapper.getPsList(param);
    }

    @Override
    public  List<Space> getSpaceList(Map<String, Object> param) {
        return this.mapper.getSpaceList(param);
    }


    @Override
    public List<Gb> getGbList(Map<String, Object> param) {
        return this.mapper.getGbList(param);
    }

    @Override
    public  List<Bp> getBpsList(Map<String, Object> param) {
        return this.mapper.getBpsList(param);
    }

    @Override
    public List<Efficiency> getEfficienciesList(Map<String, Object> param) {
        return this.mapper.getEfficienciesList(param);
    }

    @Override
    public Pageable<GoodsClassify> achieveSmallCategory(Map<String, Object> param, Pagination pagination) {
        PageRowBounds rowBounds = new PageRowBounds(pagination.getOffset(), pagination.getPageSize());
        rowBounds.setCount(true);
        List<GoodsClassify> data = this.mapper.achieveSmallCategory(param, rowBounds);
        return convertToPageable(rowBounds, data);
    }

    @Override
    public Pageable<GoodsClassify> smallCategory(Map<String, Object> param, Pagination pagination) {
        PageRowBounds rowBounds = new PageRowBounds(pagination.getOffset(), pagination.getPageSize());
        rowBounds.setCount(true);
        List<GoodsClassify> data = this.mapper.smallCategory(param, rowBounds);
        return convertToPageable(rowBounds, data);
    }

    @Override
    public List<GoodsClassify> queryListByRoot(Map<String, Object> param) {
        List<GoodsClassify>  list =  this.mapper.queryListByRoot(param);
        return list;
    }


    @Override
    public List<GoodsClassify> queryListById(Map<String ,Object> param) {
        return  this.mapper.queryListById(param);
    }

    @Override
    public Pageable<GoodsClassify> smallGoodsClassify(Map<String, Object> param, Pagination pagination) {
        PageRowBounds rowBounds = new PageRowBounds(pagination.getOffset(), pagination.getPageSize());
        rowBounds.setCount(true);
        List<GoodsClassify> data = this.mapper.smallGoodsClassify(param, rowBounds);
        return convertToPageable(rowBounds, data);
    }

    @Override
    public List<CategoryVo> getCategory(Map<String, Object> param) {
        List<CategoryVo> list = this.mapper.getCategory(param);
        return list;
    }

}
