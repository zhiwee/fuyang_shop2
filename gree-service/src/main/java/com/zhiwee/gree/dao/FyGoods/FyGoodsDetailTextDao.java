package com.zhiwee.gree.dao.FyGoods;

import com.zhiwee.gree.model.FyGoods.FyGoodsDetailText;
import xyz.icrab.common.dao.BaseDao;

public interface FyGoodsDetailTextDao extends BaseDao<FyGoodsDetailText,String> {
}
