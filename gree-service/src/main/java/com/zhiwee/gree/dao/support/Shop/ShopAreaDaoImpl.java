package com.zhiwee.gree.dao.support.Shop;

import com.zhiwee.gree.dao.Shop.ShopAreaDao;
import com.zhiwee.gree.dao.support.Shop.mapper.ShopAreaMapper;
import com.zhiwee.gree.model.Shop.ShopArea;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;

/**
 * @author jiake
 * @date 2020-02-19 12:27
 */
@Repository
public class ShopAreaDaoImpl extends BaseDaoImpl<ShopArea,Integer, ShopAreaMapper> implements ShopAreaDao {
    //extends BaseDaoImpl<Area, String, AreaMapper> implements AreaDao
}
