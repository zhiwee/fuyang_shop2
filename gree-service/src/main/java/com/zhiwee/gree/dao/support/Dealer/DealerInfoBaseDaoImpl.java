package com.zhiwee.gree.dao.support.Dealer;

import com.github.pagehelper.PageRowBounds;
import com.zhiwee.gree.dao.Dealer.DealerInfoBaseDao;
import com.zhiwee.gree.dao.support.Dealer.mapper.DealerInfoBaseMapper;

import com.zhiwee.gree.model.Dealer.DealerInfoBase;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.entity.Example;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;


@Repository
public class DealerInfoBaseDaoImpl extends BaseDaoImpl<DealerInfoBase, String, DealerInfoBaseMapper> implements DealerInfoBaseDao {


    @Override
    public List<DealerInfoBase> simpleList(Map<String, Object> param) {
        Example example = notEmptyOf(param);
        example.selectProperties("id", "name", "parentId", "typeCode");
        return mapper.selectByExample(example);
    }
    @Override
    public Pageable<DealerInfoBase> infoPage(Map<String, Object> param, Pagination pagination) {
        PageRowBounds rowBounds = new PageRowBounds(pagination.getOffset(), pagination.getPageSize());
        rowBounds.setCount(true);
        List<DealerInfoBase> data = this.mapper.infoPage(param, rowBounds);
        return convertToPageable(rowBounds, data);
    }


}
