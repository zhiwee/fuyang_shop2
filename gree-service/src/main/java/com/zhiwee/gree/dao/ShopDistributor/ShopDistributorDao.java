package com.zhiwee.gree.dao.ShopDistributor;


import com.zhiwee.gree.model.Dealer.DealerInfo;
import com.zhiwee.gree.model.ShopDistributor.ShopDistributor;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

public interface ShopDistributorDao extends BaseDao<ShopDistributor,String> {


    Pageable<ShopDistributor> infoPage(Map<String, Object> param, Pagination pagination);


    ShopDistributor defaultDistributor(Map<String,Object> mapkey);
}
