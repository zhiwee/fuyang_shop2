package com.zhiwee.gree.dao.support.HomePage;

import com.github.pagehelper.PageRowBounds;
import com.zhiwee.gree.dao.HomePage.PageChannelAdimgDao;
import com.zhiwee.gree.dao.support.HomePage.mapper.PageChannelAdimgMapper;
import com.zhiwee.gree.model.HomePage.PageChannel;
import com.zhiwee.gree.model.HomePage.PageChannelAdimg;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

@Repository
public class PageChannelAdimgDaoImpl extends BaseDaoImpl<PageChannelAdimg,String, PageChannelAdimgMapper>  implements PageChannelAdimgDao {
    @Override
    public Pageable<PageChannelAdimg> getPage(Map<String, Object> param, Pagination pagination) {
        PageRowBounds rowBounds = new PageRowBounds(pagination.getOffset(), pagination.getPageSize());
        rowBounds.setCount(true);
        List<PageChannelAdimg> data = this.mapper.getPage(param, rowBounds);
        return convertToPageable(rowBounds, data);
    }
}
