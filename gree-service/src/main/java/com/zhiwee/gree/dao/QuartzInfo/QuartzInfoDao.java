package com.zhiwee.gree.dao.QuartzInfo;


import com.zhiwee.gree.model.Partement.Partement;
import com.zhiwee.gree.model.Quartz.QuartzInfo;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.Map;

public interface QuartzInfoDao extends BaseDao<QuartzInfo, String> {


    Pageable<QuartzInfo> pageInfo(Map<String, Object> params, Pagination pagination);
}
