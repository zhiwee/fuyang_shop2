package com.zhiwee.gree.dao.support.shopBase;

import com.zhiwee.gree.dao.shopBase.AttachmentDao;
import com.zhiwee.gree.dao.support.shopBase.mapper.AttachmentMapper;
import com.zhiwee.gree.model.Attachment;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;

@Repository
public class AttachmentDaoImpl extends BaseDaoImpl<Attachment,String,AttachmentMapper> implements AttachmentDao {

}
