package com.zhiwee.gree.dao.support.Invoice;

import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.Invoice.InvoiceDao;
import com.zhiwee.gree.dao.support.Invoice.mapper.InvoiceMapper;
import com.zhiwee.gree.model.Invoice.Invoice;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

/**
 * Created by jick on 2019/7/16.
 */
@Repository
public class InvoiceDaoImpl extends BaseDaoImpl<Invoice,String,InvoiceMapper> implements InvoiceDao {


    @Override
    public List<Invoice> queryInvoice(Map<String, Object> param) {
        return  this.mapper.queryInvoice(param);
    }

    @Override
    public Pageable<Invoice> queryBaseInvoice(Map<String, Object> param, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return convertToPageable(this.mapper.queryBaseInvoice(param));
    }


    @Override
    public Invoice queryBaseInvoiceById(Map<String, Object> param) {
        return   this.mapper.queryBaseInvoiceById(param);
    }
}
