package com.zhiwee.gree.dao.support.MemberGrade.mapper;


import com.zhiwee.gree.model.GoodsInfo.GoodsInfo;
import com.zhiwee.gree.model.GoodsInfo.SolrGoodsInfo;
import com.zhiwee.gree.model.MemberGrade.MemberGrade;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;


public interface MemberGradeMapper extends BaseMapper<MemberGrade> {



    List<MemberGrade> searchAll(Map<String, Object> params);


    MemberGrade selectMax(Map<String, Object> params);
}
