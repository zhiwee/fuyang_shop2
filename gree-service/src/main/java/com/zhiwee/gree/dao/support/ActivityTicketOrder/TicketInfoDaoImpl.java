package com.zhiwee.gree.dao.support.ActivityTicketOrder;

import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.ActivityTicketOrder.TicketInfoDao;
import com.zhiwee.gree.dao.ActivityTicketOrder.TicketOrderDao;
import com.zhiwee.gree.dao.support.ActivityTicketOrder.mapper.TicketInfoMapper;
import com.zhiwee.gree.dao.support.ActivityTicketOrder.mapper.TicketOrderMapper;
import com.zhiwee.gree.model.ActivityTicketOrder.TicketInfo;
import com.zhiwee.gree.model.ActivityTicketOrder.TicketOrder;
import com.zhiwee.gree.model.ActivityTicketOrder.TicketOrderExport;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;


/**
 * Description: 
 * @author: sun
 * @Date 下午5:06 2019/6/23
 * @param:
 * @return:
 */
@Repository
public class TicketInfoDaoImpl extends BaseDaoImpl<TicketInfo, String, TicketInfoMapper> implements TicketInfoDao {


}
