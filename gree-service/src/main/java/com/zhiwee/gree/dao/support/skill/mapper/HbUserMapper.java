package com.zhiwee.gree.dao.support.skill.mapper;


import com.zhiwee.gree.model.skill.HbUser;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

public interface HbUserMapper extends BaseMapper<HbUser> {
    //extends BaseMapper<Skillhb>
    List<HbUser> queryGetHbList(Map<String,Object> param);
    List<HbUser>  queryList(Map<String,Object> param);

}