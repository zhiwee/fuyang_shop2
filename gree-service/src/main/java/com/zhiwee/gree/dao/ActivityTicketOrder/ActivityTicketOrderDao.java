package com.zhiwee.gree.dao.ActivityTicketOrder;

import com.zhiwee.gree.model.ActivityTicketOrder.ActivityTicketOrder;

import com.zhiwee.gree.model.ActivityTicketOrder.vo.TicketOrderStatisticItem;
import xyz.icrab.common.dao.BaseDao;


import java.util.List;
import java.util.Map;

/**
 * @author sun
 * @since 2018/7/12 15:39
 */
public interface ActivityTicketOrderDao extends BaseDao<ActivityTicketOrder, String> {

    List<TicketOrderStatisticItem> ticketOrderByDate(Map<String,Object> param);
}
