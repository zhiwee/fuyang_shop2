package com.zhiwee.gree.dao.support.HomePage.mapper;

import com.github.pagehelper.PageRowBounds;
import com.zhiwee.gree.model.HomePage.PcPlanmap;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

public interface PcPlanmapMapper extends BaseMapper<PcPlanmap> {
    List<PcPlanmap> getPage(Map<String, Object> params, PageRowBounds rowBounds);

    PcPlanmap getOneByid(String id);

    List<PcPlanmap> getList(Map<String, Object> params);
}
