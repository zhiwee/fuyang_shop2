package com.zhiwee.gree.dao.HomePage;

import com.zhiwee.gree.model.HomePage.PcPlanmap;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

public interface PcPlanmapDao  extends BaseDao<PcPlanmap,String> {
    Pageable<PcPlanmap> getPage(Map<String, Object> params, Pagination pagination);

    PcPlanmap selectById(String id);

    List<PcPlanmap> getList(Map<String, Object> params);
}
