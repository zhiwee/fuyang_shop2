package com.zhiwee.gree.dao.support.draw;

import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.draw.DrawInfoDao;
import com.zhiwee.gree.dao.support.draw.mapper.DrawInfoMapper;
import com.zhiwee.gree.model.Partement.Partement;
import com.zhiwee.gree.model.draw.DrawInfo;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository
public class DrawInfoDaoImpl extends BaseDaoImpl<DrawInfo, String, DrawInfoMapper> implements DrawInfoDao {
    @Override
    public List<DrawInfo> checkTime(Map<String, Object> param) {
        return mapper.checkTime(param);
    }

    @Override
    public Pageable<DrawInfo> pageInfo(Map<String, Object> params, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return convertToPageable(this.mapper.pageInfo(params));
    }

    @Override
    public List<DrawInfo> getCurrent(Date date) {
        return mapper.getCurrent(date);
    }


}
