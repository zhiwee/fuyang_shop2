package com.zhiwee.gree.dao.support.Activity.mapper;

import com.github.pagehelper.PageRowBounds;
import com.zhiwee.gree.model.Activity.ActivityInfo;
import com.zhiwee.gree.model.Activity.ActivityPage;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;


/**
 * @author SUN
 * @since 2018/7/6 14:26
 */
public interface ActivityPageMapper extends BaseMapper<ActivityPage> {

    List<ActivityPage> pageInfo(Map<String,Object> param, PageRowBounds rowBounds);
}
