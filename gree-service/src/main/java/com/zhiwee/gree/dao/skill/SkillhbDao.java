package com.zhiwee.gree.dao.skill;

import com.zhiwee.gree.model.skill.Skillhb;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

public interface SkillhbDao extends BaseDao<Skillhb,String> {
    // extends BaseDao<CommentPicture, String>
    // Pageable<ShopUserComment> achieveComment(Map<String, Object> params, Pagination pagination);
    Pageable<Skillhb> queryList(Map<String, Object> params, Pagination pagination);
    List<Skillhb> queryHbByTime(Map<String,Object> params);
}
