package com.zhiwee.gree.dao.Coupon;


import com.zhiwee.gree.model.Coupon.Coupon;
import com.zhiwee.gree.model.Coupon.CouponExport;
import com.zhiwee.gree.model.GoodsInfo.GoodsInfo;
import com.zhiwee.gree.model.GoodsInfo.SolrGoodsInfo;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

public interface CouponDao extends BaseDao<Coupon, String> {


    Pageable<Coupon> pageInfo(Map<String,Object> param, Pagination pagination);

    List<CouponExport> exportCoupon(Map<String,Object> params);

    Coupon getCouponById(String id);

    void deleteList(Map<String, Object> delId);

    void updateAll(Map<String, Object> param);

    List<Coupon> listAll(Map<String, Object> params);
}
