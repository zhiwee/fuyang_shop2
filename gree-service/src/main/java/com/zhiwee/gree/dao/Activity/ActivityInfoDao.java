package com.zhiwee.gree.dao.Activity;

import com.zhiwee.gree.model.Activity.ActivityInfo;
import xyz.icrab.common.dao.BaseDao;

import java.util.List;
import java.util.Map;

/**
 * @author sun
 * @since 2018/7/6 14:25
 */
public interface ActivityInfoDao extends BaseDao<ActivityInfo, String> {

    List<ActivityInfo> checkTime(Map<String, Object> param);

    ActivityInfo getCurrent(Map<String, Object> param);
}
