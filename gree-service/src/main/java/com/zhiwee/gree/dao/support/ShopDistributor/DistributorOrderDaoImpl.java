package com.zhiwee.gree.dao.support.ShopDistributor;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageRowBounds;
import com.zhiwee.gree.dao.ShopDistributor.DistributorAreaDao;
import com.zhiwee.gree.dao.ShopDistributor.DistributorOrderDao;
import com.zhiwee.gree.dao.support.ShopDistributor.mapper.DistributorAreaMapper;
import com.zhiwee.gree.dao.support.ShopDistributor.mapper.DistributorOrderMapper;
import com.zhiwee.gree.model.ShopDistributor.DistributorArea;
import com.zhiwee.gree.model.ShopDistributor.DistributorOrder;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;


@Repository
public class DistributorOrderDaoImpl extends BaseDaoImpl<DistributorOrder, String, DistributorOrderMapper> implements DistributorOrderDao {


    @Override
    public Pageable<DistributorOrder> infoPage(Map<String, Object> param, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return convertToPageable(this.mapper.infoPage(param));
    }
}
