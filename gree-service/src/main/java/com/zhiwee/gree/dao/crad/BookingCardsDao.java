package com.zhiwee.gree.dao.crad;

import com.zhiwee.gree.model.card.BookingCard;
import com.zhiwee.gree.model.card.BookingCards;
import com.zhiwee.gree.model.card.BookingCardsExrt;
import com.zhiwee.gree.model.card.LShopUserCardsExrt;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;


public interface BookingCardsDao extends BaseDao<BookingCards,String> {
    Pageable<BookingCards> pageInfo(Map<String, Object> params, Pagination pagination);

    List<BookingCardsExrt> queryBookingCardsExport(Map<String, Object> params);

    List<BookingCards> listBuy(Map<String, Object> qParam2);
}
