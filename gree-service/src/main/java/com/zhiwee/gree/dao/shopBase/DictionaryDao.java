package com.zhiwee.gree.dao.shopBase;

import com.zhiwee.gree.model.Dictionary;
import xyz.icrab.common.dao.BaseDao;

import java.util.Map;


/**
 * @author gll
 * @since 2018/4/21 22:31
 */
public interface DictionaryDao extends BaseDao<Dictionary, String> {
	/*
	 * 检查分组有没有重名
	 */
    int checkGroup(String group);
    /*
	 * 检查key有没有重名
	 */
    int checkKey(String key, String group);
    /*
	 * 停用启用
	 */
    int updateState(Map<String, Object> map);
}
