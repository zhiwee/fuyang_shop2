package com.zhiwee.gree.dao.goods;


import com.zhiwee.gree.model.GoodsInfo.CategoryArea;
import com.zhiwee.gree.model.GoodsInfo.GoodsClassify;
import com.zhiwee.gree.model.GoodsLabel.*;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

public interface CategoryAreaDao extends BaseDao<CategoryArea,String> {



}
