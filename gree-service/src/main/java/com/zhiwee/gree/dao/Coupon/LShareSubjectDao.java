package com.zhiwee.gree.dao.Coupon;


import com.zhiwee.gree.model.Coupon.Coupon;
import com.zhiwee.gree.model.Coupon.CouponExport;
import com.zhiwee.gree.model.Coupon.LShareSubject;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

public interface LShareSubjectDao extends BaseDao<LShareSubject, String> {


}
