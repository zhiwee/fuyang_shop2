package com.zhiwee.gree.dao.shopBase;

import com.zhiwee.gree.model.Menu;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

public interface MenuDao extends BaseDao<Menu, String> {
    Pageable getMenuByUser(Map<String, Object> param, Pagination pagination);

    List<Menu> getMenuByUser(Map<String, Object> param);

    void addMenuRoleRef(Map<String, Object> param);

    void addMenuUserRef(Map<String, Object> param);

    List<Menu> getMenuByRoleId(String roleid);

    List<Menu> getMenuByUserId(Map<String, Object> param);
}
