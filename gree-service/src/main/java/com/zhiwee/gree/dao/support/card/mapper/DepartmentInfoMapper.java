package com.zhiwee.gree.dao.support.card.mapper;

import com.zhiwee.gree.model.card.CardInfo;
import com.zhiwee.gree.model.card.DepartmentInfo;
import com.zhiwee.gree.model.card.DepartmentInfoCY;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

public interface DepartmentInfoMapper extends BaseMapper<DepartmentInfo> {
    List<DepartmentInfo> pageInfo(Map<String, Object> params);

    List<DepartmentInfoCY> listAll();
}
