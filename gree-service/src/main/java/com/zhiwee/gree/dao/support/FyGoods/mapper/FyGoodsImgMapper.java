package com.zhiwee.gree.dao.support.FyGoods.mapper;

import com.zhiwee.gree.model.FyGoods.FyGoodsImg;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

public interface FyGoodsImgMapper  extends BaseMapper<FyGoodsImg> {

    List<FyGoodsImg> queryImgList(Map<String, Object> param);


}