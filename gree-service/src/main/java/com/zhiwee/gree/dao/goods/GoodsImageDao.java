package com.zhiwee.gree.dao.goods;
import com.zhiwee.gree.model.GoodsInfo.GoodsImage;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

public interface GoodsImageDao extends BaseDao<GoodsImage,String> {
    List<GoodsImage> getWebList(Map<String, Object> params);

    Pageable<GoodsImage> getPage(Map<String, Object> param, Pagination pagination);
}
