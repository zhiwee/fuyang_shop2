package com.zhiwee.gree.dao.crad;

import com.zhiwee.gree.model.card.WelfareCard;
import com.zhiwee.gree.model.card.vo.WelfareCardExrt;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;


public interface WelfareCardDao extends BaseDao<WelfareCard,String> {

    Pageable<WelfareCard> pageInfo(Map<String, Object> params, Pagination pagination);

    List<WelfareCardExrt> listAll(Map<String, Object> params);
}
