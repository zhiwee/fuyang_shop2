package com.zhiwee.gree.dao.support.card;

import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.crad.BookingCardsDao;
import com.zhiwee.gree.dao.support.card.mapper.BookingCardsMapper;
import com.zhiwee.gree.model.card.BookingCard;
import com.zhiwee.gree.model.card.BookingCards;
import com.zhiwee.gree.model.card.BookingCardsExrt;
import com.zhiwee.gree.model.card.LShopUserCardsExrt;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;


@Repository
public class BookingCardsDaoImpl extends BaseDaoImpl<BookingCards, String, BookingCardsMapper> implements BookingCardsDao {
    @Override
    public Pageable<BookingCards> pageInfo(Map<String, Object> params, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return convertToPageable(this.mapper.pageInfo(params));
    }
    @Override
    public List<BookingCardsExrt> queryBookingCardsExport(Map<String, Object> params) {
        return mapper.queryBookingCardsExport(params);
    }

    @Override
    public List<BookingCards> listBuy(Map<String, Object> qParam2) {
        return mapper.listBuy(qParam2);
    }
}
