package com.zhiwee.gree.dao.ActivityTicketOrder;

import com.zhiwee.gree.model.ActivityTicketOrder.TicketOrder;
import com.zhiwee.gree.model.ActivityTicketOrder.TicketOrderExport;
import com.zhiwee.gree.model.ActivityTicketOrder.TicketOrderRefund;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

/**
 * @author sun
 * @since 下午5:06 2019/6/23
 */
public interface TicketOrderRefundDao extends BaseDao<TicketOrderRefund, String> {

    Pageable<TicketOrderRefund> pageInfo(Map<String, Object> param, Pagination pagination);


}
