package com.zhiwee.gree.dao.support.goods;

import com.github.pagehelper.PageRowBounds;
import com.zhiwee.gree.dao.goods.GoodsBandDao;
import com.zhiwee.gree.dao.support.goods.mapper.GoodsBandMapper;
import com.zhiwee.gree.model.GoodsInfo.GoodsBand;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

@Repository
public class GoodsBandDaoImpl extends BaseDaoImpl<GoodsBand,String,GoodsBandMapper> implements GoodsBandDao {
    @Override
    public Pageable<GoodsBand> getPage(Map<String, Object> param, Pagination pagination) {
        PageRowBounds rowBounds = new PageRowBounds(pagination.getOffset(), pagination.getPageSize());
        rowBounds.setCount(true);
        List<GoodsBand> data = this.mapper.getPage(param, rowBounds);
        return convertToPageable(rowBounds, data);
    }
}
