package com.zhiwee.gree.dao.support.Balance;


import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.Balance.BalanceReChargeDao;
import com.zhiwee.gree.dao.Balance.BalanceWithdrawalDao;
import com.zhiwee.gree.dao.support.Balance.mapper.BalanceReChargeMapper;
import com.zhiwee.gree.dao.support.Balance.mapper.BalanceWithdrawalMapper;
import com.zhiwee.gree.model.Balance.BalanceReCharge;
import com.zhiwee.gree.model.Balance.BalanceWithdrawal;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;


@Repository
public class BalanceWithdreawalDaoImpl extends BaseDaoImpl<BalanceWithdrawal, String, BalanceWithdrawalMapper> implements BalanceWithdrawalDao {



    @Override
    public Pageable<BalanceWithdrawal> pageInfo(Map<String, Object> param, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum() + 1, pagination.getPageSize());
        List<BalanceWithdrawal> baseInfos = mapper.pageInfo(param);
        return convertToPageable(baseInfos);
    }

    @Override
    public List<BalanceWithdrawal> balanceRecord(Map<String, Object> params) {
        return  this.mapper.balanceRecord(params);
    }


}
