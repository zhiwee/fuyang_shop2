package com.zhiwee.gree.dao.WithdrawalRecord;


import com.zhiwee.gree.model.WithdrawalRecord.WithdrawalRecord;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.Map;

public interface WithdrawalRecordDao extends BaseDao<WithdrawalRecord, String> {


    Pageable<WithdrawalRecord> pageInfo(Map<String, Object> params, Pagination pagination);
}
