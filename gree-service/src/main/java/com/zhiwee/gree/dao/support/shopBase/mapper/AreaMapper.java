package com.zhiwee.gree.dao.support.shopBase.mapper;

import com.zhiwee.gree.model.Area;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

public interface AreaMapper extends BaseMapper<Area> {

    List<Area> ahtree(Map<String,Object> params);

    List<Area> ahPage(Map<String,Object> params);

    List<Area> allProvince(Map<String,Object> params);
}
