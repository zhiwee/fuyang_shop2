package com.zhiwee.gree.dao.crad;

import com.zhiwee.gree.model.card.LShopUserCards;
import com.zhiwee.gree.model.card.LShopUserCardsExrt;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;


public interface LShopUserCardsDao extends BaseDao<LShopUserCards,String> {

    Pageable<LShopUserCards> getLShopUserCardsList(Map<String, Object> params, Pagination pagination);

    List<LShopUserCardsExrt> queryLShopUserCardsExport(Map<String, Object> params);

}
