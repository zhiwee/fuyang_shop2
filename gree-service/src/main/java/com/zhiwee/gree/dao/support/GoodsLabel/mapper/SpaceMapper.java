package com.zhiwee.gree.dao.support.GoodsLabel.mapper;


import com.zhiwee.gree.model.GoodsLabel.Bp;
import com.zhiwee.gree.model.GoodsLabel.Space;
import xyz.icrab.common.dao.support.mapper.BaseMapper;


public interface SpaceMapper extends BaseMapper<Space> {


}
