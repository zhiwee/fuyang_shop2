package com.zhiwee.gree.dao.support.ShopUser;


import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.ShopUser.ShopUserDao;
import com.zhiwee.gree.dao.goods.GoodsInfoDao;
import com.zhiwee.gree.dao.support.ShopUser.mapper.ShopUserMapper;
import com.zhiwee.gree.dao.support.goods.mapper.GoodsInfoMapper;
import com.zhiwee.gree.model.GoodsInfo.GoodsInfo;
import com.zhiwee.gree.model.GoodsInfo.SolrGoodsInfo;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.ShopUser.ShopUserExport;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.web.param.IdsParam;

import java.util.List;
import java.util.Map;


@Repository
public class ShopUserDaoImpl extends BaseDaoImpl<ShopUser, String, ShopUserMapper> implements ShopUserDao {


    @Override
    public List<ShopUserExport> exportShopUser(Map<String,Object> param) {
        return this.mapper.exportShopUser(param);
    }

    @Override
    public Pageable<ShopUser> pageInfo(Map<String, Object> param, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum() + 1, pagination.getPageSize());
        List<ShopUser> users = mapper.pageInfo(param);
        return convertToPageable(users);
    }

    @Override
    public void deleteUser(IdsParam param) {
        this.mapper.deleteUser(param);
    }

    @Override
    public ShopUser achieveByUser(ShopUser user) {
        return mapper.achieveByUser(user);
    }

    @Override
    public Integer registerAmount(Map<String, Object> params) {
        return mapper.registerAmount(params);

    }



}
