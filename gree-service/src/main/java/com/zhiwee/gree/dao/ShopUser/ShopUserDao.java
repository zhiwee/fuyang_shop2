package com.zhiwee.gree.dao.ShopUser;


import com.zhiwee.gree.model.GoodsInfo.GoodsInfo;
import com.zhiwee.gree.model.GoodsInfo.SolrGoodsInfo;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.ShopUser.ShopUserExport;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.web.param.IdsParam;

import java.util.List;
import java.util.Map;

public interface ShopUserDao extends BaseDao<ShopUser, String> {


    List<ShopUserExport> exportShopUser(Map<String,Object> param);

    Pageable<ShopUser> pageInfo(Map<String,Object> param, Pagination pagination);

    void deleteUser(IdsParam param);

    ShopUser achieveByUser(ShopUser user);

    Integer registerAmount(Map<String,Object> params);
}
