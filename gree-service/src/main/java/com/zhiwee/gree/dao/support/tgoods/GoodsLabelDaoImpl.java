package com.zhiwee.gree.dao.support.tgoods;

import com.zhiwee.gree.dao.support.tgoods.mapper.GoodsLabelMapper;
import com.zhiwee.gree.dao.tgoods.GoodsLabelDao;
import com.zhiwee.gree.model.FyGoods.GoodsLabel;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;

/**
 * @author DELL
 */
@Repository
public class GoodsLabelDaoImpl extends BaseDaoImpl<GoodsLabel, String, GoodsLabelMapper> implements GoodsLabelDao {

        }
