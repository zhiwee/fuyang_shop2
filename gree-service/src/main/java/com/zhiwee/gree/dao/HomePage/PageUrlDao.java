package com.zhiwee.gree.dao.HomePage;

import com.zhiwee.gree.model.HomePage.PageUrl;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.Map;

public interface PageUrlDao extends BaseDao<PageUrl,String> {
    Pageable<PageUrl> getPage(Map<String, Object> param, Pagination pagination);
}
