package com.zhiwee.gree.dao.support.Invoice.mapper;

import com.zhiwee.gree.model.Invoice.Invoice;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * Created by jick on 2019/7/16.
 */
public interface InvoiceMapper  extends BaseMapper<Invoice> {

    List<Invoice> queryInvoice(Map<String,Object> param);
    List<Invoice> queryBaseInvoice(Map<String,Object> param);
    Invoice queryBaseInvoiceById(Map<String,Object> param);



}
