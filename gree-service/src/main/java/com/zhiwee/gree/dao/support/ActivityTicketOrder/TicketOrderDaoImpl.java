package com.zhiwee.gree.dao.support.ActivityTicketOrder;

import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.ActivityTicketOrder.ActivityTicketOrderDao;
import com.zhiwee.gree.dao.ActivityTicketOrder.TicketOrderDao;
import com.zhiwee.gree.dao.support.ActivityTicketOrder.mapper.ActivityTicketOrderMapper;
import com.zhiwee.gree.dao.support.ActivityTicketOrder.mapper.TicketOrderMapper;
import com.zhiwee.gree.model.ActivityTicketOrder.ActivityTicketOrder;
import com.zhiwee.gree.model.ActivityTicketOrder.TicketOrder;
import com.zhiwee.gree.model.ActivityTicketOrder.TicketOrderExport;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;


/**
 * Description: 
 * @author: sun
 * @Date 下午5:06 2019/6/23
 * @param:
 * @return:
 */
@Repository
public class TicketOrderDaoImpl extends BaseDaoImpl<TicketOrder, String, TicketOrderMapper> implements TicketOrderDao {


    @Override
    public Pageable<TicketOrder> pageInfo(Map<String, Object> param, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum() + 1, pagination.getPageSize());
        List<TicketOrder> baseInfos = mapper.pageInfo(param);
        return convertToPageable(baseInfos);
    }

    @Override
    public Pageable<TicketOrder> achieveMyOrder(Map<String, Object> params, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum() + 1, pagination.getPageSize());
        List<TicketOrder> baseInfos = mapper.achieveMyOrder(params);
        return convertToPageable(baseInfos);
    }

    @Override
    public List<TicketOrderExport> pageList(Map<String, Object> params) {
        return mapper.pageList(params);
    }
}
