package com.zhiwee.gree.dao.support.Log.mapper;

import com.zhiwee.gree.model.Log.Log;
import xyz.icrab.common.dao.support.mapper.BaseMapper;


/**
 * @author sun on 2019/3/28
 */
public interface LogMapper extends BaseMapper<Log> {
}
