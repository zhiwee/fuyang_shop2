package com.zhiwee.gree.dao.HomePage;


import com.zhiwee.gree.model.GoodsInfo.GoodsInfo;
import com.zhiwee.gree.model.GoodsInfo.SolrGoodsInfo;
import com.zhiwee.gree.model.HomePage.HomePage;
import com.zhiwee.gree.model.HomePage.PcPlanmap;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import java.util.List;
import java.util.Map;

public interface HomePageDao extends BaseDao<HomePage, String> {

    Pageable<HomePage> achieveHomePage(Map<String,Object> params, Pagination pagination);
}
