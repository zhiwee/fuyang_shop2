package com.zhiwee.gree.dao.support.HomePage.mapper;

import com.github.pagehelper.PageRowBounds;
import com.zhiwee.gree.model.HomePage.PageNav;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

public interface PageNavMapper extends BaseMapper<PageNav> {
    List<PageNav> getPage(Map<String, Object> param, PageRowBounds rowBounds);

    PageNav getById(String id);

    List<PageNav> getList();
}
