package com.zhiwee.gree.dao.support.goods.mapper;


import com.zhiwee.gree.model.GoodsInfo.GoodsDetail;
import com.zhiwee.gree.model.GoodsInfo.GoodsInfo;
import com.zhiwee.gree.model.GoodsInfo.SolrGoodsInfo;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;


public interface GoodsDetailMapper extends BaseMapper<GoodsDetail> {



}
