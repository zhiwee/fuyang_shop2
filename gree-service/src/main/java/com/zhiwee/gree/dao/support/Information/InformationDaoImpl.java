package com.zhiwee.gree.dao.support.Information;

import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.Information.InformationDao;
import com.zhiwee.gree.dao.support.Information.mapper.InformationMapper;
import com.zhiwee.gree.model.Information.Information;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

/**
 * Created by jick on 2019/7/18.
 */
@Repository
public class InformationDaoImpl extends BaseDaoImpl<Information,String,InformationMapper> implements InformationDao {
    /*@Override
    public List<Information> queryInfoList() {
        return  this.mapper.queryInfoList();
    }*/

    /* public Pageable<Invoice> queryBaseInvoice(Map<String, Object> param, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return convertToPageable(this.mapper.queryBaseInvoice(param));
    }*/

    @Override
    public Pageable<Information> queryInfoList( Pagination pagination,Map<String, Object> param) {
        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return convertToPageable(this.mapper.queryInfoList(param));
    }

    // BaseDaoImpl<Invoice,String,InvoiceMapper> implements InvoiceDao
}
