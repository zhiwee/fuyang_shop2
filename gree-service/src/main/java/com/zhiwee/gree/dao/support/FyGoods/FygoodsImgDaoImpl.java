package com.zhiwee.gree.dao.support.FyGoods;

import com.zhiwee.gree.dao.FyGoods.FygoodsImgDao;
import com.zhiwee.gree.dao.support.FyGoods.mapper.FyGoodsImgMapper;
import com.zhiwee.gree.model.FyGoods.FyGoodsImg;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;

import java.util.List;
import java.util.Map;

@Repository
public class FygoodsImgDaoImpl extends BaseDaoImpl<FyGoodsImg,String, FyGoodsImgMapper> implements FygoodsImgDao {
    @Override
    public List<FyGoodsImg> queryImgList(Map<String, Object> param) {
        return  this.mapper.queryImgList(param);
    }
    //extends BaseDaoImpl<GoodCollect, String, GoodCollectMapper> implements GoodCollectDao
}
