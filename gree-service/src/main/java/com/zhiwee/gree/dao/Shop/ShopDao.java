package com.zhiwee.gree.dao.Shop;

import com.zhiwee.gree.model.Shop.Shop;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.Map;

public interface ShopDao extends BaseDao<Shop,String> {
    ////extends BaseDao<Area,String>

    //Pageable<Skillhb> queryList(Map<String, Object> params, Pagination pagination);
     Pageable<Shop> queryList(Map<String, Object> params, Pagination pagination);
}
