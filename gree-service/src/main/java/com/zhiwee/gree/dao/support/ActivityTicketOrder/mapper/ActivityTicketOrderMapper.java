package com.zhiwee.gree.dao.support.ActivityTicketOrder.mapper;

import com.zhiwee.gree.model.ActivityTicketOrder.ActivityTicketOrder;
import com.zhiwee.gree.model.ActivityTicketOrder.vo.TicketOrderStatisticItem;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;


/**
 * @author sun
 * @since 2018/7/6 14:26
 */
public interface ActivityTicketOrderMapper extends BaseMapper<ActivityTicketOrder> {

    List<TicketOrderStatisticItem> ticketOrderByDate(Map<String,Object> param);
}
