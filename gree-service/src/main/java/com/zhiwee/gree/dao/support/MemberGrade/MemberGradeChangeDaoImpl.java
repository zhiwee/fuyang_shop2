package com.zhiwee.gree.dao.support.MemberGrade;


import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.MemberGrade.MemberGradeChangeDao;
import com.zhiwee.gree.dao.MemberGrade.MemberGradeDao;
import com.zhiwee.gree.dao.support.MemberGrade.mapper.MemberGradeChangeMapper;
import com.zhiwee.gree.dao.support.MemberGrade.mapper.MemberGradeMapper;
import com.zhiwee.gree.model.MemberGrade.MemberGrade;
import com.zhiwee.gree.model.MemberGrade.MemberGradeChange;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.Map;


@Repository
public class MemberGradeChangeDaoImpl extends BaseDaoImpl<MemberGradeChange, String, MemberGradeChangeMapper> implements MemberGradeChangeDao {


    @Override
    public Pageable<MemberGradeChange> searchAll(Map<String, Object> params, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return convertToPageable(this.mapper.searchAll(params));
    }
}
