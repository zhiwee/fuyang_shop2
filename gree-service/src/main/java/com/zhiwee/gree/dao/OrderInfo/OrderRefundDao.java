package com.zhiwee.gree.dao.OrderInfo;


import com.zhiwee.gree.model.OrderInfo.OrderItem;
import com.zhiwee.gree.model.OrderInfo.OrderRefund;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.Map;

public interface OrderRefundDao extends BaseDao<OrderRefund, String> {


    Pageable<OrderRefund> pageInfo(Map<String,Object> params, Pagination pagination);
}
