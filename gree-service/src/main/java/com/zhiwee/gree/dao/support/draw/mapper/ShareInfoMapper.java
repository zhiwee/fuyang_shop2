package com.zhiwee.gree.dao.support.draw.mapper;

import com.zhiwee.gree.model.draw.CanDraws;
import com.zhiwee.gree.model.draw.ShareInfo;
import xyz.icrab.common.dao.support.mapper.BaseMapper;


public interface ShareInfoMapper extends BaseMapper<ShareInfo> {
}
