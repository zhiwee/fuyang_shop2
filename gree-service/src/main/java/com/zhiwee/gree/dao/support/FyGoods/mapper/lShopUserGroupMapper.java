package com.zhiwee.gree.dao.support.FyGoods.mapper;


import com.zhiwee.gree.model.FyGoods.LShopUserGroup;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;


public interface lShopUserGroupMapper extends BaseMapper<LShopUserGroup> {
    List<LShopUserGroup> queryGoodsList(Map<String, Object> param);
    List<LShopUserGroup> listAll(Map<String, Object> param);
}