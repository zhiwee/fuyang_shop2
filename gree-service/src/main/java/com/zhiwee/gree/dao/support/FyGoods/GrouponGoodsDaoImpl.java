package com.zhiwee.gree.dao.support.FyGoods;

import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.FyGoods.FygoodsDao;
import com.zhiwee.gree.dao.FyGoods.GrouponGoodsDao;
import com.zhiwee.gree.dao.support.FyGoods.mapper.FygoodsMapper;
import com.zhiwee.gree.dao.support.FyGoods.mapper.GrouponGoodsMapper;
import com.zhiwee.gree.model.FyGoods.Fygoods;
import com.zhiwee.gree.model.FyGoods.GrouponGoods;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;


@Repository
public class GrouponGoodsDaoImpl
        extends BaseDaoImpl<GrouponGoods, String, GrouponGoodsMapper> implements GrouponGoodsDao {
    @Override
    public List<GrouponGoods> queryGoodsList(Map<String, Object> param, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return (this.mapper.queryGoodsList(param));
    }

}
