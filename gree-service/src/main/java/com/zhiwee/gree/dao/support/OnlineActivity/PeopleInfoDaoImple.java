package com.zhiwee.gree.dao.support.OnlineActivity;

import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.OnlineActivity.PeopleInfoDao;
import com.zhiwee.gree.dao.support.OnlineActivity.mapper.PeopleInfoMapper;
import com.zhiwee.gree.model.GoodsInfo.GoodVo.PeopleExr;
import com.zhiwee.gree.model.GoodsInfo.LAPeople;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

@Repository
public class PeopleInfoDaoImple extends BaseDaoImpl<LAPeople, String, PeopleInfoMapper> implements PeopleInfoDao {
    @Override
    public Pageable<LAPeople> nghPeople(Map<String, Object> params, Pagination pagination) {

        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return convertToPageable(this.mapper.nghPeople(params));
    }

    @Override
    public List<LAPeople> queryInfo(Map<String, Object> params) {
        return this.mapper.queryInfo(params);
    }

    @Override
    public void upPeople(Map<String, Object> params) {
         this.mapper.upPeople(params);
    }

    @Override
    public List<PeopleExr> exportPeople(Map<String, Object> params) {
        return this.mapper.exportPeople(params);
    }

    @Override
    public void updateInfo(Map<String, Object> params) {
        this.mapper.updateInfo(params);
    }
}
