package com.zhiwee.gree.dao.support.ShopUser.mapper;


import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.ShopUser.ShopUserExport;
import xyz.icrab.common.dao.support.mapper.BaseMapper;
import xyz.icrab.common.web.param.IdsParam;

import java.util.List;
import java.util.Map;


public interface ShopUserMapper extends BaseMapper<ShopUser> {


   List<ShopUserExport> exportShopUser(Map<String,Object> param);

    List<ShopUser> pageInfo(Map<String,Object> param);

    void deleteUser(IdsParam param);

    ShopUser achieveByUser(ShopUser user);

    Integer registerAmount(Map<String,Object> params);


}
