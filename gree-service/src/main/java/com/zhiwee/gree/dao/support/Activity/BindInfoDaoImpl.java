package com.zhiwee.gree.dao.support.Activity;

import com.zhiwee.gree.dao.Activity.BindInfoDao;
import com.zhiwee.gree.dao.draw.CanDrawsDao;
import com.zhiwee.gree.dao.support.Activity.mapper.BindInfoMapper;
import com.zhiwee.gree.dao.support.draw.mapper.CanDrawsMapper;
import com.zhiwee.gree.model.Activity.BindInfo;
import com.zhiwee.gree.model.draw.CanDraws;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;

@Repository
public class BindInfoDaoImpl extends BaseDaoImpl<BindInfo, String, BindInfoMapper> implements BindInfoDao {
}
