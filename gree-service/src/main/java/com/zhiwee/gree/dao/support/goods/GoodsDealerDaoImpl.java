package com.zhiwee.gree.dao.support.goods;

import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.goods.GoodsBaseDao;
import com.zhiwee.gree.dao.goods.GoodsDealerDao;
import com.zhiwee.gree.dao.support.goods.mapper.GoodsBaseMapper;
import com.zhiwee.gree.dao.support.goods.mapper.GoodsDealerMapper;
import com.zhiwee.gree.model.GoodsInfo.GoodsBase;
import com.zhiwee.gree.model.GoodsInfo.GoodsDealer;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

/**
 * Created by jick on 2019/8/30.
 */
@Repository
public class GoodsDealerDaoImpl extends BaseDaoImpl<GoodsDealer,String, GoodsDealerMapper> implements GoodsDealerDao {


}
