package com.zhiwee.gree.dao.support.order;

import com.zhiwee.gree.dao.order.OrderGoodsDao;
import com.zhiwee.gree.dao.support.order.mapper.OrderGoodsMapper;
import com.zhiwee.gree.model.order.OrderGoods;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;

@Repository
public class OrderGoodsDaoImpl extends BaseDaoImpl<OrderGoods,String, OrderGoodsMapper> implements OrderGoodsDao {
    //extends  BaseDaoImpl<BaseOrder,String, BaseOrderMapper>
    //    implements BaseOrderDao
}
