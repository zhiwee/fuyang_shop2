package com.zhiwee.gree.dao.support.card;

import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.crad.LShopUserCardsDao;
import com.zhiwee.gree.dao.draw.CanDrawsDao;
import com.zhiwee.gree.dao.support.card.mapper.LShopUserCardsMapper;
import com.zhiwee.gree.dao.support.draw.mapper.CanDrawsMapper;
import com.zhiwee.gree.model.card.LShopUserCards;
import com.zhiwee.gree.model.card.LShopUserCardsExrt;
import com.zhiwee.gree.model.draw.CanDraws;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

@Repository
public class LShopUserCardsDaoImpl extends BaseDaoImpl<LShopUserCards, String, LShopUserCardsMapper> implements LShopUserCardsDao {

    @Override
    public Pageable<LShopUserCards> getLShopUserCardsList(Map<String, Object> params, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return convertToPageable(this.mapper.getLShopUserCardsList(params));
    }

    @Override
    public List<LShopUserCardsExrt> queryLShopUserCardsExport(Map<String, Object> params) {
        return mapper.queryLShopUserCardsExport(params);
    }
}
