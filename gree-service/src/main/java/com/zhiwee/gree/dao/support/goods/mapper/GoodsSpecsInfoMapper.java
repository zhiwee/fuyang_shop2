package com.zhiwee.gree.dao.support.goods.mapper;

import com.github.pagehelper.PageRowBounds;
import com.zhiwee.gree.model.GoodsInfo.GoodsSpecsInfo;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

public interface GoodsSpecsInfoMapper  extends BaseMapper<GoodsSpecsInfo> {
    List<GoodsSpecsInfo> getPage(Map<String, Object> param, PageRowBounds rowBounds);

    List<GoodsSpecsInfo> getItemPage(Map<String, Object> param, PageRowBounds rowBounds);
}
