package com.zhiwee.gree.dao.ShopDistributor;


import com.zhiwee.gree.model.ShopDistributor.DistributorArea;
import com.zhiwee.gree.model.ShopDistributor.ShopDistributor;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

public interface DistributorAreaDao extends BaseDao<DistributorArea,String> {


    Pageable<DistributorArea> pageArea(Map<String,Object> param, Pagination pagination);

    List<DistributorArea> searchByCategory(Map<String,Object> params);
}
