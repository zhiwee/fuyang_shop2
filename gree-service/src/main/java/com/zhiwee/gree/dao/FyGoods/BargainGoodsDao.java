package com.zhiwee.gree.dao.FyGoods;

import com.zhiwee.gree.model.FyGoods.BargainGoods;
import com.zhiwee.gree.model.FyGoods.ShareGoods;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

public interface BargainGoodsDao extends BaseDao<BargainGoods,String> {
    List<BargainGoods> queryGoodsList(Map<String, Object> param, Pagination pagination);

}
