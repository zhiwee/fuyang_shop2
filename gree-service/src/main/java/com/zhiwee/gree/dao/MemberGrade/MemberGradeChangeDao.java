package com.zhiwee.gree.dao.MemberGrade;


import com.zhiwee.gree.model.MemberGrade.MemberGrade;
import com.zhiwee.gree.model.MemberGrade.MemberGradeChange;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.Map;

public interface MemberGradeChangeDao extends BaseDao<MemberGradeChange, String> {


    Pageable<MemberGradeChange> searchAll(Map<String,Object> params, Pagination pagination);
}
