package com.zhiwee.gree.dao.support.goods.mapper;


import com.github.pagehelper.PageRowBounds;
import com.zhiwee.gree.model.GoodsInfo.GoodsClassify;
import com.zhiwee.gree.model.GoodsLabel.*;
import com.zhiwee.gree.model.ShopDistributor.CategoryVo;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

public interface GoodsClassifyMapper extends BaseMapper<GoodsClassify> {

    List<Ps> getPsList(Map<String, Object> param);

    List<Space> getSpaceList(Map<String,Object> param);

    List<Gb> getGbList(Map<String,Object> param);

    List<Bp> getBpsList(Map<String,Object> param);

    List<Efficiency> getEfficienciesList(Map<String,Object> param);

    List<GoodsClassify> achieveSmallCategory(Map<String,Object> param,PageRowBounds rowBounds);

    List<GoodsClassify> smallCategory(Map<String,Object> param, PageRowBounds rowBounds);

    List<GoodsClassify>  queryListByRoot(Map<String ,Object> param);

    List<GoodsClassify>  queryListById(Map<String ,Object> param);

    List<GoodsClassify> smallGoodsClassify(Map<String,Object> param, PageRowBounds rowBounds);

    List<CategoryVo> getCategory(Map<String, Object> param);
}
