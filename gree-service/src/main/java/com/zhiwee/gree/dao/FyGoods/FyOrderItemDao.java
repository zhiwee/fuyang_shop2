package com.zhiwee.gree.dao.FyGoods;

import com.zhiwee.gree.model.FyGoods.FyOrderItem;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.Map;

public interface FyOrderItemDao extends BaseDao<FyOrderItem,String> {
    Pageable<FyOrderItem> queryOrderItemByNum(Map<String,Object> param, Pagination pagination);

    void deleteByGoodsId(Map<String, Object> map);

    void deleteAll(Map<String, Object> params);
}
