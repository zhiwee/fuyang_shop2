package com.zhiwee.gree.dao.support.draw;

import com.zhiwee.gree.dao.draw.CanDrawsDao;
import com.zhiwee.gree.dao.support.draw.mapper.CanDrawsMapper;
import com.zhiwee.gree.model.draw.CanDraws;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
@Repository
public class CanDrawsDaoImpl extends BaseDaoImpl<CanDraws, String, CanDrawsMapper> implements CanDrawsDao {
}
