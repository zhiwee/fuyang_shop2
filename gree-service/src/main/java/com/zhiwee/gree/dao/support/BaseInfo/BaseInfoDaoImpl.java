package com.zhiwee.gree.dao.support.BaseInfo;


import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.BaseInfo.BaseInfoDao;
import com.zhiwee.gree.dao.Coupon.CouponDao;
import com.zhiwee.gree.dao.support.BaseInfo.mapper.BaseInfoMapper;
import com.zhiwee.gree.dao.support.Coupon.mapper.CouponMapper;
import com.zhiwee.gree.model.BaseInfo.BaseInfo;
import com.zhiwee.gree.model.Coupon.Coupon;
import com.zhiwee.gree.model.Coupon.CouponExport;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;


@Repository
public class BaseInfoDaoImpl extends BaseDaoImpl<BaseInfo, String, BaseInfoMapper> implements BaseInfoDao {



    @Override
    public Pageable<BaseInfo> pageInfo(Map<String, Object> param, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum() + 1, pagination.getPageSize());
        List<BaseInfo> baseInfos = mapper.pageInfo(param);
        return convertToPageable(baseInfos);
    }


}
