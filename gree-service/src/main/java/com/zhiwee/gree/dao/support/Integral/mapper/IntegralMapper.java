package com.zhiwee.gree.dao.support.Integral.mapper;



import com.zhiwee.gree.model.HomePage.HomePage;
import com.zhiwee.gree.model.Integral.Integral;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;


public interface IntegralMapper extends BaseMapper<Integral> {


    List<Integral> achieveIntegral();
}
