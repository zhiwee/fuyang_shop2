package com.zhiwee.gree.dao.support.GoodCollect;

import com.github.pagehelper.PageRowBounds;
import com.zhiwee.gree.dao.Dealer.DealerInfoBaseDao;
import com.zhiwee.gree.dao.GoodCollect.GoodCollectDao;
import com.zhiwee.gree.dao.support.Dealer.mapper.DealerInfoBaseMapper;
import com.zhiwee.gree.dao.support.GoodCollect.mapper.GoodCollectMapper;
import com.zhiwee.gree.model.Dealer.DealerInfoBase;
import com.zhiwee.gree.model.GoodCollect.GoodCollect;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.entity.Example;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;


@Repository
public class GoodCollectDaoImpl extends BaseDaoImpl<GoodCollect, String, GoodCollectMapper> implements GoodCollectDao {



    @Override
    public Pageable<GoodCollect> infoPage(Map<String, Object> param, Pagination pagination) {
        PageRowBounds rowBounds = new PageRowBounds(pagination.getOffset(), pagination.getPageSize());
        rowBounds.setCount(true);
        List<GoodCollect> data = this.mapper.infoPage(param, rowBounds);
        return convertToPageable(rowBounds, data);
    }


}
