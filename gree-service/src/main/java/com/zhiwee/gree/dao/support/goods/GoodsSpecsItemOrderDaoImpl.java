package com.zhiwee.gree.dao.support.goods;

import com.zhiwee.gree.dao.goods.GoodsSpecsItemOrderDao;
import com.zhiwee.gree.dao.support.goods.mapper.GoodsSpecsItemOrderMapper;
import com.zhiwee.gree.model.GoodsInfo.GoodsSpecsItemOrder;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;

import java.util.List;
import java.util.Map;

@Repository
public class GoodsSpecsItemOrderDaoImpl extends BaseDaoImpl<GoodsSpecsItemOrder,String, GoodsSpecsItemOrderMapper> implements GoodsSpecsItemOrderDao {

    @Override
    public List<GoodsSpecsItemOrder> getListBYGoodsId(Map<String, Object> param) {
        return this.mapper.getListBYGoodsId(param);
    }
}
