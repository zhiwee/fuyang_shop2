package com.zhiwee.gree.dao.support.card;

import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.crad.BookingCardsDao;
import com.zhiwee.gree.dao.crad.GiftCardUserDao;
import com.zhiwee.gree.dao.support.card.mapper.BookingCardsMapper;
import com.zhiwee.gree.dao.support.card.mapper.GiftCardUserMapper;
import com.zhiwee.gree.model.card.BookingCards;
import com.zhiwee.gree.model.card.BookingCardsExrt;
import com.zhiwee.gree.model.card.GiftCardUser;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;


@Repository
public class GiftCardUserDaoImpl extends BaseDaoImpl<GiftCardUser, String, GiftCardUserMapper> implements GiftCardUserDao {
    @Override
    public Pageable<GiftCardUser> pageInfo(Map<String, Object> params, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return convertToPageable(this.mapper.pageInfo(params));
    }

}
