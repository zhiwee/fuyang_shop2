package com.zhiwee.gree.dao.support.ActivityTicketOrder.mapper;

import com.zhiwee.gree.model.ActivityTicketOrder.TicketOrder;
import com.zhiwee.gree.model.ActivityTicketOrder.TicketOrderExport;
import com.zhiwee.gree.model.ActivityTicketOrder.TicketOrderRefund;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;


/**
 * @author sun
 * @since 下午5:06 2019/6/23
 */
public interface TicketOrderRefundMapper extends BaseMapper<TicketOrderRefund> {

    List<TicketOrderRefund> pageInfo(Map<String, Object> param);

}
