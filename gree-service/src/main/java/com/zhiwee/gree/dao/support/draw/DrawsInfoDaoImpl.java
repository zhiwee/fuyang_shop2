package com.zhiwee.gree.dao.support.draw;

import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.draw.DrawsInfoDao;
import com.zhiwee.gree.dao.support.draw.mapper.DrawsInfoMapper;
import com.zhiwee.gree.model.draw.DrawsInfo;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;


@Repository
public class DrawsInfoDaoImpl extends BaseDaoImpl<DrawsInfo, String, DrawsInfoMapper> implements DrawsInfoDao {


    @Override
    public List<DrawsInfo> checkAllNum(Map<String, Object> param) {
        return mapper.checkAllNum(param);
    }

    @Override
    public Pageable<DrawsInfo> pageInfo(Map<String, Object> params, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return convertToPageable(this.mapper.pageInfo(params));


    }

    @Override
    public List<DrawsInfo> listAll(Map<String, Object> param) {
        return mapper.listAll(param);
    }
}
