package com.zhiwee.gree.dao.HomePage;

import com.zhiwee.gree.model.HomePage.PageChannelAdimg;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.Map;

public interface PageChannelAdimgDao extends BaseDao<PageChannelAdimg,String> {
    Pageable<PageChannelAdimg> getPage(Map<String, Object> param, Pagination pagination);
}
