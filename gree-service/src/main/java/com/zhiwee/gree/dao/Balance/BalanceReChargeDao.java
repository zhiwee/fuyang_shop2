package com.zhiwee.gree.dao.Balance;


import com.zhiwee.gree.model.Balance.BalanceReCharge;
import com.zhiwee.gree.model.BaseInfo.BaseInfo;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.Map;
import java.util.List;

public interface BalanceReChargeDao extends BaseDao<BalanceReCharge, String> {


    Pageable<BalanceReCharge> pageInfo(Map<String, Object> param, Pagination pagination);


    List<BalanceReCharge> balanceRecord(Map<String,Object> params);
}
