package com.zhiwee.gree.dao.support.FyGoods;

import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.FyGoods.LShopUserGroupDao;
import com.zhiwee.gree.dao.support.FyGoods.mapper.lShopUserGroupMapper;
import com.zhiwee.gree.model.FyGoods.LShopUserGroup;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;


@Repository
public class LShopUserGroupDaoImpl
        extends BaseDaoImpl<LShopUserGroup, String, lShopUserGroupMapper> implements LShopUserGroupDao {
    @Override
    public Pageable<LShopUserGroup> queryGoodsList(Map<String, Object> param, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return convertToPageable(this.mapper.queryGoodsList(param));
    }

    @Override
    public List<LShopUserGroup> listAll(Map<String, Object> param) {
        return mapper.listAll(param);
    }

}
