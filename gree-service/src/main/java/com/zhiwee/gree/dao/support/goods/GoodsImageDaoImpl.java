package com.zhiwee.gree.dao.support.goods;

import com.github.pagehelper.PageRowBounds;
import com.zhiwee.gree.dao.goods.GoodsImageDao;
import com.zhiwee.gree.dao.support.goods.mapper.GoodsImageMapper;
import com.zhiwee.gree.model.GoodsInfo.GoodsBand;
import com.zhiwee.gree.model.GoodsInfo.GoodsImage;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

@Repository
public class GoodsImageDaoImpl extends BaseDaoImpl<GoodsImage,String, GoodsImageMapper> implements GoodsImageDao {
    @Override
    public List<GoodsImage> getWebList(Map<String, Object> params) {
        return this.mapper.getWebList(params);
    }

    @Override
    public Pageable<GoodsImage> getPage(Map<String, Object> param, Pagination pagination) {
        PageRowBounds rowBounds = new PageRowBounds(pagination.getOffset(), pagination.getPageSize());
        rowBounds.setCount(true);
        List<GoodsImage> data = this.mapper.getPage(param, rowBounds);
        return convertToPageable(rowBounds, data);
    }
}
