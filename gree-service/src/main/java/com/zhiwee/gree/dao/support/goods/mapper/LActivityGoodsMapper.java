package com.zhiwee.gree.dao.support.goods.mapper;


import com.zhiwee.gree.model.GoodsInfo.LAGoodsInfo;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

public interface LActivityGoodsMapper   extends BaseMapper<LAGoodsInfo> {
    List<LAGoodsInfo> secKillGoods(Map<String, Object> params);

    LAGoodsInfo queryKillInfoById(Map<String,Object> param);

    List<LAGoodsInfo> secKillGoods2(Map<String,Object> params);

}
