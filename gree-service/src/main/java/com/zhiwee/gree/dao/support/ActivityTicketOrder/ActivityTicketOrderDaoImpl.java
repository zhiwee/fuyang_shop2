package com.zhiwee.gree.dao.support.ActivityTicketOrder;

import com.zhiwee.gree.dao.ActivityTicketOrder.ActivityTicketOrderDao;
import com.zhiwee.gree.dao.support.ActivityTicketOrder.mapper.ActivityTicketOrderMapper;
import com.zhiwee.gree.model.ActivityTicketOrder.ActivityTicketOrder;

import com.zhiwee.gree.model.ActivityTicketOrder.vo.TicketOrderStatisticItem;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;

import java.util.List;
import java.util.Map;


/**
 * Description: 
 * @author: sun
 * @Date 下午11:37 2019/5/15
 * @param:
 * @return:
 */
@Repository
public class ActivityTicketOrderDaoImpl extends BaseDaoImpl<ActivityTicketOrder, String, ActivityTicketOrderMapper> implements ActivityTicketOrderDao {


    @Override
    public List<TicketOrderStatisticItem> ticketOrderByDate(Map<String, Object> param) {
        return this.mapper.ticketOrderByDate(param);
    }
}
