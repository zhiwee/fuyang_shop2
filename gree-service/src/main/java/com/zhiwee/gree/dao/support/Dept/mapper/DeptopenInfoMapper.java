package com.zhiwee.gree.dao.support.Dept.mapper;

import com.zhiwee.gree.model.DeptInfo.DeptopenInfo;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * Created by jick on 2019/8/23.
 */
public interface DeptopenInfoMapper  extends BaseMapper<DeptopenInfo> {
     List<DeptopenInfo> queryDeptOpenInfo(Map<String,Object> param);
}
