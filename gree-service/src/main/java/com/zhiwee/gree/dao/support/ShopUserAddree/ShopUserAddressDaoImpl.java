package com.zhiwee.gree.dao.support.ShopUserAddree;


import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.ShopUser.ShopUserDao;
import com.zhiwee.gree.dao.ShopUserAddress.ShopUserAddressDao;
import com.zhiwee.gree.dao.support.ShopUser.mapper.ShopUserMapper;
import com.zhiwee.gree.dao.support.ShopUserAddree.mapper.ShopUserAddressMapper;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.ShopUserAddress.ShopUserAddress;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.web.param.IdsParam;

import java.util.List;
import java.util.Map;


@Repository
public class ShopUserAddressDaoImpl extends BaseDaoImpl<ShopUserAddress, String, ShopUserAddressMapper> implements ShopUserAddressDao {


}
