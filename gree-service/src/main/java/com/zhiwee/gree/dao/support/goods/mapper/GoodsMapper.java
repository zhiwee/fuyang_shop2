package com.zhiwee.gree.dao.support.goods.mapper;



import com.zhiwee.gree.model.FyGoods.FygoodsExo;
import com.zhiwee.gree.model.GoodsInfo.GoodVo.GoodsExo;
import com.zhiwee.gree.model.GoodsInfo.GoodVo.GoodsFyExo;
import com.zhiwee.gree.model.GoodsInfo.GoodVo.GoodsInfoVO;
import com.zhiwee.gree.model.GoodsInfo.Goods;
import com.zhiwee.gree.model.GoodsInfo.LAGoodsInfo;
import com.zhiwee.gree.model.GoodsInfo.ProductAttribute;
import com.zhiwee.gree.model.OnlineActivity.OnlineActivityInfo;
import com.zhiwee.gree.model.OnlineActivity.vo.OnlineActivityInfoVo;
import xyz.icrab.common.dao.support.mapper.BaseMapper;
import xyz.icrab.common.model.Pageable;

import java.util.List;
import java.util.Map;

/**
 * Created by jick on 2019/8/30.
 */
public interface GoodsMapper   extends BaseMapper<Goods> {
    // GoodsInfoMapper extends BaseMapper<GoodsInfo> {
    //queryInfoList
    // List<ProductAttribute> queryList(Map<String,Object> params);
    List<Goods> queryInfoList(Map<String,Object> params);
    Goods queryInfoListById(Map<String,Object> params);

    /*List<Goods> secKillGoods(Map<String, Object> params);*/

   // List<SecKillGoods> secKillGoods(Map<String, Object> params);

    void upSecKillGoods(Map<String, Object> params);

    void upSecKillPeople(Map<String, Object> params);

    Goods getInfo(Map<String, Object> params);


    List<OnlineActivityInfoVo> getCurrentSceKillGods(Map<String, Object> params);

    GoodsInfoVO getOneByGoods(Map<String, Object> params);
    List<ProductAttribute> querySpecsById(Map<String, Object> param);

    List<ProductAttribute> queryAttrById(Map<String,Object> param);

    List<Goods> listAll(Map<String, Object> params);

    List<GoodsExo> listExo();

    List<GoodsFyExo> listExo2();

    List<FygoodsExo> listall();

    List<Goods>  getCount(String id);
}
