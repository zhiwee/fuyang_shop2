package com.zhiwee.gree.dao.support.draw.mapper;

import com.zhiwee.gree.model.draw.CanDraws;
import xyz.icrab.common.dao.support.mapper.BaseMapper;


public interface CanDrawsMapper extends BaseMapper<CanDraws> {
}
