package com.zhiwee.gree.dao.support.OrderInfo.mapper;


import com.zhiwee.gree.model.OrderInfo.OrderAddress;
import com.zhiwee.gree.model.OrderInfo.OrderRefund;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;


public interface OrderRefundMapper extends BaseMapper<OrderRefund> {


    List<OrderRefund> pageInfo(Map<String,Object> params);
}
