package com.zhiwee.gree.dao.draw;

import com.zhiwee.gree.model.draw.DrawInfo;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface DrawInfoDao  extends BaseDao<DrawInfo,String> {
    List<DrawInfo> checkTime(Map<String, Object> param);

    Pageable<DrawInfo> pageInfo(Map<String, Object> params, Pagination pagination);

    List<DrawInfo> getCurrent(Date date);

}
