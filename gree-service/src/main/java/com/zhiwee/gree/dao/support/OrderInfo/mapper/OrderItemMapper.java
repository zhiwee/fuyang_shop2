package com.zhiwee.gree.dao.support.OrderInfo.mapper;


import com.zhiwee.gree.model.Analysis.ItemAnalysis;
import com.zhiwee.gree.model.OrderInfo.Order;
import com.zhiwee.gree.model.OrderInfo.OrderItem;
import com.zhiwee.gree.model.OrderInfo.vo.GoodNameTopTen;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;


public interface OrderItemMapper extends BaseMapper<OrderItem> {


    List<OrderItem> orderItemList(Map<String,Object> params);

    List<OrderItem> achieveOrderRecord(List<String> ids);

    List<OrderItem> achieveItemsByOrderId(Map<String,Object> orderId);

    List<GoodNameTopTen> topTen();

    ItemAnalysis itemAnalysis(Map<String,Object> params);
}
