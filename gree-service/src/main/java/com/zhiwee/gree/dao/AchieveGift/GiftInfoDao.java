package com.zhiwee.gree.dao.AchieveGift;


import com.zhiwee.gree.model.AchieveGift.AchieveGift;
import com.zhiwee.gree.model.AchieveGift.GiftInfo;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.Map;

public interface GiftInfoDao extends BaseDao<GiftInfo, String> {


    Pageable<GiftInfo> pageInfo(Map<String, Object> params, Pagination pagination);
}
