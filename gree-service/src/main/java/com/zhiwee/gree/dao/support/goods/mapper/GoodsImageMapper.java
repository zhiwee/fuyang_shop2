package com.zhiwee.gree.dao.support.goods.mapper;

import com.github.pagehelper.PageRowBounds;
import com.zhiwee.gree.model.GoodsInfo.GoodsImage;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

public interface GoodsImageMapper extends BaseMapper<GoodsImage> {
    List<GoodsImage> getWebList(Map<String, Object> params);

    List<GoodsImage> getPage(Map<String, Object> param, PageRowBounds rowBounds);
}
