package com.zhiwee.gree.dao.support.goods.mapper;

import com.github.pagehelper.PageRowBounds;
import com.zhiwee.gree.model.GoodsInfo.GoodsCategoryAdImg;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

public interface GoodsCategoryAdImgMapper extends BaseMapper<GoodsCategoryAdImg> {
    List<GoodsCategoryAdImg> getPage(Map<String, Object> param, PageRowBounds rowBounds);

    GoodsCategoryAdImg selectById(String id);
}
