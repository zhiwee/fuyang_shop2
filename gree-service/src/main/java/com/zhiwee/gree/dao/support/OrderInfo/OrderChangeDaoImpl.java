package com.zhiwee.gree.dao.support.OrderInfo;



import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.OrderInfo.OrderChangeDao;
import com.zhiwee.gree.dao.support.OrderInfo.mapper.OrderChangeMapper;
import com.zhiwee.gree.model.OrderInfo.OrderChange;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import java.util.Map;


@Repository
public class OrderChangeDaoImpl extends BaseDaoImpl<OrderChange, String, OrderChangeMapper> implements OrderChangeDao {


    @Override
    public Pageable<OrderChange> pageInfo(Map<String, Object> params, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return convertToPageable(this.mapper.pageInfo(params));
    }

}
