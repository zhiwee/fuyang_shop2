package com.zhiwee.gree.dao.support.goods;

import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.goods.GoodsBaseDao;
import com.zhiwee.gree.dao.goods.GoodsDao;
import com.zhiwee.gree.dao.support.goods.mapper.GoodsBaseMapper;
import com.zhiwee.gree.dao.support.goods.mapper.GoodsMapper;
import com.zhiwee.gree.model.GoodsInfo.GoodVo.GoodsExo;
import com.zhiwee.gree.model.GoodsInfo.GoodVo.GoodsInfoVO;
import com.zhiwee.gree.model.GoodsInfo.Goods;
import com.zhiwee.gree.model.GoodsInfo.GoodsBase;
import com.zhiwee.gree.model.GoodsInfo.ProductAttribute;
import com.zhiwee.gree.model.OnlineActivity.vo.OnlineActivityInfoVo;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

/**
 * Created by jick on 2019/8/30.
 */
@Repository
public class GoodsBaseDaoImpl extends BaseDaoImpl<GoodsBase,String, GoodsBaseMapper> implements GoodsBaseDao {
    @Override
    public Pageable<GoodsBase> queryInfoList(Map<String, Object> params, Pagination pagination) {
      //  return null;
        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return     convertToPageable(this.mapper.queryInfoList(params));
    }

    @Override
    public GoodsBase queryInfoListById(Map<String, Object> params) {
        return mapper.queryInfoListById(params);
    }

    @Override
    public List<GoodsBase> listAll(Map<String, Object> params) {
        return mapper.listAll(params);
    }

    @Override
    public List<String> getList(String id) {
        return mapper.getList(id);
    }
}
