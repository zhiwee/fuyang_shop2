package com.zhiwee.gree.dao.draw;

import com.zhiwee.gree.model.draw.DrawUseInfo;
import com.zhiwee.gree.model.draw.DrawUseInfoExrt;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;

import java.util.List;
import java.util.Map;

public interface DrawUseInfoDao extends BaseDao<DrawUseInfo,String> {

    Pageable<DrawUseInfo> pageInfo(Map<String, Object> params, Pagination pagination);

    List<DrawUseInfoExrt> queryDrawUseInfoExport(Map<String,Object> params);

}
