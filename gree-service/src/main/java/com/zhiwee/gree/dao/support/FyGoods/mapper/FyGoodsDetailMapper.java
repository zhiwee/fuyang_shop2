package com.zhiwee.gree.dao.support.FyGoods.mapper;


import com.zhiwee.gree.model.FyGoods.FyGoodsDetail;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

public interface FyGoodsDetailMapper  extends BaseMapper<FyGoodsDetail> {

    List<FyGoodsDetail> queryByGoodsId(Map<String,Object> param);
}