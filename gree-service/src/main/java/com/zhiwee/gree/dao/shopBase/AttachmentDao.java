package com.zhiwee.gree.dao.shopBase;


import com.zhiwee.gree.model.Attachment;
import xyz.icrab.common.dao.BaseDao;

public interface AttachmentDao extends BaseDao<Attachment,String> {
}
