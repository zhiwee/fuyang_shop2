package com.zhiwee.gree.dao.support.card.mapper;

import com.zhiwee.gree.model.card.BookingCard;
import com.zhiwee.gree.model.card.BookingCards;
import com.zhiwee.gree.model.card.BookingCardsExrt;
import com.zhiwee.gree.model.card.LShopUserCardsExrt;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

public interface BookingCardsMapper extends BaseMapper<BookingCards> {
    List<BookingCards> pageInfo(Map<String, Object> params);
    List<BookingCardsExrt> queryBookingCardsExport(Map<String, Object> params);

    List<BookingCards> listBuy(Map<String, Object> qParam2);
}
