package com.zhiwee.gree.dao.crad;

import com.zhiwee.gree.model.card.GiftCardUser;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.Map;


public interface GiftCardUserDao extends BaseDao<GiftCardUser,String> {
    Pageable<GiftCardUser> pageInfo(Map<String, Object> params, Pagination pagination);

}
