package com.zhiwee.gree.dao.FyGoods;

import com.zhiwee.gree.model.FyGoods.FyGoodsImg;
import xyz.icrab.common.dao.BaseDao;

import java.util.List;
import java.util.Map;

public interface FygoodsImgDao  extends BaseDao<FyGoodsImg,String> {
    List<FyGoodsImg> queryImgList(Map<String, Object> param);
}
