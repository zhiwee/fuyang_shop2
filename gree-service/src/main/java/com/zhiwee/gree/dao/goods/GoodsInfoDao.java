package com.zhiwee.gree.dao.goods;


import com.zhiwee.gree.model.GoodsInfo.*;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

public interface GoodsInfoDao extends BaseDao<GoodsInfo, String> {




    Pageable<GoodsInfo> searchAll(Map<String, Object> params, Pagination pagination);


    void updateActivityGoodsInfo(List<LAGoodsInfo> list);

    void updateActivityPeople(List<LAPeople> list);

    void updateActivityGoodsInfo2(List<LAGoodsInfo> list);

}
