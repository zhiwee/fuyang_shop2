package com.zhiwee.gree.dao.support.Questionnaire;

import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.Questionnaire.AnswerInfoDao;
import com.zhiwee.gree.dao.Questionnaire.QuestionnaireDao;
import com.zhiwee.gree.dao.support.Questionnaire.mapper.AnswerInfoMapper;
import com.zhiwee.gree.dao.support.Questionnaire.mapper.QuestionnaireMapper;
import com.zhiwee.gree.model.Questionnaire.AnswerInfo;
import com.zhiwee.gree.model.Questionnaire.AnswerInfoExrt;
import com.zhiwee.gree.model.Questionnaire.Questionnaire;
import com.zhiwee.gree.model.draw.DrawUseInfoExrt;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

@Repository
public class AnswerInfoDaoImpl extends BaseDaoImpl<AnswerInfo, String, AnswerInfoMapper> implements AnswerInfoDao {
    @Override
    public Pageable<AnswerInfo> pageInfo(Map<String, Object> params, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return convertToPageable(this.mapper.pageInfo(params));
    }

    @Override
    public List<AnswerInfoExrt> queryAnswerInfoExport(Map<String, Object> params) {
        return this.mapper.queryAnswerInfoExport(params);
    }
}
