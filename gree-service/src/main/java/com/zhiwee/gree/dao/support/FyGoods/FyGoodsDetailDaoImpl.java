package com.zhiwee.gree.dao.support.FyGoods;

import com.zhiwee.gree.dao.FyGoods.FyGoodsDetailDao;
import com.zhiwee.gree.dao.support.FyGoods.mapper.FyGoodsDetailMapper;
import com.zhiwee.gree.model.FyGoods.FyGoodsDetail;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;

import java.util.List;
import java.util.Map;

@Repository
public class FyGoodsDetailDaoImpl extends BaseDaoImpl<FyGoodsDetail, String, FyGoodsDetailMapper>
        implements FyGoodsDetailDao {
    @Override
    public List<FyGoodsDetail> queryByGoodsId(Map<String, Object> param) {
        return  this.mapper.queryByGoodsId(param);
    }

    //BaseDaoImpl<Fygoods, String, FygoodsMapper> implements FygoodsDao
}
