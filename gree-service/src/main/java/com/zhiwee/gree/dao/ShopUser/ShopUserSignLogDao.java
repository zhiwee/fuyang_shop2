package com.zhiwee.gree.dao.ShopUser;

import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.ShopUser.ShopUserSignLog;
import xyz.icrab.common.dao.BaseDao;

import java.util.Map;

/**
 * TODO
 *
 * @author Chensong
 * @Company http://www.zhiwee.com
 * date 2020-02-10
 */
public interface ShopUserSignLogDao extends BaseDao<ShopUserSignLog, String> {
    Integer signAmount(Map<String,Object> params);
    ShopUserSignLog getOneLog(Map<String,Object> params);
}
