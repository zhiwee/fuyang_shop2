package com.zhiwee.gree.dao.goods;


import com.zhiwee.gree.model.GoodsInfo.LAGoodsInfo;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

public interface LActivityGoodsDao extends BaseDao<LAGoodsInfo,String> {
     Pageable<LAGoodsInfo> secKillGoods (Map<String,Object> params, Pagination pagination);

     LAGoodsInfo queryKillInfoById(Map<String,Object> param);

     List<LAGoodsInfo> secKillGoods2(Map<String,Object> params);



}
