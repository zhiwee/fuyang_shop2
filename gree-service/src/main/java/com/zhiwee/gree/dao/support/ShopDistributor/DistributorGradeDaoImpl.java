package com.zhiwee.gree.dao.support.ShopDistributor;

import com.github.pagehelper.PageRowBounds;
import com.zhiwee.gree.dao.ShopDistributor.DistributorGradeDao;
import com.zhiwee.gree.dao.ShopDistributor.ShopDistributorDao;
import com.zhiwee.gree.dao.support.ShopDistributor.mapper.DistributorGradeMapper;
import com.zhiwee.gree.dao.support.ShopDistributor.mapper.ShopDistributorMapper;
import com.zhiwee.gree.model.ShopDistributor.DistributorGrade;
import com.zhiwee.gree.model.ShopDistributor.ShopDistributor;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;


@Repository
public class DistributorGradeDaoImpl extends BaseDaoImpl<DistributorGrade, String, DistributorGradeMapper> implements DistributorGradeDao {



    @Override
    public Pageable<DistributorGrade> infoPage(Map<String, Object> param, Pagination pagination) {
        PageRowBounds rowBounds = new PageRowBounds(pagination.getOffset(), pagination.getPageSize());
        rowBounds.setCount(true);
        List<DistributorGrade> data = this.mapper.infoPage(param, rowBounds);
        return convertToPageable(rowBounds, data);
    }

    @Override
    public DistributorGrade selectMax(Map<String, Object> params) {
        return mapper.selectMax(params);
    }


}
