package com.zhiwee.gree.dao.goods;

import com.zhiwee.gree.model.GoodsInfo.GoodsCategoryAdImg;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.Map;


public interface GoodsCategoryAdImgDao extends BaseDao<GoodsCategoryAdImg,String> {
    Pageable<GoodsCategoryAdImg> getPage(Map<String, Object> param, Pagination pagination);

    GoodsCategoryAdImg selectById(String id);
}
