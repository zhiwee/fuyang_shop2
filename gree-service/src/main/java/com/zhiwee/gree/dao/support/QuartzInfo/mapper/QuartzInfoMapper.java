package com.zhiwee.gree.dao.support.QuartzInfo.mapper;


import com.zhiwee.gree.model.Partement.Partement;
import com.zhiwee.gree.model.Quartz.QuartzInfo;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;


public interface QuartzInfoMapper extends BaseMapper<QuartzInfo> {


    List<QuartzInfo> pageInfo(Map<String, Object> params);
}
