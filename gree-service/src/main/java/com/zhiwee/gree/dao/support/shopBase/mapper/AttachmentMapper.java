package com.zhiwee.gree.dao.support.shopBase.mapper;

import com.zhiwee.gree.model.Attachment;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

public interface AttachmentMapper extends BaseMapper<Attachment> {
    List<Attachment> query(Map<String, Object> param);
}
