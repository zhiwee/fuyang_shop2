package com.zhiwee.gree.dao.support.card;

import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.crad.UserCardsInfoDao;
import com.zhiwee.gree.dao.support.card.mapper.UserCardsInfoMapper;
import com.zhiwee.gree.model.card.UserCardsInfo;
import com.zhiwee.gree.model.card.UserCardsInfoExrt;
import com.zhiwee.gree.model.draw.DrawUseInfoExrt;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;
@Repository
public class UserCardsInfoDaoImpl extends BaseDaoImpl<UserCardsInfo, String, UserCardsInfoMapper> implements UserCardsInfoDao {
    @Override
    public Pageable pageInfo(Map<String, Object> params, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return convertToPageable(this.mapper.pageInfo(params));
    }
    @Override
    public Pageable pageInfoNoCategory(Map<String, Object> params, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return convertToPageable(this.mapper.pageInfoNoCategory(params));
    }

    @Override
    public UserCardsInfo getCurrentOne(Map<String,Object> param) {
        return mapper.getCurrentOne(param);
    }

    @Override
    public List<UserCardsInfoExrt> queryUserCardsInfoExport(Map<String, Object> params) {
        return this.mapper.queryUserCardsInfoExport(params);
    }
    @Override
    public List<UserCardsInfoExrt> queryUserCardsInfoNoCategoryExport(Map<String, Object> params) {
        return this.mapper.queryUserCardsInfoNoCategoryExport(params);
    }

    @Override
    public List<UserCardsInfo> queryUserCardsInfo(Map<String, Object> params) {
        return this.mapper.queryUserCardsInfo(params);
    }
}
