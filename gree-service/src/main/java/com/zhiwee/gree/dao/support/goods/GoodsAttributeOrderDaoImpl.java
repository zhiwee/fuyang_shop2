package com.zhiwee.gree.dao.support.goods;

import com.zhiwee.gree.dao.goods.GoodsAttributeOrderDao;
import com.zhiwee.gree.dao.support.goods.mapper.GoodsAttributeOrderMapper;
import com.zhiwee.gree.model.GoodsInfo.GoodsAttributeOrder;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;

import java.util.Map;

@Repository
public class GoodsAttributeOrderDaoImpl extends BaseDaoImpl<GoodsAttributeOrder,String, GoodsAttributeOrderMapper> implements GoodsAttributeOrderDao {
    @Override
    public void deleteByGoodsId(Map<String, Object> param) {
        this.mapper.deleteByGoodsId(param);
    }
}
