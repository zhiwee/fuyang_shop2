package com.zhiwee.gree.dao.GoodCollect;



import com.zhiwee.gree.model.Dealer.DealerInfoBase;
import com.zhiwee.gree.model.GoodCollect.GoodCollect;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

public interface GoodCollectDao extends BaseDao<GoodCollect,String> {


    Pageable<GoodCollect> infoPage(Map<String, Object> param, Pagination pagination);
}
