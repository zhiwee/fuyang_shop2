package com.zhiwee.gree.dao.support.goods;


import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.goods.ShopGoodsInfoDao;
import com.zhiwee.gree.dao.goods.ShopGoodsPictureDao;
import com.zhiwee.gree.dao.support.goods.mapper.ShopGoodsInfoMapper;
import com.zhiwee.gree.dao.support.goods.mapper.ShopGoodsPictureMapper;
import com.zhiwee.gree.model.GoodsInfo.ShopGoodsInfo;
import com.zhiwee.gree.model.GoodsInfo.ShopGoodsPicture;
import com.zhiwee.gree.model.GoodsInfo.SolrGoodsInfo;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;


@Repository
public class ShopGoodsPictureDaoImpl extends BaseDaoImpl<ShopGoodsPicture, String, ShopGoodsPictureMapper> implements ShopGoodsPictureDao {



}
