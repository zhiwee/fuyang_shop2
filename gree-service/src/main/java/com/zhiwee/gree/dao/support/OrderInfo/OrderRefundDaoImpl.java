package com.zhiwee.gree.dao.support.OrderInfo;



import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.OrderInfo.OrderAddressDao;
import com.zhiwee.gree.dao.OrderInfo.OrderRefundDao;
import com.zhiwee.gree.dao.support.OrderInfo.mapper.OrderAddressMapper;
import com.zhiwee.gree.dao.support.OrderInfo.mapper.OrderRefundMapper;
import com.zhiwee.gree.model.OrderInfo.OrderAddress;
import com.zhiwee.gree.model.OrderInfo.OrderRefund;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.Map;


@Repository
public class OrderRefundDaoImpl extends BaseDaoImpl<OrderRefund, String, OrderRefundMapper> implements OrderRefundDao {


    @Override
    public Pageable<OrderRefund> pageInfo(Map<String, Object> params, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return convertToPageable(this.mapper.pageInfo(params));
    }
}
