package com.zhiwee.gree.dao.support.Partement.mapper;


import com.zhiwee.gree.model.OrderInfo.OrderAddress;
import com.zhiwee.gree.model.Partement.Partement;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;


public interface PartementMapper extends BaseMapper<Partement> {


    List<Partement> pageInfo(Map<String,Object> params);
}
