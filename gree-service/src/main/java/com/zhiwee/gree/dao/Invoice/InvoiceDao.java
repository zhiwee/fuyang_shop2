package com.zhiwee.gree.dao.Invoice;

import com.zhiwee.gree.model.Invoice.Invoice;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

/**
 * Created by jick on 2019/7/16.
 */
public interface InvoiceDao  extends BaseDao<Invoice,String> {
    List<Invoice> queryInvoice(Map<String,Object> param);

    Pageable<Invoice> queryBaseInvoice(Map<String,Object> param,Pagination pagination);

    Invoice queryBaseInvoiceById(Map<String,Object> param);


}
