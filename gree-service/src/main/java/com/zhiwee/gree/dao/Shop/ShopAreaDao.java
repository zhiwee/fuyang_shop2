package com.zhiwee.gree.dao.Shop;

import com.zhiwee.gree.model.Shop.ShopArea;
import xyz.icrab.common.dao.BaseDao;

public interface ShopAreaDao extends BaseDao<ShopArea,Integer> {
    //extends BaseDao<Area,String>
}
