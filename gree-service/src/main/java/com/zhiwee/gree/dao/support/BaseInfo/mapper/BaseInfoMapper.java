package com.zhiwee.gree.dao.support.BaseInfo.mapper;


import com.zhiwee.gree.model.BaseInfo.BaseInfo;
import com.zhiwee.gree.model.Coupon.Coupon;
import com.zhiwee.gree.model.Coupon.CouponExport;
import org.apache.ibatis.annotations.Param;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;


public interface BaseInfoMapper extends BaseMapper<BaseInfo> {

    List<BaseInfo> pageInfo(Map<String,Object> param);

}
