package com.zhiwee.gree.dao.support.skill;

import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.skill.HbUserDao;
import com.zhiwee.gree.dao.support.skill.mapper.HbUserMapper;
import com.zhiwee.gree.model.skill.HbUser;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

@Repository
public class HbUserDaoImpl extends
        BaseDaoImpl<HbUser, String, HbUserMapper> implements HbUserDao {

    @Override
    public List<HbUser> queryGetHbList(Map<String, Object> param) {
        return  this.mapper.queryGetHbList(param);
    }

    @Override
    public Pageable<HbUser> queryList(Map<String, Object> params, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return convertToPageable(this.mapper.queryList(params));
    }
}
