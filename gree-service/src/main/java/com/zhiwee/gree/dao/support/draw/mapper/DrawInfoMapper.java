package com.zhiwee.gree.dao.support.draw.mapper;

import com.zhiwee.gree.model.draw.DrawInfo;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface DrawInfoMapper extends BaseMapper<DrawInfo> {

    List<DrawInfo> checkTime(Map<String, Object> param);

    List<DrawInfo> pageInfo(Map<String, Object> params);

    List<DrawInfo> getCurrent(Date date);
}
