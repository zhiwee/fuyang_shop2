package com.zhiwee.gree.dao.support.OrderInfo.mapper;


import com.zhiwee.gree.model.OrderInfo.OrderAddress;
import com.zhiwee.gree.model.OrderInfo.OrderItem;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;


public interface OrderAddressMapper extends BaseMapper<OrderAddress> {



}
