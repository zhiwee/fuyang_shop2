package com.zhiwee.gree.dao.support.card.mapper;


import com.zhiwee.gree.model.card.WelfareCard;
import com.zhiwee.gree.model.card.vo.WelfareCardExrt;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

public interface WelfareCardMapper extends BaseMapper<WelfareCard> {


    List<WelfareCard> pageInfo(Map<String, Object> params);

    List<WelfareCardExrt> listAll(Map<String, Object> params);
}
