package com.zhiwee.gree.dao.support.goods.mapper;

import com.zhiwee.gree.model.GoodsInfo.GoodsAttributeOrder;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.Map;

public interface GoodsAttributeOrderMapper extends BaseMapper<GoodsAttributeOrder> {
    void deleteByGoodsId(Map<String, Object> param);
}
