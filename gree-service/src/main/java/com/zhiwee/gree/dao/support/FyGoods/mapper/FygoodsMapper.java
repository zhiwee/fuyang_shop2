package com.zhiwee.gree.dao.support.FyGoods.mapper;


import com.zhiwee.gree.model.FyGoods.Fygoods;
import com.zhiwee.gree.model.FyGoods.FygoodsExo;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;


public interface FygoodsMapper extends BaseMapper<Fygoods> {
    List<Fygoods> queryGoodsList(Map<String,Object> param);
    List<Fygoods>   portalQueryGoodsList(Map<String,Object> param);
    List<Fygoods>  queryList(Map<String,Object> param);


    Fygoods queryInfoListById(Map<String, Object> param);

    List<FygoodsExo> listall();
}