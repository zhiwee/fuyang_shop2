package com.zhiwee.gree.dao.support.ShopDistributor.mapper;


import com.github.pagehelper.PageRowBounds;
import com.zhiwee.gree.model.ShopDistributor.DistributorGrade;
import com.zhiwee.gree.model.ShopDistributor.DistributorGradeChange;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;


public interface DistributorGradeChangeMapper extends BaseMapper<DistributorGradeChange> {


    List<DistributorGradeChange> infoPage(Map<String, Object> param, PageRowBounds rowBounds);



}
