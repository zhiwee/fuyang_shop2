package com.zhiwee.gree.dao.support.goods.mapper;


import com.zhiwee.gree.model.GoodsInfo.GoodsInfo;
import com.zhiwee.gree.model.GoodsInfo.ShopGoodsInfo;
import com.zhiwee.gree.model.GoodsInfo.SolrGoodsInfo;
import org.apache.ibatis.annotations.Param;
import xyz.icrab.common.dao.support.mapper.BaseMapper;

import java.util.List;
import java.util.Map;


public interface ShopGoodsInfoMapper extends BaseMapper<ShopGoodsInfo> {

    List<ShopGoodsInfo> achieveGoodsByName(Map<String,Object> param);

    List<SolrGoodsInfo> getAllGoods(Map<String,Object> param);

    List<ShopGoodsInfo> searchAll(Map<String,Object> params);

    ShopGoodsInfo achieveGoodsById(Map<String,Object> params);

    List<ShopGoodsInfo> achieveSpecialGoods(Map<String,Object> params);

    List<ShopGoodsInfo> achieveSmallGoods(Map<String,Object> param);

    List<ShopGoodsInfo> achieveRecommend(Map<String,Object> param);

    ShopGoodsInfo searchOrderGoods(@Param("goodId") String goodId);

    List<ShopGoodsInfo> achieveHotSellGood(Map<String,Object> param);
}
