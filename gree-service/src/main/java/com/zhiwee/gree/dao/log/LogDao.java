package com.zhiwee.gree.dao.log;

import com.zhiwee.gree.model.Log.Log;
import xyz.icrab.common.dao.BaseDao;

/**
 * @author sun on 2019/3/28
 */
public interface LogDao extends BaseDao<Log, String> {
}
