package com.zhiwee.gree.dao.support.ShopUserComment;


import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.ShopUserComment.CommentPictureDao;
import com.zhiwee.gree.dao.ShopUserComment.ShopUserCommentDao;
import com.zhiwee.gree.dao.support.ShopUserComment.mapper.CommentPictureMapper;
import com.zhiwee.gree.dao.support.ShopUserComment.mapper.ShopUserCommentMapper;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.ShopUserComment.CommentPicture;
import com.zhiwee.gree.model.ShopUserComment.ShopUserComment;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.web.session.Session;

import java.util.Iterator;
import java.util.List;
import java.util.Map;


@Repository
public class CommentPictureDaoImpl extends BaseDaoImpl<CommentPicture, String,CommentPictureMapper> implements CommentPictureDao {


}
