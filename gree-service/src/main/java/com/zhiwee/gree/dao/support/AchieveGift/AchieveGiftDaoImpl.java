package com.zhiwee.gree.dao.support.AchieveGift;


import com.github.pagehelper.PageHelper;
import com.zhiwee.gree.dao.AchieveGift.AchieveGiftDao;
import com.zhiwee.gree.dao.support.AchieveGift.mapper.AchieveGiftMapper;
import com.zhiwee.gree.model.AchieveGift.AchieveGift;
import com.zhiwee.gree.model.AchieveGift.vo.GiftCount;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;


@Repository
public class AchieveGiftDaoImpl extends BaseDaoImpl<AchieveGift, String, AchieveGiftMapper> implements AchieveGiftDao {

    @Override
    public Pageable<AchieveGift> searchGiftInfo(Map<String, Object> params, Pagination pagination) {
        PageHelper.startPage(pagination.getPageNum()+1, pagination.getPageSize(), true);
        return convertToPageable(this.mapper.searchGiftInfo(params));
    }

    @Override
    public List<AchieveGift> searchGift(Map<String, Object> param) {
        return this.mapper.searchGift(param);
    }

    @Override
    public List<GiftCount> giftCount(Map<String, Object> param) {
        return this.mapper.giftCount(param);
    }
}
