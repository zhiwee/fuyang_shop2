package com.zhiwee.gree.dao.support.goods.mapper;



import com.zhiwee.gree.model.GoodsInfo.GoodsDealer;
import xyz.icrab.common.dao.support.mapper.BaseMapper;


/**
 * Created by jick on 2019/8/30.
 */
public interface GoodsDealerMapper extends BaseMapper<GoodsDealer> {

}
