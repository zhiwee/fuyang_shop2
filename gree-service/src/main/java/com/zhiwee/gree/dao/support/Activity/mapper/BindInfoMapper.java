package com.zhiwee.gree.dao.support.Activity.mapper;

import com.zhiwee.gree.model.Activity.BindInfo;
import xyz.icrab.common.dao.support.mapper.BaseMapper;


public interface BindInfoMapper extends BaseMapper<BindInfo> {
}
