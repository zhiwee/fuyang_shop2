package com.zhiwee.gree.dao.crad;

import com.zhiwee.gree.model.card.CardInfo;
import com.zhiwee.gree.model.card.DepartmentInfo;
import com.zhiwee.gree.model.card.DepartmentInfoCY;
import xyz.icrab.common.dao.BaseDao;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

public interface DepartmentInfoDao extends BaseDao<DepartmentInfo,String> {

    Pageable<DepartmentInfo> pageInfo(Map<String, Object> params, Pagination pagination);

    List<DepartmentInfoCY> listAll();
}
