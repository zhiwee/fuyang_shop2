package com.zhiwee.gree.dao.support.HomePage;

import com.github.pagehelper.PageRowBounds;
import com.zhiwee.gree.dao.HomePage.PageNavDao;
import com.zhiwee.gree.dao.support.HomePage.mapper.PageNavMapper;
import com.zhiwee.gree.model.HomePage.PageNav;
import com.zhiwee.gree.model.HomePage.PcPlanmap;
import org.springframework.stereotype.Repository;
import xyz.icrab.common.dao.support.BaseDaoImpl;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;

import java.util.List;
import java.util.Map;

@Repository
public class PageNavDaoImpl extends BaseDaoImpl<PageNav,String, PageNavMapper> implements PageNavDao {
    @Override
    public Pageable<PageNav> getPage(Map<String, Object> param, Pagination pagination) {
        PageRowBounds rowBounds = new PageRowBounds(pagination.getOffset(), pagination.getPageSize());
        rowBounds.setCount(true);
        List<PageNav> data = this.mapper.getPage(param, rowBounds);
        return convertToPageable(rowBounds, data);
    }

    @Override
    public PageNav getById(String id) {
        return this.mapper.getById(id);
    }

    @Override
    public List<PageNav> getList() {
        return this.mapper.getList();
    }
}
