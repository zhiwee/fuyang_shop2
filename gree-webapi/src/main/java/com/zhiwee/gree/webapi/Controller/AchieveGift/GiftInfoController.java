package com.zhiwee.gree.webapi.Controller.AchieveGift;

import com.zhiwee.gree.model.AchieveGift.AchieveGift;
import com.zhiwee.gree.model.AchieveGift.GiftInfo;
import com.zhiwee.gree.model.ActivityTicketOrder.ActivityTicketOrder;
import com.zhiwee.gree.service.AchieveGift.AchieveGiftService;
import com.zhiwee.gree.service.AchieveGift.GiftInfoService;
import com.zhiwee.gree.service.ActivityTicketOrder.ActivityTicketOrderService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.util.IdGenerator;
import xyz.icrab.common.web.annotation.*;
import xyz.icrab.common.web.param.IdParam;
import xyz.icrab.common.web.param.IdsParam;
import xyz.icrab.common.web.util.Validator;

import javax.validation.Valid;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author sun on 2019/7/01
 */
@RestController
@RequestMapping("/giftInfo")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class GiftInfoController {
    @Autowired
    private GiftInfoService giftInfoService;

/**
 * Description: 获取所有礼品
 * @author: sun
 * @Date 下午5:33 2019/7/1
 * @param:
 * @return:
 */
    @PermissionMode(PermissionMode.Mode.White)
    @RequestMapping(value = "/pageInfo")
    public Result<?>  pageInfo(@RequestBody(required = false) Map<String, Object> params,Pagination pagination){
        Pageable<GiftInfo> achieveGifts = giftInfoService.pageInfo(params, pagination);
        return Result.ok(achieveGifts);
    }




    /**
     * Description: 通过手机号和劵号领取礼品
     * @author: sun
     * @Date 下午12:51 2019/5/6
     * @param:
     * @return:
     */
    @PermissionMode(PermissionMode.Mode.White)
    @RequestMapping(value = "/addGiftInfo")
    public Result<?> achieveGift(@RequestBody GiftInfo giftInfo){
        Validator validator = new Validator();
        validator.notNull(giftInfo.getGiftName(), "礼品名称不能为空不能为空");
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }
        Map<String,Object> params = new HashMap<>();
        params.put("giftName",giftInfo.getGiftName().trim());
        List<GiftInfo> list = giftInfoService.list(params);
        if(!list.isEmpty() && list.size() > 0){
            validator.notNull(giftInfo.getGiftName(), "存在重复的礼品名");
        }

        GiftInfo gift = new GiftInfo();
        gift.setId(IdGenerator.objectId());
        gift.setCreateTime(new Date());
        gift.setGiftName(giftInfo.getGiftName().trim());
        gift.setState(1);
        giftInfoService.save(gift);
        return Result.ok();
    }





/**
 * Description: 更新礼品
 * @author: sun
 * @Date 下午5:46 2019/7/1
 * @param:
 * @return:
 */
    @PermissionMode(PermissionMode.Mode.White)
    @RequestMapping(value = "/updateGiftInfo")
    public Result<?> updateGiftInfo(@RequestBody GiftInfo giftInfo){
        Validator validator = new Validator();
        validator.notNull(giftInfo.getId(), "礼品标识不能为空");
        validator.notNull(giftInfo.getGiftName(), "礼品名称不能为空");
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }
        giftInfo.setUpdateTime(new Date());
        giftInfo.setGiftName(giftInfo.getGiftName().trim());
        giftInfoService.update(giftInfo);
        return Result.ok();
    }



 /**
  * Description: 禁用礼品
  * @author: sun
  * @Date 下午5:50 2019/7/1
  * @param:
  * @return:
  */
    @RequestMapping("/stop")
    @ResponseBody
    public Result<?> stop(@RequestBody @Valid IdsParam map) {
        List<String> ids = map.getIds();
        GiftInfo giftInfo = new GiftInfo();
        for (String id : ids) {
            giftInfo.setId(id);
            giftInfo.setState(2);
            giftInfoService.update(giftInfo);

        }
        return Result.ok();
    }

    /**
     * Description: 启用礼品
     * @author: sun
     * @Date 下午5:50 2019/7/1
     * @param:
     * @return:
     */
    @RequestMapping("/open")
    @ResponseBody
    public Result<?> stopUseCoupon(@RequestBody @Valid IdsParam map) {
        List<String> ids = map.getIds();
        GiftInfo gift = new GiftInfo();
        for (String id : ids) {
            gift.setId(id);
            gift.setState(1);
            giftInfoService.update(gift);

        }
        return Result.ok();
    }



    /**
     * Description: 通过id获取礼品
     * @author: sun
     * @Date 下午5:50 2019/7/1
     * @param:
     * @return:
     */
    @RequestMapping("/getGift")
    @ResponseBody
    public Result<?> getGift(@RequestBody @Valid IdParam map) {
        return Result.ok(giftInfoService.get(map.getId()));
    }


    /**
     * Description: 获取所有启用的礼品
     * @author: sun
     * @Date 下午5:50 2019/7/1
     * @param:
     * @return:
     */
    @RequestMapping("/achieveAllGift")
    @ResponseBody
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> achieveAllGift() {
        Map<String,Object> params = new HashMap<>();
        params.put("state",1);
        List<GiftInfo> list = giftInfoService.list(params);
        return Result.ok(list);
    }



}
