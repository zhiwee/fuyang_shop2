package com.zhiwee.gree.webapi.Controller.GoodsInfo;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.github.pagehelper.PageInfo;
import com.zhiwee.gree.model.BaseInfo.BaseInfo;
import com.zhiwee.gree.model.GoodsInfo.*;
import com.zhiwee.gree.model.GoodsInfo.GoodVo.GoodsDescription;
import com.zhiwee.gree.model.GoodsInfo.GoodVo.GoodsHomePage;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.service.BaseInfo.BaseInfoService;
import com.zhiwee.gree.service.GoodsInfo.GoodsDetailService;
import com.zhiwee.gree.service.GoodsInfo.GoodsService;
import com.zhiwee.gree.service.GoodsInfo.ShopGoodsInfoService;
import com.zhiwee.gree.service.GoodsInfo.ShopGoodsPictureService;
import com.zhiwee.gree.service.ShopUser.ShopUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.solr.core.SolrTemplate;
import org.springframework.data.solr.core.query.*;
import org.springframework.data.solr.core.query.result.HighlightEntry;
import org.springframework.data.solr.core.query.result.HighlightPage;
import org.springframework.data.solr.core.query.result.ScoredPage;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import xyz.icrab.common.model.*;
import xyz.icrab.common.util.IdGenerator;
import xyz.icrab.common.util.KeyUtils;
import xyz.icrab.common.web.annotation.*;
import xyz.icrab.common.web.param.IdsParam;
import xyz.icrab.common.web.util.SessionUtils;
import xyz.icrab.common.web.util.Validator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;


/**
 * @author sun on 2019/4/30
 */
@RestController
@RequestMapping("/shopGoodsInfo")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class ShopGoodsInfoController {

    @Autowired
    private SolrTemplate solrTemplate;

    @Autowired
    private ShopGoodsPictureService shopGoodsPictureService;

    @Autowired
    private GoodsDetailService goodsDetailService;


    @Autowired
    private ShopGoodsInfoService shopGoodsInfoService;

    @Autowired
    private BaseInfoService baseInfoService;


    @Value("${file.upload.dir}")
    private String fileUploadDir;

    @Value("${HTTP_PICTUREURL}")
    private String httpurl;

    @Autowired
    private ShopUserService shopUserService;


    @Autowired
    private GoodsService    goodsService;

    /**
     * Description: 通过商品id查找商品
     *
     * @author: sun
     * @Date 下午10:40 2019/3/27
     * @param:
     * @return:
     */
    @RequestMapping(value = "/achieveGoodsById")
    public Result<?> achieveGoodsById(@RequestBody Map<String, Object> params) {
        if (params == null) {
            params = new HashMap<>();
        }
        params.put("goodId", params.get("id"));
        ShopGoodsInfo shopGoodsInfo = shopGoodsInfoService.achieveGoodsById(params);
        return Result.ok(shopGoodsInfo);
    }




    /**
     * Description:  商品更新,加入原来没有商品描述就添加，有的话就更新通过detailId来判断是否原来存在描述
     *
     * @author: sun
     * @Date 下午10:40 2019/3/27
     * @param:
     * @return:
     */
    @RequestMapping(value = "/updateShopGood")
    public Result<?> updateShopGood(@RequestBody ShopGoodsInfo shopGoodsInfo) {
        Validator validator = new Validator();
        validator.notEmpty(shopGoodsInfo.getId(), "标识不能为空");
        validator.notEmpty(shopGoodsInfo.getName(), "商品名称不能为空");
        validator.notNull(shopGoodsInfo.getSpecId(), "型号不能为空");
        validator.notNull(shopGoodsInfo.getUnit(), "单位不能为空");
        validator.notNull(shopGoodsInfo.getCategory(), "商品分类不能为空");
        validator.notNull(shopGoodsInfo.getPrice(), "销售价格不能为空");
        validator.notNull(shopGoodsInfo.getMarketPrice(), "市场价不能为空");
        validator.notNull(shopGoodsInfo.getStockNum(), "库存不能为空");
        validator.notNull(shopGoodsInfo.getActivityPrice(), "活动价不能为空");
        validator.notNull(shopGoodsInfo.getSellNum(), "购买数量不能为空");
        validator.notNull(shopGoodsInfo.getRetailPrice(), "分销价格不能为空");

        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }



        GoodsDetail detail = new GoodsDetail();
        if(shopGoodsInfo.getDetailId() == null || shopGoodsInfo.getDetailId() == ""){
            detail.setId(IdGenerator.objectId());
            detail.setDetail(shopGoodsInfo.getDetailName());
            detail.setCreatetime(new Date());
            goodsDetailService.save(detail);
            shopGoodsInfo.setDetailId(detail.getId());
        }else{
            detail.setId(shopGoodsInfo.getDetailId());
            detail.setDetail(shopGoodsInfo.getDetailName());
            detail.setUpdatetime(new Date());
            goodsDetailService.update(detail);
        }
        shopGoodsInfoService.update(shopGoodsInfo);
        return Result.ok();
    }



    /**
     * Description:  solrTemplate 添加
     *
     * @author: sun
     * @Date 下午10:40 2019/3/27
     * @param:
     * @return:
     */
    @RequestMapping(value = "solrTemplateAdd")
    public Result<?> solrTemplate(@RequestBody Map<String, Object> params) {
        if (params == null) {
            params = new HashMap<>();
        }
        params.put("state", 1);
        List<SolrGoodsInfo> solrGoodsInfoList = shopGoodsInfoService.getAllGoods(params);
        for(SolrGoodsInfo solrGoodsInfo : solrGoodsInfoList){
            Map<String, Object> param = new HashMap<>();
            param.put("goodId",solrGoodsInfo.getId());
            List<ShopGoodsPicture> list = shopGoodsPictureService.list(param);
            if(list.size() > 0){
                solrGoodsInfo.setPicture(list.get(0).getPicture());
            }
        }
        solrTemplate.saveBeans(solrGoodsInfoList);
        solrTemplate.commit();
        return Result.ok();
    }


    /**
     * Description:  查询所有商品
     *
     * @author: sun
     * @Date 下午10:40 2019/3/27
     * @param:
     * @return:
     */
    @RequestMapping(value = "/searchAll")
    public Result<?> searchAll(@RequestBody(required = false) Map<String, Object> params, Pagination pagination) {

      /*  Pageable<ShopGoodsInfo> GoodsInfoList = shopGoodsInfoService.searchAll(params, pagination);
        return Result.ok(GoodsInfoList);*/

            Pageable<Goods>  GoodsInfoList =  goodsService.queryInfoList(params,pagination);
            return    Result.ok( GoodsInfoList);

    }






  /**
   * Description: 工程团购展示商品
   * @author: sun
   * @Date 上午9:38 2019/7/9
   * @param:
   * @return:
   */
    @RequestMapping(value = "/searchEngineerGood")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> searchEngineerGood(String name ,String specId, Pagination pagination) {

           Map<String,Object> params = new HashMap<>();

        params.put("state",1);
        params.put("name",name);
        params.put("specId",specId);
        Pageable<ShopGoodsInfo> GoodsInfoList = shopGoodsInfoService.searchEngineerGood(params, pagination);
        return Result.ok(GoodsInfoList);
    }








    /**
     * Description:  solrTemplate 查询
     *
     * @author: sun
     * @Date 下午10:40 2019/4/28
     * @param:
     * @return:
     */
    @RequestMapping(value = "/goodsHightSearch")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> HightSearch(@RequestBody Map<String, Object> param, HttpServletRequest req, HttpServletResponse res) {
        List<BaseInfo> baseInfos = baseInfoService.list();
        BaseInfo baseInfo = baseInfos.get(0);

        HighlightQuery query = new SimpleHighlightQuery();
        //默认降序排列
        Sort sort = null;
        // 1 升序 2 降序
        if (param.get("priceDirection") != null && ((String) param.get("priceDirection")).equals("2")) {
            sort = new Sort(Sort.Direction.DESC, "price");
        }else{
            sort =  new Sort(Sort.Direction.ASC, "price");
        }
        //过滤条件
        Criteria criteria = new Criteria("state").is(1);
        if ( param.get("category") != null &&  !((String)param.get("category")).equals("")) {
            criteria = criteria.and(new Criteria("category").is((String) param.get("category")));
        }
        if ( param.get("categoryParent") != null &&  !((String)param.get("categoryParent")).equals("")) {
            criteria = criteria.and(new Criteria("categoryParent").is((String) param.get("categoryParent")));
        }
        if ( param.get("goodsName") != null &&  !((String)param.get("goodsName")).equals("")) {
            criteria = criteria.and(new Criteria("goodsInfo_keywords").contains((String) param.get("goodsName")));
        }

        if ( param.get("ps") != null &&  !((String)param.get("ps")).equals("")) {
            criteria = criteria.and(new Criteria("ps").is((String) param.get("ps")));
        }
        if ( param.get("space") != null &&  !((String)param.get("space")).equals("")) {
            criteria = criteria.and(new Criteria("space").is((String) param.get("space")));
        }
        if ( param.get("bp") != null &&  !((String)param.get("bp")).equals("")) {
            criteria = criteria.and(new Criteria("bp").is((String) param.get("bp")));
        }
        if (param.get("gb") != null &&  !((String)param.get("gb")).equals("")) {
            criteria = criteria.and(new Criteria("gb").is((String) param.get("gb")));
        }
        if ( param.get("energyEfficiencyGrade") != null &&  !((String)param.get("energyEfficiencyGrade")).equals("")) {
            criteria = criteria.and(new Criteria("energyEfficiencyGrade").is((String) param.get("energyEfficiencyGrade")));
        }
        if ( param.get("pricemin") != null && !((String) param.get("pricemin")).equals("")) {
            criteria =criteria.and(new Criteria("price").greaterThanEqual((String) param.get("pricemin"))) ;
        }
        if (param.get("pricemax") != null && !((String) param.get("pricemax")).equals("")) {
               criteria=criteria.and(new Criteria("price").greaterThanEqual((String) param.get("pricemax")));
        }


        //分页
        PageRequest pageable = null;
        if (param.get("pageNumber") == null) {
            param.put("pageNumber", 0);
            pageable = new PageRequest((Integer) param.get("pageNumber"), 12);

        } else {
            pageable = new PageRequest((Integer) param.get("pageNumber"), 12);
        }

        query.addCriteria(criteria);
        query.addSort(sort);
        query.setPageRequest(pageable);
        // 设置高亮的属性
        HighlightOptions highlightOptions = new HighlightOptions();
        // 指定高亮的域
        highlightOptions.addField("name");
        // 前缀和后缀
        highlightOptions.setSimplePrefix("<span style=\"color:red\">");
        highlightOptions.setSimplePostfix("</span>");

        query.setHighlightOptions(highlightOptions);

        HighlightPage<SolrGoodsInfo> queryForHighlightPage = solrTemplate.queryForHighlightPage(query, SolrGoodsInfo.class);

//        System.out.println(queryForHighlightPage);
        System.out.println(JSON.toJSONString(queryForHighlightPage, SerializerFeature.DisableCircularReferenceDetect));

        List<SolrGoodsInfo> content = queryForHighlightPage.getContent();
        for (SolrGoodsInfo tbItem : content) {
        xyz.icrab.common.web.session.Session  session = SessionUtils.get(req);
            ShopUser user = null;
            if (session != null) {
                //假如这个用户是经销商用户则展示的价格按身份获取价格
                user = (ShopUser) session.getAttribute("ShopUser");
                ShopUser shopUser = shopUserService.get(user.getId());
                if(shopUser.getRegisterType() == 2){
                    if (user.getDistributorGrade() == 1) {
                        tbItem.setPrice(tbItem.getOnePrice());
                    } else if (user.getDistributorGrade() == 2) {
                        tbItem.setPrice(tbItem.getTwoPrice());
                    } else if (user.getDistributorGrade() == 3) {
                        tbItem.setPrice(tbItem.getThreePrice());
                    } else {
                        tbItem.setPrice(tbItem.getPrice());
                    }
                }
            }


            if(tbItem.getSpecial() == 1 ){
                //判断时间的问题
                ShopGoodsInfo shopGoodsInfo = shopGoodsInfoService.get(tbItem.getId());
                if(new Date().before(shopGoodsInfo.getSpecialEndTime())){
                    tbItem.setPrice(tbItem.getSpecialPrice());
                }

            }



            List<HighlightEntry.Highlight> highlights = queryForHighlightPage.getHighlights(tbItem);
            // 获取高亮部分
            if(!highlights.isEmpty() && highlights.size() > 0){
            List<String> snipplets = highlights.get(0).getSnipplets();
            if(!snipplets.isEmpty() && snipplets.size() > 0){
                String string =snipplets.get(0);
                tbItem.setName(string);
            }
            }

        }
        int result = 0;
        if ((content.size() % 12) == 0) {
            result = content.size() / 12;                    // 保持double型数据类型
        } else {
            result = (content.size() / 12) + 1;             // 保持double型数据类型
        }

        Pageable page = new Page(content.size(), result, (Integer) param.get("pageNumber"), 10, content);
        return Result.ok(page);
    }

    /**
     * Description: 删除solr里面所有数据
     *
     * @author: sun
     * @Date 下午12:51 2019/5/6
     * @param:
     * @return:
     */
    @RequestMapping(value = "/deleteAll")
    public Result<?> deleteAll() {
        Query query = new SimpleQuery("*:*");
        solrTemplate.delete(query);
        solrTemplate.commit();
        return Result.ok();
    }




    /**
     * Description: 获取特价商品
     *
     * @author: sun
     * @Date 下午12:51 2019/5/6
     * @param:
     * @return:
     */
    @PermissionMode(PermissionMode.Mode.White)
    @RequestMapping(value = "/achieveSpecial")
    public Result<?> achieveSpecial(HttpServletRequest req, HttpServletResponse res) {
//        List<BaseInfo> baseInfos = baseInfoService.list();
//        BaseInfo baseInfo = baseInfos.get(0);
           Map<String,Object> params = new HashMap<>();
             params.put("special", "1");
             params.put("state", "1");
         List<ShopGoodsInfo> shopGoodsInfos = shopGoodsInfoService.list(params);
         for(ShopGoodsInfo shopGoodsInfo : shopGoodsInfos){
             //加入特价时间结束了，将特价改成销售价格

             params.clear();
             params.put("goodId",shopGoodsInfo.getId());
             List<ShopGoodsPicture> list = shopGoodsPictureService.list(params);
             if(!list.isEmpty()){
                 shopGoodsInfo.setPicture(list.get(0).getPicture());
             }

         }
        return Result.ok(shopGoodsInfos);
    }






    /**
     * Description: 获取新商品
     * @author: sun
     * @Date 下午12:51 2019/5/6
     * @param:
     * @return:
     */
    @PermissionMode(PermissionMode.Mode.White)
    @RequestMapping(value = "/achieveNews")
    public Result<?> achieveNews() {
        Query query = new SimpleQuery();
        Sort sort = new Sort(Sort.Direction.ASC, "sort");
        Criteria criteria = new Criteria("news").is(1).and(new Criteria("state").is(1));
        query.addCriteria(criteria);
        query.addSort(sort);
        ScoredPage<SolrGoodsInfo> solrGoodsInfos = solrTemplate.queryForPage(query, SolrGoodsInfo.class);
        List<SolrGoodsInfo> content = solrGoodsInfos.getContent();
        solrTemplate.commit();
        return Result.ok(content);
    }

    /**
     * Description: 启用商城商品
     * @author: sun
     * @Date 下午9:06 2019/5/8
     * @param:
     * @return:
     */
    @RequestMapping("/open")
    @ResponseBody
    public Result<?> open(@RequestBody @Valid IdsParam map) {
        List<String> ids = map.getIds();
        ShopGoodsInfo goodsInfo = new ShopGoodsInfo();
        for (String id : ids) {
            goodsInfo.setId(id);
            goodsInfo.setState(1);
            shopGoodsInfoService.update(goodsInfo);
            Map<String, Object> param = new HashMap<>();
            param.put("goodId", id);
            param.put("state", 1);
            List<SolrGoodsInfo> solrGoodsInfoList = shopGoodsInfoService.getAllGoods(param);
            if (solrGoodsInfoList.size() > 0) {
                solrTemplate.saveBean(solrGoodsInfoList.get(0));
            }
        }
        solrTemplate.commit();
        return Result.ok();
    }

    /**
     * Description: 禁用商城商品
     *
     * @author: sun
     * @Date 下午9:07 2019/5/8
     * @param:
     * @return:
     */
    @RequestMapping("/stop")
    @ResponseBody
    public Result<?> stop(@RequestBody @Valid IdsParam map) {
        List<String> ids = map.getIds();
        ShopGoodsInfo goodsInfo = new ShopGoodsInfo();
        for (String id : ids) {
            goodsInfo.setId(id);
            goodsInfo.setState(2);
            shopGoodsInfoService.update(goodsInfo);
            Map<String, Object> param = new HashMap<>();
            param.put("goodId", id);
            param.put("state", 2);
            List<SolrGoodsInfo> solrGoodsInfoList = shopGoodsInfoService.getAllGoods(param);
            if (solrGoodsInfoList.size() > 0) {
                solrTemplate.saveBean(solrGoodsInfoList.get(0));
            }
        }
        solrTemplate.commit();
        return Result.ok();
    }


    /**
     * Description: 设置新品
     * @author: sun
     * @Date 下午9:06 2019/5/8
     * @param:
     * @return:
     */
    @RequestMapping("/openNew")
    @ResponseBody
    public Result<?> isNew(@RequestBody @Valid IdsParam map) {
        List<String> ids = map.getIds();
        ShopGoodsInfo goodsInfo = new ShopGoodsInfo();
        for (String id : ids) {
            goodsInfo.setId(id);
            goodsInfo.setNews(1);
            shopGoodsInfoService.update(goodsInfo);
//            Map<String, Object> param = new HashMap<>();
//            param.put("goodId", id);
//            param.put("news", 1);
//            List<SolrGoodsInfo> solrGoodsInfoList = shopGoodsInfoService.getAllGoods(param);
//            if (solrGoodsInfoList.size() > 0) {
//                solrTemplate.saveBean(solrGoodsInfoList.get(0));
//            }
        }
//        solrTemplate.commit();
        return Result.ok();
    }

    /**
     * Description: 取消新品
     * @author: sun
     * @Date 下午9:07 2019/5/8
     * @param:
     * @return:
     */
    @RequestMapping("/stopNew")
    @ResponseBody
    public Result<?> stopNew(@RequestBody @Valid IdsParam map) {
        List<String> ids = map.getIds();
        ShopGoodsInfo goodsInfo = new ShopGoodsInfo();
        for (String id : ids) {
            goodsInfo.setId(id);
            goodsInfo.setNews(2);
            shopGoodsInfoService.update(goodsInfo);

        }

        return Result.ok();
    }


    /**
     * Description: 设置推荐商品
     *
     * @author: sun
     * @Date 下午9:06 2019/5/8
     * @param:
     * @return:
     */
    @RequestMapping("/openRecommend")
    @ResponseBody
    public Result<?> openRecommend(@RequestBody @Valid IdsParam map) {
        List<String> ids = map.getIds();
        ShopGoodsInfo goodsInfo = new ShopGoodsInfo();
        for (String id : ids) {
            goodsInfo.setId(id);
            goodsInfo.setRecommend(1);
            shopGoodsInfoService.update(goodsInfo);
//            Map<String, Object> param = new HashMap<>();
//            param.put("goodId", id);
//            param.put("recommend", 1);
//            List<SolrGoodsInfo> solrGoodsInfoList = shopGoodsInfoService.getAllGoods(param);
//            if (solrGoodsInfoList.size() > 0) {
//                solrTemplate.saveBean(solrGoodsInfoList.get(0));
//            }
        }
//        solrTemplate.commit();
        return Result.ok();
    }

    /**
     * Description: 取消推荐商品
     *
     * @author: sun
     * @Date 下午9:07 2019/5/8
     * @param:
     * @return:
     */
    @RequestMapping("/stopRecommend")
    @ResponseBody
    public Result<?> stopRecommend(@RequestBody @Valid IdsParam map) {
        List<String> ids = map.getIds();
        ShopGoodsInfo goodsInfo = new ShopGoodsInfo();
        for (String id : ids) {
            goodsInfo.setId(id);
            goodsInfo.setRecommend(2);
            shopGoodsInfoService.update(goodsInfo);
//            Map<String, Object> param = new HashMap<>();
//            param.put("goodId", id);
//            param.put("recommend", 2);
//            List<SolrGoodsInfo> solrGoodsInfoList = shopGoodsInfoService.getAllGoods(param);
//            if (solrGoodsInfoList.size() > 0) {
//                solrTemplate.saveBean(solrGoodsInfoList.get(0));
//
//            }
        }
//        solrTemplate.commit();
        return Result.ok();
    }

/**
 * Description: 商品图片上传
 * @author: sun
 * @Date 上午10:26 2019/5/28
 * @param:
 * @return:
 */
    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    @ResponseBody
    @PermissionMode(PermissionMode.Mode.White)
    public Result<List> upload(HttpServletRequest request) {
        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver(request.getSession().getServletContext());
        if (multipartResolver.isMultipart(request)) {
            List<ShopGoodsPicture> list = new ArrayList<>();
            List<Map<String, String>> result = new ArrayList<>();
            MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
            String goodId =  multiRequest.getParameter("goodId");
            Iterator iter = multiRequest.getFileNames();
            while (iter.hasNext()) {
                List<MultipartFile> files = multiRequest.getFiles(iter.next().toString());
                for (MultipartFile file : files) {
                    if (file != null) {
                       ShopGoodsPicture shopGoodsPicture = new ShopGoodsPicture();
                        Map<String, String> map = new HashMap<>();
                        String id = KeyUtils.getKey();
                        shopGoodsPicture.setId(id);
                        String suffix = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".") + 1);
                        shopGoodsPicture.setOriginalName(file.getOriginalFilename());
                        shopGoodsPicture.setGoodId(goodId);
//                        atta.setSize(file.getSize());
//                        atta.setType(suffix);
                        shopGoodsPicture.setCreateTime(new Date());
                        try {
                            String path = goodId + "/" + KeyUtils.getKey() + "." + suffix;
                            shopGoodsPicture.setPicture(httpurl+path);

                            Path target = Paths.get(fileUploadDir, path).normalize();
                            if (!Files.exists(target)) {
                                Files.createDirectories(target);
                            }
                            file.transferTo(target.toFile());
                            Runtime.getRuntime().exec("chmod  777 " + fileUploadDir+goodId);
                            Runtime.getRuntime().exec("chmod 777 " + fileUploadDir+path);
//                            Runtime.getRuntime().exec(fileUploadDir+path);
//                            map.put("id", id);
//                            map.put("path", path);
//                            result.add(map);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        list.add(shopGoodsPicture);
                    }
                }
            }
            shopGoodsPictureService.save(list);
//            attachmentService.save(list);
            return Result.ok();
        } else {
            return Result.of(Status.ClientError.UPGRADE_REQUIRED, "无图片");
        }
    }








    /**
     * Description: 启用商城商品
     *
     * @author: sun
     * @Date 下午9:06 2019/5/8
     * @param:
     * @return:
     */
    @RequestMapping("/openHotSell")
    @ResponseBody
    public Result<?> openHotSell(@RequestBody @Valid IdsParam map) {
        List<String> ids = map.getIds();
        ShopGoodsInfo goodsInfo = new ShopGoodsInfo();
        for (String id : ids) {
            goodsInfo.setId(id);
            goodsInfo.setHotSell(1);
            shopGoodsInfoService.update(goodsInfo);
//            Map<String, Object> param = new HashMap<>();
//            param.put("goodId", id);
//            param.put("hotSell", 1);
//            List<SolrGoodsInfo> solrGoodsInfoList = shopGoodsInfoService.getAllGoods(param);
//            if (solrGoodsInfoList.size() > 0) {
//                solrTemplate.saveBean(solrGoodsInfoList.get(0));
//            }
        }
//        solrTemplate.commit();
        return Result.ok();
    }

    /**
     * Description: 禁用商城商品
     *
     * @author: sun
     * @Date 下午9:07 2019/5/8
     * @param:
     * @return:
     */
    @RequestMapping("/stopHotSell")
    @ResponseBody
    public Result<?> stopHotSell(@RequestBody @Valid IdsParam map) {
        List<String> ids = map.getIds();
        ShopGoodsInfo goodsInfo = new ShopGoodsInfo();
        for (String id : ids) {
            goodsInfo.setId(id);
            goodsInfo.setHotSell(2);
            shopGoodsInfoService.update(goodsInfo);

        }
        return Result.ok();
    }


    /**
     * Description: 启用优惠商品
     *
     * @author: sun
     * @Date 下午9:06 2019/5/8
     * @param:
     * @return:
     */
    @RequestMapping("/openUseCoupon")
    @ResponseBody
    public Result<?> openUseCoupon(@RequestBody @Valid IdsParam map) {
        List<String> ids = map.getIds();
        ShopGoodsInfo goodsInfo = new ShopGoodsInfo();
        for (String id : ids) {
            goodsInfo.setId(id);
            goodsInfo.setUseCoupon(1);
            shopGoodsInfoService.update(goodsInfo);
        }

        return Result.ok();
    }

    /**
     * Description: 禁用优惠商品
     *
     * @author: sun
     * @Date 下午9:07 2019/5/8
     * @param:
     * @return:
     */
    @RequestMapping("/stopUseCoupon")
    @ResponseBody
    public Result<?> stopUseCoupon(@RequestBody @Valid IdsParam map) {
        List<String> ids = map.getIds();
        ShopGoodsInfo goodsInfo = new ShopGoodsInfo();
        for (String id : ids) {
            goodsInfo.setId(id);
            goodsInfo.setUseCoupon(2);
            shopGoodsInfoService.update(goodsInfo);

        }
        return Result.ok();
    }



}
