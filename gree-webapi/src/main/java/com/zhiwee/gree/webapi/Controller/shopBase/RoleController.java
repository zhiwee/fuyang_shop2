package com.zhiwee.gree.webapi.Controller.shopBase;

import com.zhiwee.gree.model.Role;
import com.zhiwee.gree.model.User;
import com.zhiwee.gree.model.enums.UserType;
import com.zhiwee.gree.model.ret.RoleReturnMsg;
import com.zhiwee.gree.service.shopBase.RoleService;
import com.zhiwee.gree.webapi.util.SystemControllerLog;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.Page;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.model.enums.YesOrNo;
import xyz.icrab.common.web.annotation.EnableDeleteRequest;
import xyz.icrab.common.web.annotation.EnableGetRequest;
import xyz.icrab.common.web.annotation.EnableListRequest;
import xyz.icrab.common.web.annotation.EnablePageRequest;
import xyz.icrab.common.web.annotation.Session;
import xyz.icrab.common.web.message.ValidatedMessage;
import xyz.icrab.common.web.param.IdParam;
import xyz.icrab.common.web.util.KeyUtils;
import xyz.icrab.common.web.util.Validator;

import xyz.icrab.ums.util.consts.SessionKeys;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author gll
 * @since 2018/4/21 17:11
 */
@RestController
@RequestMapping("/role")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class RoleController {

    @Autowired
    private RoleService roleService;


    @RequestMapping("/add")
    @ResponseBody
    public <T> Result<?> add(@RequestBody(required = false) Map<String, Object> param){
    	String name = (String)param.get("name");
    	String remark = (String)param.get("remark");
    	Validator validator = new Validator();
    	validator.notEmpty(name, RoleReturnMsg.NAME_EMPTY.message());
    	if(validator.isError()){
    		return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
    	}

    	Role role = new Role();
    	role.setId(KeyUtils.getKey());
    	role.setName(name);
    	role.setRemark(remark);
    	role.setState(YesOrNo.YES);
    	role.setCreatetime(new Date());
    	role.setCreateman(null);
    	roleService.save(role);
        return Result.ok();
    }


    @RequestMapping("/update")
    @ResponseBody
    public <T> Result<?> update(@RequestBody(required = false) Map<String, Object> param){
    	String id = (String)param.get("id");
    	String name = (String)param.get("name");
    	String remark = (String)param.get("remark");
    	Validator validator = new Validator();
    	validator.notEmpty(id, ValidatedMessage.EMPTY_ID);
    	validator.notEmpty(name, RoleReturnMsg.NAME_EMPTY.message());
    	if(validator.isError()){
    		return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
    	}
    	Role role = new Role();
    	role = roleService.get(id);
    	if(!role.getName().equals(name) && roleService.checkName(name)){
    		return Result.of(Status.ClientError.BAD_REQUEST, RoleReturnMsg.NAME_MANY);
    	}

    	role.setId(id);
    	role.setName(name);
    	role.setRemark(remark);
    	role.setState(YesOrNo.YES);
    	role.setUpdatetime(new Date());
    	role.setUpdateman(null);
    	roleService.update(role);
        return Result.ok();
    }

    @RequestMapping("/open")
    @ResponseBody
    public <T> Result<?> open(@RequestBody @Valid IdParam map){
        Role role = new Role();
        role.setId(map.getId());
        role.setState(YesOrNo.YES);
    	roleService.update(role);
        return Result.ok();
    }

    @RequestMapping("/stop")
    @ResponseBody
    public <T> Result<?> stop(@RequestBody @Valid IdParam map){
        Role role = new Role();
        role.setId(map.getId());
        role.setState(YesOrNo.NO);
    	roleService.update(role);
        return Result.ok();
    }

    @RequestMapping("/addUserRole")
    @ResponseBody
    public <T> Result<?> addUserRole(@RequestBody(required = false) Map<String, Object> param , @Session(SessionKeys.USER)User user, @Session(SessionKeys.ROLES) List<Role> roles){
    	String roleids = (String)param.get("roleids");
    	String userid = (String)param.get("userid");
    	Validator validator = new Validator();
    	validator.notEmpty(userid, RoleReturnMsg.USERID_EMPTY.message());
    	if(validator.isError()){
    		return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
    	}

        List<String> addRoleIds = null;
        if(StringUtils.isNotBlank(roleids)) {
    	    addRoleIds = Arrays.asList(roleids.split(","));
        }
        roleService.resetUserRole(userid, addRoleIds);
        return Result.ok();
    }

    @RequestMapping("/getPage")
    @ResponseBody
    public <T> Result<Page<Role>> getPage(@RequestBody(required = false) Map<String, Object> param , @Session("user") User user , Pagination pagination){
    	if(user.getType() != UserType.Super){
    		List<Role> userRoleList = roleService.getRoleByUserId(user.getId());
			List<String> roleids = new ArrayList<String>();
			for(Role r : userRoleList){
				roleids.add(r.getId());
			}
			param.put("ids", roleids);
			if(roleids.size() == 0){
				return Result.ok(null);
			}
    	}
    	Page<Role>  result = (Page<Role>) roleService.page(param, pagination);
        return Result.ok(result);
    }



    @RequestMapping("/getList")
    @ResponseBody
    public <T> Result<List<Role>> getList(@RequestBody(required = false) Map<String, Object> param , @Session("user") User user){
    	if(user.getType() != UserType.Super){
    		List<Role> userRoleList = roleService.getRoleByUserId(user.getId());
			List<String> roleids = new ArrayList<String>();
			for(Role r : userRoleList){
				roleids.add(r.getId());
			}
			param.put("ids", roleids);
			if(roleids.size() == 0){
				return Result.ok(null);
			}
    	}
    	List<Role> result = roleService.list(param);
        return Result.ok(result);
    }

    @RequestMapping("/getRoleByUserid")
    @ResponseBody
    public <T> Result<List<Role>> getRoleByUserid(@RequestBody(required = false) Map<String, Object> param , @Session("user") User user){
    	String userid = (String)param.get("userid");
    	Validator validator = new Validator();
    	validator.notEmpty(userid, RoleReturnMsg.USERID_EMPTY.message());
    	if(validator.isError()){
    		return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
    	}
    	List<Role> result = roleService.getRoleByUserId(userid);
        return Result.ok(result);
    }
}
