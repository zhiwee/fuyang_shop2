package com.zhiwee.gree.webapi.mqListenter;

import com.alibaba.fastjson.JSON;
import com.aliyun.oss.model.GroupGrantee;
import com.zhiwee.gree.model.FyGoods.*;
import com.zhiwee.gree.model.GoodsInfo.Goods;
import com.zhiwee.gree.model.GoodsInfo.ShopCartGoods;
import com.zhiwee.gree.model.OrderInfo.OrderItem;
import com.zhiwee.gree.model.User;
import com.zhiwee.gree.model.order.BaseOrder;
import com.zhiwee.gree.service.FyGoods.*;
import com.zhiwee.gree.service.GoodsInfo.GoodsService;
import com.zhiwee.gree.service.OrderInfo.OrderItemService;
import com.zhiwee.gree.webapi.Controller.Wxpay.FyWxPay;
import com.zhiwee.gree.webapi.util.JsonUtil;
import com.zhiwee.gree.webapi.util.SystemControllerLog;
import org.springframework.amqp.core.AcknowledgeMode;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.web.annotation.Session;
import xyz.icrab.ums.util.consts.SessionKeys;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.security.acl.Group;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class OrderMessageListener  implements MessageListener {


    @Autowired
    private RedisTemplate<String,String>  redisTemplate;

      @Resource
      private GoodsService  goodsService;
    @Resource
    private BargainGoodsService  bargainGoodsService;
    @Resource
    private ShareGoodsService  shareGoodsService;
      @Resource
      private FygoodsService   fygoodsService;
      @Resource
      private GrouponGoodsService grouponGoodsService;
      @Resource
      private FyOrderItemService  fyOrderItemService;
      @Resource
      private FyOrderService   fyOrderService;
        @Resource
        private LShopUserGroupService lShopUserGroupService;



    @Override
    public void onMessage(Message message) {
        //获取消息
        String    content = new String(message.getBody());
        List<FygoodsCart>  list    =  JSON.parseArray(content,FygoodsCart.class);
        //实现订单回滚逻辑
        if(list.size()>0){
            rollBack(list);
        }
        System.out.println(new Date()+"本次执行开了" +list.toString());
    }


      /**
       * @Author: jick
       * @Date: 2020/1/7 17:10
       * 数据回滚逻辑
       */
    private   void    rollBack(List<FygoodsCart>  list){
        String orderNum = list.get(0).getOrderNum();
        //首先查询订单信息，判断该订单是否已经支付
            FyOrder  fyOrder  =  new FyOrder();
            fyOrder.setOrderNum(orderNum);
            FyOrder   getOrderInfo =  fyOrderService.getOne(fyOrder);
            //如果付款了且是拼团
            if (null != getOrderInfo&&getOrderInfo.getState()==2){
                FyOrderItem fyOrderItem = new FyOrderItem();
                fyOrderItem.setOrderNum(orderNum);
                fyOrderItem.setType(4);
                fyOrderItem = fyOrderItemService.getOne(fyOrderItem);
                if (fyOrderItem!=null) {
//                拼团失败
                LShopUserGroup lShopUserGroup=new LShopUserGroup();
                lShopUserGroup.setState(0);
                lShopUserGroup.setOrderNum(orderNum);
                lShopUserGroup.setUserId(getOrderInfo.getUserId());
                lShopUserGroup.setGoodsId(fyOrderItem.getGoodsId());
                lShopUserGroup= lShopUserGroupService.getOne(lShopUserGroup);
                    if (null != lShopUserGroup) {
                    //拼团商品
                    GrouponGoods grouponGoods = grouponGoodsService.get(fyOrderItem.getGoodsId());
                        if (grouponGoods != null) {
                            wxPayRefund(fyOrderItem);
                            lShopUserGroupService.delete(lShopUserGroup);
                            grouponGoods.setStoreCount(grouponGoods.getStoreCount() + list.get(0).getCount());
                            grouponGoodsService.update(grouponGoods);
                        }
                    }
                }
                return;//表示该订单已经付款过
            }
            //如果未付款
            if(null != getOrderInfo&&getOrderInfo.getState()==1) {
                //如果未支付，删除未支付订单基本信息
                Map<String, Object> params = new HashMap<>(5);
                //2表示删除
                params.put("isdelete", 2);
                params.put("orderNum", list.get(0).getOrderNum());
                fyOrderService.deleteByOrderNum(params);
                fyOrderItemService.deleteAll(params);
                //库存回滚
                for (FygoodsCart f : list) {
                    //商品
                    Goods goods = goodsService.get(f.getFyGoodsId());
                    ShareGoods shareGoods = shareGoodsService.get(f.getFyGoodsId());
                    //正常商品
                    Fygoods fygoods = fygoodsService.get(f.getFyGoodsId());
                    //拼团商品
                    GrouponGoods grouponGoods = grouponGoodsService.get(f.getFyGoodsId());
                    BargainGoods bargainGoods = bargainGoodsService.get(f.getFyGoodsId());
                    if (null != goods) {
                        //库存还原
                        goods.setStoreCount(goods.getStoreCount() + f.getCount());
                        goodsService.update(goods);
                    }
                    if (null != bargainGoods) {
                        //库存还原
                        bargainGoods.setStoreCount(bargainGoods.getStoreCount() + f.getCount());
                        bargainGoodsService.update(bargainGoods);
                    }
                    if (null != fygoods) {
                        fygoods.setStoreCount(fygoods.getStoreCount() + f.getCount());
                        fygoodsService.update(fygoods);
                    }
                    if (null != grouponGoods) {
                        FyOrderItem fyOrderItem = new FyOrderItem();
                        fyOrderItem.setOrderNum(list.get(0).getOrderNum());
                        fyOrderItem = fyOrderItemService.getOne(fyOrderItem);
                        if (fyOrderItem != null) {
                            wxPayRefund(fyOrderItem);
                        }
                        grouponGoods.setStoreCount(grouponGoods.getStoreCount() + f.getCount());
                        grouponGoodsService.update(grouponGoods);
                    }
                    if (null != shareGoods) {
                        shareGoods.setStoreCount(shareGoods.getStoreCount() + f.getCount());
                        shareGoodsService.update(shareGoods);
                    }
                }
            }
    }



    public Result<?> wxPayRefund(FyOrderItem fyOrderItem) {
        //非空判断
        if (null != fyOrderItem) {
            //查询订单表，
            Map<String, Object> map = new HashMap<>();
            map.put("orderNum", fyOrderItem.getOrderNum());
            FyOrder getOrderInfo = fyOrderService.getOne(map);
            BigDecimal payPrice = getOrderInfo.getPayPrice().multiply(new BigDecimal(100));
            //支付金额减去已经退款的金额
            BigDecimal total_fee = payPrice.stripTrailingZeros();
            if (getOrderInfo.getState() ==1) {
                return Result.of(Status.ClientError.BAD_REQUEST, "只有支付的订单才可以退款");
            }
            if (total_fee.compareTo(fyOrderItem.getPayPrice()) < 0) {
                return Result.of(Status.ClientError.BAD_REQUEST, "退款金额异常");
            }
            getOrderInfo.setRefoundPrice(fyOrderItem.getPayPrice());
            //调用微信退款
            Result<?> result = FyWxPay.wxPayRefund(getOrderInfo,fyOrderItem);

            //如果退款成功，该改变订单状态
//            System.out.println(result.getCode());
            if ("200".equals(result.getCode())) {
                //退款逻辑
                returnOrder(fyOrderItem);
                return Result.ok("退款成功");
            }
        }
        return Result.of(Status.ClientError.BAD_REQUEST, "退款失败");
    }

    /**
     * 退款逻辑
     */
    public void returnOrder(FyOrderItem fyOrderItem) {
        //改变商品基本表信息
        Map<String, Object> param = new HashMap<>();
        param.put("orderNum", fyOrderItem.getOrderNum());
        //根据订单号查询基本订单信息
        FyOrder fyOrder = fyOrderService.getOne(param);
        //退款金额更新
        fyOrder.setRefoundPrice(fyOrder.getRefoundPrice().add(fyOrderItem.getPayPrice()));
        //1表示包含退款订单
        fyOrder.setIshasRefound(2);
        if ((fyOrder.getRefoundPrice().compareTo(fyOrder.getPayPrice())) == 0) {
            //4表示表示全额退款
            fyOrder.setState(4);
        } else {
            //3表示部分退款
            fyOrder.setState(3);
        }
        //退单时间
        fyOrder.setRefoundTime(new Date());
        fyOrderService.update(fyOrder);
        //更新订单基本表信息
        //已确认退款
        fyOrderItem.setRefoundState(2);
        fyOrderItem.setOperatorName("订单超时");
        fyOrderItemService.update(fyOrderItem);
    }
}
