package com.zhiwee.gree.webapi.Controller.card;

import com.zhiwee.gree.model.FyGoods.FyOrder;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.User;
import com.zhiwee.gree.model.WxPay.WeChatConfig;
import com.zhiwee.gree.model.WxPay.wxPayFuYang;
import com.zhiwee.gree.model.card.*;
import com.zhiwee.gree.model.card.vo.CheckCY;
import com.zhiwee.gree.model.card.vo.CheckCard;
import com.zhiwee.gree.service.GoodsInfo.GoodsCategoryService;
import com.zhiwee.gree.service.ShopUser.ShopUserService;
import com.zhiwee.gree.service.card.*;
import com.zhiwee.gree.webapi.Controller.Wxpay.PayUtil;
import com.zhiwee.gree.webapi.Controller.Wxpay.WXPay2;
import com.zhiwee.gree.webapi.Controller.Wxpay.WxpayController;
import com.zhiwee.gree.webapi.util.*;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.jeecgframework.poi.excel.ExcelExportUtil;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.enmus.ExcelType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.util.KeyUtils;
import xyz.icrab.common.web.annotation.EnableGetRequest;
import xyz.icrab.common.web.annotation.EnableListRequest;
import xyz.icrab.common.web.annotation.PermissionMode;
import xyz.icrab.common.web.annotation.Session;
import xyz.icrab.common.web.param.IdParam;
import xyz.icrab.ums.util.consts.SessionKeys;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.*;

@RequestMapping("/cardInfo")
@Controller
@ResponseBody
@EnableListRequest
@EnableGetRequest
/**
 * @author tzj
 */
public class CardInfoController {

    @Autowired
    private CardInfoService cardInfoService;
    @Autowired
    private LShopUserCardsService lShopUserCardsService;
    @Autowired
    private LShopUserCardWxPayService lShopUserCardWxPayService;
    @Autowired
    private UserCardsInfoService userCardsInfoService;
    @Autowired
    private GoodsCategoryService goodsCategoryService;
    @Autowired
    private ShopUserService shopUserService;
    @Autowired
    private DepartmentInfoService departmentInfoService;


    /**
     * @return
     * @author tzj
     * @description 珍藏卡页面
     * @date 2019/12/11 12:55
     */
    @RequestMapping(value = "/infoPage")
    public Result<?> infoPage(@RequestBody Map<String, Object> params, Pagination pagination) {
        if (params == null) {
            params = new HashMap<>();
        }
        Pageable<CardInfo> page = cardInfoService.pageInfo(params, pagination);
        return Result.ok(page);
    }

    /**
     * @return
     * @description 购卡信息页面
     * @date 2019/12/11 12:55
     */
    @RequestMapping(value = "/userCardinfoPage")
    public Result<?> userCardsinfoPage(@RequestBody Map<String, Object> params, Pagination pagination) {
        if (params == null) {
            params = new HashMap<>();
        }

        if (StringUtils.isNotBlank((String) params.get("startCreateTime"))) {
            params.put("startCreateTime", (String) params.get("startCreateTime") + " 00:00:00");
        }

        if (StringUtils.isNotBlank((String) params.get("endCreateTime"))) {
            params.put("endCreateTime", (String) params.get("endCreateTime") + " 23:59:59");
        }
        if (StringUtils.isNotBlank((String) params.get("startPayTime"))) {
            params.put("startPayTime", (String) params.get("startPayTime") + " 00:00:00");
        }
        if (StringUtils.isNotBlank((String) params.get("startPayTime"))) {
            params.put("endPayTime", (String) params.get("endPayTime") + " 23:59:59");
        }

        Pageable<LShopUserCards> page = lShopUserCardsService.getLShopUserCardsList(params, pagination);
        return Result.ok(page);
    }

    /**
     * Description: 购卡信息导出
     *
     * @Date 下午4:36 2019/12/12
     * @param:
     * @return:
     */
    @RequestMapping("/exportUserCards")
    public void export(HttpServletRequest req, HttpServletResponse rsp) throws IOException {
        Map<String, Object> params = new HashMap<>(10);
        params.put("startCreateTime", req.getParameter("startCreateTime"));
        params.put("endCreateTime", req.getParameter("endCreateTime"));
        params.put("startPayTime", req.getParameter("startPayTime"));
        params.put("endPayTime", req.getParameter("endPayTime"));
        params.put("operatorUserName", req.getParameter("operatorUserName"));
        params.put("operatorNickName", req.getParameter("operatorNickName"));
        params.put("userName", req.getParameter("userName"));
        params.put("phone", req.getParameter("phone"));

        if (StringUtils.isNotBlank((String) params.get("startCreateTime"))) {
            params.put("startCreateTime", (String) params.get("startCreateTime") + " 00:00:00");
        }
        if (StringUtils.isNotBlank((String) params.get("endCreateTime"))) {
            params.put("endCreateTime", (String) params.get("endCreateTime") + " 23:59:59");
        }
        if (StringUtils.isNotBlank((String) params.get("startPayTime"))) {
            params.put("startPayTime", (String) params.get("startPayTime") + " 00:00:00");
        }
        if (StringUtils.isNotBlank((String) params.get("startPayTime"))) {
            params.put("endPayTime", (String) params.get("endPayTime") + " 23:59:59");
        }

        List<LShopUserCardsExrt> result;
        result = lShopUserCardsService.queryLShopUserCardsExport(params);

        // 导出Excel
        OutputStream os = rsp.getOutputStream();

        rsp.setContentType("application/x-download");
        // 设置导出文件名称
        rsp.setHeader("Content-Disposition",
                "attachment;filename=" + DateFormatUtils.format(new Date(), "yyyyMMddHHmmssSSS") + ".xlsx");
        ExportParams param = new ExportParams("购卡信息", "购卡信息", "购卡信息表");
        param.setAddIndex(true);
        param.setType(ExcelType.XSSF);
        Workbook excel = ExcelExportUtil.exportExcel(param, LShopUserCardsExrt.class, result);
        excel.write(os);
        os.flush();
        os.close();
    }

    @RequestMapping("/delete")
    @ResponseBody
    public Result<?> delete(@RequestBody CardInfo cardInfo) {
        cardInfoService.delete(cardInfo);
        return Result.ok();
    }

    @RequestMapping(value = "/add")
    public Result<?> add(@Session(SessionKeys.USER) User user, @RequestBody(required = false) CardInfo cardInfo) {
        cardInfo.setId(KeyUtils.getUUID());
        cardInfo.setCreatetime(new Date());
        cardInfo.setCreateman(user.getId());
        cardInfoService.save(cardInfo);
        return Result.ok();
    }

    @RequestMapping(value = "/get")
    public Result<?> get(@Session(SessionKeys.USER) User user, @RequestBody(required = false) CardInfo cardInfo) {
        cardInfo.setUpdateman(user.getId());
        CardInfo one = cardInfoService.getInfo(cardInfo);
        return Result.ok(one);
    }

    @RequestMapping(value = "/update")
    @SuppressWarnings("all")
    public Result<?> update(@Session(SessionKeys.USER) User user, @RequestBody(required = false) CardInfo cardInfo) {
        cardInfo.setUpdatetime(new Date());
        cardInfo.setUpdateman(user.getId());
        cardInfoService.update(cardInfo);
        return Result.ok();
    }

    /**
     * @author tzj
     * @description 用户购买珍藏卡
     * @date 2019/12/11 12:55
     */
    @SystemControllerLog(description = "用户购买珍藏卡")
//    @RequestMapping(value = "/addShopUser")
    @RequestMapping(value = "/addShopUserClose")
    public Result<?> addShopUser(@Session("ShopUser") ShopUser user) {
        Map<String, Object> param = new HashMap<>();
        if (user.getPhone() == null || "".equals(user.getPhone())) {
            return Result.of(Status.ClientError.BAD_REQUEST, "请您先绑定手机号码");
        }

        LShopUserCards oldCard = null;
        param.put("phone", user.getPhone());
        List<LShopUserCards> list = lShopUserCardsService.list(param);
        if (!list.isEmpty()) {
            for (LShopUserCards card : list) {
                if (card.getState() == 1 || card.getState() == 2 || card.getState() == 3) {
                    return Result.of(Status.ClientError.BAD_REQUEST, "您已经购买过本产品,请支付或领取后直接使用");
                } else if (card.getState() == 6) {
                    return Result.of(Status.ClientError.BAD_REQUEST, "您正在申请退卡中，无法重复购买");
                } else if (card.getState() == 4 || card.getState() == 5) {
                    oldCard = card;
                }
            }
        }
        LShopUserCards lShopUserCards;
        //如果之前买过、则使用之前的那条记录
        if (oldCard != null) {
            lShopUserCards = oldCard;
        } else {
            lShopUserCards = new LShopUserCards();
            lShopUserCards.setId(KeyUtils.getKey());
        }
        lShopUserCards.setShopUserId(user.getId());
        lShopUserCards.setUsername(user.getUsername());
        lShopUserCards.setPhone(user.getPhone());
        lShopUserCards.setCreatetime(new Date());
        lShopUserCards.setState(1);
        //是否直接加券
        addShopUserCards(user);

        if (oldCard != null) {
            lShopUserCardsService.update(lShopUserCards);
        } else {
            lShopUserCardsService.save(lShopUserCards);
        }
        return Result.ok("购买成功，请支付");
    }

    /**
     * @author tzj
     * @description 珍藏卡绑定推荐券号
     * @date 2019/12/11 12:55
     */
    @RequestMapping(value = "/bindTicket")
    public Result<?> bindTicket(@Session("ShopUser") ShopUser user, @RequestBody Map<String, Object> params) {
        Map<String, Object> map = new HashMap<>();
        map.put("shopUserId", (String) user.getId());
        LShopUserCards lShopUserCards = lShopUserCardsService.getOne(map);
        if (lShopUserCards != null) {
            lShopUserCards.setTicket((String) params.get("ticket"));
            lShopUserCardsService.update(lShopUserCards);
            return Result.ok("绑定券号成功");
        } else {
            return Result.of(Status.ClientError.BAD_REQUEST, "您尚未购买珍藏卡");
        }
    }

    /**
     * @return
     * @author tzj
     * @description 用户添加珍藏卡的优惠券
     * @date 2019/12/14 0:25
     */
    private void addShopUserCards(ShopUser user) {
        List<UserCardsInfo> userCardsInfos = new ArrayList<>();
        List<CardInfo> cardList = cardInfoService.list();
        for (CardInfo cardInfo : cardList) {
            UserCardsInfo userCardsInfo = new UserCardsInfo();
            userCardsInfo.setId(KeyUtils.getKey());
            userCardsInfo.setName(cardInfo.getName());
            userCardsInfo.setCategoryCodes(cardInfo.getCategoryCodes());
            userCardsInfo.setDepartmentCodes(cardInfo.getDepartmentCodes());
            userCardsInfo.setMinMoney(cardInfo.getMinMoney());
            userCardsInfo.setUseMoney(cardInfo.getUseMoney());
            userCardsInfo.setShopUserId(user.getId());
            userCardsInfo.setMonth(getCurrentMouth());
            userCardsInfo.setState(1);
            String lastDay = getLastDay();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date dateTime = null;
            try {
                dateTime = simpleDateFormat.parse(lastDay);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            userCardsInfo.setBeginTime(new Date());
            userCardsInfo.setEndTime(dateTime);
            userCardsInfos.add(userCardsInfo);
        }
        userCardsInfoService.save(userCardsInfos);
    }

    /**
     * 获取本月最后1天
     *
     * @return
     */
    private String getLastDay() {
        Calendar cale = Calendar.getInstance();
   /*     int year = cale.get(Calendar.YEAR);
        int month = cale.get(Calendar.MONTH) + 1;
        int day = cale.get(Calendar.DATE);
        int hour = cale.get(Calendar.HOUR_OF_DAY);
        int minute = cale.get(Calendar.MINUTE);
        int second = cale.get(Calendar.SECOND);*/

        // 获取当月第一天和最后一天
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String firstday, lastday;
        // 获取前月的第一天
        cale = Calendar.getInstance();
        cale.add(Calendar.MONTH, 0);
        cale.set(Calendar.DAY_OF_MONTH, 1);
        firstday = format.format(cale.getTime());
        // 获取前月的最后一天
        cale = Calendar.getInstance();
        cale.add(Calendar.MONTH, 1);
        cale.set(Calendar.DAY_OF_MONTH, 0);
        lastday = format.format(cale.getTime());
        System.out.println("本月第一天和最后一天分别是 ： " + firstday + " and " + lastday);

        // 获取当前日期字符串
        Date d = new Date();
        /*System.out.println("当前日期字符串1：" + format.format(d));
        System.out.println("当前日期字符串2：" + year + "-" + month + "-" + day + " "
             + hour + ":" + minute + ":" + second);  */
        return (lastday + " 23:59:59");
    }

//    /**
//     * 定时更新优惠券
//     */
//    @Scheduled(cron = "0 0/1 * * * ?")
//    public void printTrest() {
//        System.out.println("定时任务执行了测试的" + new Date());
//    }
    /**
     * 每月1日1点钟触发
     */
    @Scheduled(cron = "0 0 1 1 * ?")
//    @PermissionMode(PermissionMode.Mode.White)
    public void updateUserCardInfos() {
        //找到所有优惠卡信息
        List<CardInfo> cardList = cardInfoService.list();
        //找到所有购买了珍藏卡的用
        List<LShopUserCards> userList = lShopUserCardsService.list();
        for (LShopUserCards lShopUserCards : userList) {
            if (lShopUserCards.getState() == 1 || lShopUserCards.getState() == 2 || lShopUserCards.getState() == 6) {
                int state;
                if (lShopUserCards.getState() == 6) {
                    state = 2;
                } else {
                    state = lShopUserCards.getState();
                }
                List<UserCardsInfo> userCards = new ArrayList<>();
                for (CardInfo cardInfo : cardList) {
                    UserCardsInfo userCardsInfo = new UserCardsInfo();
                    userCardsInfo.setId(KeyUtils.getKey());
                    userCardsInfo.setName(cardInfo.getName());
                    userCardsInfo.setCategoryCodes(cardInfo.getCategoryCodes());
                    userCardsInfo.setDepartmentCodes(cardInfo.getDepartmentCodes());
                    userCardsInfo.setMinMoney(cardInfo.getMinMoney());
                    userCardsInfo.setUseMoney(cardInfo.getUseMoney());
                    userCardsInfo.setShopUserId(lShopUserCards.getShopUserId());
                    userCardsInfo.setBeginTime(new Date());
                    userCardsInfo.setState(state);
                    userCardsInfo.setMonth(getCurrentMouth());
                    String lastDay = getLastDay();
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date dateTime = null;
                    try {
                        dateTime = simpleDateFormat.parse(lastDay);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    userCardsInfo.setEndTime(dateTime);
                    userCards.add(userCardsInfo);
                }
                userCardsInfoService.save(userCards);
            }
            System.out.println("定时【添加券】任务结束" + new Date());
        }
    }

    @RequestMapping("/checkUserCardInfo")
    @PermissionMode(PermissionMode.Mode.White)
    @ResponseBody
    public Result<?> checkUserCardInfo(HttpServletRequest request, @RequestBody Map<String, Object> param) {
        List<LShopUserCards> list = lShopUserCardsService.list(param);
        if (!list.isEmpty()) {
            return Result.ok(list.get(0));
        } else {
            return Result.of(Status.ClientError.BAD_REQUEST, "您尚未购买过本产品");
        }
    }

    @RequestMapping("/payUserCardInfo")
    @PermissionMode(PermissionMode.Mode.White)
    @ResponseBody
    public Result<?> payUserCardInfo(@Session(SessionKeys.USER) User user,
//                                     @Session(SessionKeys.ROLES) List<Role> roles,
                                     HttpServletRequest request,
                                     @RequestBody Map<String, Object> param) {
        Map<String, Object> map = new HashMap<>();
        map.put("id", (String) param.get("id"));
        LShopUserCards shopUserCard = lShopUserCardsService.getOne(map);
        if (shopUserCard != null) {
            shopUserCard.setState(2);
            shopUserCard.setMoney(300d);
            shopUserCard.setPayTime(new Date());
            shopUserCard.setOperatorUserid(user.getId());
            shopUserCard.setOperatorUsername(user.getUsername());
            shopUserCard.setOperatorNickname(user.getNickname());
            shopUserCard.setCardcode((String) param.get("cardcode"));
            //查询用户当前月的未支付的优惠券
            param.clear();
            param.put("shopUserId", shopUserCard.getShopUserId());
//            param.put("month", LocalDate.now().getMonthValue());
            param.put("month", getCurrentMouth());
            param.put("state", 1);
            List<UserCardsInfo> list = userCardsInfoService.list(param);
            if (list != null) {
                for (UserCardsInfo cardsInfo : list) {
                    cardsInfo.setState(2);
                    userCardsInfoService.update(cardsInfo);
                }
            }
            lShopUserCardsService.update(shopUserCard);
            return Result.of(Status.Success.OK, "支付成功");
        } else {
            return Result.of(Status.ClientError.BAD_REQUEST, "您尚未购买过本产品");
        }
    }

    @SystemControllerLog(description = "长益校验优惠券")
    @RequestMapping("/checkCoupons")
    @PermissionMode(PermissionMode.Mode.White)
    @ResponseBody
    public Result<?> checkCoupons(HttpServletRequest request, @RequestBody CheckCard checkCard) {
        //超市分类订单
        List<CheckCY> csList = new ArrayList<>(50);
        //百购家电分类订单
        List<CheckCY> bgJdList = new ArrayList<>(50);

        //满足条件的订单
        List<CheckCY> satisfyBj = new ArrayList<>(100);
        List<CheckCY> satisfyCs = new ArrayList<>(100);
        //错误订单
        List<CheckCY> result = new ArrayList<>(50);
        //当前月份
        int month = getCurrentMouth();
        //判断用户ID
        if (checkCard.getId() == null || ("").equals(checkCard.getId())) {
            return Result.of(Status.ClientError.BAD_REQUEST, "未接收到用户唯一值");
        }

        //确认客户信息
        ShopUser shopUser = new ShopUser();
        shopUser.setId(checkCard.getId());
        shopUser = shopUserService.getOne(shopUser);
        Date checkTime = shopUser.getCheckTime();
        long time = checkTime.getTime();
        long time1 = (new Date()).getTime();
        long l = (time1 - time) / 60/1000;
        if (l>3){
            return Result.of(Status.ClientError.BAD_REQUEST, "超时!请重新获取二维码");
        }



        if (shopUser == null) {
            return Result.of(Status.ClientError.BAD_REQUEST, "未能通过唯一值获取用户信息，请校验");
        }

        //确认客户购买了优惠券
        LShopUserCards lShopUserCards = new LShopUserCards();
        lShopUserCards.setShopUserId(checkCard.getId());
        lShopUserCards.setState(2);
        lShopUserCards = lShopUserCardsService.getOne(lShopUserCards);
        if (lShopUserCards == null) {
            return Result.of(Status.ClientError.BAD_REQUEST, "该顾客未购买或未支付珍藏卡");
        }
        //找到当前用月份可用的优惠券
        Map<String, Object> param = new HashMap<>(5);
        //可用
        param.put("state", 2);
        //当前月份
        param.put("month", month);
        param.put("shopUserId", shopUser.getId());
        List<UserCardsInfo> userCardsInfoList = userCardsInfoService.list(param);
        if (userCardsInfoList.isEmpty()) {
            return Result.of(Status.ClientError.BAD_REQUEST, "该用户当前没有可以使用的优惠券");
        }
        //判断传过来的订单信息
        if (checkCard.getList() == null || checkCard.getList().isEmpty()) {
            return Result.of(Status.ClientError.BAD_REQUEST, "未获取订单信息");
        }

        List<CheckCY> list = checkCard.getList();
        list.removeIf(next -> next.getDepartmentCode() == null || "".equals(next.getDepartmentCode()) || "".equals(next.getCategoryCode()) || next.getCategoryCode() == null
                || next.getTotalPrice() == null || next.getTotalPrice() < 0);

        if (list.isEmpty()) {
            return Result.of(Status.ClientError.BAD_REQUEST, "订单信息传值错误");
        }

        //区分 BG JD CS分类
        for (CheckCY checkCy : list) {
            //获取开头的两个字符串
            String head2char = get2Char(checkCy.getCategoryCode());
            if ("CS".equals(head2char)) {
                csList.add(checkCy);
            } else if ("BG".equals(head2char) || "JD".equals(head2char)) {
                bgJdList.add(checkCy);
            } else {
                checkCy.setState(0);
                checkCy.setMsg("未找到分类头字符");
            }
        }

        if (csList.isEmpty() && bgJdList.isEmpty()) {
            return Result.of(Status.ClientError.BAD_REQUEST, "订单分类前两个字符必须用业态");
        }
        //可以使用的优惠券
        if (!bgJdList.isEmpty()) {
            //用来移除满足条件的订单
            List<UserCardsInfo> needRemove = new ArrayList<>();
            Iterator<UserCardsInfo> iterator = userCardsInfoList.iterator();
            if (userCardsInfoList.size() > 0) {
                for (UserCardsInfo userCardsInfo : userCardsInfoList) {
                    List<CheckCY> remark = new ArrayList<>();
                    ;  //把部门和分类切割成单个
                    String departmentCodes = userCardsInfo.getDepartmentCodes();
                    String categoryCodes = userCardsInfo.getCategoryCodes();
                    String[] systemDepartmentCodes = departmentCodes.split(",");
                    String[] systemCategoryCodes = categoryCodes.split(",");
                    //循环订单 找到可以使用同一张优惠券的
                    for (CheckCY checkCy : bgJdList) {
                        String cyDepartmentCode = checkCy.getDepartmentCode();
                        String cyCategoryCode = checkCy.getCategoryCode();
                        //判断订单的部门code和优惠券的部门Code是否相同
                        newOne:
                        for (String systemDepartmentCode : systemDepartmentCodes) {
                            boolean deFlg = checkHomology(systemDepartmentCode, cyDepartmentCode);
                            if (deFlg) {
                                //判断分类code和订单的分类code是佛相同
                                for (String systemCategoryCode : systemCategoryCodes) {
                                    boolean catFlg = checkHomology(systemCategoryCode, cyCategoryCode);
                                    if (catFlg) {
                                        remark.add(checkCy);
                                        break newOne;
                                    }
                                }
                            }
                        }
                    }
                    //如果有订单可以使用该优惠券 想加
                    if (!remark.isEmpty()) {
                        double allMoney = 0.0;
                        int currMoney = 0;
                        for (CheckCY checkCY : remark) {
                            allMoney = allMoney + checkCY.getTotalPrice();
                        }
                        //满足条件
                        if (allMoney >= userCardsInfo.getMinMoney() * 100) {
                            needRemove.add(userCardsInfo);
                            for (int i = 0; i < remark.size(); i++) {
                                double v = remark.get(i).getTotalPrice() / allMoney * userCardsInfo.getUseMoney();
                                if (i == remark.size() - 1) {
                                    remark.get(i).setCanUseMoney((userCardsInfo.getUseMoney() - currMoney) * 100);
                                    remark.get(i).setCouponId(userCardsInfo.getId());
                                    satisfyBj.add(remark.get(i));
                                    for (CheckCY checkCY : remark) {
                                        bgJdList.removeIf(next -> next.equals(checkCY));

                                        /* Iterator<CheckCY> bgJdListIterator = bgJdList.iterator();
                                           while (bgJdListIterator.hasNext()) {
                                            CheckCY next = bgJdListIterator.next();
                                            if (next.equals(checkCY)) {
                                                bgJdListIterator.remove();
                                            }
                                        } */
                                    }
                                    break;
                                }
                                int i1 = (new Double(v)).intValue();
                                currMoney = currMoney + i1;
                                remark.get(i).setCanUseMoney(i1 * 100);
                                remark.get(i).setCouponId(userCardsInfo.getId());
                                satisfyBj.add(remark.get(i));
                                for (CheckCY checkCY : remark) {
                                    bgJdList.removeIf(next -> next.equals(checkCY));

                                }
                            }
                        }
                    }

                    if (userCardsInfoList.size() <= 0) {
                        break;
                    }
                    if (bgJdList.size() <= 0) {
                        break;
                    }
                }
                //必须在循环外删除
                while (iterator.hasNext()) {
                    UserCardsInfo next = iterator.next();
                    for (UserCardsInfo userCardsInfo1 : needRemove) {
                        if (next.equals(userCardsInfo1)) {
                            iterator.remove();
                        }
                    }
                }

            }
        }


        //可以使用的优惠券
        if (!csList.isEmpty()) {
            //用来移除满足条件的订单
            List<UserCardsInfo> needRemove = new ArrayList<>();
            Iterator<UserCardsInfo> iterator = userCardsInfoList.iterator();
            if (userCardsInfoList.size() > 0) {
                oldMan:
                for (UserCardsInfo userCardsInfo : userCardsInfoList) {
                    List<CheckCY> remark = new ArrayList<>();
                    String departmentCodes = userCardsInfo.getDepartmentCodes();
                    String categoryCodes = userCardsInfo.getCategoryCodes();
                    String[] systemDepartmentCodes = departmentCodes.split(",");
                    String[] systemCategoryCodes = categoryCodes.split(",");
                    //循环订单
                    for (CheckCY checkCy : csList) {
                        String cyDepartmentCode = checkCy.getDepartmentCode();
                        String cyCategoryCode = checkCy.getCategoryCode();
                        //判断订单的部门code和优惠券的部门Code是否相同
                        newOne:
                        for (String systemDepartmentCode : systemDepartmentCodes) {
                            boolean deFlg = checkHomology(systemDepartmentCode, cyDepartmentCode);
                            if (deFlg) {
                                //判断分类code和订单的分类code是佛相同
                                for (String systemCategoryCode : systemCategoryCodes) {
                                    boolean catFlg = checkHomologyCs(systemCategoryCode, cyCategoryCode);
                                    if (catFlg) {
                                        remark.add(checkCy);
                                        break newOne;
                                    }
                                }
                            }
                        }
                    }
                    //如果有订单可以使用该优惠券 想加
                    if (!remark.isEmpty()) {
                        int currMoney = 0;
                        double allMoney = 0.0;
                        for (CheckCY checkCY : remark) {
                            allMoney = allMoney + checkCY.getTotalPrice();
                        }
                        //满足条件
                        if (allMoney >= userCardsInfo.getMinMoney() * 100) {
                            needRemove.add(userCardsInfo);
                            for (int i = 0; i < remark.size(); i++) {
                                double v = remark.get(i).getTotalPrice() / allMoney * userCardsInfo.getUseMoney();
                                if (i == remark.size() - 1) {
                                    remark.get(i).setCanUseMoney((userCardsInfo.getUseMoney() - currMoney) * 100);
                                    remark.get(i).setCouponId(userCardsInfo.getId());
                                    satisfyBj.add(remark.get(i));
                                    for (CheckCY checkCY : remark) {
                                        csList.removeIf(next -> next.equals(checkCY));
                                    }
                                    break;
                                }
                                int i1 = (new Double(v)).intValue();
                                currMoney = currMoney + i1;
                                remark.get(i).setCanUseMoney(i1 * 100);
                                remark.get(i).setCouponId(userCardsInfo.getId());
                                satisfyCs.add(remark.get(i));
                                for (CheckCY checkCY : remark) {
                                    csList.removeIf(next -> next.equals(checkCY));
                                }
                            }
                        }
                    }
                    //必须在循环外删除

                    if (userCardsInfoList.size() <= 0) {
                        break;
                    }
                    if (csList.size() <= 0) {
                        break;
                    }
                }
                while (iterator.hasNext()) {
                    UserCardsInfo next = iterator.next();
                    for (UserCardsInfo userCardsInfo1 : needRemove) {
                        if (next.equals(userCardsInfo1)) {
                            iterator.remove();
                        }
                    }
                }
            }
        }


        if (satisfyBj.isEmpty() && satisfyCs.isEmpty()) {
            return Result.of(Status.ClientError.BAD_REQUEST, "未找到可以使用的优惠券");
        }
        result.addAll(satisfyBj);
        result.addAll(satisfyCs);
        return Result.ok(result);
    }

    private int getCurrentMouth() {
        Calendar cal = Calendar.getInstance();
        return cal.get(Calendar.MONTH) + 1;
    }

    /**
     * @author tzj
     * @description 判断是否同源
     * @date 2019/12/18 12:32
     */
    private boolean checkHomology(String systemCode, String cyCode) {
        boolean flg = false;
        //换成字符串数组
        String[] systemCodes = getStr2char(systemCode);
        String[] cyCodes = getStr2char(cyCode);
        //循环我们系统优惠券的部门数组
        for (int i = 0; i < systemCodes.length; i++) {
            if (i < cyCodes.length) {
                if (!cyCodes[i].equals(systemCodes[i])) {
                    flg = false;
                    break;
                } else {
                    flg = true;
                }
            } else {
                flg = false;
            }

        }


//        boolean flg = false;
//        //换成字符串数组
//        String[] gets = getStr2char(str1);
//        String[] gets1 = getStr2char(str2);
//        //循环我们系统优惠券的部门数组
//        for (int i = 0; i < gets1.length; i++) {
//
//            if (i < gets.length) {
//                if (!gets1[i].equals(gets[i])) {
//                    flg = false;
//                    break;
//                } else {
//                    flg = true;
//                }
//
//            } else {
//                break;
//            }
//        }
        return flg;
    }

    /**
     * @author tzj
     * @description 获取分类的前两个字符串
     * @date 2019/12/18 12:55
     */
    private String get2Char(String categoryCode) {
        String[] str = getStr2char(categoryCode);
        return str[0];
    }

    /**
     * @author tzj
     * @description 把字符串分隔每两个一组的数组
     * @date 2019/12/18 11:54
     */
    private String[] getStr2char(String str) {
        //字符串
        int m = str.length() / 2;
        if (m * 2 < str.length()) {
            m++;
        }
        String[] strS = new String[m];
        int j = 0;
        for (int i = 0; i < str.length(); i++) {
            //每隔两个
            if (i % 2 == 0) {
                strS[j] = "" + str.charAt(i);
            } else {
                //将字符加上两个空格
                strS[j] = strS[j] + "" + str.charAt(i);
                j++;
            }
        }
        return strS;
    }

    private char[] getStr1char(String str1) {
        char[] ss = str1.toCharArray();
        return ss;
    }

    private boolean checkHomologyCs(String str1, String str2) {
        boolean flg = false;
        //换成字符串数组
        char[] gets = getStr1char(str1);
        char[] gets1 = getStr1char(str2);
        //循环我们系统优惠券的部门数组
        for (int i = 0; i < gets.length; i++) {
            if (i < gets1.length) {
                if (gets[i] != gets1[i]) {
                    flg = false;
                    break;
                } else {
                    flg = true;
                }
            }
        }
        return flg;
    }


    @SystemControllerLog(description = "长益使用优惠券")
    @RequestMapping("/useCoupons")
    @PermissionMode(PermissionMode.Mode.White)
    @ResponseBody
    public Result<?> useCoupon(HttpServletRequest request, @RequestBody List<UseCouponBean> list) {
        if (list.size() <= 0) {
            return Result.of(Status.ClientError.BAD_REQUEST, "未接收到优惠券信息");
        }
        List<ResultCy> result = new ArrayList<>(20);
        for (UseCouponBean bean : list) {
            UserCardsInfo userCardsInfo = new UserCardsInfo();
            userCardsInfo.setId(bean.getId());
            userCardsInfo = userCardsInfoService.getOne(userCardsInfo);
            if (userCardsInfo != null && userCardsInfo.getState() == 2) {
                userCardsInfo.setCostMoney(bean.getCostMoney());
                ResultCy resultCy = new ResultCy();
                userCardsInfo.setState(3);
                userCardsInfo.setCreatetime(new Date());
                userCardsInfoService.update(userCardsInfo);
                resultCy.setId(bean.getId());
                resultCy.setState(1);
                resultCy.setMsg("成功");
                result.add(resultCy);
            } else {
                ResultCy resultCy = new ResultCy();
                resultCy.setId(bean.getId());
                resultCy.setState(0);
                resultCy.setMsg("未找到优惠券或该优惠券已经使用了");
                result.add(resultCy);
            }
        }

        return Result.ok(result);
    }

    @SystemControllerLog(description = "长益回退优惠券")
    @RequestMapping("/backCoupons")
    @PermissionMode(PermissionMode.Mode.White)
    @ResponseBody
    public Result<?> backCoupons(HttpServletRequest request, @RequestBody String[] ids) {

        if (ids.length <= 0) {
            return Result.of(Status.ClientError.BAD_REQUEST, "未接收到优惠券信息");
        }
        List<ResultCy> result = new ArrayList<>(20);

        for (String id : ids) {
            UserCardsInfo userCardsInfo = new UserCardsInfo();
            userCardsInfo.setId(id);
            userCardsInfo = userCardsInfoService.getOne(userCardsInfo);
            if (userCardsInfo != null && userCardsInfo.getState() == 3) {
                ResultCy resultCy = new ResultCy();
                userCardsInfo.setState(2);
                userCardsInfoService.update(userCardsInfo);
                resultCy.setId(id);
                resultCy.setState(1);
                resultCy.setMsg("成功");
                result.add(resultCy);
            } else {
                ResultCy resultCy = new ResultCy();
                resultCy.setId(id);
                resultCy.setState(0);
                resultCy.setMsg("未找到优惠券或该优惠券未使用");
                result.add(resultCy);
            }
        }

        return Result.ok(result);
    }


    /**
     * @author tzj
     * @description 用户查看自己珍藏卡
     * @date 2019/12/13 14:40
     */
    @RequestMapping(value = "/getCard")
    public Result<?> getCard(HttpServletRequest request, @Session("ShopUser") ShopUser user) {
        LShopUserCards lShopUserCards = new LShopUserCards();
        lShopUserCards.setShopUserId(user.getId());
        lShopUserCards = lShopUserCardsService.getOne(lShopUserCards);
        if (lShopUserCards != null) {
            return Result.ok(lShopUserCards);
        } else {
            return null;
        }
    }

    /**
     * @author tzj
     * @description 用户退卡
     * @date 2019/12/29 14:27
     */
    @RequestMapping("/backCard")
    public Result<?> refreshUrl(@RequestBody @Valid IdParam param) {
        //取到购买记录的ID
        LShopUserCards lShopUserCards = new LShopUserCards();
        lShopUserCards.setId(param.getId());
        lShopUserCards = lShopUserCardsService.getOne(lShopUserCards);
        if (lShopUserCards != null) {
            String shopUserId = lShopUserCards.getShopUserId();
            Map<String, Object> params = new HashMap<>(5);
            params.put("shopUserId", shopUserId);
            //找出用户所有的优惠券记录
            List<UserCardsInfo> list = userCardsInfoService.list(params);
            for (UserCardsInfo userCardsInfo : list) {
                //如果有使用的不能退卡
                if (userCardsInfo.getState() == 3) {
                    return Result.of(Status.ClientError.BAD_REQUEST, "该用户已经使用了优惠券 : " + userCardsInfo.getName() + "  不能退卡");
                }
            }
            //所有优惠券
            for (UserCardsInfo userCardsInfo : list) {
                userCardsInfo.setState(5);
                userCardsInfo.setMonth(0);
                userCardsInfoService.update(userCardsInfo);
            }
            //购买记录
            lShopUserCards.setState(4);
            lShopUserCardsService.update(lShopUserCards);
        } else {
            return Result.of(Status.ClientError.BAD_REQUEST, "未找到该记录请刷新后再试");
        }
        return Result.ok();
    }

    /**
     * @author tzj
     * @description 珍藏卡预售
     * @date 2019/12/29 19:57
     */
    @RequestMapping(value = "/bindPhone")
    public Result<?> bindPhone(@Session(SessionKeys.USER) User user, @RequestBody Map<String, Object> params) {
        //直接写入信息
        LShopUserCards lShopUserCards = new LShopUserCards();
        lShopUserCards.setId(KeyUtils.getKey());
        //状态用3代替
        lShopUserCards.setState(3);
        lShopUserCards.setPhone((String) params.get("phone"));
        //用户id随机生成
        Map<String, Object> map = new HashMap<>();
        map.put("phone", (String) params.get("phone"));
        List<LShopUserCards> lists = lShopUserCardsService.list(map);
        if (!lists.isEmpty()) {
            return Result.of(Status.ClientError.BAD_REQUEST, "该手机号，已购卡或已预售，请检查！");
        }

        lShopUserCards.setShopUserId(KeyUtils.getKey());
        lShopUserCards.setCardcode((String) params.get("cardcode"));
        lShopUserCards.setTicket((String) params.get("ticket"));
        lShopUserCards.setUsername("预售");

        lShopUserCards.setMoney(300D);
        lShopUserCards.setOperatorUsername(user.getUsername());
        lShopUserCards.setOperatorNickname(user.getUsername());
        lShopUserCards.setOperatorUserid(user.getId());
        lShopUserCards.setCreatetime(new Date());
        lShopUserCards.setCreateman(user.getId());

        lShopUserCardsService.save(lShopUserCards);
        return Result.ok();
    }


    /**
     * @author tzj
     * @description 领取珍藏卡
     * @date 2019/12/29 19:57
     */
    @RequestMapping(value = "/addShopUser2")
    public Result<?> getCard(@Session("ShopUser") ShopUser shopUser) {
        shopUser=shopUserService.get(shopUser.getId());
        if ("".equals(shopUser.getPhone()) || null == shopUser.getPhone()) {
            return Result.of(Status.ClientError.UNAUTHORIZED, "请先绑定手机号");
        }
        //通过手机号和状态找到该用户预售信息
        Map<String, Object> param = new HashMap<>(10);
        param.put("state", 3);
        param.put("phone", shopUser.getPhone());
        List<LShopUserCards> list = lShopUserCardsService.list(param);
        if (list.size() != 1) {
            return Result.of(Status.ClientError.BAD_REQUEST, "未找到预售信息请确认");
        }
        //更新用户id
        list.get(0).setShopUserId(shopUser.getId());
        list.get(0).setState(2);
        list.get(0).setUpdateman("预售");
        list.get(0).setUsername(shopUser.getNickname());
        list.get(0).setPayTime(new Date());
        //添加优惠券
        addShopUserCardsYs(shopUser);
        lShopUserCardsService.update(list.get(0));
        return Result.ok(list.get(0));
    }


    /**
     * @author tzj
     * @description 预售卡添加优惠券
     * @date 2019/12/29 20:36
     */
    private void addShopUserCardsYs(ShopUser user) {
        List<UserCardsInfo> userCardsInfos = new ArrayList<>();
        List<CardInfo> cardList = cardInfoService.list();
        for (CardInfo cardInfo : cardList) {
            UserCardsInfo userCardsInfo = new UserCardsInfo();
            userCardsInfo.setId(KeyUtils.getKey());
            userCardsInfo.setName(cardInfo.getName());
            userCardsInfo.setCategoryCodes(cardInfo.getCategoryCodes());
            userCardsInfo.setDepartmentCodes(cardInfo.getDepartmentCodes());
            userCardsInfo.setMinMoney(cardInfo.getMinMoney());
            userCardsInfo.setUseMoney(cardInfo.getUseMoney());
            userCardsInfo.setShopUserId(user.getId());
            userCardsInfo.setCreatetime(new Date());
            userCardsInfo.setMonth(getCurrentMouth());
            userCardsInfo.setState(2);
            String lastDay = getLastDay();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date dateTime = null;
            try {
                dateTime = simpleDateFormat.parse(lastDay);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            userCardsInfo.setBeginTime(new Date());
            userCardsInfo.setEndTime(dateTime);
            userCardsInfos.add(userCardsInfo);
        }
        userCardsInfoService.save(userCardsInfos);
    }


    /**
     * @author tzj
     * @description 所有珍藏卡用户
     * @date 2019/12/29 19:57
     */
    @RequestMapping(value = "/commentAmount")
    public Result<?> commentAmount(@Session(SessionKeys.USER) User user,@RequestBody(required = false) Map<String, Object> param) {
        if (!"1a".equals(user.getId())){
            return null;
        }
        return Result.ok(lShopUserCardsService.count());
    }

    /**
     *
     *           ▄         ▄  ▄       ▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄         ▄
     *          ▐░▌       ▐░▌▐░▌     ▐░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░▌       ▐░▌
     *          ▐░▌       ▐░▌ ▐░▌   ▐░▌ ▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀█░▌▐░▌       ▐░▌
     *          ▐░▌       ▐░▌  ▐░▌ ▐░▌  ▐░▌       ▐░▌▐░▌       ▐░▌▐░▌       ▐░▌
     *          ▐░▌   ▄   ▐░▌   ▐░▐░▌   ▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄█░▌
     *          ▐░▌  ▐░▌  ▐░▌    ▐░▌    ▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌
     *          ▐░▌ ▐░▌░▌ ▐░▌   ▐░▌░▌   ▐░█▀▀▀▀▀▀▀▀▀ ▐░█▀▀▀▀▀▀▀█░▌ ▀▀▀▀█░█▀▀▀▀
     *          ▐░▌▐░▌ ▐░▌▐░▌  ▐░▌ ▐░▌  ▐░▌          ▐░▌       ▐░▌     ▐░▌
     *          ▐░▌░▌   ▐░▐░▌ ▐░▌   ▐░▌ ▐░▌          ▐░▌       ▐░▌     ▐░▌
     *          ▐░░▌     ▐░░▌▐░▌     ▐░▌▐░▌          ▐░▌       ▐░▌     ▐░▌
     *          ▀▀       ▀▀  ▀       ▀  ▀            ▀         ▀       ▀
     *
     */

    /**
     * 在线购卡记录界面
     *
     * @param params
     * @param pagination
     * @return
     */
    @RequestMapping(value = "/wxPayListPage")
    public Result<?> wxPayListPage(@RequestBody Map<String, Object> params, Pagination pagination) {
        if (params == null) {
            params = new HashMap<>();
        }

        if (StringUtils.isNotBlank((String) params.get("startPayTime"))) {
            params.put("startPayTime", (String) params.get("startPayTime") + " 00:00:00");
        }
        if (StringUtils.isNotBlank((String) params.get("startPayTime"))) {
            params.put("endPayTime", (String) params.get("endPayTime") + " 23:59:59");
        }
        if (StringUtils.isNotBlank((String) params.get("startCancelTime"))) {
            params.put("startCancelTime", (String) params.get("startCancelTime") + " 00:00:00");
        }
        if (StringUtils.isNotBlank((String) params.get("endCancelTime"))) {
            params.put("endCancelTime", (String) params.get("endCancelTime") + " 23:59:59");
        }

        Pageable<LShopUserCardWxPayInfo> page = lShopUserCardWxPayService.getLShopUserCardWxPayList(params, pagination);
        return Result.ok(page);
    }

    /**
     * 在线购卡记录导出
     *
     * @param req
     * @param rsp
     * @throws IOException
     */
    @RequestMapping("/exportWxPayList")
    public void exportWxPayList(HttpServletRequest req, HttpServletResponse rsp) throws IOException {
        Map<String, Object> params = new HashMap<>(10);
        params.put("startCancelTime", req.getParameter("startCancelTime"));
        params.put("endCancelTime", req.getParameter("endCancelTime"));
        params.put("startPayTime", req.getParameter("startPayTime"));
        params.put("endPayTime", req.getParameter("endPayTime"));
        params.put("userName", req.getParameter("userName"));
        params.put("phone", req.getParameter("phone"));
        params.put("state", req.getParameter("state"));

        if (StringUtils.isNotBlank((String) params.get("startPayTime"))) {
            params.put("startPayTime", (String) params.get("startPayTime") + " 00:00:00");
        }
        if (StringUtils.isNotBlank((String) params.get("startPayTime"))) {
            params.put("endPayTime", (String) params.get("endPayTime") + " 23:59:59");
        }
        if (StringUtils.isNotBlank((String) params.get("startCancelTime"))) {
            params.put("startCancelTime", (String) params.get("startCancelTime") + " 00:00:00");
        }
        if (StringUtils.isNotBlank((String) params.get("endCancelTime"))) {
            params.put("endCancelTime", (String) params.get("endCancelTime") + " 23:59:59");
        }

        List<LShopUserCardWxPayInfoExrt> result;
        result = lShopUserCardWxPayService.queryLShopUserCardWxPayExport(params);

        // 导出Excel
        OutputStream os = rsp.getOutputStream();

        rsp.setContentType("application/x-download");
        // 设置导出文件名称
        rsp.setHeader("Content-Disposition",
                "attachment;filename=" + DateFormatUtils.format(new Date(), "yyyyMMddHHmmssSSS") + ".xlsx");
        ExportParams param = new ExportParams("在线购卡记录", "在线购卡记录", "在线购卡记录表");
        param.setAddIndex(true);
        param.setType(ExcelType.XSSF);
        Workbook excel = ExcelExportUtil.exportExcel(param, LShopUserCardWxPayInfoExrt.class, result);
        excel.write(os);
        os.flush();
        os.close();
    }

    /**
     * 产生随机字符串
     *
     * @param length 要求字符串的长度
     * @return
     */
    public static String getRandomString(int length) {
        String str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < length; i++) {
            int number = random.nextInt(62);
            sb.append(str.charAt(number));
        }
        return sb.toString();
    }

    /**
     * TODO
     *
     * @author cs
     * @description 珍藏卡微信支付 -- 统一下单
     * @date 2019/12/25 12:17
     */
    @RequestMapping("/wxPay")
    @ResponseBody
    public Result<?> payFy(HttpServletRequest request, @Session("ShopUser") ShopUser shopUser) throws Exception {
        //TODO 查询、检测用户已购珍藏卡信息
        if (shopUser.getPhone() == null || "".equals(shopUser.getPhone())) {
            return Result.of(Status.ClientError.BAD_REQUEST, "请您先绑定手机号码");
        }
        LShopUserCardWxPayInfo payInfo = new LShopUserCardWxPayInfo();

        Map<String, Object> param = new HashMap<>();
        param.put("phone", shopUser.getPhone());
        List<LShopUserCards> list = lShopUserCardsService.list(param);
        if (list.isEmpty()) {
            return Result.of(Status.ClientError.BAD_REQUEST, "尚未购买珍藏卡，无法支付!");
        } else {
            for (LShopUserCards lShopUserCards : list) {
                if (lShopUserCards.getState() != 1) {
                    return Result.of(Status.ClientError.BAD_REQUEST, "您已经购买过本卡");
                } else {
                    payInfo.setCardId(lShopUserCards.getId());
                }
            }
        }
        //TODO 获取未超时的待支付订单
        boolean hasOld = false;
        payInfo.setState(1);
        List<LShopUserCardWxPayInfo> list2 = lShopUserCardWxPayService.list(payInfo);
        Long nowTime = System.currentTimeMillis();

        if (!list2.isEmpty()) {
            for (LShopUserCardWxPayInfo info : list2) {
                //预支付交易会话标识不为空、并且距离上次未成功的支付不超过110分钟（微信默认2小时失效）
                if ((!info.getPrepayId().isEmpty()) && ((nowTime - info.getCreatetime().getTime()) / 60000) <= 110) {
                    hasOld = true;
                    payInfo = info;
                } else {
                    info.setState(-1);
                    lShopUserCardWxPayService.update(info);
                }
            }
        }
        if (!hasOld) {
            //TODO 请求微信统一下单接口
            //构建请求字段
            payInfo.setId(KeyUtils.getKey());
            payInfo.setShopUserId(shopUser.getId());
            payInfo.setOpenId(shopUser.getOpenId());
            payInfo.setTotalFee("30000");
            payInfo.setGoodName("阜阳商厦30周年珍藏卡");
            payInfo.setGoodDetail("珍藏卡线上购买");
            payInfo.setCreatetime(new Date());
            payInfo.setOutTradeNo(new SimpleDateFormat("yyyyMMddHHmmss").format(payInfo.getCreatetime()));
            payInfo.setCreateIp("127.0.0.1");
            //组和生成签名
            SortedMap<String, String> signInfoMap = new TreeMap<>();
            signInfoMap.put("appid", WeChatConfig.APPIDFuYang);
            signInfoMap.put("mch_id", WeChatConfig.MCHIDFuYang);
            signInfoMap.put("nonce_str", getRandomString(32));
            signInfoMap.put("notify_url", WeChatConfig.WECHAT_NOTIFYURLFuYangCard);
            signInfoMap.put("trade_type", WeChatConfig.Pay);
            signInfoMap.put("openid", payInfo.getOpenId());
            signInfoMap.put("body", payInfo.getGoodName());
            signInfoMap.put("detail", payInfo.getGoodDetail());
            signInfoMap.put("out_trade_no", payInfo.getOutTradeNo());
            signInfoMap.put("total_fee", payInfo.getTotalFee());
            signInfoMap.put("spbill_create_ip", payInfo.getCreateIp());
            signInfoMap.put("sign", WXPayUtil.createSign(signInfoMap, WeChatConfig.APIKEYFuYang));

            String payXml = WXPayUtil.mapToXml(signInfoMap);
            //请求统一下单
            String wxResultStr = HttpUtil.doPost(WeChatConfig.pay_url, payXml, 4000);
            if (null == wxResultStr) {
                return Result.of(Status.ClientError.BAD_REQUEST, "请求微信统一下单超时");
            }

            Map<String, String> wxReturnMap = WXPayUtil.xmlToMap(wxResultStr);
            if (!"SUCCESS".equals(wxReturnMap.get("return_code"))) {
                return Result.of(Status.ClientError.BAD_REQUEST, "微信下单失败：" + wxReturnMap.get("return_msg"));
            } else if (!"SUCCESS".equals(wxReturnMap.get("result_code"))) {
                return Result.of(Status.ClientError.BAD_REQUEST, "微信下单失败：" + wxReturnMap.get("err_code_des"));
            }
            payInfo.setPrepayId(wxReturnMap.get("prepay_id"));
            //TODO 添加数据库记录
            lShopUserCardWxPayService.save(payInfo);
        }

        //TODO 返回给小程序所需的支付字段信息
        Map<String, Object> response = new HashMap<String, Object>();

        SortedMap<String, String> signMap = new TreeMap<>();
        signMap.put("appId", WeChatConfig.APPIDFuYang);
        signMap.put("nonceStr", getRandomString(32));
        signMap.put("package", "prepay_id=" + payInfo.getPrepayId());
        signMap.put("signType", WeChatConfig.SIGNTYPE);
        signMap.put("timeStamp", String.valueOf(System.currentTimeMillis() / 1000));
        String signStr = WXPayUtil.createSign(signMap, WeChatConfig.APIKEYFuYang);

        response.put("nonceStr", signMap.get("nonceStr"));
        response.put("package", signMap.get("package"));
        response.put("timeStamp", signMap.get("timeStamp"));
        response.put("paySign", signStr);

        return Result.ok(response);
    }

    /**
     * TODO
     *
     * @param request
     * @param response
     * @throws Exception
     * @author cs
     * @description 珍藏卡微信支付--支付回调接口
     */
    @SuppressWarnings("all")
    @RequestMapping(value = "/wxPayNotify")
    @PermissionMode(PermissionMode.Mode.White)
    @SystemControllerLog(description = "支付回调接口")
    public synchronized void wxNotify(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String orderId = null;
        BufferedReader br = new BufferedReader(new InputStreamReader((ServletInputStream) request.getInputStream()));
        String line = null;
        StringBuilder sb = new StringBuilder();
        while ((line = br.readLine()) != null) {
            sb.append(line);
        }
        br.close();
        //sb为微信返回的xml
        String notityXml = sb.toString();
        /**此处添加自己的业务逻辑代码end**/
        //通知微信服务器已经支付成功
        String resXml = "<xml><return_code><![CDATA[SUCCESS]]></return_code>" +
                "<return_msg><![CDATA[OK]]></return_msg></xml>";
        Map map = PayUtil.doXMLParse(notityXml);

        String returnCode = (String) map.get("return_code");
        if ("SUCCESS".equals(returnCode)) {
            //验证签名是否正确
            Map<String, Object> validParams = PayUtil.paraFilter(map);  //回调验签时需要去除sign和空值参数
            String validStr = PayUtil.createLinkString(validParams);//把数组所有元素，按照“参数=参数值”的模式用“&”字符拼接成字符串
            String sign = PayUtil.sign(validStr, WeChatConfig.APIKEYFuYang, "utf-8").toUpperCase();//拼装生成服务器端验证的签名
            //根据微信官网的介绍，此处不仅对回调的参数进行验签，还需要对返回的金额与系统订单的金额进行比对等
            if (sign.equals(map.get("sign"))) {
                /**此处添加自己的业务逻辑代码start**/
                //TODO 修改订单信息、切换珍藏卡、优惠券状态
                Map<String, Object> selectMap = new HashMap<>();
                selectMap.put("outTradeNo", map.get("out_trade_no"));
//                selectMap.put("state", 1);
                LShopUserCardWxPayInfo payInfo = lShopUserCardWxPayService.getOne(selectMap);
                if (payInfo != null && payInfo.getState() == 1) {
                    payInfo.setState(2);
                    payInfo.setPaytime(new SimpleDateFormat("yyyyMMddHHmmss").parse((String) map.get("time_end")));
                    payInfo.setUpdatetime(new Date());
                    lShopUserCardWxPayService.update(payInfo);

                    selectMap.clear();
                    selectMap.put("id", payInfo.getCardId());
                    LShopUserCards shopUserCard = lShopUserCardsService.getOne(selectMap);
                    if (shopUserCard == null) {
                        return;
                    }
                    shopUserCard.setMoney(300d);
                    shopUserCard.setOperatorUsername("微信支付");
                    shopUserCard.setOperatorNickname("微信支付");
                    shopUserCard.setState(2);
                    shopUserCard.setPayTime(payInfo.getPaytime());
                    lShopUserCardsService.update(shopUserCard);

                    selectMap.clear();
                    selectMap.put("shopUserId", payInfo.getShopUserId());
                    List<UserCardsInfo> list = userCardsInfoService.list(selectMap);
                    if (list != null && !list.isEmpty()) {
                        for (UserCardsInfo cardsInfo : list) {
                            cardsInfo.setState(2);
                            cardsInfo.setUpdatetime(payInfo.getUpdatetime());
                            userCardsInfoService.update(cardsInfo);
                        }
                    }
                }
            }

        } else {
            resXml = "<xml> \n" +
                    "  <return_code><![CDATA[FAIL]]></return_code>\n" +
                    "  <return_msg><![CDATA[支付回调失败]]></return_msg>\n" +
                    "</xml>";
        }

        BufferedOutputStream out = new BufferedOutputStream(
                response.getOutputStream());
        out.write(resXml.getBytes());
        out.flush();
        out.close();
    }

    /**
     * TODO
     *
     * @param shopUser
     * @return
     * @description 用户申请退卡
     */
    @RequestMapping("/applyRefund")
    @ResponseBody
    public Result<?> applyRefund(@Session("ShopUser") ShopUser shopUser) {
        Map<String, Object> params = new HashMap<>();
        //TODO 用户是否买卡、卡的当前状态、卡是否是线上支付的
        params.put("shopUserId", shopUser.getId());
        List<LShopUserCards> lShopUserCardsList = lShopUserCardsService.list(params);
        if (lShopUserCardsList.isEmpty()) {
            return Result.of(Status.ClientError.BAD_REQUEST, "尚未购买珍藏卡，无法申请退款");
        } else {
            for (LShopUserCards shopUserCards : lShopUserCardsList) {
                if (shopUserCards.getState() == 1) {
                    return Result.of(Status.ClientError.BAD_REQUEST, "尚未支付，无法申请退款");
                } else if (shopUserCards.getState() == 3) {
                    return Result.of(Status.ClientError.BAD_REQUEST, "尚未领取，无法申请退款");
                } else if (shopUserCards.getState() == 4 || shopUserCards.getState() == 5) {
                    return Result.of(Status.ClientError.BAD_REQUEST, "已成功退款，请不要重复申请");
                } else if (shopUserCards.getState() == 6) {
                    return Result.of(Status.ClientError.BAD_REQUEST, "正在申请退款中，请耐心等待");
                }
                //判断是否是线上支付
                params.clear();
                params.put("shopUserId", shopUser.getId());
                params.put("cardId", shopUserCards.getId());
                params.put("state", 2);
                List<LShopUserCardWxPayInfo> wxPayInfoList = lShopUserCardWxPayService.list(params);
                if (wxPayInfoList.isEmpty()) {
                    return Result.of(Status.ClientError.BAD_REQUEST, "线下支付不支持申请线上退款");
                }
            }
        }
        //TODO 是否使用过优惠券
        params.clear();
        params.put("shopUserId", shopUser.getId());
        params.put("state", 3);
        List<UserCardsInfo> userCardsInfoList = userCardsInfoService.list(params);
        if (!userCardsInfoList.isEmpty()) {
            return Result.of(Status.ClientError.BAD_REQUEST, "已使用过优惠券，无法申请退款");
        }
        //TODO 修改卡状态为待退卡、修改支付订单状态为待退卡
        for (LShopUserCards shopUserCards : lShopUserCardsList) {
            shopUserCards.setState(6);
            params.clear();
            params.put("shopUserId", shopUser.getId());
            params.put("cardId", shopUserCards.getId());
            params.put("state", 2);
            LShopUserCardWxPayInfo wxPayInfo = lShopUserCardWxPayService.getOne(params);
            if (wxPayInfo == null) {
                return Result.of(Status.ClientError.BAD_REQUEST, "未查询到微信支付订单，无法申请退款");
            }
            wxPayInfo.setState(3);
            wxPayInfo.setCanceltime(new Date());
            lShopUserCardWxPayService.update(wxPayInfo);
            lShopUserCardsService.update(shopUserCards);
        }
        return Result.ok("已成功申请退款");
    }

    /**
     * TODO
     *
     * @author cs
     * @description 后台处理退卡
     */
    @RequestMapping("/wxPayRefund")
    @ResponseBody
    public Result<?> wxPayRefund(@RequestBody Map<String, Object> params) {
        boolean allowRefund;
        allowRefund = (Boolean) params.get("allowRefund");

        Map<String, Object> queryParams = new HashMap<>();
        queryParams.put("id", params.get("payId"));
        LShopUserCardWxPayInfo wxPayInfo = lShopUserCardWxPayService.getOne(queryParams);

        if (allowRefund) {
            //TODO 请求微信退款
            if (wxPayInfo.getOutRefundNo() == null || wxPayInfo.getOutRefundNo().isEmpty()) {
                wxPayInfo.setRefundFee(wxPayInfo.getTotalFee());
                wxPayInfo.setOutRefundNo(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
                lShopUserCardWxPayService.update(wxPayInfo);
            }

            SortedMap<String, String> parameters = new TreeMap<>();
            parameters.put("appid", WeChatConfig.APPIDFuYang);
            parameters.put("mch_id", WeChatConfig.MCHIDFuYang);
            parameters.put("nonce_str", getRandomString(32));
            parameters.put("sign_type", "MD5");
            parameters.put("notify_url", WeChatConfig.WECHAT_NOTIFYURLFuYangCardRefund);
            parameters.put("total_fee", wxPayInfo.getTotalFee());
            parameters.put("refund_fee", wxPayInfo.getRefundFee());
            parameters.put("out_trade_no", wxPayInfo.getOutTradeNo());
            parameters.put("out_refund_no", wxPayInfo.getOutRefundNo());
            parameters.put("sign", WXPayUtil.createSign(parameters, WeChatConfig.APIKEYFuYang));

            //附带双向证书的请求
            WxPayRefundConfig refundWxPayConfig = new WxPayRefundConfig();
            Map<String, String> returnMap = null;
            WXPay2 wxPay = new WXPay2(refundWxPayConfig);
            try {
                returnMap = wxPay.refund(parameters);
                //业务逻辑代码
                if ("SUCCESS".equals(returnMap.get("result_code"))) {
                    return Result.ok("已成功申请退款");
                } else {
                    return Result.of(Status.ClientError.BAD_REQUEST, "请求参数错误，申请退款失败");
                }
            } catch (Exception e) {
                e.printStackTrace();
                return Result.of(Status.ClientError.BAD_REQUEST, "发生错误，申请退款失败");
            }

        } else {
            //驳回退款、回退订单状态、回退珍藏卡状态
            wxPayInfo.setState(2);
            lShopUserCardWxPayService.update(wxPayInfo);

            queryParams.clear();
            queryParams.put("id", wxPayInfo.getCardId());
            LShopUserCards cards = lShopUserCardsService.getOne(queryParams);
            cards.setState(2);
            lShopUserCardsService.update(cards);
            return Result.ok("已驳回退款申请");
        }

    }

    /**
     * TODO
     *
     * @param request
     * @param response
     * @throws Exception
     * @author cs
     * @description 珍藏卡微信支付--退款回调接口
     */
    @SuppressWarnings("all")
    @RequestMapping(value = "/wxPayRefundNotify")
    @PermissionMode(PermissionMode.Mode.White)
    @SystemControllerLog(description = "退款回调接口")
    public synchronized void wxPayRefundNotify(HttpServletRequest request, HttpServletResponse response) throws Exception {
        InputStream inputStream;
        StringBuffer sb = new StringBuffer();
        inputStream = request.getInputStream();
        String s;
        BufferedReader in = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
        while ((s = in.readLine()) != null) {
            sb.append(s);
        }
        in.close();
        inputStream.close();

        String resXml = "";
        //解析xml成map
        Map<String, String> param = new HashMap<String, String>();
        param = XMLUtil.doXMLParse(sb.toString());
        if (param != null) {
            if (param.get("return_code").equals("SUCCESS")) {
                //退款成功
                //解密加密信息
                String deStr;
                deStr = WXPayUtil.decReqInfo(param.get("req_info"), WeChatConfig.APIKEYFuYang);
                param.clear();
                param = XMLUtil.doXMLParse(deStr);
                //更新退款信息
                if ("SUCCESS".equals((String) param.get("refund_status"))) {
                    Map<String, Object> params = new HashMap<>();
                    params.put("outRefundNo", param.get("out_refund_no"));
                    LShopUserCardWxPayInfo wxPayInfo = lShopUserCardWxPayService.getOne(params);
                    wxPayInfo.setState(4);
                    wxPayInfo.setCanceltime(new Date());
                    wxPayInfo.setUpdatetime(wxPayInfo.getCanceltime());
                    lShopUserCardWxPayService.update(wxPayInfo);
                    //珍藏卡、相关优惠券
                    params.clear();
                    params.put("id", wxPayInfo.getCardId());
                    params.put("shopUserId", wxPayInfo.getShopUserId());
                    LShopUserCards cards = lShopUserCardsService.getOne(params);
                    params.clear();
                    params.put("shopUserId", wxPayInfo.getShopUserId());
                    List<UserCardsInfo> userCardsInfoList = userCardsInfoService.list(params);
                    for (UserCardsInfo userCardsInfo : userCardsInfoList) {
                        userCardsInfo.setState(5);
                        userCardsInfoService.update(userCardsInfo);
                    }
                    cards.setState(4);
                    lShopUserCardsService.update(cards);
                    //通知微信服务器已经退款成功
                    resXml = "<xml> \n" +
                            "  <return_code><![CDATA[SUCCESS]]></return_code>\n" +
                            "  <return_msg><![CDATA[OK]]></return_msg>\n" +
                            "</xml>";
                }
            } else {
                //通知微信服务器已经退款成功
                resXml = "<xml> \n" +
                        "  <return_code><![CDATA[FAIL]]></return_code>\n" +
                        "  <return_msg><![CDATA[退款回调失败]]></return_msg>\n" +
                        "</xml>";
            }
        }
        BufferedOutputStream out = new BufferedOutputStream(
                response.getOutputStream());
        out.write(resXml.getBytes());
        out.flush();
        out.close();
    }


}