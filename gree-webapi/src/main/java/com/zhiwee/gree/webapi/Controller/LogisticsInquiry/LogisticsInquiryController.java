package com.zhiwee.gree.webapi.Controller.LogisticsInquiry;
import com.alibaba.fastjson.JSON;
import com.zhiwee.gree.model.Log.Log;
import com.zhiwee.gree.model.Logistics.Logistics;
import com.zhiwee.gree.service.log.LogService;
import com.zhiwee.gree.webapi.LogisticsInquiryUtil.LogisticsInquiryUtil;
import com.zhiwee.gree.webapi.util.SystemControllerLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.web.annotation.*;
import xyz.icrab.common.web.util.Validator;

import java.util.HashMap;
import java.util.Map;

/**
 * @author sun on 2019/7/11
 */
@RestController
@RequestMapping("/logisticsInquiry")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class LogisticsInquiryController {



@RequestMapping("/searchLogistics")
@PermissionMode(PermissionMode.Mode.White)
    public Result<?> searchLogistics(@RequestBody Logistics logistics) throws Exception{
    Validator validator = new Validator();
    validator.notNull(logistics.getNo(), "快递单号不能为空");
    if (validator.isError()) {
        return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
    }
    String s = LogisticsInquiryUtil.searchLogiscsInquiry(logistics.getNo());
    Map params = (Map)JSON.parse(s);
    System.out.println(params);
    String  status =(String) params.get("status");
    if(status.equals("0")){
        return Result.ok(params.get("result"));
    }else{
        return Result.of(Status.ClientError.BAD_REQUEST, params.get("msg"));
    }

}


}
