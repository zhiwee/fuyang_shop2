package com.zhiwee.gree.webapi.Timer;


import com.zhiwee.gree.service.Coupon.CouponService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.util.Calendar;
import java.util.Date;

/**
 * @author sun on 2019/6/06
 */
@Component
public class UpdateCouponsTimer implements  InitializingBean {

    private java.util.Timer timer = null;

    @Autowired
    private CouponService couponService;

    @Override
    public void afterPropertiesSet() throws Exception {

        /**
         * 设置一个定时器
         */
        timer = new java.util.Timer(true);

        /**
         * 定时器到指定的时间时,执行某个操作(如某个类,或方法)
         */

       System.out.println("定时器加载成功");
        //设置执行时间
        Calendar calendar =Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day =calendar.get(Calendar.DAY_OF_MONTH);//每天
        //定制每天的1:00:00执行，
        calendar.set(year, month, day, 16, 21, 00);
        Date date = calendar.getTime();
//        System.out.println(date);

        int period = 24 * 60 * 60 * 1000;
        //每天的date时刻执行task，每隔persion 时间重复执行
        timer.schedule(new DelFileTask(couponService), date, period);
//        在 指定的date时刻执行task, 仅执行一次
//        timer.schedule(new DelFileTask(arg0.getServletContext()), date);
         System.out.println("定时器加载结束");

    }
}
