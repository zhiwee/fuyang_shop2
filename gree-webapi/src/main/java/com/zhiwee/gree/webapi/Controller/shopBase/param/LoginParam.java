package com.zhiwee.gree.webapi.Controller.shopBase.param;



import com.zhiwee.gree.model.ret.LoginReturnMsg;

import javax.validation.constraints.NotEmpty;

/**
 * @author Knight
 * @since 2018/4/12 17:18
 */
public class LoginParam {

    /**
     * 登录
     */
    @NotEmpty(message = LoginReturnMsg.EMPTY_USERNAME)
    private String username;

    @NotEmpty(message = LoginReturnMsg.EMPTY_PASSWORD)
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
