package com.zhiwee.gree.webapi.util;


import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.zhiwee.gree.model.Interface;
import com.zhiwee.gree.model.Menu;
import org.apache.commons.collections4.CollectionUtils;
import xyz.icrab.common.model.enums.YesOrNo;
import xyz.icrab.common.web.session.Session;
import xyz.icrab.common.web.support.extension.AbstractResourcePermissionResolver;
import xyz.icrab.common.web.util.SessionUtils;


public class DefaultResourcePermissionResolver extends AbstractResourcePermissionResolver {
    public DefaultResourcePermissionResolver() {
    }

    public boolean hasSessionPermission(String uri) {
        return SessionUtils.has();
    }

    public boolean hasAuthPermission(String uri) {
        Session session = SessionUtils.get();
        if (session == null) {
            return false;
        } else {
            Map<String, Menu> menuMap = (Map)session.getAttribute("all:menus");
            Menu menu = null;
            if(menuMap != null){
                menu    = (Menu)menuMap.get(uri);
                if (menu == null || !YesOrNo.YES.value().equals(menu.getCheckauth())) {
                    menu = null;
                }
            }


            Map<String, Interface> interfaceMap = (Map)session.getAttribute("all:interfaces");
            Interface api = null;
            if (interfaceMap != null)   {
               api = (Interface)interfaceMap.get(uri);
                if (api == null || YesOrNo.YES != api.getCheckauth()) {
                    api = null;
                }
            }

            if (menu == null && api == null) {
                return true;
            } else {
                List apis;
                Iterator var8;
                if (menu != null) {
                    apis = (List)session.getAttribute("menus");
                    if (CollectionUtils.isNotEmpty(apis)) {
                        var8 = apis.iterator();

                        while(var8.hasNext()) {
                            Menu m = (Menu)var8.next();
                            if (uri.equals(m.getUrl())) {
                                return true;
                            }
                        }
                    }
                }

                if (api != null) {
                    apis = (List)session.getAttribute("interfaces");
                    if (CollectionUtils.isNotEmpty(apis)) {
                        var8 = apis.iterator();

                        while(var8.hasNext()) {
                            Interface i = (Interface)var8.next();
                            if (uri.equals(i.getUrl())) {
                                return true;
                            }
                        }
                    }
                }

                return false;
            }
        }
    }

    public boolean hasStrictPermission(String uri) {
        Session session = SessionUtils.get();
        if (session == null) {
            return false;
        } else {
            List<Menu> menus = (List)session.getAttribute("menus");
            if (CollectionUtils.isNotEmpty(menus)) {
                Iterator var4 = menus.iterator();

                while(var4.hasNext()) {
                    Menu m = (Menu)var4.next();
                    if (uri.equals(m.getUrl())) {
                        return true;
                    }
                }
            }

            List<Interface> apis = (List)session.getAttribute("interfaces");
            if (CollectionUtils.isNotEmpty(apis)) {
                Iterator var8 = apis.iterator();

                while(var8.hasNext()) {
                    Interface i = (Interface)var8.next();
                    if (uri.equals(i.getUrl())) {
                        return true;
                    }
                }
            }

            return false;
        }
    }
}
