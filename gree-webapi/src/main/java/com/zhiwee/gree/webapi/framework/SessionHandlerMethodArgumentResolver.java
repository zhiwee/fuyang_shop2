package com.zhiwee.gree.webapi.framework;

/**
 * @author sun on 2019/7/12
 */


import java.lang.annotation.Annotation;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;
import xyz.icrab.common.web.session.Session;
import xyz.icrab.common.web.session.SessionService;
import xyz.icrab.common.web.util.RequestUtils;

public class SessionHandlerMethodArgumentResolver implements HandlerMethodArgumentResolver {

    private SessionService sessionService;

    public SessionHandlerMethodArgumentResolver() {
    }

    public boolean supportsParameter(MethodParameter parameter) {
        Class parameterType = parameter.getParameterType();
        if (parameterType != Session.class && !Session.class.isAssignableFrom(parameterType)) {
            Annotation[] annotations = parameter.getParameterAnnotations();
            if (ArrayUtils.isNotEmpty(annotations)) {
                Annotation[] var4 = annotations;
                int var5 = annotations.length;

                for(int var6 = 0; var6 < var5; ++var6) {
                    Annotation annotation = var4[var6];
                    if (annotation.annotationType() == xyz.icrab.common.web.annotation.Session.class) {
                        return true;
                    }
                }
            }

            return false;
        } else {
            return true;
        }
    }

    public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer, NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {
        String token = RequestUtils.getToken();
        Session session;
            if (StringUtils.isBlank(token)) {
                session = null;
            } else {
            session = this.sessionService.get(token);
        }

        Class parameterType = parameter.getParameterType();
        xyz.icrab.common.web.annotation.Session annotation = (xyz.icrab.common.web.annotation.Session)parameter.getParameterAnnotation(xyz.icrab.common.web.annotation.Session.class);
        if (annotation != null && annotation.required() && session == null) {
            throw new HandleNoSessionException("获取session为null，绑定失败");
        } else if (session == null) {
            return null;
        } else if (parameterType != Session.class && !Session.class.isAssignableFrom(parameterType)) {
            String field = annotation.value();
            if (StringUtils.isBlank(field)) {
                return session;
            } else {
                Object value = session.getAttribute(field);
                if (annotation.required() && value == null) {
                    throw new HandleNoSessionException("获取session#" + field + "为null，绑定失败");
                } else {
                    return value;
                }
            }
        } else {
            return session;
        }
    }

    public SessionService getSessionService() {
        return this.sessionService;
    }

    public void setSessionService(SessionService sessionService) {
        this.sessionService = sessionService;
    }
}
