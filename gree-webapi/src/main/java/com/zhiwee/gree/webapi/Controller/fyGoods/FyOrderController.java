package com.zhiwee.gree.webapi.Controller.fyGoods;

import com.alibaba.fastjson.JSON;
import com.zhiwee.gree.model.Coupon.Coupon;
import com.zhiwee.gree.model.FyGoods.*;
import com.zhiwee.gree.model.FyGoods.vo.FyOrderResultVo;
import com.zhiwee.gree.model.FyGoods.vo.FygoodsCartVo;
import com.zhiwee.gree.model.GoodsInfo.Categorys;
import com.zhiwee.gree.model.GoodsInfo.Goods;
import com.zhiwee.gree.model.GoodsInfo.GoodsBase;
import com.zhiwee.gree.model.GoodsInfo.GoodsDealer;
import com.zhiwee.gree.model.OrderInfo.OrderAddress;
import com.zhiwee.gree.model.OrderInfo.vo.OrderStatisticItem;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.User;
import com.zhiwee.gree.model.WxPay.WeChatConfig;
import com.zhiwee.gree.model.card.BookingCard;
import com.zhiwee.gree.model.card.GiftCardUser;
import com.zhiwee.gree.service.Coupon.CouponService;
import com.zhiwee.gree.service.FyGoods.*;
import com.zhiwee.gree.service.GoodsInfo.CategorysService;
import com.zhiwee.gree.service.GoodsInfo.GoodsBaseService;
import com.zhiwee.gree.service.GoodsInfo.GoodsDealerService;
import com.zhiwee.gree.service.GoodsInfo.GoodsService;
import com.zhiwee.gree.service.OrderInfo.OrderAddressService;
import com.zhiwee.gree.service.ShopUser.ShopUserService;
import com.zhiwee.gree.service.card.GiftCardUserService;
import com.zhiwee.gree.webapi.Controller.Wxpay.FyWxPay;
import com.zhiwee.gree.webapi.util.JsonUtil;
import com.zhiwee.gree.webapi.util.SystemControllerLog;
import com.zhiwee.gree.webapi.util.WXPayUtil;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessagePostProcessor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import sun.security.provider.SHA;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.util.IdGenerator;
import xyz.icrab.common.util.KeyUtils;
import xyz.icrab.common.web.annotation.*;
import xyz.icrab.common.web.param.IdParam;
import xyz.icrab.ums.util.consts.SessionKeys;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;


/**
 * @author DELL
 */
@RestController
@RequestMapping("/fyGoods")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class FyOrderController {
    @Resource
    private GoodsService goodsService;
    @Resource
    private BargainGoodsService bargainGoodsService;
    @Resource
    private CategorysService categorysService;
    @Resource
    private GoodsBaseService goodsBaseService;
    @Resource
    private GrouponGoodsService grouponGoodsService;
    @Resource
    private ShareGoodsService shareGoodsService;
    @Resource
    private GoodsDealerService goodsDealerService;
    @Resource
    private ShopUserService shopUserService;
    @Resource
    private FyOrderService fyOrderService;

    @Resource
    private FyOrderItemService fyOrderItemService;
    @Resource
    private GiftCardUserService giftCardUserService;
    @Resource
    private RabbitTemplate rabbitTemplate;

    @Resource
    private RedisTemplate<String, String> redisTemplate;

    @Resource
    private FygoodsService fygoodsService;
    @Resource
    private CouponService couponService;
    @Resource
    private OrderAddressService orderAddressService;
    //秒杀
    private static final Integer SKILL_TYPE = 3;
    //正常
    private static final Integer ZHENGCHANG_TYPE = 1;
    //直播
    private static final Integer ZHIBO_TYPE = 2;
    //拼团
    private static final Integer PINTUAN_TYPE = 4;
    //分享赚
    private static final Integer SHARE_TYPE = 5;
    //砍价
    private static final Integer BARGAIN_TYPE = 6;

    /**
     * 生成预订单信息
     */
    @RequestMapping("/payOrder")
    @Transactional
    public Result<?> payOrder(@Session("ShopUser") ShopUser shopUser, @RequestBody FygoodsCartVo fygoodsCartVo) {
        //确认用户信息
        shopUser = shopUserService.get(shopUser.getId());
        if (shopUser.getPhone() == null) {
            return Result.of(Status.ClientError.BAD_REQUEST, "请先完善信息");
        }
        //公共资源
        String key = KeyUtils.getKey();
        Date date = new Date();
        StringBuilder goodBody = new StringBuilder();
        //存放订单详情信息
        List<FyOrderItem> fyOrderItemList = new ArrayList<>();
        //总价标识
        BigDecimal TotalPrice = new BigDecimal(0);
        BigDecimal discount = new BigDecimal(0);
        boolean discountFlg = false;
        boolean costFlg = false;
        //秒杀 拼团 不能使用优惠券
        if (fygoodsCartVo.getType().equals(ZHENGCHANG_TYPE) || fygoodsCartVo.getType().equals(ZHIBO_TYPE) || fygoodsCartVo.getType().equals(SHARE_TYPE)) {
            //找到优惠券
            discount = getBigDecimal(shopUser, fygoodsCartVo, key, discount, couponService);
        }
        //如果有折扣
        if (discount.compareTo(BigDecimal.ZERO) > 0) {
            int discountInt = discount.intValue();
            List<FygoodsCart> list = fygoodsCartVo.getList();
            int avg = discountInt / list.size();
            if (discountInt % list.size() == 0) {
                for (FygoodsCart fygoodsCart : list) {
                    fygoodsCart.setDiscount(new BigDecimal(avg));
                }
            } else {
                for (int i = 0; i < list.size(); i++) {
                    if (i == list.size() - 1) {
                        int total = avg + discountInt % list.size();
                        list.get(i).setDiscount(new BigDecimal(total));
                    } else {
                        list.get(i).setDiscount(new BigDecimal(avg));
                    }
                }
            }
            discountFlg = true;
        }

        //如果使用佣金
        if (fygoodsCartVo.getActivityCost() != null) {
            if (fygoodsCartVo.getActivityCost().compareTo(BigDecimal.ZERO) > 0) {
                int cost = fygoodsCartVo.getActivityCost().intValue();
                List<FygoodsCart> list = fygoodsCartVo.getList();
                int avg = cost / list.size();
                if (cost % list.size() == 0) {
                    for (FygoodsCart fygoodsCart : list) {
                        fygoodsCart.setActivityCost(new BigDecimal(avg));
                    }
                } else {
                    for (int i = 0; i < list.size(); i++) {
                        if (i == list.size() - 1) {
                            int total = avg + cost % list.size();
                            list.get(i).setActivityCost(new BigDecimal(total));
                        } else {
                            list.get(i).setActivityCost(new BigDecimal(avg));
                        }
                    }
                }
                costFlg = true;
            }
        }


        for (FygoodsCart fygoodsCart : fygoodsCartVo.getList()) {

            //判断库存
            Boolean flg = checkStoreCount(fygoodsCart, fygoodsCartVo.getType());
            if (flg) {
                return Result.of(Status.ClientError.BAD_REQUEST, "库存不足");
            }

            //校验商品是否是家电 2是家电
            Boolean jiaDian = checkJiaDian(fygoodsCart, fygoodsCartVo.getType());
            if (jiaDian) {
                if ("".equals(fygoodsCart.getDealerId()) || fygoodsCart.getDealerId() == null) {
                    return Result.of(Status.ClientError.BAD_REQUEST, fygoodsCart.getGoodsName() + "为家电商品请选择家电门店");
                }
            }

            //计算总价   getTotalPrice 方法获取当前商品总价
            BigDecimal totalPrice = getTotalPrice(fygoodsCart, fygoodsCartVo.getType());
            if (totalPrice == null) {
                return Result.of(Status.ClientError.BAD_REQUEST, fygoodsCart.getGoodsName() + "商品存在问题请勿购买");
            }
            TotalPrice = TotalPrice.add(totalPrice);
            //商品名称描述，存入数据库用户标识订单。
            goodBody.append(fygoodsCart.getGoodsName()).append("-");
            FyOrderItem fyOrderItem = createOrderDetail(fygoodsCart, fygoodsCart.getCount(), key, fygoodsCart.getDealerId(), fygoodsCartVo.getType(), discountFlg, costFlg);
            fyOrderItemList.add(fyOrderItem);
            //记录订单号，延迟消息队列要用到
            fygoodsCart.setOrderNum(key);
        }


        //查询数据库，获取对应商品的信息
        FyOrder order = new FyOrder();
        //默认未发货
        order.setDeliver(1);
        order.setShareId(fygoodsCartVo.getShareId());
        order.setFyCode(fygoodsCartVo.getFyCode());
        order.setId(KeyUtils.getKey());
        order.setActivityCost(fygoodsCartVo.getActivityCost());
        order.setOrderNum(key);
        order.setOrderTime(date);
        order.setUserName(shopUser.getNickname());
        order.setUserId(shopUser.getId());
        order.setPhoneNum(shopUser.getPhone());
        order.setAllPrice(TotalPrice);
        order.setDiscount(discount);
        //支付金额，活动家减去折扣
        BigDecimal subtract;
        if (fygoodsCartVo.getActivityCost() != null) {
            subtract = TotalPrice.subtract(discount).subtract(fygoodsCartVo.getActivityCost());
        } else {
            subtract = TotalPrice.subtract(discount);
        }

        if (subtract.compareTo(BigDecimal.ZERO) > 0) {
            order.setPayPrice(subtract);
        } else {
            order.setPayPrice(new BigDecimal(0.0));
        }
        order.setGoodsBody(goodBody.toString());
        //1表示为删除的状态
        order.setIsdelete(1);
        //1表示未支付的状态
        order.setState(1);
        //是否申请退款 1 表示没有  2 表示有
        order.setIsrefound(1);
        //退款金额默认为0
        order.setRefoundPrice(new BigDecimal(0));
        //默认不包含退款订单
        order.setIshasRefound(2);
        //认领时间
        order.setClaimTime(fygoodsCartVo.getClaimTime());
        //订单信息存入数据库
        fyOrderService.save(order);

        // sendOrderMessage(order.getOrderNum());//测试用的
        //订单信息存一份到redis，支付页面获取数据直接从redis中拿，减轻数据库的压力   订单基本信息+订单详情
        FyOrderResultVo fyOrderResultVo = new FyOrderResultVo();
        fyOrderResultVo.setFyOrder(order);
        fyOrderResultVo.setFyOrderItemList(fyOrderItemList);
        redisTemplate.opsForValue().set("order:" + order.getOrderNum(), JsonUtil.object2JsonStr(fyOrderResultVo));
        redisTemplate.expire("order:" + order.getOrderNum(), 120, TimeUnit.SECONDS);
        //发送信息至延迟 消息队列，主要用来做订单延时处理
        sendDelayMessage(fygoodsCartVo.getList());
        //返回预订单信息
        return Result.ok(order);
    }

    /**
     * @author tzj
     * @description 计算总价   goods.getShopPrice().multiply(new BigDecimal(fygoodsCart.getCount()))
     * @date 2020/4/26 16:34
     */
    private BigDecimal getTotalPrice(FygoodsCart fygoodsCart, int type) {
        //正常
        if (type == ZHENGCHANG_TYPE || type == ZHIBO_TYPE) {
            Goods goods = goodsService.get(fygoodsCart.getFyGoodsId());
            if (goods == null || goods.getState() == 2) {
                return null;
            } else {
                return goods.getShopPrice().multiply(new BigDecimal(fygoodsCart.getCount()));
            }
        }
        //秒杀
        if (type == SKILL_TYPE) {
            Fygoods fygoods = fygoodsService.get(fygoodsCart.getFyGoodsId());
            if (fygoods == null || fygoods.getState() == 2) {
                return null;
            } else {
                return fygoods.getKillPrice().multiply(new BigDecimal(fygoodsCart.getCount()));
            }
        }
        //拼团
        if (type == PINTUAN_TYPE) {
            GrouponGoods grouponGoods = grouponGoodsService.get(fygoodsCart.getFyGoodsId());
            if (grouponGoods == null || grouponGoods.getState() == 2) {
                return null;
            } else {
                return grouponGoods.getGroupPrice().multiply(new BigDecimal(fygoodsCart.getCount()));
            }
        }
        //分享赚
        if (type == SHARE_TYPE) {
            ShareGoods shareGoods = shareGoodsService.get(fygoodsCart.getFyGoodsId());
            if (shareGoods == null || shareGoods.getState() == 2) {
                return null;
            } else {
                return shareGoods.getPrice().multiply(new BigDecimal(fygoodsCart.getCount()));
            }
        }


        return null;
    }

    /**
     * @author tzj
     * @description 判断是否家电
     * @date 2020/4/26 16:22
     */
    private Boolean checkJiaDian(FygoodsCart fygoodsCart, int type) {
        //正常
        if (type == ZHENGCHANG_TYPE || type == ZHIBO_TYPE) {
            return goodsService.get(fygoodsCart.getFyGoodsId()).getJiadian() == 2;
        }
        //秒杀
        if (type == SKILL_TYPE) {
            return goodsService.get(fygoodsCart.getGoodsId()).getJiadian() == 2;
        }
        //拼团
        if (type == PINTUAN_TYPE) {
            return goodsService.get(fygoodsCart.getGoodsId()).getJiadian() == 2;
        }
        return false;
    }

    /**
     * @author tzj
     * @description 判断库存
     * @date 2020/4/26 16:03
     */
    private Boolean checkStoreCount(FygoodsCart fygoodsCart, int type) {
        //正常
        if (type == ZHENGCHANG_TYPE || type == ZHIBO_TYPE) {
            return fygoodsCart.getCount() > goodsService.get(fygoodsCart.getFyGoodsId()).getStoreCount();
        }
        //秒杀
        if (type == SKILL_TYPE) {
            return fygoodsCart.getCount() > fygoodsService.get(fygoodsCart.getFyGoodsId()).getStoreCount();
        }
        //拼团
        if (type == PINTUAN_TYPE) {
            return fygoodsCart.getCount() > grouponGoodsService.get(fygoodsCart.getFyGoodsId()).getStoreCount();
        }
        //分享赚
        if (type == SHARE_TYPE) {
            return fygoodsCart.getCount() > shareGoodsService.get(fygoodsCart.getFyGoodsId()).getStoreCount();
        }
        //砍价
        if (type == BARGAIN_TYPE) {
            return fygoodsCart.getCount() > bargainGoodsService.get(fygoodsCart.getFyGoodsId()).getStoreCount();
        }
        return false;
    }

    /**
     * @author tzj
     * @description 找折扣
     * @date 2020/4/26 15:59
     */
    public static BigDecimal getBigDecimal(@Session("ShopUser") ShopUser shopUser, @RequestBody FygoodsCartVo fygoodsCartVo, String key, BigDecimal discount, CouponService couponService) {
        if (StringUtils.isNotEmpty(fygoodsCartVo.getCouponsId())) {
            Coupon coupon = couponService.get(fygoodsCartVo.getCouponsId());
            //判断是否过期等
            if (coupon != null && coupon.getCouponState() == 1 && coupon.getEndTime().after(new Date()) && coupon.getUserId().equals(shopUser.getId())) {
                coupon.setCouponState(2);
                coupon.setOrderId(key);
                couponService.update(coupon);
                discount = new BigDecimal(coupon.getCouponMoney());
            }
        }
        return discount;
    }


    /**
     * @author tzj
     * @description 商品详情存入数据库
     * @date 2020/4/20 11:41
     */
    public FyOrderItem createOrderDetail(FygoodsCart fygoodsCart, Integer count, String key, String dealerId, int type, Boolean discountFlg, Boolean costFlg) {
        FyOrderItem fyOrderItem = new FyOrderItem();
        //如果是家电下单
        GoodsDealer goodsDealer = new GoodsDealer();
        if (!"".equals(dealerId) && dealerId != null) {
            goodsDealer = goodsDealerService.get(dealerId);
        }
        //存储家电信息
        if (goodsDealer != null) {
            fyOrderItem.setDealerId(dealerId);
            fyOrderItem.setJiadian(2);
            fyOrderItem.setDealerName(goodsDealer.getName());
        }


        //封装实体类
        fyOrderItem.setId(KeyUtils.getKey());
        fyOrderItem.setDiscount(fygoodsCart.getDiscount());
        fyOrderItem.setOrderNum(key);
        fyOrderItem.setSendType(fygoodsCart.getSendType());
        fyOrderItem.setCount(count);
        fyOrderItem.setType(fygoodsCart.getType());
        //1表示未删除
        fyOrderItem.setIsdelete(1);
        //是否申请退款  1 表示否
        fyOrderItem.setIsrefound(1);
        //是否退款成功  1 表示否
        fyOrderItem.setRefoundState(1);

        //正常
        if (type == ZHENGCHANG_TYPE || type == ZHIBO_TYPE) {
            Goods goods = goodsService.get(fygoodsCart.getFyGoodsId());
            fyOrderItem.setGoodsName(goods.getName());
            fyOrderItem.setGoodsId(goods.getId());
            fyOrderItem.setGoodsCover(goods.getGoodCover());
            fyOrderItem.setSinglePrice(goods.getShopPrice());
            if (discountFlg) {
                if (costFlg) {
                    fyOrderItem.setDiscount(fygoodsCart.getDiscount());
                    fyOrderItem.setActivityCost(fygoodsCart.getActivityCost());
                    fyOrderItem.setPayPrice(goods.getShopPrice().multiply(new BigDecimal(count)).subtract(fygoodsCart.getDiscount()).subtract(fygoodsCart.getActivityCost()));
                } else {
                    fyOrderItem.setPayPrice(goods.getShopPrice().multiply(new BigDecimal(count)).subtract(fygoodsCart.getDiscount()));
                }
            } else {
                if (costFlg) {
                    fyOrderItem.setActivityCost(fygoodsCart.getActivityCost());
                    fyOrderItem.setPayPrice(goods.getShopPrice().multiply(new BigDecimal(count)).subtract(fygoodsCart.getActivityCost()));
                } else {
                    fyOrderItem.setPayPrice(goods.getShopPrice().multiply(new BigDecimal(count)));
                }
            }
            //更新数据库里面商品的库存
            //减库存
            goods.setStoreCount(goods.getStoreCount() - count);
            //更新操作
            goodsService.update(goods);
        }
        //秒杀
        if (type == SKILL_TYPE) {
            Fygoods goods = fygoodsService.get(fygoodsCart.getFyGoodsId());
            fyOrderItem.setGoodsName(goods.getGoodsName());
            fyOrderItem.setGoodsId(goods.getId());
            fyOrderItem.setGoodsCover(goods.getGoodCover());
            fyOrderItem.setSinglePrice(goods.getKillPrice());
            if (discountFlg) {
                if (costFlg) {
                    fyOrderItem.setDiscount(fygoodsCart.getDiscount());
                    fyOrderItem.setActivityCost(fygoodsCart.getActivityCost());
                    fyOrderItem.setPayPrice(goods.getKillPrice().multiply(new BigDecimal(count)).subtract(fygoodsCart.getDiscount()).subtract(fygoodsCart.getActivityCost()));
                } else {
                    fyOrderItem.setPayPrice(goods.getKillPrice().multiply(new BigDecimal(count)).subtract(fygoodsCart.getDiscount()));
                }
            } else {
                if (costFlg) {
                    fyOrderItem.setActivityCost(fygoodsCart.getActivityCost());
                    fyOrderItem.setPayPrice(goods.getKillPrice().multiply(new BigDecimal(count)).subtract(fygoodsCart.getActivityCost()));
                } else {
                    fyOrderItem.setPayPrice(goods.getKillPrice().multiply(new BigDecimal(count)));
                }
            }
            //更新数据库里面商品的库存
            //减库存
            goods.setStoreCount(goods.getStoreCount() - count);
            //更新操作
            fygoodsService.update(goods);
        }
        //拼团
        if (type == PINTUAN_TYPE) {
            GrouponGoods goods = grouponGoodsService.get(fygoodsCart.getFyGoodsId());
            fyOrderItem.setGoodsName(goods.getGoodsName());
            fyOrderItem.setGoodsId(goods.getId());
            fyOrderItem.setGoodsCover(goods.getGoodCover());
            fyOrderItem.setSinglePrice(goods.getGroupPrice());
            if (discountFlg) {
                if (costFlg) {
                    fyOrderItem.setDiscount(fygoodsCart.getDiscount());
                    fyOrderItem.setActivityCost(fygoodsCart.getActivityCost());
                    fyOrderItem.setPayPrice(goods.getGroupPrice().multiply(new BigDecimal(count)).subtract(fygoodsCart.getDiscount()).subtract(fygoodsCart.getActivityCost()));
                } else {
                    fyOrderItem.setPayPrice(goods.getGroupPrice().multiply(new BigDecimal(count)).subtract(fygoodsCart.getDiscount()));
                }
            } else {
                if (costFlg) {
                    fyOrderItem.setActivityCost(fygoodsCart.getActivityCost());
                    fyOrderItem.setPayPrice(goods.getGroupPrice().multiply(new BigDecimal(count)).subtract(fygoodsCart.getActivityCost()));
                } else {
                    fyOrderItem.setPayPrice(goods.getGroupPrice().multiply(new BigDecimal(count)));
                }
            }
            //更新数据库里面商品的库存
            //减库存
            goods.setStoreCount(goods.getStoreCount() - count);
            //更新操作
            grouponGoodsService.update(goods);
        }
        //分享赚
        if (type == SHARE_TYPE) {
            ShareGoods goods = shareGoodsService.get(fygoodsCart.getFyGoodsId());
            fyOrderItem.setGoodsName(goods.getGoodsName());
            fyOrderItem.setGoodsId(goods.getId());
            fyOrderItem.setGoodsCover(goods.getGoodCover());
            fyOrderItem.setSinglePrice(goods.getPrice());
            if (discountFlg) {
                if (costFlg) {
                    fyOrderItem.setDiscount(fygoodsCart.getDiscount());
                    fyOrderItem.setActivityCost(fygoodsCart.getActivityCost());
                    fyOrderItem.setPayPrice(goods.getPrice().multiply(new BigDecimal(count)).subtract(fygoodsCart.getDiscount()).subtract(fygoodsCart.getActivityCost()));
                } else {
                    fyOrderItem.setPayPrice(goods.getPrice().multiply(new BigDecimal(count)).subtract(fygoodsCart.getDiscount()));
                }
            } else {
                if (costFlg) {
                    fyOrderItem.setActivityCost(fygoodsCart.getActivityCost());
                    fyOrderItem.setPayPrice(goods.getPrice().multiply(new BigDecimal(count)).subtract(fygoodsCart.getActivityCost()));
                } else {
                    fyOrderItem.setPayPrice(goods.getPrice().multiply(new BigDecimal(count)));
                }
            }
            //更新数据库里面商品的库存
            //减库存
            goods.setStoreCount(goods.getStoreCount() - count);
            //更新操作
            shareGoodsService.update(goods);
        }
        //详情信息存入数据库
        fyOrderItemService.save(fyOrderItem);
        return fyOrderItem;
    }
    /**
     * @author tzj
     * @description 通过订单号查询订单信息
     * @date 2020/4/20 11:41
     */
    @RequestMapping("/getOrderInfo")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> getOrderInfo(@RequestBody Map<String, Object> param) {
        FyOrderResultVo fyOrderResultVo = new FyOrderResultVo();
        //从redis中拿数据
        String str = redisTemplate.opsForValue().get("order:" + param.get("orderNum"));
        if (null != str) {
            fyOrderResultVo = JsonUtil.jsonStr2Object(str, FyOrderResultVo.class);
        }
        return Result.ok(fyOrderResultVo);
    }


    /**
     * 微信支付
     */
    @RequestMapping("/wxpay")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> wxpay(HttpServletRequest request, @Session("ShopUser") ShopUser shopUser, @RequestBody OrderAddress orderAddress) {
        //通过订单号查询与订单订单信息
        Map<String, Object> queryMap = new HashMap<>();
        queryMap.put("orderNum", orderAddress.getOrderNum());
        FyOrder order = fyOrderService.getOne(queryMap);
        if (order == null) {
            return Result.of(Status.ClientError.BAD_REQUEST, "订单创建错误");
        }
        if (orderAddress.getType()!=null&&orderAddress.getType()==6){
            List<FyOrderItem> list = fyOrderItemService.list(queryMap);
            for (FyOrderItem fyOrderItem : list) {
                fyOrderItem.setSendType(orderAddress.getSendType());
                fyOrderItemService.update(fyOrderItem);
            }
        }

        //认领时间
        if (orderAddress.getClaimTime() != null) {
            order.setClaimTime(orderAddress.getClaimTime());
            fyOrderService.update(order);
        }
        //地址订单地址
        if (orderAddress.getCity() == null || orderAddress.getArea() == null || orderAddress.getProvince() == null ||
                orderAddress.getDetailAddress() == null) {
            return Result.of(Status.ClientError.BAD_REQUEST, "地址信息不全请检查");
        }
        OrderAddress info = new OrderAddress();
        info.setId(KeyUtils.getKey());
        info.setProvince(orderAddress.getProvince());
        info.setCity(orderAddress.getCity());
        info.setArea(orderAddress.getArea());
        info.setDetailAddress(orderAddress.getDetailAddress());
        info.setOrderId(order.getId());
        info.setDeliveryUser(orderAddress.getDeliveryUser());
        info.setDeliveryPhone(orderAddress.getDeliveryPhone());
        //存入订单地址表
        orderAddressService.save(info);
        //调用微信支付
        Result<?> result = null;
        try {
            result = FyWxPay.wxPay(request, order, shopUser);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


    /**
     * 未支付生成订单信息没支付，再次微信支付
     */
    @RequestMapping("/reset/payOrder")
    @Transactional
    public Result<?> resetPayOderController(HttpServletRequest request, @Session("ShopUser") ShopUser shopUser, @RequestBody Map<String, Object> param) {
        //更具订单号查寻未支付的订单信息。
        FyOrder unPayFyOrder = fyOrderService.getOne(param);
        Map<String, Object> params = new HashMap<>(5);
        params.put("orderNum", unPayFyOrder.getOrderNum());
        List<FyOrderItem> list = fyOrderItemService.list(params);
        if (list.isEmpty()){
            return Result.of(Status.ClientError.BAD_REQUEST,  "订单存在问题请勿支付");
        }
        long l = unPayFyOrder.getOrderTime().getTime()+ 60 * 60 * 60;
        long currentTime = System.currentTimeMillis();
        if (l<currentTime){
            return Result.of(Status.ClientError.BAD_REQUEST,  "订单超时请左滑删除");
        }






        //调用微信支付
        Result<?> result = null;
        try {
            result = FyWxPay.wxPay(request, unPayFyOrder, shopUser);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


    /**
     * 微信支付的回调
     */
    @RequestMapping(value = "/wxPay/wxNotify")
    @PermissionMode(PermissionMode.Mode.White)
    @SystemControllerLog(description = "支付回调查看")
    public synchronized void wxNotify(HttpServletRequest request, HttpServletResponse response) throws Exception {
        System.out.println("微信支付回调开始");
        InputStream inputStream = request.getInputStream();
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
            String line = null;
            StringBuilder sb = new StringBuilder();
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            br.close();
            inputStream.close();
            //sb为微信返回的xml
            String notityXml = sb.toString();
            Map<String, String> map = WXPayUtil.xmlToMap(notityXml);
            SortedMap<String, String> sortedMap = WXPayUtil.getSortedMap(map);
            //判断签名是否正确
            if (WXPayUtil.isCorrectSign(sortedMap, WeChatConfig.APIKEYFuYang)) {
                if ("SUCCESS".equals(sortedMap.get("result_code"))) {
                    //获取订单号
                    String out_trade_no = (String) sortedMap.get("out_trade_no");
                    //改变订单状态并且产生积分
                    sendIntegral(out_trade_no);
                    //将订单号发送至消息队列
                    sendOrderMessage(out_trade_no);
                    //添加自己的业务逻辑代码
                    response.setContentType("text/xml");
                    response.getWriter().println("success");
                    return;
                }
            }
            response.setContentType("text/xml");
            response.getWriter().println("fail");
        } catch (Exception e) {
            System.out.println("错误");
        }

    }

    /**
     * @author tzj
     * @description 根据订单生成积分
     * @date 2020-05-18 9:26
     */
    private void sendIntegral(String out_trade_no) {
        FyOrder fyOrder = new FyOrder();
        fyOrder.setOrderNum(out_trade_no);
        fyOrder = fyOrderService.getOne(fyOrder);
        if (fyOrder != null) {
            fyOrder.setState(2);
            fyOrder.setPayTime(new Date());
            fyOrderService.update(fyOrder);
        } else {
            return;
        }
        ShopUser shareUser = new ShopUser();


        ShopUser shopUser = shopUserService.get(fyOrder.getUserId());

        Map<String, Object> param = new HashMap<>(5);
        param.put("orderNum", out_trade_no);
        List<FyOrderItem> list = fyOrderItemService.list(param);
        double newIntegral = shopUser.getIntegral();
        System.out.println("用户原来积分为" + newIntegral);
        double newActivityCost = 0.0;
        if (CollectionUtils.isNotEmpty(list)) {
            for (FyOrderItem fyOrderItem : list) {
                if (ZHENGCHANG_TYPE.equals(fyOrderItem.getType()) || ZHIBO_TYPE.equals(fyOrderItem.getType())||SHARE_TYPE.equals(fyOrderItem.getType())) {
                    Double integral = getIntegral(fyOrderItem.getGoodsId());
                    System.out.println("新增积分为" + fyOrderItem.getPayPrice().doubleValue() / integral);
                    System.out.println("新增积分为" + fyOrderItem.getPayPrice().doubleValue() / integral);
                    System.out.println("新增积分为" + fyOrderItem.getPayPrice().doubleValue() / integral);
                    newIntegral = newIntegral + fyOrderItem.getPayPrice().doubleValue() / integral;
                }
                if (SHARE_TYPE.equals(fyOrderItem.getType())) {
                    newActivityCost = newActivityCost + getCost(fyOrderItem.getGoodsId()) * fyOrderItem.getCount();
                }
            }
        }
        shopUser.setIntegral(newIntegral);
        shopUserService.update(shopUser);
        if (StringUtils.isNotEmpty(fyOrder.getShareId())||fyOrder.getShareId()!=null) {
            shareUser = shopUserService.get(fyOrder.getShareId());
            shareUser.setActivityCost(shareUser.getActivityCost() + newActivityCost);
            shopUserService.update(shareUser);
        }
    }

    /**
     * @author tzj
     * @description 找到佣金
     * @date 2020-05-19 9:37
     */
    private Double getCost(String goodsId) {
        ShareGoods shareGoods = shareGoodsService.get(goodsId);
        if (shareGoods != null) {
            return shareGoods.getActivityCost().doubleValue();
        }
        return 0.0;
    }

    /**
     * @author tzj
     * @description 获取当前商品积分规则
     * @date 2020-05-18 9:38
     */
    private Double getIntegral(String goodsId) {
        Goods goods = goodsService.get(goodsId);
        if (goods != null) {
            GoodsBase goodsBase = goodsBaseService.get(goods.getGoodsId());
            if (goodsBase != null) {
                Categorys categorys = categorysService.get(goodsBase.getCategoryId());
                if (categorys != null) {
                    return categorys.getIntegral();
                }
            }
        }
        return 0.0;
    }

    /*
     * 小程序申请退款
     */
    @RequestMapping("/apply/refund")
    public Result<?> applyRefund(@RequestBody FyOrderItem fyOrderItem) {
        //通过订单编码找到对应的订单信息
        fyOrderItem = fyOrderItemService.get(fyOrderItem.getId());
        FyOrder fyOrder = new FyOrder();
        fyOrder.setOrderNum(fyOrderItem.getOrderNum());
        fyOrder = fyOrderService.getOne(fyOrder);
        if (fyOrder == null) {
            return Result.of(Status.ClientError.BAD_REQUEST, "未查询到响应的订单信息");
        }
        if (fyOrder.getState() != 2) {
            return Result.of(Status.ClientError.BAD_REQUEST, "只有已经支付的订单才能退款");
        }
        //更新订单详情表

        //2表示是申请退款
        fyOrderItem.setIsrefound(2);
        //更新数据库
        fyOrderItemService.update(fyOrderItem);
        //更新基本信息表
        Map<String, Object> map = new HashMap<>();
        map.put("orderNum", fyOrderItem.getOrderNum());
        //1表示是否申请退款
        map.put("isrefound", 2);
        fyOrderService.updateSelective(map);
        return Result.ok();
    }

    /**
     * @author tzj
     * @description 后台退款
     * @date 2020/4/20 11:46
     */
    @RequestMapping("/programRefund")
    @ResponseBody
    public Result<?> wxPayRefund(@Session(SessionKeys.USER) User user, @RequestBody FyOrderItem fyOrderItem) {
        fyOrderItem=      fyOrderItemService.get(fyOrderItem.getId());
        //非空判断
        if (null != fyOrderItem) {
            //查询订单表，
            Map<String, Object> map = new HashMap<>();
            map.put("orderNum", fyOrderItem.getOrderNum());
            FyOrder getOrderInfo = fyOrderService.getOne(map);
            BigDecimal payPrice = getOrderInfo.getPayPrice().multiply(new BigDecimal(100));
            //支付金额减去已经退款的金额
            if (getOrderInfo.getState() == 1) {
                return Result.of(Status.ClientError.BAD_REQUEST, "只有支付的订单才可以退款");
            }
            if (payPrice.compareTo(fyOrderItem.getPayPrice()) < 0) {
                return Result.of(Status.ClientError.BAD_REQUEST, "退款金额异常");
            }
            getOrderInfo.setRefoundPrice(fyOrderItem.getPayPrice());

            if (fyOrderItem.getPayPrice().compareTo(BigDecimal.ZERO) == 0) {
                ShopUser shopUser = shopUserService.get(getOrderInfo.getUserId());
                Double activityCost = shopUser.getActivityCost();
                if (fyOrderItem.getActivityCost()!=null&&fyOrderItem.getActivityCost().compareTo(BigDecimal.ZERO) > 0) {
                    activityCost = activityCost + fyOrderItem.getActivityCost().doubleValue();
                }
                if (StringUtils.isNotEmpty(getOrderInfo.getGiftCardId())) {
                    GiftCardUser giftCardUser = giftCardUserService.get(getOrderInfo.getGiftCardId());
                    Double realityMoney;
                    if (fyOrderItem.getGiftPrice()!=null &&fyOrderItem.getGiftPrice().compareTo(BigDecimal.ZERO) > 0) {
                        realityMoney = giftCardUser.getRealityMoney() + fyOrderItem.getGiftPrice().doubleValue();
                        giftCardUser.setRealityMoney(realityMoney);
                        giftCardUserService.update(giftCardUser);
                    }
                }
                returnOrder(user, fyOrderItem, getOrderInfo.getShareId());
                shopUser.setActivityCost(activityCost);
                shopUserService.update(shopUser);
                return Result.ok();
            }
            //调用微信退款
            Result<?> result = FyWxPay.wxPayRefund(getOrderInfo, fyOrderItem);
            //如果退款成功，该改变订单状态
            if ("200".equals(result.getCode())) {
                //退款逻辑
                returnOrder(user, fyOrderItem, getOrderInfo.getShareId());
                return Result.ok("退款成功");
            }
        }
        return Result.of(Status.ClientError.BAD_REQUEST, "退款失败");
    }


    /**
     * 退款逻辑
     */
    public void returnOrder(User user, FyOrderItem fyOrderItem, String id) {
        //改变商品基本表信息
        Map<String, Object> param = new HashMap<>();
        param.put("orderNum", fyOrderItem.getOrderNum());
        //根据订单号查询基本订单信息
        FyOrder fyOrder = fyOrderService.getOne(param);
        //退款金额更新
        fyOrder.setRefoundPrice(fyOrder.getRefoundPrice().add(fyOrderItem.getPayPrice()));
        //1表示包含退款订单
        fyOrder.setIshasRefound(2);
        if ((fyOrder.getRefoundPrice().compareTo(fyOrder.getPayPrice())) == 0) {
            //4表示表示全额退款
            fyOrder.setState(4);
        } else {
            //3表示部分退款
            fyOrder.setState(3);
        }
        //退单时间
        fyOrder.setRefoundTime(new Date());
        fyOrderService.update(fyOrder);
        //更新订单基本表信息
        //已确认退款
        fyOrderItem.setRefoundState(2);
        fyOrderItem.setOperatorName(user.getNickname());
        fyOrderItemService.update(fyOrderItem);
        ShopUser shopUser = shopUserService.get(fyOrder.getUserId());
        ShopUser shareUser = shopUserService.get(fyOrder.getShareId());


        if (StringUtils.isNotEmpty(fyOrder.getGiftCardId())) {
            GiftCardUser giftCardUser = giftCardUserService.get(fyOrder.getGiftCardId());
            if (giftCardUser != null) {
                giftCardUser.setRealityMoney(giftCardUser.getRealityMoney() + fyOrderItem.getGiftPrice().doubleValue());
            }
        }
        if (ZHIBO_TYPE.equals(fyOrderItem.getType()) || ZHENGCHANG_TYPE.equals(fyOrderItem.getType())) {
            //减少积分
            if (shopUser != null) {
                //减少积分
                Double integral = getIntegral(fyOrderItem.getGoodsId());
                double v = fyOrderItem.getPayPrice().doubleValue() / integral;
                double v1 = shopUser.getIntegral() - v;
                if (v1 < 0) {
                    shopUser.setIntegral(0.0);
                } else {
                    shopUser.setIntegral(v1);
                }
                shopUserService.update(shopUser);
            }

            //改变商品表的的库存
            Goods goods = goodsService.get(fyOrderItem.getGoodsId());
            if (null != goods) {
                //原来库存加上退款商品的库存
                goods.setStoreCount(goods.getStoreCount() + fyOrderItem.getCount());
                goodsService.update(goods);
            }
        } else if (SKILL_TYPE.equals(fyOrderItem.getType())) {
            //正常商品
            Fygoods fygoods = fygoodsService.get(fyOrderItem.getFyGoodsId());
            if (null != fygoods) {
                //原来库存加上退款商品的库存
                fygoods.setStoreCount(fygoods.getStoreCount() + fyOrderItem.getCount());
                fygoodsService.update(fygoods);
            }
        } else if (PINTUAN_TYPE.equals(fyOrderItem.getType())) {
            GrouponGoods goods = grouponGoodsService.get(fyOrderItem.getGoodsId());
            if (null != goods) {
                //原来库存加上退款商品的库存
                goods.setStoreCount(goods.getStoreCount() + fyOrderItem.getCount());
                grouponGoodsService.update(goods);
            }
        } else if (SHARE_TYPE.equals(fyOrderItem.getType())) {
            ShareGoods goods = shareGoodsService.get(fyOrderItem.getGoodsId());

            //减少积分
            Double integral = getIntegral(fyOrderItem.getGoodsId());
            double v = fyOrderItem.getPayPrice().doubleValue() / integral;
            double v1 = shopUser.getIntegral() - v;
            if (v1 < 0) {
                shopUser.setIntegral(0.0);
            } else {
                shopUser.setIntegral(v1);
            }
            shopUserService.update(shopUser);

            if (null != goods) {
                //减少佣金
                if (shareUser != null) {
                    BigDecimal multiply = goods.getActivityCost().multiply(new BigDecimal(fyOrderItem.getCount()));
                    double v2 = shopUser.getActivityCost() - multiply.doubleValue();
                    if (v2 < 0) {
                        shareUser.setActivityCost(0.0);
                    } else {
                        shareUser.setActivityCost(v);
                    }
                    shopUserService.update(shareUser);
                }
                //原来库存加上退款商品的库存
                goods.setStoreCount(goods.getStoreCount() + fyOrderItem.getCount());
                shareGoodsService.update(goods);
            }
        }
    }


    /**
     * 发送订单消息至消息队列
     */

    private void sendOrderMessage(String orderNum) {
        rabbitTemplate.convertAndSend(
                "exchange.order.done",
                "orderNum",
                orderNum,
                new MessagePostProcessor() {
                    @Override
                    public Message postProcessMessage(Message message) throws AmqpException {
                        //消息有效期30分钟
                        message.getMessageProperties().setExpiration(String.valueOf(1800000));
//                        message.getMessageProperties();
                        return message;
                    }
                });
    }


    /**
     * 发送延迟消息消息队列。主要用来做订单回滚
     */


    private void sendDelayMessage(List<FygoodsCart> fygoodsCarts) {
        rabbitTemplate.convertAndSend(
                "exchange.delay.order.begin",
                "delay",
                JSON.toJSONString(fygoodsCarts),
                new MessagePostProcessor() {
                    @Override
                    public Message postProcessMessage(Message message) throws AmqpException {
                        //消息有效期30分钟
                        //message.getMessageProperties().setExpiration(String.valueOf(7200000));
//                          message.getMessageProperties();
                        message.getMessageProperties().setExpiration(String.valueOf(180000));
                        return message;
                    }
                });
    }

    /**
     * @author tzj
     * @description
     * @date 2020/4/8 16:19
     */
    @RequestMapping("/deliver")
    public Result<?> deliver(@RequestBody @Valid IdParam idsParam) {
        FyOrder fyOrder = new FyOrder();
        fyOrder.setId(idsParam.getId());
        fyOrder = fyOrderService.getOne(fyOrder);
        if (fyOrder == null) {
            return Result.of(Status.ClientError.BAD_REQUEST, "订单未找到");
        }
        fyOrder.setDeliver(2);
        fyOrderService.update(fyOrder);
        return Result.ok("发货成功");
    }


    /**
     * @author tzj
     * @description 秒杀订单
     * @date 2020/4/10 11:41
     */
    @RequestMapping("/skillOrder")
    @PermissionMode(PermissionMode.Mode.White)
    @Transactional
    public Result<?> skillOrder(@Session("ShopUser") ShopUser shopUser, @RequestBody FygoodsCartVo fygoodsCartVo) {
        //生成订单详情
        String key = KeyUtils.getKey();
        Date date = new Date();
        String goodBody = "";
        //存放订单详情信息
        List<FyOrderItem> fyOrderItemList = new ArrayList<>();
        for (FygoodsCart fygoodsCart : fygoodsCartVo.getList()) {
            Map<String, Object> param = new HashMap<>(10);
            param.put("skillId", fygoodsCart.getGoodsId());
            Fygoods goods = fygoodsService.queryInfoListById(param);
            //判断购买数量是否超过限制
            if (goods.getLimitCount() < fygoodsCart.getCount()) {
                return Result.of(Status.ClientError.BAD_REQUEST, "超过限购数量" + (fygoodsCart.getCount() - goods.getLimitCount()));
            }
            if (goods.getStoreCount() < fygoodsCart.getCount()) {
                return Result.of(Status.ClientError.BAD_REQUEST, "超过库存数量，剩余商品数量为" + goods.getStoreCount());
            }
//            商品名称描述，存入数据库用户标识订单。
            goodBody += goods.getGoodsName() + "-";
            FyOrderItem fyOrderItem = createSkillOrder(goods, date, fygoodsCart.getCount(), key);
            fyOrderItemList.add(fyOrderItem);
//            记录订单号，延迟消息队列要用到
            fygoodsCart.setOrderNum(key);
        }
        //查询数据库，获取对应商品的信息
        FyOrder order = new FyOrder();
        //默认未发货
        order.setDeliver(1);
        order.setId(IdGenerator.uuid());
        order.setOrderNum(key);
        order.setOrderTime(date);
        order.setUserName(shopUser.getNickname());
        order.setUserId(shopUser.getId());
        order.setType(fygoodsCartVo.getType());
        order.setPhoneNum(shopUser.getPhone());
        order.setAllPrice(fygoodsCartVo.getPayAll());
        order.setDiscount(fygoodsCartVo.getDiscount());
        //支付金额，活动家减去折扣
        order.setPayPrice(fygoodsCartVo.getPayAll().subtract(fygoodsCartVo.getDiscount()));
        order.setGoodsBody(goodBody);
        //1表示未删除的状态
        order.setIsdelete(1);
        //1表示未支付的状态
        order.setState(1);
        //是否申请退款 1 表示没有  2 表示有
        order.setIsrefound(1);
        //退款金额默认为0
        order.setRefoundPrice(new BigDecimal(0));
        //默认不包含退款订单
        order.setIshasRefound(2);
        //认领时间
        order.setClaimTime(fygoodsCartVo.getClaimTime());
        //订单信息存入数据库
        fyOrderService.save(order);
        //订单对应的地址信息存入数据库
        //订单信息存一份到redis，支付页面获取数据直接从redis中拿，减轻数据库的压力   订单基本信息+订单详情
        FyOrderResultVo fyOrderResultVo = new FyOrderResultVo();
        fyOrderResultVo.setFyOrder(order);
        fyOrderResultVo.setFyOrderItemList(fyOrderItemList);
        redisTemplate.opsForValue().set("order:" + order.getOrderNum(), JsonUtil.object2JsonStr(fyOrderResultVo));
        redisTemplate.expire("order:" + order.getOrderNum(), 120, TimeUnit.SECONDS);
        //发送信息至延迟 消息队列，主要用来做订单延时处理
        sendDelayMessage(fygoodsCartVo.getList());
        //返回预订单信息
        return Result.ok(order);
    }


    @Transactional
    public FyOrderItem createSkillOrder(Fygoods goods, Date date, Integer count, String key) {
        FyOrderItem fyOrderItem = new FyOrderItem();
        //封装实体类
        fyOrderItem.setId(IdGenerator.uuid());
        fyOrderItem.setGoodsName(goods.getGoodsName());
        fyOrderItem.setGoodsId(goods.getId());
        fyOrderItem.setGoodsCover(goods.getCover());
        fyOrderItem.setOrderNum(key);
        fyOrderItem.setSinglePrice(goods.getKillPrice());
        fyOrderItem.setCount(count);
        fyOrderItem.setCategorysId(goods.getCategoryId());
        fyOrderItem.setCategorysName(goods.getCategoryName());
        fyOrderItem.setBrandId(goods.getBrandId());
        fyOrderItem.setBrandName(goods.getBrandName());
        //1表示未删除
        fyOrderItem.setIsdelete(1);
        //是否申请退款  1 表示否
        fyOrderItem.setIsrefound(1);
        //是否退款成功  2 表示否
        fyOrderItem.setRefoundState(2);
        fyOrderItem.setPayPrice(goods.getKillPrice().multiply(new BigDecimal(count)));
        //更新数据库里面商品的库存
        //减库存
        goods.setStoreCount(goods.getStoreCount() - count);
        //更新操作
        fygoodsService.update(goods);
        //详情信息存入数据库
        fyOrderItemService.save(fyOrderItem);
        return fyOrderItem;
    }

    /**
     * @author tzj
     * @description 订单额统计
     * @date 2020/4/11 13:18
     */
    @RequestMapping("/statisticAllAmountByDate")
    public Result<Collection<?>> statisticAllAmountByDate(@RequestBody(required = false) Map<String, Object> param) {
        if (param == null) {
            param = new HashMap<>();
        }
        List<OrderStatisticItem> items = fyOrderService.statisticAllAmountByDate(param);
        if (items == null) {
            return Result.ok(Collections.emptyList());
        }
        return Result.ok(items);
    }

    /**
     * @author tzj
     * @description 销售量
     * @date 2020/4/11 13:36
     */
    @RequestMapping("/qidoGoodsNameNumTopTen")
    public Result<Object> qidoGoodsNameNumTopTen(@RequestBody(required = false) Map<String, Object> param) {
        return Result.ok(fyOrderService.qidoGoodsNameNumTopTen(param));
    }

    @RequestMapping("/qidoGoodsNameMoneyTopTen")
    public Result<Object> qidoGoodsNameMoneyTopTen(@RequestBody(required = false) Map<String, Object> param) {
        return Result.ok(fyOrderService.qidoGoodsNameMoneyTopTen(param));
    }


    /**
     * @author tzj
     * @description 今日新增订单
     * @date 2020/4/27 11:36
     */
    @RequestMapping("/orderAmount")
    public Result<?> orderAmount(@Session(SessionKeys.USER) User user) {
        if (!"1a".equals(user.getId())){
            return null;
        }
        String fmt = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(fmt);
        String startTime = sdf.format(new Date());
        String endTime = startTime + "23:59:59";
        Map<String, Object> params = new HashMap<>();
        params.put("startTime", startTime);
        params.put("endTime", endTime);
        return Result.ok(fyOrderService.orderAmount(params));

    }


    /**
     * @author tzj
     * @description 累计销售额
     * @date 2020/4/27 11:40
     */
    @RequestMapping("totalPrice")
    public Result<?> totalPrice(@Session(SessionKeys.USER) User user,@RequestBody(required = false) Map<String, Object> param) {
        if (!"1a".equals(user.getId())){
            return null;
        }
        if (param == null) {
            param = new HashMap<>();
        }
        param.put("state", 2);
        Double totalPrice = fyOrderService.totalPrice(param);
        return Result.ok(totalPrice);
    }

    /**
     * @author tzj
     * @description 今日销售额
     * @date 2020/4/27 11:41
     */

    @RequestMapping("todayPrice")
    public Result<?> todayPrice(@Session(SessionKeys.USER) User user,@RequestBody Map<String, Object> param) {
        if (!"1a".equals(user.getId())){
            return null;
        }
        if (param == null) {
            param = new HashMap<>();
        }
        param.put("state", 2);
        String fmt = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(fmt);
        String startTimeS = sdf.format(new Date());
        String endTime = startTimeS + " 23:59:59";
        String startTime = startTimeS + " 00:00:00";
        param.put("startTime", startTime);
        param.put("endTime", endTime);
        Double totalPrice = fyOrderService.totalPrice(param);
        return Result.ok(totalPrice);
    }
    /**
     * @author tzj
     * @description 订单生成
     * @date 2020-05-20 10:07
     */
    @RequestMapping("/newPay")
    public Result<?> newPay(@Session("ShopUser") ShopUser shopUser, @RequestBody FygoodsCartVo fygoodsCartVo) {
        shopUser= shopUserService.get(shopUser.getId());
        for (FygoodsCart fygoodsCart : fygoodsCartVo.getList()) {
            Boolean storeCount= checkStoreCount(fygoodsCart,fygoodsCartVo.getType());
            if (storeCount){
                return Result.of(Status.ClientError.BAD_REQUEST, "该商品库存不足，请调整数量");
            }
        }
        if (SKILL_TYPE.equals(fygoodsCartVo.getType())||SHARE_TYPE.equals(fygoodsCartVo.getType()) || PINTUAN_TYPE.equals(fygoodsCartVo.getType()) || BARGAIN_TYPE.equals(fygoodsCartVo.getType())) {
//            Boolean flg1 = checkOnly(shopUser, fygoodsCartVo);
//            if (flg1) {
//                return Result.of(Status.ClientError.BAD_REQUEST, "您已经参加了该商品的活动");
//            }
            Boolean flg2 = checkTime(fygoodsCartVo);
            if (flg2){
                return Result.of(Status.ClientError.BAD_REQUEST, "活动未开始或者已经结束了");
            }
        }
        Boolean flg3= checkState(fygoodsCartVo);
        if (flg3){
            return Result.of(Status.ClientError.BAD_REQUEST, "该商品已经下架了");
        }
        String key = KeyUtils.getKey();
        //校验用户信息
        if (shopUser == null || StringUtils.isEmpty(shopUser.getPhone())) {
            return Result.of(Status.ClientError.FORBIDDEN, "请先完善收货信息");
        }
        BigDecimal totalPriceNew = getTotalPriceNew(fygoodsCartVo, fygoodsCartVo.getType());
        BigDecimal hasDiscount = new BigDecimal(0);
        BigDecimal discount = new BigDecimal(0);
        //如果使用了佣金,秒杀不能使用
        if (!SKILL_TYPE.equals(fygoodsCartVo.getType()) && !PINTUAN_TYPE.equals(fygoodsCartVo.getType()) && !BARGAIN_TYPE.equals(fygoodsCartVo.getType())) {
            if (fygoodsCartVo.getCostType() == 1) {
                int enoughPrice = payByActivityCost(shopUser, totalPriceNew, fygoodsCartVo, key);
                //1 代表佣金足够支付订单
                if (enoughPrice == 1) {
                    //产生积分和佣金
                    sendIntegral(key);
                    shopUser.setActivityCost(shopUser.getActivityCost() - totalPriceNew.doubleValue());
                    shopUserService.update(shopUser);
                    return Result.of(Status.ClientError.GONE, "订单完成");
                } else {
                    //把佣金的钱加入总折扣
                    hasDiscount = hasDiscount.add(new BigDecimal(shopUser.getActivityCost()));
                }
            }
            //是否使用优惠券
            discount = getBigDecimal(shopUser, fygoodsCartVo, key, discount, couponService);
            if (discount.compareTo(BigDecimal.ZERO) > 0) {
                hasDiscount = hasDiscount.add(discount);
                //折扣打到总价金额
                if (hasDiscount.compareTo(totalPriceNew) >= 0) {
                    payByCostAndDiscount(fygoodsCartVo, shopUser, totalPriceNew, discount, key);
                    shopUser.setActivityCost(0.0);
                    shopUserService.update(shopUser);
                    //产生积分和佣金
                    sendIntegral(key);
                    return Result.of(Status.ClientError.GONE, "订单完成");

                }
            }
        }

        //如有礼品卡
        if (StringUtils.isNotEmpty(fygoodsCartVo.getGiftCardUser())) {
            GiftCardUser giftCardUser = giftCardUserService.get(fygoodsCartVo.getGiftCardUser());
            //礼品卡的钱加入总折扣
            if (giftCardUser != null&&giftCardUser.getRealityMoney()>0) {
                hasDiscount = hasDiscount.add(new BigDecimal(giftCardUser.getRealityMoney()));
            }
            //如果折扣足够支付
            if (hasDiscount.compareTo(totalPriceNew) >= 0) {
                //判断是否有佣金
                if (fygoodsCartVo.getCostType() == 1) {
                    //有使用优惠券
                    if (discount.compareTo(BigDecimal.ZERO) > 0) {
                        payByCostAndCardAndDiscount(fygoodsCartVo, shopUser, totalPriceNew, discount);
                        giftCardUser.setRealityMoney(giftCardUser.getRealityMoney()-(totalPriceNew.doubleValue()-discount.doubleValue()-shopUser.getActivityCost()));
                        giftCardUserService.update(giftCardUser);
                        //产生积分和佣金
                        sendIntegral(key);
                    } else {
                        //产生积分和佣金
                        sendIntegral(key);
                        payByCostAndCard(fygoodsCartVo, shopUser, totalPriceNew);
                        giftCardUser.setRealityMoney(giftCardUser.getRealityMoney()-(totalPriceNew.doubleValue()-shopUser.getActivityCost()));
                        giftCardUserService.update(giftCardUser);
                    }
                    shopUser.setActivityCost(0.0);
                    shopUserService.update(shopUser);
                    return Result.of(Status.ClientError.GONE, "订单完成");
                } else {
                    if (discount.compareTo(BigDecimal.ZERO) > 0) {
                        //产生积分和佣金
                        sendIntegral(key);
                        payByCardAndDiscount(fygoodsCartVo, shopUser, totalPriceNew, discount);
                    } else {
                        //产生积分和佣金
                        sendIntegral(key);
                        payByCard(fygoodsCartVo, shopUser, totalPriceNew);
                        giftCardUser.setRealityMoney(giftCardUser.getRealityMoney()-totalPriceNew.doubleValue());
                        giftCardUserService.update(giftCardUser);
                    }
                    return Result.of(Status.ClientError.GONE, "订单完成");
                }

            }
        }

        int flg = 3;
        List<FyOrderItem> fyOrderItemList = new ArrayList<>();

        if (discount.compareTo(BigDecimal.ZERO) > 0) {
            fygoodsCartVo = avgDiscount(fygoodsCartVo, discount);
        }
        if (fygoodsCartVo.getCostType() == 1) {
            if (shopUser.getActivityCost()>0) {
                fygoodsCartVo = avgCost(fygoodsCartVo, new BigDecimal(shopUser.getActivityCost()));
                shopUser.setActivityCost(0.0);
                shopUserService.update(shopUser);
            }
        }
        if (StringUtils.isNotEmpty(fygoodsCartVo.getGiftCardUser())) {
            GiftCardUser giftCardUser = giftCardUserService.get(fygoodsCartVo.getGiftCardUser());
            if(giftCardUser!=null&&giftCardUser.getRealityMoney()>0) {
                fygoodsCartVo = avgGiftCard(fygoodsCartVo, new BigDecimal(giftCardUser.getRealityMoney()));
            }
        }
        BigDecimal subtract = totalPriceNew.subtract(hasDiscount);
        for (FygoodsCart fygoodsCart : fygoodsCartVo.getList()) {
            fygoodsCart.setOrderNum(key);
            FyOrderItem orderDetailNew = createOrderDetailNew(fygoodsCart, key, flg, shopUser);
            fyOrderItemList.add(orderDetailNew);
        }
        FyOrder order = createOrder(fygoodsCartVo, key, shopUser, totalPriceNew, new BigDecimal(0), fyOrderItemList, flg, subtract);
        return Result.ok(order);
    }

    private Boolean checkState(FygoodsCartVo fygoodsCartVo) {
        if (SHARE_TYPE.equals(fygoodsCartVo.getType())){
            ShareGoods shareGoods = shareGoodsService.get(fygoodsCartVo.getList().get(0).getFyGoodsId());
            Goods goods = goodsService.get(shareGoods.getGoodsId());
            GoodsBase goodsBase = goodsBaseService.get(goods.getGoodsId());
            if (goodsBase.getState()!=1||goodsBase.getIsdown()==2){
                return true;
            }
        }
        if (ZHENGCHANG_TYPE.equals(fygoodsCartVo.getType())||ZHIBO_TYPE.equals(fygoodsCartVo.getType())){
            Goods goods = goodsService.get(fygoodsCartVo.getList().get(0).getFyGoodsId());
            GoodsBase goodsBase = goodsBaseService.get(goods.getGoodsId());
            if (goodsBase.getState()!=1||goodsBase.getIsdown()==2){
                return true;
            }
        }
        if (PINTUAN_TYPE.equals(fygoodsCartVo.getType())){
            GrouponGoods grouponGoods= grouponGoodsService.get(fygoodsCartVo.getList().get(0).getFyGoodsId());
            Goods goods = goodsService.get(grouponGoods.getGoodsId());
            GoodsBase goodsBase = goodsBaseService.get(goods.getGoodsId());
            if (goodsBase.getState()!=1||goodsBase.getIsdown()==2){
                return true;
            }
        }
        if (SKILL_TYPE.equals(fygoodsCartVo.getType())){
            Fygoods fygoods= fygoodsService.get(fygoodsCartVo.getList().get(0).getFyGoodsId());
            Goods goods = goodsService.get(fygoods.getGoodsId());
            GoodsBase goodsBase = goodsBaseService.get(goods.getGoodsId());
            if (goodsBase.getState()!=1||goodsBase.getIsdown()==2){
                return true;
            }
        }
        if (BARGAIN_TYPE.equals(fygoodsCartVo.getType())){
            BargainGoods bargainGoods= bargainGoodsService.get(fygoodsCartVo.getList().get(0).getFyGoodsId());
            Goods goods = goodsService.get(bargainGoods.getGoodsId());
            GoodsBase goodsBase = goodsBaseService.get(goods.getGoodsId());
            if (goodsBase.getState()!=1||goodsBase.getIsdown()==2){
                return true;
            }
        }
        return false;
    }

    private Boolean checkTime(FygoodsCartVo fygoodsCartVo) {
        if (SHARE_TYPE.equals(fygoodsCartVo.getType())){
            ShareGoods shareGoods = shareGoodsService.get(fygoodsCartVo.getList().get(0).getFyGoodsId());
            if (shareGoods!=null){
                return shareGoods.getStartTime().after(new Date()) || shareGoods.getEndTime().before(new Date());
            }
        }
        if (PINTUAN_TYPE.equals(fygoodsCartVo.getType())){
            GrouponGoods grouponGoods= grouponGoodsService.get(fygoodsCartVo.getList().get(0).getFyGoodsId());
            if (grouponGoods!=null){
                return grouponGoods.getStartTime().after(new Date()) || grouponGoods.getEndTime().before(new Date());
            }
        }
        if (SKILL_TYPE.equals(fygoodsCartVo.getType())){
            Fygoods fygoods= fygoodsService.get(fygoodsCartVo.getList().get(0).getFyGoodsId());
            if (fygoods!=null){
                return fygoods.getKillStartTime().after(new Date()) || fygoods.getKillEndTime().before(new Date());
            }
        }
        if (BARGAIN_TYPE.equals(fygoodsCartVo.getType())){
            BargainGoods bargainGoods= bargainGoodsService.get(fygoodsCartVo.getList().get(0).getFyGoodsId());
            if (bargainGoods!=null){
                return bargainGoods.getStartTime().after(new Date()) || bargainGoods.getEndTime().before(new Date());
            }
        }
        return true;
    }

    /**
     * @author tzj
     * @description 特使商品只能同时一旦
     * @date 2020-05-22 15:02
     */
    private Boolean checkOnly(ShopUser shopUser, FygoodsCartVo fygoodsCartVo) {
        FyOrderItem fyOrderItem = new FyOrderItem();
        fyOrderItem.setUserId(shopUser.getId());
        fyOrderItem.setGoodsId(fygoodsCartVo.getList().get(0).getFyGoodsId());
        fyOrderItem.setIsdelete(1);
        fyOrderItem = fyOrderItemService.getOne(fyOrderItem);
        if (fyOrderItem != null) {
            FyOrder fyOrder = new FyOrder();
            fyOrder.setOrderNum(fyOrderItem.getOrderNum());
            fyOrder = fyOrderService.getOne(fyOrder);
            return fyOrder.getState() == 1;
        } else {
            return false;
        }
    }

    private void payByCard(FygoodsCartVo fygoodsCartVo, ShopUser shopUser, BigDecimal totalPriceNew) {
        fygoodsCartVo = avgGiftCard(fygoodsCartVo, totalPriceNew);
        String key = KeyUtils.getKey();
        Integer flg = 1;
        List<FyOrderItem> fyOrderItemList = new ArrayList<>();
        for (FygoodsCart fygoodsCart : fygoodsCartVo.getList()) {
            fygoodsCart.setOrderNum(key);
            FyOrderItem orderDetailNew = createOrderDetailNew(fygoodsCart, key, flg, shopUser);
            fyOrderItemList.add(orderDetailNew);
        }
        createOrder(fygoodsCartVo, key, shopUser, totalPriceNew, new BigDecimal(0), fyOrderItemList, flg, new BigDecimal(0));

    }

    private void payByCardAndDiscount(FygoodsCartVo fygoodsCartVo, ShopUser shopUser, BigDecimal totalPriceNew, BigDecimal discount) {
        BigDecimal subtract = totalPriceNew.subtract(discount);
        fygoodsCartVo = avgGiftCard(fygoodsCartVo, subtract);
        String key = KeyUtils.getKey();
        Integer flg = 1;
        List<FyOrderItem> fyOrderItemList = new ArrayList<>();
        for (FygoodsCart fygoodsCart : fygoodsCartVo.getList()) {
            fygoodsCart.setOrderNum(key);
            FyOrderItem orderDetailNew = createOrderDetailNew(fygoodsCart, key, flg, shopUser);
            fyOrderItemList.add(orderDetailNew);
        }
        createOrder(fygoodsCartVo, key, shopUser, totalPriceNew, new BigDecimal(0), fyOrderItemList, flg, new BigDecimal(0));
    }

    private void payByCostAndCardAndDiscount(FygoodsCartVo fygoodsCartVo, ShopUser shopUser, BigDecimal totalPriceNew, BigDecimal discount) {
        String key = KeyUtils.getKey();
        fygoodsCartVo = avgCost(fygoodsCartVo, new BigDecimal(shopUser.getActivityCost()));
        fygoodsCartVo = avgDiscount(fygoodsCartVo, discount);
        BigDecimal subtract = totalPriceNew.subtract(new BigDecimal(shopUser.getActivityCost())).subtract(discount);
        fygoodsCartVo = avgGiftCard(fygoodsCartVo, subtract);
        Integer flg = 1;
        List<FyOrderItem> fyOrderItemList = new ArrayList<>();
        for (FygoodsCart fygoodsCart : fygoodsCartVo.getList()) {
            fygoodsCart.setOrderNum(key);
            FyOrderItem orderDetailNew = createOrderDetailNew(fygoodsCart, key, flg, shopUser);
            fyOrderItemList.add(orderDetailNew);
        }
        createOrder(fygoodsCartVo, key, shopUser, totalPriceNew, new BigDecimal(0), fyOrderItemList, flg, new BigDecimal(0));
    }

    private FygoodsCartVo avgGiftCard(FygoodsCartVo fygoodsCartVo, BigDecimal subtract) {
        List<FygoodsCart> list = fygoodsCartVo.getList();
        BigDecimal totalCost = new BigDecimal(0);
        MathContext mc = new MathContext(2, RoundingMode.HALF_DOWN);
        BigDecimal avg = subtract.divide(new BigDecimal(list.size()), mc);
        if (subtract.intValue() % fygoodsCartVo.getList().size() == 0) {
            for (FygoodsCart fygoodsCart : fygoodsCartVo.getList()) {
                fygoodsCart.setGiftPrice(avg);
            }
        } else {
            for (int i = 0; i < list.size(); i++) {
                if (i == list.size() - 1) {
                    list.get(i).setGiftPrice(subtract.subtract(totalCost));
                } else {
                    list.get(i).setGiftPrice(avg);
                    totalCost = totalCost.add(avg);
                }
            }
        }
        return fygoodsCartVo;
    }

    /**
     * @author tzj
     * @description 佣金加优惠券支付
     * @date 2020-05-20 13:35
     */
    private void payByCostAndDiscount(FygoodsCartVo fygoodsCartVo, ShopUser shopUser, BigDecimal totalPriceNew, BigDecimal discount, String key) {
        fygoodsCartVo = avgCost(fygoodsCartVo, new BigDecimal(shopUser.getActivityCost()));
        fygoodsCartVo = avgDiscount(fygoodsCartVo, discount);
        Integer flg = 1;
        List<FyOrderItem> fyOrderItemList = new ArrayList<>();
        for (FygoodsCart fygoodsCart : fygoodsCartVo.getList()) {
            FyOrderItem orderDetailNew = createOrderDetailNew(fygoodsCart, key, flg, shopUser);
            fygoodsCart.setOrderNum(key);
            fyOrderItemList.add(orderDetailNew);
        }
        createOrder(fygoodsCartVo, key, shopUser, totalPriceNew, new BigDecimal(0), fyOrderItemList, flg, new BigDecimal(0));
    }

    /**
     * @author tzj
     * @description 平摊优惠券
     * @date 2020-05-20 13:21
     */
    private FygoodsCartVo avgDiscount(FygoodsCartVo fygoodsCartV, BigDecimal discounto) {
        List<FygoodsCart> list = fygoodsCartV.getList();
        BigDecimal totalCost = new BigDecimal(0);
        MathContext mc = new MathContext(2, RoundingMode.HALF_DOWN);
        BigDecimal avg = discounto.divide(new BigDecimal(list.size()), mc);
        if (discounto.intValue() % fygoodsCartV.getList().size() == 0) {
            for (FygoodsCart fygoodsCart : fygoodsCartV.getList()) {
                fygoodsCart.setDiscount(avg);
            }
        } else {
            for (int i = 0; i < list.size(); i++) {
                if (i == list.size() - 1) {
                    list.get(i).setDiscount(discounto.subtract(totalCost));
                } else {
                    list.get(i).setDiscount(avg);
                    totalCost = totalCost.add(avg);
                }
            }
        }
        return fygoodsCartV;
    }


    /**
     * @author tzj
     * @description 平摊佣金
     * @date 2020-05-20 13:21
     */
    private FygoodsCartVo avgCost(FygoodsCartVo fygoodsCartVo, BigDecimal totalPriceNew) {
        MathContext mc = new MathContext(2, RoundingMode.HALF_DOWN);
        List<FygoodsCart> list = fygoodsCartVo.getList();
        BigDecimal avg = totalPriceNew.divide(new BigDecimal(list.size()), mc);
        BigDecimal totalCost = new BigDecimal(0);
        //商品平摊佣金
        if (totalPriceNew.intValue() % list.size() == 0) {
            for (FygoodsCart fygoodsCart : list) {
                fygoodsCart.setActivityCost(avg);
            }
        } else {
            for (int i = 0; i < list.size(); i++) {
                if (i == list.size() - 1) {
                    list.get(i).setActivityCost(totalPriceNew.subtract(totalCost));
                } else {
                    totalCost = totalCost.add(avg);
                    list.get(i).setActivityCost(avg);
                }
            }
        }
        return fygoodsCartVo;
    }


    /**
     * @author tzj
     * @description 佣金礼品卡支付
     * @date 2020-05-20 11:46
     */
    private void payByCostAndCard(FygoodsCartVo fygoodsCartVo, ShopUser shopUser, BigDecimal totalPriceNew) {
        Double activityCost = shopUser.getActivityCost();
        GiftCardUser giftCardUser = giftCardUserService.get(fygoodsCartVo.getGiftCardUser());

        MathContext mc = new MathContext(2, RoundingMode.HALF_DOWN);
        BigDecimal cost = new BigDecimal(activityCost);
        //礼品需要扣除的金额
        BigDecimal giftPrice = totalPriceNew.subtract(cost);
        giftCardUser.setRealityMoney(giftCardUser.getRealityMoney() - giftPrice.doubleValue());
        giftCardUserService.update(giftCardUser);
        //减少佣金
        shopUser.setActivityCost(0.0);
        shopUserService.update(shopUser);

        List<FygoodsCart> list = fygoodsCartVo.getList();
        BigDecimal avg = cost.divide(new BigDecimal(list.size()), mc);
        BigDecimal totalCost = new BigDecimal(0);
        BigDecimal totalGift = new BigDecimal(0);
        //商品平摊佣金
        if (cost.intValue() % list.size() == 0) {
            for (FygoodsCart fygoodsCart : list) {
                fygoodsCart.setActivityCost(avg);
            }
        } else {
            for (int i = 0; i < list.size(); i++) {
                if (i == list.size() - 1) {
                    list.get(i).setActivityCost(cost.subtract(totalCost));
                } else {
                    totalCost = totalCost.add(avg);
                    list.get(i).setActivityCost(avg);
                }
            }
        }

        //商品平摊礼品卡
        if (giftPrice.intValue() % list.size() == 0) {
            for (FygoodsCart fygoodsCart : list) {
                fygoodsCart.setGiftPrice(avg);
            }
        } else {
            for (int i = 0; i < list.size(); i++) {
                if (i == list.size() - 1) {
                    list.get(i).setGiftPrice(giftPrice.subtract(totalGift));
                } else {
                    totalGift = totalGift.add(avg);
                    list.get(i).setGiftPrice(avg);
                }
            }
        }
        //两种方式付款
        Integer flg = 2;
        List<FyOrderItem> fyOrderItemList = new ArrayList<>();
        String key = KeyUtils.getKey();
//        createOrderNew();
        for (FygoodsCart fygoodsCart : list) {
            FyOrderItem orderDetailNew = createOrderDetailNew(fygoodsCart, key, flg, shopUser);
            fygoodsCart.setOrderNum(key);
            fyOrderItemList.add(orderDetailNew);
        }
        createOrder(fygoodsCartVo, key, shopUser, totalPriceNew, new BigDecimal(0), fyOrderItemList, flg, new BigDecimal(0));
    }

    /**
     * @author tzj
     * @description 校验用户佣金是否足够支付
     * @date 2020-05-20 10:42
     */
    private int payByActivityCost(ShopUser shopUser, BigDecimal totalPriceNew, FygoodsCartVo fygoodsCartVo, String key) {
        shopUser= shopUserService.get(shopUser.getId());
        Double activityCost = shopUser.getActivityCost();
        if (totalPriceNew.compareTo(new BigDecimal(activityCost)) <= 0) {
            createAllByCost(fygoodsCartVo, shopUser, totalPriceNew, key);
            return 1;
        } else {
            return -1;
        }
    }

    /**
     * @author tzj
     * @description 佣金支付全部
     * @date 2020-05-20 10:52
     */
    private void createAllByCost(FygoodsCartVo fygoodsCartVo, ShopUser shopUser, BigDecimal totalPriceNew, String key) {
        Integer flg = 1;
        List<FyOrderItem> fyOrderItemList = new ArrayList<>();
        fygoodsCartVo = avgCost(fygoodsCartVo, totalPriceNew);
        for (FygoodsCart fygoodsCart : fygoodsCartVo.getList()) {
            FyOrderItem orderDetailNew = createOrderDetailNew(fygoodsCart, key, flg, shopUser);
            fygoodsCart.setOrderNum(key);
            fyOrderItemList.add(orderDetailNew);
        }
        createOrder(fygoodsCartVo, key, shopUser, totalPriceNew, new BigDecimal(0), fyOrderItemList, flg, new BigDecimal(0));


    }


    /**
     * @author tzj
     * @description 创建订单
     * @date 2020-05-20 13:59
     */
    private FyOrder createOrder(FygoodsCartVo fygoodsCartVo, String key, ShopUser shopUser, BigDecimal totalPrice, BigDecimal discount, List<FyOrderItem> fyOrderItemList, Integer flg, BigDecimal lowPrice) {
        //查询数据库，获取对应商品的信息
        FyOrder order = new FyOrder();
        //默认未发货
        order.setDeliver(1);
        order.setGiftCardId(fygoodsCartVo.getGiftCardUser());
        order.setShareId(fygoodsCartVo.getShareId());
        order.setFyCode(fygoodsCartVo.getFyCode());
        order.setId(KeyUtils.getKey());
        order.setActivityCost(fygoodsCartVo.getActivityCost());
        order.setOrderNum(key);
        order.setOrderTime(new Date());
        order.setUserName(shopUser.getNickname());
        order.setUserId(shopUser.getId());
        order.setPhoneNum(shopUser.getPhone());
        order.setAllPrice(totalPrice);
        order.setDiscount(discount);
        StringBuilder goodsBody = new StringBuilder();
        for (FyOrderItem fyOrderItem : fyOrderItemList) {
            goodsBody.append(fyOrderItem.getGoodsName()).append("-");
        }
        order.setGoodsBody(goodsBody.toString());
        //1表示为删除的状态
        order.setIsdelete(1);
        if (flg == 1) {
            //1表示未支付的状态
            order.setState(2);
            order.setPayPrice(new BigDecimal(0));
            order.setPayType(2);
        } else if (flg==2){
            order.setState(2);
            order.setPayPrice(new BigDecimal(0));
            order.setPayType(2);
        }else {
            order.setState(1);
            order.setPayPrice(lowPrice);
            order.setPayType(4);
        }
        //是否申请退款 1 表示没有  2 表示有
        order.setIsrefound(1);
        //退款金额默认为0
        order.setRefoundPrice(new BigDecimal(0));
        //默认不包含退款订单
        order.setIshasRefound(2);
        //认领时间
        order.setClaimTime(fygoodsCartVo.getClaimTime());
        //订单信息存入数据库
        fyOrderService.save(order);

        // sendOrderMessage(order.getOrderNum());//测试用的
        //订单信息存一份到redis，支付页面获取数据直接从redis中拿，减轻数据库的压力   订单基本信息+订单详情
        FyOrderResultVo fyOrderResultVo = new FyOrderResultVo();
        fyOrderResultVo.setFyOrder(order);
        fyOrderResultVo.setFyOrderItemList(fyOrderItemList);
        redisTemplate.opsForValue().set("order:" + order.getOrderNum(), JsonUtil.object2JsonStr(fyOrderResultVo));
        redisTemplate.expire("order:" + order.getOrderNum(), 120, TimeUnit.SECONDS);
        //发送信息至延迟 消息队列，主要用来做订单延时处理
        sendDelayMessage(fygoodsCartVo.getList());
        //返回预订单信息
        return order;
    }

    /**
     * @author tzj
     * @description 佣金支付全部
     * @date 2020-05-20 10:57
     */
    private FyOrderItem createOrderDetailNew(FygoodsCart fygoodsCart, String key, Integer flg, ShopUser shopUser) {
        FyOrderItem fyOrderItem = new FyOrderItem();
        //如果是家电下单
        GoodsDealer goodsDealer = new GoodsDealer();
        if (!"".equals(fygoodsCart.getDealerId()) && fygoodsCart.getDealerId() != null) {
            goodsDealer = goodsDealerService.get(fygoodsCart.getDealerId());
        }
        //存储家电信息
        if (goodsDealer != null) {
            fyOrderItem.setDealerId(fygoodsCart.getDealerId());
            fyOrderItem.setJiadian(2);
            fyOrderItem.setDealerName(goodsDealer.getName());
        }
        //封装实体类
        fyOrderItem.setId(KeyUtils.getKey());
        fyOrderItem.setCreateTime(new Date());
        fyOrderItem.setUserId(shopUser.getId());
        fyOrderItem.setDiscount(fygoodsCart.getDiscount());
        fyOrderItem.setActivityCost(fygoodsCart.getActivityCost());
        fyOrderItem.setGiftPrice(fygoodsCart.getGiftPrice());
        fyOrderItem.setOrderNum(key);
        fyOrderItem.setSendType(1);
        fyOrderItem.setCount(fygoodsCart.getCount());
        fyOrderItem.setType(fygoodsCart.getType());
        //1表示未删除
        fyOrderItem.setIsdelete(1);
        //是否申请退款  1 表示否
        fyOrderItem.setIsrefound(1);
        //是否退款成功  1 表示否
        fyOrderItem.setRefoundState(1);
        Integer type = fygoodsCart.getType();

        if (type.equals(ZHENGCHANG_TYPE) || type.equals(ZHIBO_TYPE)) {
            fyOrderItem = createDetailByNormal(fyOrderItem, fygoodsCart, key, flg);
        } else if (type.equals(SKILL_TYPE)) {
            fyOrderItem = createDetailBySecKill(fyOrderItem, fygoodsCart, key, flg);
        } else if (type.equals(PINTUAN_TYPE)) {
            fyOrderItem = createDetailByPinTuan(fyOrderItem, fygoodsCart, key, flg);
        } else if (type.equals(SHARE_TYPE)) {
            fyOrderItem = createDetailByShare(fyOrderItem, fygoodsCart, key, flg);
        } else if (type.equals(BARGAIN_TYPE)) {
            fyOrderItem = createDetailByBargain(fyOrderItem, fygoodsCart, key, flg);
        }
        return fyOrderItem;
    }

    private FyOrderItem createDetailByBargain(FyOrderItem fyOrderItem, FygoodsCart fygoodsCart, String key, Integer flg) {
        BargainGoods goods = bargainGoodsService.get(fygoodsCart.getFyGoodsId());
        fyOrderItem.setGoodsName(goods.getGoodsName());
        fyOrderItem.setGoodsId(goods.getId());
        fyOrderItem.setGoodsCover(goods.getGoodCover());
        if (flg == 2 || flg == 1) {
            fyOrderItem.setPayPrice(new BigDecimal(0));
        } else {
            BigDecimal multiply = goods.getPrice().multiply(new BigDecimal(fyOrderItem.getCount()));
            if (fyOrderItem.getDiscount() != null) {
                multiply = multiply.subtract(fyOrderItem.getDiscount());
            }
            if (fyOrderItem.getActivityCost() != null) {
                multiply = multiply.subtract(fyOrderItem.getActivityCost());
            }
            if (fyOrderItem.getGiftPrice() != null) {
                multiply = multiply.subtract(fyOrderItem.getGiftPrice());
            }
            fyOrderItem.setPayPrice(multiply);
        }
        fyOrderItem.setSinglePrice(goods.getPrice());
        goods.setStoreCount(goods.getStoreCount() -fygoodsCart.getCount());
        //更新操作
        bargainGoodsService.update(goods);
        fyOrderItemService.save(fyOrderItem);
        return fyOrderItem;

    }

    private FyOrderItem createDetailByShare(FyOrderItem fyOrderItem, FygoodsCart fygoodsCart, String key, Integer flg) {
        ShareGoods goods = shareGoodsService.get(fygoodsCart.getFyGoodsId());
        fyOrderItem.setGoodsName(goods.getGoodsName());
        fyOrderItem.setGoodsId(goods.getId());
        fyOrderItem.setGoodsCover(goods.getGoodCover());
        if (flg == 2 || flg == 1) {
            fyOrderItem.setPayPrice(new BigDecimal(0));
        } else {
            BigDecimal multiply = goods.getPrice().multiply(new BigDecimal(fyOrderItem.getCount()));
            if (fyOrderItem.getDiscount() != null) {
                multiply = multiply.subtract(fyOrderItem.getDiscount());
            }
            if (fyOrderItem.getActivityCost() != null) {
                multiply = multiply.subtract(fyOrderItem.getActivityCost());
            }
            if (fyOrderItem.getGiftPrice() != null) {
                multiply = multiply.subtract(fyOrderItem.getGiftPrice());
            }
            fyOrderItem.setPayPrice(multiply);
        }
        fyOrderItem.setSinglePrice(goods.getPrice());
        goods.setStoreCount(goods.getStoreCount() -fygoodsCart.getCount());
        //更新操作
        shareGoodsService.update(goods);
        fyOrderItemService.save(fyOrderItem);
        return fyOrderItem;
    }

    private FyOrderItem createDetailByPinTuan(FyOrderItem fyOrderItem, FygoodsCart fygoodsCart, String key, Integer flg) {
        GrouponGoods goods = grouponGoodsService.get(fygoodsCart.getFyGoodsId());
        fyOrderItem.setGoodsName(goods.getGoodsName());
        fyOrderItem.setGoodsId(goods.getId());
        fyOrderItem.setGoodsCover(goods.getGoodCover());
        if (flg == 2 || flg == 1) {
            fyOrderItem.setPayPrice(new BigDecimal(0));
        } else {
            BigDecimal multiply = goods.getGroupPrice().multiply(new BigDecimal(fyOrderItem.getCount()));
            if (fyOrderItem.getDiscount() != null) {
                multiply = multiply.subtract(fyOrderItem.getDiscount());
            }
            if (fyOrderItem.getActivityCost() != null) {
                multiply = multiply.subtract(fyOrderItem.getActivityCost());
            }
            if (fyOrderItem.getGiftPrice() != null) {
                multiply = multiply.subtract(fyOrderItem.getGiftPrice());
            }
            fyOrderItem.setPayPrice(multiply);
        }
        fyOrderItem.setSinglePrice(goods.getGroupPrice());
        goods.setStoreCount(goods.getStoreCount()-fygoodsCart.getCount());
        //更新操作
        grouponGoodsService.update(goods);
        fyOrderItemService.save(fyOrderItem);

        return fyOrderItem;
    }

    /**
     * @author tzj
     * @description 秒杀商品下单
     * @date 2020-05-20 11:15
     */
    private FyOrderItem createDetailBySecKill(FyOrderItem fyOrderItem, FygoodsCart fygoodsCart, String key, Integer flg) {
        Fygoods goods = fygoodsService.get(fygoodsCart.getFyGoodsId());
        fyOrderItem.setGoodsName(goods.getGoodsName());
        fyOrderItem.setGoodsId(goods.getId());
        fyOrderItem.setGoodsCover(goods.getGoodCover());
        if (flg == 2 || flg == 1) {
            fyOrderItem.setPayPrice(new BigDecimal(0));
        } else {
            BigDecimal multiply = goods.getKillPrice().multiply(new BigDecimal(fyOrderItem.getCount()));
            if (fyOrderItem.getDiscount() != null) {
                multiply = multiply.subtract(fyOrderItem.getDiscount());
            }
            if (fyOrderItem.getActivityCost() != null) {
                multiply = multiply.subtract(fyOrderItem.getActivityCost());
            }
            if (fyOrderItem.getGiftPrice() != null) {
                multiply = multiply.subtract(fyOrderItem.getGiftPrice());
            }
            fyOrderItem.setPayPrice(multiply);
        }
        fyOrderItem.setSinglePrice(goods.getKillPrice());
        //减库存
        goods.setStoreCount(goods.getStoreCount() - fygoodsCart.getCount());
        //更新操作
        fygoodsService.update(goods);
        fyOrderItemService.save(fyOrderItem);
        return fyOrderItem;
    }

    /**
     * @author tzj
     * @description 佣金支付全部
     * @date 2020-05-20 11:04
     */
    private FyOrderItem createDetailByNormal(FyOrderItem fyOrderItem, FygoodsCart fygoodsCart, String key, Integer flg) {
        Goods goods = goodsService.get(fygoodsCart.getFyGoodsId());
        fyOrderItem.setGoodsName(goods.getName());
        fyOrderItem.setGoodsId(goods.getId());
        fyOrderItem.setGoodsCover(goods.getGoodCover());
        fyOrderItem.setSinglePrice(goods.getShopPrice());
        if (flg == 2 || flg == 1) {
            fyOrderItem.setPayPrice(new BigDecimal(0));
        } else {
            BigDecimal multiply = goods.getShopPrice().multiply(new BigDecimal(fyOrderItem.getCount()));
            if (fyOrderItem.getDiscount() != null) {
                multiply = multiply.subtract(fyOrderItem.getDiscount());
            }
            if (fyOrderItem.getActivityCost() != null) {
                multiply = multiply.subtract(fyOrderItem.getActivityCost());
            }
            if (fyOrderItem.getGiftPrice() != null) {
                multiply = multiply.subtract(fyOrderItem.getGiftPrice());
            }
            fyOrderItem.setPayPrice(multiply);
        }
        //减库存
        goods.setStoreCount(goods.getStoreCount() - fygoodsCart.getCount());
        goods.setSaleCount(goods.getSaleCount()+fygoodsCart.getCount());

        //更新操作
        goodsService.update(goods);
        fyOrderItemService.save(fyOrderItem);
        return fyOrderItem;
    }

    /**
     * @author tzj
     * @description 计算总价
     * @date 2020-05-20 10:44
     */
    private BigDecimal getTotalPriceNew(FygoodsCartVo fygoodsCartVo, Integer type) {
        if (type.equals(ZHENGCHANG_TYPE) || type.equals(ZHIBO_TYPE)) {
            return getTotalPriceNormal(fygoodsCartVo);
        } else if (type.equals(SKILL_TYPE)) {
            return getTotalPriceSecKill(fygoodsCartVo);
        } else if (type.equals(PINTUAN_TYPE)) {
            return getTotalPricePingTuan(fygoodsCartVo);
        } else if (type.equals(SHARE_TYPE)) {
            return getTotalPriceShare(fygoodsCartVo);
        } else if (type.equals(BARGAIN_TYPE)) {
            return getTotalPriceBargain(fygoodsCartVo);
        } else {
            return new BigDecimal(0);
        }
    }

    private BigDecimal getTotalPriceBargain(FygoodsCartVo fygoodsCartVo) {
        BigDecimal totalPrice = new BigDecimal(0);
        for (FygoodsCart fygoodsCart : fygoodsCartVo.getList()) {
            BargainGoods bargainGoods = bargainGoodsService.get(fygoodsCart.getFyGoodsId());
            if (bargainGoods == null || bargainGoods.getState() == 2) {
                return null;
            } else {
                totalPrice = bargainGoods.getPrice().multiply(new BigDecimal(fygoodsCart.getCount())).add(totalPrice);
            }
        }
        return totalPrice;
    }

    /**
     * @author tzj
     * @description 分享赚总价
     * @date 2020-05-20 10:37
     */
    private BigDecimal getTotalPriceShare(FygoodsCartVo fygoodsCartVo) {
        BigDecimal totalPrice = new BigDecimal(0);
        for (FygoodsCart fygoodsCart : fygoodsCartVo.getList()) {
            ShareGoods shareGoods = shareGoodsService.get(fygoodsCart.getFyGoodsId());
            if (shareGoods == null || shareGoods.getState() == 2) {
                return null;
            } else {
                totalPrice = shareGoods.getPrice().multiply(new BigDecimal(fygoodsCart.getCount())).add(totalPrice);
            }
        }
        return totalPrice;
    }

    /**
     * @author tzj
     * @description 拼团
     * @date 2020-05-20 10:35
     */
    private BigDecimal getTotalPricePingTuan(FygoodsCartVo fygoodsCartVo) {
        BigDecimal totalPrice = new BigDecimal(0);
        for (FygoodsCart fygoodsCart : fygoodsCartVo.getList()) {
            GrouponGoods grouponGoods = grouponGoodsService.get(fygoodsCart.getFyGoodsId());
            if (grouponGoods == null || grouponGoods.getState() == 2) {
                return null;
            } else {
                totalPrice = grouponGoods.getGroupPrice().multiply(new BigDecimal(fygoodsCart.getCount())).add(totalPrice);
            }
        }
        return totalPrice;
    }

    /**
     * @author tzj
     * @description 秒杀商品总价
     * @date 2020-05-20 10:33
     */
    private BigDecimal getTotalPriceSecKill(FygoodsCartVo fygoodsCartVo) {
        BigDecimal totalPrice = new BigDecimal(0);
        for (FygoodsCart fygoodsCart : fygoodsCartVo.getList()) {
            //秒杀
            Fygoods fygoods = fygoodsService.get(fygoodsCart.getFyGoodsId());
            if (fygoods == null || fygoods.getState() == 2) {
                return null;
            } else {
                totalPrice = fygoods.getKillPrice().multiply(new BigDecimal(fygoodsCart.getCount())).add(totalPrice);
            }
        }
        return totalPrice;
    }

    /**
     * @author tzj
     * @description 正常商品总价
     * @date 2020-05-20 10:29
     */
    private BigDecimal getTotalPriceNormal(FygoodsCartVo fygoodsCartVo) {
        BigDecimal totalPrice = new BigDecimal(0);
        for (FygoodsCart fygoodsCart : fygoodsCartVo.getList()) {
            Goods goods = goodsService.get(fygoodsCart.getFyGoodsId());
            if (goods == null || goods.getState() == 2) {
                return null;
            } else {
                totalPrice = totalPrice.add(goods.getShopPrice().multiply(new BigDecimal(fygoodsCart.getCount())));
            }
        }
        return totalPrice;
    }

    @RequestMapping(value = "/wxNotifyTest")
    @PermissionMode(PermissionMode.Mode.White)
    public synchronized void wxNotifyTest(@RequestBody  Map<String, Object> map )  {
       String id= (String) map.get("id");
                    //改变订单状态并且产生积分
                    sendIntegral(id);
                    //将订单号发送至消息队列
                    sendOrderMessage(id);

    }
    @RequestMapping(value = "/delOne")
    public Result<?> delOne(@RequestBody  Map<String, Object> map )  {
        String orderNum= (String) map.get("orderNum");
        //改变订单状态并且产生积分
        FyOrder fyOrder=new FyOrder();
        fyOrder.setOrderNum(orderNum);
        fyOrder=fyOrderService.getOne(fyOrder);
        if (fyOrder.getState()==2&&fyOrder.getDeliver()==1){
            return  Result.of(Status.ClientError.BAD_REQUEST,"未发货订单不能删除");
        }
        if (fyOrder.getId()!=null){
            fyOrder.setIsdelete(2);
            fyOrderService.update(fyOrder);
        }
        List<FyOrderItem> list = fyOrderItemService.list(map);
        if (list.size()>0){
            for (FyOrderItem fyOrderItem : list) {
                fyOrderItem.setIsdelete(2);
                fyOrderItemService.update(fyOrderItem);
            }
        }
        return Result.ok();
    }

}
