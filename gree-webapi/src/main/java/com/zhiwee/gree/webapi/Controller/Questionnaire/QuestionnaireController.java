package com.zhiwee.gree.webapi.Controller.Questionnaire;

import com.zhiwee.gree.model.Questionnaire.Questionnaire;
import com.zhiwee.gree.service.Questionnaire.QuestionnaireService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.web.annotation.EnableGetRequest;
import xyz.icrab.common.web.annotation.EnableListRequest;
import java.util.HashMap;
import java.util.Map;

@RequestMapping("/question")
@Controller
@ResponseBody
@EnableListRequest
@EnableGetRequest
public class QuestionnaireController {

    @Autowired
    private QuestionnaireService questionnaireService;

    /**
     * @return
     * @author tzj
     * @description 页面
     * @date 2019/12/8 19:32
     */
    @RequestMapping(value = "/infoPage")
    @SuppressWarnings("all")
    public Result<?> infoPage(@RequestBody Map<String, Object> params, Pagination pagination) {
        if (params == null) {
            params = new HashMap<>();
        }

        Pageable<Questionnaire> page = questionnaireService.pageInfo(params, pagination);
        return Result.ok();
    }



}
