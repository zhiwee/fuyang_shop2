package com.zhiwee.gree.webapi.QuartzUtil;

import com.zhiwee.gree.model.Quartz.QuartzInfo;
import com.zhiwee.gree.service.QuartzInfo.QuartzInfoService;
import org.quartz.Scheduler;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author sun on 2019/7/3
 */
@Component
public class QuartzInfoInit implements InitializingBean {
    @Autowired
    private QuartzInfoService quartzInfoService;



    @Autowired
    private SchedulerFactoryBean schedulerFactoryBean;

    private static String  time=  "0 z x * * ?";

    @Override
    public void afterPropertiesSet() throws Exception {
        List<QuartzInfo> quartzInfoList = quartzInfoService.list();

        Scheduler sche = schedulerFactoryBean.getScheduler();
        for (QuartzInfo quartzInfo : quartzInfoList) {
            if(quartzInfo.getState() == 1){
                Pattern p = Pattern.compile("x");
                Matcher m = p.matcher(time);
                String hour = m.replaceAll(quartzInfo.getHour().toString());

                p = Pattern.compile("z");
                m = p.matcher(hour);
                String min = m.replaceAll(quartzInfo.getMin().toString());

                QuartzManager.addJob(sche,quartzInfo.getJobName(),quartzInfo.getJobGroupName(),quartzInfo.getTriggerName(),
                        quartzInfo.getTriggerGroupName(), QuartzJob.class, min);
            }

        }


    }
}
