package com.zhiwee.gree.webapi.Controller.card;

import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.User;
import com.zhiwee.gree.model.card.LShopUserCards;
import com.zhiwee.gree.model.card.UserCardsInfo;
import com.zhiwee.gree.model.card.UserCardsInfoExrt;
import com.zhiwee.gree.service.ShopUser.ShopUserService;
import com.zhiwee.gree.service.card.UserCardsInfoService;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.jeecgframework.poi.excel.ExcelExportUtil;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.enmus.ExcelType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.web.annotation.EnableGetRequest;
import xyz.icrab.common.web.annotation.EnableListRequest;
import xyz.icrab.common.web.annotation.PermissionMode;
import xyz.icrab.common.web.annotation.Session;
import xyz.icrab.common.web.param.IdParam;
import xyz.icrab.ums.util.consts.SessionKeys;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.io.OutputStream;
import java.time.LocalDate;
import java.util.*;

@RequestMapping("/userCardsInfo")
@Controller
@ResponseBody
@EnableListRequest
@EnableGetRequest

/**
 * @author tzj
 * @description
 * @date 2019/12/14 0:31
 * @return
*/
public class UserCardsInfoController {

    @Autowired
    private UserCardsInfoService userCardsInfoService;
@Autowired
private ShopUserService shopUserService;

    /**
     * @author tzj
     * @description 珍藏卡页面
     * @date 2019/12/11 12:55
     * @return
    */
    @RequestMapping(value = "/infoPage")
    @SuppressWarnings("all")
    public Result<?> infoPage(@RequestBody Map<String, Object> params, Pagination pagination) {
        if (params == null) {
            params = new HashMap<>();
        }

//        if (StringUtils.isNotBlank((String) params.get("startCreateTime"))) {
//            params.put("startCreateTime", (String) params.get("startCreateTime") + " 00:00:00");
//        }
//        if (StringUtils.isNotBlank((String) params.get("endCreateTime"))) {
//            params.put("endCreateTime", (String) params.get("endCreateTime") + " 23:59:59");
//        }

        Pageable<UserCardsInfo> page = userCardsInfoService.pageInfoNoCategory(params, pagination);
        return Result.ok(page);
    }


    /**
     * Description: 导出
     * @author: chensong
     * @Date 下午4:36 2019/12/12
     * @param:
     * @return:
     */
    @RequestMapping("/export")
    public void export(HttpServletRequest req, HttpServletResponse rsp) throws IOException {
        Map<String, Object> params = new HashMap<>(10);
//        params.put("startCreateTime", req.getParameter("startCreateTime"));
//        params.put("endCreateTime", req.getParameter("endCreateTime"));
        params.put("nickName", req.getParameter("nickName"));
        params.put("phone", req.getParameter("phone"));

//        if (StringUtils.isNotBlank((String) params.get("startCreateTime"))) {
//            params.put("startCreateTime",  params.get("startCreateTime") + " 00:00:00");
//        }
//        if (StringUtils.isNotBlank((String) params.get("endCreateTime"))) {
//            params.put("endCreateTime",  params.get("endCreateTime") + " 23:59:59");
//        }

        List<UserCardsInfoExrt> result ;
        result = userCardsInfoService.queryUserCardsInfoNoCategoryExport(params);

        // 导出Excel
        OutputStream os = rsp.getOutputStream();

        rsp.setContentType("application/x-download");
        // 设置导出文件名称
        rsp.setHeader("Content-Disposition",
                "attachment;filename=" + DateFormatUtils.format(new Date(), "yyyyMMddHHmmssSSS") + ".xlsx");
        ExportParams param = new ExportParams("购卡客户名单", "购买珍藏卡顾客信息", "购卡客户信息表");
        param.setAddIndex(true);
        param.setType(ExcelType.XSSF);
        Workbook excel = ExcelExportUtil.exportExcel(param, UserCardsInfoExrt.class, result);
        excel.write(os);
        os.flush();
        os.close();
    }


    /**
     * Description: 小程序获取个人优惠券
     *
     * @author: sun
     * @Date 下午10:40 2019/3/27
     * @param:
     * @return:
     */
    @RequestMapping(value = "/getMyCoupons")
    public Result<?> getMyCoupons(@RequestBody(required = false) Map<String, Object> param
            ,@Session("ShopUser") ShopUser user) {
        Map<String, Object> params = new HashMap<>();
        params.put("shopUserId", user.getId());
        params.put("month", LocalDate.now().getMonthValue());
        params.put("onlyPayState", "1");
        List<UserCardsInfo> list = userCardsInfoService.queryUserCardsInfo(params);
        return Result.ok(list);
    }


        /**
         * @author tzj
         * @description 回退优惠券
         * @date 2020/1/2 16:37
        */
    @RequestMapping("/backCoupon")
    public Result<?> refreshUrl(@RequestBody @Valid IdParam param,@Session(SessionKeys.USER) User user) {
        //取到购买记录的ID
        UserCardsInfo userCardsInfo=new UserCardsInfo();
        userCardsInfo.setId(param.getId());
        userCardsInfo= userCardsInfoService.getOne(userCardsInfo);
        if(userCardsInfo==null){
            return Result.of(Status.ClientError.BAD_REQUEST,"未找到该优惠券记录");
        }
        userCardsInfo.setUpdatetime(new Date());
        userCardsInfo.setState(2);
        userCardsInfo.setUpdateman(user.getId());
        userCardsInfoService.update(userCardsInfo);
        return Result.ok();
    }




}
