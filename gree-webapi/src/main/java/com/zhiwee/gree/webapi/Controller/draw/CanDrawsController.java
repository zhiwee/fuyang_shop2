package com.zhiwee.gree.webapi.Controller.draw;


import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.draw.CanDraws;
import com.zhiwee.gree.service.ShopUser.ShopUserService;
import com.zhiwee.gree.service.draw.CanDrawsService;
import com.zhiwee.gree.webapi.util.SystemControllerLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.util.KeyUtils;
import xyz.icrab.common.web.annotation.EnableGetRequest;
import xyz.icrab.common.web.annotation.EnableListRequest;
import xyz.icrab.common.web.annotation.PermissionMode;
import xyz.icrab.common.web.annotation.Session;

import javax.annotation.Resource;
import java.util.Date;
import java.util.Map;

@RequestMapping("/canDraw")
@Controller
@ResponseBody
@EnableListRequest
@EnableGetRequest

/**
 * @author tzj
 * @description
 * @date 2019/12/14 0:34
 * @return
*/
public class CanDrawsController {
     @Resource
    private CanDrawsService canDrawsService;
    @Resource
    private ShopUserService shopUserService;


        /**
         * @author tzj
         * @description 增加抽奖次数
         * @date 2020/4/9 9:11
        */
        @RequestMapping("/addTime")
        @ResponseBody
        public Result<?> addTime(@Session("ShopUser") ShopUser shopUser,@RequestBody Map<String,Object> param) {
            shopUser=  shopUserService.get(shopUser.getId());
            if (shopUser.getPhone()==null||"".equals(shopUser.getPhone())){
                return Result.of(Status.ClientError.UNAUTHORIZED,"请完善手机号信息");
            }
            String key = KeyUtils.getKey();
            CanDraws canDraws=new CanDraws();
            canDraws.setShopUserId(shopUser.getId());
            canDraws=   canDrawsService.getOne(canDraws);
            if (canDraws==null) {
                CanDraws canDraws1=new CanDraws();
                canDraws1.setShopUserId(shopUser.getId());
                canDraws1.setId(key);
                canDraws1.setUpdateman(key);
                canDraws1.setDrawsSum(1);
                canDraws1.setCreatetime(new Date());
                canDrawsService.save(canDraws1);
                return Result.ok();
            }else {
                canDraws.setDrawsSum(1);
                canDrawsService.update(canDraws);
                return Result.ok();
            }
        }
}
