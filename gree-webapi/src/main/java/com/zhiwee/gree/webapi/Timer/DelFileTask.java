package com.zhiwee.gree.webapi.Timer;

/**
 * Description: 定时器任务处理
 * @author: sun
 * @Date 下午3:15 2019/6/6
 * @param:
 * @return:
 */

import com.zhiwee.gree.model.Coupon.Coupon;
import com.zhiwee.gree.service.Coupon.CouponService;

import javax.servlet.ServletContext;
import java.util.*;

public class DelFileTask extends TimerTask{

    private static boolean isRunning = false;

    private CouponService couponService;

    private ServletContext context = null;

    public DelFileTask() {
        super();
    }

    public DelFileTask(CouponService couponService) {
        this.couponService = couponService;
    }
    @Override
    public void run() {

        if (!isRunning) {
            Map<String,Object> params = new HashMap<>();
            params.put("couponState","1");
          List<Coupon> couponList = couponService.list(params);
          for(Coupon coupon : couponList){
               if(new Date().after(coupon.getEndTime())){
                   coupon.setCouponState(3);
                   couponService.update(coupon);
               }
          }
            isRunning = false;
//            context.log("指定任务执行结束");
        } else {
//            context.log("上一次任务执行还未结束");
        }
    }

}
