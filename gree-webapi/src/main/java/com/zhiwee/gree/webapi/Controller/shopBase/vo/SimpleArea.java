package com.zhiwee.gree.webapi.Controller.shopBase.vo;

import java.util.List;

/**
 * @author Knight
 * @since 2018/7/9 17:30
 */
public class SimpleArea {

    private String code;

    private String name;

    private Integer type;

    private List<SimpleArea> children;

    public SimpleArea() {
    }

    public SimpleArea(String code, String name, Integer type) {
        this.code = code;
        this.name = name;
        this.type = type;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public List<SimpleArea> getChildren() {
        return children;
    }

    public void setChildren(List<SimpleArea> children) {
        this.children = children;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
