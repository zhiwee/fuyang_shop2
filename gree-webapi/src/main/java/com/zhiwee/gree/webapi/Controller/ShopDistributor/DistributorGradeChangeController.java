package com.zhiwee.gree.webapi.Controller.ShopDistributor;


import com.zhiwee.gree.model.ShopDistributor.DistributorGrade;
import com.zhiwee.gree.model.ShopDistributor.DistributorGradeChange;
import com.zhiwee.gree.service.ShopDistributor.DistributorGradeChangeService;
import com.zhiwee.gree.service.ShopDistributor.DistributorGradeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.web.annotation.EnableGetRequest;
import xyz.icrab.common.web.annotation.EnableListRequest;

import java.util.HashMap;
import java.util.Map;

@RequestMapping("/distributorGradeChange")
@Controller
@ResponseBody
@EnableListRequest
@EnableGetRequest
public class DistributorGradeChangeController {

@Autowired
private DistributorGradeChangeService distributorGradeChangeService;




 /**
  * Description: 获取配送商基础信息改变记录
  * @author: sun
  * @Date 上午11:58 2019/6/17
  * @param:
  * @return:
  */
    @RequestMapping("/infoPage")
    @ResponseBody
    public Result<?> infoPage(@RequestBody Map<String, Object> param, Pagination pagination) {
        if (param == null) {
            param = new HashMap<>();
        }
        Pageable<DistributorGradeChange> pageable = distributorGradeChangeService.infoPage(param, pagination);
        return Result.ok(pageable);
    }






}
