package com.zhiwee.gree.webapi.Controller.shopBase;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.web.annotation.PermissionMode;
import xyz.icrab.common.web.annotation.PermissionMode.Mode;
import xyz.icrab.common.web.util.ConstantConfig;
import xyz.icrab.common.web.util.RuntimeConfig;

/**
 * @author sun on 2019/4/28
 */
@RequestMapping({"/runtime"})
public class RuntimeController {
    @Autowired(
            required = false
    )
    private RuntimeConfig runtimeConfig;
    @Autowired(
            required = false
    )
    private ConstantConfig constantConfig;

    public RuntimeController() {
    }

    @RequestMapping({"/config"})
    @ResponseBody
    @PermissionMode(Mode.White)
    public Result<?> config() {
        return Result.ok(this.runtimeConfig.getMap());
    }

    @RequestMapping({"/consts"})
    @ResponseBody
    @PermissionMode(Mode.White)
    public Result<Map<String, Map<String, String>>> consts() {
        return Result.ok(this.constantConfig.getConsts());
    }
}
