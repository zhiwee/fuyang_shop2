package com.zhiwee.gree.webapi.util;

import com.zhiwee.gree.model.IdentifiedUser;
import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import java.io.IOException;
import java.util.Map;
import java.util.Objects;
import org.apache.commons.lang3.StringUtils;
import xyz.icrab.common.web.session.Session;
import xyz.icrab.common.web.util.SessionUtils;

public class PermissionDirective implements TemplateDirectiveModel {
    public PermissionDirective() {
    }

    public void execute(Environment env, Map params, TemplateModel[] loopVars, TemplateDirectiveBody body) throws TemplateException, IOException {
        Object permissionCode = params.get("code");
        if (!Objects.isNull(permissionCode) && !StringUtils.isBlank(permissionCode.toString())) {
            String code = permissionCode.toString();
            Session session = SessionUtils.get();
            if (session != null) {
                IdentifiedUser user = (IdentifiedUser)session.getAttribute("auth");
                if (user.hasButton(code)) {
                    body.render(env.getOut());
                }

            }
        } else {
            body.render(env.getOut());
        }
    }
}
