package com.zhiwee.gree.webapi.Controller.Questionnaire;

import com.zhiwee.gree.model.OrderInfo.OrderExport;
import com.zhiwee.gree.model.Questionnaire.AnswerInfo;
import com.zhiwee.gree.model.Questionnaire.AnswerInfoExrt;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.User;
import com.zhiwee.gree.model.draw.CanDraws;
import com.zhiwee.gree.service.Questionnaire.AnswerInfoService;
import com.zhiwee.gree.service.draw.CanDrawsService;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.jeecgframework.poi.excel.ExcelExportUtil;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.enmus.ExcelType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.util.KeyUtils;
import xyz.icrab.common.web.annotation.EnableGetRequest;
import xyz.icrab.common.web.annotation.EnableListRequest;
import xyz.icrab.common.web.annotation.Session;
import xyz.icrab.ums.util.consts.SessionKeys;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.security.Key;
import java.util.*;

@RequestMapping("/answerInfo")
@Controller
@ResponseBody
@EnableListRequest
@EnableGetRequest
public class AnswerInfoController {

    @Autowired
    private AnswerInfoService answerInfoService;
    @Autowired
    private CanDrawsService canDrawsService;
    /**
     * @return
     * @author tzj
     * @description 问卷答案页面
     * @date 2019/12/8 19:32
     */
    @RequestMapping(value = "/infoPage")
    @SuppressWarnings("all")
    public Result<?> infoPage(@RequestBody Map<String, Object> params, Pagination pagination) {
        if (params == null) {
            params = new HashMap<>();
        }
        Pageable<AnswerInfo> page = answerInfoService.pageInfo(params, pagination);
        return Result.ok(page);
    }

    /**
     * @author tzj
     * @description 用户问卷答案
     * @date 2019/12/10 10:11
     * @return
    */
    @RequestMapping(value = "/add")
    public Result<?> add(@Session("ShopUser") ShopUser user, @RequestBody(required = false) AnswerInfo answerInfo) {
            Boolean hasOne = checkRepeat(user);
            if (hasOne){
                return Result.of(Status.ClientError.BAD_REQUEST,"该用户已经参加过本次问卷调查");
            }
            answerInfo.setId(KeyUtils.getUUID());
            answerInfo.setUserId(user.getId());
            CanDraws canDraws=new CanDraws();
            //抽奖次数加一
            canDraws.setShopUserId(user.getId());
            canDraws.setId(KeyUtils.getUUID());
            canDraws.setDrawsSum(1);

            canDrawsService.save(canDraws);
            answerInfoService.save(answerInfo);
            return Result.ok();
    }
        //该用户是否已经填写过
    private Boolean checkRepeat(ShopUser user) {
        AnswerInfo answerInfo=new AnswerInfo();
        answerInfo.setUserId(user.getId());
        answerInfo= answerInfoService.getOne(answerInfo);
        return answerInfo != null;
    }

    /**
     * @author tzj
     * @description 校验是否参加
     * @date 2019/12/10 10:21
     * @return
    */
    @RequestMapping(value = "/check")
    public Result<?> check(@Session("shopUser") ShopUser user) {
        Boolean aBoolean = checkRepeat(user);
        if (aBoolean){
            return Result.of(Status.ClientError.BAD_REQUEST,"该用户已经参加过本次问卷调查");
        }else {
            return Result.ok();
        }
    }

    /**
     * Description: 订单导出
     *
     * @author: chen
     * @Date 下午4:36 2019/5/12
     * @param:
     * @return:
     */
    @RequestMapping("/export")
    public void export(HttpServletRequest req, HttpServletResponse rsp, @Session(SessionKeys.USER) User user) throws IOException {
        Map<String, Object> params = new HashMap<>();
        List<AnswerInfoExrt> result = new ArrayList<>();
        result = answerInfoService.queryAnswerInfoExport(params);

        // 导出Excel
        OutputStream os = rsp.getOutputStream();

        rsp.setContentType("application/x-download");
        // 设置导出文件名称
        rsp.setHeader("Content-Disposition",
                "attachment;filename=" + DateFormatUtils.format(new Date(), "yyyyMMddHHmmssSSS") + ".xlsx");
        ExportParams param = new ExportParams("问卷信息", "问卷题目信息、相关答案", "问卷信息表");
        param.setAddIndex(true);
        param.setType(ExcelType.XSSF);
        Workbook excel = ExcelExportUtil.exportExcel(param, AnswerInfoExrt.class, result);
        excel.write(os);
        os.flush();
        os.close();
    }




}
