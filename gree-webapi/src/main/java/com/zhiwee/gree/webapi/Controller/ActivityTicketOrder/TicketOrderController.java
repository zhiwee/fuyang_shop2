package com.zhiwee.gree.webapi.Controller.ActivityTicketOrder;


import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.internal.util.AlipaySignature;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.alipay.api.request.AlipayTradeRefundRequest;
import com.alipay.api.response.AlipayTradeRefundResponse;
import com.zhiwee.gree.model.ActivityTicketOrder.TicketOrder;
import com.zhiwee.gree.model.ActivityTicketOrder.TicketOrderExport;
import com.zhiwee.gree.model.Alipay.AliRefund;
import com.zhiwee.gree.model.Alipay.AlipayVo;
import com.zhiwee.gree.model.BaseInfo.BaseInfo;
import com.zhiwee.gree.model.Dealer.DealerInfo;
import com.zhiwee.gree.model.OrderInfo.OrderRefundReason;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.User;
import com.zhiwee.gree.model.WxPay.WeChatConfig;
import com.zhiwee.gree.model.WxPay.WeChatParams;
import com.zhiwee.gree.service.ActivityTicketOrder.ActivityTicketOrderService;
import com.zhiwee.gree.service.ActivityTicketOrder.TicketOrderService;
import com.zhiwee.gree.service.BaseInfo.BaseInfoService;
import com.zhiwee.gree.service.Dealer.DealerInfoService;
import com.zhiwee.gree.service.ShopUser.ShopUserService;
import com.zhiwee.gree.util.SequenceUtils;
import com.zhiwee.gree.webapi.util.ClientCustomSSL;
import com.zhiwee.gree.webapi.util.PayForUtil;
import com.zhiwee.gree.webapi.util.WeixinPay;
import com.zhiwee.gree.webapi.util.XMLUtil;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.jeecgframework.poi.excel.ExcelExportUtil;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.enmus.ExcelType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import shaded.com.google.gson.Gson;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.model.enums.YesOrNo;
import xyz.icrab.common.util.IdGenerator;
import xyz.icrab.common.web.annotation.EnableGetRequest;
import xyz.icrab.common.web.annotation.EnablePageRequest;
import xyz.icrab.common.web.annotation.PermissionMode;
import xyz.icrab.common.web.annotation.Session;
import xyz.icrab.common.web.param.IdParam;
import xyz.icrab.common.web.util.Validator;
import xyz.icrab.ums.util.consts.SessionKeys;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.*;
import java.util.*;


/**
 * @author sun
 * @since 2019/6/23 15:11
 */
@RestController
@RequestMapping("/ticketOrder")
@EnablePageRequest
@EnableGetRequest
public class TicketOrderController {


    @Value("${ALIPAY.APPID}")
    private String app_id;
    @Value("${ALIPAY.PRIVATEKEY}")
    private String merchant_private_key;
    @Value("${ALIPAY.PUBLICKEY}")
    private String alipay_public_key;
    @Value("${ALIPAY.NOTIFY_TICKETURL}")
    private String notify_url;
    @Value("${ALIPAY.RETURNA_TICKETURL}")
    private String return_url;
    @Value("${ALIPAY.SIGN}")
    private String sign_type;
    private String charset = "utf-8";

    @Value("${ALIPAY.SERVER}")
    private String gatewayUrL;


    @Autowired
    private TicketOrderService ticketOrderService;


    @Autowired
    private ActivityTicketOrderService activityTicketOrderService;

    @Autowired
    private DealerInfoService dealerInfoService;

    @Autowired
    private BaseInfoService baseInfoService;

    @Autowired
    private ShopUserService shopUserService;

    @Autowired
    private SequenceUtils sequenceUtils;
/**
 * Description: 劵的购买记录
 * @author: sun
 * @Date 下午5:20 2019/6/23
 * @param:
 * @return:
 */
    @RequestMapping({"/page"})
    public Result<Pageable<TicketOrder>> page(@RequestBody(required = false) Map<String, Object> param, Pagination pagination) {
        return Result.ok(ticketOrderService.pageInfo(param, pagination));
    }



 /**
  * Description: 购买认筹劵
  * @author: sun
  * @Date 上午9:36 2019/6/24
  * @param:
  * @return:
  */
    @RequestMapping("/createOrder")
    public Result<?> page(@Session("ShopUser") ShopUser user,@RequestBody TicketOrder ticketOrder) {
        Validator validator = new Validator();
        validator.notEmpty(ticketOrder.getPhone(), "手机号不能修改");
        validator.notNull(ticketOrder.getAreaCode(), "区域编码不能为空");
        validator.notNull(ticketOrder.getProvinceCode(), "区域编码不能为空");
        validator.notNull(ticketOrder.getCityCode(), "区域编码不能为空");
        validator.notNull(ticketOrder.getDetailAddress(), "详细地址不能为空");
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }

        ShopUser shopUser = shopUserService.get(user.getId());
        if(shopUser.getUsername().equals("")){
            return Result.of(Status.ClientError.BAD_REQUEST, "请先绑定手机号");
        }

        //如果没有传门店过来，查找默认的门店
        if(ticketOrder.getDealerId() == null || ticketOrder.getDealerId().equals("") ){
            Map<String,Object> params = new HashMap<>();
            params.put("isDefault",1);
            List<DealerInfo>  dealerInfos= dealerInfoService.list(params);
            if(dealerInfos.isEmpty() && dealerInfos.size() > 0){
                return Result.of(Status.ClientError.BAD_REQUEST, "默认门店不存在");
            }else{
                ticketOrder.setDealerId(dealerInfos.get(0).getId());
                ticketOrder.setActivityId(dealerInfos.get(0).getActivityId());
            }
        }else{
            DealerInfo dealerInfo = dealerInfoService.get(ticketOrder.getDealerId());
            if(dealerInfo == null){
                return Result.of(Status.ClientError.BAD_REQUEST, "该门店不存在");
            }else{
                if(dealerInfo.getState() != YesOrNo.YES){
                    return Result.of(Status.ClientError.BAD_REQUEST, "该门店被禁用，无法认筹");
                }
                ticketOrder.setActivityId(dealerInfo.getActivityId());
            }
        }

        List<BaseInfo> list = baseInfoService.list();
        BaseInfo baseInfo = list.get(0);
        Double money = baseInfo.getRealPrice();
        if(money == null || money < 0){
            return Result.of(Status.ClientError.BAD_REQUEST, "请联系管理员购买劵");
        }
        if(ticketOrder.getAmount() == null || ticketOrder.getAmount() <= 0){
            return Result.of(Status.ClientError.BAD_REQUEST, "请填写正确的数量");
        }
        List<TicketOrder> ticketOrders = new ArrayList<>();
        Random ra =new Random();
        for(int i = 0;i < ticketOrder.getAmount();i++){
            TicketOrder to = new TicketOrder();
            to.setId(IdGenerator.objectId());
            String  ticket =sequenceUtils.addTicketByDay();
            to.setTicketSeqno(ticket);
            to.setType(1);
//            to.setOrderPassword(String.valueOf((Math.random()*9+1)*100000));
            to.setOrderPassword(ra.nextInt(999999)+"");
            to.setOrderName(ticketOrder.getOrderName());
            to.setDealerId(ticketOrder.getDealerId());
            to.setActivityId(ticketOrder.getActivityId());
            to.setDeliveryProvince(ticketOrder.getDeliveryProvince());
            to.setDeliveryCity(ticketOrder.getDeliveryCity());
            to.setDeliveryArea(ticketOrder.getDeliveryArea());
            to.setProvinceCode(ticketOrder.getProvinceCode());
            to.setCityCode(ticketOrder.getCityCode());
            to.setAreaCode(ticketOrder.getAreaCode());
            to.setDetailAddress(ticketOrder.getDetailAddress());
            to.setCreateTime(new Date());
            to.setOrderNum(sequenceUtils.ticketOrderByDay());
            to.setUserId(user.getId());
            to.setOrderState(1);
            to.setRefundState(1);
            to.setPhone(ticketOrder.getPhone());
            to.setRealPrice(money);
            ticketOrders.add(to);
        }

        ticketOrderService.save(ticketOrders);
        return Result.ok();
    }


/**
 * Description: 支付宝支付
 * @author: sun
 * @Date 上午11:03 2019/6/24
 * @param:
 * @return:
 */
    @GetMapping("/alipay")
    @PermissionMode(PermissionMode.Mode.White)
    @ResponseBody
    private String alipayPay(String out_trade_no) throws AlipayApiException {
        AlipayVo vo = new AlipayVo();
        TicketOrder order = ticketOrderService.get(out_trade_no);
        vo.setTotal_amount(order.getRealPrice().toString());
        vo.setOut_trade_no(order.getId());
        vo.setSubject("nelson-test-title");
        vo.setProduct_code("FAST_INSTANT_TRADE_PAY"); //这个是固定的
        String json = new Gson().toJson(vo);
        System.out.println(json + "孙登凯无敌哈哈哈哈哈哈哈哈");
        AlipayClient alipayClient = new DefaultAlipayClient(gatewayUrL, app_id, merchant_private_key, "json", charset, alipay_public_key, sign_type);
        // 设置请求参数
        AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();

        alipayRequest.setNotifyUrl(notify_url);
        alipayRequest.setBizContent(json);
        String result = alipayClient.pageExecute(alipayRequest).getBody();
        System.out.println(result);
        return result; //这里生成一个表单，会自动提交
    }

    /**
     * @param request
     * @param out_trade_no 商户订单号
     * @param trade_no     支付宝交易凭证号
     * @param trade_status 交易状态
     * @return String
     * @throws AlipayApiException
     * @throws
     * @Title: alipayNotify
     * @Description: 支付宝回调接口
     * @author SUN
     */
    @PostMapping("/notify")
    @PermissionMode(PermissionMode.Mode.White)
    private Result<?>  alipayNotify(HttpServletRequest request, String out_trade_no, String trade_no, String trade_status)
            throws AlipayApiException {
        TicketOrder order = ticketOrderService.get(out_trade_no);
        Map<String, String> map = new HashMap<String, String>();
        Map<String, String[]> requestParams = request.getParameterMap();
        for (Iterator<String> iter = requestParams.keySet().iterator(); iter.hasNext(); ) {
            String name = iter.next();
            String[] values = requestParams.get(name);
            String valueStr = "";
            for (int i = 0; i < values.length; i++) {
                valueStr = (i == values.length - 1) ? valueStr + values[i] : valueStr + values[i] + ",";
                System.out.println(valueStr + "孙登凯无敌" + i);
            }
            map.put(name, valueStr);
        }
        boolean signVerified = false;
        try {
            signVerified = AlipaySignature.rsaCheckV1(map, alipay_public_key, charset, sign_type);
        } catch (AlipayApiException e) {
            e.printStackTrace();
            return Result.of(Status.ClientError.BAD_REQUEST, "签名失败，支付失败");// 验签发生异常,则直接返回失败
        }
        if (signVerified) {
            order.setOrderState(2);
            order.setPayTime(new Date());
            order.setPayType(1);
            order.setUpdateTime(new Date());
            ticketOrderService.updateAndSave(order);
            return Result.ok("支付成功");
        } else {
            System.out.println("验证失败,不去更新状态");
            return Result.of(Status.ClientError.BAD_REQUEST, "支付失败");
        }
    }




    /**
     * Description: 支付宝退款
     * @author: sun
     * @Date 下午3:52 2019/5/13
     * @param: out_trade_no 保持唯一 退款金额不能大于订单金额
     * @return:
     */
    @RequestMapping(value = "/orderBackMoney")
    @ResponseBody
    public Object orderBackMoney(@RequestBody(required = false) AliRefund aliRefund) {
        List<BaseInfo> baseInfos  =  baseInfoService.list();
        if(!baseInfos.get(0).getRefundPassword().equals(aliRefund.getRefundPassword())){
            return Result.of(Status.ClientError.BAD_REQUEST, "退款密码不正确");
        }
        AliRefund refund = new AliRefund();
        TicketOrder order = ticketOrderService.get(aliRefund.getOut_trade_no());
        if(order==null){
            Result.of(Status.ClientError.BAD_REQUEST, "该订单不存在");
        }
        if (order.getOrderState() != 2 || order.getPayType() != 1 || order.getRefundState() != 2) {
            return Result.of(Status.ClientError.BAD_REQUEST, "该订单未付款或者该订单不是支付宝支付或者该订单没有申请无法退款");
        }
        Double refundMoney = Double.valueOf(aliRefund.getRefund_amount());
        if(refundMoney > order.getRealPrice()){
            return Result.of(Status.ClientError.BAD_REQUEST, "退款金额超过订单的金额");
        }
        if(refundMoney <= 0){
            return Result.of(Status.ClientError.BAD_REQUEST, "退款金额不能小于等于0");
        }
        refund.setOut_trade_no(order.getId());
        refund.setRefund_amount(refundMoney.toString());
        refund.setRefund_reason("正常退款");
        AlipayClient alipayClient = new DefaultAlipayClient(gatewayUrL, app_id, merchant_private_key, "json", charset, alipay_public_key, sign_type);
        AlipayTradeRefundRequest request = new AlipayTradeRefundRequest();
        String json = new Gson().toJson(refund);
        request.setBizContent(json);
        AlipayTradeRefundResponse response = null;
        try {
            response = alipayClient.execute(request);
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        if (response.isSuccess()) {
            System.out.println(response.getBody());
            order.setUpdateTime(new Date());
            order.setRefundMoney(refundMoney);
            order.setRefundState(4);
            order.setOrderState(4);
            ticketOrderService.updateTicketOrder(order);
            return  Result.ok("退款成功");
        } else {
            return  Result.ok("退款失败");
        }
    }



/**
 * Description: 微信扫码支付
 * @author: sun
 * @Date 上午11:17 2019/6/24
 * @param:
 * @return:
 */

    @RequestMapping("/wxpay")
    @ResponseBody
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> wxPay(@RequestBody WeChatParams ps) throws Exception {
        TicketOrder order = ticketOrderService.get(ps.getOut_trade_no());
        ps.setBody("认筹劵");
        ps.setTotal_fee(String.valueOf((int) (order.getRealPrice() * 100)));
        ps.setOut_trade_no(order.getId());
        ps.setAttach("xiner");
        ps.setMemberid("888");
        String   trade_type = WeChatConfig.scanPay;
        Map map = WeixinPay.getCodeUrl(ps,WeChatConfig.WECHAT_TICKETNOTIFYURL,trade_type,null);
        if(!"SUCCESS".equals((String) map.get("return_code"))){
            return Result.of(Status.ClientError.BAD_REQUEST, (String) map.get("return_msg"));
        }
        String urlCode = (String) map.get("code_url");
//        String generate = CodesUtils.generate(urlCode);
        System.out.println(urlCode);
        return Result.ok(urlCode);
    }



    /**
     * pc端微信支付之后的回调方法
     *
     * @param request
     * @param response
     * @throws Exception
     */
    @PermissionMode(PermissionMode.Mode.White)
    @RequestMapping(value = "wechat_notifurl", method = RequestMethod.POST)
    public void wechat_notify_url_pc(HttpServletRequest request, HttpServletResponse response) throws Exception {

        //读取参数
        InputStream inputStream;
        StringBuffer sb = new StringBuffer();
        inputStream = request.getInputStream();
        String s;
        BufferedReader in = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
        while ((s = in.readLine()) != null) {
            sb.append(s);
        }
        in.close();
        inputStream.close();

        //解析xml成map
        Map<String, String> m = new HashMap<String, String>();
        m = XMLUtil.doXMLParse(sb.toString());

        //过滤空 设置 TreeMap
        SortedMap<Object, Object> packageParams = new TreeMap<Object, Object>();
        Iterator<String> it = m.keySet().iterator();
        while (it.hasNext()) {
            String parameter = it.next();
            String parameterValue = m.get(parameter);

            String v = "";
            if (null != parameterValue) {
                v = parameterValue.trim();
            }
            packageParams.put(parameter, v);
        }
        // 微信支付的API密钥
        String key = WeChatConfig.APIKEY; // key

//        lg.info("微信支付返回回来的参数："+packageParams);
        //判断签名是否正确
        if (PayForUtil.isTenpaySign("UTF-8", packageParams, key)) {
            //------------------------------
            //处理业务开始
            //------------------------------
            String resXml = "";
            if ("SUCCESS".equals((String) packageParams.get("result_code"))) {
                String out_trade_no = (String) packageParams.get("out_trade_no");
                TicketOrder order = ticketOrderService.get(out_trade_no);
                order.setPayTime(new Date());
                order.setOrderState(2);
                order.setPayType(2);
                ticketOrderService.updateAndSave(order);

                resXml = "<xml>" + "<return_code><![CDATA[SUCCESS]]></return_code>"
                        + "<return_msg><![CDATA[OK]]></return_msg>" + "</xml> ";

            } else {
//                lg.info("支付失败,错误信息：" + packageParams.get("err_code"));
                resXml = "<xml>" + "<return_code><![CDATA[FAIL]]></return_code>"
                        + "<return_msg><![CDATA[报文为空]]></return_msg>" + "</xml> ";
            }
            //------------------------------
            //处理业务完毕
            //------------------------------
            BufferedOutputStream out = new BufferedOutputStream(
                    response.getOutputStream());
            out.write(resXml.getBytes());
            out.flush();
            out.close();
        } else {
            System.out.println("支付失败");
        }

    }





    /**
     * Description: 微信退款的接口
     *
     * @author: sun
     * @Date 下午1:56 2019/5/17
     * @param:
     * @return:
     */
    @RequestMapping("/refund")
    @ResponseBody
    public Result<?> wxPayRefund(@RequestBody WeChatParams ps) {
        List<BaseInfo>  baseInfos  =  baseInfoService.list();
        if(!baseInfos.get(0).getRefundPassword().equals(ps.getRefundPassword())){
            return Result.of(Status.ClientError.BAD_REQUEST, "退款密码不正确");
        }
        String id = ps.getOut_trade_no();
        TicketOrder order = ticketOrderService.get(id);
        if (order.getOrderState() != 2 || order.getPayType() != 2 || order.getRefundState() != 2 ) {
            return Result.of(Status.ClientError.BAD_REQUEST, "该订单未付款或者该订单不是微信支付或者改订单没有申请无法退款");
        }
        Double refundMoney = Double.valueOf(ps.getTotal_fee());
        if(refundMoney > order.getRealPrice()*100){
            return Result.of(Status.ClientError.BAD_REQUEST, "退款金额超过订单的金额");
        }
        if(refundMoney <= 0){
            return Result.of(Status.ClientError.BAD_REQUEST, "退款金额不能小于等于0");
        }
        Map map = ClientCustomSSL.setUrl(order.getId(),order.getRealPrice(), "",refundMoney,WeChatConfig.REFUND_TICKETURL);
        if (map.get("status").equals("SUCCESS")) {
            order.setRefundMoney(refundMoney);
            order.setOrderState(4);
            order.setRefundState(4);
            ticketOrderService.updateTicketOrder(order);
            return Result.ok("申请退款成功");
        } else {
            return Result.of(Status.ClientError.BAD_REQUEST, map.get("msg"));
        }

    }


    /**
     * Description: 微信支申请退款后的回调方法
     *
     * @author: sun
     * @Date 下午1:56 2019/5/17
     * @param:
     * @return:
     */
    @RequestMapping("/refundUrl")
    @ResponseBody
    public Result<?> wxPayRefundUrl(HttpServletRequest req) throws Exception {
        InputStream inputStream;
        StringBuffer sb = new StringBuffer();
        inputStream = req.getInputStream();
        String s;
        BufferedReader in = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
        while ((s = in.readLine()) != null) {
            sb.append(s);
        }
        in.close();
        inputStream.close();

        //解析xml成map
        Map<String, String> param = new HashMap<String, String>();
        param = XMLUtil.doXMLParse(sb.toString());
        System.out.println(param + "微信退款");
        if (param != null) {
            if (param.get("return_code").equals("SUCCESS")) {
                //退款成功，修改订单账号
                TicketOrder order = ticketOrderService.get(param.get("out_trade_no"));
                order.setOrderState(4);
                order.setRefundState(4);
                ticketOrderService.update(order);
                return Result.ok("申请退款成功");
            } else {
                return Result.of(Status.ClientError.BAD_REQUEST, param.get("return_msg"));
            }
        }

        return Result.of(Status.ClientError.BAD_REQUEST, "退款失败");

    }






    /**
     * Description: 申请认筹劵退款
     * @author: sun
     * @Date 下午11:52 2019/5/19
     * @param:
     * @return:
     */
    @PermissionMode(PermissionMode.Mode.White)
    @RequestMapping("/backMoney")
    public Object backMoney(@RequestBody OrderRefundReason orderRefundReason, @Session("ShopUser") ShopUser user) {

        TicketOrder order = ticketOrderService.get(orderRefundReason.getOrderId());

        if(order == null || order.getOrderState() != 2){
            return Result.of(Status.ClientError.BAD_REQUEST, "该订单未支付无法申请退款");
        }

        //1 未申请退款 2 申请退款中  3 拒绝申请退款  4 同意申请退款
        if(order.getRefundState() == 2 || order.getRefundState() == 4 ){

            return Result.of(Status.ClientError.BAD_REQUEST, "订单申请退款中或者改订单已经同意退款");

        }
        order.setRefundState(2);
        order.setUpdateTime(new Date());
        ticketOrderService.update(order);
        return Result.ok("申请退款成功，等待商家审核");
    }



    /**
     * Description: 获取认筹劵购买记录
     * @author: sun
     * @Date 上午10:47 2019/5/12
     * @param:
     * @return:
     */
    @RequestMapping("/achieveMyTicketOrder")
    public Result<?> achieveMyTicketOrder(@RequestBody Map<String, Object> params, @Session("ShopUser") ShopUser shopUser, Pagination pagination) {
        params.put("userId", shopUser.getId());
        Pageable<TicketOrder> page = ticketOrderService.achieveMyOrder(params, pagination);
        return Result.ok(page);
    }





/**
 * Description: 取消订单
 * @author: sun
 * @Date 下午1:47 2019/6/24
 * @param:
 * @return:
 */
    @PermissionMode(PermissionMode.Mode.White)
    @RequestMapping("/cancleOrder")
    public Result<?> cancleOrder(@RequestBody @Valid IdParam map) {
        TicketOrder order = ticketOrderService.get(map.getId());
        if (order.getOrderState() != 1) {
            return Result.of(Status.ClientError.BAD_REQUEST, "只有未支付的订单可以取消订单");
        }
        order.setOrderState(3);
        ticketOrderService.update(order);
        return Result.ok();
    }




    /**
     * Description: 拒绝退款
     * @author: sun
     * @Date 下午1:47 2019/6/24
     * @param:
     * @return:
     */
    @RequestMapping("/refuceBack")
    public Result<?> refuceBack(@RequestBody @Valid IdParam map) {
        TicketOrder order = ticketOrderService.get(map.getId());
        if (order.getOrderState() == 2 && order.getRefundState() == 2) {
            return Result.of(Status.ClientError.BAD_REQUEST, "只有已支付的订单并且订单申请了退款才可以拒绝退款");
        }
        order.setRefundState(3);
        ticketOrderService.update(order);
        return Result.ok();
    }






    /**
     * Description: 认筹劵购买记录导出
     * @author: sun
     * @Date 下午8:52 2019/6/24
     * @param:
     * @return:
     */
    @RequestMapping("/export")
    public void export(HttpServletRequest req, HttpServletResponse rsp, @Session(SessionKeys.USER) User user) throws IOException {
        Map<String, Object> params = new HashMap<>();
        params.put("orderState", req.getParameter("orderState"));
        params.put("payType", req.getParameter("payType"));
        if (StringUtils.isNotBlank((String) params.get("endCreateTime"))) {
            params.put("endCreateTime", (String) params.get("endCreateTime") + " 23:59:59");
        }
        List<TicketOrderExport> result = new ArrayList<>();
        result = ticketOrderService.pageList(params);

        // 导出Excel
        OutputStream os = rsp.getOutputStream();

        rsp.setContentType("application/x-download");
        // 设置导出文件名称
        rsp.setHeader("Content-Disposition",
                "attachment;filename=" + DateFormatUtils.format(new Date(), "yyyyMMddHHmmssSSS") + ".xlsx");
        ExportParams param = new ExportParams("认出劵购买信息", "认出劵购买信息", "认出劵购买信息");
        param.setAddIndex(true);
        param.setType(ExcelType.XSSF);
        Workbook excel = ExcelExportUtil.exportExcel(param, TicketOrderExport.class, result);
        excel.write(os);
        os.flush();
        os.close();
    }

}
