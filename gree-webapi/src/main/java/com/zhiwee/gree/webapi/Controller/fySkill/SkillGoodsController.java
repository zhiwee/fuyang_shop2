package com.zhiwee.gree.webapi.Controller.fySkill;

import com.zhiwee.gree.model.FyGoods.*;
import com.zhiwee.gree.model.GoodsInfo.GoodVo.GoodsExo;
import com.zhiwee.gree.model.GoodsInfo.Goods;
import com.zhiwee.gree.model.GoodsInfo.GoodsInfoPicture;
import com.zhiwee.gree.model.resultBase.FileResult;
import com.zhiwee.gree.service.FyGoods.FyGoodsDetailTextService;
import com.zhiwee.gree.service.FyGoods.FygoodsImgService;
import com.zhiwee.gree.service.FyGoods.FygoodsService;
import com.zhiwee.gree.service.GoodsInfo.GoodsInfoPictureService;
import com.zhiwee.gree.service.GoodsInfo.GoodsService;
import com.zhiwee.gree.service.support.FileUpload.FileUploadService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.jeecgframework.poi.excel.ExcelExportUtil;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.util.KeyUtils;
import xyz.icrab.common.web.annotation.*;
import xyz.icrab.common.web.param.IdParam;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.util.*;

/**
 * @author DELL
 */
@RestController
@RequestMapping("/skillGoods")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class SkillGoodsController {
    @Resource
    private FileUploadService fileUploadService;

    @Resource
    private FygoodsService fygoodsService;
    @Resource
    private GoodsService goodsService;
    @Resource
    private GoodsInfoPictureService goodsInfoPictureService;
    @Resource
    private   FygoodsImgService  fygoodsImgService;

    @Resource
    private FyGoodsDetailTextService fyGoodsDetailTextService;


    /**
     * @author tzj
     * @description 编辑商品
     * @date 2020/4/26 8:57
    */
      @RequestMapping("/saveOrUpdate")
     public   Result<?>  skillGoodsSaveController(@RequestBody Fygoods fygoods){
          if(null== fygoods.getLimitCount()){
              fygoods.setLimitCount(1);
          }
          //如果id为空，则是添加操作
          if (fygoods.getId() == null || "".equals(fygoods.getId().trim()) || fygoods.getId().trim().length() ==0) {
              //确保同一时间段只有一个商品
              Boolean result = checkOnly(fygoods);
              if (!result){
                  return Result.of(Status.ClientError.BAD_REQUEST,"与已经存在的秒杀商品冲突,请检查");
              }
              fygoods.setId(KeyUtils.getKey());
              fygoods.setLiveUrl("pages/lowerdetail/lowerdetail.html?lowerid="+fygoods.getId()+"&goodstype=3");
              fygoods.setState(1);
              fygoods.setIskill(1);
              fygoodsService.save(fygoods);
          }else{
              Boolean result = checkOnly(fygoods);
              if (!result){
                  return Result.of(Status.ClientError.BAD_REQUEST,"与已经存在的秒杀商品冲突,请检查");
              }
              //如果id不为空则是更新操作
              fygoods.setLiveUrl("pages/lowerdetail/lowerdetail.html?lowerid="+fygoods.getId()+"&goodstype=3");
              fygoods.setState(1);
              fygoods.setIskill(1);
              fygoodsService.update(fygoods);
          }
           return   Result.ok(fygoods);
     }

     /**
      * @author tzj
      * @description 确定唯一
      * @date 2020/4/26 15:45
     */
    private Boolean checkOnly(Fygoods grouponGoods) {
        Map<String,Object> param=new HashMap<>(6);
        param.put("goodsId",grouponGoods.getGoodsId());
        List<Fygoods> list = fygoodsService.list(param);
        //是否为空
        if (CollectionUtils.isNotEmpty(list)) {
            //大于1直接返回
            if (list.size()>1){
                return false;
            }
            Fygoods groupGoods1 = list.get(0);
            //id相同
            if (!grouponGoods.getId().equals(groupGoods1.getId())) {
                //如果开始时间在他们中间则不行
                return (!grouponGoods.getKillStartTime().after(groupGoods1.getKillStartTime()) || !grouponGoods.getKillStartTime().before(groupGoods1.getKillEndTime())) &&
                        //如果开始时间和结束时间都超过不行
                        (!grouponGoods.getKillStartTime().before(groupGoods1.getKillStartTime()) || !grouponGoods.getKillEndTime().after(groupGoods1.getKillEndTime())) &&
                        //如果结束时间在它开始之后且 在它结束时间之前 不行
                        (!grouponGoods.getKillEndTime().after(groupGoods1.getKillStartTime()) || !grouponGoods.getKillEndTime().before(groupGoods1.getKillEndTime()));
            }else {
                return true;
            }

        }else {
            return true;
        }
    }


    /**
    * @author tzj
    * @description 图片
    * @date 2020/4/26 8:58
   */
     @RequestMapping("/imgUpload")
    public Result<?> uploadBackPicture(@RequestPart("file") MultipartFile file ) throws Exception {
        FileResult result= fileUploadService.fileUpload(file.getOriginalFilename(),file.getInputStream());
        return Result.ok(result.getFileUrl());
    }


 /**
  * @author tzj
  * @description 获取秒杀商品列表
  * @date 2020/4/26 8:56
 */
    @RequestMapping("/list")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> getFyGoodsList(Pagination pagination){
        Map<String,Object> param = new HashMap<>();
        param.put("iskill",1);
        Pageable<Fygoods> list =  fygoodsService.queryGoodsList(param,pagination);
        return   Result.ok(list);

    }



    /**
     * @author tzj
     * @description 启用警用
     * @date 2020/4/26 8:58f
    */
    @RequestMapping("/openOrStop")
    public Result<?> fyGoodsOpenOrStop(@RequestBody Fygoods fygoods)  {
        fygoodsService.update(fygoods);
        return Result.ok();
    }



   /**
    * @author tzj
    * @description 通过ID查询信息
    * @date 2020/4/26 8:58
   */
    @RequestMapping("/get")
    public    Result<?> get(@RequestBody IdParam param){
        Fygoods  fygoods  =  fygoodsService.get(param.getId());
        return  Result.ok(fygoods);
    }


    /**
     * @Author: jick
     * @Date: 2019/12/10 23:55
     * 删除操作
     */
    @RequestMapping("/delete")
    public  Result<?>  fyGoodsDelete(@RequestBody IdParam idParam){
            //删除商品表的数据
            Fygoods fygoods  = new Fygoods();
            fygoods.setId(idParam.getId());
            fygoodsService.delete(fygoods);
            return    Result.ok();
    }




    /**
     * @Author: jick
     * @Date: 2019/12/12 11:47
     * 前台展示秒杀商品
     */
    @RequestMapping(value = "/portal/list")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> portalGetFyGoodsList(@RequestBody(required =  false) Map<String,Object> param){
        if(null ==param){
            param = new HashMap<>(10);
        }
        param.put("state",1);
//        param.put("date",new Date());
         List<Fygoods> list  =  fygoodsService.portalQueryGoodsList(param);
        if (CollectionUtils.isNotEmpty(list)){
            for (Fygoods next : list) {
                Goods goods = goodsService.get(next.getGoodsId());
                if (goods!=null){
                    Map<String, Object> params=new HashMap<>(5);
                    params.put("goodsId",goods.getGoodsId());
                    List<String> list1=new ArrayList<>(20);
                    List<GoodsInfoPicture> goodsInfoPictures = goodsInfoPictureService.listAll(params);
                    if (CollectionUtils.isNotEmpty(goodsInfoPictures)){
                        for (GoodsInfoPicture goodsInfoPicture : goodsInfoPictures) {
                            list1.add(goodsInfoPicture.getImgUrl());
                        }
                    }
                    goods.setUrlList(list1);
                    next.setGoods(goods);
                }
            }
        }
        return   Result.ok(list);
    }

    /**
     * @Author: jick
     * @Date: 2019/12/12 13:00
     * 获取商品样图
     */
    @RequestMapping("/img/list")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> goodsImglist(@RequestBody Map<String, Object> param){
        List<FyGoodsImg>  list =  fygoodsImgService.queryImgList(param);
        return Result.ok(list);
    }


    /**
     * @Author: jick
     * @Date: 2019/12/12 13:02
     * 获取商品详情图
     */
        @RequestMapping("/get/goodsItem")
        @PermissionMode(PermissionMode.Mode.White)
        public  Result<?>  getGoodsItem(@RequestBody Map<String, Object> param){
            FyGoodsDetailText  getInfo  =   fyGoodsDetailTextService.getOne(param);
            return   Result.ok(getInfo);

        }


        /**
         * @Author: jick
         * @Date: 2019/12/12 19:23
         * 根据商品的id获取商品的基本信息
         */
        @RequestMapping("/get/baseInfo")
        @PermissionMode(PermissionMode.Mode.White)
        public  Result<?> getBaseInfo(@RequestBody Map<String,Object> param){
            Fygoods  fygoods  = fygoodsService.getOne(param);
            return   Result.ok(fygoods);
        }

    @RequestMapping("/exportFyGoods")
    public void exportFyGoods(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        List<FygoodsExo> list = fygoodsService.listall();
        // 导出Excel
        OutputStream os = resp.getOutputStream();
        resp.setContentType("application/x-download");
        // 设置导出文件名称
        resp.setHeader("Content-Disposition",
                "attachment;filename=" + DateFormatUtils.format(new Date(), "yyyyMMddHHmmssSSS") + ".xls");
        ExportParams param = new ExportParams("秒杀信息", "秒杀信息", "商品秒杀信息");
        //param.setType(ExcelType.XSSF);
        Workbook excel = ExcelExportUtil.exportExcel(param, FygoodsExo.class, list);
        excel.write(os);
        os.flush();
        os.close();
    }




}
