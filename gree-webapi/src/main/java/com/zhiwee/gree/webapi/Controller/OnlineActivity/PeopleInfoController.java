package com.zhiwee.gree.webapi.Controller.OnlineActivity;

import com.zhiwee.gree.model.Coupon.CouponExport;
import com.zhiwee.gree.model.GoodsInfo.GoodVo.PeopleExr;
import com.zhiwee.gree.model.GoodsInfo.Goods;
import com.zhiwee.gree.model.GoodsInfo.LAPeople;
import com.zhiwee.gree.model.User;
import com.zhiwee.gree.service.OnlineActivity.OnlineActivityInfoService;
import com.zhiwee.gree.service.OnlineActivity.PeopleInfoService;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.jeecgframework.poi.excel.ExcelExportUtil;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.enmus.ExcelType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import shaded.org.apache.commons.lang3.StringUtils;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.web.annotation.EnableListRequest;
import xyz.icrab.common.web.annotation.EnablePageRequest;
import xyz.icrab.common.web.annotation.PermissionMode;
import xyz.icrab.common.web.annotation.Session;
import xyz.icrab.common.web.param.IdsParam;
import xyz.icrab.ums.util.consts.SessionKeys;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author SUN
 * @since 2018/7/6 14:22
 */
@Controller
@RequestMapping("/people")
@EnablePageRequest
@EnableListRequest
public class PeopleInfoController {


@Autowired
private PeopleInfoService peopleInfoService;

    /**
     * @author tzj
     * @description 顾客信息
     * @date 2019/9/24 14:44
     * @return
     */

    @RequestMapping(value = "nghPeople")
    @ResponseBody
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> nghPeople(@RequestBody(required = false) Map<String, Object> params, Pagination pagination) {
        if (params.get("activityId") ==null ||  params.get("activityId") ==""){
            return Result.ok();
        }
        Pageable<LAPeople> list =   peopleInfoService.nghPeople(params,pagination);
        return Result.ok(list);
    }



    /**
     * 通过id查询商品的详细信息
     */
    @RequestMapping("/get")
    @ResponseBody
    public   Result<?>  getMe(@RequestBody(required = false) Map<String, Object> params){
        if(params==null){
            params = new HashedMap();
        }
        List<LAPeople>  laPeople =  peopleInfoService.queryInfo(params);
        return  Result.ok(laPeople.get(0));
    }


    /**
     * @author tzj
     * @description 秒杀商品表更新价格
     * @date 2019/9/24 14:44
     * @return
     */

    @RequestMapping(value = "upPeople")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?>  upPeople(@RequestBody(required = false) Map<String, Object> params) {
        peopleInfoService.upPeople(params);
        return Result.ok();
    }


    /**
     * @author tzj
     * @description 内购会信息导出
     * @date 2019/9/24 21:23
     * @return
    */
    @RequestMapping("/exportPeople")
    public void exportTicket(HttpServletRequest req, HttpServletResponse rep) throws IOException {
        Map<String, Object> params = new HashMap<>();
        params.put("activityId", (String) req.getParameter("activityId"));
        if (req.getParameter("state") != null && req.getParameter("state") != "") {
            params.put("state", req.getParameter("state"));
        }

        List<PeopleExr> result = peopleInfoService.exportPeople(params);

        OutputStream os = rep.getOutputStream();

        rep.setContentType("application/x-download");
        rep.setHeader("Content-Disposition",
                "attachment;filename=" + DateFormatUtils.format(new Date(), "yyyyMMddHHmmssSSS") + ".xlsx");
        ExportParams param = new ExportParams("内购会人员信息", "内购会人员信息");
        param.setAddIndex(true);
        param.setType(ExcelType.XSSF);
        Workbook excel = ExcelExportUtil.exportExcel(param, PeopleExr.class, result);
        excel.write(os);
        os.flush();
        os.close();
    }


    /**
     * @author tzj
     * @description  修改顾客信息
     * @date 2019/9/24 22:28
     * @return
    */
    @RequestMapping("/updateInfo")
    @ResponseBody
    public   Result<?>  updateInfo(@RequestBody(required = false) Map<String, Object> params){
        peopleInfoService.upPeople(params);
        return  Result.ok();
    }





}
