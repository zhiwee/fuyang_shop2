package com.zhiwee.gree.webapi.Controller.order;

import com.alibaba.fastjson.JSON;
import com.zhiwee.gree.model.GoodsInfo.GoodVo.ShopCartGoodsVo;
import com.zhiwee.gree.model.GoodsInfo.Goods;
import com.zhiwee.gree.model.GoodsInfo.ShopCartGoods;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.order.BaseOrder;
import com.zhiwee.gree.service.GoodsInfo.GoodsService;
import com.zhiwee.gree.webapi.util.DateUtil;
import com.zhiwee.gree.webapi.util.JsonUtil;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessagePostProcessor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.util.IdGenerator;
import xyz.icrab.common.web.annotation.EnableDeleteRequest;
import xyz.icrab.common.web.annotation.EnableGetRequest;
import xyz.icrab.common.web.annotation.EnableListRequest;
import xyz.icrab.common.web.annotation.EnablePageRequest;
import xyz.icrab.common.web.session.Session;
import xyz.icrab.common.web.util.SessionUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.*;


/**
 * @Author: jick
 * @Date: 2019/10/24 18:36
 */
@RestController
@RequestMapping("/userOrder")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class UserOrderController  {


    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;


    @Autowired
    private GoodsService  goodsService;


    /**
     * @Author: jick
     * @Date: 2019/10/24 18:36
     * 创建订单，生成预订单信息
     */
    @RequestMapping("/createOrder")
    public Result<?> createOrder(@RequestBody BaseOrder baseOrder, HttpServletRequest request, HttpServletResponse response){


        Session session = SessionUtils.get(request);
        ShopUser user = null;

        if(session  !=null){
            user = (ShopUser) session.getAttribute("user");
            Date   date  =  new Date();
            //主键
            baseOrder.setId(IdGenerator.uuid());
            //订单号
              baseOrder.setOrderNum(DateUtil.date2Str(date));
            //支付人id
            baseOrder.setPayerId(user.getId());
           //订单生成时间
            baseOrder.setCreateTime(date);
            //状态,1表示未支付
            baseOrder.setState(1);
            //积分
//            baseOrder.setIntegral((baseOrder.getRealPay().divide(new BigDecimal(100))).doubleValue());
            //订单类型  1 普通订单 2分销订单
            baseOrder.setType(1);
            //将订单信息存入至redis中
            //登录存redis
            HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
            Map<String,String>  resultMap  = new HashMap<>();

            // 重新添加至map
            resultMap.put(baseOrder.getOrderNum(), JsonUtil.object2JsonStr(baseOrder));
            // 添加至redis
            hashOperations.putAll("userOrder:"+user.getId() , resultMap);

            //发送消息值消息队列,用来监控失效消息。
            sendDelayMessage(baseOrder);
        }
        return   Result.ok();
    }

    /**
     * @Author: jick
     * @Date: 2019/10/25 11:44
     *  发送订单信息到消息队列
     */
    public void sendDelayMessage(BaseOrder baseOrder){
        rabbitTemplate.convertAndSend(
                "exchange.delay.order.begin",
                "delay",
                JSON.toJSONString(baseOrder),
                new MessagePostProcessor() {
                    @Override
                    public Message postProcessMessage(Message message) throws AmqpException {
                        //消息有效期30分钟
                        //message.getMessageProperties().setExpiration(String.valueOf(1800000));
                        message.getMessageProperties().setExpiration(String.valueOf(1800000));
                        return message;
                    }
                });
    }

    /**
     * @Author: jick
     * @Date: 2019/10/25 16:28
     * 记录需要支付的商品信息
     */
    @RequestMapping("/orderGoodsInfo")
    public   Result<?>  orderGoodsInfo(@RequestBody Map<String,Object>  param,HttpServletRequest request, HttpServletResponse response){
   // public   Result<?>  orderGoodsInfo(@RequestBody String id ,HttpServletRequest request, HttpServletResponse response){
        Session session = SessionUtils.get(request);
        ShopUser user = null;

        BigDecimal allPayPrice =  new BigDecimal(0.0);//需要支付金额
        ShopCartGoodsVo  shopCartGoodsVo  =  new ShopCartGoodsVo();

        //定义集合用来存放从购物车中国拿出来的数据
        List<ShopCartGoods>  list  =   new ArrayList<>();

        if (session != null) {
            user = (ShopUser) session.getAttribute("ShopUser");


            //登录存redis
            HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
            // 先查询
            Map<String, String> resultMap = hashOperations.entries("userCart:"+user.getId());
           // Map<String, String> resultMap = hashOperations.entries("userCart:5d257e46155f825ff4e287b9");
            Map<String,String>  goodsMap =  new HashMap<>();
            // 根据map的key获取value
            //对获取到的key值进行切割
            String[] strs = ((String)param.get("id")).split(",");
           // String[] strs = id.split(",");
            //循环获取购物车中的商品信息
            if(null !=strs && strs.length>0){
                for(String  str:strs){
                    String  shopGoods = resultMap.get(str);
                    //转换成原始数据
                    ShopCartGoods  shopCartGoods  =   JsonUtil.jsonStr2Object(shopGoods, ShopCartGoods.class);

                    //判断库存
                    Goods  goods  =  goodsService.get(shopCartGoods.getGoodsId());

                    if(goods.getStoreCount()-shopCartGoods.getCount()<0){
                        return  Result.of(Status.ClientError.BAD_REQUEST, shopCartGoods.getName()+"库存不足");
                    }
                    //库存更新
                    goods.setStoreCount(goods.getStoreCount()-shopCartGoods.getCount());
                    //销量更新
                    goods.setSaleCount(goods.getSaleCount()+shopCartGoods.getCount());


                    // 重新添加至map
                    goodsMap.put(shopCartGoods.getGoodsId(), JsonUtil.object2JsonStr(shopCartGoods));
                    // 添加至redis
                    hashOperations.putAll("orderGoods:"+user.getId() , goodsMap);
                  //  hashOperations.putAll("orderGoods:5d257e46155f825ff4e287b9", goodsMap);

                    //删除购物车里面对应的商品信息
                    hashOperations.delete("userCart:"+user.getId(),str);

                    //添加至新的集合
                    list.add(shopCartGoods);

                    allPayPrice = allPayPrice.add(shopCartGoods.getTotalPrice());
                }

                shopCartGoodsVo.setCartGoodsList(list);
                shopCartGoodsVo.setAllPrice(allPayPrice);

            }

        }
        return   Result.ok(shopCartGoodsVo);
    }


}
