package com.zhiwee.gree.webapi.Controller.sms;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.util.sms.SMSUtils;
import xyz.icrab.common.web.annotation.PermissionMode;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Description: 短信
 *
 * @author: sun
 * @Date 上午10:08 2019/5/12
 * @param:
 * @return:
 */
@RestController
@RequestMapping("/sms")
public class SmsController {

    private static final ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() * 2);

    static {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            executor.shutdown();
        }));
    }

    @Value("${sms.captcha.timeout}")
    private int captchatTimeout;

    @Value("${sms.captcha.templateCode}")
    private String captchaTemplateCode;

    @Autowired
    private RedisTemplate redisClient;

    @Autowired
    private SMSUtils smsUtils;

    @RequestMapping("/sendcaptcha")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> sendCaptcha(@RequestBody Map<String, Object> param, HttpServletRequest request, HttpServletResponse response) {
        String mobile = (String) param.get("mobile");

        if (mobile == null || mobile.equals("")) {
            return Result.of(Status.ClientError.BAD_REQUEST, "手机号码不合法");
        }

        String captcha = RandomStringUtils.randomNumeric(6);//生成随机二维码
        String key = getCaptchaKey(mobile);
        redisClient.opsForValue().set(key, captcha, captchatTimeout, TimeUnit.SECONDS);
        Map<String, String> map = new HashMap<>();
        map.put("captcha", captcha);
        map.put("code", captcha);
        executor.submit(() -> {
            smsUtils.sendSMS(mobile, captchaTemplateCode, map);
        });//发送短信

        return Result.ok();
    }

    @RequestMapping("/checkcaptcha")
    @PermissionMode(PermissionMode.Mode.White)
    public Object checkCaptcha(@RequestBody Map<String, Object> param) {
        String mobile = (String) param.get("mobile");
        String captcha = (String) param.get("captcha");
        if (StringUtils.isAnyBlank(mobile, captcha)) {

            return Result.of(Status.ClientError.BAD_REQUEST, "验证码不正确");
        }

        String key = getCaptchaKey(mobile);
        String result = (String) redisClient.opsForValue().get(key);
        if (result == null || !result.equals(captcha)) {
            return Result.of(Status.ClientError.FORBIDDEN, "验证码错误或已经过期");
        }

        return Result.ok(true);
    }

    private String getCaptchaKey(String mobile) {
        return "captcha:phone:active:" + mobile;
    }

}
