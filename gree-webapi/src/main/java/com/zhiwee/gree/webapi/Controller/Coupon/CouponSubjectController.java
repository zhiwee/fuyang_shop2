package com.zhiwee.gree.webapi.Controller.Coupon;


import com.zhiwee.gree.model.Coupon.CouponSubject;
import com.zhiwee.gree.model.Coupon.LShareSubject;
import com.zhiwee.gree.service.Coupon.CouponService;
import com.zhiwee.gree.service.Coupon.CouponSubjectService;
import com.zhiwee.gree.service.Coupon.LShareSubjectService;
import com.zhiwee.gree.webapi.util.SystemControllerLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.util.KeyUtils;
import xyz.icrab.common.web.annotation.*;
import xyz.icrab.common.web.param.IdParam;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.security.Key;
import java.util.*;

/**
 * @author tzj
 * @description
 * @date 2019/9/25 20:37
*/


@RestController
@RequestMapping("/couponSubject")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class CouponSubjectController {

    @Resource
    private CouponSubjectService couponSubjectService;

  @Resource
  private CouponService couponService;
    @Resource
    private LShareSubjectService lShareSubjectService;
    /**
     * @author tzj
     * @description 查询专题
     * @date 2019/9/25 20:38
     * @return
    */

    @SystemControllerLog(description = "查询所有代金券")
    @RequestMapping({"/page"})
    @PermissionMode(PermissionMode.Mode.White)
    public Result<Pageable<CouponSubject>> page(@RequestBody(required = false) Map<String, Object> param, Pagination pagination) {
        return Result.ok(couponSubjectService.pageInfo(param, pagination));
    }


    /**
     * @author tzj
     * @description  删除专题
     * @date 2019/9/25 23:23
     * @return
    */
    @SystemControllerLog(description = "删除专题")
    @RequestMapping({"/deleteCoupon"})
    @ResponseBody
    public Result<?> deleteCoupon(@RequestBody @Valid Map<String, String> param) {
        String id = param.get("orderId");

        //物理删除所有券
        String idp[]=id.split(",");
           Map<String,Object> delId=new HashMap<>();
           if (idp[0] ==null || idp[0] ==""){
               return Result.ok();
           }
           delId.put("subjectId",idp[0]);
           couponService.deleteList(delId);
        //物理删除
//        List<String> ids = Arrays.asList(id.split(","));
//        couponSubjectService.delete(ids);
        CouponSubject couponSubject=new CouponSubject();
        couponSubject.setId(idp[0]);
        couponSubject.setState(3);
        couponSubjectService.update(couponSubject);
        return Result.ok();
    }



    /**
     * @author tzj
     * @description 启用主题
     * @date 2019/9/26 9:13
     * @return
    */
    @RequestMapping("/open")
    @ResponseBody
    public Result<?> open(@RequestBody @Valid IdParam map) {
        CouponSubject couponSubject = new CouponSubject();
        couponSubject.setId(map.getId());
        couponSubject.setState(1);

        Map<String,Object> param=new HashMap<>();
        param.put("subjectId",map.getId());
        param.put("state",1);

        couponService.updateAll(param);
        couponSubjectService.update(couponSubject);

        return Result.ok();
    }


    /**
     * @author tzj
     * @date 2019/9/26 9:13
     */
    @RequestMapping("/addShare")
    @ResponseBody
    public Result<?> addShare(@RequestBody LShareSubject lShareSubject) {

        List<LShareSubject> list = lShareSubjectService.list();
        if (list !=null&&list.size()>0){
            List<String> keys=new ArrayList<>(100);
            for (LShareSubject shareSubject : list) {
                keys.add(shareSubject.getId());
            }
            lShareSubjectService.delete(keys);
        }
        lShareSubject.setId(KeyUtils.getKey());
        lShareSubject.setState(1);
        lShareSubjectService.save(lShareSubject);
        return Result.ok();
    }

    /**
     * @author tzj
     * @date 2019/9/26 9:13
     */
    @RequestMapping("/getShare")
    @ResponseBody
    public Result<?> getShare() {
        List<LShareSubject> list = lShareSubjectService.list();
        if (list !=null&&list.size()>0) {
            return Result.ok(list.get(0));
        }else {
            return Result.ok();
        }

    }




    /**
     * @author tzj
     * @description 禁用主题
     * @date 2019/9/26 9:14
     * @return
    */
    @RequestMapping("/stop")
    @ResponseBody
    public Result<?> stop(@RequestBody @Valid IdParam map) {
        CouponSubject couponSubject = new CouponSubject();
        couponSubject.setId(map.getId());
        couponSubject.setState(2);

        Map<String,Object> param=new HashMap<>();
        param.put("subjectId",map.getId());
        param.put("state",2);

        couponService.updateAll(param);
        couponSubjectService.update(couponSubject);
        return Result.ok();
    }


    /**
     * @author tzj
     * @description 查询专题
     * @date 2019/9/26 17:08
     * @return
    */
    @SystemControllerLog(description = "查询专题")
    @RequestMapping({"/get"})
    @ResponseBody
    public Result<?> get(@RequestBody @Valid Map<String, String> param) {
        CouponSubject couponSubject = couponSubjectService.get(param.get("id"));
        return Result.ok(couponSubject);
    }





}
