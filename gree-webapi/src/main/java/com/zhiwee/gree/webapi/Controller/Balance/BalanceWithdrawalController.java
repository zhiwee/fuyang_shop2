package com.zhiwee.gree.webapi.Controller.Balance;

import com.zhiwee.gree.model.Balance.BalanceWithdrawal;
import com.zhiwee.gree.model.BaseInfo.BaseInfo;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.service.Balance.BalanceWithdrawalService;
import com.zhiwee.gree.service.BaseInfo.BaseInfoService;
import com.zhiwee.gree.service.ShopUser.ShopUserService;
import com.zhiwee.gree.webapi.util.ClientCustomSSL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.util.IdGenerator;
import xyz.icrab.common.web.annotation.*;
import xyz.icrab.common.web.param.IdParam;
import xyz.icrab.common.web.param.IdsParam;
import xyz.icrab.common.web.util.Validator;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author sun on 2019/6/24
 */
@RestController
@RequestMapping("/balanceWithdrawal")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class BalanceWithdrawalController {

    @Autowired
    private BalanceWithdrawalService balanceWithdrawalService;

    @Autowired
    private ShopUserService shopUserService;
   /**
    * Description: 查询所有的提现余额的记录
    * @author: sun
    * @Date 下午11:18 2019/6/24
    * @param:
    * @return:
    */
    @RequestMapping({"/page"})
    public Result<Pageable<BalanceWithdrawal>> page(@RequestBody(required = false) Map<String, Object> param, Pagination pagination) {
        return Result.ok(balanceWithdrawalService.pageInfo(param, pagination));
    }




  /**
   * Description: 微信申请体现充值金额
   * @author: sun
   * @Date 下午12:03 2019/6/25
   * @param:
   * @return:
   */
    @PermissionMode(PermissionMode.Mode.White)
    @RequestMapping("/applyMoney")
    public Result<?> applyMoney(@RequestBody BalanceWithdrawal balanceWithdrawal, @Session("ShopUser") ShopUser user) {
        Validator validator = new Validator();
        validator.notNull(balanceWithdrawal.getMoney(), "提现金额不能为空");
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }
        if (balanceWithdrawal.getMoney() <= 0) {
            return Result.of(Status.ClientError.BAD_REQUEST, "提现金额不能小于等于0");
        }
        ShopUser shopUser = shopUserService.get(user.getId());
        if (balanceWithdrawal.getMoney() > shopUser.getBalance()) {
            return Result.of(Status.ClientError.BAD_REQUEST, "提现金额不能大于账户余额");
        }
        balanceWithdrawal.setId(IdGenerator.objectId());
        balanceWithdrawal.setUserId(user.getId());
        balanceWithdrawal.setCreateTime(new Date());
        balanceWithdrawal.setState(1);
        balanceWithdrawalService.save(balanceWithdrawal);
        return Result.ok();
    }




  /**
   * Description: 同意体现
   * @author: sun
   * @Date 下午12:11 2019/6/25
   * @param:
   * @return:
   */
    @RequestMapping("/open")
    @ResponseBody
    public Result<?> open(@RequestBody @Valid IdsParam map) {
        List<String> ids = map.getIds();
//        BalanceWithdrawal balanceWithdrawal = null;
        for (String id : ids) {
            BalanceWithdrawal balanceWithdrawal = balanceWithdrawalService.get(id);

            ShopUser shopUser = shopUserService.get(balanceWithdrawal.getUserId());
            if(balanceWithdrawal.getMoney() > shopUser.getBalance()){
                return Result.of(Status.ClientError.BAD_REQUEST, "提现金额不能大于账户余额");
            }
            balanceWithdrawal.setId(id);
            balanceWithdrawal.setState(2);
            Map params = ClientCustomSSL.withdrawal(balanceWithdrawal.getId(),shopUser.getOpenId(),balanceWithdrawal.getMoney());
            if(params.get("status").equals("SUCCESS")){
                shopUser.setBalance(BigDecimal.valueOf(shopUser.getBalance()).subtract(BigDecimal.valueOf(balanceWithdrawal.getMoney())).doubleValue());
                shopUserService.update(shopUser);
                balanceWithdrawalService.update(balanceWithdrawal);
            }else{
                return Result.of(Status.ClientError.BAD_REQUEST, params.get("msg"));
            }

        }

        return Result.ok();
    }

/**
 * Description: 拒绝体现
 * @author: sun
 * @Date 下午12:10 2019/6/25
 * @param:
 * @return:
 */
    @RequestMapping("/stop")
    @ResponseBody
    public Result<?> stop(@RequestBody @Valid IdsParam map) {
        List<String> ids = map.getIds();
        BalanceWithdrawal balanceWithdrawal = new BalanceWithdrawal();
        for (String id : ids) {
            balanceWithdrawal.setId(id);
            balanceWithdrawal.setState(3);
            balanceWithdrawalService.update(balanceWithdrawal);
        }

        return Result.ok();
    }



    /**
     * Description: 获取个人体现记录
     * @author: sun
     * @Date 下午11:19 2019/6/24
     * @param:
     * @return:
     */
    @RequestMapping("/balanceRecord")
    public Result<?> page(@Session("ShopUser") ShopUser user) {
        Map<String,Object> params = new HashMap<>();
        params.put("userId",user.getId());
        List<BalanceWithdrawal> list = balanceWithdrawalService.balanceRecord(params);
        return Result.ok(list);


    }



}
