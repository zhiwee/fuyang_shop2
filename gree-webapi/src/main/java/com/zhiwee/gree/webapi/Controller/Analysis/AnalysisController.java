package com.zhiwee.gree.webapi.Controller.Analysis;

import com.zhiwee.gree.model.Activity.ActivityPage;
import com.zhiwee.gree.model.Analysis.ItemAnalysis;
import com.zhiwee.gree.model.User;
import com.zhiwee.gree.service.Activity.ActivityPageService;
import com.zhiwee.gree.service.OrderInfo.OrderItemService;
import com.zhiwee.gree.service.OrderInfo.OrderService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.util.IdGenerator;
import xyz.icrab.common.web.annotation.EnableListRequest;
import xyz.icrab.common.web.annotation.EnablePageRequest;
import xyz.icrab.common.web.annotation.PermissionMode;
import xyz.icrab.common.web.annotation.Session;
import xyz.icrab.common.web.param.IdParam;
import xyz.icrab.common.web.param.IdsParam;
import xyz.icrab.common.web.util.Validator;
import xyz.icrab.ums.util.consts.SessionKeys;

import javax.validation.Valid;
import java.util.*;

/**
 * @author SUN
 * @since 2019/7/6 14:22
 */
@RestController
@RequestMapping("/analysis")
public class AnalysisController {

   
    @Autowired
    private OrderItemService orderItemService;



    /**
     * Description: 售出商品数量统计
     * @author: sun
     * @Date 下午2:11 2019/7/16
     * @param:
     * @return:
     */
    @RequestMapping({"/itemAnalysis"})
    public Result<?> page(@RequestBody(required = false) Map<String, Object> param) {
        if(param == null){
            param = new HashMap<>();
        }
        Map<String, Object> retailNature = new HashMap<>();
        Map<String, Object> engineerAmount= new HashMap<>();
        Map<String, Object> allAmount= new HashMap<>();
//        if (StringUtils.isNotBlank((String) param.get("endCreateTime"))) {
//            retailNature.put("endCreateTime", (String) param.get("endCreateTime") + "23:59:59");
//            engineerAmount.put("endCreateTime", (String) param.get("endCreateTime") + "23:59:59");
//            allAmount.put("endCreateTime", (String) param.get("endCreateTime") + "23:59:59");
//        }


        List<ItemAnalysis> itemAnalysisList = new ArrayList<>();
        /**零售自然量
         * 1支付宝 2 微信 3银联 4 转账
         */
        retailNature.put("payType","4");
        retailNature.put("startCreateTime",param.get("startCreateTime"));
        retailNature.put("endCreateTime",param.get("endCreateTime"));
        ItemAnalysis retailNatureAnalysis = orderItemService.itemAnalysis(retailNature);
        if(retailNatureAnalysis != null){
            retailNatureAnalysis.setBusinessType("零售自然量");
            itemAnalysisList.add(retailNatureAnalysis);
        }


        /**家用工程
         * 1商城正常下单 2线下活动下单 3分销订单 4 工程团购订单
         */
        engineerAmount.put("isOffline","4");
        engineerAmount.put("startCreateTime",param.get("startCreateTime"));
        engineerAmount.put("endCreateTime",param.get("endCreateTime"));
        ItemAnalysis engineerAnalysis = orderItemService.itemAnalysis(engineerAmount);
        if(engineerAnalysis != null){
            engineerAnalysis.setBusinessType("家用工程");
            itemAnalysisList.add(engineerAnalysis);
        }



        /**
         * 全部订单
         *
         */
        allAmount.put("startCreateTime",param.get("startCreateTime"));
        allAmount.put("endCreateTime",param.get("endCreateTime"));
        ItemAnalysis allAnalysis = orderItemService.itemAnalysis(allAmount);
        if(allAnalysis != null){
            allAnalysis.setBusinessType("家用零售");
            itemAnalysisList.add(allAnalysis);
        }

        return Result.ok(itemAnalysisList);
    }


}
