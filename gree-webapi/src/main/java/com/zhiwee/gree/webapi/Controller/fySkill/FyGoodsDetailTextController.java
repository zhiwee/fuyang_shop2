package com.zhiwee.gree.webapi.Controller.fySkill;

import com.zhiwee.gree.model.FyGoods.FyGoodsDetailText;
import com.zhiwee.gree.model.resultBase.BaseResult;
import com.zhiwee.gree.service.FyGoods.FyGoodsDetailTextService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.util.IdGenerator;
import xyz.icrab.common.web.annotation.EnableDeleteRequest;
import xyz.icrab.common.web.annotation.EnableGetRequest;
import xyz.icrab.common.web.annotation.EnableListRequest;
import xyz.icrab.common.web.annotation.EnablePageRequest;

@RestController
@RequestMapping("/skillgoodsDetailText")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class FyGoodsDetailTextController {


    @Autowired
    private FyGoodsDetailTextService  fyGoodsDetailTextService;


    /**
     * @Author: jick
     * @Date: 2019/12/11 12:54
     * 商品详情存入数据库
     */
    @RequestMapping("/saveOrUpdate")
    public Result<?> goodsItemTextSave(@RequestBody FyGoodsDetailText  fyGoodsDetailText){

        FyGoodsDetailText f = new FyGoodsDetailText();
        f.setGoodsId(fyGoodsDetailText.getGoodsId());
        //判断数据库里面有没有数据，如果有是更新操作，没有是添加操作
        FyGoodsDetailText  fyGoodsDetailText1  =   fyGoodsDetailTextService.getOne(f);

        if(null !=fyGoodsDetailText1){
            fyGoodsDetailText.setId(fyGoodsDetailText1.getId());
            fyGoodsDetailTextService.update(fyGoodsDetailText);
        }else {
            fyGoodsDetailText.setId(IdGenerator.uuid());
            fyGoodsDetailTextService.save(fyGoodsDetailText);
        }

        return  Result.ok()  ;
    }


    /**
     * @Author: jick
     * @Date: 2019/12/11 16:51
     * 更具商品的id查询商品的详情信息
     */

    @RequestMapping("/get")
     public  Result<?>  getGoodsItem(@RequestBody FyGoodsDetailText  fyGoodsDetailText){

        FyGoodsDetailText f = new FyGoodsDetailText();
        f.setGoodsId(fyGoodsDetailText.getGoodsId());
        FyGoodsDetailText  getInfo  =   fyGoodsDetailTextService.getOne(f);
           return   Result.ok(getInfo);

     }



}
