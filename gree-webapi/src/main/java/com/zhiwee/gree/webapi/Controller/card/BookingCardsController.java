package com.zhiwee.gree.webapi.Controller.card;


import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.WxPay.WeChatConfig;
import com.zhiwee.gree.model.card.*;
import com.zhiwee.gree.service.ShopUser.ShopUserService;
import com.zhiwee.gree.service.card.BookingCardService;
import com.zhiwee.gree.service.card.BookingCardUserService;
import com.zhiwee.gree.service.card.BookingCardsService;
import com.zhiwee.gree.webapi.Controller.Wxpay.PayUtil;
import com.zhiwee.gree.webapi.Controller.Wxpay.WXPay2;
import com.zhiwee.gree.webapi.util.*;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.jeecgframework.poi.excel.ExcelExportUtil;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.enmus.ExcelType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.util.KeyUtils;
import xyz.icrab.common.web.annotation.EnableGetRequest;
import xyz.icrab.common.web.annotation.EnableListRequest;
import xyz.icrab.common.web.annotation.PermissionMode;
import xyz.icrab.common.web.annotation.Session;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

@RequestMapping("/bookingCards")
@Controller
@ResponseBody
@EnableListRequest
@EnableGetRequest
public class BookingCardsController {
    @Autowired
    private BookingCardService bookingCardService;
    @Autowired
    private BookingCardsService bookingCardsService;
    @Autowired
    private BookingCardUserService bookingCardUserService;
    @Autowired
    private ShopUserService shopUserService;
    @Value("${ZHENCANGKA_URL}")
    private String cardUrl;
    /**
     * @return
     * @author cs
     * @description 已购预售卡页面
     * @date 2020/02/14 15:55
     */
    @RequestMapping(value = "/infoPage")
    public Result<?> infoPage(@RequestBody Map<String, Object> params, Pagination pagination) {
        if (params == null) {
            params = new HashMap<>();
        }
        if (!"".equals((String) params.get("startCreateTime"))) {
            params.put("startCreateTime", (String) params.get("startCreateTime") + " 00:00:00");
        }
        if (!"".equals((String) params.get("endCreateTime"))) {
            params.put("endCreateTime", (String) params.get("endCreateTime") + " 23:59:59");
        }
        Pageable<BookingCards> page = bookingCardsService.pageInfo(params, pagination);
        return Result.ok(page);
    }


    /**
     * Description: 购卡信息导出
     *
     * @Date 下午4:36 2019/12/12
     * @param:
     * @return:
     */
    @RequestMapping("/exportUserCards")
    public void export(HttpServletRequest req, HttpServletResponse rsp) throws IOException {
        Map<String, Object> params = new HashMap<>(10);
        params.put("startCreateTime", req.getParameter("startCreateTime"));
        params.put("endCreateTime", req.getParameter("endCreateTime"));
        params.put("userName", req.getParameter("userName"));
        params.put("phone", req.getParameter("phone"));
        params.put("state", req.getParameter("state"));

        if (StringUtils.isNotBlank((String) params.get("startCreateTime"))) {
            params.put("startCreateTime", (String) params.get("startCreateTime") + " 00:00:00");
        }
        if (StringUtils.isNotBlank((String) params.get("endCreateTime"))) {
            params.put("endCreateTime", (String) params.get("endCreateTime") + " 23:59:59");
        }

        List<BookingCardsExrt> result;
        result = bookingCardsService.queryBookingCardsExport(params);

        // 导出Excel
        OutputStream os = rsp.getOutputStream();

        rsp.setContentType("application/x-download");
        // 设置导出文件名称
        rsp.setHeader("Content-Disposition",
                "attachment;filename=" + DateFormatUtils.format(new Date(), "yyyyMMddHHmmssSSS") + ".xlsx");
        ExportParams param = new ExportParams("预售卡购卡信息", "预售卡信息", "购卡信息表");
        param.setAddIndex(true);
        param.setType(ExcelType.XSSF);
        Workbook excel = ExcelExportUtil.exportExcel(param, BookingCardsExrt.class, result);
        excel.write(os);
        os.flush();
        os.close();
    }

    @RequestMapping("/backbookingcard")
    @ResponseBody
    public Result<?> delete(@RequestBody BookingCards bookingCards) {
        bookingCards = bookingCardsService.get(bookingCards.getId());
        if (bookingCards.getState() == 1||bookingCards.getState() ==3) {
            return Result.ok(refundCard(bookingCards));
        } else {
            return Result.of(Status.ClientError.FORBIDDEN, "已支付或者领取由状态才能进行退卡");
        }
    }

    /**
     * @author cs
     * @description 小程序用户的已购预售卡
     * @date 2020/02/14 09:33
     */
    @RequestMapping(value = "/myCards")
    public Result<?> myCards(@Session("ShopUser") ShopUser user) {
        Map<String, Object> qParam = new HashMap<>();
        qParam.put("userId", user.getId());
        List<BookingCards> bookingCardsList = bookingCardsService.list(qParam);

        bookingCardsList.removeIf(next -> next.getState() == -1 || next.getState() == 0 || next.getState() == 2);
        for (BookingCards bookingCards : bookingCardsList) {
            BookingCard bookingCard = bookingCardService.get(bookingCards.getBookingCardId());
            bookingCards.setBookingCard(bookingCard);
        }
        return Result.ok(bookingCardsList);
    }

    /**
     * 产生随机字符串
     *
     * @param length 要求字符串的长度
     * @return
     */
    public static String getRandomString(int length) {
        String str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < length; i++) {
            int number = random.nextInt(62);
            sb.append(str.charAt(number));
        }
        return sb.toString();
    }

    /**
     * @author cs
     * @description 用户购买预售卡
     * @date 2020/02/13 16:36
     */
    @RequestMapping(value = "/buyCard")
    public Result<?> buyCard(@Session("ShopUser") ShopUser user, @RequestBody Map<String, Object> params) throws Exception {
        user=  shopUserService.get(user.getId());
        if (user.getPhone() == null || "".equals(user.getPhone())) {
            return Result.of(Status.ClientError.FORBIDDEN, "请您先绑定手机号码");
        }

        //判断是否已经购买
        Map<String, Object> qParam = new HashMap<>(5);
        qParam.put("userId", user.getId());
        qParam.put("bookingCardId", params.get("bookingcardid"));
        BookingCards cards = null;
        List<BookingCards> bookingCardsList = bookingCardsService.list(qParam);
        //找到购买总量
        Map<String, Object> qParam2 = new HashMap<>(5);
        qParam2.put("bookingCardId", params.get("bookingcardid"));
        qParam2.put("state", 1);
        qParam2.put("state2", 3);
        List<BookingCards> bookingCardsList2 = bookingCardsService.listBuy(qParam2);

        // 找到预售卡实际抵用金额，和可以购买次数。
        BookingCard bookingCard = new BookingCard();
        bookingCard.setId((String) params.get("bookingcardid"));

        bookingCard = bookingCardService.getOne(bookingCard);
        if (bookingCard==null){
            return Result.of(Status.ClientError.BAD_REQUEST, "该卡已经售完了");
        }
        if (bookingCard.getStartTime().after(new Date())){
            return Result.of(Status.ClientError.BAD_REQUEST, "该卡还未开始出售");
        }
        if (bookingCard.getEndTime().before(new Date())){
            return Result.of(Status.ClientError.BAD_REQUEST, "该卡已经结束售卖了");
        }
        Integer numbers = bookingCard.getNumbers();
            //是否超过总量限制
        if (bookingCardsList2.size()>=bookingCard.getLimitNum()){
            return Result.of(Status.ClientError.BAD_REQUEST, "该卡已经售完了");
        }
        //找到购卡有效次数
        Integer min = 0;

        if (!bookingCardsList.isEmpty()) {
            for (BookingCards c : bookingCardsList) {
                if (c.getState() == 1||c.getState() == 3) {
                    min = min + 1;
                } else if (c.getState() == 0) {
                    if ((!c.getPrepayId().isEmpty()) && (System.currentTimeMillis() - c.getCreatetime().getTime()) / 6000 <= 110) {
                        cards = c;
                    } else {
                        //更新超时订单
                        c.setState(-1);
                        bookingCardsService.update(c);
                    }
                }
            }
        }
        //判断是否超过限购数量
        if (min >= numbers) {
            return Result.of(Status.ClientError.BAD_REQUEST, "您已购买过该预售卡" + numbers + "次！");
        }
        if (cards == null) {
            //没有未支付的订单
            qParam.clear();
            qParam.put("id", params.get("bookingcardid"));
            BookingCard card = bookingCardService.getOne(qParam);

            cards = new BookingCards();
            cards.setState(0);
            cards.setFyCode((String) params.get("fyCode"));
            cards.setId(KeyUtils.getKey());
            cards.setPhone(user.getPhone());
            cards.setUserName(user.getNickname());
            cards.setUserId(user.getId());
            cards.setOpenId(user.getOpenId());
            cards.setBookingCardId(card.getId());
            cards.setBookingMoney(card.getBookingMoney());
            cards.setRealityMoney(card.getRealityMoney());
            cards.setTotalFee(String.valueOf(card.getBookingMoney() * 100).replace(".0", ""));
            cards.setGoodName("阜阳商厦预售卡");
            cards.setGoodDetail(card.getName());
            cards.setCreatetime(new Date());
            cards.setOutTradeNo(KeyUtils.getKey());
            cards.setCreateIp("127.0.0.1");
            //组和生成签名
            SortedMap<String, String> signInfoMap = new TreeMap<>();
            signInfoMap.put("appid", WeChatConfig.APPIDFuYang);
            signInfoMap.put("mch_id", WeChatConfig.MCHIDFuYang);
            signInfoMap.put("nonce_str", getRandomString(32));
            signInfoMap.put("notify_url", WeChatConfig.WECHAT_NOTIFYURLBookingCard);
            signInfoMap.put("trade_type", WeChatConfig.Pay);
            signInfoMap.put("openid", cards.getOpenId());
            signInfoMap.put("body", cards.getGoodName());
            signInfoMap.put("detail", cards.getGoodDetail());
            signInfoMap.put("out_trade_no", cards.getOutTradeNo());
            signInfoMap.put("total_fee", cards.getTotalFee());
            signInfoMap.put("spbill_create_ip", cards.getCreateIp());
            signInfoMap.put("sign", WXPayUtil.createSign(signInfoMap, WeChatConfig.APIKEYFuYang));

            String payXml = WXPayUtil.mapToXml(signInfoMap);
            //请求统一下单
            String wxResultStr = HttpUtil.doPost(WeChatConfig.pay_url, payXml, 4000);
            if (null == wxResultStr) {
                return Result.of(Status.ClientError.BAD_REQUEST, "请求微信统一下单超时");
            }

            Map<String, String> wxReturnMap = WXPayUtil.xmlToMap(wxResultStr);
            if (!"SUCCESS".equals(wxReturnMap.get("return_code"))) {
                return Result.of(Status.ClientError.BAD_REQUEST, "微信下单失败：" + wxReturnMap.get("return_msg"));
            } else if (!"SUCCESS".equals(wxReturnMap.get("result_code"))) {
                return Result.of(Status.ClientError.BAD_REQUEST, "微信下单失败：" + wxReturnMap.get("err_code_des"));
            }
            cards.setPrepayId(wxReturnMap.get("prepay_id"));
            //TODO 添加数据库记录
            bookingCardsService.save(cards);
        }
        //返回给小程序所需的支付字段信息
        //调用微信支付流程
        Map<String, Object> response = new HashMap<String, Object>();

        SortedMap<String, String> signMap = new TreeMap<>();
        signMap.put("appId", WeChatConfig.APPIDFuYang);
        signMap.put("nonceStr", getRandomString(32));
        signMap.put("package", "prepay_id=" + cards.getPrepayId());
        signMap.put("signType", WeChatConfig.SIGNTYPE);
        signMap.put("timeStamp", String.valueOf(System.currentTimeMillis() / 1000));
        String signStr = WXPayUtil.createSign(signMap, WeChatConfig.APIKEYFuYang);

        response.put("nonceStr", signMap.get("nonceStr"));
        response.put("package", signMap.get("package"));
        response.put("timeStamp", signMap.get("timeStamp"));
        response.put("paySign", signStr);

        return Result.ok(response);
    }

    /**
     * TODO
     *
     * @param request
     * @param response
     * @throws Exception
     * @author cs
     * @description 微信支付--支付回调接口
     */
    @SuppressWarnings("all")
    @RequestMapping(value = "/wxPayNotify")
    @PermissionMode(PermissionMode.Mode.White)
//    @SystemControllerLog(description = "预售卡支付回调接口")
    public synchronized void wxNotify(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String orderId = null;
        BufferedReader br = new BufferedReader(new InputStreamReader((ServletInputStream) request.getInputStream()));
        String line = null;
        StringBuilder sb = new StringBuilder();
        while ((line = br.readLine()) != null) {
            sb.append(line);
        }
        br.close();
        //sb为微信返回的xml
        String notityXml = sb.toString();
        /**此处添加自己的业务逻辑代码end**/
        //通知微信服务器已经支付成功
        String resXml = "<xml><return_code><![CDATA[SUCCESS]]></return_code>" +
                "<return_msg><![CDATA[OK]]></return_msg></xml>";
        Map map = PayUtil.doXMLParse(notityXml);

        String returnCode = (String) map.get("return_code");
        if ("SUCCESS".equals(returnCode)) {
            //验证签名是否正确
            Map<String, Object> validParams = PayUtil.paraFilter(map);  //回调验签时需要去除sign和空值参数
            String validStr = PayUtil.createLinkString(validParams);//把数组所有元素，按照“参数=参数值”的模式用“&”字符拼接成字符串
            String sign = PayUtil.sign(validStr, WeChatConfig.APIKEYFuYang, "utf-8").toUpperCase();//拼装生成服务器端验证的签名
            //根据微信官网的介绍，此处不仅对回调的参数进行验签，还需要对返回的金额与系统订单的金额进行比对等
            if (sign.equals(map.get("sign"))) {
                /**此处添加自己的业务逻辑代码start**/
                //TODO 修改订单信息、切换卡状态
                Map<String, Object> selectMap = new HashMap<>();
                selectMap.put("outTradeNo", map.get("out_trade_no"));
                selectMap.put("state", 0);
                BookingCards bookingCards = bookingCardsService.getOne(selectMap);
                if (bookingCards != null) {
                    bookingCards.setState(1);
                    bookingCards.setPaytime(new SimpleDateFormat("yyyyMMddHHmmss").parse((String) map.get("time_end")));
                    bookingCards.setUpdatetime(new Date());
                    bookingCardsService.update(bookingCards);
                    //更新用户预售卡表记录总金额
//                    BookingCardUser bookingCardUser = new BookingCardUser();
//                    bookingCardUser.setUserId(bookingCards.getUserId());
//                    bookingCardUser.setBookingCardId(bookingCards.getBookingCardId());
//                    bookingCardUser = bookingCardUserService.getOne(bookingCardUser);
//                    //如果以前没有购买狗
//                    if (bookingCardUser == null) {
//                        BookingCardUser newOne = new BookingCardUser();
//                        newOne.setState(1);
//                        newOne.setUserId(bookingCards.getUserId());
//                        newOne.setBookingCardId(bookingCards.getBookingCardId());
//                        newOne.setBookingMoney(bookingCards.getBookingMoney());
//                        newOne.setId(KeyUtils.getKey());
//                        newOne.setCreatetime(new Date());
//                        newOne.setRealityMoney(bookingCards.getRealityMoney());
//                        bookingCardUserService.save(newOne);
//                    } else {
//                        //如果以前够购卡记录 更新
//                        Double bookingMoney = bookingCardUser.getBookingMoney();
//                        bookingCardUser.setBookingMoney(bookingMoney + bookingCards.getBookingMoney());
//                        Double realityMoney = bookingCardUser.getRealityMoney();
//                        bookingCardUser.setRealityMoney(realityMoney + bookingCards.getRealityMoney());
//                        bookingCardUser.setUpdatetime(new Date());
//                        bookingCardUserService.update(bookingCardUser);
//                    }
                }
            }

        } else {
            resXml = "<xml> \n" +
                    "  <return_code><![CDATA[FAIL]]></return_code>\n" +
                    "  <return_msg><![CDATA[支付回调失败]]></return_msg>\n" +
                    "</xml>";
        }

        BufferedOutputStream out = new BufferedOutputStream(
                response.getOutputStream());
        out.write(resXml.getBytes());
        out.flush();
        out.close();
    }


    /**
     * TODO
     *
     * @author cs
     * @description 退卡
     */
    public Result<?> refundCard(BookingCards bookingCards) {
//        Map<String, Object> map = new HashMap<>();
//        map.put("userId",user.getId());
//        map.put("bookingCardId",params.get("bookingcardid"));
//        map.put("state",1);
//        List<BookingCards> list = bookingCardsService.list(map);
        //找到用户购卡记录
//        BookingCardUser bookingCardUser=new BookingCardUser();
//        bookingCardUser.setUserId(user.getId());
//        bookingCardUser.setBookingCardId((String) params.get("bookingcardid"));
//        bookingCardUser= bookingCardUserService.getOne(bookingCardUser);
//        if (bookingCardUser==null){
//            return Result.of(Status.ClientError.BAD_REQUEST, "退卡错误请联系管理员");
//        }

//        BookingCards bookingCards = null;
//        if (list.isEmpty()){
//            return Result.of(Status.ClientError.BAD_REQUEST, "用户尚未购买该预售卡");
//        }else {
//            bookingCards = list.get(0);
//        }
//        if (bookingCards.getRealityMoney()<bookingCardUser.getRealityMoney()){
//            return Result.of(Status.ClientError.BAD_REQUEST, "已有消费金额无法退卡");
//        }

        //TODO 请求微信退款
        if (bookingCards.getOutRefundNo() == null || bookingCards.getOutRefundNo().isEmpty()) {
            bookingCards.setRefundFee(bookingCards.getTotalFee());
            bookingCards.setOutRefundNo(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
            bookingCardsService.update(bookingCards);
        }

        SortedMap<String, String> parameters = new TreeMap<>();
        parameters.put("appid", WeChatConfig.APPIDFuYang);
        parameters.put("mch_id", WeChatConfig.MCHIDFuYang);
        parameters.put("nonce_str", getRandomString(32));
        parameters.put("sign_type", "MD5");
        parameters.put("notify_url", WeChatConfig.WECHAT_NOTIFYURLBookingCardRefund);
        parameters.put("total_fee", bookingCards.getTotalFee());
        parameters.put("refund_fee", bookingCards.getRefundFee());
        parameters.put("out_trade_no", bookingCards.getOutTradeNo());
        parameters.put("out_refund_no", bookingCards.getOutRefundNo());
        parameters.put("sign", WXPayUtil.createSign(parameters, WeChatConfig.APIKEYFuYang));
        //附带双向证书的请求
        WxPayRefundConfig refundWxPayConfig = new WxPayRefundConfig();
        Map<String, String> returnMap = null;
        WXPay2 wxPay = new WXPay2(refundWxPayConfig);
        try {
            returnMap = wxPay.refund(parameters);
            //业务逻辑代码
            if ("SUCCESS".equals(returnMap.get("result_code"))) {
                return Result.ok("已申请退款");
            } else {
                return Result.of(Status.ClientError.BAD_REQUEST, "请求参数错误，申请退款失败");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Result.of(Status.ClientError.BAD_REQUEST, "发生错误，申请退款失败");
        }
    }


    /**
     * TODO
     *
     * @param request
     * @param response
     * @throws Exception
     * @author cs
     * @description 预售卡微信退款回调接口
     */
    @SuppressWarnings("all")
    @RequestMapping(value = "/wxPayRefundNotify")
    @PermissionMode(PermissionMode.Mode.White)
//    @SystemControllerLog(description = "预售卡退款回调接口")
    public synchronized void wxPayRefundNotify(HttpServletRequest request, HttpServletResponse response) throws Exception {
        InputStream inputStream;
        StringBuffer sb = new StringBuffer();
        inputStream = request.getInputStream();
        String s;
        BufferedReader in = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
        while ((s = in.readLine()) != null) {
            sb.append(s);
        }
        in.close();
        inputStream.close();

        String resXml = "";
        //解析xml成map
        Map<String, String> param = new HashMap<String, String>();
        param = XMLUtil.doXMLParse(sb.toString());
        if (param != null) {
            if (param.get("return_code").equals("SUCCESS")) {
                //退款成功
                //解密加密信息
                String deStr;
                deStr = WXPayUtil.decReqInfo(param.get("req_info"), WeChatConfig.APIKEYFuYang);
                param.clear();
                param = XMLUtil.doXMLParse(deStr);
                //更新退款信息
                if ("SUCCESS".equals((String) param.get("refund_status"))) {
                    Map<String, Object> params = new HashMap<>();
                    params.put("outRefundNo", param.get("out_refund_no"));
//                    params.put("state", 1);
                    BookingCards cards = bookingCardsService.getOne(params);
                    cards.setState(2);
                    cards.setCanceltime(new Date());
                    cards.setUpdatetime(cards.getCanceltime());
                    bookingCardsService.update(cards);

//                    //减少可用金额
//                    BookingCardUser bookingCardUser = new BookingCardUser();
//                    bookingCardUser.setUserId(cards.getUserId());
//                    bookingCardUser.setBookingCardId(cards.getBookingCardId());
//                    bookingCardUser = bookingCardUserService.getOne(bookingCardUser);
//                    if (bookingCardUser != null) {
//                        Double bookingMoney = bookingCardUser.getBookingMoney();
//                        bookingCardUser.setRealityMoney(bookingMoney - cards.getBookingMoney());
//                        Double realityMoney = bookingCardUser.getRealityMoney();
//                        bookingCardUser.setRealityMoney(realityMoney - cards.getRealityMoney());
//                        bookingCardUser.setUpdatetime(new Date());
//                        bookingCardUserService.update(bookingCardUser);
//                    }

                    //通知微信服务器已经退款成功
                    resXml = "<xml> \n" +
                            "  <return_code><![CDATA[SUCCESS]]></return_code>\n" +
                            "  <return_msg><![CDATA[OK]]></return_msg>\n" +
                            "</xml>";
                }
            } else {
                //通知微信服务器已经退款成功
                resXml = "<xml> \n" +
                        "  <return_code><![CDATA[FAIL]]></return_code>\n" +
                        "  <return_msg><![CDATA[退款回调失败]]></return_msg>\n" +
                        "</xml>";
            }
        }
        BufferedOutputStream out = new BufferedOutputStream(
                response.getOutputStream());
        out.write(resXml.getBytes());
        out.flush();
        out.close();
    }

    /**
     * 根据订单号查账单
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/orderquery")
    @PermissionMode(PermissionMode.Mode.White)
    public String orderquery(HttpServletRequest request) throws Exception {
        SortedMap<String, String> signInfoMap = new TreeMap<>();
        signInfoMap.put("appid", WeChatConfig.APPIDFuYang);
        signInfoMap.put("mch_id", WeChatConfig.MCHIDFuYang);
        signInfoMap.put("nonce_str", getRandomString(32));
        signInfoMap.put("out_trade_no",  request.getParameter("no"));
        signInfoMap.put("sign", WXPayUtil.createSign(signInfoMap, WeChatConfig.APIKEYFuYang));

        String payXml = WXPayUtil.mapToXml(signInfoMap);
        //请求统一下单
        String wxResultStr = HttpUtil.doPost("https://api.mch.weixin.qq.com/pay/orderquery", payXml, 4000);
        return wxResultStr;
    }


    /**
     * @author tzj
     * @description 用户展示个人信息二维码
     * @date 2019/12/13 9:27
     */
    @RequestMapping(value = "getUrl2")
    public Result<?> getUrl2(@Session("ShopUser") ShopUser shopUser) {
        shopUser.setCheckTime(new Date());
        shopUserService.update(shopUser);
             Map<String ,Object> param =new HashMap<>();
            param.put("userId",shopUser.getId());
        List<BookingCards> list = bookingCardsService.list(param);
        if (CollectionUtils.isEmpty(list)) {
            return Result.of(Status.ClientError.BAD_REQUEST,"未找到购卡记录");
        }else {
            if ("1".equals(shopUser.getDealerId())) {
                shopUserService.generateImg(shopUser);
                shopUser.setDealerId("2");
                shopUserService.update(shopUser);
            }
            return Result.ok(shopUser.getId());
        }
    }

    /**
     * @author tzj
     * @description 用户展示个人信息二维码
     * @date 2019/12/13 9:27
     */
    @RequestMapping(value = "check")
    public Result<?> check(@Session("ShopUser") ShopUser shopUser) {
            shopUser.setCheckTime(new Date());
                shopUserService.generateImg(shopUser);
                shopUser.setDealerId("2");
                shopUserService.update(shopUser);
            return Result.ok(shopUser.getId());

    }
    @PermissionMode(PermissionMode.Mode.White)
    @RequestMapping(value = "check2")
    public Result<?> check2(@RequestBody String id) {
        ShopUser shopUser = shopUserService.get(id);
        shopUser.setCheckTime(new Date());
        shopUserService.generateImg(shopUser);
        shopUser.setDealerId("2");
        shopUserService.update(shopUser);
        return Result.ok(shopUser.getId());

    }

    @RequestMapping(value = "getUrl")
    public Result<?> getUrl(@Session("ShopUser") ShopUser shopUser) {
            String url = shopUserService.refreshUrl(shopUser.getId(), cardUrl);
            return Result.ok(url);
    }


}
