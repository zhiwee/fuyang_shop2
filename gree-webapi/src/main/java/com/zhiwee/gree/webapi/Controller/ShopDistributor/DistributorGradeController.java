package com.zhiwee.gree.webapi.Controller.ShopDistributor;


import com.zhiwee.gree.model.GoodsInfo.GoodsClassify;
import com.zhiwee.gree.model.ShopDistributor.DistributorGrade;
import com.zhiwee.gree.model.ShopDistributor.ShopDistributor;
import com.zhiwee.gree.model.User;
import com.zhiwee.gree.service.GoodsInfo.GoodsClassifyService;
import com.zhiwee.gree.service.ShopDistributor.DistributorGradeService;
import com.zhiwee.gree.service.ShopDistributor.ShopDistributorService;
import com.zhiwee.gree.webapi.util.SystemControllerLog;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.util.KeyUtils;
import xyz.icrab.common.web.annotation.EnableGetRequest;
import xyz.icrab.common.web.annotation.EnableListRequest;
import xyz.icrab.common.web.annotation.Session;
import xyz.icrab.common.web.param.IdParam;
import xyz.icrab.common.web.param.IdsParam;
import xyz.icrab.common.web.util.Validator;
import xyz.icrab.ums.util.consts.SessionKeys;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.*;

@RequestMapping("/distributorGrade")
@Controller
@ResponseBody
@EnableListRequest
@EnableGetRequest
public class DistributorGradeController {

@Autowired
private DistributorGradeService distributorGradeService;




 /**
  * Description: 获取经销商等级信息
  * @author: sun
  * @Date 上午11:58 2019/6/17
  * @param:
  * @return:
  */
    @RequestMapping("/infoPage")
    @ResponseBody
    public Result<?> infoPage(@RequestBody Map<String, Object> param, Pagination pagination) {
        if (param == null) {
            param = new HashMap<>();
        }
        Pageable<DistributorGrade> pageable = distributorGradeService.infoPage(param, pagination);
        return Result.ok(pageable);
    }


    /**
     * Description: 添加会员等级
     * @author: sun
     * @Date 上午10:21 2019/6/19
     * @param:
     * @return:
     */
    @RequestMapping(value = "/addDistributorGrade")
    public Result<?> addHomePage( @RequestBody DistributorGrade memberGrade)  {
        Validator validator = new Validator();
        validator.notEmpty(memberGrade.getName(), "等级名称不能为空");
//        validator.notNull(homePage.getType(), "类型不能为空");
        validator.notNull(memberGrade.getEndAmount(), "结束金额为空");
        validator.notNull(memberGrade.getStartAmount(), "开始金额不能为空");
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }
        if(memberGrade.getStartAmount()>= memberGrade.getEndAmount()){
            return Result.of(Status.ClientError.BAD_REQUEST, "开始金额不能大于结束金额");
        }
        Map<String, Object> params = new HashMap<>();
        params.put("max",1);
        DistributorGrade member =  distributorGradeService.selectMax(params);
        if(member != null) {
            if (member.getEndAmount() >= memberGrade.getStartAmount()) {
                return Result.of(Status.ClientError.BAD_REQUEST, "和"+member.getName()+"存在区间重复");
            }
        }
        memberGrade.setState(1);
        memberGrade.setIsdefault(2);
        memberGrade.setCreateTime(new Date());
        distributorGradeService.save(memberGrade);
        return Result.ok();
    }



    /**
     * Description: 更新会员等级
     * @author: sun
     * @Date 上午10:21 2019/6/19
     * @param:
     * @return:
     */
    @RequestMapping(value = "/updateDistributorGrade")
    public Result<?> updateMemberGrade( @RequestBody DistributorGrade memberGrade)  {
        Validator validator = new Validator();
        validator.notEmpty(memberGrade.getName(), "等级名称不能为空");
//        validator.notNull(homePage.getType(), "类型不能为空");
        validator.notNull(memberGrade.getEndAmount(), "结束金额为空");
        validator.notNull(memberGrade.getStartAmount(), "开始金额不能为空");
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }
        if(memberGrade.getStartAmount()>= memberGrade.getEndAmount()){
            return Result.of(Status.ClientError.BAD_REQUEST, "开始金额不能大于结束金额");
        }
        Map<String, Object> params = new HashMap<>();
        params.put("maxId",memberGrade.getId());
        //比他大的最小的
        DistributorGrade maxmember =  distributorGradeService.selectMax(params);
        if(maxmember != null) {
            if (maxmember.getStartAmount() <= memberGrade.getEndAmount()) {
                return Result.of(Status.ClientError.BAD_REQUEST, "和"+maxmember.getName()+"存在区间重复");
            }
        }
        Map<String, Object> param = new HashMap<>();
        param.put("minId",memberGrade.getId());
        //比他小的最大的
        DistributorGrade minmember =  distributorGradeService.selectMax(param);
        if(minmember != null) {
            if (minmember.getEndAmount() >= memberGrade.getStartAmount()) {
                return Result.of(Status.ClientError.BAD_REQUEST, "和"+minmember.getName()+"存在区间重复");
            }
        }
        memberGrade.setUpdateTime(new Date());
        distributorGradeService.update(memberGrade);
        return Result.ok();
    }


    /**
     * Description: 获取会员等级
     * @author: sun
     * @Date 上午10:21 2019/6/19
     * @param:
     * @return:
     */
    @RequestMapping(value = "/achieveMemberGrade")
    public Result<?> achieveMemberGrade( @RequestBody DistributorGrade memberGrade)  {
        DistributorGrade member = distributorGradeService.getOne(memberGrade);
        return Result.ok(member);
    }

   /**
    * Description: 获取启用的经销商会员等级
    * @author: sun
    * @Date 下午3:01 2019/6/23
    * @param:
    * @return:
    */
    @RequestMapping(value = "/achieveList")
    public Result<?> achieveList()  {
        Map<String,Object> params  = new HashMap<>();
        params.put("state",1);
        List<DistributorGrade> list = distributorGradeService.list(params);
        return Result.ok(list);
    }

    /**
     * Description: 等级启用
     * @author: sun
     * @Date 上午11:35 2019/6/19
     * @param:
     * @return:
     */
    @RequestMapping("/open")
    @ResponseBody
    public  Result<?> open(@RequestBody DistributorGrade memberGrade ){
        memberGrade.setState(1);
        distributorGradeService.update(memberGrade);
        return Result.ok();
    }


    /**
     * Description: 等级禁用
     * @author: sun
     * @Date 上午11:35 2019/6/19
     * @param:
     * @return:
     */
    @RequestMapping("/stop")
    @ResponseBody
    public  Result<?> stop(@RequestBody DistributorGrade memberGrade ){
        DistributorGrade member = distributorGradeService.getOne(memberGrade);
        if(member.getIsdefault() == 1){
            return Result.of(Status.ClientError.BAD_REQUEST, "这个是默认等级不能禁用");
        }
        memberGrade.setState(2);
        distributorGradeService.update(memberGrade);
        return Result.ok();
    }



    /**
     * Description: 等级设置默认，只会传过来不是默认的等级的id
     * @author: sun
     * @Date 上午11:35 2019/6/19
     * @param:
     * @return:
     */
    @RequestMapping("/openDefault")
    @ResponseBody
    public  Result<?> openDefault(@RequestBody DistributorGrade memberGrade ){
        Map<String ,Object> param = new HashMap<>();
        param.put("isdefault",1);
        List<DistributorGrade> list = distributorGradeService.list(param);
        if(!list.isEmpty() && list.size() > 0){
            return Result.of(Status.ClientError.BAD_REQUEST, "存在默认等级，无法设置");
        }

        memberGrade.setIsdefault(1);
        distributorGradeService.update(memberGrade);
        return Result.ok();
    }

    /**
     * Description: 等级取消默认
     * @author: sun
     * @Date 上午11:35 2019/6/19
     * @param:
     * @return:
     */
    @RequestMapping("/stopDefault")
    @ResponseBody
    public  Result<?> stopDefault(@RequestBody DistributorGrade memberGrade ){
        memberGrade.setIsdefault(2);
        distributorGradeService.update(memberGrade);
        return Result.ok();
    }

}
