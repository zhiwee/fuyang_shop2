package com.zhiwee.gree.webapi.Controller.shopBase;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import com.zhiwee.gree.model.Dictionary;
import com.zhiwee.gree.model.User;
import com.zhiwee.gree.model.ret.DictionaryReturnMsg;
import com.zhiwee.gree.service.shopBase.DictionaryService;
import com.zhiwee.gree.webapi.util.SystemControllerLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.model.enums.YesOrNo;
import xyz.icrab.common.web.annotation.EnableDeleteRequest;
import xyz.icrab.common.web.annotation.EnableGetRequest;
import xyz.icrab.common.web.annotation.EnableListRequest;
import xyz.icrab.common.web.annotation.EnablePageRequest;
import xyz.icrab.common.web.annotation.Session;
import xyz.icrab.common.web.message.ValidatedMessage;
import xyz.icrab.common.web.param.IdParam;
import xyz.icrab.common.web.util.KeyUtils;
import xyz.icrab.common.web.util.Validator;

/**
 * @author gll
 * @since 2018/4/21 17:11
 */
@RestController
@RequestMapping("/dictionary")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class DictionaryController {

    @Autowired
    private DictionaryService dictionaryService;

	@SystemControllerLog(description="增加数据字典")
    @RequestMapping("/add")
    @ResponseBody
    public <T> Result<?> add(@RequestBody(required = false) Map<String, Object> param, @Session("user") User user){
    	String key = (String)param.get("key");
    	String value = (String)param.get("value");
    	String group = (String)param.get("group");
    	String remark = (String)param.get("remark");
    	Validator validator = new Validator();
    	validator.notEmpty(key, DictionaryReturnMsg.KEY_EMPTY.message());
    	validator.notEmpty(value, DictionaryReturnMsg.VALUE_EMPTY.message());
    	validator.notEmpty(group, DictionaryReturnMsg.GROUP_EMPTY.message());
    	if(validator.isError()){
    		return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
    	}
    	if(dictionaryService.checkKey(key, group)){
    		return Result.of(Status.ClientError.BAD_REQUEST, DictionaryReturnMsg.KEY_MANY.message());
    	}
    	Dictionary dictionary = new Dictionary();
    	dictionary.setId(KeyUtils.getKey());
    	dictionary.setGroup(group);
    	//周广改过
    	//dictionary.setRemark(remark);
    	dictionary.setKey(key);
    	dictionary.setValue(value);
    	dictionary.setState(YesOrNo.YES);
    	dictionary.setCreatetime(new Date());
    	dictionary.setCreateman(user.getId());
    	dictionaryService.save(dictionary);
        return Result.ok();
    }
	@SystemControllerLog(description="修改数据字典")
    @RequestMapping("/update")
    @ResponseBody
    public <T> Result<?> update(@RequestBody(required = false) Map<String, Object> param){
    	String id = (String)param.get("id");
    	String key = (String)param.get("key");
    	String value = (String)param.get("value");
    	String group = (String)param.get("group");
    	String remark = (String)param.get("remark");
    	Validator validator = new Validator();
    	validator.notEmpty(id, ValidatedMessage.EMPTY_ID);
    	validator.notEmpty(key, DictionaryReturnMsg.KEY_EMPTY.message());
    	validator.notEmpty(value, DictionaryReturnMsg.VALUE_EMPTY.message());
    	validator.notEmpty(group, DictionaryReturnMsg.GROUP_EMPTY.message());
    	if(validator.isError()){
    		return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
    	}
    	Dictionary dictionary = new Dictionary();
    	dictionary.setGroup(group);
    	dictionary.setKey(key);
    	dictionary = dictionaryService.getOne(dictionary);
    	if(!dictionary.getKey().equals(key) && !dictionary.getGroup().equals(group) && dictionaryService.checkKey(key, group)){
    		return Result.of(Status.ClientError.BAD_REQUEST, DictionaryReturnMsg.KEY_MANY.message());
    	}
    	
    	dictionary.setId(id);
    	dictionary.setGroup(group);

    	//周广改过
    	//dictionary.setRemark(remark);
    	dictionary.setValue(value);
    	dictionary.setState(YesOrNo.YES);
    	dictionaryService.update(dictionary);
        return Result.ok();
    }
	@SystemControllerLog(description="启用数据字典")
    @RequestMapping("/open")
    @ResponseBody
    public <T> Result<?> open(@RequestBody @Valid IdParam map){
    	Dictionary dictionary = new Dictionary();
    	dictionary.setId(map.getId());
    	dictionary.setState(YesOrNo.YES);
    	dictionaryService.update(dictionary);
        return Result.ok();
    }
	@SystemControllerLog(description="禁用数据字典")
    @RequestMapping("/stop")
    @ResponseBody
    public <T> Result<?> stop(@RequestBody @Valid IdParam map){
        Dictionary dictionary = new Dictionary();
        dictionary.setId(map.getId());
        dictionary.setState(YesOrNo.NO);
    	dictionaryService.update(dictionary);
        return Result.ok();
    }
	@SystemControllerLog(description="查询组数据字典（map）")
    @RequestMapping("/getMapByGroup")
    @ResponseBody
    public <T> Result<Map<String,String>> getMap(@RequestBody(required = false) Map<String, Object> param){
    	String group = (String)param.get("group");
    	Validator validator = new Validator();
    	validator.notEmpty(group, DictionaryReturnMsg.GROUP_EMPTY.message());
    	if(validator.isError()){
    		return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
    	}
    	List<Dictionary> list = dictionaryService.list(param);
    	Map<String,String> result = new HashMap<>();
    	for(Dictionary d : list){
    		result.put(d.getKey(), d.getValue());
    	}
        return Result.ok(result);
    }
	@SystemControllerLog(description="查询组数据字典列表")
    @RequestMapping("/{group}/getList")
    @ResponseBody
    public <T> Result<List<Dictionary>> getListByGroup(@PathVariable String group){
    	Map<String,Object> param = new HashMap<>();
    	Validator validator = new Validator();
    	validator.notEmpty(group, DictionaryReturnMsg.GROUP_EMPTY.message());
    	if(validator.isError()){
    		return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
    	}
    	param.put("group", group);
        return Result.ok(dictionaryService.list(param));
    }
}
