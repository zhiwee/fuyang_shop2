package com.zhiwee.gree.webapi.util;

/**
 * @author sun on 2019/5/16
 */

import com.alibaba.fastjson.JSONObject;
import com.zhiwee.gree.model.WxPay.WeChatConfig;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.util.EntityUtils;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;

import javax.net.ssl.SSLContext;
import java.io.File;
import java.io.FileInputStream;
import java.security.KeyStore;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Description: 微信退款需要证书
 * @author: sun
 * @Date 下午2:08 2019/5/17
 * @param:
 * @return:
 */
public class ClientCustomSSL {
//    "/home/newah/cert/apiclient_cert.p12"
   //  private static  String path = "/home/newah/cert/apiclient_cert.p12";
private static  String path = "D:/cert/WXCertUtil/cert/1232511302_20200103_cert/apiclient_cert.p12";

//    private static  String path = "/Users/mac/Downloads/cert/apiclient_cert.p12";

    /**
     *      * @Author: sun
     *      * @Description:微信退款方法封装   注意：：微信金额的单位是分 所以这里要X100 转成int是因为 退款的时候不能有小数点
     *      * @param orderId 订单号
     *      * @param wxTransactionNumber 微信那边的交易单号
     *      * @param totalFee 订单的金额   
     */
    public static Map setUrl(String orderId,double totalFee,String wxTransactionNumber,double refundMoney, String notifyUrl) {
        try {
            KeyStore keyStore = KeyStore.getInstance("PKCS12");

//            new File("/Users/mac/Downloads/cert/apiclient_cert.p12")
            FileInputStream instream = new FileInputStream(new File(path));
            try {
                keyStore.load(instream, WeChatConfig.MCHIDFuYang.toCharArray());
            } finally {
                instream.close();
            }
            // Trust own CA and all self-signed certs
            SSLContext sslcontext = SSLContexts.custom().loadKeyMaterial(keyStore, WeChatConfig.MCHIDFuYang.toCharArray()).build();
            // Allow TLSv1 protocol only
            SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(
                    sslcontext, new String[]{"TLSv1"}, null,
                    SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
            CloseableHttpClient httpclient = HttpClients.custom()
                    .setSSLSocketFactory(sslsf).build();
            HttpPost httppost = new HttpPost("https://api.mch.weixin.qq.com/secapi/pay/refund");

            String xml = WeixinRefundUtil.wxPayRefund(orderId, String.valueOf((int) (totalFee*100) ),wxTransactionNumber,String.valueOf((int) (refundMoney*100) ),  notifyUrl);
          //  String xml = WeixinRefundUtil.wxPayRefund(orderId, String.valueOf(totalFee),wxTransactionNumber,String.valueOf(refundMoney),  notifyUrl);

            try {
                StringEntity se = new StringEntity(xml);
                httppost.setEntity(se);
                System.out.println("executing request" + httppost.getRequestLine());
                CloseableHttpResponse responseEntry = httpclient.execute(httppost);
                try {
                    HttpEntity entity = responseEntry.getEntity();
                    System.out.println(responseEntry.getStatusLine());
                    if (entity != null) {
                        System.out.println("Response content length: "
                                + entity.getContentLength());
                        SAXReader saxReader = new SAXReader();
                        Document document = saxReader.read(entity.getContent());
                        Element rootElt = document.getRootElement();
//                        System.out.println("根节点：" + rootElt.getName());
//                        System.out.println("===" + rootElt.elementText("result_code"));
//                        System.out.println("===" + rootElt.elementText("return_msg"));
                        String resultCode = rootElt.elementText("result_code");

//                        JSONObject result = new JSONObject();
                        Map<String ,String> map = new HashMap();

                        Document documentXml = DocumentHelper.parseText(xml);
                        Element rootEltXml = documentXml.getRootElement();
                        if (resultCode.equals("SUCCESS")) {
                            String out_trade_no = rootElt.elementText("out_trade_no");
//                            return Result.ok(out_trade_no);
//                            System.out.println("=================prepay_id====================" + rootElt.elementText("prepay_id"));
//
//                            System.out.println("=================sign====================" + rootEltXml.elementText("sign"));
//                            result.put("weixinPayUrl", rootElt.elementText("code_url"));
//                            result.put("prepayId", rootElt.elementText("prepay_id"));
//                            result.put("status", "success");
//                            result.put("msg", "success");
                               map.put("status","SUCCESS");
                               map.put("out_trade_no",out_trade_no);
                               return map;
                        } else {
//                            return Result.of(Status.ClientError.BAD_REQUEST, rootElt.elementText("err_code_des"));
//                            result.put("status", "false");
//                            result.put("msg", rootElt.elementText("err_code_des"));
                            System.out.println(rootElt.elementText("err_code_des"));
                            map.put("status","false");
                            map.put("msg",rootElt.elementText("err_code_des"));
                            return map;
                        }
                       // return result;
                    }
                    EntityUtils.consume(entity);
                } finally {
                    responseEntry.close();
                }
            } finally {
                httpclient.close();
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
//            JSONObject result = new JSONObject();
//            result.put("status", "error");
//            result.put("msg", e.getMessage());
            Map<String ,String> param = new HashMap();

            param.put("status","false");
            param.put("msg",e.getMessage());
            return param;
        }
    }







    /**
     *      * @Author: sun
     *      * @Description:微信发红包方法封装   注意：：微信金额的单位是分 所以这里要X100 转成int是因为 退款的时候不能有小数点
     *      * @param merchantNumber 商户这边的订单号
     *      * @param wxTransactionNumber 微信那边的交易单号
     *      * @param totalFee 订单的金额   
     */
    public static Map cashVoucher(String orderNum, String re_openid, double totalFee) {
        System.out.println(orderNum+"。。。。。。。。。"+re_openid+"，。。。。。。。。"+"。。。。。。"+totalFee);
        try {
            KeyStore keyStore = KeyStore.getInstance("PKCS12");
            FileInputStream instream = new FileInputStream(new File(path));
            try {
                keyStore.load(instream, WeChatConfig.HNMCHID.toCharArray());
            } finally {
                instream.close();
            }
            // Trust own CA and all self-signed certs
            SSLContext sslcontext = SSLContexts.custom().loadKeyMaterial(keyStore, WeChatConfig.HNMCHID.toCharArray()).build();
            // Allow TLSv1 protocol only
            SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(
                    sslcontext, new String[]{"TLSv1"}, null,
                    SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
            CloseableHttpClient httpclient = HttpClients.custom()
                    .setSSLSocketFactory(sslsf).build();
            HttpPost httppost = new HttpPost("https://api.mch.weixin.qq.com/mmpaymkttransfers/sendredpack");


            String xml = WeixinRefundUtil.cashVoucher(orderNum, re_openid, String.valueOf((int) (totalFee * 100)));

            try {
                StringEntity se = new StringEntity(xml);
                httppost.setEntity(se);

                System.out.println("executing request" + httppost.getRequestLine());
                CloseableHttpResponse responseEntry = httpclient.execute(httppost);
                try {
                    HttpEntity entity = responseEntry.getEntity();
                    System.out.println(responseEntry.getStatusLine());
                    if (entity != null) {
                        System.out.println("Response content length: "
                                + entity.getContentLength());
                        SAXReader saxReader = new SAXReader();
                        Document document = saxReader.read(entity.getContent());
                        Element rootElt = document.getRootElement();
                        String resultCode = rootElt.elementText("result_code");
                        Map<String ,String> map = new HashMap();
                        Document documentXml = DocumentHelper.parseText(xml);
                        Element rootEltXml = documentXml.getRootElement();
                        if (resultCode.equals("SUCCESS")) {
                            System.out.println("孙凯"+resultCode);
                            map.put("status","SUCCESS");
                            map.put("msg","发送成功");
                            return map;
                        } else {
                            System.out.println(rootElt.elementText("err_code_des")+"/n"
                                    +"err_code"+rootElt.elementText("err_code")+"/n"
                                    +"re_openid"+rootElt.elementText("re_openid")+"/n"
                                    +"total_amount"+rootElt.elementText("total_amount")+"/n"
                                    +"mch_id"+rootElt.elementText("mch_id")+"/n"
                                    +"mch_billno"+rootElt.elementText("mch_billno")+"/n"
                                    +"wxappid"+rootElt.elementText("wxappid")
                                     );
                            map.put("status","false");
                            map.put("msg",rootElt.elementText("err_code_des"));
                            return map;
                        }

                    }
                    EntityUtils.consume(entity);
                } finally {
                    responseEntry.close();
                }
            } finally {
                httpclient.close();
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            Map<String ,String> param = new HashMap();
            param.put("status","false");
            param.put("msg",e.getMessage());
            return param;
        }
    }





    /**  微信支付到个人微信零钱
     *      * @Author: sun
     *      * @Description:微信发红包方法封装   注意：：微信金额的单位是分 所以这里要X100 转成int是因为 退款的时候不能有小数点
     *      * @param merchantNumber 商户这边的订单号
     *      * @param wxTransactionNumber 微信那边的交易单号
     *      * @param totalFee 订单的金额   
     */
    public static Map withdrawal(String orderNum, String re_openid, double totalFee) {
        System.out.println(orderNum+"。。。。。。。。。"+re_openid+"，。。。。。。。。"+"。。。。。。"+totalFee);
        try {
            KeyStore keyStore = KeyStore.getInstance("PKCS12");
            FileInputStream instream = new FileInputStream(new File(path));
            try {
                keyStore.load(instream, WeChatConfig.MCHID.toCharArray());
            } finally {
                instream.close();
            }
            // Trust own CA and all self-signed certs
            SSLContext sslcontext = SSLContexts.custom().loadKeyMaterial(keyStore, WeChatConfig.MCHID.toCharArray()).build();
            // Allow TLSv1 protocol only
            SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(
                    sslcontext, new String[]{"TLSv1"}, null,
                    SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
            CloseableHttpClient httpclient = HttpClients.custom()
                    .setSSLSocketFactory(sslsf).build();
            HttpPost httppost = new HttpPost("https://api.mch.weixin.qq.com/mmpaymkttransfers/promotion/transfers");


            String xml = WeixinRefundUtil.withdrawal(orderNum, re_openid, String.valueOf((int) (totalFee * 100)));

            try {
                StringEntity se = new StringEntity(xml);
                httppost.setEntity(se);

                System.out.println("executing request" + httppost.getRequestLine());
                CloseableHttpResponse responseEntry = httpclient.execute(httppost);
                try {
                    HttpEntity entity = responseEntry.getEntity();
                    System.out.println(responseEntry.getStatusLine());
                    if (entity != null) {
                        System.out.println("Response content length: "
                                + entity.getContentLength());
                        SAXReader saxReader = new SAXReader();
                        Document document = saxReader.read(entity.getContent());
                        Element rootElt = document.getRootElement();
                        String resultCode = rootElt.elementText("result_code");
                        String return_msg = rootElt.elementText("return_msg");
                        Map<String ,String> map = new HashMap();
                        Document documentXml = DocumentHelper.parseText(xml);
                        Element rootEltXml = documentXml.getRootElement();
                        if (resultCode.equals("SUCCESS")) {
                            System.out.println("孙凯"+resultCode);
                            map.put("status","SUCCESS");
                            map.put("msg","发送成功");
                            return map;
                        } else {
                            System.out.println(return_msg+"wuside孙凯 kaizi"+resultCode);
                            map.put("status","false");
                            map.put("msg",return_msg);
                            return map;
                        }

                    }
                    EntityUtils.consume(entity);
                } finally {
                    responseEntry.close();
                }
            } finally {
                httpclient.close();
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            Map<String ,String> param = new HashMap();
            param.put("status","false");
            param.put("msg",e.getMessage());
            return param;
        }
    }


}