package com.zhiwee.gree.webapi.Controller.Partement;

import com.zhiwee.gree.model.Activity.ActivityInfo;
import com.zhiwee.gree.model.ActivityTicketOrder.ActivityTicketOrder;
import com.zhiwee.gree.model.Coupon.Coupon;
import com.zhiwee.gree.model.DeptInfo.DeptInfo;
import com.zhiwee.gree.model.OnlineActivity.OnlineActivityInfo;
import com.zhiwee.gree.model.OrderInfo.Order;
import com.zhiwee.gree.model.OrderInfo.OrderAddress;
import com.zhiwee.gree.model.OrderInfo.OrderExport;
import com.zhiwee.gree.model.OrderInfo.OrderItem;
import com.zhiwee.gree.model.Partement.Partement;
import com.zhiwee.gree.model.ShopDistributor.DistributorArea;
import com.zhiwee.gree.model.ShopDistributor.DistributorOrder;
import com.zhiwee.gree.model.ShopDistributor.ShopDistributor;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.User;
import com.zhiwee.gree.service.Activity.ActivityInfoService;
import com.zhiwee.gree.service.ActivityTicketOrder.ActivityTicketOrderService;
import com.zhiwee.gree.service.Coupon.CouponService;
import com.zhiwee.gree.service.OnlineActivity.OnlineActivityInfoService;
import com.zhiwee.gree.service.OrderInfo.OrderAddressService;
import com.zhiwee.gree.service.OrderInfo.OrderItemService;
import com.zhiwee.gree.service.OrderInfo.OrderService;
import com.zhiwee.gree.service.Partement.PartementService;
import com.zhiwee.gree.service.ShopDistributor.DistributorAreaService;
import com.zhiwee.gree.service.ShopDistributor.DistributorOrderService;
import com.zhiwee.gree.service.ShopDistributor.ShopDistributorService;
import com.zhiwee.gree.util.SequenceUtils;
import com.zhiwee.gree.webapi.util.SystemControllerLog;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.jeecgframework.poi.excel.ExcelExportUtil;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.enmus.ExcelType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.*;
import xyz.icrab.common.util.IdGenerator;
import xyz.icrab.common.util.KeyUtils;
import xyz.icrab.common.web.annotation.*;
import xyz.icrab.common.web.param.IdParam;
import xyz.icrab.common.web.param.IdsParam;
import xyz.icrab.common.web.util.Validator;
import xyz.icrab.ums.util.consts.SessionKeys;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.io.OutputStream;
import java.util.*;


/**
 * @author sun on 2019/6/16
 */
@RestController
@RequestMapping("/partement")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class PartementController {

    @Autowired
    private PartementService partementService;



/**
 * Description: 添加
 * @author: sun
 * @Date 下午3:20 2019/6/13
 * @param:
 * @return:
 */
    @RequestMapping("/addPartement")
    public Result<?> add(@RequestBody Partement partement) {
        Validator validator = new Validator();
        validator.notEmpty(partement.getPartement(), "名称不能为空");
        validator.notNull(partement.getType(), "类型不能为空");
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }
        String id = KeyUtils.getKey();
        partement.setId(id);
        partement.setCreateTime(new Date());
        partement.setState(1);
        String parentId = partement.getParentId();
        String dealerIdPath = null;
        Partement parent = null;
        if (StringUtils.isNotBlank(parentId)) {
            parent = partementService.get(parentId);
            if (parent != null) {
                dealerIdPath = parent.getIdPath() + id + ",";
            }
        } else {
            parentId = "ROOT";
            dealerIdPath = parentId + id + ",";
        }
        partement.setParentId(parentId);
        partement.setIdPath(dealerIdPath);
        partementService.save(partement);
        return Result.ok();
    }



    /**
     * Description: 编辑
     * @author: sun
     * @Date 下午3:20 2019/6/13
     * @param:
     * @return:
     */
    @RequestMapping("/updatePartement")
    public Result<?> updatePartement(@RequestBody Partement partement) {
        Validator validator = new Validator();
        validator.notEmpty(partement.getId(), "标识不能为空");
        validator.notEmpty(partement.getPartement(), "名称不能为空");
        validator.notNull(partement.getType(), "类型不能为空");
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }

        String id = partement.getId();
        partement.setUpdateTime(new Date());
        String parentId = partement.getParentId();
        String dealerIdPath = null;
        Partement parent = null;
        if (StringUtils.isNotBlank(parentId)) {
            parent = partementService.get(parentId);
            if (parent != null) {
                dealerIdPath = parent.getIdPath() + id + ",";
            }
        } else {
            parentId = "ROOT";
            dealerIdPath = parentId + id + ",";
        }
        partement.setParentId(parentId);
        partement.setIdPath(dealerIdPath);

        partementService.update(partement);
        return Result.ok();
    }



    /**
     * Description: 通过id获取信息
     *
     * @author: sun
     * @Date 下午3:20 2019/6/13
     * @param:
     * @return:
     */
    @RequestMapping("/getOne")
    public Result<?> getOne(@RequestBody @Valid IdParam map) {
     String id = map.getId();
        Partement partement = partementService.get(id);
        return Result.ok(partement);
    }

    /**
     * Description: 获取所有信息
     *
     * @author: sun
     * @Date 下午3:20 2019/6/13
     * @param:
     * @return:
     */
    @RequestMapping("/pageInfo")
    public Result<?> pageInfo(@RequestBody Map<String, Object> params,  Pagination pagination) {
        if (params == null) {
            params = new HashMap<>();
        }

        Pageable<Partement> page = partementService.pageInfo(params, pagination);
        return Result.ok(page);
    }



    /**
     * Description: 启用
     *
     * @author: sun
     * @Date 下午3:20 2019/6/13
     * @param:
     * @return:
     */
    @RequestMapping("/open")
    public Result<?> open(@RequestBody @Valid IdsParam map) {
        List<String> ids = map.getIds();
        Partement partement = new Partement();
        for (String id : ids) {
            partement.setId(id);
            partement.setState(1);
            partementService.update(partement);
        }

        return Result.ok();
    }

    /**
     * Description: 禁用
     *
     * @author: sun
     * @Date 下午3:20 2019/6/13
     * @param:
     * @return:
     */
    @RequestMapping("/stop")
    public Result<?> stop(@RequestBody @Valid IdsParam map) {
        List<String> ids = map.getIds();
        Partement partement = new Partement();
        for (String id : ids) {
            partement.setId(id);
            partement.setState(2);
            partementService.update(partement);
        }

        return Result.ok();
    }



    @RequestMapping("/tree")
    @PermissionMode(PermissionMode.Mode.White)
    @ResponseBody
    public Result<?> simpleFilterTree(@RequestBody(required =false)Map<String,Object> param, @Session("user") User user) {
        if (param == null) {
            param = new HashMap<>();
        }
        param.put("state", new Object[]{1, 2});
        param.put("idPath", ",ROOT,1,");
        List<Partement> list = partementService.list(param);
        List<Tree<Partement>> trees = new ArrayList<>(list.size());
        for (Partement record : list) {
            Tree<Partement> tree = new Tree<>(record.getId(), record.getPartement(), record.getParentId(), record);
            trees.add(tree);
        }
        if (trees.size() == 0) {
            Tree<Partement> tree = new Tree<>("", "所有部门", "", null);
            trees.add(tree);
        }
        return Result.ok(trees);
    }




    @RequestMapping("/disable")
    public Result<?> disable(@RequestBody IdParam param) {
        partementService.disable(param.getId());
        return Result.ok();
    }


    @RequestMapping("/enable")
    public Result<?> enable(@RequestBody IdParam param) {
        partementService.enable(param.getId());
        return Result.ok();
    }




}




