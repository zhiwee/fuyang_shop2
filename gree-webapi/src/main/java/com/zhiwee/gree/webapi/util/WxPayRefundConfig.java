package com.zhiwee.gree.webapi.util;

import com.github.wxpay.sdk.WXPayConfig;
import com.zhiwee.gree.model.WxPay.WeChatConfig;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 * TODO
 *
 * @author Chensong
 * @Company http://www.zhiwee.com
 * date 2020-01-07
 */
public class WxPayRefundConfig  implements WXPayConfig {

    @Override
    public String getAppID() {
        return WeChatConfig.APPIDFuYang;
    }

    @Override
    public String getMchID() {
        return  WeChatConfig.MCHIDFuYang;
    }

    @Override
    public String getKey() {
        return  WeChatConfig.APIKEYFuYang;
    }

    @Override
    public InputStream getCertStream() {
        try {
            InputStream inputStream = new FileInputStream(new File(WeChatConfig.WxPayRefundConfigCertPath));
            return inputStream;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public int getHttpConnectTimeoutMs() {
        return 6*1000;
    }

    @Override
    public int getHttpReadTimeoutMs() {
        return 8*1000;
    }
}

