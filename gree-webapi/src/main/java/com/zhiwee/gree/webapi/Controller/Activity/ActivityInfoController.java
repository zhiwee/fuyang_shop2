package com.zhiwee.gree.webapi.Controller.Activity;

import com.alipay.api.domain.Activity;
import com.zhiwee.gree.model.Activity.ActivityInfo;
import com.zhiwee.gree.model.OnlineActivity.OnlineActivityInfo;
import com.zhiwee.gree.model.User;

import com.zhiwee.gree.model.enums.OnlineActivityType;
import com.zhiwee.gree.service.Activity.ActivityInfoService;

import com.zhiwee.gree.webapi.util.SystemControllerLog;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import sun.security.util.KeyUtil;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;

import xyz.icrab.common.model.Status;
import xyz.icrab.common.util.KeyUtils;
import xyz.icrab.common.web.annotation.EnableListRequest;
import xyz.icrab.common.web.annotation.EnablePageRequest;

import xyz.icrab.common.web.annotation.PermissionMode;
import xyz.icrab.common.web.annotation.Session;

import xyz.icrab.common.web.param.IdParam;
import xyz.icrab.common.web.param.IdsParam;
import xyz.icrab.common.web.util.Validator;
import xyz.icrab.ums.util.consts.SessionKeys;

import javax.validation.Valid;
import java.util.*;

/**
 * @author SUN
 * @since 2018/7/6 14:22
 */
@Controller
@RequestMapping("/activityInfo")
@EnablePageRequest
@EnableListRequest
public class ActivityInfoController {

   
    @Autowired
    private ActivityInfoService activityInfoService;


/**
 * Description: 
 * @author: sun
 * @Date 下午11:45 2019/5/15
 * @param:
 * @return:
 */
    @RequestMapping("/page")
    @ResponseBody
    public Result<Pageable<ActivityInfo>> page(@Session(SessionKeys.USER) User user, @RequestBody(required = false) Map<String, Object> param, Pagination pagination) {
        if (param == null) {
            param = new HashMap<>();
        }
        Pageable<ActivityInfo> pageable = activityInfoService.page(param, pagination);
        return Result.ok(pageable);
    }


    /**
     * Description:
     * @author: sun
     * @Date 下午11:45 2019/5/15
     * @param:
     * @return:
     */
    @RequestMapping("/list")
    @ResponseBody
    public Result<?> list(@RequestBody  Map<String, Object> map) {
        if (map == null) {
            map = new HashMap<>();
        }
        List<ActivityInfo> list = activityInfoService.list(map);
        return Result.ok(list);
    }


    /**
     * @author tzj
     * @description 新增
     * @date 2020/1/15 14:53
    */
    @RequestMapping("/add")
    @ResponseBody
    public Result<?> add(@RequestBody ActivityInfo activityInfo, @Session(SessionKeys.USER) User user) {
        Validator validator = new Validator();
        validator.notEmpty(activityInfo.getName(), "活动名称不能为空");
        validator.notNull(activityInfo.getBegintime(), "开始时间不能为空");
        validator.notNull(activityInfo.getEndtime(), "结束时间不能为空");
        validator.isTrue(activityInfo.getBegintime().before(activityInfo.getEndtime()), "开始时间必须小于结束时间");
         //校验时间
          Boolean flg=  checkTime(activityInfo);
            if (!flg){
                return Result.of(Status.ClientError.BAD_REQUEST, "已有活动在当前时间段:");
            }
        activityInfo.setId(KeyUtils.getKey());
        activityInfo.setCreatetime(new Date());
        activityInfo.setCreateman(user.getId());
        activityInfo.setState(1);
        activityInfoService.save(activityInfo);
        return Result.ok();
    }


    /**
     * @author tzj
     * @description 修改
     * @date 2020/1/15 14:53
    */
    @RequestMapping(value = "/update")
    public Result<?> update(@Session(SessionKeys.USER) User user, @RequestBody(required = false) ActivityInfo activityInfo) {
        Validator validator = new Validator();
        validator.notEmpty(activityInfo.getName(), "活动名称不能为空");
        validator.notNull(activityInfo.getBegintime(), "开始时间不能为空");
        validator.notNull(activityInfo.getEndtime(), "结束时间不能为空");
        validator.isTrue(activityInfo.getBegintime().before(activityInfo.getEndtime()), "开始时间必须小于结束时间");
        //校验时间
        Boolean flg=  checkTime(activityInfo);
        if (!flg){
            return Result.of(Status.ClientError.BAD_REQUEST, "已有活动在当前时间段:");
        }
        activityInfo.setUpdatetime(new Date());
        activityInfo.setUpdateman(user.getId());
        activityInfoService.update(activityInfo);
        return Result.ok();
    }


        /**
         * @author tzj
         * @description 同一时间只能有一个活动
         * @date 2020/1/15 14:24
        */
    private Boolean checkTime(ActivityInfo activityInfo) {
        Map<String, Object> param = new HashMap<>(10);
        param.put("endTime", activityInfo.getEndtime());
        param.put("beginTime", activityInfo.getBegintime());
        param.put("state", 1);
        List<ActivityInfo> list=activityInfoService.checkTime(param);
        if (list.size() > 0) {
            return list.size() == 1 && activityInfo.getId().equals(list.get(0).getId());
        } else {
            return true;
        }
    }


    /**
     * @author tzj
     * @description 获取当前活动信息
     * @date 2020/1/15 14:54
     */
    @CrossOrigin
    @RequestMapping("/getCurrent")
    @PermissionMode(PermissionMode.Mode.White)
    @ResponseBody
    public Result<?> getCurrent(  ) {
      ActivityInfo activityInfo;
      Map<String,Object> param=new HashMap<>();
      param.put("time",new Date());
      param.put("state",1);
        activityInfo  =  activityInfoService.getCurrent(param);
        return Result.ok(activityInfo);
    }

    /**
     * @author tzj
     * @description 获取新
     * @date 2020/1/15 14:54
    */
    @RequestMapping("/get")
    @ResponseBody
    public Result<?> get(@RequestBody ActivityInfo activityInfo) {
        activityInfo= activityInfoService.getOne(activityInfo);
        return Result.ok(activityInfo);
    }

    /**
     * @author tzj
     * @description 禁用
     * @date 2020/1/15 14:54
    */
    @RequestMapping("/disable")
    @ResponseBody
    public Result<?> disable(@RequestBody @Valid IdParam param) {
        ActivityInfo activityInfo=new ActivityInfo();
        activityInfo.setId(param.getId());
        activityInfo.setState(2);
        activityInfoService.update(activityInfo);
        return Result.ok();
    }

    /**
     * @author tzj
     * @description 启用
     * @date 2020/1/15 14:55
    */
    @RequestMapping("/enable")
    @ResponseBody
    public Result<?> enable(@RequestBody @Valid IdParam param) {
        ActivityInfo activityInfo=new ActivityInfo();
        activityInfo.setId(param.getId());
        activityInfo.setState(1);
        activityInfoService.update(activityInfo);
        return Result.ok();
    }






}
