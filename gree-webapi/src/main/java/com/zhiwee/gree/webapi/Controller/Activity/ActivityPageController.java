package com.zhiwee.gree.webapi.Controller.Activity;

import com.zhiwee.gree.model.Activity.ActivityInfo;
import com.zhiwee.gree.model.Activity.ActivityPage;
import com.zhiwee.gree.model.User;
import com.zhiwee.gree.service.Activity.ActivityInfoService;
import com.zhiwee.gree.service.Activity.ActivityPageService;
import com.zhiwee.gree.webapi.util.SystemControllerLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.util.IdGenerator;
import xyz.icrab.common.web.annotation.EnableListRequest;
import xyz.icrab.common.web.annotation.EnablePageRequest;
import xyz.icrab.common.web.annotation.PermissionMode;
import xyz.icrab.common.web.annotation.Session;
import xyz.icrab.common.web.param.IdParam;
import xyz.icrab.common.web.param.IdsParam;
import xyz.icrab.common.web.util.Validator;
import xyz.icrab.ums.util.consts.SessionKeys;

import javax.validation.Valid;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author SUN
 * @since 2019/7/6 14:22
 */
@Controller
@RequestMapping("/activityPage")
@EnablePageRequest
@EnableListRequest
public class ActivityPageController {

   
    @Autowired
    private ActivityPageService activityPageService;


/**
 * Description: 获取pc端的活动页面
 * @author: sun
 * @Date 上午10:35 2019/7/8
 * @param:
 * @return:
 */
    @RequestMapping("/page")
    @ResponseBody
    public Result<Pageable<ActivityPage>> page( @RequestBody(required = false) Map<String, Object> param, Pagination pagination) {
        if (param == null) {
            param = new HashMap<>();
        }
        Pageable<ActivityPage> pageable = activityPageService.pageInfo(param, pagination);
        return Result.ok(pageable);
    }


/**
 * Description: 获取pc端的活动页面
 * @author: sun
 * @Date 上午10:34 2019/7/8
 * @param:
 * @return:
 */
@PermissionMode(PermissionMode.Mode.White)
    @RequestMapping("/pcActivityPage")
    @ResponseBody
    public Result<?> pcActivityPage() {
        Map<String,Object> params = new HashMap<>();
        params.put("state","1");
        params.put("type","1");
        List<ActivityPage> list = activityPageService.list(params);
        return Result.ok(list);
    }



    /**
     * Description: 获取手机端的
     * @author: sun
     * @Date 上午10:34 2019/7/8
     * @param:
     * @return:
     */
    @PermissionMode(PermissionMode.Mode.White)
    @RequestMapping("/phoneActivityPage")
    @ResponseBody
    public Result<?> phoneActivityPage() {
        Map<String,Object> params = new HashMap<>();
        params.put("state","1");
        params.put("type","2");
        List<ActivityPage> list = activityPageService.list(params);
        return Result.ok(list);
    }



    /**
   * Description: 启用
   * @author: sun
   * @Date 上午10:37 2019/7/8
   * @param:
   * @return:
   */
    @RequestMapping("/open")
    @ResponseBody
    public Result<?> open(@RequestBody @Valid IdsParam map) {
        List<String> ids = map.getIds();
        ActivityPage activityPage = new ActivityPage();
        for (String id : ids) {
            activityPage.setId(id);
            activityPage.setState(1);
            activityPageService.update(activityPage);
        }
        return Result.ok();
    }




  /**
   * Description: 禁用
   * @author: sun
   * @Date 上午10:37 2019/7/8
   * @param:
   * @return:
   */
    @RequestMapping("/stop")
    @ResponseBody
    public Result<?> stop(@RequestBody @Valid IdsParam map) {
        List<String> ids = map.getIds();
        ActivityPage activityPage = new ActivityPage();
        for (String id : ids) {
            activityPage.setId(id);
            activityPage.setState(2);
            activityPageService.update(activityPage);
        }
        return Result.ok();
    }

 /**
  * Description: 根据id获取页面信息
  * @author: sun
  * @Date 上午10:50 2019/7/8
  * @param:
  * @return:
  */
    @RequestMapping("/get")
    @ResponseBody
    public Result<?> getMe(@RequestBody @Valid IdParam map){
        ActivityPage activityPage = activityPageService.get(map.getId());
        return Result.ok(activityPage);
    }


    /**
     * Description:  添加页面
     *
     * @author: sun
     * @Date 下午10:40 2019/3/27
     * @param:
     * @return:
     */

    @RequestMapping(value = "addActivityPage")
    @ResponseBody
    public Result<?> addActivityPage(@RequestBody ActivityPage activityPage, @Session(SessionKeys.USER) User user) {
        Validator validator = new Validator();
        validator.notEmpty(activityPage.getName(), "名称不能为空");
        validator.notNull(activityPage.getType(), "类型不能为空");
        validator.notNull(activityPage.getActivityUrl(), "地址不能为空");
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }
        activityPage.setId(IdGenerator.objectId());
        activityPage.setState(1);
        activityPage.setCreateTime(new Date());
        activityPageService.save(activityPage);
        return Result.ok();
    }


    /**
     * Description: 更新信息
     *
     * @author: sun
     * @Date 下午9:06 2019/5/8
     * @param:
     * @return:
     */

    @RequestMapping("/updateActivityPage")
    @ResponseBody
    public Result<?> updateActivityPage(@RequestBody ActivityPage activityPage) {
        Validator validator = new Validator();
        validator.notNull(activityPage.getId(), "表示不能为空");
        validator.notNull(activityPage.getName(), "名称不能为空");
        validator.notNull(activityPage.getActivityUrl(), "地址不能为空");
        validator.notNull(activityPage.getType(), "类型不能为空");
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }
        activityPage.setUpdateTime(new Date());
        activityPageService.update(activityPage);
        return Result.ok();
    }


}
