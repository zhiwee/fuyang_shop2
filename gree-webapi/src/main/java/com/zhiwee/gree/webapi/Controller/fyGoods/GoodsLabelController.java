package com.zhiwee.gree.webapi.Controller.fyGoods;

import com.zhiwee.gree.model.FyGoods.GoodsLabel;
import com.zhiwee.gree.model.GoodsInfo.Goods;
import com.zhiwee.gree.service.tgoods.GoodsLabelService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Tree;
import xyz.icrab.common.util.KeyUtils;
import xyz.icrab.common.web.annotation.PermissionMode;
import xyz.icrab.common.web.param.IdParam;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author DELL
 */
@ResponseBody
@RequestMapping("/goodsLabel")
@RestController
public class GoodsLabelController {

    @Resource
    private GoodsLabelService goodsLabelService;



    /**
     * @author tzj
     * @description 页面
     * @date 2020/4/2 10:14
     */
    @RequestMapping("/page")
    public Result<?> page(@RequestBody Map<String, Object> param, Pagination pagination) {
        param.put("state",1);
        Pageable<GoodsLabel> pageable = goodsLabelService.page(param, pagination);
        return Result.ok(pageable);
    }


    /**
     * @author tzj
     * @description 树形结构
     * @date 2019/12/12 12:46
     */
    @RequestMapping("/tree")
    public Result<?> tree(@RequestBody(required = false) Map<String, Object> params) {
        if (params == null) {
            params = new HashMap<>();
        }
        List<GoodsLabel> list = goodsLabelService.list(params);
        List<Tree<GoodsLabel>> trees = new ArrayList<>(list.size());
        for (GoodsLabel goodsCategory : list) {
            Tree<GoodsLabel> tree = new Tree<>(goodsCategory.getId(), goodsCategory.getName(), goodsCategory.getParentId(), goodsCategory);
            trees.add(tree);
        }
        return Result.ok(trees);
    }

    /**
     * @author tzj
     * @description 新增
     * @date 2020/4/2 10:14
     */
    @RequestMapping("/add")
    public Result<?> add(@RequestBody GoodsLabel goodsLabel) {
        goodsLabel.setId(KeyUtils.getKey());
        goodsLabel.setState(1);
        if (goodsLabel.getParentId()!=null&& !"".equals(goodsLabel.getParentId())){
            goodsLabel.setRank(2);
        }else {
            goodsLabel.setRank(1);
        }
        goodsLabelService.save(goodsLabel);
        return Result.ok();
    }

    @RequestMapping("/selectOne")
    @ResponseBody
    public Result<?> selectOne(@RequestBody IdParam param) {
        GoodsLabel goodsCategory = goodsLabelService.get(param.getId());
        return Result.ok(goodsCategory);
    }

    @RequestMapping("/update")
    public Result<?> update(@RequestBody GoodsLabel goodsLabel) {
        if (goodsLabel.getParentId() == null || goodsLabel.getParentId().equals("")) {
            goodsLabel.setRank(2);
        } else {
            goodsLabel.setRank(1);
        }
        goodsLabelService.update(goodsLabel);
        return Result.ok();
    }








}
