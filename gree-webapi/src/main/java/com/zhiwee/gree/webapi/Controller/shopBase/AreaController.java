package com.zhiwee.gree.webapi.Controller.shopBase;

import com.zhiwee.gree.model.Area;
import com.zhiwee.gree.model.enums.AreaType;
import com.zhiwee.gree.model.ret.AreaReturnMsg;
import com.zhiwee.gree.service.shopBase.AreaService;
import com.zhiwee.gree.webapi.Controller.shopBase.vo.SimpleArea;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.NoHandlerFoundException;
import xyz.icrab.common.model.*;
import xyz.icrab.common.model.enums.YesOrNo;
import xyz.icrab.common.web.annotation.EnableDeleteRequest;
import xyz.icrab.common.web.annotation.EnableGetRequest;
import xyz.icrab.common.web.annotation.EnableListRequest;
import xyz.icrab.common.web.annotation.EnablePageRequest;
import xyz.icrab.common.web.annotation.PermissionMode;
import xyz.icrab.common.web.param.IdParam;
import xyz.icrab.common.web.util.KeyUtils;
import xyz.icrab.common.web.util.Validator;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequestMapping("/area")
@RestController
@ResponseBody
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class AreaController {
    @Autowired
    private AreaService areaService;

    /**
     * 省市区联动，默认省
     * @return
     */
    @RequestMapping("/initProvince")
    public Result<?> initProvince(){
        Map map=new HashMap();
        map.put("type","1");
        List<Area> list=areaService.list(map);
        return Result.ok(list);
    }
    /**
     * 省市区联动，默认市、区
     * @return
     */
    @RequestMapping("/initCity")
    public Result<?> initCity(@RequestBody(required = false) Map<String, Object> params){
        List<Area> list=areaService.list(params);
        return Result.ok(list);
    }
    @RequestMapping("/save")
    public Result<?> add(@RequestBody Area area) {
        Validator validator = new Validator();
        validator.notEmpty(area.getName(), AreaReturnMsg.AREA_NAME_EMPTY.message());
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }
        if (StringUtils.isBlank(area.getId())) {
            area.setIsdel(YesOrNo.NO.value());
            String id = KeyUtils.getKey();
            area.setId(id);
            if (StringUtils.isBlank(area.getParentid())) {
                area.setParentid("ROOT");
                area.setParentids("ROOT," + id);
            } else {
                area.setParentids(area.getParentids() + "," + id);
            }
            areaService.save(area);
        } else {
            if (area.getParentids().indexOf(area.getId()) < 0) {
                area.setParentids(area.getParentids() + "," + area.getId());
            }
            areaService.update(area);
        }
        return Result.ok();
    }
/**
 * Description: 获取所有区域
 * @author: sun
 * @Date 下午4:57 2019/5/21
 * @param:
 * @return:
 */
    @RequestMapping("/tree")
    public Result<?> tree(@RequestBody(required = false) Map<String, Object> params) {
        if(params==null){
            params= new HashMap<>();
        }
        params.put("isdel", YesOrNo.NO.value());
        List<Area> list = areaService.list();
        List<Tree<Area>> trees = new ArrayList<>(list.size());
        for (Area area : list) {
            Tree<Area> tree = new Tree<>(area.getId(), area.getName(), area.getParentid(), area);
            trees.add(tree);
        }
        return Result.ok(trees);
    }


    /**
     * Description: 获取所有安徽的地区
     * @author: sun
     * @Date 下午4:57 2019/5/21
     * @param:
     * @return:
     */
    @RequestMapping("/ahtree")
    public Result<?> ahtree(@RequestBody(required = false) Map<String, Object> params) {
        if(params==null){
            params= new HashMap<>();
        }
        params.put("isdel", YesOrNo.NO.value());
        params.put("parentids", "ROOT,03");
        List<Area> list = areaService.ahtree(params);
        List<Tree<Area>> trees = new ArrayList<>(list.size());
        for (Area area : list) {
            Tree<Area> tree = new Tree<>(area.getId(), area.getName(), area.getParentid(), area);
            trees.add(tree);
        }
        return Result.ok(trees);
    }


    /**
     * Description: 获取所有的省
     * @author: sun
     * @Date 下午4:57 2019/5/21
     * @param:
     * @return:
     */
    @RequestMapping("/allProvince")
    public Result<?> allProvince(@RequestBody(required = false) Map<String, Object> params,Pagination pagination) {
        if(params==null){
            params= new HashMap<>();
        }
        params.put("isdel", YesOrNo.NO.value());
        params.put("parentids", "ROOT");
        Pageable<Area> page = areaService.allProvince(params,pagination);
        return Result.ok(page);
    }



    /**
     * Description: 获取所有安徽的地区
     * @author: sun
     * @Date 下午4:57 2019/5/21
     * @param:
     * @return:
     */
    @RequestMapping("/ahPage")
    public Result<?> ahPage(@RequestBody(required = false) Map<String, Object> params,Pagination pagination) {
        if(params==null){
            params= new HashMap<>();
        }
        params.put("isdel", YesOrNo.NO.value());
        if(params.get("parentids") != null && !((String)params.get("parentids")).equals("") ){
            params.put("parentids", "ROOT,03,"+params.get("parentids"));
        }else{
            params.put("parentids", "ROOT,03");
        }

        Pageable<Area> page = areaService.ahPage(params,pagination);
       return Result.ok(page);
    }



    @RequestMapping("/get")
    public Result<?> get(@RequestBody @Valid IdParam param) throws NoHandlerFoundException {
        Area area = areaService.get(param.getId());
        if(StringUtils.isNotBlank(area.getParentid())&& !"ROOT".equals(area.getParentid())){
            Area child = areaService.get(area.getParentid());
            if(child != null){
                area.setParentname(child.getName());
            }
        }
        return Result.ok(area);
    }

    /**
     * Description: 前端获取地区的方法
     * @author: sun
     * @Date 下午4:56 2019/5/21
     * @param:
     * @return:
     */
    @RequestMapping("/simpletree")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> tree() {
        Map<String, Object> param = new HashMap<>();
        param.put("type", new Integer[]{AreaType.PROVINCE.getValue(), AreaType.CITY.getValue(), AreaType.COUNTY.getValue()});
        param.put("isdel", YesOrNo.NO.value());

        List<SimpleArea> result = new ArrayList<>();
        Map<String, SimpleArea> resultMap = new HashMap<>();
        Map<String, Area> areaMap = new HashMap<>();

        List<Area> data = areaService.list(param);
        if(CollectionUtils.isNotEmpty(data)) {
            for (Area area : data) {
                SimpleArea simpleArea = new SimpleArea(area.getCode(), area.getName(), area.getType());
                if(AreaType.PROVINCE.getValue() == area.getType()) {
                    result.add(simpleArea);
                }
                areaMap.put(area.getId(), area);
                resultMap.put(area.getCode(), simpleArea);
            }
            for (Area area : data) {
                if(AreaType.PROVINCE.getValue() != area.getType()) {
                    String parentid = area.getParentid();
                    if(StringUtils.isNotBlank(parentid)) {
                        Area parent = areaMap.get(parentid);
                        if(parent != null) {
                            SimpleArea parentSimpleArea = resultMap.get(parent.getCode());
                            if(parentSimpleArea != null) {
                                SimpleArea self = resultMap.get(area.getCode());
                                if(parentSimpleArea.getChildren() == null) {
                                    parentSimpleArea.setChildren(new ArrayList<>());
                                }
                                parentSimpleArea.getChildren().add(self);
                            }
                        }
                    }
                }
            }
        }
//        if (StringUtils.isNotBlank(callback)) {
//            //把结果封装成一个js语句响应
//            MappingJacksonValue mappingJacksonValue = new MappingJacksonValue( Result.ok(result));
//            mappingJacksonValue.setJsonpFunction(callback);
//            return mappingJacksonValue;
//        }
        return Result.ok(result);
    }





    @RequestMapping("/children")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<List<SimpleArea>> children(@RequestBody(required = false) Map<String, Object> param) {
        if(param == null) {
            param = new HashMap<>();
        }
        param.put("isdel", YesOrNo.NO.value());

        String code = (String) param.remove("code");
        if(StringUtils.isNotBlank(code)) {
            param.put("parentid", code);
        }else{
            param.put("parentid", "ROOT");
        }

        List<SimpleArea> result = new ArrayList<>();
        List<Area> data = areaService.list(param);
        if(CollectionUtils.isNotEmpty(data)) {
            for (Area area : data) {
                SimpleArea simpleArea = new SimpleArea(area.getCode(), area.getName(), area.getType());
                result.add(simpleArea);
            }
        }
        return Result.ok(result);
    }
}
