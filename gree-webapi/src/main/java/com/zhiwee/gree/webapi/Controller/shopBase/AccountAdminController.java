package com.zhiwee.gree.webapi.Controller.shopBase;

import com.zhiwee.gree.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.web.annotation.Session;
import xyz.icrab.common.web.param.IdParam;
import xyz.icrab.common.web.session.SessionService;
import xyz.icrab.ums.util.consts.SessionKeys;

import javax.validation.Valid;

/**
 * @author Knight
 * @since 2018/9/20 17:07
 */
@RequestMapping("/admin/account")
public class AccountAdminController {

    @Autowired
    private SessionService sessionService;

    @RequestMapping("/forceLogout")
    @ResponseBody
    public Result<?> forceLogout(@Valid @RequestBody IdParam param, @Session(SessionKeys.USER) User user) {
        if(user.isSuperUser()) {
        }
        return Result.ok();
    }

    @RequestMapping("/forceAllLogout")
    @ResponseBody
    public Result<?> forceLogout(@Session(SessionKeys.USER) User user) {
        if(user.isSuperUser()) {
            sessionService.removeAll();
        }
        return Result.ok();
    }

}
