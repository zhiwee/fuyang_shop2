package com.zhiwee.gree.webapi.Controller.GoodsInfo;

import com.zhiwee.gree.model.GoodsInfo.GoodsSpecsInfo;
import com.zhiwee.gree.model.GoodsInfo.GoodsSpecsItem;
import com.zhiwee.gree.service.GoodsInfo.GoodsSpecsItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.util.KeyUtils;
import xyz.icrab.common.web.param.IdParam;
import xyz.icrab.common.web.param.IdsParam;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

/**
 * 商品规格  周广
 */
@ResponseBody
@RequestMapping("/goodsSpecsItem")
@RestController
public class GoodsSpecsItemController {

    @Autowired
    private GoodsSpecsItemService goodsSpecsItemService;

    /***
     * 分页查询，周广
     * @param param
     * @param pagination
     * @return
     */
    @RequestMapping("/page")
    public Result<?> parentPage(@RequestBody Map<String, Object> param, Pagination pagination) {
        if (param == null) {
            param = new HashMap<>();
        }
        Pageable<GoodsSpecsItem> pageable = goodsSpecsItemService.getPage(param, pagination);
        return Result.ok(pageable);
    }

    /**
     * 增加 周广
     * @param goodsSpecsItem
     * @return
     */
    @RequestMapping("/add")
    public Result<?> add(@RequestBody GoodsSpecsItem goodsSpecsItem){
        goodsSpecsItem.setId(KeyUtils.getKey());
        goodsSpecsItemService.save(goodsSpecsItem);
        return Result.ok();
    }

    /**
     * 编辑  周广
     * @param goodsSpecsItem
     * @return
     */
    @RequestMapping("/update")
    public Result<?> update(@RequestBody GoodsSpecsItem goodsSpecsItem){
        goodsSpecsItemService.update(goodsSpecsItem);
        return Result.ok();
    }

    /***
     * 查询单个  周广
     * @param param
     * @return
     */
    @RequestMapping("/selectOne")
    @ResponseBody
    public Result<?> selectOne(@RequestBody IdParam param){
        return Result.ok(goodsSpecsItemService.get(param.getId()));
    }

    /***
     * 删除 周广
     * @param param
     * @return
     */
    @RequestMapping("/delete")
    @ResponseBody
    public Result<?> delete(@RequestBody @Valid IdsParam param){
        for (String id : param.getIds()) {
            GoodsSpecsItem goodsSpecsItem=new GoodsSpecsItem();
            goodsSpecsItem.setId(id);
            goodsSpecsItemService.delete(goodsSpecsItem);
        }
        return Result.ok();
    }
}
