package com.zhiwee.gree.webapi.Controller.Integral;


import com.zhiwee.gree.model.Integral.Integral;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.User;
import com.zhiwee.gree.service.Integral.IntegralService;
import com.zhiwee.gree.webapi.util.SystemControllerLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.web.annotation.*;
import xyz.icrab.common.web.param.IdParam;
import xyz.icrab.common.web.util.Validator;
import xyz.icrab.ums.util.consts.SessionKeys;
import javax.validation.Valid;
import java.util.Date;


/**
 * @author sun on 2019/5/6
 */
@RestController
@RequestMapping("/integral")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class IntegralController {
       @Autowired
       private IntegralService integralService;






    /**
     * Description:获取积分信息
     * @author: sun
     * @Date 下午12:51 2019/5/6
     * @param:
     * @return:
     */

    @RequestMapping(value = "/achieveIntegral")
    public Result<?> achieveIntegral(Pagination pagination){
        Pageable<Integral> integras = integralService.achieveIntegral(pagination);
         return Result.ok(integras);
    }


    /**
     * Description:更新积分
     * @author: sun
     * @Date 下午12:51 2019/5/6
     * @param:
     * @return:
     */

    @RequestMapping(value = "/updateIntegral")
    public Result<?> updateIntegral(@Session(SessionKeys.USER) User user,@RequestBody Integral integral){
        Validator validator = new Validator();
        validator.notEmpty(integral.getId(), "请重新选择");
        validator.notNull(integral.getDivisor(), "比例不能为空");
        validator.notNull(integral.getDividend(), "比例不能为空");
        integral.setUpdateTime(new Date());
        integral.setUpdateMan(user.getId());
         integralService.update(integral);
        return Result.ok();
    }



    /**
     * Description:获取积分
     * @author: sun
     * @Date 下午12:51 2019/5/6
     * @param:
     * @return:
     */

    @RequestMapping(value = "/achieveIntegralById")
    public Result<?> achieveIntegralById(@RequestBody @Valid IdParam map){
        Integral integral = integralService.get(map.getId());
        return Result.ok(integral);
    }



    /**
     * Description: 启用积分信息
     * @author: sun
     * @Date 下午2:00 2019/5/7
     * @param:
     * @return:
     */
    @RequestMapping("/open")
    public  Result<?> open(@RequestBody @Valid IdParam map) {
        Integral integral = new Integral();
        integral.setId(map.getId());
        integral.setState(1);
        integralService.update(integral);
        return Result.ok();
    }

    /**
     * Description: 禁用积分信息
     * @author: sun
     * @Date 下午2:00 2019/5/7
     * @param:
     * @return:
     */
    @RequestMapping("/stop")
    public  Result<?> stop(@RequestBody @Valid IdParam map) {
        Integral integral = new Integral();
        integral.setId(map.getId());
        integral.setState(2);
        integralService.update(integral);
        return Result.ok();
    }


    /**
     * @author tzj
     * @description 用户购买成功后改变积分
     * @date 2019/12/27 12:44
    */

    @RequestMapping(value = "/changeIntegral")
    public Result<?> changeIntegral(@Session("ShopUser")ShopUser shopUser,Double totalPrice ){
            integralService.changeIntegral(shopUser, totalPrice);
        return Result.ok();
    }

}
