package com.zhiwee.gree.webapi.Controller.shopBase;

import com.zhiwee.gree.model.Attachment;
import com.zhiwee.gree.model.User;
import com.zhiwee.gree.service.shopBase.AttachmentService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.NoHandlerFoundException;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.util.oss.OSSUtils;
import xyz.icrab.common.web.annotation.EnableDeleteRequest;
import xyz.icrab.common.web.annotation.EnableGetRequest;
import xyz.icrab.common.web.annotation.EnableListRequest;
import xyz.icrab.common.web.annotation.EnablePageRequest;
import xyz.icrab.common.web.annotation.PermissionMode;
import xyz.icrab.common.web.annotation.Session;
import xyz.icrab.common.web.util.KeyUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@ResponseBody
@RequestMapping("/attachment")
@Controller
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class AttachmentController {
    @Autowired
    private AttachmentService attachmentService;
    @Resource
    private OSSUtils ossUtils;
    @Value("${bucketName}")
    String bucketName;

    @RequestMapping("/upload")
    public Result<?> add(HttpServletRequest request, @Session("user") User user) {
        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver(request.getSession().getServletContext());
        if (multipartResolver.isMultipart(request)) {
            List<Attachment> list = new ArrayList<>();
            List<Map<String,String>> result = new ArrayList<>();
            MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
            Iterator iter = multiRequest.getFileNames();
            while (iter.hasNext()) {
                List<MultipartFile> files = multiRequest.getFiles(iter.next().toString());
                for (MultipartFile file : files) {
                    if (file != null) {
                        Attachment atta = new Attachment();
                        Map<String,String> map = new HashMap<>();
                        String id = KeyUtils.getKey();
                        atta.setId(id);
                        String suffix= file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".")+1);
                        atta.setOriginalname(file.getOriginalFilename());
                        //周广改过
                        //atta.setSize(file.getSize());
                        atta.setType(suffix);
                        atta.setCreatetime(new Date());
                        if(user!=null) {
                            atta.setCreateman(user.getId());
                            atta.setCreatename(user.getUsername());
                        }
                        InputStream inputStream = null;
                        try {
                            inputStream = file.getInputStream();
                            String path = DateFormatUtils.format(new Date(), "yyyy/MM/dd") + "/" + KeyUtils.getKey()+"."+suffix;
                            atta.setPath(path);
                            ossUtils.uploadByInputStream(bucketName, path, inputStream);
                            map.put("id",id);
                            map.put("path",path);
                            result.add(map);
                        } catch (IOException e) {
                            e.printStackTrace();
                        } finally {
                            if (inputStream != null) {
                                try {
                                    inputStream.close();
                                } catch (IOException e) {
                                }
                            }
                        }
                        list.add(atta);
                    }
                }
            }
            if(list.size()==1){
                attachmentService.save(list.get(0));
                return Result.ok(result.get(0));
            }else if (list.size()>1){
                attachmentService.save(list);
                return Result.ok(result);
            }
            return Result.ok("上传成功!");
        } else {
            return Result.of(Status.ClientError.UPGRADE_REQUIRED, "无附件");
        }
    }

    @RequestMapping("/bulkUpload")
    public Result<?> bulkUpload(HttpServletRequest request, @Session("user") User user) {
        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver(request.getSession().getServletContext());
        if (multipartResolver.isMultipart(request)) {
            List<Attachment> list = new ArrayList<>();
            List<Map<String,String>> result = new ArrayList<>();
            MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
            Iterator iter = multiRequest.getFileNames();
            while (iter.hasNext()) {
                List<MultipartFile> files = multiRequest.getFiles(iter.next().toString());
                for (MultipartFile file : files) {
                    if (file != null) {
                        Attachment atta = new Attachment();
                        Map<String,String> map = new HashMap<>();
                        String id = KeyUtils.getKey();
                        atta.setId(id);
                        String suffix= file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".")+1);
                        atta.setOriginalname(file.getOriginalFilename());
                        //周广改过
                        //atta.setSize(file.getSize());
                        atta.setType(suffix);
                        atta.setCreatetime(new Date());
                        if(user!=null) {
                            atta.setCreateman(user.getId());
                            atta.setCreatename(user.getUsername());
                        }
                        InputStream inputStream = null;
                        try {
                            inputStream = file.getInputStream();
                            String path = DateFormatUtils.format(new Date(), "yyyy/MM/dd") + "/" + KeyUtils.getKey()+"."+suffix;
                            atta.setPath(path);
                            ossUtils.uploadByInputStream(bucketName, path, inputStream);
                            map.put("id",id);
                            map.put("path",path);
                            result.add(map);
                        } catch (IOException e) {
                            e.printStackTrace();
                        } finally {
                            if (inputStream != null) {
                                try {
                                    inputStream.close();
                                } catch (IOException e) {
                                }
                            }
                        }
                        list.add(atta);
                    }
                }
            }
            attachmentService.save(list);
            return Result.ok(result);
        } else {
            return Result.of(Status.ClientError.UPGRADE_REQUIRED, "无附件");
        }
    }
    @RequestMapping("/whiteBulkUpload")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> whiteBulkUpload(HttpServletRequest request) {
        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver(request.getSession().getServletContext());
        if (multipartResolver.isMultipart(request)) {
            List<Attachment> list = new ArrayList<>();
            List<Map<String,String>> result = new ArrayList<>();
            MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
            Iterator iter = multiRequest.getFileNames();
            while (iter.hasNext()) {
                List<MultipartFile> files = multiRequest.getFiles(iter.next().toString());
                for (MultipartFile file : files) {
                    if (file != null) {
                        Attachment atta = new Attachment();
                        Map<String,String> map = new HashMap<>();
                        String id = KeyUtils.getKey();
                        atta.setId(id);
                        String suffix= file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".")+1);
                        atta.setOriginalname(file.getOriginalFilename());
                        //周广改过
                        //atta.setSize(file.getSize());
                        atta.setType(suffix);
                        atta.setCreatetime(new Date());
                        InputStream inputStream = null;
                        try {
                            inputStream = file.getInputStream();
                            String path = DateFormatUtils.format(new Date(), "yyyy/MM/dd") + "/" + KeyUtils.getKey()+"."+suffix;
                            atta.setPath(path);
                            ossUtils.uploadByInputStream(bucketName, path, inputStream);
                            map.put("id",id);
                            map.put("path",path);
                            result.add(map);
                        } catch (IOException e) {
                            e.printStackTrace();
                        } finally {
                            if (inputStream != null) {
                                try {
                                    inputStream.close();
                                } catch (IOException e) {
                                }
                            }
                        }
                        list.add(atta);
                    }
                }
            }
            attachmentService.save(list);
            return Result.ok(result);
        } else {
            return Result.of(Status.ClientError.UPGRADE_REQUIRED, "无附件");
        }
    }

    @RequestMapping("/page")
    @ResponseBody
    public <T> Result<?> page(@RequestBody(required = false) Map<String, Object> param, Pagination pagination) throws NoHandlerFoundException {
        if (StringUtils.isNotBlank((String) param.get("endTime"))) {
            param.put("endTime", (String) param.get("endTime") + " 23:59:59");
        }
        Pageable<Attachment> page = attachmentService.page(param, pagination);
        return Result.ok(page);
    }
}
