package com.zhiwee.gree.webapi.Controller.GoodsInfo;

import com.zhiwee.gree.model.GoodsInfo.GoodsClassify;
import com.zhiwee.gree.model.GoodsInfo.GoodsSpecsItemOrder;
import com.zhiwee.gree.model.GoodsInfo.ProductAttribute;
import com.zhiwee.gree.service.GoodsInfo.GoodsClassifyService;
import com.zhiwee.gree.service.GoodsInfo.ProductAttributeService;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.util.KeyUtils;
import xyz.icrab.common.web.annotation.*;
import xyz.icrab.common.web.param.IdParam;
import xyz.icrab.common.web.param.IdsParam;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jick on 2019/8/28.
 */
@RestController
@RequestMapping("/attribute")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class ProductAttributeController {

    @Autowired
    private GoodsClassifyService goodsClassifyService;


    @Autowired
    private ProductAttributeService  productAttributeService;


    /**
     * 获取模型列表
     * @return  http://localhost:8080/goodsCategory/parentPage
     */
    @RequestMapping("/modeList")
    @PermissionMode(PermissionMode.Mode.White)
    public List<GoodsClassify> queryListByRoot(){
      Map<String, Object> params  = new HashedMap();
        return  goodsClassifyService.queryListByRoot(params);
    }

    /**
     *
     *   if (param == null) {
     param = new HashMap<>();
     }
     param.put("type", 1);
     Pageable<GoodsCategory> pageable = goodsCategoryService.getPage(param, pagination);
     return Result.ok(pageable);
     */


    /**
     * 属性信息入库
     */

    /**
     *
     *
     */
    @RequestMapping(value = "/save")
    public Result<?>  productAttributeSave(@RequestBody(required = false) ProductAttribute productAttribute){

       //切割重新赋值
        /*String[]  str = productAttribute.getMode().split("/");
        productAttribute.setMode(str[0]);
        productAttribute.setModeId(str[1].trim());*/
         productAttribute.setId(KeyUtils.getKey());

         //对option值进行重新拼接操作,目的将将每一级之间加上一个空格
        String[]   str2 = productAttribute.getOptional().split("\n");
        String  newStr2 ="";
        for(String s:str2){
            newStr2+=" "+s;
        }

        productAttribute.setOptional(newStr2.trim());


         //存入数据库
         productAttributeService.save(productAttribute);
         return    Result.ok();
    }


    /**
     * 更新
     */
    @RequestMapping(value = "/update")
    public Result<?>  productAttributeUpdte(@RequestBody(required = false) ProductAttribute productAttribute){



        //对option值进行重新拼接操作,目的将将每一级之间加上一个空格
        String[]   str2 = productAttribute.getOptional().split("\n");
        String  newStr2 ="";
        for(String s:str2){
            newStr2+=" "+s;
        }

        productAttribute.setOptional(newStr2.trim());


        //存入数据库
        productAttributeService.update(productAttribute);
        return    Result.ok();
    }



    /**
     * 返回列表
     */

    @RequestMapping(value = "searchAll")
    public Result<?>  searchAll(@RequestBody(required = false) Map<String, Object> params,Pagination pagination) {
        Pageable<ProductAttribute>  list =   productAttributeService.queryList(params,pagination);
        return Result.ok(list);
    }

    /**
     *
     */

    @RequestMapping("/get")
    @PermissionMode(PermissionMode.Mode.White)
    @ResponseBody
    public   Result<?>  getMe(@RequestBody @Valid IdParam map){
        ProductAttribute   productAttribute   =  productAttributeService.get(map.getId());
        return  Result.ok(productAttribute);
    }

    /***
     * 删除
     * @param param
     * @return
     */
    @RequestMapping("/delete")
    @ResponseBody
    public Result<?> delete(@RequestBody @Valid IdsParam param){
        for (String id : param.getIds()) {
            ProductAttribute productAttribute=new ProductAttribute();
            productAttribute.setId(id);
            productAttributeService.delete(productAttribute);
        }
        return Result.ok();
    }


    /**
     * 确认会销商品 周广
     * @param param
     * @return
     */
    @RequestMapping("/selectBycategoryId")
    public Result<?> selectBycategoryId(@RequestBody Map<String, Object> param){
        if (param == null) {
            param = new HashMap<>();
        }
        if(param.get("categoryId")==null){
            return Result.of(Status.ClientError.BAD_REQUEST,"请检查商品是否存在分类");
        }
        List<ProductAttribute> productAttributeList=productAttributeService.selectBycategoryId(param);
        return Result.ok(productAttributeList);
    }

    /**
     * 商城前端使用 周广
     * @param
     * @return
     */
    @RequestMapping("/getAttribute")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> getAttribute(String categoryId){
        if (StringUtils.isBlank(categoryId)){
            return Result.of(Status.ClientError.BAD_REQUEST,"系统未检测到该分类");
        }
        Map<String,Object> param=new HashMap<>();
        param.put("categoryId",categoryId);
        List<ProductAttribute> productAttributeList=productAttributeService.selectBycategoryId(param);
        return Result.ok(productAttributeList);
    }

}
