package com.zhiwee.gree.webapi.framework;

/**
 * @author sun on 2019/7/12
 */
public class HandleNoSessionException extends RuntimeException {
    public HandleNoSessionException() {
    }

    public HandleNoSessionException(String message) {
        super(message);
    }

    public HandleNoSessionException(String message, Throwable cause) {
        super(message, cause);
    }

    public HandleNoSessionException(Throwable cause) {
        super(cause);
    }

    public HandleNoSessionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
