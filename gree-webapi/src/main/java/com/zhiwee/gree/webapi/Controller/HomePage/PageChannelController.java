package com.zhiwee.gree.webapi.Controller.HomePage;

import com.zhiwee.gree.model.GoodsInfo.GoodVo.GategoryVo;
import com.zhiwee.gree.model.HomePage.PageChannel;
import com.zhiwee.gree.model.HomePage.PageNav;
import com.zhiwee.gree.model.HomePage.PageV0.PageChannelAllVO;
import com.zhiwee.gree.model.HomePage.PageV0.PageChannelVO;
import com.zhiwee.gree.service.HomePage.PageChannelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.util.KeyUtils;
import xyz.icrab.common.web.annotation.PermissionMode;
import xyz.icrab.common.web.param.IdParam;
import xyz.icrab.common.web.param.IdsParam;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/pageChannel")
public class PageChannelController {

    @Autowired
    private PageChannelService pageChannelService;

    /**
     * 前端使用 接口  周广
     * @return
     */
    @RequestMapping("/getList")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> getList(){
      Map<String,Object> param = new HashMap<>();
      param.put("state","1");
      List<PageChannelVO> pageChannelVOList=pageChannelService.getlist(param);

      List<PageChannelVO> onePageChannelVO=new ArrayList<>();
      List<PageChannelVO> twoPageChannelVO=new ArrayList<>();
      List<PageChannelVO> sreePageChannelVO=new ArrayList<>();
      List<PageChannelVO> fourPageChannelVO=new ArrayList<>();
      List<PageChannelVO> fivePageChannelVO=new ArrayList<>();

      for (PageChannelVO pageChannelVO:pageChannelVOList) {
            if(pageChannelVO.getType()==1){
                fourPageChannelVO.add(pageChannelVO);
            }else if(pageChannelVO.getType()==2){
                onePageChannelVO.add(pageChannelVO);
            }else if(pageChannelVO.getType()==3){
                fivePageChannelVO.add(pageChannelVO);
            } else if(pageChannelVO.getType()==4){
                twoPageChannelVO.add(pageChannelVO);
            } else if(pageChannelVO.getType()==5){
                sreePageChannelVO.add(pageChannelVO);
            }
        }
        PageChannelAllVO onePageChannelAllVO=new PageChannelAllVO();
        onePageChannelAllVO.setSonlist(onePageChannelVO);
        PageChannelAllVO twoPageChannelAllVO=new PageChannelAllVO();
        twoPageChannelAllVO.setSonlist(twoPageChannelVO);
        PageChannelAllVO sreePageChannelAllVO=new PageChannelAllVO();
        sreePageChannelAllVO.setSonlist(sreePageChannelVO);
        PageChannelAllVO fourPageChannelAllVO=new PageChannelAllVO();
        fourPageChannelAllVO.setSonlist(fourPageChannelVO);
        PageChannelAllVO fivePageChannelAllVO=new PageChannelAllVO();
        fivePageChannelAllVO.setSonlist(fivePageChannelVO);
        List<PageChannelAllVO> pageChannelAlls=new ArrayList<>();
        pageChannelAlls.add(onePageChannelAllVO);
        pageChannelAlls.add(twoPageChannelAllVO);
        pageChannelAlls.add(sreePageChannelAllVO);
        pageChannelAlls.add(fourPageChannelAllVO);
        pageChannelAlls.add(fivePageChannelAllVO);
      return  Result.ok(pageChannelAlls);
    }

    /***
     * 分页查询，周广
     * @param param
     * @param pagination
     * @return
     */
    @RequestMapping("/page")
    public Result<?> page(@RequestBody Map<String, Object> param, Pagination pagination) {
        if (param == null) {
            param = new HashMap<>();
        }
        Pageable<PageChannel> pageable = pageChannelService.getPage(param, pagination);
        return Result.ok(pageable);
    }

    /**
     * 增加  周广
     * @param pageChannel
     * @return
     */
    @RequestMapping("/add")
    public Result<?> add(@RequestBody PageChannel pageChannel){
        pageChannel.setId(KeyUtils.getKey());
        pageChannelService.save(pageChannel);
        return Result.ok();
    }
    /**
     * 更新  周广
     * @param pageChannel
     * @return
     */
    @RequestMapping("/update")
    public Result<?> update(@RequestBody PageChannel pageChannel){
        pageChannelService.update(pageChannel);
        return Result.ok();
    }

    /***
     * 查询单个  周广
     * @param param
     * @return
     */
    @RequestMapping("/selectOne")
    @ResponseBody
    public Result<?> selectOne(@RequestBody IdParam param){
        return Result.ok(pageChannelService.get(param.getId()));
    }

    /***
     * 启用  周广
     * @param param
     * @return
     */
    @RequestMapping("/enable")
    @ResponseBody
    public Result<?> enable(@RequestBody @Valid IdsParam param){
        for (String id : param.getIds()) {
            PageChannel pageChannel=new PageChannel();
            pageChannel.setId(id);
            pageChannel.setState(1);
            pageChannelService.update(pageChannel);
        }
        return Result.ok();
    }

    /***
     * 禁用  周广
     * @param param
     * @return
     */
    @RequestMapping("/disable")
    @ResponseBody
    public Result<?> disable(@RequestBody @Valid IdsParam param){
        for (String id : param.getIds()) {
            PageChannel pageChannel=new PageChannel();
            pageChannel.setId(id);
            pageChannel.setState(2);
            pageChannelService.update(pageChannel);
        }
        return Result.ok();
    }


    /***
     * 删除 周广
     * @param param
     * @return
     */
    @RequestMapping("/delete")
    @ResponseBody
    public Result<?> delete(@RequestBody @Valid IdsParam param){
        for (String id : param.getIds()) {
            PageChannel pageChannel=new PageChannel();
            pageChannel.setId(id);
            pageChannelService.delete(pageChannel);
        }
        return Result.ok();
    }
}
