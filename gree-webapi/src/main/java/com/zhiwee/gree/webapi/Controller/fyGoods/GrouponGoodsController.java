package com.zhiwee.gree.webapi.Controller.fyGoods;


import com.zhiwee.gree.model.FyGoods.GrouponGoods;
import com.zhiwee.gree.model.GoodsInfo.Goods;
import com.zhiwee.gree.model.GoodsInfo.GoodsBase;
import com.zhiwee.gree.model.GoodsInfo.GoodsInfoPicture;
import com.zhiwee.gree.model.OnlineActivity.OnlineActivityInfo;
import com.zhiwee.gree.model.WxPay.WeChatConfig;
import com.zhiwee.gree.service.FyGoods.GrouponGoodsService;
import com.zhiwee.gree.service.FyGoods.LShopUserGroupService;
import com.zhiwee.gree.service.GoodsInfo.GoodsBaseService;
import com.zhiwee.gree.service.GoodsInfo.GoodsInfoPictureService;
import com.zhiwee.gree.service.GoodsInfo.GoodsService;
import com.zhiwee.gree.webapi.util.HttpUtil;
import net.sf.json.JSONObject;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.util.KeyUtils;
import xyz.icrab.common.web.annotation.*;
import xyz.icrab.common.web.param.IdParam;

import javax.annotation.Resource;
import java.util.*;

/**
 * @author DELL
 */
@RestController
@RequestMapping("/grouponGoods")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class GrouponGoodsController {
    @Resource
    private LShopUserGroupService lShopUserGroupService;
    @Resource
    private GrouponGoodsService grouponGoodsService;
    @Resource
    private GoodsService goodsService;
    @Resource
    private GoodsInfoPictureService goodsInfoPictureService;
    @Resource
    private GoodsBaseService goodsBaseService;
    /**
     * @author tzj
     * @description 获取列表
     * @date 2020/4/26 8:56
     */
    @RequestMapping("/list")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> getFyGoodsList(Pagination pagination) {
        Map<String, Object> param = new HashMap<>(10);
        param.put("dateTime",new Date());
        param.put("header",1);
        List<GrouponGoods> list = grouponGoodsService.queryGoodsList(param,pagination);
        if (CollectionUtils.isNotEmpty(list)){
            for (GrouponGoods next : list) {
                Goods goods = goodsService.get(next.getGoodsId());
                if (goods!=null){
                    Map<String, Object> params=new HashMap<>(5);
                    params.put("goodsId",goods.getGoodsId());
                    List<String> list1=new ArrayList<>(20);
                    List<GoodsInfoPicture> goodsInfoPictures = goodsInfoPictureService.listAll(params);
                    if (CollectionUtils.isNotEmpty(goodsInfoPictures)){
                        for (GoodsInfoPicture goodsInfoPicture : goodsInfoPictures) {
                            list1.add(goodsInfoPicture.getImgUrl());
                        }
                    }
                    GoodsBase goodsBase = goodsBaseService.get(goods.getGoodsId());
                    goods.setGoodsBase(goodsBase);
                    goods.setUrlList(list1);
                    next.setGoods(goods);
                }
            }
        }
        return Result.ok(list);
    }

    @RequestMapping("/list2")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> getFyGoodsList2(Pagination pagination) {
        Map<String, Object> param = new HashMap<>(10);
        Pageable<GrouponGoods> list = grouponGoodsService.page(param, pagination);
        return Result.ok(list);
    }

    /**
     * @author tzj
     * @description 新增编辑商品
     * @date 2020/4/26 8:57
     */
    @RequestMapping("/saveOrUpdate")
    public Result<?> skillGoodsSaveController(@RequestBody GrouponGoods grouponGoods) {
        if (null == grouponGoods.getLimitCount()) {
            grouponGoods.setLimitCount(1);
        }
        //如果id为空，则是添加操作
        if (grouponGoods.getId() == null || "".equals(grouponGoods.getId().trim()) || grouponGoods.getId().trim().length() == 0) {
            //确保同一时间段只有一个商品
            Boolean result = checkOnly(grouponGoods);
            if (!result){
                return Result.of(Status.ClientError.BAD_REQUEST,"与已经存在的拼团商品冲突,请检查");
            }
            grouponGoods.setId(KeyUtils.getKey());
            grouponGoods.setState(1);
            grouponGoodsService.save(grouponGoods);
        } else {
            //如果id不为空则是更新操作
            //确保同一时间段只有一个商品
            Boolean result = checkOnly(grouponGoods);
            if (!result){
                return Result.of(Status.ClientError.BAD_REQUEST,"与已经存在的拼团商品冲突,请检查");
            }
            grouponGoods.setState(1);
            grouponGoodsService.update(grouponGoods);
        }
        return Result.ok(grouponGoods);
    }

    /**
     * @author tzj
     * @description
     * @date 2020/4/26 15:25
     */
    private Boolean checkOnly(GrouponGoods grouponGoods) {
        Map<String,Object> param=new HashMap<>(6);
        param.put("goodsId",grouponGoods.getGoodsId());
        List<GrouponGoods> list = grouponGoodsService.list(param);
        //是否为空
        if (CollectionUtils.isNotEmpty(list)) {
            //大于1直接返回
            if (list.size()>1){
                return false;
            }
            GrouponGoods groupGoods1 = list.get(0);
            //id相同
            if (!grouponGoods.getId().equals(groupGoods1.getId())) {
                    //如果开始时间在他们中间则不行
                    return (!grouponGoods.getStartTime().after(groupGoods1.getStartTime()) || !grouponGoods.getStartTime().before(groupGoods1.getEndTime())) &&
                            //如果开始时间和结束时间都超过不行
                            (!grouponGoods.getStartTime().before(groupGoods1.getStartTime()) || !grouponGoods.getEndTime().after(groupGoods1.getEndTime())) &&
                            //如果结束时间在它开始之后且 在它结束时间之前 不行
                            (!grouponGoods.getEndTime().after(groupGoods1.getStartTime()) || !grouponGoods.getEndTime().before(groupGoods1.getEndTime()));
            }else {
                return true;
            }

        }else {
            return true;
        }
    }


    /**
     * @author tzj
     * @description 通过ID查询信息
     * @date 2020/4/26 8:58
     */
    @RequestMapping("/get")
    public Result<?> get(@RequestBody IdParam param) {
        GrouponGoods grouponGoods = grouponGoodsService.get(param.getId());
        return Result.ok(grouponGoods);
    }

    /**
     * @author tzj
     * @description 删除
     * @date 2020/4/26 13:20
     */
    @RequestMapping("/delete")
    public Result<?> fyGoodsDelete(@RequestBody IdParam idParam) {
        //删除商品表的数据
        GrouponGoods grouponGoods = new GrouponGoods();
        grouponGoods.setId(idParam.getId());
        grouponGoodsService.delete(grouponGoods);
        return Result.ok();
    }



    /**
     * @author tzj
     * @description 发送订阅消息
     * @date 2020/4/27 10:12
    */
    @RequestMapping("/getMsg")
    @PermissionMode(PermissionMode.Mode.White)
    public   Result<?>  getMsg( @RequestBody(required = false)  Map<String,Object>  param ){
        String grant_type="client_credential";
        String appid= WeChatConfig.APPIDFuYang;
        String secret=  WeChatConfig.APIKEYFuYang;
        String url="https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&&appid=wx575dba0ef8ff034f&&secret=7f7f89ad5023ce314a4ba5b6496201b8";
        String resultMap = HttpUtil.sendGet(url);
        if (null == resultMap) {
            return Result.of(Status.ClientError.BAD_REQUEST, "请求失败，请重试");
        }
        Map<Object, Object> result1 = jsonToMap(resultMap);
        String access_token= (String) result1.get("access_token");
        String data="{\n" +
                "\t\"touser\":\"oobK-4pqLM2RByv_i-cOm-cFRLGQ\",\n" +
                "\t\"template_id\":\"ISUye1-_o__RzWkJPpcx-AIEAKZsHIa-6o1BvaxWOd0\",\n" +
                "\t\"data\": \" 'thing1': { 'value': 智能手机}, 'thing6': { 'value': 恭喜您拼团}\"\n" +
                "}";
        String url2= "https://api.weixin.qq.com/cgi-bin/message/subscribe/send?access_token="+access_token;
        String s = HttpUtil.doPost(url2, data, 4000);
        System.out.println(s);
        return  Result.ok();
    }









    /**
     * json string 转换为 map 对象
     * @param jsonObj
     * @return
     */
    public static Map<Object, Object> jsonToMap(Object jsonObj) {
        JSONObject jsonObject = JSONObject.fromObject(jsonObj);
        Map<Object, Object> map = (Map)jsonObject;
        return map;
    }





}
