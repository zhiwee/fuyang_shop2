package com.zhiwee.gree.webapi.Controller.shopBase;

import com.zhiwee.gree.model.Interface;
import com.zhiwee.gree.model.User;
import com.zhiwee.gree.model.enums.UserType;
import com.zhiwee.gree.model.ret.InterfaceReturnMsg;
import com.zhiwee.gree.service.shopBase.InterfaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.Page;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Relation;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.model.enums.YesOrNo;
import xyz.icrab.common.web.annotation.EnableDeleteRequest;
import xyz.icrab.common.web.annotation.EnableGetRequest;
import xyz.icrab.common.web.annotation.EnableListRequest;
import xyz.icrab.common.web.annotation.EnablePageRequest;
import xyz.icrab.common.web.annotation.Session;
import xyz.icrab.common.web.message.ValidatedMessage;
import xyz.icrab.common.web.param.IdParam;
import xyz.icrab.common.web.util.KeyUtils;
import xyz.icrab.common.web.util.Validator;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author gll
 * @since 2018/4/21 17:11
 */
@RestController
@RequestMapping("/interface")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class InterfaceController {

    @Autowired
    private InterfaceService interfaceService;
    @RequestMapping("/add")
    @ResponseBody
    public <T> Result<?> add(@RequestBody(required = false) Map<String, Object> param){
    	String name = (String)param.get("name");
    	String moduleid = (String)param.get("moduleid");
    	String url = (String)param.get("url");
    	Validator validator = new Validator();
    	validator.notEmpty(name, InterfaceReturnMsg.NAME_EMPTY.message());
    	validator.notEmpty(moduleid, InterfaceReturnMsg.MODULEID_EMPTY.message());
    	validator.notEmpty(url, InterfaceReturnMsg.URL_EMPTY.message());
    	if(validator.isError()){
    		return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
    	}

    	Interface i = new Interface();
    	i.setId(KeyUtils.getKey());
    	i.setCheckauth(YesOrNo.YES);
    	i.setModuleid(moduleid);
    	i.setName(name);
    	i.setUrl(url);
    	interfaceService.save(i);
        return Result.ok();
    }

    @RequestMapping("/update")
    @ResponseBody
    public <T> Result<?> update(@RequestBody(required = false) Map<String, Object> param){
    	String id = (String)param.get("id");
    	String name = (String)param.get("name");
    	String moduleid = (String)param.get("moduleid");
    	String url = (String)param.get("url");
    	Validator validator = new Validator();
    	validator.notEmpty(id, ValidatedMessage.EMPTY_ID);
    	validator.notEmpty(name, InterfaceReturnMsg.NAME_EMPTY.message());
    	validator.notEmpty(moduleid, InterfaceReturnMsg.MODULEID_EMPTY.message());
    	validator.notEmpty(url, InterfaceReturnMsg.URL_EMPTY.message());
    	if(validator.isError()){
    		return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
    	}

    	Interface i = new Interface();
    	i.setId(id);
    	i.setModuleid(moduleid);
    	i.setName(name);
    	i.setUrl(url);
    	interfaceService.update(i);
        return Result.ok();
    }


    @RequestMapping("/addUserInterface")
    @ResponseBody
    public <T> Result<?> addUserInterface(@RequestBody(required = false) Map<String, Object> param , @Session("user") User user){
    	String interfaceids = (String)param.get("interfaceids");
    	String userid = (String)param.get("userid");
    	Validator validator = new Validator();
    	validator.notEmpty(userid, InterfaceReturnMsg.USERID_EMPTY.message());
    	validator.notEmpty(interfaceids, InterfaceReturnMsg.INTERFACEIDS_EMPTY.message());
    	if(validator.isError()){
    		return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
    	}
    	List<Relation> result = new ArrayList<>();
		String[] strList = interfaceids.split(",");
    	for(String interfaceid : strList){
        	Relation r = new Relation();
    		r.setId(KeyUtils.getKey());
    		r.setMainId(userid);
    		r.setResourceId(interfaceid);
    		result.add(r);
    	}

    	interfaceService.saveUserInterface(result,user);
        return Result.ok();
    }

    @RequestMapping("/addRoleInterface")
    @ResponseBody
    public <T> Result<?> addRoleInterface(@RequestBody(required = false) Map<String, Object> param, @Session("user") User user){
    	String interfaceids = (String)param.get("interfaceids");
    	String roleid = (String)param.get("roleid");
    	Validator validator = new Validator();
    	validator.notEmpty(roleid, InterfaceReturnMsg.ROLEIDS_EMPTY.message());
    	validator.notEmpty(interfaceids, InterfaceReturnMsg.INTERFACEIDS_EMPTY.message());
    	if(validator.isError()){
    		return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
    	}
    	List<Relation> result = new ArrayList<>();
		String[] strList = interfaceids.split(",");
    	for(String interfaceid : strList){
        	Relation r = new Relation();
    		r.setId(KeyUtils.getKey());
    		r.setMainId(roleid);
    		r.setResourceId(interfaceid);
    		result.add(r);
    	}
    	interfaceService.saveRoleInterface(result,user);
        return Result.ok();
    }
    @RequestMapping("/open")
    @ResponseBody
    public <T> Result<?> open(@RequestBody @Valid IdParam map){
    	Interface i = new Interface();
    	i.setId(map.getId());
    	i.setCheckauth(YesOrNo.YES);
    	interfaceService.update(i);
        return Result.ok();
    }

    @RequestMapping("/stop")
    @ResponseBody
    public <T> Result<?> stop(@RequestBody @Valid IdParam map){
    	Interface i = new Interface();
    	i.setId(map.getId());
    	i.setCheckauth(YesOrNo.NO);
    	interfaceService.update(i);
        return Result.ok();
    }

    @RequestMapping("/getIntByRoleIds")
    @ResponseBody
    public <T> Result<List<Interface>> getIntByRoleIds(@RequestBody(required = false) Map<String, Object> param){
    	String roleids = (String)param.get("roleids");
    	Validator validator = new Validator();
    	validator.notEmpty(roleids, InterfaceReturnMsg.ROLEIDS_EMPTY.message());
    	if(validator.isError()){
    		return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
    	}
    	String[] roleidArr = roleids.split(",");
    	List<Interface> result = interfaceService.getIntByRoleIds(roleidArr);
        return Result.ok(result);
    }
    @RequestMapping("/getIntByUserId")
    @ResponseBody
    public <T> Result<List<Interface>> getIntByUserIds(@RequestBody(required = false) Map<String, Object> param){
    	String userid = (String)param.get("userid");
    	Validator validator = new Validator();
    	validator.notEmpty(userid, InterfaceReturnMsg.USERID_EMPTY.message());
    	if(validator.isError()){
    		return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
    	}
    	List<Interface> result = interfaceService.getIntByUserId(userid);
        return Result.ok(result);
    }

    @RequestMapping("/getPage")
    @ResponseBody
    public <T> Result<Page<Interface>> getPage(@RequestBody(required = false) Map<String, Object> param , @Session("user") User user ,Pagination pagination){
//    	if(user.getType() != UserType.Super.value()){

		//周广改过
		if(user.getType() != UserType.Super){
    		List<Interface> userIntList = interfaceService.getIntByUserId(user.getId());
			List<String> intids = new ArrayList<String>();
			for(Interface i : userIntList){
				intids.add(i.getId());
			}
			param.put("ids", intids);
			if(intids.size() == 0){
				return Result.ok(null);
			}
    	}
    	Page<Interface> result = (Page<Interface>) interfaceService.page(param, pagination);
        return Result.ok(result);
    }

    @RequestMapping("/getList")
    @ResponseBody
    public <T> Result<List<Interface>> getList(@RequestBody(required = false) Map<String, Object> param , @Session("user") User user){
    	//if(user.getType() != UserType.Super.value()){
		//周广改过
		if(user.getType() != UserType.Super){
    		List<Interface> userIntList = interfaceService.getIntByUserId(user.getId());
			List<String> intids = new ArrayList<String>();
			for(Interface i : userIntList){
				intids.add(i.getId());
			}
			param.put("ids", intids);
			if(intids.size() == 0){
				return Result.ok(null);
			}
    	}
    	List<Interface> result = interfaceService.list(param);
        return Result.ok(result);
    }
}
