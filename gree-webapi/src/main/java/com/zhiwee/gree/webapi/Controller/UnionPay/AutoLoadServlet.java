package com.zhiwee.gree.webapi.Controller.UnionPay;

import com.zhiwee.gree.model.UnionPay.SDKConfig;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

/**
 * Description: 加载配置文件的类，tomcat启动的时候就开始加载配置文件
 * @author: sun
 * @Date 下午2:50 2019/6/4
 * @param:
 * @return:
 */
public class AutoLoadServlet extends HttpServlet {

	@Override
	public void init(ServletConfig config) throws ServletException {

		SDKConfig.getConfig().loadPropertiesFromSrc();

		super.init();
	}











}
