package com.zhiwee.gree.webapi.Controller.OrderInfo;

import com.zhiwee.gree.model.ActivityTicketOrder.ActivityTicketOrder;
import com.zhiwee.gree.model.Alipay.AliRefund;
import com.zhiwee.gree.model.BaseInfo.BaseInfo;
import com.zhiwee.gree.model.Coupon.Coupon;
import com.zhiwee.gree.model.OrderInfo.*;
import com.zhiwee.gree.model.OrderInfo.vo.OrderBaseInfo;
import com.zhiwee.gree.model.OrderInfo.vo.OrderStatisticItem;
import com.zhiwee.gree.model.ShopDistributor.DistributorOrder;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.User;
import com.zhiwee.gree.service.ActivityTicketOrder.ActivityTicketOrderService;
import com.zhiwee.gree.service.BaseInfo.BaseInfoService;
import com.zhiwee.gree.service.Coupon.CouponService;
import com.zhiwee.gree.service.OrderInfo.OrderAddressService;
import com.zhiwee.gree.service.OrderInfo.OrderChangeService;
import com.zhiwee.gree.service.OrderInfo.OrderItemService;
import com.zhiwee.gree.service.OrderInfo.OrderService;
import com.zhiwee.gree.service.ShopDistributor.DistributorOrderService;
import com.zhiwee.gree.service.ShopUser.ShopUserService;
import com.zhiwee.gree.util.SequenceUtils;
import com.zhiwee.gree.webapi.OrderUtil.OrderUtil;
import com.zhiwee.gree.webapi.util.SystemControllerLog;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.jeecgframework.poi.excel.ExcelExportUtil;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.enmus.ExcelType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.util.IdGenerator;
import xyz.icrab.common.web.annotation.*;
import xyz.icrab.common.web.param.IdParam;
import xyz.icrab.common.web.param.IdsParam;
import xyz.icrab.common.web.util.Validator;
import xyz.icrab.ums.util.consts.SessionKeys;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * @author sun on 2019/4/30
 */
@RestController
@RequestMapping("/orderChange")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class OrderChangeController {

    @Autowired
    private OrderChangeService orderChangeService;




    /**
     * Description: 获取所有订单
     *
     * @author: sun
     * @Date 上午10:47 2019/5/12
     * @param:
     * @return:
     */
    @RequestMapping("/pageInfo")
    public Result<?> pageInfo(@RequestBody Map<String, Object> params, @Session(SessionKeys.USER) User user, Pagination pagination) {
        if (params == null) {
            params = new HashMap<>();
        }
        if (StringUtils.isNotBlank((String) params.get("endCreateTime"))) {
            params.put("endCreateTime", (String) params.get("endCreateTime") + "23:59:59");
        }
        Pageable<OrderChange> page = orderChangeService.pageInfo(params, pagination);
        return Result.ok(page);
    }


}




