package com.zhiwee.gree.webapi.Controller.fyGoods;

import com.zhiwee.gree.model.FyGoods.FygoodsCart;
import com.zhiwee.gree.model.GoodsInfo.Goods;
import com.zhiwee.gree.model.GoodsInfo.GoodsBand;
import com.zhiwee.gree.model.GoodsInfo.GoodsBase;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.service.GoodsInfo.GoodsBandService;
import com.zhiwee.gree.service.GoodsInfo.GoodsBaseService;
import com.zhiwee.gree.service.GoodsInfo.GoodsService;
import com.zhiwee.gree.webapi.util.DateUtil;
import com.zhiwee.gree.webapi.util.JsonUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.web.annotation.*;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

@RestController
@RequestMapping("/fyGoodsCart")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class FyCartController {
    @Resource
    private GoodsService goodsService;
    @Resource
    private GoodsBaseService goodsBaseService;
    @Resource
    private RedisTemplate<String,String>  redisTemplate;
    @Resource
    private GoodsBandService goodsBandService;
    /**
     * @Author: jick
     * @Date: 2020/1/9 17:36
     * 商品添加至购物车
     * 长益传递过来的商品，与秒杀有点差异
     */
    @RequestMapping("/add")
     public Result<?>  goodsAddToCart(@Session("ShopUser") ShopUser shopUser, @RequestBody(required = false) Map<String, Object> param){
        String typeStr= (String) param.get("type");
        Integer type=Integer.valueOf(typeStr);
         //根据id查寻数据信息
         Goods goods  =   goodsService.get((String)param.get("fyGoodsId"));

        if (goods==null){
             return  Result.of(Status.ClientError.BAD_REQUEST,"该商品不存在");
         }
        GoodsBase goodsBase = goodsBaseService.get(goods.getGoodsId());
        if (goodsBase.getIsdown()==2){
            return  Result.of(Status.ClientError.BAD_REQUEST,"该商品已经下架了");
        }
        GoodsBand goodsBand = goodsBandService.get(goodsBase.getBrandId());
        //首先判断redis值是否存在该商品，如果不存在直接添加，存在则修改更新
         HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();

         // 先查询,判断redis中是否有商品信息
         Map<String, String> resultMap = hashOperations.entries("userCart:"+shopUser.getId());

         //如果不为空
         if(!resultMap.isEmpty() && resultMap.size()>0){

             //redis中有数据，判断是否有对应的数据
             // 根据map的key获取value
             String  goodsStr = resultMap.get((String) param.get("fyGoodsId"));
             //判断Redis中是否有对应的商品信息，如果存在进行修改商品数量操作(id 型号)
             if(null != goodsStr  &&  goodsStr.length() > 0){
                 //转换成原始数据
                 FygoodsCart fygoodsCart   =   JsonUtil.jsonStr2Object(goodsStr, FygoodsCart.class);
                 //重新计算数量和总价格
                 fygoodsCart.setSendType(goods.getSendType());
                 fygoodsCart.setType(type);
                 fygoodsCart.setCount((Integer) param.get("count"));
                 fygoodsCart.setTotalPrice(fygoodsCart.getPrice().multiply(BigDecimal.valueOf(fygoodsCart.getCount())));
                 // 重新添加至map
                 resultMap.put(String.valueOf(param.get("fyGoodsId")), JsonUtil.object2JsonStr(fygoodsCart));
             }else{
                 //如果没有对应的商品则直接添加
                 //添加操作
                 FygoodsCart  fygoodsCart  =  new FygoodsCart();
                 fygoodsCart.setId(DateUtil.date2Str(new Date()));
                 fygoodsCart.setUserId(shopUser.getId());
                 fygoodsCart.setSendType(goods.getSendType());
                 fygoodsCart.setFyGoodsId(goods.getId());
                 fygoodsCart.setType(type);
                 fygoodsCart.setBrandName(goodsBand.getName());
                 fygoodsCart.setBrandUrl(goodsBand.getCover());
                 fygoodsCart.setPhoneNum(shopUser.getPhone());
                 fygoodsCart.setGoodsId(goods.getGoodsId());
                 fygoodsCart.setGoodsName(goods.getName());
                 fygoodsCart.setCover(goods.getGoodCover());
                 fygoodsCart.setPrice(goods.getShopPrice());
                 fygoodsCart.setJiadian(goods.getJiadian());
                 fygoodsCart.setLabel1id(goods.getLabel1id());
                 fygoodsCart.setLabel2id(goods.getLabel2id());
                 fygoodsCart.setLabel3id(goods.getLabel3id());
                 fygoodsCart.setLabel1value(goods.getLabel1value());
                 fygoodsCart.setLabel2value(goods.getLabel2value());
                 fygoodsCart.setLabel3value(goods.getLabel3value());
                 fygoodsCart.setLabel1parentId(goods.getLabel1parentId());
                 fygoodsCart.setLabel2parentId(goods.getLabel2parentId());
                 fygoodsCart.setLabel3parentId(goods.getLabel3parentId());
                 fygoodsCart.setLabel1parentName(goods.getLabel1parentName());
                 fygoodsCart.setLabel2parentName(goods.getLabel2parentName());
                 fygoodsCart.setLabel3parentName(goods.getLabel3parentName());
                 fygoodsCart.setCount((Integer) param.get("count"));
                 fygoodsCart.setTotalPrice(goods.getShopPrice());
                 // 新增商品购物车信息
                 resultMap.put(String.valueOf(param.get("fyGoodsId")), JsonUtil.object2JsonStr(fygoodsCart));
             }
         }else {
             //该用户购物车为空，则直接添加
             //添加操作
             FygoodsCart  fygoodsCart  =  new FygoodsCart();
             fygoodsCart.setId(DateUtil.date2Str(new Date()));
             fygoodsCart.setUserId(shopUser.getId());
             fygoodsCart.setSendType(goods.getSendType());
             fygoodsCart.setFyGoodsId(goods.getId());
             fygoodsCart.setPhoneNum(shopUser.getPhone());
             fygoodsCart.setGoodsId(goods.getGoodsId());
             fygoodsCart.setGoodsName(goods.getName());
             fygoodsCart.setType(type);
             fygoodsCart.setBrandName(goodsBand.getName());
             fygoodsCart.setBrandUrl(goodsBand.getCover());
             fygoodsCart.setJiadian(goods.getJiadian());
             fygoodsCart.setCover(goods.getGoodCover());
             fygoodsCart.setPrice(goods.getShopPrice());
             fygoodsCart.setLabel1id(goods.getLabel1id());
             fygoodsCart.setLabel2id(goods.getLabel2id());
             fygoodsCart.setLabel3id(goods.getLabel3id());
             fygoodsCart.setLabel1value(goods.getLabel1value());
             fygoodsCart.setLabel2value(goods.getLabel2value());
             fygoodsCart.setLabel3value(goods.getLabel3value());
             fygoodsCart.setLabel1parentId(goods.getLabel1parentId());
             fygoodsCart.setLabel2parentId(goods.getLabel2parentId());
             fygoodsCart.setLabel3parentId(goods.getLabel3parentId());
             fygoodsCart.setLabel1parentName(goods.getLabel1parentName());
             fygoodsCart.setLabel2parentName(goods.getLabel2parentName());
             fygoodsCart.setLabel3parentName(goods.getLabel3parentName());
             fygoodsCart.setCount((Integer) param.get("count"));
             fygoodsCart.setTotalPrice(goods.getShopPrice());
             // 新增商品购物车信息
             resultMap.put(String.valueOf(param.get("fyGoodsId")), JsonUtil.object2JsonStr(fygoodsCart));
         }
        hashOperations.putAll("userCart:"+shopUser.getId() , resultMap);
         return   Result.ok();

     }



}
