package com.zhiwee.gree.webapi.Controller.ShopDistributor;


import com.zhiwee.gree.model.ShopDistributor.DistributorArea;
import com.zhiwee.gree.model.ShopDistributor.DistributorCategory;
import com.zhiwee.gree.model.User;
import com.zhiwee.gree.service.ShopDistributor.DistributorAreaService;
import com.zhiwee.gree.service.ShopDistributor.DistributorCategoryService;
import com.zhiwee.gree.webapi.util.SystemControllerLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.util.IdGenerator;
import xyz.icrab.common.web.annotation.EnableGetRequest;
import xyz.icrab.common.web.annotation.EnableListRequest;
import xyz.icrab.common.web.annotation.Session;
import xyz.icrab.common.web.param.IdsParam;
import xyz.icrab.common.web.util.Validator;
import xyz.icrab.ums.util.consts.SessionKeys;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequestMapping("/distributorCategory")
@Controller
@ResponseBody
@EnableListRequest
@EnableGetRequest
public class ShopDistributorCategoryController {


    @Autowired
    private DistributorCategoryService distributorCategoryService;





    /**
     * Description: 添加配送商的配送的商品分类
     * @author: sun
     * @Date 下午11:52 2019/5/21
     * @param:
     * @return:
     */
    @SystemControllerLog(description="添加配送商的配送商的商品分类")
    @RequestMapping(value = "/add")
    @ResponseBody
    public Result<?> addShopDistributor(@RequestBody DistributorCategory distributorCategory, @Session(SessionKeys.USER) User user)  {
        Validator validator = new Validator();
        validator.notEmpty(distributorCategory.getCategoryId(), "商品分类不能为空");
        validator.notEmpty(distributorCategory.getDistributorId(), "配送商不能为空");
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }
        String[] categoryId =  distributorCategory.getCategoryId().split(",");
        List<DistributorCategory> distributorCategories = new ArrayList<>();
        DistributorCategory darea = null;
        Map<String,Object> params = new HashMap();


        for(String id  : categoryId ){
            darea = new DistributorCategory();
            params.put("categoryId",id);
            params.put("distributorId",distributorCategory.getDistributorId());

            List<DistributorCategory> list = distributorCategoryService.list(params);
            if(!list.isEmpty() && list.size() > 0){
                return Result.of(Status.ClientError.BAD_REQUEST, "存在添加的分类冲突了");
            }
            darea.setId(IdGenerator.objectId());
            darea.setDistributorId(distributorCategory.getDistributorId());
            darea.setCategoryId(id);
            darea.setState(1);
            distributorCategories.add(darea);
        }
        distributorCategoryService.save(distributorCategories);
        return Result.ok();
    }




    /**
     * Description: 获取配送商对应商品分类
     * @author: sun
     * @Date 下午11:52 2019/5/21
     * @param:
     * @return:
     */
    @RequestMapping(value = "/achieveCategory")
    @ResponseBody
    public Result<?> achieveCategory(@RequestBody Map<String, Object> param, Pagination pagination)  {

        Pageable<DistributorCategory>classifies = distributorCategoryService.achieveSmallCategory(param,pagination);
        return Result.ok(classifies);
    }



    /**
     * Description: 删除商品分类
     * @author: sun
     * @Date 下午11:52 2019/5/21
     * @param:
     * @return:
     */
    @RequestMapping(value = "/deleteCategory")
    @ResponseBody
    public Result<?> deleteCategory(@RequestBody IdsParam idsParam)  {
            distributorCategoryService.delete(idsParam.getIds());

        return Result.ok();
    }



}
