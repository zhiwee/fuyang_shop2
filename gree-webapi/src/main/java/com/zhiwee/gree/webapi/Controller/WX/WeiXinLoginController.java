package com.zhiwee.gree.webapi.Controller.WX;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import xyz.icrab.common.web.annotation.PermissionMode;


@Controller
public class WeiXinLoginController {

    RestTemplate restTemplate = new  RestTemplate();

    private String appid="wx575dba0ef8ff034f";

    private String secret="7f7f89ad5023ce314a4ba5b6496201b8";

    private String wxApiCode2Session="https://api.weixin.qq.com/sns/jscode2session?appid=wx575dba0ef8ff034f&secret=7f7f89ad5023ce314a4ba5b6496201b8&js_code={1}&grant_type=authorization_code";



    /**
     * TODO【微信】登录凭证校验
     *
     * @param code 临时登录凭证
     * @return
     */
    @RequestMapping(value = "/member/weixin")
    @PermissionMode(PermissionMode.Mode.White)
    @ResponseBody
    public String wxCode2Session(String code) {
        System.out.println("wxCode2Session");
        String res = restTemplate.getForObject(wxApiCode2Session, String.class, code);
        System.out.println(res);
        return res;
    }



}