package com.zhiwee.gree.webapi.Controller.Wxpay;

import com.zhiwee.gree.model.FyGoods.FyOrder;
import com.zhiwee.gree.model.FyGoods.FyOrderItem;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.WxPay.WeChatConfig;
import com.zhiwee.gree.webapi.util.*;
import org.springframework.util.DigestUtils;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.util.KeyUtils;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.Key;
import java.util.*;

import static org.apache.commons.httpclient.HttpConstants.getContentBytes;

public class FyWxPay {



    /**
     * @Author: jick
     * @Date: 2020/1/6 13:59
     * 阜阳商厦小程序微信支付
     */
    public static Result<?>  wxPay(HttpServletRequest request,FyOrder order,ShopUser shopUser) throws Exception {
        String openid =   shopUser.getOpenId();
        BigDecimal payPrice = order.getPayPrice().multiply(new BigDecimal(100));
        //去除小数点
        String total_fee = payPrice.stripTrailingZeros().toPlainString();
        //生成的随机字符串
        String nonce_str = getRandomString(32);
        //商品名称
        //获取客户端的ip地址
        String spbill_create_ip = getIpAddress(request);
        //组和生成签名
        SortedMap<String,String> packageParams = new TreeMap<>();
        //组装参数，用户生成统一下单接口的签名
        packageParams.put("appid", WeChatConfig.APPIDFuYang);
        packageParams.put("mch_id", WeChatConfig.MCHIDFuYang);
        packageParams.put("nonce_str", nonce_str);
        packageParams.put("body", order.getGoodsBody());
        //商户订单号,自己的订单ID
          packageParams.put("out_trade_no", order.getOrderNum());
        //支付金额，这边需要转成字符串类型，否则后面的签名会失败
        packageParams.put("total_fee",total_fee);
        packageParams.put("spbill_create_ip", spbill_create_ip);
        //支付成功后的回调地址
        packageParams.put("notify_url", WeChatConfig.WECHAT_NOTIFYURLFuYang);
        //支付方式
        packageParams.put("trade_type", WeChatConfig.Pay);
        //用户的openID，自己获取
        packageParams.put("openid", openid);
        //加密类型
        //             packageParams.put("sign_type", "MD5");

        //sign签名
        String sign = WXPayUtil.createSign(packageParams, WeChatConfig.APIKEYFuYang);
        packageParams.put("sign",sign);

        //map转xml
        String payXml = WXPayUtil.mapToXml(packageParams);
        //统一下单
        String orderStr = HttpUtil.doPost(WeChatConfig.pay_url,payXml,4000);
        System.out.println(orderStr);
        if(null == orderStr) {
            return null;
        }
        Map<String, String> unifiedOrderMap =  WXPayUtil.xmlToMap(orderStr);
        System.out.println(unifiedOrderMap.toString());
        Long timeStamp = System.currentTimeMillis()/ 1000;
        //  String stringSignTemp ="appId="+ WeChatConfig.APPIDFuYang +"&nonceStr="+ nonce_str +"&package=prepay_id="+unifiedOrderMap.get("prepay_id")+"&signType="+ WeChatConfig.SIGNTYPE +"&timeStamp="+ timeStamp;
        //再次签名，这个签名用于小程序端调用wx.requesetPayment方法
        //  String paySign2 = PayUtil.sign(stringSignTemp,WeChatConfig.APIKEYFuYang,"utf-8").toUpperCase();
        if ("INVALID_REQUEST".equals(unifiedOrderMap.get("err_code"))){
            return  Result.of(Status.ClientError.BAD_REQUEST,"银行卡存在，但与姓名证件号其中一项或全部不匹配");
        }


        SortedMap<String,String> packageParams2 = new TreeMap<>();
        packageParams2.put("appId", WeChatConfig.APPIDFuYang);
        packageParams2.put("nonceStr", nonce_str);
        packageParams2.put("package","prepay_id="+unifiedOrderMap.get("prepay_id"));
        packageParams2.put("signType", WeChatConfig.SIGNTYPE);
        packageParams2.put("timeStamp",String.valueOf(timeStamp));
        String paySign2 = WXPayUtil.createSign(packageParams2, WeChatConfig.APIKEYFuYang);
       // System.out.println("测试  "+paySign2);
        // signType="+ WeChatConfig.SIGNTYPE +"&timeStamp="+ timeStamp;
        //返回给移动端需要的参数
        Map <String,Object> response = new HashMap<String,Object>();
        response.put("nonceStr",nonce_str);
        response.put("package","prepay_id="+unifiedOrderMap.get("prepay_id"));
        response.put("timeStamp",timeStamp);
        response.put("paySign",paySign2);
        return  Result.ok(response);
    }


    /**
     * @Author: jick
     * @Date: 2020/1/7 16:20
     * 小程序微信退款
     */
    public static Result<?> wxPayRefund(FyOrder fyOrder,FyOrderItem fyOrderItem) {
        String nonce_str = getRandomString(32);
        //去除小数点后面的0
        BigDecimal payPrice = fyOrder.getPayPrice().multiply(new BigDecimal(100));
        String total_fee = payPrice.stripTrailingZeros().toPlainString();
        //扩大100
        BigDecimal refoundPrice = fyOrder.getRefoundPrice().multiply(new BigDecimal(100));
        String bigDecimal = refoundPrice.stripTrailingZeros().toPlainString();
        //生成sign签名
        SortedMap<String,String>  parameters = new TreeMap<>();
        parameters.put("appid", WeChatConfig.APPIDFuYang);
        parameters.put("mch_id", WeChatConfig.MCHIDFuYang);
        parameters.put("nonce_str",nonce_str);
        parameters.put("sign_type","MD5");
        //这个是你要退款的流水号，你支付的时候微信返还给你的
        //  parameters.put("transaction_id", data.getTransaction_id());
        parameters.put("out_refund_no", KeyUtils.getKey());
        parameters.put("total_fee",total_fee.toString());
        parameters.put("refund_fee",bigDecimal.toString());
        parameters.put("out_trade_no",fyOrder.getOrderNum());
        // parameters.put("refund_fee_type", data.getRefund_fee_type());
        //参数不知道什么意思的，对着官方给的API文档看看就清楚了
        String sign = WXPayUtil.createSign(parameters, WeChatConfig.APIKEYFuYang);  //工具类的方法,key就是平台上获取的
        parameters.put("sign", sign);
        RefundWxPayConfig refundWxPayConfig = new RefundWxPayConfig();
        Map<String, String>  returnMap  = null;
        WXPay2 wxPay = new WXPay2(refundWxPayConfig);
        try {
            returnMap = wxPay.refund(parameters);
            System.out.println(returnMap);
            System.out.println("kakaka"+returnMap.get("result_code"));
            //业务逻辑代码
            if("SUCCESS".equals(returnMap.get("result_code"))){
                return  Result.ok();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  Result.of(Status.ClientError.BAD_REQUEST,"退款失败");
    }



    /**
     * @author tzj
     * @description 获取用户IP地址
     * @date 2019/12/25 11:21
     */
    public static String getIpAddress(HttpServletRequest request) {
        // 避免反向代理不能获取真实地址, 取X-Forwarded-For中第一个非unknown的有效IP字符串
        String ip = request.getHeader("x-forwarded-for");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        return ip;
    }
    /**
     * 解析xml，取出其中的result_code，和prepay_id
     */
    public HashMap<String ,String> doXMLToMap(String results) {
        HashMap<String ,String> result = new HashMap<String ,String>();
        String return_code = results.substring(results.indexOf("<return_code><![CDATA[")+22, results.indexOf("]]></return_code>"));
        result.put("return_code", return_code);
        if(return_code.equals("SUCCESS")) {
            String prepay_id = results.substring(results.indexOf("<prepay_id><![CDATA[")+20, results.indexOf("]]></prepay_id>"));
            result.put("prepay_id", prepay_id);
        }
        return result;

    }


    /**
     *
     * requestUrl请求地址
     *  requestMethod请求方法
     *  outputStr参数
     */
    public static String httpRequest(String requestUrl, String requestMethod, String outputStr) {
        // 创建SSLContext
        StringBuffer buffer = null;
        try {
            URL url = new URL(requestUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod(requestMethod);
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.connect();
            // 往服务器端写内容
            if (null != outputStr) {
                OutputStream os = conn.getOutputStream();
                os.write(outputStr.getBytes("utf-8"));
                os.close();
            }
            // 读取服务器端返回的内容
            InputStream is = conn.getInputStream();
            InputStreamReader isr = new InputStreamReader(is, "utf-8");
            BufferedReader br = new BufferedReader(isr);
            buffer = new StringBuffer();
            String line = null;
            while ((line = br.readLine()) != null) {
                buffer.append(line);
            }
            br.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return buffer.toString();
    }





    //length用户要求产生字符串的长度
    public static String getRandomString(int length){
        String str="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        Random random=new Random();
        StringBuffer sb=new StringBuffer();
        for(int i=0;i<length;i++){
            int number=random.nextInt(62);
            sb.append(str.charAt(number));
        }
        return sb.toString();
    }


    /**
     * 签名字符串
     * @param
     * @param key 密钥
     * @return 签名结果
     */
    public static String sign(String text, String key, String input_charset) {
        text = text + "&key=" + key;
        return DigestUtils.md5DigestAsHex(getContentBytes(text, input_charset));
    }















}
