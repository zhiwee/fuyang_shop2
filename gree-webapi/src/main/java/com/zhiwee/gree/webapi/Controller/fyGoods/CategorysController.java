package com.zhiwee.gree.webapi.Controller.fyGoods;

import com.zhiwee.gree.model.FyGoods.PictureInfo;
import com.zhiwee.gree.model.GoodsInfo.Categorys;
import com.zhiwee.gree.model.GoodsInfo.GoodVo.CategorysExo;
import com.zhiwee.gree.model.GoodsInfo.Goods;
import com.zhiwee.gree.service.GoodsInfo.CategorysService;
import com.zhiwee.gree.service.GoodsInfo.GoodsService;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.jeecgframework.poi.excel.ExcelExportUtil;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.*;
import xyz.icrab.common.util.KeyUtils;
import xyz.icrab.common.web.annotation.PermissionMode;
import xyz.icrab.common.web.param.IdParam;
import xyz.icrab.common.web.param.IdsParam;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.io.OutputStream;
import java.util.*;

/**
 * 商品分类  周广
 * @author DELL
 */
@ResponseBody
@RequestMapping("/categorys")
@RestController
public class CategorysController {

    @Resource
    private CategorysService categorysService;

    @Resource
    private GoodsService goodsService;
    /**
     * @author tzj
     * @description 页面
     * @date 2020/4/2 10:14
    */
    @RequestMapping("/page")
    public Result<?> page(@RequestBody Map<String, Object> param, Pagination pagination) {
        if (param == null) {
            param = new HashMap<>();
        }
        Pageable<Categorys> pageable = categorysService.getPage(param, pagination);
        return Result.ok(pageable);
    }

    /**
     * @author tzj
     * @description 树形结构
     * @date 2019/12/12 12:46
     */
    @RequestMapping("/tree")
    public Result<?> tree(@RequestBody(required = false) Map<String, Object> params) {
        if (params == null) {
            params = new HashMap<>();
        }
        List<Categorys> list = categorysService.list(params);
        List<Tree<Categorys>> trees = new ArrayList<>(list.size());
        for (Categorys goodsCategory : list) {
            Tree<Categorys> tree = new Tree<>(goodsCategory.getId(), goodsCategory.getName(), goodsCategory.getParentId(), goodsCategory);
            trees.add(tree);
        }
        return Result.ok(trees);
    }


    /**
     * @author tzj
     * @description 新增
     * @date 2020/4/2 10:14
    */
    @RequestMapping("/add")
    public Result<?> add(@RequestBody Categorys categorys) {
        categorys.setId(KeyUtils.getKey());
        categorys.setState(1);
        if (categorys.getParentId()!=null&& !"".equals(categorys.getParentId())){
            categorys.setRank(2);
        }else {
            categorys.setRank(1);
        }
        categorysService.save(categorys);
        return Result.ok();
    }


    @RequestMapping("/selectOne")
    @ResponseBody
    public Result<?> selectOne(@RequestBody IdParam param) {
        Categorys goodsCategory = categorysService.get(param.getId());
        return Result.ok(goodsCategory);
    }


    @RequestMapping("/update")
    public Result<?> update(@RequestBody Categorys goodsCategory) {
        if (goodsCategory.getParentId() == null || goodsCategory.getParentId().equals("")) {
            goodsCategory.setRank(2);
        } else {
            goodsCategory.setRank(1);
        }
        categorysService.update(goodsCategory);
        return Result.ok();
    }



    /**
     * @author tzj
     * @description 通过楼层获取商品分类
     * @date 2020/4/8 9:26
    */
    @RequestMapping("/getCategoryByFloor")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> getCategoryByFloor(@RequestBody(required = false) Map<String, Object> params) {
        List<Categorys> list = categorysService.list(params);
        return Result.ok(list);
    }

    /**
     * @author tzj
     * @description 通过分类获取商品
     * @date 2020/4/8 9:26
     */
    @RequestMapping("/getGoodsByCategory")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> getGoodsByCategory(@RequestBody(required = false) Map<String, Object> params) {
        List<Goods> list = goodsService.list(params);
        return Result.ok(list);
    }

    /**
     * @author tzj
     * @description 分类导出
     * @date 2020/4/13 13:06
     */
    @RequestMapping("/exportCategory")
    public void exportCategory(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        List<CategorysExo> list= categorysService.listExo();
        // 导出Excel
        OutputStream os = resp.getOutputStream();
        resp.setContentType("application/x-download");
        // 设置导出文件名称
        resp.setHeader("Content-Disposition",
                "attachment;filename=" + DateFormatUtils.format(new Date(), "yyyyMMddHHmmssSSS") + ".xls");
        ExportParams param = new ExportParams("商品分类信息", "商品分类信息", "商品分类信息");
        //param.setType(ExcelType.XSSF);
        Workbook excel = ExcelExportUtil.exportExcel(param, CategorysExo.class, list);
        excel.write(os);
        os.flush();
        os.close();
    }

    /**
     * @author tzj
     * @description 删除
     * @date 2020/4/27 9:53
    */
    @RequestMapping("/delete")
    @ResponseBody
    public Result<?> enable(@RequestBody @Valid IdsParam param){
        for (String id : param.getIds()) {
            Categorys categorys=new Categorys();
            categorys.setId(id);
            categorysService.delete(categorys);
        }
        return Result.ok();
    }



}