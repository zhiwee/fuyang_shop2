package com.zhiwee.gree.webapi.Controller.shopCart;

import com.zhiwee.gree.model.GoodsInfo.GoodVo.ShopCartGoodsVo;
import com.zhiwee.gree.model.GoodsInfo.Goods;
import com.zhiwee.gree.model.GoodsInfo.ShopCartGoods;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.service.GoodsInfo.GoodsService;
import com.zhiwee.gree.util.CookieUtils;
import com.zhiwee.gree.util.JsonUtils;
import com.zhiwee.gree.webapi.util.JsonUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.util.IdGenerator;
import xyz.icrab.common.web.annotation.*;
import xyz.icrab.common.web.session.Session;
import xyz.icrab.common.web.util.SessionUtils;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: jick
 * @Date: 2019/10/11 11:23
 * 购物车
 */
@RestController
@RequestMapping("/goodsCart")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class GoodsCartController {
    @Value("${COOKIE_SHOPCART_EXPIRE}")
    private Integer COOKIE_CART_EXPIRE;


    @Autowired
    private GoodsService  goodsService;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;




    /**
     * @Author:jick
     * @Date: 2019/10/11 11:23
     * 添加商品进入购物车
     */
    @PermissionMode(PermissionMode.Mode.White)
    @RequestMapping("/addCart")
    public Result<?> addCart(@RequestBody(required = false) Map<String, Object> param, HttpServletResponse res,
                             HttpServletRequest req) {
        Session session = SessionUtils.get(req);
        ShopUser user = null;
        //ShopUser  user = (ShopUser) session.getAttribute("ShopUser");
       // System.out.println(user.toString());

      //  System.out.println(user2);

        boolean  flag =  false;
        if(session !=null){
            user = (ShopUser) session.getAttribute("ShopUser");
            addGoodsToRedis(user,param,res,req);

            return  Result.ok();


        }else {
            return   null;
        }

      /* else{
            //未登录存cookie

            //获取数据
            String    goodsId  = (String) param.get("id");
            Integer count = (Integer) param.get("count");
            //从数据库查询数据
            Goods  goods  =   goodsService.get(goodsId);


            //首先从cookie中拿数据
            List<ShopCartGoods>  list  = getCartListFromCookie(req);

            //如果购物车
            if(list.size()>0){
               for (ShopCartGoods  g : list){
                   if(g.getGoodsId().equals(goodsId)){
                       //数量等于原来的数量加要传入的数量
                       count+=g.getCount();
                       g.setCount(count);
                       g.setTotalPrice(goods.getShopPrice().multiply(BigDecimal.valueOf(count)));
                       flag=true;
                       break;//跳出
                   }

               }

            }
               if(!flag){
                //创建一个购物车实体对象
                ShopCartGoods cartGoods  = new ShopCartGoods();
                cartGoods.setId(IdGenerator.uuid());
                cartGoods.setGoodsId(goodsId);
                cartGoods.setName((String) param.get("name"));
                cartGoods.setCover(goods.getGoodCover());
                cartGoods.setSpec((String) param.get("spec"));
                cartGoods.setPrice(goods.getShopPrice());
                cartGoods.setCount(count);
                cartGoods.setTotalPrice(goods.getShopPrice().multiply(BigDecimal.valueOf(count)));
                list.add(cartGoods);

            }
            //写入cookie
            CookieUtils.setCookie(req, res, "goodsCart", JsonUtils.objectToJson(list), COOKIE_CART_EXPIRE, true);

            return   Result.ok();

        }*/



    }


    /**
     * @Author: jick
     * @Date: 2019/10/11 13:41
     * 从cookie中获取信息
     */

    private List<ShopCartGoods> getCartListFromCookie(HttpServletRequest request) {
        String json = CookieUtils.getCookieValue(request, "goodsCart", true);

        //判断json是否为空
        if (StringUtils.isBlank(json)) {
             return new  ArrayList<>();
        }
        //把json转换成商品列表
        List<ShopCartGoods> list = JsonUtils.jsonToList(json,ShopCartGoods.class);
       // List<ShopCartGoods> list = JSONObject.parseArray(json, ShopCartGoods.class);

        return   list;
    }




    /**
     * @Author: jick
     * @Date: 2019/10/11 14:43
     * 购物车类表展示
     */
      @RequestMapping("/showCartGoods")
      @PermissionMode(PermissionMode.Mode.White)
    public Result<?> showCartList(HttpServletRequest request, HttpServletResponse response){
          //goodsCart

          //从cookie中取购物车列表
         // List<ShopCartGoods> cartList = getCartListFromCookie(request);
          //判断用户是否为登录状态
          Session session = SessionUtils.get(request);
          ShopUser user = null;

          if (session != null) {
              user = (ShopUser) session.getAttribute("ShopUser");
              //从cookie中取购物车列表
              //如果不为空，把cookie中的购物车商品和服务端的购物车商品合并。
             // shopCartService.mergeCart(user.getId(), cartList);
             /* if(cartList.size()>0){
                  for(ShopCartGoods  g : cartList){
                      Map   param  = new HashMap();
                      param.put("id",g.getGoodsId());
                      param.put("count",g.getCount());
                      param.put("name",g.getName());
                      param.put("spec",g.getSpec());
                      addGoodsToRedis(user,param,response,request);
                  }
                  //把cookie中的购物车删除
                  CookieUtils.deleteCookie(request, response, "goodsCart");
              }*/

              //从redis取购物车列表
              ShopCartGoodsVo shopCartGoodsVo =   getListFromRedis(user);

              return   Result.ok(shopCartGoodsVo);


          }


        return   null;

    }


    /**
     * @Author: jick
     * @Date: 2019/10/12 11:31
     * 添加信息至redis
     */
    private   void    addGoodsToRedis(ShopUser user,Map<String, Object> param, HttpServletResponse res, HttpServletRequest req){

        //从数据库查询数据
        Goods  goods  =   goodsService.get((String) param.get("id"));


        //登录存redis
        HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
        // 先查询,判断redis中是否有商品信息
        Map<String, String> resultMap = hashOperations.entries("userCart:"+user.getId());


        if(!resultMap.isEmpty() && resultMap.size() > 0){

            // 根据map的key获取value
            String  shopGoods = resultMap.get((String) param.get("id")+"-"+(String) param.get("spec"));

            //判断Redis中是否有对应的商品信息，如果存在进行修改商品数量操作(id 型号)
            if(null != shopGoods  &&  shopGoods.length() > 0){

                //转换成原始数据
                ShopCartGoods  shopCartGoods  =   JsonUtil.jsonStr2Object(shopGoods, ShopCartGoods.class);

                //重新计算数量和总价格
                shopCartGoods.setCount(shopCartGoods.getCount()+(Integer) param.get("count"));
                shopCartGoods.setTotalPrice(goods.getShopPrice().multiply(BigDecimal.valueOf(shopCartGoods.getCount())));

                // 重新添加至map
                resultMap.put(String.valueOf(param.get("id"))+"-"+String.valueOf(param.get("spec")), JsonUtil.object2JsonStr(shopCartGoods));

            }else{//redis中有商品信息，但是没有对应的商品信息，直接添加

                //获取数据
                String    goodsId  = (String) param.get("id");
                Integer count = (Integer) param.get("count");
                //创建一个购物车实体对象
                ShopCartGoods cartGoods  = new ShopCartGoods();
                cartGoods.setId(IdGenerator.uuid());
                cartGoods.setGoodsId(goodsId);
                cartGoods.setName((String) param.get("name"));
                cartGoods.setCover(goods.getGoodCover());
                cartGoods.setSpec((String) param.get("spec"));
                cartGoods.setPrice(goods.getShopPrice());
                cartGoods.setCount(count);
                cartGoods.setTotalPrice(goods.getShopPrice().multiply(BigDecimal.valueOf(count)));
                cartGoods.setStoreCount(goods.getStoreCount());
                // 新增商品购物车信息
                resultMap.put(String.valueOf(param.get("id"))+"-"+String.valueOf(param.get("spec")), JsonUtil.object2JsonStr(cartGoods));

            }
        }  else {//redis中没有商品信息，直接添加

            //获取数据
            String    goodsId  = (String) param.get("id");
            Integer count = (Integer) param.get("count");
            //创建一个购物车实体对象
            ShopCartGoods cartGoods  = new ShopCartGoods();
            cartGoods.setId(IdGenerator.uuid());
            cartGoods.setGoodsId(goodsId);
            cartGoods.setName((String) param.get("name"));
            cartGoods.setCover(goods.getGoodCover());
            cartGoods.setSpec((String) param.get("spec"));
            cartGoods.setPrice(goods.getShopPrice());
            cartGoods.setCount(count);
            cartGoods.setTotalPrice(goods.getShopPrice().multiply(BigDecimal.valueOf(count)));
            cartGoods.setStoreCount(goods.getStoreCount());

            // 新增商品购物车信息
            resultMap.put(String.valueOf(param.get("id"))+"-"+String.valueOf(param.get("spec")), JsonUtil.object2JsonStr(cartGoods));

        }
        // 添加至redis
        hashOperations.putAll("userCart:"+user.getId() , resultMap);

    }


    /**
     * @Author: jick
     * @Date: 2019/10/12 11:48
     * 获取redis中的数据
     */
    private ShopCartGoodsVo getListFromRedis(ShopUser user){

        //创建一个返回对象
        ShopCartGoodsVo  shopCartGoodsVo  = new ShopCartGoodsVo();

        HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
        // 先查询
        Map<String, String> resultMap = hashOperations.entries("userCart:"+user.getId());
        // 判断map是否为空
        if (!resultMap.isEmpty() && resultMap.size() > 0) {
            // 初始化返回对象
            List<ShopCartGoods> shopCartGoodsList = new ArrayList<>();
            BigDecimal allPrice = new BigDecimal(0);
            // 循环获取
            for (Map.Entry<String, String> map : resultMap.entrySet()) {
              //  GoodsVo goodsVo = JsonUtil.jsonStr2Object(map.getValue(), GoodsVo.class);
                  ShopCartGoods  shopCartGoods  = JsonUtil.jsonStr2Object(map.getValue(),ShopCartGoods.class);
                  shopCartGoodsList.add(shopCartGoods);
                  allPrice  =   allPrice.add(shopCartGoods.getTotalPrice());

            }

            shopCartGoodsVo.setCartGoodsList(shopCartGoodsList);
            shopCartGoodsVo.setAllPrice(allPrice);
        }
        return   shopCartGoodsVo;

    }
    
    
    /**
     * @Author: jick
     * @Date: 2019/10/12 14:40
     * 删除购物车数据
     */
    @RequestMapping("/showCartGoodsDel")
    @PermissionMode(PermissionMode.Mode.White)
     public  Result<?>  delCartGoods(@RequestBody(required = false) Map<String, Object> param, HttpServletRequest request,
                                     HttpServletResponse response){

        Session session = SessionUtils.get(request);
        ShopUser user = null;

        if (session != null) {
            //在登录的情况下删除redis中数据
            user = (ShopUser) session.getAttribute("ShopUser");
          //  shopCartService.clearCartItem(user.getId());

            HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
            // 先查询
            Map<String, String> resultMap = hashOperations.entries("userCart:"+user.getId());

            //非空判断，非空就遍历查找
            // 判断map是否为空
            if (!resultMap.isEmpty() && resultMap.size() > 0) {


                String[]  ids = ((String) param.get("ids")).split(",");

                //遍历删除
                if(ids.length>0){
                    for(String id : ids){
                        hashOperations.delete("userCart:"+user.getId(),id);
                    }
                }
                /*
                if(id !=null && id.length()>0){

                    return   Result.ok();
                }
                */
                return     Result.ok();



            }

            return Result.ok();
        }
        //为登录删除cookie中数据
       // CookieUtils.deleteCookie(request, response, "goodsCart");


        return   Result.ok();

       }



       /**
        * @Author: jick
        * @Date: 2019/10/12 15:31
        * 清空购物车
        */
       @RequestMapping("/clearCart")
       @PermissionMode(PermissionMode.Mode.White)
       public Result<?>   clearCartAllGoods( HttpServletRequest request, HttpServletResponse response){
           Session  session =  SessionUtils.get(request);
           ShopUser  user = null;

           if(session != null){
               //登录情况下删除redis里面数据
               user = (ShopUser) session.getAttribute("ShopUser");
               redisTemplate.delete("userCart:"+user.getId());
           }

          /* else {
               //未登录情况下
               CookieUtils.deleteCookie(request, response, "goodsCart");
           }
           */


           return   Result.ok();

        }



        /**
         * @Author: jick
         * @Date: 2019/10/12 15:55
         * 获取购物车中商品的数量
         */
        @RequestMapping("/getCount")
        @PermissionMode(PermissionMode.Mode.White)
        public  Integer  getCartGoodsCount(HttpServletRequest request, HttpServletResponse response){
            Session  session =  SessionUtils.get(request);
            ShopUser  user = null;

            Integer  total  =0;

            if(session != null){
                user = (ShopUser) session.getAttribute("ShopUser");
                HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
                // 先查询
                Map<String, String> resultMap = hashOperations.entries(  "userCart:"+user.getId());

                if(!resultMap.isEmpty() && resultMap.size()>0){
                    //循环添加
                    for (Map.Entry<String, String> map : resultMap.entrySet()) {
                         ShopCartGoods  shopCartGoods = JsonUtil.jsonStr2Object(map.getValue(), ShopCartGoods.class);
                        total += shopCartGoods.getCount();
                    }


                }

            }
            return   total;

        }








}
