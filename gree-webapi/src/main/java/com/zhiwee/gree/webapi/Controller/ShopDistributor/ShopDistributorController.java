package com.zhiwee.gree.webapi.Controller.ShopDistributor;


import com.zhiwee.gree.model.Area;
import com.zhiwee.gree.model.GoodsInfo.GoodsClassify;
import com.zhiwee.gree.model.ShopDistributor.*;
import com.zhiwee.gree.model.User;
import com.zhiwee.gree.model.enums.UserType;
import com.zhiwee.gree.service.GoodsInfo.GoodsClassifyService;
import com.zhiwee.gree.service.ShopDistributor.DistributorAreaService;
import com.zhiwee.gree.service.ShopDistributor.DistributorGradeService;
import com.zhiwee.gree.service.ShopDistributor.ShopDistributorService;
import com.zhiwee.gree.service.shopBase.AreaService;
import com.zhiwee.gree.service.shopBase.RoleService;
import com.zhiwee.gree.service.shopBase.UserService;
import com.zhiwee.gree.webapi.util.SystemControllerLog;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import xyz.icrab.common.model.*;
import xyz.icrab.common.model.enums.YesOrNo;
import xyz.icrab.common.util.KeyUtils;
import xyz.icrab.common.web.annotation.EnableGetRequest;
import xyz.icrab.common.web.annotation.EnableListRequest;
import xyz.icrab.common.web.annotation.Session;
import xyz.icrab.common.web.param.IdParam;
import xyz.icrab.common.web.param.IdsParam;
import xyz.icrab.common.web.util.Validator;
import xyz.icrab.ums.util.consts.SessionKeys;
import javax.annotation.Resource;
import javax.validation.Valid;
import java.beans.Transient;
import java.util.*;

@RequestMapping("/shopDistributor")
@Controller
@ResponseBody
@EnableListRequest
@EnableGetRequest
public class ShopDistributorController {
    @Resource
    private ShopDistributorService shopDistributorService;

    @Autowired
    private GoodsClassifyService goodsClassifyService;

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private AreaService areaService;

    @Autowired
    private DistributorAreaService distributorAreaService;

    /**
     * Description: 添加配送门店和账号
     * @author: 周广
     * @param:
     * @return:
     */
    @RequestMapping(value = "/addDealerAndUser")
    @ResponseBody
    @Transient
    public Result<?> addDealerAndUser(@RequestBody DealerAndUserVo dealerAndUserVo, @Session(SessionKeys.USER) User user)  {
        Validator validator = new Validator();
        validator.notEmpty(dealerAndUserVo.getDealerName(), "门店名称不能为空");
        validator.notEmpty(dealerAndUserVo.getManagerName(), "门店负责人不能为空");
        validator.notEmpty(dealerAndUserVo.getUserTelephone(), "门店电话不能为空");
        validator.notEmpty(dealerAndUserVo.getUserName(), "登录账户名不能为空");
        validator.notEmpty(dealerAndUserVo.getUserPassword(), "登录账户密码不能为空");
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }
        ShopDistributor dealer=new ShopDistributor();
        dealer.setId(KeyUtils.getKey());
        dealer.setDealerId(dealerAndUserVo.getDealerId());
        dealer.setDealerName(dealerAndUserVo.getDealerName());
        dealer.setParentName(dealerAndUserVo.getParentName());
        dealer.setPhone(dealerAndUserVo.getUserTelephone());
        dealer.setManagerName(dealerAndUserVo.getManagerName());
        dealer.setUserRediss(dealerAndUserVo.getUserRediss());
        dealer.setUserSum(dealerAndUserVo.getUserSum());
        dealer.setCreateMan(user.getId());
        dealer.setType(2);
        dealer.setState(1);
        dealer.setDefaultDistributor(2);
        dealer.setCreateTime(new Date());
        User userOne=new User();
        userOne.setId(KeyUtils.getKey());
        userOne.setUsername(dealerAndUserVo.getUserName());
        userOne.setNickname(dealerAndUserVo.getManagerName());
        userOne.setPassword(dealerAndUserVo.getUserPassword());
        userOne.setMobile(dealerAndUserVo.getPhone());
        userOne.setEmail(dealerAndUserVo.getUserEml());
        userOne.setType(UserType.Retailer);
        userOne.setState(YesOrNo.YES);
        userOne.setCreateTime(new Date());
        userOne.setDistributorId(dealer.getId());
        userOne.setDistributorName(dealer.getDealerName());
        shopDistributorService.save(dealer);
        userService.save(userOne);
        List<String> roleList = Arrays.asList("5cf275c2ecf97d0fce904474".split(","));
        roleService.resetUserRole(userOne.getId(),roleList);
        return Result.ok();
    }

    /**
     * Description: getOne，查询
     * @author: 周广
     * @param:
     * @return:
     */
    @RequestMapping(value = "/selectDealerAndUser")
    @ResponseBody
    @Transient
    public  Result<?> selectDealerAndUser(@RequestBody @Valid IdParam map) {
        ShopDistributor shopDistributor = shopDistributorService.get(map.getId());
        Map<String,Object> getmap=new HashMap<>();
        getmap.put("distributorId",map.getId());
        User user=userService.getOne(getmap);
        DealerAndUserVo dealerAndUserVo=new DealerAndUserVo();
        dealerAndUserVo.setDealerId(shopDistributor.getId());
        dealerAndUserVo.setDealerName(shopDistributor.getDealerName());
        dealerAndUserVo.setParentName(shopDistributor.getParentName());
        dealerAndUserVo.setUserTelephone(shopDistributor.getPhone());
        dealerAndUserVo.setManagerName(shopDistributor.getManagerName());
        dealerAndUserVo.setUserRediss(shopDistributor.getUserRediss());
        dealerAndUserVo.setUserSum(shopDistributor.getUserSum());

        dealerAndUserVo.setUserId(user.getId());
        dealerAndUserVo.setUserName(user.getUsername());
        dealerAndUserVo.setUserPassword(user.getPassword());
        dealerAndUserVo.setPhone(user.getMobile());
        dealerAndUserVo.setUserEml(user.getEmail());
        return Result.ok(dealerAndUserVo);
    }


    /**
     * Description: 更新配送门店和账号
     * @author: 周广
     * @param:
     * @return:
     */
    @RequestMapping(value = "/updateDealerAndUser")
    @ResponseBody
    @Transient
    public Result<?> updateDealerAndUser(@RequestBody DealerAndUserVo dealerAndUserVo, @Session(SessionKeys.USER) User user)  {

        Validator validator = new Validator();

        validator.notEmpty(dealerAndUserVo.getUserName(), "登录账户名不能为空");
        validator.notEmpty(dealerAndUserVo.getUserPassword(), "登录账户密码不能为空");
        validator.notEmpty(dealerAndUserVo.getDealerName(), "门店名称不能为空");
        validator.notEmpty(dealerAndUserVo.getManagerName(), "门店负责人不能为空");
        validator.notEmpty(dealerAndUserVo.getUserTelephone(), "门店电话不能为空");

        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }
        ShopDistributor dealerInfo=new ShopDistributor();
        dealerInfo.setManagerName(dealerAndUserVo.getManagerName());
        dealerInfo.setUserRediss(dealerAndUserVo.getUserRediss());
        dealerInfo.setUserSum(dealerAndUserVo.getUserSum());
        dealerInfo.setId(dealerAndUserVo.getDealerId());
        dealerInfo.setDealerName(dealerAndUserVo.getDealerName());
        dealerInfo.setParentName(dealerAndUserVo.getParentName());
        dealerInfo.setPhone(dealerAndUserVo.getUserTelephone());
        dealerInfo.setUpdateMan(user.getId());
        dealerInfo.setUpdateTime(new Date());
        User userOne=new User();
        userOne.setId(dealerAndUserVo.getUserId());
        userOne.setMobile(dealerAndUserVo.getPhone());
        userOne.setEmail(dealerAndUserVo.getUserEml());
        userOne.setUsername(dealerAndUserVo.getUserName());
        userOne.setNickname(dealerAndUserVo.getManagerName());
        userOne.setPassword(dealerAndUserVo.getUserPassword());
        userOne.setDistributorName(dealerInfo.getDealerName());
        shopDistributorService.update(dealerInfo);
        userService.update(userOne);
        return Result.ok();
    }



    /**
     * Description: 添加非配送商门店
     * @author: sun
     * @Date 下午11:52 2019/5/21
     * @param:
     * @return:
     */
    @RequestMapping(value = "/addShopDistributor")
    @ResponseBody
    public Result<?> addShopDistributor(@RequestBody ShopDistributor shopDistributor, @Session(SessionKeys.USER) User user)  {
        Validator validator = new Validator();
        validator.notEmpty(shopDistributor.getDealerName(), "门店名称不能为空");
        validator.notEmpty(shopDistributor.getManagerName(), "门店负责人不能为空");
        validator.notEmpty(shopDistributor.getPhone(), "门店电话不能为空");
        validator.notEmpty(shopDistributor.getParentName(), "经销商不能为空");
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }
        shopDistributor.setId(KeyUtils.getKey());
        shopDistributor.setCreateMan(user.getId());
        shopDistributor.setType(2);
        shopDistributor.setState(1);
//        shopDistributor.setAmount(0.0);
//        Map<String, Object> params = new HashMap<>();
//        params.put("isdefault", 1);
//        DistributorGrade memberGrade = distributorGradeService.getOne(params);
//        if(memberGrade != null){
//            shopDistributor.setGrade(memberGrade.getId());
//        }else{
//            shopDistributor.setGrade(1);
//        }
        shopDistributor.setCreateTime(new Date());
        shopDistributorService.save(shopDistributor);
        return Result.ok();
    }

    /**
     * Description: 更新非门店配送商信息
     * @author: sun
     * @Date 下午11:52 2019/5/21
     * @param:
     * @return:
     */
    @RequestMapping(value = "/updateShopDistributor")
    @ResponseBody
    public Result<?> updateShopDistributor(@RequestBody ShopDistributor shopDistributor, @Session(SessionKeys.USER) User user)  {
        Validator validator = new Validator();
        validator.notEmpty(shopDistributor.getId(), "请重新选择");
        validator.notEmpty(shopDistributor.getDealerName(), "门店名称不能为空");
        validator.notEmpty(shopDistributor.getManagerName(), "门店负责人不能为空");
        validator.notEmpty(shopDistributor.getPhone(), "门店电话不能为空");
        validator.notEmpty(shopDistributor.getParentName(), "经销商不能为空");
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }
        shopDistributor.setUpdateMan(user.getId());
        shopDistributor.setUpdateTime(new Date());
        shopDistributorService.update(shopDistributor);
        return Result.ok();
    }




    /**
  * Description: 获取一个经销商的信息
  * @author: sun
  * @Date 下午11:59 2019/5/21
  * @param:
  * @return:
  */
    @RequestMapping("/selectOne")
    @ResponseBody
    public  Result<?> selectOne(@RequestBody @Valid IdParam map) {
        ShopDistributor shopDistributor = shopDistributorService.get(map.getId());
        return Result.ok(shopDistributor);
    }



    /**
  * Description: 更新配送商的区域
  * @author: sun
  * @Date 下午11:52 2019/5/21
  * @param:
  * @return:
  */

    @RequestMapping(value = "/updateArea")
    @ResponseBody
    public Result<?> updateArea(@RequestBody ShopDistributor shopDistributor, @Session(SessionKeys.USER) User user)  {
        Validator validator = new Validator();
        validator.notEmpty(shopDistributor.getId(), "请重新选择");
        validator.notEmpty(shopDistributor.getAreaId(), " 区域不能为空");
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }
        shopDistributor.setUpdateMan(user.getId());
        shopDistributor.setUpdateTime(new Date());
        shopDistributorService.update(shopDistributor);
        return Result.ok();
    }




    /**
     * Description: 获取所有配送商信息
     * @author: sun
     * @Date 下午3:19 2019/5/21
     * @param:
     * @return:
     */
    @SystemControllerLog(description = "获取所有配送商信息")
    @RequestMapping("/infoPage")
    @ResponseBody
    public Result<?> infoPage(@RequestBody Map<String, Object> param, Pagination pagination) {
        if (param == null) {
            param = new HashMap<>();
        }
        Pageable<ShopDistributor> pageable = shopDistributorService.infoPage(param, pagination);
        return Result.ok(pageable);
    }






    /**
     * Description: 获取所有配送商信息不分页
     * @author: sun
     * @Date 下午3:19 2019/5/21
     * @param:
     * @return:
     */
    @SystemControllerLog(description = "获取所有配送商信息")
    @RequestMapping("/searchByName")
    @ResponseBody
    public Result<?> page(@RequestBody Map<String, Object> param) {
        if (param == null) {
            param = new HashMap<>();
        }
      List<ShopDistributor>  shopDistributors= shopDistributorService.list(param);
        return Result.ok(shopDistributors);
    }





    /**
     * Description:启用配送商
     * @author: sun
     * @Date 下午3:20 2019/5/21
     * @param:
     * @return:
     */
    @RequestMapping("/open")
    @ResponseBody
    public Result<?> open(@RequestBody IdsParam param) {
        List<String> list = param.getIds();
        for(String id : list){
            ShopDistributor shopDistributor = new ShopDistributor();
            shopDistributor.setId(id);
            shopDistributor.setState(1);
            shopDistributorService.update(shopDistributor);
        }
        return Result.ok();
    }
/**
 * Description: 禁用配送商
 * @author: sun
 * @Date 下午3:26 2019/5/21
 * @param:
 * @return:
 */
    @RequestMapping("/stop")
    @ResponseBody
    public Result<?> stop(@RequestBody IdsParam param) {
        List<String> list = param.getIds();
        for(String id : list){
            ShopDistributor shopDistributor = new ShopDistributor();
            shopDistributor.setId(id);
            shopDistributor.setState(2);
            shopDistributorService.update(shopDistributor);
        }
        return Result.ok();
    }

    /**
     * Description:设置默认配送商
     * @author: sun
     * @Date 下午3:20 2019/5/21
     * @param:
     * @return:
     */
    @RequestMapping("/confirm")
    @ResponseBody
    public Result<?> confirm(@RequestBody IdsParam param) {
        Map<String,Object> params = new HashMap<>();
        params.put("defaultDistributor",1);
        List<ShopDistributor> shopDistributors = shopDistributorService.list(params);
        if(shopDistributors.size() > 0){
            return Result.of(Status.ClientError.BAD_REQUEST, "已经存在默认配送商");
        }
        List<String> list = param.getIds();
        for(String id : list){
            ShopDistributor shopDistributor = new ShopDistributor();
            shopDistributor.setId(id);
            shopDistributor.setDefaultDistributor(1);
            shopDistributorService.update(shopDistributor);
        }
        return Result.ok();
    }
    /**
     * Description: 取消默认配送商
     * @author: sun
     * @Date 下午3:26 2019/5/21
     * @param:
     * @return:
     */
    @RequestMapping("/cancle")
    @ResponseBody
    public Result<?> cancle(@RequestBody IdsParam param) {
        List<String> list = param.getIds();
        for(String id : list){
            ShopDistributor shopDistributor = new ShopDistributor();
            shopDistributor.setId(id);
            shopDistributor.setDefaultDistributor(2);
            shopDistributorService.update(shopDistributor);
        }
        return Result.ok();
    }






    /**
     * Description: 添加配送商的商品分类
     * @author: sun
     * @Date 下午11:52 2019/5/21
     * @param:
     * @return:
     */
    @RequestMapping(value = "/addCategory")
    @ResponseBody
    public Result<?> addCategory(@RequestBody ShopDistributor shopDistributor)  {
        Validator validator = new Validator();
        validator.notEmpty(shopDistributor.getId(), "未获取配送商信息");
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }
         ShopDistributor distributor = shopDistributorService.get(shopDistributor.getId());
        if(distributor.getCategoryIdPath() == null || distributor.getCategoryIdPath().equals("")){
            distributor.setCategoryIdPath(shopDistributor.getCategoryIdPath());
        }else{
            distributor.setCategoryIdPath(distributor.getCategoryIdPath()+","+shopDistributor.getCategoryIdPath());
        }
        shopDistributor.setCreateTime(new Date());
        shopDistributorService.update(distributor);
        return Result.ok();
    }


    /**
     * Description: 获取配送商对应商品分类
     * @author: sun
     * @Date 下午11:52 2019/5/21
     * @param:
     * @return:
     */
    @RequestMapping(value = "/achieveCategory")
    @ResponseBody
    public Result<?> achieveCategory(@RequestBody Map<String, Object> param, Pagination pagination)  {
        ShopDistributor distributor = shopDistributorService.get((String)param.get("id"));
//        Map<String,Object> param = new HashMap<>();
        param.put("categoryIds",distributor.getCategoryIdPath().split(","));
        if(distributor.getCategoryIdPath() == null || distributor.getCategoryIdPath().equals("")){
            return Result.ok(null);
        }
        Pageable<GoodsClassify>classifies = goodsClassifyService.achieveSmallCategory(param,pagination);
        return Result.ok(classifies);
    }

    /**
     * Description: 减去配送商的商品分类
     * @author: sun
     * @Date 下午11:52 2019/5/21
     * @param:
     * @return:
     */
    @RequestMapping(value = "/deleteCategory")
    @ResponseBody
    public Result<?> deleteCategory(@RequestBody ShopDistributor shopDistributor, @Session(SessionKeys.USER) User user)  {
        Validator validator = new Validator();
        validator.notEmpty(shopDistributor.getId(), "请重新选择");
        validator.notEmpty(shopDistributor.getCategoryIdPath(), "商品分类不可为空");
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }
         ShopDistributor distributor = shopDistributorService.get(shopDistributor.getId());
        String categoryIds = distributor.getCategoryIdPath();
        List<String>  categorys = new ArrayList<>();
        List<String>  categoryIdpath = new ArrayList<>();
        //原来的的categoryIdpath
        String[] ids = categoryIds.split(",");
        for(String id :ids){
            categorys .add(id);

        }
        //减去的categoryIdpath
        String[] id = shopDistributor.getCategoryIdPath().split(",");
        for(String sign :id){
            categoryIdpath.add(sign);

        }
        categorys.removeAll(categoryIdpath);
//        String category="";
        if(categorys.size()>0){
            String category =  StringUtils.join(categorys.toArray(), ",");
            distributor.setCategoryIdPath(category);
        }else{
            //设置为null不会更新
            distributor.setCategoryIdPath("");
        }
        shopDistributorService.update(distributor);
        return Result.ok();
    }




    /**
     * Description: 获取配送商没有的商品分类
     * @author: sun
     * @Date 下午12:50 2019/5/6
     * @param:
     * @return:
     */
    @RequestMapping("/achieveSmallCategory")
    public Result<?> achieveSmallCategory(@RequestBody Map<String, Object> param, Pagination pagination) {
        if(param == null) {
            param = new HashMap<>();
        }
        ShopDistributor shopDistributor = shopDistributorService.get((String)param.get("dealerId"));
        String   categoryIdPath = shopDistributor.getCategoryIdPath();
        if(categoryIdPath != null && !categoryIdPath.equals("")){
            param.put("categoryIdPath",categoryIdPath.split(","));
        }
        Pageable<GoodsClassify> goodsClassifies  = goodsClassifyService.achieveSmallCategory(param,pagination);
        return Result.ok(goodsClassifies);
    }

    /**
     * 周广，获取商品分类
     */
    @RequestMapping("/getCategory")
    public Result<?> getCategory(@RequestBody Map<String, Object> param) {
        List<CategoryVo> goodsClassifies  = goodsClassifyService.getCategory(param);
        return Result.ok(goodsClassifies);
    }
    /**
     * 周广，批量添加
     */
    @RequestMapping("/addAreaCategory")
    public Result<?> addAreaCategory(@RequestBody Map<String, Object> param) {
        String dealerId=(String)param.get("dealerId");
        String CategoryId=(String)param.get("CategoryId");
        String provinceVue=(String)param.get("provinceVue");
        String cityVue=(String)param.get("cityVue");
        String districtVue=(String)param.get("districtVue");
        List<Area> areaList=new ArrayList<>();
        if(StringUtils.isBlank(cityVue)){
            Map<String,Object> areaMap=new HashMap<>();
            areaMap.put("parentids",","+provinceVue+",");
            areaList=areaService.list(areaMap);
        }else if(StringUtils.isBlank(districtVue)){
            Map<String,Object> areaMap=new HashMap<>();
            areaMap.put("parentids",","+cityVue+",");
            areaList=areaService.list(areaMap);
        }else{
            Map<String,Object> areaMap=new HashMap<>();
            areaMap.put("parentids",","+districtVue);
            areaList=areaService.list(areaMap);
        }
        List<DistributorArea> distributorAreaList=new ArrayList<>();
        for (Area A:areaList) {
            DistributorArea distributorArea=new DistributorArea();
            distributorArea.setId(KeyUtils.getKey());
            distributorArea.setAreaId(A.getId());
            distributorArea.setDistributorId(dealerId);
            distributorArea.setCategoryId(CategoryId);
            distributorArea.setState(1);
            distributorAreaList.add(distributorArea);
        }
        distributorAreaService.save(distributorAreaList);
        return Result.ok();
    }

    /**
     * 周广，根据门店删除
     */
    @RequestMapping("/deleteAreaCategory")
    public Result<?> deleteAreaCategory(@RequestBody Map<String, Object> param) {
        DistributorArea distributorArea=new DistributorArea();
        distributorArea.setDistributorId((String)param.get("dealerId"));
        distributorAreaService.delete(distributorArea);
        return Result.ok();
    }


}
