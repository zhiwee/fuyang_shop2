package com.zhiwee.gree.webapi.Controller.OrderInfo;

import com.zhiwee.gree.model.OrderInfo.Order;
import com.zhiwee.gree.model.OrderInfo.OrderRefundReason;
import com.zhiwee.gree.model.OrderInfo.RefundReasonPicture;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.service.OrderInfo.OrderRefundReasonService;
import com.zhiwee.gree.service.OrderInfo.OrderService;
import com.zhiwee.gree.webapi.util.SystemControllerLog;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.util.IdGenerator;
import xyz.icrab.common.util.KeyUtils;
import xyz.icrab.common.web.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;


/**
 * @author sun on 2019/4/30
 */
@RestController
@RequestMapping("/orderRefundReason")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class OrderRefundReasonController {



    @Autowired
    private OrderService orderService;

    @Autowired
    private OrderRefundReasonService orderRefundReasonService;



    @Value("${file.upload.dir}")
    private String fileUploadDir;

    @Value("${HTTP_PICTUREURL}")
    private String httpurl;
    /**
     * Description: 查看申请明细
     *
     * @author: sun
     * @Date 上午10:47 2019/5/12
     * @param:
     * @return:
     */
    @RequestMapping("/pageInfo")
    public Result<?> pageInfo(@RequestBody Map<String, Object> params,Pagination pagination) {
        if (params == null) {
            params = new HashMap<>();
        }

        Pageable<OrderRefundReason> page = orderRefundReasonService.pageInfo(params, pagination);
        return Result.ok(page);
    }

    /**
     * Description: 申请退款
     * @author: sun
     * @Date 下午11:52 2019/5/19
     * @param:
     * @return:
     */

    @PermissionMode(PermissionMode.Mode.White)
    @RequestMapping("/backMoney")
    public Result<?> backMoney(@RequestBody OrderRefundReason orderRefundReason, @Session("ShopUser") ShopUser user) {

        Order order = orderService.get(orderRefundReason.getOrderId());
        if(order == null || order.getOrderState() != 2 ){

            return Result.of(Status.ClientError.BAD_REQUEST, "该订单未支付或者该订单已经发货无法申请退款");
        }
        //申请退款状态  1 未申请退换货  2 申请退货中  3已允许退货，4拒绝退货 5 申请换货中 6允许换货 7 拒绝换货  8申请退款中  9 同意退款 10 拒绝退款
        if(order.getRefundState() == 2 || order.getRefundState() == 5 || order.getRefundState() == 8){

            return Result.of(Status.ClientError.BAD_REQUEST, "订单申请结束后才可以申请");
        }
        orderRefundReasonService.backMoney(order,orderRefundReason,user);
        return Result.ok("申请退款成功，等待商家审核");
    }

    /**
     * Description: 申请退货
     * @author: sun
     * @Date 下午11:52 2019/5/19
     * @param:
     * @return:
     */

    @PermissionMode(PermissionMode.Mode.White)
    @RequestMapping("/backGood")
    public Result<?>  backGood(@RequestBody OrderRefundReason orderRefundReason,@Session("ShopUser") ShopUser user) {

        Order order = orderService.get(orderRefundReason.getOrderId());
        if(order == null || order.getOrderState() != 2 ){
            return Result.of(Status.ClientError.BAD_REQUEST, "该订单未支付无法申请退货");
        }
        //申请退款状态  1 未申请退换货  2 申请退货中  3已允许退货，4拒绝退货 5 申请换货中 6允许换货 7 拒绝换货  8申请退款中  9 同意退款 10 拒绝退款
        if(order.getRefundState() == 2 || order.getRefundState() == 5 || order.getRefundState() == 8){
            return Result.of(Status.ClientError.BAD_REQUEST, "订单申请结束后才可以申请");
        }

        orderRefundReasonService.backGood(order,orderRefundReason,user);

        return Result.ok("申请退货成功，等待商家审核");
    }




    /**
     * Description: 申请换货
     * @author: sun
     * @Date 下午11:52 2019/5/19
     * @param:
     * @return:
     */
    @PermissionMode(PermissionMode.Mode.White)
    @RequestMapping("/changeGood")
    public Result<?>  changeGood(@RequestBody OrderRefundReason orderRefundReason, String callback,@Session("ShopUser") ShopUser user) {

        Order order = orderService.get(orderRefundReason.getOrderId());
        if(order == null || order.getOrderState() != 2){

            return Result.of(Status.ClientError.BAD_REQUEST, "该订单未支付无法申请换货");
        }
        //申请退款状态  1 未申请退换货  2 申请退货中  3已允许退货，4拒绝退货 5 申请换货中 6允许换货 7 拒绝换货  8申请退款中  9 同意退款 10 拒绝退款
        if(order.getRefundState() == 2 || order.getRefundState() == 5 || order.getRefundState() == 8){
            return Result.of(Status.ClientError.BAD_REQUEST, "订单审核结束后才可以申请");
        }

        orderRefundReasonService.changeGood(order,orderRefundReason,user);

        return Result.ok("申请换货成功，等待商家审核");
    }





    /**
     * Description: 拒绝申请
     * @author: sun
     * @Date 下午11:52 2019/5/19
     * @param:
     * @return:
     * 申请退款状态  1 未申请退换货  2 申请退货中  3已允许退货，4拒绝退货 5 申请换货中 6允许换货 7 拒绝换货  8申请退款中  9 同意退款 10 拒绝退款
     *    1 退货  2 换货，3退款
     */

    @SystemControllerLog(description = "拒绝申请")
    @RequestMapping("/refuce")
    public Result<?> refuse(@RequestBody OrderRefundReason orderRefundReason) {
        OrderRefundReason orderRefund = orderRefundReasonService.get(orderRefundReason.getId());
        Order order = orderService.get(orderRefund.getOrderId());
        if(orderRefund.getState() == 2){
            return Result.of(Status.ClientError.BAD_REQUEST, "该申请已经被拒绝");
        }
        if(orderRefund.getState() == 3){
            return Result.of(Status.ClientError.BAD_REQUEST, "该申请已经被同意");
        }
        if(orderRefund.getType() ==1 ){
            order.setRefundState(4);
        }else if(orderRefund.getType() == 2){
            order.setRefundState(7);
        } else if(orderRefund.getType() == 3){
            order.setRefundState(10);
        }
        orderRefund.setRefuseReason(orderRefundReason.getRefuseReason());
        orderRefund.setState(2);
        orderRefund.setUpdateTime(new Date());
        orderRefundReasonService.updateOrders(orderRefund,order);
        return Result.ok("拒绝成功");
    }



    /**
     * Description: 获取退款 退货，的记录
     * @author: sun
     * @Date 下午11:52 2019/5/19
     * @param:
     * @return:
     * 申请退款状态  1 未申请退换货  2 申请退货中  3已允许退货，4拒绝退货 5 申请换货中 6允许换货 7 拒绝换货  8申请退款中  9 同意退款 10 拒绝退款
     * 1 退货  2 换货，3退款
     */

    @RequestMapping("/achieveRefundInfo")
    public Result<?>  achieveRefundInfo(@Session("ShopUser") ShopUser user) {
        Map<String,Object> param = new HashMap<>();
        param.put("userId",user.getId());
       List<OrderRefundReason> orderRefundReasonList = orderRefundReasonService.achieveRefundInfo(param);
       if(orderRefundReasonList.isEmpty() && orderRefundReasonList.size() == 0){

           return Result.of(Status.ClientError.BAD_REQUEST, "还没有购买过商品");
       }else{

           return Result.ok(orderRefundReasonList);
       }
    }



    /**
     * Description: 获取换货的记录
     * @author: sun
     * @Date 下午11:52 2019/5/19
     * @param:
     * @return:
     * 申请退款状态  1 未申请退换货  2 申请退货中  3已允许退货，4拒绝退货 5 申请换货中 6允许换货 7 拒绝换货  8申请退款中  9 同意退款 10 拒绝退款
     * 1 退货  2 换货，3退款
     */

    @RequestMapping("/achieveChangeOrderInfo")
    public Result<?>  achieveChangeOrderInfo(@Session("ShopUser") ShopUser user) {
        Map<String,Object> param = new HashMap<>();
        param.put("userId",user.getId());
        List<OrderRefundReason> orderRefundReasonList = orderRefundReasonService.achieveChangeOrderInfo(param);
        if(orderRefundReasonList.isEmpty() && orderRefundReasonList.size() == 0){
            return Result.of(Status.ClientError.BAD_REQUEST, "还没有购买过商品");
        }else{

            return Result.ok(orderRefundReasonList);
        }
    }




    /**
     * Description: 用户僧请退换货图片上传
     * @author: sun
     * @Date 上午10:26 2019/5/28
     * @param:
     * @return:
     */
    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    @ResponseBody
    @PermissionMode(PermissionMode.Mode.White)
    public Result<List> upload(HttpServletRequest request) throws IOException {
        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver(request.getSession().getServletContext());
        if (multipartResolver.isMultipart(request)) {
            List<String> result = new ArrayList<>();
            MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
            Iterator iter = multiRequest.getFileNames();
            while (iter.hasNext()) {
                List<MultipartFile> files = multiRequest.getFiles(iter.next().toString());
                for (MultipartFile file : files) {
                    if (file != null) {
                        Map<String, String> map = new HashMap<>();
                        String id = KeyUtils.getKey();

                        String suffix = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".") + 1);
                        try {
                            String path = "refundReason" + "/" + KeyUtils.getKey() + "." + suffix;

                            Path target = Paths.get(fileUploadDir, path).normalize();
                            if (!Files.exists(target)) {
                                Files.createDirectories(target);
                            }
                            file.transferTo(target.toFile());
                            Runtime.getRuntime().exec("chmod 777 " + fileUploadDir+path);
                            result.add(httpurl+path);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            return Result.ok(result);
        } else {
            return Result.of(Status.ClientError.UPGRADE_REQUIRED, "无图片");
        }
    }

}
