package com.zhiwee.gree.webapi.Controller.shopCart;

import com.alipay.api.domain.MybankCreditSupplychainTradeCancelModel;
import com.zhiwee.gree.model.GoodsInfo.Goods;
import com.zhiwee.gree.model.GoodsInfo.ShopGoodsCollection;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.User;
import com.zhiwee.gree.model.resultBase.BaseResult;
import com.zhiwee.gree.service.GoodsInfo.GoodsService;
import com.zhiwee.gree.webapi.util.JsonUtil;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.web.annotation.EnableDeleteRequest;
import xyz.icrab.common.web.annotation.EnableGetRequest;
import xyz.icrab.common.web.annotation.EnableListRequest;
import xyz.icrab.common.web.annotation.EnablePageRequest;
import xyz.icrab.common.web.session.Session;
import xyz.icrab.common.web.util.SessionUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/goodsCollection")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class GoodsCollectionController {

    @Autowired
    private GoodsService   goodsService;

    @Autowired
    private RedisTemplate<String,String>  redisTemplate;


    /**
     * @Author: jick
     * @Date: 2019/10/16 10:24
     * 点击商品收藏至redis缓存数据库
     */
    @RequestMapping("/add")
     public BaseResult addGoodsToCollection(HttpServletResponse res, HttpServletRequest req,@RequestBody Map<String,Object>  param){

        //判断用户是否为登录状态
        Session session = SessionUtils.get(req);
         ShopUser user = null;
        if(session !=null){
            user = (ShopUser) session.getAttribute("ShopUser");

            String  goodsId = (String) param.get("id");

            //首先查询redis，判断商品是否一经添加到收藏
            HashOperations<String,String,String>   hashOperations  =  redisTemplate.opsForHash();
            Map<String,String>  resultMap  =   hashOperations.entries("goodsCollection:"+user.getId());



            //如果存在，给出反馈信息
            if(null !=resultMap.get(goodsId) && resultMap.get(goodsId).length()>0){
                return    new BaseResult(201,"该商品已经收藏");
            }else{
                //通过goodsID查询数据库，拿到关键信息
                Goods   goods =   goodsService.get(goodsId);

                ShopGoodsCollection   shopGoodsCollection  = new ShopGoodsCollection();
                shopGoodsCollection.setGoodsId(goodsId);
                shopGoodsCollection.setCover(goods.getGoodCover());
                shopGoodsCollection.setName(goods.getName());
                shopGoodsCollection.setPrice(goods.getShopPrice());

                if(goods.getStoreCount()>0)
                    shopGoodsCollection.setStoreInfo("库存充足");
                else
                    shopGoodsCollection.setStoreInfo("商品已售罄");

                //商品信息存入至redis
                //resultMap.put(String.valueOf(param.get("id")), JsonUtil.object2JsonStr(cartGoods));
                resultMap.put(goodsId, JsonUtil.object2JsonStr(shopGoodsCollection));
                // 添加至redis
                hashOperations.putAll("goodsCollection:"+user.getId() , resultMap);
                return    new BaseResult(200,"收藏成功，请在个人中心中查看");

            }

        }
        return  new BaseResult(203,"请登录");


     }


    /**
     * @Author: jick
     * @Date: 2019/10/16 11:21
     * 查询查询收藏列表
     */
    @RequestMapping("/list")
    public Result<?> getList(HttpServletRequest request, HttpServletResponse response){


        //判断用户是否为登录状态
        Session session = SessionUtils.get(request);
        ShopUser user = null;

        if(session !=null){

            user = (ShopUser) session.getAttribute("ShopUser");

            //首先查询redis，判断商品是否一经添加到收藏
            HashOperations<String,String,String>   hashOperations  =  redisTemplate.opsForHash();
            Map<String,String>  resultMap  =   hashOperations.entries("goodsCollection:"+user.getId());

            List<ShopGoodsCollection> list  =  new ArrayList<>();


            if(!resultMap.isEmpty()&&resultMap.size()>0){

                //循环遍历，同时装换
                for(String s:resultMap.keySet()){
                    ShopGoodsCollection  shopGoodsCollection =
                            JsonUtil.jsonStr2Object(resultMap.get(s),ShopGoodsCollection.class);
                    list.add(shopGoodsCollection);
                }


            }


            return   Result.ok(list);
        }
        return null;







    }







    /**
     * @Author: jick
     * @Date: 2019/10/16 11:21
     *移除收藏
     */
    @RequestMapping("/delete")
    public  Result<?>  deleteCollection(HttpServletRequest request, HttpServletResponse response, @RequestBody Map<String,Object> param){

        //判断用户是否为登录状态
        Session session = SessionUtils.get(request);
        ShopUser user = null;

        if(session !=null){
            user = (ShopUser) session.getAttribute("ShopUser");
            //先查询
            HashOperations<String,String,String> hashOperations =  redisTemplate.opsForHash();
            Map<String,String>  resultMap =  hashOperations.entries("goodsCollection:"+user.getId());

            if(!resultMap.isEmpty()&&resultMap.size()>0){
                hashOperations.delete("goodsCollection:"+user.getId(),param.get("goodsId"));
            }
            return  Result.ok();
        }
        return null;



    }

























}
