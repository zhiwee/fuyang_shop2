package com.zhiwee.gree.webapi.Controller.ActivityTicketOrder;


import com.zhiwee.gree.model.ActivityTicketOrder.vo.TicketOrderStatisticItem;
import com.zhiwee.gree.service.Activity.ActivityInfoService;


import com.zhiwee.gree.service.ActivityTicketOrder.ActivityTicketOrderService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.web.annotation.EnableGetRequest;
import xyz.icrab.common.web.annotation.EnablePageRequest;

import java.util.*;


/**
 * @author sun
 * @since 2018/7/12 15:11
 */
@RestController
@RequestMapping("/activityTicketOrder")
@EnablePageRequest
@EnableGetRequest
public class ActivityTicketOrderController {


    @Autowired
    private ActivityInfoService activityInfoService;
    @Autowired
    private ActivityTicketOrderService activityTicketOrderService;


/**
 * Description: 认筹统计
 * @author: sun
 * @Date 上午10:05 2019/7/5
 * @param:
 * @return:
 */
    @RequestMapping("/ticketOrderByDate")
    public Result<Collection<?>> statisticAllByDate(@RequestBody(required = false) Map<String, Object> param) {
        if (param == null) {
            param = new HashMap<>();
        }

        List<TicketOrderStatisticItem> items = activityTicketOrderService.ticketOrderByDate(param);
        if (items == null) {
            return Result.ok(Collections.emptyList());
        }
        return Result.ok(items);
    }
}
