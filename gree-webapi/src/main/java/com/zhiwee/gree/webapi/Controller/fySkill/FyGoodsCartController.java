package com.zhiwee.gree.webapi.Controller.fySkill;

import com.zhiwee.gree.model.FyGoods.Fygoods;
import com.zhiwee.gree.model.FyGoods.FygoodsCart;
import com.zhiwee.gree.model.GoodsInfo.Goods;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.resultBase.BaseResult;
import com.zhiwee.gree.service.FyGoods.FygoodsService;
import com.zhiwee.gree.service.GoodsInfo.GoodsInfoService;
import com.zhiwee.gree.service.GoodsInfo.GoodsService;
import com.zhiwee.gree.webapi.util.DateUtil;
import com.zhiwee.gree.webapi.util.JsonUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.web.annotation.*;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;

@RestController
@RequestMapping("/skillgoodsCart")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class FyGoodsCartController {

    @Resource
    private FygoodsService  fygoodsService;
    @Resource
    private GoodsInfoService  goodsInfoService;
    @Resource
    private RedisTemplate<String, String> redisTemplate;
    @Resource
    private GoodsService goodsService;

    /**
     * @Author: jick
     * @Date: 2019/12/12 16:00
     *购物车信息存入redis
     */
    @RequestMapping("/add")
    public Result<?>  addCart(@Session("ShopUser") ShopUser shopUser, @RequestBody(required = false) Map<String, Object> param){

        //首先判断redis值是否存在该商品，如果不存在直接添加，存在则修改更新
          Integer type= (Integer) param.get("type");
                if (StringUtils.isEmpty(String.valueOf(type))){
                    return Result.of(Status.ClientError.BAD_REQUEST,"缺少参数类型");
                }
        Goods fygoods  =   goodsService.get((String)param.get("id"));

        HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
        // 先查询,判断redis中是否有商品信息
        Map<String, String> resultMap = hashOperations.entries("userCart:"+shopUser.getId());

        if(!resultMap.isEmpty() && resultMap.size() > 0){
            //redis中有数据，判断是否有对应的数据
            // 根据map的key获取value
            String  goodsStr = resultMap.get((String) param.get("id"));


            //判断Redis中是否有对应的商品信息，如果存在进行修改商品数量操作(id 型号)
            if(null != goodsStr  &&  goodsStr.length() > 0){
                //转换成原始数据
                 FygoodsCart  fygoodsCart   =   JsonUtil.jsonStr2Object(goodsStr, FygoodsCart.class);

                //重新计算数量和总价格

                fygoodsCart.setCount((Integer) param.get("count"));
                fygoodsCart.setTotalPrice(fygoodsCart.getPrice().multiply(BigDecimal.valueOf(fygoodsCart.getCount())));

                // 重新添加至map
                resultMap.put(String.valueOf(param.get("id")), JsonUtil.object2JsonStr(fygoodsCart));

            }else{
                //如果没有对应的商品则直接添加
                //添加操作
                FygoodsCart  fygoodsCart  =  new FygoodsCart();
                fygoodsCart.setId(DateUtil.date2Str(new Date()));
                fygoodsCart.setUserId(shopUser.getId());
                fygoodsCart.setPhoneNum(shopUser.getPhone());
                fygoodsCart.setGoodsId(fygoods.getId());
                fygoodsCart.setGoodsName(fygoods.getName());
                fygoodsCart.setCover(fygoods.getGoodCover());
                fygoodsCart.setPrice(fygoods.getShopPrice());
                fygoodsCart.setCount(1);
                fygoodsCart.setTotalPrice(fygoods.getShopPrice());
                // 新增商品购物车信息
                resultMap.put(String.valueOf(param.get("id")), JsonUtil.object2JsonStr(fygoodsCart));
            }

        }else{
            //添加操作
            FygoodsCart  fygoodsCart  =  new FygoodsCart();
            fygoodsCart.setId(DateUtil.date2Str(new Date()));
            fygoodsCart.setUserId(shopUser.getId());
            fygoodsCart.setPhoneNum(shopUser.getPhone());
            fygoodsCart.setGoodsId(fygoods.getId());
            fygoodsCart.setGoodsName(fygoods.getName());
            fygoodsCart.setCover(fygoods.getGoodCover());
            fygoodsCart.setPrice(fygoods.getShopPrice());
            fygoodsCart.setCount(1);
            fygoodsCart.setTotalPrice(fygoods.getShopPrice());
            // 新增商品购物车信息
            resultMap.put(String.valueOf(param.get("id")), JsonUtil.object2JsonStr(fygoodsCart));
        }
        // 添加至redis
        hashOperations.putAll("userCart:"+shopUser.getId() , resultMap);
        return    Result.ok();

    }




    /**
     * @Author: jick
     * @Date: 2019/12/12 17:30
     * 获取购物车类表
     */
    @RequestMapping("/cart/list")
  //  @PermissionMode(PermissionMode.Mode.White)
    public   Result<?>  getFyGoodsCartList(@Session("ShopUser") ShopUser shopUser){
  //  public   Result<?>  getFyGoodsCartList(){
   //    ShopUser shopUser = new ShopUser();
   //    shopUser.setId("5df30734fbe19b3508d9ab05");

        HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
        Map<String, String> resultMap = hashOperations.entries("userCart:"+shopUser.getId());

        // 初始化返回对象
        List<FygoodsCart> fygoodsCartList = new ArrayList<>();

        //循环将信息加入集合
        if (!resultMap.isEmpty() && resultMap.size() > 0) {

          //  BigDecimal allPrice = new BigDecimal(0);

            // 循环获取
            for (Map.Entry<String, String> map : resultMap.entrySet()) {
                //  GoodsVo goodsVo = JsonUtil.jsonStr2Object(map.getValue(), GoodsVo.class);
                FygoodsCart   fygoodsCart = JsonUtil.jsonStr2Object(map.getValue(),FygoodsCart.class);
                fygoodsCartList.add(fygoodsCart);
               // allPrice  =   allPrice.add(fygoodsCart.getTotalPrice());
            }

            Collections.sort(fygoodsCartList);//排序

        }
        return   Result.ok(fygoodsCartList);

    }


    /**
     * @Author: jick
     * @Date: 2019/12/12 17:47
     * 删除购物车里面的数据
     */
       @RequestMapping("/cart/delete")
     public  Result<?>  ClearGooodsCart(@Session("ShopUser") ShopUser shopUser,@RequestBody  Map<String,Object> param){


         HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
         // 先查询
         Map<String, String> resultMap = hashOperations.entries("userCart:"+shopUser.getId());

         //非空判断，非空就遍历查找
         // 判断map是否为空
         if (!resultMap.isEmpty() && resultMap.size() > 0) {
             String[]  ids = ((String) param.get("ids")).split(",");
             //遍历删除
             if(ids.length>0){
                 for(String id : ids){
                     hashOperations.delete("userCart:"+shopUser.getId(),id);
                 }
             }

         }
         return     Result.ok();
     }






























    
    
    
}
