package com.zhiwee.gree.webapi.Controller.card;


import com.zhiwee.gree.model.FyGoods.BargainGoods;
import com.zhiwee.gree.model.Shop.Shop;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.User;
import com.zhiwee.gree.model.card.BookingCard;
import com.zhiwee.gree.model.card.BookingCards;
import com.zhiwee.gree.service.ShopUser.ShopUserService;
import com.zhiwee.gree.service.card.BookingCardService;
import com.zhiwee.gree.service.card.BookingCardUserService;
import com.zhiwee.gree.service.card.BookingCardsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.util.KeyUtils;
import xyz.icrab.common.util.sms.SMSResult;
import xyz.icrab.common.util.sms.SMSUtils;
import xyz.icrab.common.web.annotation.EnableGetRequest;
import xyz.icrab.common.web.annotation.EnableListRequest;
import xyz.icrab.common.web.annotation.PermissionMode;
import xyz.icrab.common.web.annotation.Session;
import xyz.icrab.ums.util.consts.SessionKeys;

import java.text.SimpleDateFormat;
import java.util.*;

@RequestMapping("/bookingCardUser")
@Controller
@ResponseBody
@EnableListRequest
@EnableGetRequest
public class BookingCardUserController {
    @Autowired
    private BookingCardService bookingCardService;
    @Autowired
    private BookingCardsService bookingCardsService;
    @Autowired
    private BookingCardUserService bookingCardUserService;
    @Autowired
    private ShopUserService shopUserService;

    @Value("${sms.captcha.noBookingCardcode}")
    private String noBookingCardcode;

    @Autowired
    private SMSUtils smsUtils;

    /**
     * @author tzj
     * @description app获取用户预售卡
     * @date 2020/2/22 11:23
     */
    @RequestMapping("/getBooking")
    @ResponseBody
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> getBooking(@RequestBody(required = false) Map<String, Object> params) {
        if (params.get("phone") == null || "".equals(params.get("phone"))) {
            return Result.of(Status.ClientError.BAD_REQUEST, "请输入客户手机号!");
        }
        List<BookingCards> list = bookingCardsService.list(params);
        if (list.size() > 0) {
            list.removeIf(next -> next.getState() == -1 || next.getState() == 0 || next.getState() == 2);
            return Result.ok(list);
        } else {
            return Result.of(Status.ClientError.BAD_REQUEST, "该客户尚未购买或未支付预售卡!");
        }
    }

    /**
     * @author tzj
     * @description app核销预售卡
     * @date 2020/2/22 11:23
     */
    @RequestMapping("/noBooking")
    @ResponseBody
    public Result<?> noBooking(@RequestBody(required = false) BookingCards bookingCards, @Session(SessionKeys.USER) User user) {
        bookingCards = bookingCardsService.getOne(bookingCards);
        if (bookingCards == null) {
            return Result.of(Status.ClientError.BAD_REQUEST, "查无此卡请重试!");
        }
        bookingCards.setState(3);
        bookingCards.setConfirmMan(user.getNickname());
        bookingCards.setConfirmTime(new Date());
        bookingCardsService.update(bookingCards);

        //短信通知
        return Result.ok(sendNoBookingCardMsg(bookingCards.getPhone(),bookingCards.getConfirmTime(),"0558-2296576"));
    }

    public SMSResult sendNoBookingCardMsg(String mobile, Date drawTime, String dealerPhone) {
        Map<String, String> map = new HashMap<>();
        map.put("drawTime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(drawTime));
        map.put("dealerPhone", dealerPhone);
        return smsUtils.sendSMS(mobile, noBookingCardcode, map);
    }


    @RequestMapping("/checkBook")
    @ResponseBody
    public Result<?> checkBook(@RequestBody(required = false) BookingCards bookingCards, @Session(SessionKeys.USER) User user) {
        bookingCards.setBookingCardId("1778217b76a146569cef2947ebc0e0dd");
        bookingCards = bookingCardsService.getOne(bookingCards);
        if (bookingCards == null) {
            return Result.of(Status.ClientError.BAD_REQUEST, "查无此卡请确认是否购买!");
        }
        if (bookingCards.getState()==1) {
            bookingCards.setState(3);
            bookingCards.setConfirmId(user.getId());
            bookingCards.setConfirmMan(user.getNickname());
            bookingCards.setConfirmTime(new Date());
            bookingCardsService.update(bookingCards);
                   return Result.ok("核销成功");
        }else {
            return Result.of(Status.ClientError.BAD_REQUEST, "您已经核销过一次，无需再次核销!");
        }
        //短信通知
//        return Result.ok(sendNoBookingCardMsg(bookingCards.getPhone(),bookingCards.getConfirmTime(),"0558-2296576"));
    }


    @RequestMapping("/getInfo")
    @ResponseBody
    public Result<?> getInfo(@RequestBody(required = false) BookingCards bookingCards, @Session(SessionKeys.USER) User user) {
//        bookingCards.getNewTime()
        ShopUser shopUser = shopUserService.get(bookingCards.getUserId());
        Date checkTime = shopUser.getCheckTime();
        long time = checkTime.getTime();
        long time1 = (new Date()).getTime();
        long l = (time1 - time) / 60/1000;
        if (l>2){
            return Result.of(Status.ClientError.BAD_REQUEST, "超时!请重新获取二维码");
        }


        bookingCards.setBookingCardId("1778217b76a146569cef2947ebc0e0dd");
        bookingCards = bookingCardsService.getOne(bookingCards);
        if (bookingCards == null) {
            return Result.of(Status.ClientError.BAD_REQUEST, "查无此卡请确认是否购买!");
        }
        if (bookingCards.getState()==1) {
            return Result.ok(bookingCards);
        }else if (bookingCards.getState()==3){
            return Result.of(Status.ClientError.BAD_REQUEST, "您已经核销过一次，无需再次核销!");
        }else {
            return Result.of(Status.ClientError.BAD_REQUEST, "请确认是否购买过预售卡!");
        }
        //短信通知
//        return Result.ok(sendNoBookingCardMsg(bookingCards.getPhone(),bookingCards.getConfirmTime(),"0558-2296576"));
    }

    @RequestMapping("/getNum")
    @ResponseBody
    public Result<?> getNum( @Session(SessionKeys.USER) User user) {
            Map <String,Object> param=new HashMap<>(5);
            param.put("confirmId",user.getId());
        List<BookingCards> list = bookingCardsService.list(param);
        return Result.ok(list.size());
    }
    @RequestMapping("/getAll")
    @ResponseBody
    public Result<?> getAll(@RequestBody Map<String, Object> params, @Session(SessionKeys.USER) User user) {
        if (params==null) {
          params = new HashMap<>(5);
        }
        params.put("confirmId",user.getId());
        List<BookingCards> list = bookingCardsService.list(params);
        return Result.ok(list);
    }






}