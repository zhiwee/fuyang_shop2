package com.zhiwee.gree.webapi.Controller.GoodsInfo;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.zhiwee.gree.model.FyGoods.GoodsLabel;
import com.zhiwee.gree.model.GoodsInfo.*;
import com.zhiwee.gree.model.GoodsInfo.GoodVo.CategorysExo;
import com.zhiwee.gree.model.GoodsInfo.GoodVo.GoodsExo;
import com.zhiwee.gree.model.GoodsInfo.GoodVo.GoodsFyExo;
import com.zhiwee.gree.model.GoodsInfo.GoodVo.GoodsInfoVO;
import com.zhiwee.gree.model.OnlineActivity.vo.OnlineActivityInfoVo;
import com.zhiwee.gree.model.User;
import com.zhiwee.gree.service.GoodsInfo.*;
import com.zhiwee.gree.service.support.FileUpload.FileUploadService;
import com.zhiwee.gree.service.tgoods.GoodsLabelService;
import com.zhiwee.gree.webapi.util.JsonUtil;
import com.zhiwee.gree.webapi.util.SystemControllerLog;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.http.impl.auth.GGSSchemeBase;
import org.apache.poi.ss.usermodel.Workbook;
import org.jeecgframework.poi.excel.ExcelExportUtil;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.solr.core.SolrTemplate;
import org.springframework.data.solr.core.query.*;
import org.springframework.data.solr.core.query.result.HighlightEntry;
import org.springframework.data.solr.core.query.result.HighlightPage;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.*;
import xyz.icrab.common.util.IdGenerator;
import xyz.icrab.common.util.KeyUtils;
import xyz.icrab.common.web.annotation.*;
import xyz.icrab.common.web.param.IdsParam;
import xyz.icrab.ums.util.consts.SessionKeys;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.*;


/**
 * @author jick
 * @date 2019/8/30
 */
@RestController
@RequestMapping("/goods")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class GoodsController {
    @Resource
    private GoodsService goodsService;
    @Resource
    private GoodsBaseService goodsBaseService;
    @Resource
    private GoodsLabelService goodsLabelService;
    @Resource
    private CategorysService categorysService;
    @Resource
    private GoodsInfoPictureService goodsInfoPictureService;
    @Resource
    private GoodsCategoryService goodsCategoryService;
    @Resource
    private FileUploadService fileUploadService;
    @Resource
    private LActivityGoodsService lActivityGoodsService;
    @Resource
    private SolrTemplate solrTemplate;
    @Resource
    private RedisTemplate<String, String> redisTemplate;


    /**
     * 根据分类获取商品，分页，前台使用
     */
    @RequestMapping("/getGoods")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> getGoods(String goodsId) {
        if ("".equals(goodsId)) {
            return Result.of(Status.ClientError.BAD_REQUEST, "系统未检测到商品");
        }
        Map<String, Object> params = new HashMap<>();
        params.put("goodsId", goodsId);
        GoodsInfoVO goods = goodsService.getOneByGoods(params);
        return Result.ok(goods);
    }

    /**
     * 商品信息入库
     *
     * @param goods
     * @return
     */

    @RequestMapping(value = "/saveOrupdate")
    @ResponseBody
    public Result<?> productInfoSave(@RequestBody Goods goods) {
        if (goods.getShopPrice() == null || goods.getStoreCount() == null) {
            return Result.of(Status.ClientError.BAD_REQUEST, "商品信息不全请检查");
        }
        //必须是三种不同的标签
        Boolean flgLabel = checkOnlyLabel(goods);
        if (flgLabel) {
            return Result.of(Status.ClientError.BAD_REQUEST, "必须是不同的标签");
        }

        //如果主键为空
        if (goods.getId() == null || "".equals(goods.getId())) {
            //设置主键
            String key = KeyUtils.getKey();
            goods.setState(1);
            goods.setId(key);
            goods.setSaleCount(0);

            //校验是否有同样标签的商品
            Boolean flg = checkOnly(goods);
            if (flg) {
                return Result.of(Status.ClientError.BAD_REQUEST, "已经有了同样标签的商品");
            }
            GoodsBase goodsBase = goodsBaseService.get(goods.getGoodsId());
            if (goodsBase == null) {
                return Result.of(Status.ClientError.BAD_REQUEST, "基础商品不存在请重新添加");
            }
            goods.setJiadian(goodsBase.getJiadian());
            goods.setSendType(goodsBase.getSendType());
            //把标签的父类放进去
            goods = setFather(goods);
            //存入数据库
            goodsService.save(goods);
        } else {
            //校验是否有同样标签的商品
            Boolean flg = checkOnly2(goods);
            if (flg) {
                return Result.of(Status.ClientError.BAD_REQUEST, "已经有了同样标签的商品");
            }
            GoodsBase goodsBase = goodsBaseService.get(goods.getGoodsId());
            if (goodsBase == null) {
                return Result.of(Status.ClientError.BAD_REQUEST, "基础商品不存在请重新添加");
            }
            goods.setJiadian(goodsBase.getJiadian());
            goods.setSendType(goodsBase.getSendType());

            //否则是更新操作
            goods = setFather(goods);
            goods.setState(1);
            goodsService.update(goods);
        }
        return Result.ok(goods);

    }

    /**
     * @author tzj
     * @description 必须是三种不同的标签
     * @date 2020/4/16 13:55
     */
    private Boolean checkOnlyLabel(Goods goods) {
        GoodsLabel goodsLabel = new GoodsLabel();
        GoodsLabel goodsLabel2 = new GoodsLabel();
        GoodsLabel goodsLabel3 = new GoodsLabel();
        if (goods.getLabel1id() != null && !"".equals(goods.getLabel1id())) {
            String label1id = goods.getLabel1id();
            goodsLabel = goodsLabelService.get(label1id);
        }
        if (goods.getLabel2id() != null && !"".equals(goods.getLabel2id())) {
            String label2id = goods.getLabel2id();
            goodsLabel2 = goodsLabelService.get(label2id);
        }
        if (goods.getLabel3id() != null && !"".equals(goods.getLabel3id())) {
            String label3id = goods.getLabel3id();
            goodsLabel3 = goodsLabelService.get(label3id);
        }
        if (goodsLabel.getParentId() != null && goodsLabel2.getParentId() != null && goodsLabel.getParentId().equals(goodsLabel2.getParentId())) {
            return true;
        }
        if (goodsLabel.getParentId() != null && goodsLabel3.getParentId() != null && goodsLabel.getParentId().equals(goodsLabel3.getParentId())) {
            return true;
        }
        if (goodsLabel2.getParentId() != null && goodsLabel3.getParentId() != null && goodsLabel2.getParentId().equals(goodsLabel3.getParentId())) {
            return true;
        }
        return false;
    }

    /**
     * @author tzj
     * @description 商品必须是唯一
     * @date 11:50
     */
    private Boolean checkOnly(Goods goods) {
        Map<String, Object> param = new HashMap<>(5);
        param.put("goodsId", goods.getGoodsId());
        List<Goods> list = goodsService.list(param);
        if (list.size() > 0) {
            String label1id = goods.getLabel1id();
            String label2id = goods.getLabel2id();
            String label3id = goods.getLabel3id();
            for (Goods next : list) {
                String label1id1 = next.getLabel1id();
                String label2id1 = next.getLabel2id();
                String label3id1 = next.getLabel3id();
                for (int i = 0; i < 3; i++) {
                    if (label1id1.equals(label1id) || label1id1.equals(label2id) || label1id1.equals(label3id)) {
                        if (label2id1.equals(label1id) || label2id1.equals(label2id) || label2id1.equals(label2id)) {
                            if (label3id1.equals(label1id) || label3id1.equals(label2id) || label3id1.equals(label3id)) {
                                return true;
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    /**
     * @author tzj
     * @description 商品必须是唯一
     * @date 11:50
     */
    private Boolean checkOnly2(Goods goods) {
        Map<String, Object> param = new HashMap<>(5);
        param.put("goodsId", goods.getGoodsId());
        List<Goods> list = goodsService.list(param);
        if (list.size() > 0) {
            //如果只有一条记录且是自身
            if (list.size() == 1 && list.get(0).getId().equals(goods.getId())) {
                return false;
            }
            int num = 0;
            String label1id = goods.getLabel1id();
            String label2id = goods.getLabel2id();
            String label3id = goods.getLabel3id();
            List<Goods> checkList = new ArrayList<>();
            for (Goods next : list) {
                String label1id1 = next.getLabel1id();
                String label2id1 = next.getLabel2id();
                String label3id1 = next.getLabel3id();
                if (label1id1.equals(label1id) || label1id1.equals(label2id) || label1id1.equals(label3id)) {
                    if (label2id1.equals(label1id) || label2id1.equals(label2id) || label2id1.equals(label2id)) {
                        if (label3id1.equals(label1id) || label3id1.equals(label2id) || label3id1.equals(label3id)) {
                            num++;
                            checkList.add(next);
                        }
                    }
                }
            }
            if (num > 1) {
                return true;
            } else if (num == 1) {
                return !goods.getId().equals(checkList.get(0).getId());
            } else {
                return false;
            }
        }
        return false;
    }

    /**
     * @author tzj
     * @description 找到标签父类存储
     * @date 2020/4/16 11:20
     */
    private Goods setFather(Goods goods) {
        if (!"".equals(goods.getLabel1id()) && goods.getLabel1id() != null) {
            GoodsLabel one = goodsLabelService.get(goods.getLabel1id());
            GoodsLabel goodsLabel = goodsLabelService.get(one.getParentId());
            goods.setLabel1parentId(goodsLabel.getId());
            goods.setLabel1parentName(goodsLabel.getName());
        }
        if (!"".equals(goods.getLabel2id()) && goods.getLabel2id() != null) {
            GoodsLabel one = goodsLabelService.get(goods.getLabel2id());
            GoodsLabel goodsLabel = goodsLabelService.get(one.getParentId());
            goods.setLabel2parentId(goodsLabel.getId());
            goods.setLabel2parentName(goodsLabel.getName());
        }
        if (!"".equals(goods.getLabel3id()) && goods.getLabel3id() != null) {
            GoodsLabel one = goodsLabelService.get(goods.getLabel3id());
            GoodsLabel goodsLabel = goodsLabelService.get(one.getParentId());
            goods.setLabel3parentId(goodsLabel.getId());
            goods.setLabel3parentName(goodsLabel.getName());
        }
        return goods;
    }

    /**
     * @author tzj
     * @description 循环扎到所有他所在的二级
     * @date 2019/12/27 17:12
     */
    private GoodsCategory getFather(GoodsCategory goodsCategory) {
        GoodsCategory goodsCategory1 = new GoodsCategory();
        if (goodsCategory.getRank() == 2) {
            return goodsCategory;
        } else {
            goodsCategory1.setId(goodsCategory.getParentId());
            goodsCategory1 = goodsCategoryService.getOne(goodsCategory1);
            getFather(goodsCategory1);
            return goodsCategory1;
        }
    }


    /**
     * 列表展示
     */

    @RequestMapping(value = "searchAll")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> searchAll(@RequestBody(required = false) Map<String, Object> params, Pagination pagination) {
        Pageable<Goods> list = goodsService.queryInfoList(params, pagination);
        return Result.ok(list);
    }


    /**
     * 删除操作
     */
    @RequestMapping("/delete")
    public Result<?> goodsInfoDel(@RequestBody @Valid IdsParam map) {
        List<String> ids = map.getIds();

        for (String id : ids) {
            Goods goods = new Goods();
            goods.setId(id);
            goods.setState(2);
            goodsService.update(goods);
        }
        return Result.ok();

    }


    /**
     * 通过id查询商品的详细信息
     */
    @RequestMapping("/get")
    public Result<?> getMe(@RequestBody(required = false) Map<String, Object> params) {

        Goods goods = goodsService.queryInfoListById(params);
        return Result.ok(goods);
    }


    /**
     * @author tzj
     * @description 秒杀商品
     * @date 2019/9/24 19:03
     */
    @RequestMapping("/getInfo")
    public Result<?> getInfo(@RequestBody(required = false) Map<String, Object> params) {

        LAGoodsInfo laGoodsInfo = lActivityGoodsService.queryKillInfoById(params);
        return Result.ok(laGoodsInfo);
    }


    /**
     * @return
     * @author tzj
     * @description 秒杀商品表
     * @date 2019/9/24 14:44
     */

    @RequestMapping(value = "secKillGoods")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> secKillGoods(@RequestBody(required = false) Map<String, Object> params, Pagination pagination) {

        params.put("activityType", 1);
        Pageable<LAGoodsInfo> list = lActivityGoodsService.secKillGoods(params, pagination);


        List<LAGoodsInfo> list2 = lActivityGoodsService.secKillGoods2(params);

        HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();

        // 先查询
        Map<String, String> resultMap = hashOperations.entries("SeckillGoods:001");

        //循环将值插入到redis

        if (resultMap.isEmpty()) {
            for (LAGoodsInfo lag : list2) {
                resultMap.put(lag.getActivityId() + ":" + lag.getGoodsId(), JsonUtil.object2JsonStr(lag));
            }
            hashOperations.putAll("SeckillGoods:001", resultMap);
        }

        return Result.ok(list);
    }

    /**
     * @Author: jick
     * @Date: 2019/10/19 17:48
     * 将缓存数据库里面的数据同步至数据库
     */
    @RequestMapping(value = "redisToDatabase")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> redisSysDatabase() {

        HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
        // 先查询
        Map<String, String> resultMap = hashOperations.entries("SeckillGoods:001");

        if (!resultMap.isEmpty()) {
            for (Map.Entry<String, String> map : resultMap.entrySet()) {
                LAGoodsInfo laGoodsInfo = JsonUtil.jsonStr2Object(map.getValue(), LAGoodsInfo.class);
                lActivityGoodsService.update(laGoodsInfo);
            }
            //删除缓存数据库数据。

        }

        return Result.ok();
    }


    /**
     * @return
     * @author tzj
     * @description 秒杀商品表更新价格
     * @date 2019/9/24 14:44
     */

    @RequestMapping("/upSecKillGoods")
    public Result<?> upSecKillGoods(@RequestBody(required = false) Map<String, Object> params) {
        LAGoodsInfo lActivityGoods = new LAGoodsInfo();
        lActivityGoods.setSecKillPrice(new BigDecimal((String) params.get("secKillPrice")));
        lActivityGoods.setId((String) params.get("id"));
        lActivityGoods.setStockNum(Integer.valueOf((String) params.get("stockNum")));
        lActivityGoodsService.update(lActivityGoods);
        return Result.ok();
    }


    /**
     * @author tzj
     * @description 前端获取秒杀商品
     * @date 2019/9/25 14:26
     */
    @RequestMapping("/getCurrentSceKillGoods")
    @PermissionMode(PermissionMode.Mode.White)
    @ResponseBody
    public Result<?> getCurrentSceKillGoods(@RequestBody(required = false) Map<String, Object> params) {
        if (params == null) {
            params = new HashMap<>();
        }
        params.put("time", new Date());
        params.put("activityType", 1);
        params.put("state", 1);


        List<OnlineActivityInfoVo> list = goodsService.getCurrentSceKillGoods(params);

      /*  for (OnlineActivityInfoVo seckillGood : list) {
            redisTemplate.boundHashOps("SeckillGoods").put(seckillGood.getId(),seckillGood);
        }*/

        if (list.size() > 0) {
            return Result.ok(list);
        } else {
            return Result.ok();
        }
    }


    @RequestMapping("/disableSec")
    @ResponseBody
    public Result<?> disable(@RequestBody @Valid IdsParam param, @Session(SessionKeys.USER) User user) {
        String id = param.getIds().get(0);

        //设置参数
        LAGoodsInfo laGoodsInfo = new LAGoodsInfo();
        laGoodsInfo.setId(id);
        laGoodsInfo.setState(2);

        lActivityGoodsService.update(laGoodsInfo);
        return Result.ok();
    }


    @RequestMapping("/enableSec")
    @ResponseBody
    public Result<?> enable(@RequestBody @Valid IdsParam param) {
        String id = param.getIds().get(0);

        //设置参数
        LAGoodsInfo laGoodsInfo = new LAGoodsInfo();
        laGoodsInfo.setId(id);
        laGoodsInfo.setState(1);

        lActivityGoodsService.update(laGoodsInfo);
        return Result.ok();
    }

    @RequestMapping("/disableNgh")
    @ResponseBody
    public Result<?> disableNgh(@RequestBody @Valid IdsParam param, @Session(SessionKeys.USER) User user) {
        String id = param.getIds().get(0);
        Map<String, Object> params = new HashMap<>();
        params.put("id", id);
        params.put("state", 2);
        goodsService.upSecKillPeople(params);
        return Result.ok();
    }


    @RequestMapping("/enableNgh")
    @ResponseBody
    public Result<?> enableNgh(@RequestBody @Valid IdsParam param, @Session(SessionKeys.USER) User user) {
        String id = param.getIds().get(0);
        Map<String, Object> params = new HashMap<>();
        params.put("id", id);
        params.put("state", 1);
        goodsService.upSecKillPeople(params);
        return Result.ok();
    }


    /**
     * @author tzj
     * @description 商品详情图片信息存入数据库
     * @date 2019/12/27 17:44
     */
    @RequestMapping("/goodsInfoSave")
    @ResponseBody
    public Result<?> goodsImgsave(@RequestBody GoodsInfoPicture goodsInfoPicture) {
        //url地址切割
        String[] str = goodsInfoPicture.getImgUrl().split(",");
        for (String s : str) {
            //图片信息存入数据库
            goodsInfoPicture.setId(IdGenerator.uuid());
            goodsInfoPicture.setImgUrl(s);
            goodsInfoPicture.setCreateTime(new Date());
            goodsInfoPictureService.save(goodsInfoPicture);
        }
        return Result.ok();
    }




   /*
   @return: 查询商品详细图片列表   （前台和后台）
   * @Author: jick
   * @Date: 2019/9/29 14:30
   */

    @RequestMapping("/GoodsInfoPictureList")
    @ResponseBody
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> goodsImglist(String goodsId) {

        GoodsInfoPicture goodsInfoPicture = new GoodsInfoPicture();
        goodsInfoPicture.setGoodsId(goodsId);
        List<GoodsInfoPicture> list = goodsInfoPictureService.list(goodsInfoPicture);
        return Result.ok(list);

    }


    /*
       商品详情图片删除
    * @Author: jick
    * @Date: 2019/9/29 23:00
    */
    @RequestMapping("/detailPictureDelete")
    @ResponseBody
    public Result<?> goodsInfoPictureDelController(String pathName) {

        //删除服务器里面的数据
        fileUploadService.fileDelete(pathName);

        //删除数据里面的数据；
        GoodsInfoPicture goodsInfoPicture = new GoodsInfoPicture();

        goodsInfoPicture.setImgUrl(pathName);

        goodsInfoPictureService.delete(goodsInfoPicture);

        return Result.ok();

    }


    /*

     * @Author: jick
     * 前台点击商品，查看对应商品基本信息。
     * 价格  库存  销量   等等。
     * @Date: 2019/10/8 10:26
     */
    @RequestMapping("/getPortalBaseInfo")
    @ResponseBody
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> queryPortalGoodsInfoById(String id) {

        //非空判断
        if (null == id) {
            return null;
        } else {

            Goods goods = goodsService.get(id);
            return Result.ok(goods);

        }

    }

    /**
     * @Author: jick
     * @Date: 2019/10/8 15:04
     * 前台规格参数展示。
     */
    @RequestMapping("/getSpecsById")
    @ResponseBody
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> querySpecsByIdController(String id) {
        //非空判断
        Map<String, Object> params = new HashMap<>();
        params.put("id", id);

        if (null == id) {
            return null;
        } else {
            List<ProductAttribute> list = goodsService.querySpecsById(params);
            return Result.ok(list);
        }

    }


    /**
     * @Author: jick
     * @Date: 2019/10/9 17:54
     * 前端查看商品的参数属性
     */
    @RequestMapping("/queryAttrInfoById")
    @ResponseBody
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> showGoodsAttrInfo(String id) {

        //非空判断
        Map<String, Object> params = new HashMap<>();
        params.put("id", id);

        if (null == id) {
            return null;
        } else {
            List<ProductAttribute> list = goodsService.queryAttrById(params);
            return Result.ok(list);
        }

        //  rm   -rf  *
    }


    /**
     * @Author: jick
     * @Date: 2019/10/9 17:03
     * 商品信息导搜索引擎
     */
    @RequestMapping("/solrTemplateAdd")
    @ResponseBody
    public Result<?> goodsInfoAddSolr(@RequestBody Map<String, Object> params) {
        if (params == null) {
            params = new HashMap<>();
        }

        params.put("state", 1);

        //搜索所有状态为1的数据
        List<Goods> l1 = goodsService.list(params);
        List<SolrGoods> l2 = new ArrayList<>();

        //获取数据封装
        for (Goods g : l1) {
            SolrGoods solrGoods = new SolrGoods();
            solrGoods.setId(g.getId());
            solrGoods.setName(g.getName());
            solrGoods.setActivePrice(g.getActivePrice().doubleValue());
            solrGoods.setShopPrice(g.getShopPrice().doubleValue());
            solrGoods.setGoodCover(g.getGoodCover());
            solrGoods.setDescribes(g.getDescribes());
            solrGoods.setSaleCount(g.getSaleCount());
            solrGoods.setCommentNum(g.getCommentNum());
            l2.add(solrGoods);
        }
        //存入数据库
        solrTemplate.saveBeans(l2);
        solrTemplate.commit();
        return Result.ok();

    }


    /**
     * @Author: jick
     * @Date: 2019/10/10 10:32
     * 删除搜索引擎中所有的数据
     */
    @RequestMapping(value = "/deleteAll")
    public Result<?> deleteAll() {
        Query query = new SimpleQuery("*:*");
        solrTemplate.delete(query);
        solrTemplate.commit();
        return Result.ok();
    }


    /**
     * @Author: jick
     * @Date: 2019/10/10 14:38
     * 搜索商品
     */
    @RequestMapping(value = "/goodsHightSearch")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> HightSearch(@RequestBody Map<String, Object> param, HttpServletRequest req, HttpServletResponse res) {

        HighlightQuery query = new SimpleHighlightQuery();

        //过滤条件

        Criteria criteria = new Criteria();
        //根据title
        if (param.get("name") != null && !"".equals((String) param.get("name"))) {
            criteria = criteria.and(new Criteria("goodsInfo_keywords").contains((String) param.get("name")));

        }
        //根据价格升序或降序
        if (param.get("shopPrice") != null && !"".equals((String) param.get("shopPrice"))) {
            if ("1".equals(param.get("shopPrice"))) {
                //按照价格降序排序
                Sort sort = new Sort(Sort.Direction.DESC, "shopPrice");
                query.addSort(sort);
            } else if ("2".equals(param.get("shopPrice"))) {
                //按照价格升序升序排序
                Sort sort = new Sort(Sort.Direction.ASC, "shopPrice");
                query.addSort(sort);
            }

        }
        //根据价格区间搜索
        if (param.get("pricemin") != null && !"".equals((String) param.get("pricemin"))) {
            criteria = criteria.and(new Criteria("shopPrice").greaterThanEqual((String) param.get("pricemin")));
        }
        if (param.get("pricemax") != null && !"".equals((String) param.get("pricemax"))) {
            criteria = criteria.and(new Criteria("shopPrice").lessThanEqual((String) param.get("pricemax")));
        }
        //根据销量进行升序或者降序排序
         /* if(param.get("saleCount") != null &&  !((String)param.get("saleCount")).equals("")){
              if(param.get("saleCount").equals("1")){
                  //按照销量降序排序
                  Sort sort = new Sort(Sort.Direction.DESC,"saleCount");
                  query.addSort(sort);
              }else if(param.get("saleCount").equals("2")){
                  //按照销量升序升序排序
                  Sort sort = new Sort(Sort.Direction.ASC,"saleCount");
                  query.addSort(sort);
              }
          }*/

        //根据评论进行升序或者降序排序
        if (param.get("commentNum") != null && !"".equals((String) param.get("commentNum"))) {
            if ("1".equals(param.get("commentNum"))) {
                //按照评论降序排序
                Sort sort = new Sort(Sort.Direction.DESC, "commentNum");
                query.addSort(sort);
            } else if ("2".equals(param.get("commentNum"))) {
                //按照评论升序升序排序
                Sort sort = new Sort(Sort.Direction.ASC, "commentNum");
                query.addSort(sort);
            }
        }

        //综合查询,根据销量排序
        if (param.get("curr") != null && !"".equals((String) param.get("curr"))) {
            //按照销量降序排序
            Sort sort = new Sort(Sort.Direction.DESC, "1");
            query.addSort(sort);

        }


        //分页
        PageRequest pageable = null;
        if (param.get("pageNumber") == null) {
            param.put("pageNumber", 1);
            pageable = new PageRequest((Integer) param.get("pageNumber") - 1, 4);

        } else {
            pageable = new PageRequest((Integer) param.get("pageNumber") - 1, 4);
        }

        query.addCriteria(criteria);
        query.setPageRequest(pageable);
        // 设置高亮的属性
        HighlightOptions highlightOptions = new HighlightOptions();
        // 指定高亮的域
        highlightOptions.addField("name");
        // 前缀和后缀
        if (param.get("name") != null && !"".equals((String) param.get("name"))) {
            highlightOptions.setSimplePrefix("<em style='color:red'>");
            highlightOptions.setSimplePostfix("</em>");
            // 设置高亮选项
            query.setHighlightOptions(highlightOptions);
        }

        /*  // 高亮显示设置
          HighlightQuery query = new SimpleHighlightQuery();
          HighlightOptions highlightOptions = new HighlightOptions().addField("item_title");// 设置高亮字段
          highlightOptions.setSimplePrefix("<em style='color:red'>");// 设置高亮前缀
          highlightOptions.setSimplePostfix("</em>");// 设置高亮后缀
          query.setHighlightOptions(highlightOptions);// 设置高亮选项
          */


        query.setHighlightOptions(highlightOptions);

        HighlightPage<SolrGoods> queryForHighlightPage = solrTemplate.queryForHighlightPage(query, SolrGoods.class);

//        System.out.println(queryForHighlightPage);
        System.out.println(JSON.toJSONString(queryForHighlightPage, SerializerFeature.DisableCircularReferenceDetect));

        List<SolrGoods> content = queryForHighlightPage.getContent();
        // for (SolrGoods tbItem : content) {
        for (HighlightEntry<SolrGoods> h : queryForHighlightPage.getHighlighted()) {


            SolrGoods item = h.getEntity();
            //设置高亮
           /*   if(){

              }*/

            //设置高亮
            if (param.get("name") != null && !"".equals((String) param.get("name"))) {


                if (item.getName().contains((String) param.get("name"))) {
                    String[] strs = item.getName().split((String) param.get("name"));
                    String newName = item.getName();
                    if (strs.length > 1) {
                        newName = strs[0] + "<em style='color:red'>" + (String) param.get("name") + "</em>" + strs[1];

                    } else if (strs.length == 1) {

                        newName = strs[0] + "<em style='color:red'>" + (String) param.get("name") + "</em>";
                    } else if (strs.length == 0) {
                        newName = "<em style='color:red'>" + (String) param.get("name") + "</em>";
                    }
                    item.setName(newName);


                }

            }



           /*   if (h.getHighlights().size() > 0 && h.getHighlights().get(0).getSnipplets().size() > 0) {
                  List<String> a1 = h.getHighlights().get(0).getSnipplets();
                   HighlightEntry.Highlight a2 = h.getHighlights().get(0);
                  item.setName(h.getHighlights().get(0).getSnipplets().get(0));// 设置高亮显示结果。因为只有一个高亮域“.addField("item_title")”,所以不需要遍历，直接取第一个
              }*/

            //List<HighlightEntry.Highlight> highlights = queryForHighlightPage.getHighlights(tbItem);
            // 获取高亮部分
            /*  if(!highlights.isEmpty() && highlights.size() > 0){
                  List<String> snipplets = highlights.get(0).getSnipplets();
                  if(!snipplets.isEmpty() && snipplets.size() > 0){
                      String string =snipplets.get(0);
                      tbItem.setName(string);
                  }
              }*/

        }
        /*  int result = 0;
          if ((content.size() % 4) == 0) {
              result = content.size() / 4;                    // 保持double型数据类型
          } else {
              result = (content.size() / 4) + 1;             // 保持double型数据类型
          }*/

        int a = queryForHighlightPage.getTotalPages();//返回总页数
        long b = queryForHighlightPage.getTotalElements();//返回总记录数

        //  Pageable page = new Page(content.size(), result, (Integer) param.get("pageNumber"), 4, content);
        Pageable page = new Page(b, a, (Integer) param.get("pageNumber"), 4, content);

        return Result.ok(page);
    }


    /**
     * @author tzj
     * @description 小程序通过分获取商品
     * @date 2020/1/3 14:37
     */
    @RequestMapping("/getFyGoods")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> getFyGoods(@RequestBody(required = false) Map<String, Object> param) {
        if (param == null) {
            param = new HashMap<>(50);
        }
        List<Goods> list = goodsService.list(param);
        return Result.ok(list);
    }

    /**
     * @author tzj
     * @description 小程序通过ID获取商品信息
     * @date 2020/1/3 14:37
     */
    @RequestMapping("/getFyGoodsInfo")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> getFyGoodsInfo(Goods goods) {
        Goods list = goodsService.getOne(goods);
        return Result.ok(list);
    }


    /**
     * @author tzj
     * @description 小程序前端获取楼层商品
     * @date 2020/2/21 10:53
     */
    @RequestMapping("/getAllGoods")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> getAllGoods(@RequestBody(required = false) Map<String, Object> param) {
        //如果什么都不传
        List<Goods> allList = new ArrayList<>();
        if (param == null || param.isEmpty()) {
            for (int i = 1; i <= 7; i++) {
                param.put("floor", i);
                List<Goods> list = goodsService.list(param);
                allList.addAll(list);
            }
        } else {
            allList = goodsService.list(param);
        }
        return Result.ok(allList);
    }


    /**
     * @author tzj
     * @description 导出
     * @date 2020/4/13 13:06
     */
    @RequestMapping("/exportFyGoods")
    public void exportFyGoods(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        List<GoodsExo> list = goodsService.listExo();
        // 导出Excel
        OutputStream os = resp.getOutputStream();
        resp.setContentType("application/x-download");
        // 设置导出文件名称
        resp.setHeader("Content-Disposition",
                "attachment;filename=" + DateFormatUtils.format(new Date(), "yyyyMMddHHmmssSSS") + ".xls");
        ExportParams param = new ExportParams("商品分类信息", "商品分类信息", "商品分类信息");
        //param.setType(ExcelType.XSSF);
        Workbook excel = ExcelExportUtil.exportExcel(param, GoodsExo.class, list);
        excel.write(os);
        os.flush();
        os.close();
    }

    @RequestMapping("/exportFyGoods2")
    public void exportFyGoods2(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        List<GoodsFyExo> list = goodsService.listExo2();
        // 导出Excel
        OutputStream os = resp.getOutputStream();
        resp.setContentType("application/x-download");
        // 设置导出文件名称
        resp.setHeader("Content-Disposition",
                "attachment;filename=" + DateFormatUtils.format(new Date(), "yyyyMMddHHmmssSSS") + ".xls");
        ExportParams param = new ExportParams("商品分类信息", "商品分类信息", "商品分类信息");
        Workbook excel = ExcelExportUtil.exportExcel(param, GoodsFyExo.class, list);
        excel.write(os);
        os.flush();
        os.close();
    }
    /**
     * @author tzj
     * @description 商品信息
     * @date 2020/4/8 9:43
     */
    @RequestMapping("/getGoodsByKeyWord")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> getGoodsByKeyWord(@RequestBody(required = false) Map<String, Object> params) {
        List<Goods> list = goodsService.listAll(params);
        return Result.ok(list);
    }

    /**
     * @author tzj
     * @description 通过基础商品获取标签
     * @date 2020/4/16 15:27
     */
    @RequestMapping("/getLabelByBase")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> getLabelByBase(@RequestBody(required = false) Map<String, Object> params) {
        if (params == null) {
            params = new HashMap<>();
        }
        params.put("state", 1);
        //去到该商品的所有标签
        List<Goods> list = goodsService.listAll(params);
        List<String> labels = new ArrayList<>();
        List<String> fatherString = new ArrayList<>();
        if (list.size() > 0) {
            List<GoodsLabel> sonLabel = new ArrayList<>();
            List<GoodsLabel> fatherLabel = new ArrayList<>();
            for (Goods goods : list) {
                if (goods.getLabel1id() != null && !"".equals(goods.getLabel1id())) {
                    labels.add(goods.getLabel1id());
                }
                if (goods.getLabel2id() != null && !"".equals(goods.getLabel2id())) {
                    labels.add(goods.getLabel2id());
                }
                if (goods.getLabel3id() != null && !"".equals(goods.getLabel3id())) {
                    labels.add(goods.getLabel3id());
                }
            }

            //遍历找到他们的爸爸的id
            Set<String> result = new HashSet(labels);
            for (String s : result) {
                GoodsLabel goodsLabel = goodsLabelService.get(s);
                fatherString.add(goodsLabel.getParentId());
                sonLabel.add(goodsLabel);
            }
            //遍历找到他们的爸爸们
            Set<String> resultFather = new HashSet(fatherString);
            for (String s : resultFather) {
                GoodsLabel goodsLabel = goodsLabelService.get(s);
                fatherLabel.add(goodsLabel);
            }
            //循环爸爸保存儿子
            for (GoodsLabel goodsLabel : fatherLabel) {
                for (GoodsLabel label : sonLabel) {
                    if (label.getParentId().equals(goodsLabel.getId())) {
                        if (goodsLabel.getList() == null) {
                            goodsLabel.setList(new ArrayList<GoodsLabel>());
                        }
                        goodsLabel.getList().add(label);
                    }
                }
            }
            return Result.ok(fatherLabel);
        } else {
            return null;
        }
    }

    /**
     * @author tzj
     * @description 通过标签找到商品
     * @date 2020/4/16 17:42
     */
    @RequestMapping("/getGoodsByLabel")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> getGoodsBase(@RequestBody(required = false) Map<String, Object> params) {
        if (params == null) {
            params = new HashMap<>();
        }
        String label1id = (String) params.get("label1id");
        String label2id = (String) params.get("label2id");
        String label3id = (String) params.get("label3id");

        //获取标签存入集合
        Map<String, Object> keyString = new HashMap();
        if (!"".equals(label1id) && label1id != null) {
            keyString.put("label1id",label1id);
        }
        if (!"".equals(label2id) && label2id != null) {
            keyString.put("label2id",label2id);
        }
        if (!"".equals(label3id) && label3id != null) {
            keyString.put("label3id",label3id);
        }

        String goodsId = (String) params.get("goodsId");
        if ("".equals(goodsId) || goodsId == null) {
            return Result.of(Status.ClientError.BAD_REQUEST, "获取信息失败请重试");
        }
        //找到类型商品
        keyString.put("goodsId",goodsId);
        List<Goods> list = goodsService.list(keyString);
        if (list != null) {
            return Result.ok(list.get(0));
        } else {
            return Result.of(Status.ClientError.BAD_REQUEST, "该类型已经售完，请重新选择");
        }
    }


    /*
     * @author tzj
     * @description 通过标签找到具体商品
     * @date 2020/4/23 18:02
     */
    private List<Goods> choose(List<Goods> goodsList, Map<String, String> keyString) {
        Set<String> strings = keyString.keySet();

        List<Goods> result = new ArrayList<>(100);

        for (String string : strings) {
            for (Goods goods : goodsList) {


            }
        }

        return null;
    }


    /**
     * @author tzj
     * @description
     * @date 2020/4/8 9:43
     */
    @RequestMapping("/getAllByGoodsId")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> getAllByLabel(@RequestBody(required = false) Map<String, Object> params) {
        String goodsId = (String) params.get("goodsId");
        if ("".equals(goodsId) || goodsId == null) {
            return Result.of(Status.ClientError.BAD_REQUEST, "获取信息失败请重试");
        }
        //找到类型商品
        Map<String, Object> param = new HashMap<>();
        param.put("goodsId", goodsId);
        param.put("state", 1);
        List<Goods> goodsList = goodsService.listAll(param);
        if (goodsList.size() > 0) {
            return Result.ok(goodsList);
        } else {
            return Result.of(Status.ClientError.BAD_REQUEST, "该类型的已经售完了,请选择其他商品");
        }
    }


}
