package com.zhiwee.gree.webapi.Controller.shopCart;


import com.zhiwee.gree.model.BaseInfo.BaseInfo;
import com.zhiwee.gree.model.GoodsInfo.GoodsInfo;
import com.zhiwee.gree.model.GoodsInfo.ShopGoodsInfo;
import com.zhiwee.gree.model.GoodsInfo.ShopGoodsPicture;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.User;
import com.zhiwee.gree.service.BaseInfo.BaseInfoService;
import com.zhiwee.gree.service.GoodsInfo.GoodsInfoService;
import com.zhiwee.gree.service.GoodsInfo.ShopGoodsInfoService;
import com.zhiwee.gree.service.GoodsInfo.ShopGoodsPictureService;
import com.zhiwee.gree.service.ShopUser.ShopUserService;
import com.zhiwee.gree.service.shopCart.ShopCartService;
import com.zhiwee.gree.util.CookieUtils;
import com.zhiwee.gree.util.JsonUtils;
import com.zhiwee.gree.webapi.util.SystemControllerLog;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.util.HSSFColor;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.solr.core.SolrTemplate;


import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.web.annotation.*;
import xyz.icrab.common.web.session.Session;
import xyz.icrab.common.web.util.SessionUtils;
import xyz.icrab.ums.util.consts.SessionKeys;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;


/**
 * @author sun on 2019/4/29
 */
@RestController
@RequestMapping("/shopCart")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class ShopCartController {

    @Value("${COOKIE_SHOPCART_EXPIRE}")
    private Integer COOKIE_CART_EXPIRE;

    @Autowired
    private ShopCartService shopCartService;

    @Autowired
    private GoodsInfoService goodsInfoService;

    @Autowired
    private ShopGoodsInfoService shopGoodsInfoService;

    @Autowired
    private ShopGoodsPictureService shopGoodsPictureService;

    @Autowired
    private ShopUserService shopUserService;


    @Autowired
    private BaseInfoService baseInfoService;






    /**
     * Description: 从cookie中获取商品
     *
     * @author: sun
     * @Date 上午11:15 2019/5/7
     * @param:
     * @return:
     */
    private List<ShopGoodsInfo> getCartListFromCookie(HttpServletRequest request) {
        String json = CookieUtils.getCookieValue(request, "shopCart", true);
        //判断json是否为空
        if (StringUtils.isBlank(json)) {
            return new ArrayList<>();
        }
        //把json转换成商品列表
        List<ShopGoodsInfo> list = JsonUtils.jsonToList(json, ShopGoodsInfo.class);
        return list;
    }

    /**
     * Description: 购物车展示
     *
     * @author: sun
     * @Date 上午11:24 2019/5/7
     * @param:
     * @return:
     */
//    @SystemControllerLog(description = "购物车展示")
    @PermissionMode(PermissionMode.Mode.White)
    @RequestMapping("/showCart")
    public Result<?> showCartList(HttpServletRequest request, HttpServletResponse response) {
//        List<BaseInfo> baseInfos = baseInfoService.list();
//        BaseInfo baseInfo = baseInfos.get(0);
        //从cookie中取购物车列表
        List<ShopGoodsInfo> cartList = getCartListFromCookie(request);
        //判断用户是否为登录状态
        Session session = SessionUtils.get(request);
        ShopUser user = null;
        //如果是登录状态
        if (session != null) {
            user = (ShopUser) session.getAttribute("ShopUser");
            //从cookie中取购物车列表
            //如果不为空，把cookie中的购物车商品和服务端的购物车商品合并。
            shopCartService.mergeCart(user.getId(), cartList);
            //把cookie中的购物车删除
            CookieUtils.deleteCookie(request, response, "shopCart");
            //从服务端取购物车列表
            cartList = shopCartService.getCartList(user.getId());
        }




        return Result.ok(cartList);
    }

    /**
     * Description: 更新购物车数量
     *
     * @author: sun
     * @Date 上午11:55 2019/5/7
     * @param:
     * @return:
     */
//    @SystemControllerLog(description = "更新购物车数量")
    @RequestMapping("/updateCart")
    @PermissionMode(PermissionMode.Mode.White)
    public Object updateCartNum(@RequestBody(required = false) Map<String, Object> param
            , HttpServletRequest request, HttpServletResponse response) {
        //判断用户是否为登录状态
        Session session = SessionUtils.get(request);
        ShopUser user = null;
        if (session != null) {
            user = (ShopUser) session.getAttribute("ShopUser");
            shopCartService.updateCartNum(user.getId(), (String) param.get("itemId"), (Integer) param.get("num"));
            return Result.ok();
        }
        //从cookie中取购物车列表
        List<ShopGoodsInfo> cartList = getCartListFromCookie(request);
        //遍历商品列表找到对应的商品
        for (ShopGoodsInfo tbItem : cartList) {
            if (tbItem.getId().equals((String) param.get("itemId"))) {
                //更新数量
                tbItem.setAmount((Integer) param.get("num"));
                break;
            }
        }
        //把购物车列表写回cookie
        CookieUtils.setCookie(request, response, "shopCart", JsonUtils.objectToJson(cartList), COOKIE_CART_EXPIRE, true);
        //返回成功

        return Result.ok();
    }


    /**
     * Description: 删除购物车
     *
     * @author: sun
     * @Date 上午11:52 2019/5/7
     * @param:
     * @return:
     */
//    @SystemControllerLog(description = "删除购物车商品")
    @RequestMapping("/deleteCart")
    @PermissionMode(PermissionMode.Mode.White)
    public Object deleteCartItem(@RequestBody(required = false) Map<String, Object> param, HttpServletRequest request,
                                 HttpServletResponse response) {
        //判断用户是否为登录状态
        Session session = SessionUtils.get(request);
        ShopUser user = null;
        if (session != null) {
            user = (ShopUser) session.getAttribute("ShopUser");
            shopCartService.deleteCartItem(user.getId(), (String) param.get("itemId"));
            return Result.ok();
        }
        //从cookie中取购物车列表
        List<ShopGoodsInfo> cartList = getCartListFromCookie(request);
        //遍历列表，找到要删除的商品
        Iterator<ShopGoodsInfo>  infoIterator = cartList.iterator();
        String[] ids  = ((String) param.get("itemId")).split(",");
         while(infoIterator.hasNext()){
             ShopGoodsInfo next = infoIterator.next();
             for(String itemId : ids){
             if (next.getId().equals(itemId)){
                 //删除商品
                 infoIterator.remove();
             }
             }
         }
        //把购物车列表写入cookie
        CookieUtils.setCookie(request, response, "shopCart", JsonUtils.objectToJson(cartList), COOKIE_CART_EXPIRE, true);
        return Result.ok();
    }


    /**
     * Description:清空购物车
     *
     * @author: sun
     * @Date 上午11:52 2019/5/7
     * @param:
     * @return:
     */
    @RequestMapping("/clear")
    @PermissionMode(PermissionMode.Mode.White)
    public Object clearCartItem(HttpServletRequest request,
                                HttpServletResponse response) {
        Session session = SessionUtils.get(request);
        ShopUser user = null;
        if (session != null) {
            user = (ShopUser) session.getAttribute("ShopUser");
            shopCartService.clearCartItem(user.getId());
            return Result.ok();
        }
        CookieUtils.deleteCookie(request, response, "shopCart");

        return Result.ok();
    }








}
