package com.zhiwee.gree.webapi.Controller.fyGoods;

import com.zhiwee.gree.model.FyGoods.FyOrder;
import com.zhiwee.gree.model.FyGoods.FyOrderItem;
import com.zhiwee.gree.model.FyGoods.GrouponGoods;
import com.zhiwee.gree.model.FyGoods.LShopUserGroup;
import com.zhiwee.gree.model.GoodsInfo.Goods;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.service.FyGoods.FyOrderItemService;
import com.zhiwee.gree.service.FyGoods.FyOrderService;
import com.zhiwee.gree.service.FyGoods.GrouponGoodsService;
import com.zhiwee.gree.service.FyGoods.LShopUserGroupService;
import com.zhiwee.gree.service.ShopUser.ShopUserService;
import com.zhiwee.gree.webapi.Controller.Wxpay.FyWxPay;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.util.KeyUtils;
import xyz.icrab.common.web.annotation.*;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;

/**
 * @author DELL
 */
@RestController
@RequestMapping("/lShopUserGroup")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class lShopUserGroupController {
    @Resource
    private LShopUserGroupService lShopUserGroupService;
    @Resource
    private GrouponGoodsService grouponGoodsService;
    @Resource
    private ShopUserService shopUserService;
    @Resource
    private FyOrderItemService fyOrderItemService;
    @Resource
    private FyOrderService fyOrderService;


    /*
    正常 未开始 以结束
    */
    private static final int NORMALNUM = 0;
    private static final int BEFORENUM = 1;
    private static final int OVERNUM = 2;

    /**
     * @author tzj
     * @description 开团
     * @date 2020/4/26 13:20
     */
    @RequestMapping("/startGroup")
    public Result<?> startGroup(@Session("ShopUser") ShopUser shopUser, @RequestBody GrouponGoods grouponGoods) {
        //判断用户信息是否完善
        Boolean flg = checkUser(shopUser);
        if (StringUtils.isEmpty(grouponGoods.getOrderNum())){
            return Result.of(Status.ClientError.BAD_REQUEST, "拼团失败请先付款");
        }
        String orderNum = grouponGoods.getOrderNum();
        if (flg) {
            return Result.of(Status.ClientError.BAD_REQUEST, "请完善用户信息");
        }
        //找到拼团信息
        grouponGoods = grouponGoodsService.get(grouponGoods.getId());
        if (grouponGoods == null) {
            return Result.of(Status.ClientError.BAD_REQUEST, "拼团结束请下次再来");
        }
        //校验时间
        int i = checkTime(grouponGoods);
        if (i != NORMALNUM) {
            if (i == BEFORENUM) {
                return Result.of(Status.ClientError.BAD_REQUEST, "拼团未开始");
            }
            if (i == OVERNUM) {
                return Result.of(Status.ClientError.BAD_REQUEST, "拼团已经结束");
            }
        }
        //干掉之前未成功的拼团记录
        deleteFail(shopUser, grouponGoods);
        //创建记录
        Date date = new Date();
        Integer duration = grouponGoods.getDuration();
        long time = date.getTime();
        long endTime = time + duration * 60 * 60 * 1000;
        Date date2 = new Date();
        date2.setTime(endTime);

        String key = KeyUtils.getKey();
        LShopUserGroup lShopUserGroup = new LShopUserGroup();
        lShopUserGroup.setCreateTime(date);
        lShopUserGroup.setEndTime(date2);
        lShopUserGroup.setGoodsId(grouponGoods.getId());
        lShopUserGroup.setGoodsCover(grouponGoods.getCover());
        lShopUserGroup.setGoodsName(grouponGoods.getGoodsName());
        lShopUserGroup.setPrice(grouponGoods.getPrice());
        lShopUserGroup.setGroupPrice(grouponGoods.getGroupPrice());
        lShopUserGroup.setHeader(1);
        lShopUserGroup.setId(key);
        lShopUserGroup.setOrderNum(orderNum);
        lShopUserGroup.setKeyWord(key);
        lShopUserGroup.setUserId(shopUser.getId());
        lShopUserGroup.setUserName(shopUser.getNickname());
        lShopUserGroup.setUserPhone(shopUser.getPhone());
        lShopUserGroup.setState(0);
        lShopUserGroupService.save(lShopUserGroup);
        return Result.ok(lShopUserGroup);
    }


    /**
     * @author tzj
     * @description 参团
     * @date 2020/4/26 13:20
     */
    @RequestMapping("/inGroup")
    public Result<?> inGroup(@Session("ShopUser") ShopUser shopUser, @RequestBody LShopUserGroup lShopUserGroup) {
        if (StringUtils.isEmpty(lShopUserGroup.getOrderNum())){
            return Result.of(Status.ClientError.BAD_REQUEST, "拼团失败请先付款");
        }
        String orderNum = lShopUserGroup.getOrderNum();
        lShopUserGroup = lShopUserGroupService.get(lShopUserGroup.getId());
        if (lShopUserGroup == null || lShopUserGroup.getState() == 1) {
                //退款
                wxPayRefund(orderNum);
            return Result.of(Status.ClientError.BAD_REQUEST, "该团已经取消请等待退款");
        }
        //判断用户信息是否完善
        Boolean flg = checkUser(shopUser);
        if (flg) {
            return Result.of(Status.ClientError.BAD_REQUEST, "请完善用户信息");
        }

        Boolean overTime = checkOvertime(lShopUserGroup);
        if (overTime) {
            //清理
            lShopUserGroupService.delete(lShopUserGroup);
            return Result.of(Status.ClientError.BAD_REQUEST, "拼团结束请下次再来");
        }

        //找到拼团商品
        GrouponGoods grouponGoods = getGroupGoods(lShopUserGroup);
        if (grouponGoods == null) {
            return Result.of(Status.ClientError.BAD_REQUEST, "拼团结束请下次再来");
        }
        //校验时间
        int i = checkTime(grouponGoods);
        if (i != NORMALNUM) {
            if (i == BEFORENUM) {
                return Result.of(Status.ClientError.BAD_REQUEST, "拼团未开始");
            }
            if (i == OVERNUM) {
                return Result.of(Status.ClientError.BAD_REQUEST, "拼团已经结束");
            }
        }
        //查看此人是否已经入团
        Boolean hasIn = checkGroup(shopUser, grouponGoods, lShopUserGroup.getKeyWord());
        if (hasIn) {
            return Result.of(Status.ClientError.BAD_REQUEST, "您已经加入了该团");
        }
        //干掉之前未成功的拼团记录
        deleteFail(shopUser, grouponGoods);
        //创建记录
        String key = KeyUtils.getKey();
        LShopUserGroup newOne = new LShopUserGroup();
        newOne.setCreateTime(new Date());
        newOne.setGoodsId(grouponGoods.getGoodsId());
        newOne.setHeader(2);
        newOne.setId(key);
        newOne.setKeyWord(lShopUserGroup.getKeyWord());
        newOne.setGoodsCover(grouponGoods.getCover());
        newOne.setGoodsName(grouponGoods.getGoodsName());
        newOne.setPrice(grouponGoods.getPrice());
        newOne.setGroupPrice(grouponGoods.getGroupPrice());
        newOne.setUserId(shopUser.getId());
        newOne.setUserName(shopUser.getNickname());
        newOne.setUserPhone(shopUser.getPhone());
        newOne.setState(0);
        newOne.setOrderNum(orderNum);
        lShopUserGroupService.save(newOne);
        //校验拼团是否成功
        checkSuccess(grouponGoods, lShopUserGroup.getKeyWord());
        return Result.ok(newOne);
    }

    private void wxPayRefund(String orderNum) {
        FyOrderItem fyOrderItem = new FyOrderItem();
        fyOrderItem.setOrderNum(orderNum);
        fyOrderItem = fyOrderItemService.getOne(fyOrderItem);
        if (fyOrderItem != null) {
            wxPayRefund2(fyOrderItem);
        }


    }

    /**
     * @author tzj
     * @description 此团是否超市
     * @date 2020-05-09 9:22
     */
    private Boolean checkOvertime(LShopUserGroup lShopUserGroup) {
        lShopUserGroup = lShopUserGroupService.get(lShopUserGroup.getId());
        if (lShopUserGroup == null) {
            return true;
        } else {
            return lShopUserGroup.getEndTime().before(new Date());
        }
    }


    /**
     * @author tzj
     * @description 拼团是否成功
     * @date 2020/4/26 15:08
     */
    private void checkSuccess(GrouponGoods grouponGoods, String keyWord) {
        Map<String, Object> param = new HashMap<>(5);
        param.put("keyWord", keyWord);
        List<LShopUserGroup> list = lShopUserGroupService.list(param);
        //如果成功
        if (list.size() >= grouponGoods.getLimitCount()) {
            for (LShopUserGroup lShopUserGroup : list) {
                lShopUserGroup.setState(1);
                lShopUserGroupService.update(lShopUserGroup);
            }
        }
    }

    /**
     * @author tzj
     * @description 校验是否入团
     * @date 2020/4/26 14:49
     */
    private Boolean checkGroup(ShopUser shopUser, GrouponGoods grouponGoods, String key) {
        LShopUserGroup lShopUserGroup = new LShopUserGroup();
        lShopUserGroup.setKeyWord(key);
        lShopUserGroup.setUserId(shopUser.getId());
        lShopUserGroup.setGoodsId(grouponGoods.getGoodsId());
        return lShopUserGroupService.getOne(lShopUserGroup) != null;
    }

    /**
     * @author tzj
     * @description 找到记录
     * @date 2020/4/26 14:47
     */
    private GrouponGoods getGroupGoods(LShopUserGroup lShopUserGroup) {
        GrouponGoods grouponGoods = new GrouponGoods();
        grouponGoods.setId(lShopUserGroup.getGoodsId());
        grouponGoods = grouponGoodsService.getOne(grouponGoods);
        return grouponGoods;
    }


    /**
     * @author tzj
     * @description 未成功的拼团记录清楚
     * @date 2020/4/26 14:18
     */
    private void deleteFail(ShopUser shopUser, GrouponGoods grouponGoods) {
        Map<String, Object> param = new HashMap<>(5);
        param.put("userId", shopUser.getId());
        param.put("goodsId", grouponGoods.getGoodsId());
        param.put("state", 1);
        List<LShopUserGroup> list = lShopUserGroupService.list(param);
        for (LShopUserGroup lShopUserGroup : list) {
            lShopUserGroupService.delete(lShopUserGroup);
        }
    }

    /**
     * @author tzj
     * @description 时间校验 1未开始 2 以结束 0正常
     * @date 2020/4/26 14:00
     */
    private int checkTime(GrouponGoods grouponGoods) {
        Date date = new Date();
        if (date.before(grouponGoods.getStartTime())) {
            return BEFORENUM;
        }
        if (date.after(grouponGoods.getEndTime())) {
            return OVERNUM;
        }
        return NORMALNUM;
    }

    /**
     * @author tzj
     * @description 校验用户信息
     * @date 2020/4/26 13:54
     */
    private Boolean checkUser(ShopUser shopUser) {
        shopUser = shopUserService.get(shopUser.getId());
        return StringUtils.isEmpty(shopUser.getPhone());
    }

    /**
     * @author tzj
     * @description 开团记录
     * @date 2020-05-09 11:22
     */
    @RequestMapping("/list")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> list(@RequestBody Map<String, Object> param) {
        String goodsId = (String) param.get("goodsId");
        if (StringUtils.isEmpty(goodsId)) {
            return Result.of(Status.ClientError.BAD_REQUEST, "缺少参数，请重试");
        }
        List<LShopUserGroup> lShopUserGroups = new ArrayList<>();
        Date date = new Date();
        param.put("dateTime", date);
        lShopUserGroups = lShopUserGroupService.listAll(param);
        return Result.ok(lShopUserGroups);
    }



    /**
     * @author tzj
     * @description 开始退款
     * @date 2020-05-12 17:04
    */
    public Result<?> wxPayRefund2(FyOrderItem fyOrderItem) {
        //非空判断
        if (null != fyOrderItem) {
            //查询订单表，
            Map<String, Object> map = new HashMap<>();
            map.put("orderNum", fyOrderItem.getOrderNum());
            FyOrder getOrderInfo = fyOrderService.getOne(map);
            BigDecimal payPrice = getOrderInfo.getPayPrice().multiply(new BigDecimal(100));
            //支付金额减去已经退款的金额
            BigDecimal total_fee = payPrice.stripTrailingZeros();
            if (getOrderInfo.getState() ==1) {
                return Result.of(Status.ClientError.BAD_REQUEST, "只有支付的订单才可以退款");
            }
            if (total_fee.compareTo(fyOrderItem.getPayPrice()) < 0) {
                return Result.of(Status.ClientError.BAD_REQUEST, "退款金额异常");
            }
            getOrderInfo.setRefoundPrice(fyOrderItem.getPayPrice());
            //调用微信退款
            Result<?> result = FyWxPay.wxPayRefund(getOrderInfo,fyOrderItem);

            //如果退款成功，该改变订单状态
//            System.out.println(result.getCode());
            if ("200".equals(result.getCode())) {
                //退款逻辑
                returnOrder(fyOrderItem);
                return Result.ok("退款成功");
            }
        }
        return Result.of(Status.ClientError.BAD_REQUEST, "退款失败");
    }

    /**
     * 退款逻辑
     */
    public void returnOrder(FyOrderItem fyOrderItem) {
        //改变商品基本表信息
        Map<String, Object> param = new HashMap<>();
        param.put("orderNum", fyOrderItem.getOrderNum());
        //根据订单号查询基本订单信息
        FyOrder fyOrder = fyOrderService.getOne(param);
        //退款金额更新
        fyOrder.setRefoundPrice(fyOrder.getRefoundPrice().add(fyOrderItem.getPayPrice()));
        //1表示包含退款订单
        fyOrder.setIshasRefound(2);
        if ((fyOrder.getRefoundPrice().compareTo(fyOrder.getPayPrice())) == 0) {
            //4表示表示全额退款
            fyOrder.setState(4);
        } else {
            //3表示部分退款
            fyOrder.setState(3);
        }
        //退单时间
        fyOrder.setRefoundTime(new Date());
        fyOrderService.update(fyOrder);
        //更新订单基本表信息
        //已确认退款
        fyOrderItem.setRefoundState(2);
        fyOrderItem.setOperatorName("拼团失败");
        fyOrderItemService.update(fyOrderItem);
        //改变商品表的的库存
        GrouponGoods goods = grouponGoodsService.get(fyOrderItem.getGoodsId());
        if (null != goods) {
            //原来库存加上退款商品的库存
            goods.setStoreCount(goods.getStoreCount() + fyOrderItem.getCount());
            grouponGoodsService.update(goods);
        }
    }
}
