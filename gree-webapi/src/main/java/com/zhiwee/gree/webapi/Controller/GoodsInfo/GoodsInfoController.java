package com.zhiwee.gree.webapi.Controller.GoodsInfo;

import com.zhiwee.gree.model.GoodsInfo.*;


import com.zhiwee.gree.service.GoodsInfo.GoodsInfoService;
import com.zhiwee.gree.service.GoodsInfo.ShopGoodsInfoService;
import com.zhiwee.gree.webapi.util.SystemControllerLog;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.*;

import org.springframework.web.multipart.MultipartFile;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.util.IdGenerator;
import xyz.icrab.common.web.annotation.*;
import xyz.icrab.common.web.param.IdsParam;
import xyz.icrab.common.web.util.RequestUtils;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * @author sun on 2019/4/30
 */
@RestController
@RequestMapping("/goodsInfo")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class GoodsInfoController {



    @Autowired
    private GoodsInfoService goodsInfoService;



    @Autowired
    private ShopGoodsInfoService shopGoodsInfoService;






    /**
     * Description:  查询所有商品
     * @author: sun
     * @Date 下午10:40 2019/3/27
     * @param:
     * @return:
     */
    @RequestMapping(value = "searchAll")
    public Result<?>  searchAll(@RequestBody(required = false) Map<String, Object> params,Pagination pagination) {
        Pageable<GoodsInfo> GoodsInfoList = goodsInfoService.searchAll(params, pagination);
        return Result.ok(GoodsInfoList);
    }





}
