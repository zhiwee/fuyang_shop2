package com.zhiwee.gree.webapi.Controller.Alipay;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.internal.util.AlipaySignature;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.alipay.api.request.AlipayTradeRefundRequest;
import com.alipay.api.response.AlipayTradeRefundResponse;
import com.google.gson.Gson;
import com.zhiwee.gree.model.Alipay.AliRefund;
import com.zhiwee.gree.model.Alipay.AlipayVo;
import com.zhiwee.gree.model.BaseInfo.BaseInfo;
import com.zhiwee.gree.model.OrderInfo.Order;
import com.zhiwee.gree.service.BaseInfo.BaseInfoService;
import com.zhiwee.gree.service.OrderInfo.OrderService;
import com.zhiwee.gree.webapi.util.PayForUtil;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.web.annotation.PermissionMode;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;


import com.alibaba.fastjson.JSON;
/**
 * @author sun on 2019/5/12
 */
@RequestMapping("/alipay")
@Controller
public class AlipayController {
    private  static  final Logger logger = LoggerFactory.getLogger(AlipayController. class);
    @Value("${ALIPAY.APPID}")
    private String app_id;
    @Value("${ALIPAY.PRIVATEKEY}")
    private String merchant_private_key;
    @Value("${ALIPAY.PUBLICKEY}")
    private String alipay_public_key;
    @Value("${ALIPAY.NOTIFY_URL}")
    private String notify_url;
    @Value("${ALIPAY.RETURNA_URL}")
    private String return_url;
    @Value("${ALIPAY.SIGN}")
    private String sign_type;
    private String charset = "utf-8";

    @Value("${ALIPAY.SERVER}")
    private String gatewayUrL;

    @Autowired
    private OrderService orderService;




    @Autowired
    private BaseInfoService baseInfoService;

    /**
 * Description: 支付宝支付
 * @author: sun
 * @Date 下午6:22 2019/5/19
 * @param:
 * @return:
 */
    @RequestMapping("/pay")
    @ResponseBody
    @PermissionMode(PermissionMode.Mode.White)
    private String  alipayPay(String out_trade_no, HttpServletRequest req , HttpServletResponse res) throws AlipayApiException, IOException {
        //这个应该是从前端端传过来的，这里为了测试就从后台写死了
        AlipayVo vo = new AlipayVo();
        Order order = orderService.get(out_trade_no);
        //工程订单1 未审核 2 审核通过  3 审核不通过
//        if(order.getIsOffline() == 4 && order.getPermitPay() == 2){
//
//        }
        vo.setTotal_amount(order.getRealPrice().toString());
        vo.setOut_trade_no(order.getId());
        vo.setSubject("nelson-test-title");
        vo.setProduct_code("FAST_INSTANT_TRADE_PAY"); //这个是固定的
        String json = new Gson().toJson(vo);
        System.out.println(json + "孙登凯无敌哈哈哈哈哈哈哈哈");
        AlipayClient alipayClient = new DefaultAlipayClient(gatewayUrL, app_id, merchant_private_key, "json", charset, alipay_public_key, sign_type);
        // 设置请求参数
        AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();
//        alipayRequest.setReturnUrl(return_url);
        alipayRequest.setNotifyUrl(notify_url);
        alipayRequest.setBizContent(json);
        String result = alipayClient.pageExecute(alipayRequest).getBody();
        System.out.println(result);
//        PrintWriter writer = res.getWriter();
//        writer.write(result);
//        writer.flush();
//        writer.close();
        return result; //这里生成一个表单，会自动提交
    }


    /**
     * @param request
     * @param out_trade_no 商户订单号
     * @param trade_no     支付宝交易凭证号
     * @param trade_status 交易状态
     * @return String
     * @throws AlipayApiException
     * @throws
     * @Title: alipayNotify
     * @Description: 支付宝回调接口
     * @author SUN
     */
    @PostMapping("/notify")
    @PermissionMode(PermissionMode.Mode.White)
    private String  alipayNotify(HttpServletRequest request, String out_trade_no, String trade_no, String trade_status)
            throws AlipayApiException {
        Order order = orderService.get(out_trade_no);
        Map<String, String> map = new HashMap<String, String>();
        Map<String, String[]> requestParams = request.getParameterMap();
        for (Iterator<String> iter = requestParams.keySet().iterator(); iter.hasNext(); ) {
            String name = iter.next();
            String[] values = requestParams.get(name);
            String valueStr = "";
            for (int i = 0; i < values.length; i++) {
                valueStr = (i == values.length - 1) ? valueStr + values[i] : valueStr + values[i] + ",";
                System.out.println(valueStr + "孙登凯无敌" + i);
            }
            map.put(name, valueStr);
        }
        boolean signVerified = false;
        try {
            signVerified = AlipaySignature.rsaCheckV1(map, alipay_public_key, charset, sign_type);
        } catch (AlipayApiException e) {
            e.printStackTrace();
            return "redirect:http://192.168.0.244:8083/ah-webshop/pages/order/order_list.html";
//            return ("fail");// 验签发生异常,则直接返回失败
        }
        if (signVerified) {
            order.setOrderState(2);
            order.setPayType(1);
            order.setPayTime(new Date());
//            orderService.update(order);
            return "redirect:http://192.168.0.244:8083/ah-webshop/pages/order/order_list.html";
//
//            return ("success");
        } else {
            System.out.println("验证失败,不去更新状态");
            return "redirect:http://192.168.0.244:8083/ah-webshop/pages/order/order_list.html";
//            return ("fail");
        }
    }


    /**
     * @param request
     * @param out_trade_no 商户订单号
     * @param trade_no     支付宝交易凭证号
     * @param trade_status 交易状态
     * @return String
     * @throws AlipayApiException
     * @throws
     * @Title: alipayReturn
     * @Description: 支付宝回调接口
     * @author SUN
     */
//    @GetMapping("/return")
//    @PermissionMode(PermissionMode.Mode.White)
//    private String alipayReturn(Map<String, String> params, HttpServletRequest request, String out_trade_no, String trade_no, String total_amount)
//            throws AlipayApiException {
//        Order order = orderService.get(out_trade_no);
//        Map<String, String> map = new HashMap<String, String>();
//        Map<String, String[]> requestParams = request.getParameterMap();
//        for (Iterator<String> iter = requestParams.keySet().iterator(); iter.hasNext(); ) {
//            String name = iter.next();
//            String[] values = requestParams.get(name);
//            String valueStr = "";
//            for (int i = 0; i < values.length; i++) {
//                valueStr = (i == values.length - 1) ? valueStr + values[i] : valueStr + values[i] + ",";
//                System.out.println(valueStr + "唐作家无敌" +i);
//            }
//            map.put(name, valueStr);
//        }
//        boolean signVerified = false;
//        try {
//            //验证过程
//            signVerified = AlipaySignature.rsaCheckV1(map, alipay_public_key, charset, sign_type);
//        } catch (AlipayApiException e) {
//            e.printStackTrace();
//            return ("fail");// 验签发生异常,则直接返回失败
//        }
//        if (signVerified) {
//            order.setOrderState(2);
//            orderService.update(order);
//            return ("success");
//        } else {
//            System.out.println("验证失败,不去更新状态");
//            return ("fail");
//        }
//    }

/**
 * Description: 支付宝退款
 * @author: sun
 * @Date 下午3:52 2019/5/13
 * @param: out_trade_no 保持唯一 退款金额不能大于订单金额
 * setOut_request_no 部分退需要传入的参数
 * @return:
 */
    @RequestMapping(value = "/orderBackMoney")
    @ResponseBody
    public Result<?> orderBackMoney(@RequestBody AliRefund aliRefund) {
        List<BaseInfo> baseInfos  =  baseInfoService.list();
        if(!baseInfos.get(0).getRefundPassword().equals(aliRefund.getRefundPassword())){
            return Result.of(Status.ClientError.BAD_REQUEST, "退款密码不正确");
        }
        AliRefund refund = new AliRefund();
        refund.setOut_request_no(PayForUtil.buildRandom(4) + "");
        Order order = orderService.get(aliRefund.getOut_trade_no());
        if(order==null){
            Result.of(Status.ClientError.BAD_REQUEST, "该订单不存在");
        }
        if (order.getOrderState() != 2 || order.getPayType() != 1 || (order.getRefundState() != 2 && order.getRefundState() != 8)) {
            return Result.of(Status.ClientError.BAD_REQUEST, "该订单未付款或者该订单不是支付宝支付或者该订单没有申请无法退款");
        }
        Double refundMoney = Double.valueOf(aliRefund.getRefund_amount());
        if(refundMoney > order.getRealPrice()){
            return Result.of(Status.ClientError.BAD_REQUEST, "退款金额超过订单的金额");
        }
        if(refundMoney <= 0){
            return Result.of(Status.ClientError.BAD_REQUEST, "退款金额不能小于等于0");
        }
        refund.setOut_trade_no(order.getId());
        refund.setRefund_amount(refundMoney.toString());
        refund.setRefund_reason("正常退款");
        AlipayClient alipayClient = new DefaultAlipayClient(gatewayUrL, app_id, merchant_private_key, "json", charset, alipay_public_key, sign_type);
        AlipayTradeRefundRequest request = new AlipayTradeRefundRequest();
        String json = new Gson().toJson(refund);
        request.setBizContent(json);
        AlipayTradeRefundResponse response = null;
        try {
            response = alipayClient.execute(request);
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        if (response.isSuccess()) {
            System.out.println(response.getBody());
            Map<String,Object> params = new HashMap<>();
            params.put("out_trade_no",order.getId());
            params.put("refundMoney",refundMoney);
            //业务逻辑处理
//            System.out.println("调用成功");
//            order.setOrderState(4);

            return  Result.ok("退款成功");
        } else {
           return  Result.ok("退款失败");
        }
    }


}


