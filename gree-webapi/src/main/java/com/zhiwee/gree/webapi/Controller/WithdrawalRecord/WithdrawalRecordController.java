package com.zhiwee.gree.webapi.Controller.WithdrawalRecord;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zhiwee.gree.model.BaseInfo.BaseInfo;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.ShopUser.ShopUserExport;
import com.zhiwee.gree.model.WithdrawalRecord.WithdrawalRecord;
import com.zhiwee.gree.model.WxPay.WeChatConfig;
import com.zhiwee.gree.service.BaseInfo.BaseInfoService;
import com.zhiwee.gree.service.ShopUser.ShopUserService;
import com.zhiwee.gree.service.WithdrawalRecord.WithdrawalRecordService;
import com.zhiwee.gree.util.JudgeRequestDeviceUtil;
import com.zhiwee.gree.webapi.util.ClientCustomSSL;
import com.zhiwee.gree.webapi.util.HttpUtil;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.jeecgframework.poi.excel.ExcelExportUtil;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.enmus.ExcelType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.util.IdGenerator;
import xyz.icrab.common.util.KeyUtils;
import xyz.icrab.common.web.annotation.*;
import xyz.icrab.common.web.consts.HeaderNames;
import xyz.icrab.common.web.param.IdParam;
import xyz.icrab.common.web.param.IdsParam;
import xyz.icrab.common.web.session.SessionService;
import xyz.icrab.common.web.support.session.DefaultSession;
import xyz.icrab.common.web.util.Validator;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;


/**
 * @author sun on 2019/4/30
 */
@RestController
@RequestMapping("/withdrawalRecord")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class WithdrawalRecordController {

    @Autowired
    private WithdrawalRecordService withdrawalRecordService;


    @Autowired
    private ShopUserService shopUserService;



    @Autowired
    private BaseInfoService baseInfoService;


    /**
     * Description: 查询所有申请记录
     *
     * @author: sun
     * @Date 下午3:15 2019/6/14
     * @param:
     * @return:
     */
    @RequestMapping({"/page"})
    public Result<Pageable<WithdrawalRecord>> page(@RequestBody(required = false) Map<String, Object> param, Pagination pagination) {
        return Result.ok(withdrawalRecordService.pageInfo(param, pagination));
    }

    /**
     * Description:  申请提现
     *
     * @author: sun
     * @Date 下午3:35 2019/6/14
     * @param:
     * @return:
     */
    @PermissionMode(PermissionMode.Mode.White)
    @RequestMapping("/applyMoney")
    public Result<?> applyMoney(@RequestBody WithdrawalRecord withdrawalRecord, @Session("ShopUser") ShopUser user) {
        List<BaseInfo> baseInfos  =  baseInfoService.list();
        Validator validator = new Validator();
        validator.notNull(withdrawalRecord.getMoney(), "提现金额不能为空");
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }
        if (withdrawalRecord.getMoney() <= 0) {
            return Result.of(Status.ClientError.BAD_REQUEST, "提现金额不能小于等于0");
        }

        if(withdrawalRecord.getMoney() <  Double.valueOf("0.3")){
            return Result.of(Status.ClientError.BAD_REQUEST, "提现金额不能小于3角");
        }
        ShopUser shopUser = shopUserService.get(user.getId());
        if (withdrawalRecord.getMoney() > shopUser.getActivityCost()) {
            return Result.of(Status.ClientError.BAD_REQUEST, "提现金额不能大于账户提成总额");
        }
        withdrawalRecord.setId(IdGenerator.objectId());
        withdrawalRecord.setUserId(user.getId());
        withdrawalRecord.setCreateTime(new Date());
        withdrawalRecord.setState(1);
        withdrawalRecordService.save(withdrawalRecord);
        BaseInfo baseInfo = baseInfos.get(0);

        //假如设置了允许直接体现则直接体现
        if(baseInfo.getIsApply() == 1){
            WithdrawalRecord wr = withdrawalRecordService.get(withdrawalRecord.getId());
            if(wr != null){
                Map params = ClientCustomSSL.withdrawal(wr.getId(),shopUser.getOpenId(),wr.getMoney());
                if(params.get("status").equals("SUCCESS")){
                    shopUser.setActivityCost(BigDecimal.valueOf(shopUser.getActivityCost()).subtract(BigDecimal.valueOf(wr.getMoney())).doubleValue());
//                    shopUserService.update(shopUser);
                    wr.setState(2);
                    withdrawalRecordService.updateWrAndUser(wr,shopUser);
                }else{
                    return Result.of(Status.ClientError.BAD_REQUEST, params.get("msg"));
                }
            }
        }
        return Result.ok();
    }




    /**
     * Description:  获取个人提现记录
     *
     * @author: sun
     * @Date 下午3:35 2019/6/14
     * @param:
     * @return:
     */
    @PermissionMode(PermissionMode.Mode.White)
    @RequestMapping("/getRecord")
    public Result<?> getRecord( @Session("ShopUser") ShopUser user, Pagination pagination) {
        Map<String,Object> params = new HashMap<>();
        params.put("userId",user.getId());
        return Result.ok(withdrawalRecordService.pageInfo(params, pagination));
    }



    /**
     * Description: 同意提现
     *
     * @author: sun
     * @Date 下午9:06 2019/6/14
     * @param:
     * @return:
     */
    @RequestMapping("/open")
    @ResponseBody
    public Result<?> open(@RequestBody @Valid IdsParam map) {
        List<String> ids = map.getIds();
          WithdrawalRecord withdrawalRecord = new WithdrawalRecord();
        for (String id : ids) {
            WithdrawalRecord drawa = withdrawalRecordService.get(id);

            ShopUser shopUser = shopUserService.get(drawa.getUserId());
            if(drawa.getMoney() > shopUser.getActivityCost()){
                return Result.of(Status.ClientError.BAD_REQUEST, "提现金额不能大于提成总额");
            }
            withdrawalRecord.setId(id);
            withdrawalRecord.setState(2);
            Map params = ClientCustomSSL.withdrawal(drawa.getId(),shopUser.getOpenId(),drawa.getMoney());
            if(params.get("status").equals("SUCCESS")){
                shopUser.setActivityCost(BigDecimal.valueOf(shopUser.getActivityCost()).subtract(BigDecimal.valueOf(drawa.getMoney())).doubleValue());
//                shopUserService.update(shopUser);
                withdrawalRecordService.updateWrAndUser(withdrawalRecord,shopUser);
            }else{
                return Result.of(Status.ClientError.BAD_REQUEST, params.get("msg"));
            }

        }

        return Result.ok();
    }

    /**
     * Description: 拒绝提现
     *
     * @author: sun
     * @Date 下午9:07 2019/6/14
     * @param:
     * @return:
     */
    @RequestMapping("/stop")
    @ResponseBody
    public Result<?> stop(@RequestBody @Valid IdsParam map) {
        List<String> ids = map.getIds();
        WithdrawalRecord withdrawalRecord = new WithdrawalRecord();
        for (String id : ids) {
            withdrawalRecord.setId(id);
            withdrawalRecord.setState(3);
            withdrawalRecordService.update(withdrawalRecord);
        }

        return Result.ok();
    }





}
