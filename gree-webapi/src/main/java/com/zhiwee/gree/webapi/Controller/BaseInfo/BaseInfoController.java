package com.zhiwee.gree.webapi.Controller.BaseInfo;

import com.zhiwee.gree.model.BaseInfo.BaseInfo;
import com.zhiwee.gree.model.Coupon.Coupon;
import com.zhiwee.gree.model.Coupon.CouponExport;
import com.zhiwee.gree.model.User;
import com.zhiwee.gree.service.BaseInfo.BaseInfoService;
import com.zhiwee.gree.service.Coupon.CouponService;
import com.zhiwee.gree.service.GoodsInfo.GoodsClassifyService;
import com.zhiwee.gree.util.ExecutorUtils;
import com.zhiwee.gree.util.RedisLock;
import com.zhiwee.gree.webapi.util.SystemControllerLog;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.jeecgframework.poi.excel.ExcelExportUtil;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.enmus.ExcelType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.web.annotation.*;
import xyz.icrab.common.web.param.IdParam;
import xyz.icrab.common.web.util.Validator;
import xyz.icrab.ums.util.consts.SessionKeys;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.io.OutputStream;
import java.util.*;


/**
 * @author sun on 2019/4/30
 */
@RestController
@RequestMapping("/baseInfo")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class BaseInfoController {

    @Autowired
    private BaseInfoService baseInfoService;


    /**
     * Description: 查询基础信息
     * @author: sun
     * @Date 上午10:32 2019/4/29
     * @param:
     * @return:
     */
    @RequestMapping({"/page"})
    public Result<Pageable<BaseInfo>> page(@RequestBody(required = false) Map<String, Object> param, Pagination pagination) {
        return Result.ok(baseInfoService.pageInfo(param, pagination));
    }

    /**
     * Description: 更新基础信息
     * @author: sun
     * @Date 下午9:06 2019/5/8
     * @param:
     * @return:
     */
    @RequestMapping("/updateBaseInfo")
    @ResponseBody
    public  Result<?> updateCoupon(@RequestBody BaseInfo baseInfo) {
        Validator validator = new Validator();
        validator.notNull(baseInfo.getRefundPassword(), "退款密码不能为空");
        validator.notNull(baseInfo.getSendIntegral(), "注册赠送积分不能为空");
        validator.notNull(baseInfo.getInitPassword(), "初始化密码不能为空");
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }


        baseInfoService.update(baseInfo);
        return Result.ok(baseInfo);
    }




    /**
     * Description: 基础信息获取
     *
     * @author: sun
     * @Date 下午9:32 2019/5/19
     * @param:
     * @return:
     */
    @RequestMapping("/achieveBaseInfo")
    public Result<?> confirmState(@RequestBody @Valid IdParam map) {
       String ids = map.getId();
        BaseInfo baseInfo = baseInfoService.get(ids);
        return Result.ok(baseInfo);
    }



    /**
     * Description: 获取特价时间
     *
     * @author: sun
     * @Date 下午9:32 2019/5/19
     * @param:
     * @return:
     */
    @PermissionMode(PermissionMode.Mode.White)
    @RequestMapping("/achieveSpecialTime")
    public Result<?> achieveSpecialTime() {
         List<BaseInfo> baseInfos = baseInfoService.list();
        return Result.ok(baseInfos.get(0));
    }








/**
 * Description: 开启直接申请
 * @author: sun
 * @Date 上午9:39 2019/7/15
 * @param:
 * @return:
 */
    @RequestMapping("/open")
    @ResponseBody
    public Result<?> open(@RequestBody @Valid IdParam map) {
        BaseInfo baseInfo = baseInfoService.get(map.getId());
        baseInfo.setIsApply(1);
        baseInfoService.update(baseInfo);
        return Result.ok();
    }

/**
 * Description: 关闭直接申请
 * @author: sun
 * @Date 上午9:39 2019/7/15
 * @param:
 * @return:
 */
    @RequestMapping("/stop")
    @ResponseBody
    public Result<?> stop(@RequestBody @Valid IdParam map) {
        BaseInfo baseInfo = baseInfoService.get(map.getId());
        baseInfo.setIsApply(2);
        baseInfoService.update(baseInfo);
        return Result.ok();
    }

}
