package com.zhiwee.gree.webapi.Controller.shopBase.vo;

import xyz.icrab.common.model.enums.Gender;

/**
 * @author Knight
 * @since 2018/6/7 14:34
 */
public class UserProfile {

    private String name;

    private String avatar;

    private Gender gender;

    private Integer type;

    private String token;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
