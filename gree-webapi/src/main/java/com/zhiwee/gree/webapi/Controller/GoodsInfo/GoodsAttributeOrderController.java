package com.zhiwee.gree.webapi.Controller.GoodsInfo;
import com.zhiwee.gree.model.GoodsInfo.GoodsAttributeOrder;
import com.zhiwee.gree.service.GoodsInfo.GoodsAttributeOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.util.KeyUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequestMapping("/goodsAttributeOrder")
@RestController
public class GoodsAttributeOrderController {

    @Autowired
    private GoodsAttributeOrderService goodsAttributeOrderService;

    /**
     * 增加 周广
     * @param goodsAttributeOrders
     * @return
     */
    @RequestMapping("/add")
    public Result<?> add(@RequestBody List<GoodsAttributeOrder> goodsAttributeOrders){
        for (GoodsAttributeOrder goodsAttributeOrder:goodsAttributeOrders) {
            goodsAttributeOrder.setId(KeyUtils.getKey());
        }
        HashMap<String,Object> param = new HashMap<>();
        param.put("goodsId",goodsAttributeOrders.get(0).getGoodsId());
        goodsAttributeOrderService.deleteByGoodsId(param);
        goodsAttributeOrderService.save(goodsAttributeOrders);
        return Result.ok();
    }
    /**
     * 根据商品查询   周广
     * @param param
     * @return
     */
    @RequestMapping("/selectOne")
    public Result<?> selectOne(@RequestBody Map<String, Object> param){
        if (param == null) {
            param = new HashMap<>();
        }
        if(param.get("goodsId")==null){
            return Result.of(Status.ClientError.BAD_REQUEST,"请先完成基础信息");
        }
        List<GoodsAttributeOrder> goodsAttributeOrders=goodsAttributeOrderService.list(param);
        return Result.ok(goodsAttributeOrders);
    }
    /**
     * 重置 周广
     * @param param
     * @return
     */
    @RequestMapping("/delete")
    public Result<?> delete(@RequestBody Map<String, Object> param){
        if (param == null) {
            param = new HashMap<>();
        }
        if(param.get("goodsId")==null){
            return Result.of(Status.ClientError.BAD_REQUEST,"请先完成基础信息");
        }
        goodsAttributeOrderService.deleteByGoodsId(param);
        return Result.ok();
    }
}
