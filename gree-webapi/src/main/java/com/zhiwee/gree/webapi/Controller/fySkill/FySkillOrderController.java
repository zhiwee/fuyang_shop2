package com.zhiwee.gree.webapi.Controller.fySkill;

import com.alibaba.fastjson.JSON;
import com.zhiwee.gree.model.FyGoods.*;
import com.zhiwee.gree.model.FyGoods.vo.FyOrderResultVo;
import com.zhiwee.gree.model.FyGoods.vo.FyOrderVo;
import com.zhiwee.gree.model.FyGoods.vo.FygoodsCartVo;
import com.zhiwee.gree.model.GoodsInfo.Goods;
import com.zhiwee.gree.model.GoodsInfo.GoodsBase;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.User;
import com.zhiwee.gree.model.card.GiftCardUser;
import com.zhiwee.gree.service.Coupon.CouponService;
import com.zhiwee.gree.service.FyGoods.*;
import com.zhiwee.gree.service.GoodsInfo.CategorysService;
import com.zhiwee.gree.service.GoodsInfo.GoodsBaseService;
import com.zhiwee.gree.service.GoodsInfo.GoodsDealerService;
import com.zhiwee.gree.service.GoodsInfo.GoodsService;
import com.zhiwee.gree.service.ShopUser.ShopUserService;
import com.zhiwee.gree.service.card.GiftCardUserService;
import com.zhiwee.gree.webapi.Controller.fyGoods.FyOrderController;
import com.zhiwee.gree.webapi.util.JsonUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.jeecgframework.poi.excel.ExcelExportUtil;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessagePostProcessor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.*;
import xyz.icrab.common.util.IdGenerator;
import xyz.icrab.common.util.KeyUtils;
import xyz.icrab.common.web.annotation.*;
import xyz.icrab.ums.util.consts.SessionKeys;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


@RestController
@RequestMapping("/skillgoods")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class FySkillOrderController {
    @Resource
    private RedisTemplate<String,String > redisTemplate;
    @Resource
    private GiftCardUserService giftCardUserService;
    @Resource
    private  FyOrderService  fyOrderService;
    @Resource
    private FyOrderItemService  fyOrderItemService;
    @Resource
    private GoodsService  fygoodsService;
    @Resource
    private RabbitTemplate   rabbitTemplate;
    @Resource
    private FygoodsService   goodsService;
    @Resource
    private CouponService couponService;

    @Resource
    private BargainGoodsService bargainGoodsService;
    @Resource
    private CategorysService categorysService;
    @Resource
    private GoodsBaseService goodsBaseService;
    @Resource
    private GrouponGoodsService grouponGoodsService;
    @Resource
    private ShareGoodsService shareGoodsService;
    @Resource
    private GoodsDealerService goodsDealerService;
    @Resource
    private ShopUserService shopUserService;

    /**
     * @Author: jick
     * @Date: 2019/12/13 18:10
     * 生成预订单信息
     */
    @RequestMapping("/createOrder")
    @Transactional
    public Result<?> createOrder(HttpServletRequest request, @Session("ShopUser") ShopUser shopUser, @RequestBody FygoodsCartVo fygoodsCartVo) throws Exception{
        shopUser=   shopUserService.get(shopUser.getId());
        //加锁
        Lock  lock = new ReentrantLock();
        lock.lock();
        try {
            HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
            FyOrder  fyOrder =    new FyOrder();
            String key = KeyUtils.getKey();
            Double activityCost = shopUser.getActivityCost();
            if(null ==shopUser.getPhone() || shopUser.getPhone().trim() ==""){
                return   Result.of(Status.ClientError.FORBIDDEN,"请填写手机号等基本信息");
            }
            //总价标识
            BigDecimal totalPrice = new BigDecimal(0);
            BigDecimal discount=new BigDecimal(0);
            BigDecimal hasDisCount=new BigDecimal(0);
            //找到优惠券
            discount = FyOrderController.getBigDecimal(shopUser, fygoodsCartVo, key, discount, couponService);
            boolean discountFlg=false;
            //生成订单详情 将购物车里面的商品的名称  数量   单价格传过来
            List<FygoodsCart>   fygoodsList = fygoodsCartVo.getList();
            //存放订单详情信息
            for (FygoodsCart f : fygoodsList){
                //计算总价
                Goods fygoods=   fygoodsService.get(f.getFyGoodsId());
                //查看库存，判断该商品的库存是否充足
                if(fygoods != null&&fygoods.getState() != 2){
                    if(fygoods.getStoreCount()==0 || fygoods.getStoreCount()<f.getCount() ){
                        return  Result.of(Status.ClientError.BAD_REQUEST,"库存不足");
                    }
                }else {
                    return  Result.of(Status.ClientError.BAD_REQUEST,"该商品不存在");
                }
                totalPrice=totalPrice.add(fygoods.getShopPrice());
            }

            //如果折扣存在
            if(discount.compareTo(BigDecimal.ZERO) > 0) {
                //如果折扣大于总价
                if (discount.compareTo(totalPrice) >= 0){
                    discount=totalPrice;
                    Result<FyOrder> orderDetail = createOrderDetail(fygoodsCartVo, discount, totalPrice, shopUser);
                    return Result.of(Status.ClientError.GONE, "订单完成");
                }else {
                    hasDisCount=hasDisCount.add(discount);
                    discountFlg=true;
                }
            }
            if (fygoodsCartVo.getCostType() == 1) {
                if(!discountFlg) {
                    int enoughPrice = payByActivityCost(shopUser, totalPrice, fygoodsCartVo, key, hasDisCount);
                    //1 代表佣金足够支付订单
                    if (enoughPrice == 1) {
                        shopUser.setActivityCost(shopUser.getActivityCost() - totalPrice.doubleValue());
                        shopUserService.update(shopUser);
                        return Result.of(Status.ClientError.GONE, "订单完成");
                    } else {
                        //把佣金的钱加入总折扣
                        hasDisCount = hasDisCount.add(new BigDecimal(shopUser.getActivityCost()));
                    }
                }else {
                    hasDisCount= hasDisCount.add(new BigDecimal(activityCost));
                    if(hasDisCount.compareTo(totalPrice)>=0){
                        fygoodsList = avgDiscount(fygoodsList, discount, totalPrice);
                        fygoodsList= avgActivityCost(fygoodsList,totalPrice,totalPrice.subtract(discount));
                        fygoodsCartVo.setList(fygoodsList);
                        createByAVtivityadndiscount(fygoodsCartVo, shopUser, key,totalPrice,discount,shopUser.getActivityCost());
                        double v = shopUser.getActivityCost() - totalPrice.doubleValue();
                        if (v>0) {
                            shopUser.setActivityCost(v);
                        }else {
                            shopUser.setActivityCost(0.0);
                        }
                        shopUserService.update(shopUser);
                        return Result.of(Status.ClientError.GONE, "订单完成");
                    }else {
                        hasDisCount = hasDisCount.add(new BigDecimal(shopUser.getActivityCost()));
                    }
                }
            }

            //如有礼品卡
            if (StringUtils.isNotEmpty(fygoodsCartVo.getGiftCardUser())) {
                GiftCardUser giftCardUser = giftCardUserService.get(fygoodsCartVo.getGiftCardUser());
                if (giftCardUser.getRealityMoney()>0) {
                    //礼品卡的钱加入总折扣
                    if (giftCardUser != null) {
                        hasDisCount = hasDisCount.add(new BigDecimal(giftCardUser.getRealityMoney()));
                    }
                    //如果折扣足够支付
                    if (hasDisCount.compareTo(totalPrice) >= 0) {
                        //判断是否有佣金
                        if (fygoodsCartVo.getCostType() == 1) {
                            Double realityMoney = giftCardUser.getRealityMoney();
                            BigDecimal subtract=new BigDecimal(0);
                            //有使用优惠券
                            if (discountFlg) {
                                subtract  = totalPrice.subtract(discount).subtract(new BigDecimal(shopUser.getActivityCost()));
                                payByCostAndCardAndDiscount(fygoodsCartVo, shopUser, totalPrice, discount);
                            } else {
                                 subtract = totalPrice.subtract(new BigDecimal(shopUser.getActivityCost()));
                                payByCostAndCard(fygoodsCartVo, shopUser, totalPrice);
                            }
                            giftCardUser.setRealityMoney(realityMoney-subtract.doubleValue());
                            giftCardUserService.update(giftCardUser);

                            shopUser.setActivityCost(0.00);
                            shopUserService.update(shopUser);
                            return Result.of(Status.ClientError.GONE, "订单完成");
                        } else {
                            if (discountFlg) {
                                payByCardAndDiscount(fygoodsCartVo, shopUser, totalPrice, discount);
                            } else {
                                payByCard(fygoodsCartVo, shopUser, totalPrice);
                            }
                            giftCardUser.setRealityMoney(giftCardUser.getRealityMoney() - totalPrice.doubleValue());
                            giftCardUserService.update(giftCardUser);
                            return Result.of(Status.ClientError.GONE, "订单完成");
                        }
                    } else {
                        hasDisCount = hasDisCount.add(new BigDecimal(giftCardUser.getRealityMoney()));
                    }
                }
            }

            List<FyOrderItem> fyOrderItemList = new ArrayList<>();
            String goodsInfoStr="";

            if (discount.compareTo(BigDecimal.ZERO) > 0) {
                List<FygoodsCart> fygoodsCarts = avgDiscount(fygoodsCartVo.getList(), discount, totalPrice);
                fygoodsCartVo.setList(fygoodsCarts);
            }
            if (fygoodsCartVo.getCostType() == 1) {
                shopUser.setActivityCost(0.0);
                shopUserService.update(shopUser);

                List<FygoodsCart> fygoodsCarts = avgActivityCost(fygoodsCartVo.getList(), totalPrice, new BigDecimal(shopUser.getActivityCost()));
                fygoodsCartVo.setList(fygoodsCarts);
            }
            if (StringUtils.isNotEmpty(fygoodsCartVo.getGiftCardUser())) {
                GiftCardUser giftCardUser = giftCardUserService.get(fygoodsCartVo.getGiftCardUser());
                if (giftCardUser.getRealityMoney()>0) {
                    fygoodsCartVo = avgGiftCard(fygoodsCartVo, new BigDecimal(giftCardUser.getRealityMoney()));
                }

            }
            BigDecimal subtract = totalPrice.subtract(hasDisCount);
            for (FygoodsCart f : fygoodsCartVo.getList()) {
                Goods fygoods=   fygoodsService.get(f.getFyGoodsId());
                //生成订单信息
                //将商品详情拼接成一个字符串
                goodsInfoStr +=f.getGoodsName()+":"+f.getPrice()+"*"+f.getCount()+",";
                FyOrderItem  fyOrderItem  =  new FyOrderItem();
                fyOrderItem.setId(KeyUtils.getKey());
                fyOrderItem.setType(f.getType());
                fyOrderItem.setSendType(f.getSendType());
                fyOrderItem.setJiadian(fygoods.getJiadian());
                fyOrderItem.setGoodsName(f.getGoodsName());
                fyOrderItem.setActivityCost(f.getPrice());
                fyOrderItem.setGoodsCover(f.getCover());
                fyOrderItem.setGoodsId(f.getFyGoodsId());
                fyOrderItem.setOrderNum(key);
                fyOrderItem.setDealerName(f.getDealerName());
                fyOrderItem.setCreateTime(new Date());
                fyOrderItem.setDealerId(f.getDealerId());
                fyOrderItem.setSinglePrice(f.getPrice());
                fyOrderItem.setDiscount(f.getDiscount());
                fyOrderItem.setGiftPrice(f.getGiftPrice());
                fyOrderItem.setActivityCost(f.getActivityCost());
                fyOrderItem.setCount(f.getCount());
                fyOrderItem.setUserId(shopUser.getId());
                //1表示未删除
                fyOrderItem.setIsdelete(1);
                //是否申请退款  1 表示否
                fyOrderItem.setIsrefound(1);
                //是否退款成功  1 表示否
                fyOrderItem.setRefoundState(1);
                fyOrderItem.setPayPrice(fygoods.getShopPrice().multiply(new BigDecimal(f.getCount())));
                if (f.getActivityCost()!=null&&f.getActivityCost().compareTo(BigDecimal.ZERO)>0) {
                    fyOrderItem.setPayPrice(fyOrderItem.getPayPrice().subtract(f.getActivityCost()));
                }
                if (f.getDiscount()!=null&&f.getDiscount().compareTo(BigDecimal.ZERO)>0) {
                    fyOrderItem.setPayPrice(fyOrderItem.getPayPrice().subtract(f.getDiscount()));
                }
                if (f.getGiftPrice()!=null&&f.getGiftPrice().compareTo(BigDecimal.ZERO)>0) {
                    fyOrderItem.setPayPrice(fyOrderItem.getPayPrice().subtract(f.getGiftPrice()));
                }
                fyOrderItemList.add(fyOrderItem);
                //订单详情存入数据库
                fyOrderItemService.save(fyOrderItem);
                //1表示添加购物车购买  2表示不添加购物车直接购买
                if(1==fygoodsCartVo.getByCar()){
                    //删除购物车里面的数据
                    hashOperations.delete("userCart:"+shopUser.getId(),f.getFyGoodsId());
                }
                //记录订单号，延迟消息队列要用到
                f.setOrderNum(key);
            }
            createOrderNew(fygoodsCartVo, key, shopUser, totalPrice, activityCost, goodsInfoStr,fyOrderItemList,discount,subtract);
            return   Result.ok(fyOrder);
        }finally {
            //释放锁
            lock.unlock();
        }

    }

    private void createOrderNew(FygoodsCartVo fygoodsCartVo, String key, ShopUser shopUser, BigDecimal totalPrice, Double activityCost, String goodsInfoStr, List<FyOrderItem> fyOrderItemList, BigDecimal discount, BigDecimal subtract) {
        FyOrder fyOrder=new FyOrder();
        //实体类封装
        fyOrder.setId(IdGenerator.uuid());
        fyOrder.setOrderNum(key);
        fyOrder.setFyCode(fygoodsCartVo.getFyCode());
        fyOrder.setUserName(shopUser.getNickname());
        fyOrder.setUserId(shopUser.getId());
        fyOrder.setPhoneNum(shopUser.getPhone());
        fyOrder.setAllPrice(totalPrice);
        fyOrder.setDiscount(discount);
        fyOrder.setGiftCardId(fygoodsCartVo.getGiftCardUser());
        //实际支付金额，总金额减去折扣
        fyOrder.setPayPrice(subtract);
        fyOrder.setGoodsBody(goodsInfoStr);
        fyOrder.setOrderTime(new Date());
        //未删除状态
        fyOrder.setIsdelete(1);
        //下单成功，是未支付状态
        fyOrder.setState(1);
        //是否申请退款 1 表示没有  2 表示有
        fyOrder.setIsrefound(1);
        fyOrder.setDeliver(1);
        //退款金额默认为0
        fyOrder.setRefoundPrice(new BigDecimal(0));
        //订单信息存入数据库
        fyOrderService.save(fyOrder);
        //订单信息存一份到redis，支付页面获取数据直接从redis中拿，减轻数据库的压力   订单基本信息+订单详情
        FyOrderResultVo  fyOrderResultVo  = new FyOrderResultVo();
        fyOrderResultVo.setFyOrder(fyOrder);
        fyOrderResultVo.setFyOrderItemList(fyOrderItemList);
        redisTemplate.opsForValue().set("order:"+fyOrder.getOrderNum(), JsonUtil.object2JsonStr(fyOrderResultVo ));
        redisTemplate.expire("order:"+fyOrder.getOrderNum(),120, TimeUnit.SECONDS);
        // sendOrderMessage(fyOrder.getOrderNum());
        //发送信息至延迟 消息队列，主要用来做订单回滚
        sendDelayMessage(fygoodsCartVo.getList());
    }

    private void payByCard(FygoodsCartVo fygoodsCartVo, ShopUser shopUser, BigDecimal totalPrice) {
        HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
        String goodsInfoStr="";
        fygoodsCartVo = avgGiftCard(fygoodsCartVo, totalPrice);

        String key = KeyUtils.getKey();
        Integer flg = 1;
        List<FyOrderItem> fyOrderItemList = new ArrayList<>();
        for (FygoodsCart f : fygoodsCartVo.getList()) {
            Goods fygoods=   fygoodsService.get(f.getFyGoodsId());
            //生成订单信息
            //将商品详情拼接成一个字符串
            goodsInfoStr +=f.getGoodsName()+":"+f.getPrice()+"*"+f.getCount()+",";
            FyOrderItem  fyOrderItem  =  new FyOrderItem();
            fyOrderItem.setId(KeyUtils.getKey());
            fyOrderItem.setType(f.getType());
            fyOrderItem.setSendType(f.getSendType());
            fyOrderItem.setJiadian(fygoods.getJiadian());
            fyOrderItem.setCreateTime(new Date());
            fyOrderItem.setGoodsName(f.getGoodsName());
            fyOrderItem.setActivityCost(f.getActivityCost());
            fyOrderItem.setGoodsCover(f.getCover());
            fyOrderItem.setGoodsId(f.getFyGoodsId());
            fyOrderItem.setOrderNum(key);
            fyOrderItem.setGiftPrice(f.getGiftPrice());
            fyOrderItem.setDealerName(f.getDealerName());
            fyOrderItem.setDealerId(f.getDealerId());
            fyOrderItem.setSinglePrice(f.getPrice());
            fyOrderItem.setDiscount(f.getDiscount());
            fyOrderItem.setCount(f.getCount());
            //1表示未删除
            fyOrderItem.setIsdelete(1);
            //是否申请退款  1 表示否
            fyOrderItem.setIsrefound(1);
            //是否退款成功  1 表示否
            fyOrderItem.setRefoundState(1);
            fyOrderItem.setPayPrice(new BigDecimal(0));
            fyOrderItemList.add(fyOrderItem);
            //订单详情存入数据库
            fyOrderItemService.save(fyOrderItem);
            //1表示添加购物车购买  2表示不添加购物车直接购买
            if(1==fygoodsCartVo.getByCar()){
                //删除购物车里面的数据
                hashOperations.delete("userCart:"+shopUser.getId(),f.getFyGoodsId());
            }
            //记录订单号，延迟消息队列要用到
            f.setOrderNum(key);
        }
        createOrderByAVtivityadndiscountandCard(fygoodsCartVo, key, shopUser, totalPrice, 0.0, goodsInfoStr,fyOrderItemList,new BigDecimal(0));
    }

    private void payByCardAndDiscount(FygoodsCartVo fygoodsCartVo, ShopUser shopUser, BigDecimal totalPrice, BigDecimal discount) {
        HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
        String key = KeyUtils.getKey();
        String goodsInfoStr="";
        List<FygoodsCart> fygoodsCarts = avgActivityCost(fygoodsCartVo.getList(), totalPrice, new BigDecimal(shopUser.getActivityCost()));
        fygoodsCartVo.setList(fygoodsCarts);
        BigDecimal subtract = totalPrice.subtract(discount);
        fygoodsCartVo = avgGiftCard(fygoodsCartVo, subtract);
        List<FygoodsCart> fygoodsCarts1 = avgDiscount(fygoodsCartVo.getList(), discount, totalPrice);
        List<FyOrderItem> fyOrderItemList = new ArrayList<>();
        for (FygoodsCart f : fygoodsCarts1) {
            Goods fygoods=   fygoodsService.get(f.getFyGoodsId());
            //生成订单信息
            //将商品详情拼接成一个字符串
            goodsInfoStr +=f.getGoodsName()+":"+f.getPrice()+"*"+f.getCount()+",";
            FyOrderItem  fyOrderItem  =  new FyOrderItem();
            fyOrderItem.setId(KeyUtils.getKey());
            fyOrderItem.setType(f.getType());
            fyOrderItem.setSendType(f.getSendType());
            fyOrderItem.setJiadian(fygoods.getJiadian());
            fyOrderItem.setGoodsName(f.getGoodsName());
            fyOrderItem.setActivityCost(f.getPrice());
            fyOrderItem.setGoodsCover(f.getCover());
            fyOrderItem.setGoodsId(f.getFyGoodsId());
            fyOrderItem.setOrderNum(key);
            fyOrderItem.setDealerName(f.getDealerName());
            fyOrderItem.setDealerId(f.getDealerId());
            fyOrderItem.setSinglePrice(f.getPrice());
            fyOrderItem.setDiscount(f.getDiscount());
            fyOrderItem.setCount(f.getCount());
            //1表示未删除
            fyOrderItem.setIsdelete(1);
            //是否申请退款  1 表示否
            fyOrderItem.setIsrefound(1);
            //是否退款成功  1 表示否
            fyOrderItem.setRefoundState(1);
            fyOrderItem.setPayPrice(new BigDecimal(0));
            fyOrderItemList.add(fyOrderItem);
            //订单详情存入数据库
            fyOrderItemService.save(fyOrderItem);
            //1表示添加购物车购买  2表示不添加购物车直接购买
            if(1==fygoodsCartVo.getByCar()){
                //删除购物车里面的数据
                hashOperations.delete("userCart:"+shopUser.getId(),f.getFyGoodsId());
            }
            //记录订单号，延迟消息队列要用到
            f.setOrderNum(key);
        }
        createOrderByAVtivityadndiscountandCard(fygoodsCartVo, key, shopUser, totalPrice, 0.0, goodsInfoStr,fyOrderItemList,discount);




    }

    private void payByCostAndCard(FygoodsCartVo fygoodsCartVo, ShopUser shopUser, BigDecimal totalPrice) {
        HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
        String key = KeyUtils.getKey();
        String goodsInfoStr="";
        List<FygoodsCart> fygoodsCarts = avgActivityCost(fygoodsCartVo.getList(), totalPrice, new BigDecimal(shopUser.getActivityCost()));
        fygoodsCartVo.setList(fygoodsCarts);
        BigDecimal subtract = totalPrice.subtract(new BigDecimal(shopUser.getActivityCost()));
        fygoodsCartVo = avgGiftCard(fygoodsCartVo, subtract);
        Integer flg = 1;
        List<FyOrderItem> fyOrderItemList = new ArrayList<>();
        for (FygoodsCart f : fygoodsCartVo.getList()) {
            Goods fygoods=   fygoodsService.get(f.getFyGoodsId());
            //生成订单信息
            //将商品详情拼接成一个字符串
            goodsInfoStr +=f.getGoodsName()+":"+f.getPrice()+"*"+f.getCount()+",";
            FyOrderItem  fyOrderItem  =  new FyOrderItem();
            fyOrderItem.setId(KeyUtils.getKey());
            fyOrderItem.setType(f.getType());
            fyOrderItem.setSendType(f.getSendType());
            fyOrderItem.setJiadian(fygoods.getJiadian());
            fyOrderItem.setGoodsName(f.getGoodsName());
            fyOrderItem.setActivityCost(f.getActivityCost());
            fyOrderItem.setGoodsCover(f.getCover());
            fyOrderItem.setUserId(shopUser.getId());
            fyOrderItem.setCreateTime(new Date());
            fyOrderItem.setGoodsId(f.getFyGoodsId());
            fyOrderItem.setOrderNum(key);
            fyOrderItem.setDealerName(f.getDealerName());
            fyOrderItem.setDealerId(f.getDealerId());
            fyOrderItem.setSinglePrice(f.getPrice());
            fyOrderItem.setDiscount(f.getDiscount());
            fyOrderItem.setCount(f.getCount());
            fyOrderItem.setGiftPrice(f.getGiftPrice());
            //1表示未删除
            fyOrderItem.setIsdelete(1);
            //是否申请退款  1 表示否
            fyOrderItem.setIsrefound(1);
            //是否退款成功  1 表示否
            fyOrderItem.setRefoundState(1);
            fyOrderItem.setPayPrice(new BigDecimal(0));
            fyOrderItemList.add(fyOrderItem);
            //订单详情存入数据库
            fyOrderItemService.save(fyOrderItem);
            //1表示添加购物车购买  2表示不添加购物车直接购买
            if(1==fygoodsCartVo.getByCar()){
                //删除购物车里面的数据
                hashOperations.delete("userCart:"+shopUser.getId(),f.getFyGoodsId());
            }
            //记录订单号，延迟消息队列要用到
            f.setOrderNum(key);
        }
        createOrderByAVtivityadndiscountandCard(fygoodsCartVo, key, shopUser, totalPrice, shopUser.getActivityCost(), goodsInfoStr,fyOrderItemList,new BigDecimal(0));


    }

    private void payByCostAndCardAndDiscount(FygoodsCartVo fygoodsCartVo, ShopUser shopUser, BigDecimal totalPrice, BigDecimal discount ) {
        HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
        String key = KeyUtils.getKey();
        String goodsInfoStr="";
        List<FygoodsCart> fygoodsCarts = avgActivityCost(fygoodsCartVo.getList(), totalPrice, new BigDecimal(shopUser.getActivityCost()));
        fygoodsCarts = avgDiscount(fygoodsCarts, discount,totalPrice);
        fygoodsCartVo.setList(fygoodsCarts);
        BigDecimal subtract = totalPrice.subtract(new BigDecimal(shopUser.getActivityCost())).subtract(discount);
        fygoodsCartVo = avgGiftCard(fygoodsCartVo, subtract);
        Integer flg = 1;
        List<FyOrderItem> fyOrderItemList = new ArrayList<>();
        for (FygoodsCart f : fygoodsCartVo.getList()) {
            Goods fygoods=   fygoodsService.get(f.getFyGoodsId());
            //生成订单信息
            //将商品详情拼接成一个字符串
            goodsInfoStr +=f.getGoodsName()+":"+f.getPrice()+"*"+f.getCount()+",";
            FyOrderItem  fyOrderItem  =  new FyOrderItem();
            fyOrderItem.setId(KeyUtils.getKey());
            fyOrderItem.setType(f.getType());
            fyOrderItem.setSendType(f.getSendType());
            fyOrderItem.setJiadian(fygoods.getJiadian());
            fyOrderItem.setGoodsName(f.getGoodsName());
            fyOrderItem.setGiftPrice(f.getPrice());
            fyOrderItem.setGoodsCover(f.getCover());
            fyOrderItem.setGoodsId(f.getFyGoodsId());
            fyOrderItem.setOrderNum(key);
            fyOrderItem.setDealerName(f.getDealerName());
            fyOrderItem.setDealerId(f.getDealerId());
            fyOrderItem.setSinglePrice(f.getPrice());
            fyOrderItem.setDiscount(new BigDecimal(0));
            fyOrderItem.setActivityCost(new BigDecimal(0));
            fyOrderItem.setCount(f.getCount());
            //1表示未删除
            fyOrderItem.setIsdelete(1);
            //是否申请退款  1 表示否
            fyOrderItem.setIsrefound(1);
            //是否退款成功  1 表示否
            fyOrderItem.setRefoundState(1);
            fyOrderItem.setPayPrice(new BigDecimal(0));
            fyOrderItemList.add(fyOrderItem);
            //订单详情存入数据库
            fyOrderItemService.save(fyOrderItem);
            //1表示添加购物车购买  2表示不添加购物车直接购买
            if(1==fygoodsCartVo.getByCar()){
                //删除购物车里面的数据
                hashOperations.delete("userCart:"+shopUser.getId(),f.getFyGoodsId());
            }
            //记录订单号，延迟消息队列要用到
            f.setOrderNum(key);
        }
        createOrderByAVtivityadndiscountandCard(fygoodsCartVo, key, shopUser, totalPrice, shopUser.getActivityCost(), goodsInfoStr,fyOrderItemList,discount);
    }

    private void createOrderByAVtivityadndiscountandCard(FygoodsCartVo fygoodsCartVo, String key, ShopUser shopUser, BigDecimal totalPrice, Double activityCost, String goodsInfoStr, List<FyOrderItem> fyOrderItemList, BigDecimal discount) {
        FyOrder fyOrder=new FyOrder();
        //实体类封装
        fyOrder.setId(IdGenerator.uuid());
        fyOrder.setOrderNum(key);
        fyOrder.setFyCode(fygoodsCartVo.getFyCode());
        fyOrder.setUserName(shopUser.getNickname());
        fyOrder.setUserId(shopUser.getId());
        fyOrder.setPhoneNum(shopUser.getPhone());
        fyOrder.setAllPrice(totalPrice);
        fyOrder.setDiscount(discount);
        fyOrder.setGiftCardId(fygoodsCartVo.getGiftCardUser());
        //实际支付金额，总金额减去折扣
        fyOrder.setPayPrice(new BigDecimal(0));
        fyOrder.setGoodsBody(goodsInfoStr);
        fyOrder.setOrderTime(new Date());
        //未删除状态
        fyOrder.setIsdelete(1);
        fyOrder.setPayType(4);
        fyOrder.setActivityCost(new BigDecimal(activityCost));
        //下单成功，是未支付状态
        fyOrder.setState(2);
        //是否申请退款 1 表示没有  2 表示有
        fyOrder.setIsrefound(1);
        fyOrder.setDeliver(1);
        //退款金额默认为0
        fyOrder.setRefoundPrice(new BigDecimal(0));

        //订单信息存入数据库
        fyOrderService.save(fyOrder);
        //订单信息存一份到redis，支付页面获取数据直接从redis中拿，减轻数据库的压力   订单基本信息+订单详情
        FyOrderResultVo  fyOrderResultVo  = new FyOrderResultVo();
        fyOrderResultVo.setFyOrder(fyOrder);
        fyOrderResultVo.setFyOrderItemList(fyOrderItemList);
        redisTemplate.opsForValue().set("order:"+fyOrder.getOrderNum(), JsonUtil.object2JsonStr(fyOrderResultVo ));
        redisTemplate.expire("order:"+fyOrder.getOrderNum(),120, TimeUnit.SECONDS);
        // sendOrderMessage(fyOrder.getOrderNum());
        //发送信息至延迟 消息队列，主要用来做订单回滚
        sendDelayMessage(fygoodsCartVo.getList());
    }

    private FygoodsCartVo avgGiftCard(FygoodsCartVo fygoodsCartVo, BigDecimal subtract) {
        List<FygoodsCart> list = fygoodsCartVo.getList();
        BigDecimal totalCost = new BigDecimal(0);
        MathContext mc = new MathContext(2, RoundingMode.HALF_DOWN);
        BigDecimal avg = subtract.divide(new BigDecimal(list.size()), mc);
        if (subtract.intValue() % fygoodsCartVo.getList().size() == 0) {
            for (FygoodsCart fygoodsCart : fygoodsCartVo.getList()) {
                fygoodsCart.setGiftPrice(avg);
            }
        } else {
            for (int i = 0; i < list.size(); i++) {
                if (i == list.size() - 1) {
                    list.get(i).setGiftPrice(subtract.subtract(totalCost));
                } else {
                    list.get(i).setGiftPrice(avg);
                    totalCost = totalCost.add(avg);
                }
            }
        }
        return fygoodsCartVo;

    }

    private List<FygoodsCart> avgActivityCost(List<FygoodsCart> fygoodsList, BigDecimal totalPrice, BigDecimal activityCost) {
        //计算折扣
        BigDecimal hasUse=new BigDecimal(0);
        for (int i=0 ;i<fygoodsList.size();i++){
            if (i==fygoodsList.size()-1){
                fygoodsList.get(i).setActivityCost(activityCost.subtract(hasUse));
            }else {
                MathContext mc = new MathContext(2, RoundingMode.HALF_DOWN);
                BigDecimal price = fygoodsList.get(i).getPrice();
                BigDecimal divide = price.divide(totalPrice, mc);
                BigDecimal multiply = divide.multiply(activityCost);
                hasUse=hasUse.add(multiply);
                fygoodsList.get(i).setActivityCost(multiply);
            }
        }
        return fygoodsList;


    }

    private int payByActivityCost(ShopUser shopUser, BigDecimal totalPrice,  FygoodsCartVo  fygoodsCartVo, String key,BigDecimal hasDiscount) {
        shopUser=  shopUserService.get(shopUser.getId());
        Double activityCost = shopUser.getActivityCost();
        if (totalPrice.compareTo(new BigDecimal(activityCost)) <= 0) {
            createDetailByAVtivity(fygoodsCartVo, shopUser, key,totalPrice,new BigDecimal(0),shopUser.getActivityCost());
            return 1;
        } else {
            return -1;
        }
    }


    private void createByAVtivityadndiscount(FygoodsCartVo fygoodsCartVo, ShopUser shopUser, String key, BigDecimal totalPrice, BigDecimal discount, Double activityCost) {
        List<FyOrderItem>  fyOrderItemList  = new ArrayList<>();
        HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
        String  goodsInfoStr = "";

        for (FygoodsCart f : fygoodsCartVo.getList()){
            Goods fygoods=   fygoodsService.get(f.getFyGoodsId());
            //生成订单信息
            //将商品详情拼接成一个字符串
            goodsInfoStr +=f.getGoodsName()+":"+f.getPrice()+"*"+f.getCount()+",";
            FyOrderItem  fyOrderItem  =  new FyOrderItem();
            fyOrderItem.setId(KeyUtils.getKey());
            fyOrderItem.setType(f.getType());
            fyOrderItem.setSendType(f.getSendType());
            fyOrderItem.setJiadian(fygoods.getJiadian());
            fyOrderItem.setGoodsName(f.getGoodsName());
            fyOrderItem.setActivityCost(f.getPrice());
            fyOrderItem.setGoodsCover(f.getCover());
            fyOrderItem.setGoodsId(f.getFyGoodsId());
            fyOrderItem.setOrderNum(key);
            fyOrderItem.setDealerName(f.getDealerName());
            fyOrderItem.setDealerId(f.getDealerId());
            fyOrderItem.setSinglePrice(f.getPrice());
            fyOrderItem.setDiscount(f.getDiscount());
            fyOrderItem.setActivityCost(f.getActivityCost());
            fyOrderItem.setCount(f.getCount());
            fyOrderItem.setUserId(shopUser.getId());
            fyOrderItem.setCreateTime(new Date());
            //1表示未删除
            fyOrderItem.setIsdelete(1);
            //是否申请退款  1 表示否
            fyOrderItem.setIsrefound(1);
            //是否退款成功  1 表示否
            fyOrderItem.setRefoundState(1);
            fyOrderItem.setPayPrice(new BigDecimal(0));
            fyOrderItemList.add(fyOrderItem);
            //订单详情存入数据库
            fyOrderItemService.save(fyOrderItem);
            //1表示添加购物车购买  2表示不添加购物车直接购买
            if(1==fygoodsCartVo.getByCar()){
                //删除购物车里面的数据
                hashOperations.delete("userCart:"+shopUser.getId(),f.getFyGoodsId());
            }
            //记录订单号，延迟消息队列要用到
            f.setOrderNum(key);
        }
        createOrderByAVtivityadndiscount(fygoodsCartVo, key, shopUser, totalPrice, activityCost, goodsInfoStr,fyOrderItemList,discount);
    }

    private void createOrderByAVtivityadndiscount(FygoodsCartVo fygoodsCartVo, String key, ShopUser shopUser, BigDecimal totalPrice, Double activityCost, String goodsInfoStr, List<FyOrderItem> fyOrderItemList,BigDecimal discount ) {
        FyOrder fyOrder=new FyOrder();
        //实体类封装
        fyOrder.setId(IdGenerator.uuid());
        fyOrder.setOrderNum(key);
        fyOrder.setFyCode(fygoodsCartVo.getFyCode());
        fyOrder.setUserName(shopUser.getNickname());
        fyOrder.setUserId(shopUser.getId());
        fyOrder.setPhoneNum(shopUser.getPhone());
        fyOrder.setAllPrice(totalPrice);
        fyOrder.setDiscount(discount);
        //实际支付金额，总金额减去折扣
        fyOrder.setPayPrice(new BigDecimal(0));
        fyOrder.setGoodsBody(goodsInfoStr);
        fyOrder.setOrderTime(new Date());
        fyOrder.setActivityCost(new BigDecimal(activityCost));
        //未删除状态
        fyOrder.setIsdelete(1);
        fyOrder.setPayType(5);
        //下单成功，是未支付状态
        fyOrder.setState(2);
        //是否申请退款 1 表示没有  2 表示有
        fyOrder.setIsrefound(1);
        fyOrder.setDeliver(1);
        //退款金额默认为0
        fyOrder.setRefoundPrice(new BigDecimal(0));

        //订单信息存入数据库
        fyOrderService.save(fyOrder);
        //订单信息存一份到redis，支付页面获取数据直接从redis中拿，减轻数据库的压力   订单基本信息+订单详情
        FyOrderResultVo  fyOrderResultVo  = new FyOrderResultVo();
        fyOrderResultVo.setFyOrder(fyOrder);
        fyOrderResultVo.setFyOrderItemList(fyOrderItemList);
        redisTemplate.opsForValue().set("order:"+fyOrder.getOrderNum(), JsonUtil.object2JsonStr(fyOrderResultVo ));
        redisTemplate.expire("order:"+fyOrder.getOrderNum(),120, TimeUnit.SECONDS);
        // sendOrderMessage(fyOrder.getOrderNum());
        //发送信息至延迟 消息队列，主要用来做订单回滚
        sendDelayMessage(fygoodsCartVo.getList());
    }


    private void createDetailByAVtivity(FygoodsCartVo fygoodsCartVo, ShopUser shopUser, String key, BigDecimal totalPrice, BigDecimal bigDecimal, Double activityCost) {
        List<FyOrderItem>  fyOrderItemList  = new ArrayList<>();
        HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
        String  goodsInfoStr = "";

        for (FygoodsCart f : fygoodsCartVo.getList()){
            Goods fygoods=   fygoodsService.get(f.getFyGoodsId());
            //生成订单信息
            //将商品详情拼接成一个字符串
            goodsInfoStr +=f.getGoodsName()+":"+f.getPrice()+"*"+f.getCount()+",";
            FyOrderItem  fyOrderItem  =  new FyOrderItem();
            fyOrderItem.setId(KeyUtils.getKey());
            fyOrderItem.setType(f.getType());
            fyOrderItem.setSendType(f.getSendType());
            fyOrderItem.setJiadian(fygoods.getJiadian());
            fyOrderItem.setGoodsName(f.getGoodsName());
            fyOrderItem.setPayType(2);
            fyOrderItem.setActivityCost(f.getPrice());
            fyOrderItem.setGoodsCover(f.getCover());
            fyOrderItem.setGoodsId(f.getFyGoodsId());
            fyOrderItem.setCreateTime(new Date());
            fyOrderItem.setOrderNum(key);
            fyOrderItem.setDealerName(f.getDealerName());
            fyOrderItem.setDealerId(f.getDealerId());
            fyOrderItem.setSinglePrice(f.getPrice());
            fyOrderItem.setDiscount(f.getDiscount());
            fyOrderItem.setCount(f.getCount());
            fyOrderItem.setUserId(shopUser.getId());
            //1表示未删除
            fyOrderItem.setIsdelete(1);
            //是否申请退款  1 表示否
            fyOrderItem.setIsrefound(1);
            //是否退款成功  1 表示否
            fyOrderItem.setRefoundState(1);
            //佣金支付
                fyOrderItem.setPayPrice(new BigDecimal(0));
            fyOrderItemList.add(fyOrderItem);
            //订单详情存入数据库
            fyOrderItemService.save(fyOrderItem);
            //1表示添加购物车购买  2表示不添加购物车直接购买
            if(1==fygoodsCartVo.getByCar()){
                //删除购物车里面的数据
                hashOperations.delete("userCart:"+shopUser.getId(),f.getFyGoodsId());
            }
            //记录订单号，延迟消息队列要用到
            f.setOrderNum(key);
        }
        createOrderByActivityCost(fygoodsCartVo, key, shopUser, totalPrice, activityCost, goodsInfoStr,fyOrderItemList);
    }

    private void createOrderByActivityCost(FygoodsCartVo fygoodsCartVo, String key, ShopUser shopUser, BigDecimal totalPrice, Double activityCost, String goodsInfoStr, List<FyOrderItem> fyOrderItemList) {
        FyOrder fyOrder=new FyOrder();
        //实体类封装
        fyOrder.setId(IdGenerator.uuid());
        fyOrder.setOrderNum(key);
        fyOrder.setFyCode(fygoodsCartVo.getFyCode());
        fyOrder.setUserName(shopUser.getNickname());
        fyOrder.setUserId(shopUser.getId());
        fyOrder.setPhoneNum(shopUser.getPhone());
        fyOrder.setAllPrice(totalPrice);
        fyOrder.setPayTime(new Date());
        fyOrder.setActivityCost(new BigDecimal(activityCost));
        fyOrder.setDiscount(new BigDecimal(0));
        //实际支付金额，总金额减去折扣
        fyOrder.setPayPrice(new BigDecimal(0));
        fyOrder.setGoodsBody(goodsInfoStr);
        fyOrder.setOrderTime(new Date());
        //未删除状态
        fyOrder.setIsdelete(1);
        //下单成功，是未支付状态
        fyOrder.setState(2);
        //是否申请退款 1 表示没有  2 表示有
        fyOrder.setIsrefound(1);
        fyOrder.setDeliver(1);
        //退款金额默认为0
        fyOrder.setRefoundPrice(new BigDecimal(0));

        //订单信息存入数据库
        fyOrderService.save(fyOrder);
        //订单信息存一份到redis，支付页面获取数据直接从redis中拿，减轻数据库的压力   订单基本信息+订单详情
        FyOrderResultVo  fyOrderResultVo  = new FyOrderResultVo();
        fyOrderResultVo.setFyOrder(fyOrder);
        fyOrderResultVo.setFyOrderItemList(fyOrderItemList);
        redisTemplate.opsForValue().set("order:"+fyOrder.getOrderNum(), JsonUtil.object2JsonStr(fyOrderResultVo ));
        redisTemplate.expire("order:"+fyOrder.getOrderNum(),120, TimeUnit.SECONDS);
        // sendOrderMessage(fyOrder.getOrderNum());
        //发送信息至延迟 消息队列，主要用来做订单回滚
        sendDelayMessage(fygoodsCartVo.getList());
    }


    private List<FygoodsCart> avgDiscount(List<FygoodsCart>   fygoodsList, BigDecimal discount, BigDecimal totalPrice) {
        //计算折扣
        BigDecimal hasUse=new BigDecimal(0);
        for (int i=0 ;i<fygoodsList.size();i++){
            if (i==fygoodsList.size()-1){
                fygoodsList.get(i).setDiscount(discount.subtract(hasUse));
            }else {
                MathContext mc = new MathContext(2, RoundingMode.HALF_DOWN);
                BigDecimal price = fygoodsList.get(i).getPrice();
                BigDecimal divide = price.divide(totalPrice, mc);
                BigDecimal multiply = divide.multiply(discount);
                hasUse=hasUse.add(multiply);
                fygoodsList.get(i).setDiscount(multiply);
            }
        }
        return fygoodsList;
    }

    private  Result<FyOrder> createOrderDetail(FygoodsCartVo fygoodsCartVo,BigDecimal discount,BigDecimal totalPrice,ShopUser shopUser) {
        List<FygoodsCart>   fygoodsList = fygoodsCartVo.getList();
        String key=KeyUtils.getKey();
        fygoodsList = avgDiscount(fygoodsList,discount,totalPrice);
        fygoodsCartVo.setList(fygoodsList);
        Result<FyOrder> detail = createDetail(fygoodsCartVo, shopUser, key,totalPrice,discount);
        return detail;
    }

    private      Result<FyOrder>   createDetail(FygoodsCartVo fygoodsCartVo, ShopUser shopUser,String key,BigDecimal totalPrice,BigDecimal discount ) {
        List<FyOrderItem>  fyOrderItemList  = new ArrayList<>();
        HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
        String  goodsInfoStr = "";

        for (FygoodsCart f : fygoodsCartVo.getList()){
            Goods fygoods=   fygoodsService.get(f.getFyGoodsId());
            //生成订单信息
            //将商品详情拼接成一个字符串
            goodsInfoStr +=f.getGoodsName()+":"+f.getPrice()+"*"+f.getCount()+",";
            FyOrderItem  fyOrderItem  =  new FyOrderItem();
            fyOrderItem.setId(KeyUtils.getKey());
            fyOrderItem.setType(f.getType());
            fyOrderItem.setSendType(f.getSendType());
            fyOrderItem.setJiadian(fygoods.getJiadian());
            fyOrderItem.setGoodsName(f.getGoodsName());
            fyOrderItem.setGoodsCover(f.getCover());
            fyOrderItem.setGoodsId(f.getFyGoodsId());
            fyOrderItem.setOrderNum(key);
            fyOrderItem.setDealerName(f.getDealerName());
            fyOrderItem.setDealerId(f.getDealerId());
            fyOrderItem.setSinglePrice(f.getPrice());
            fyOrderItem.setDiscount(f.getDiscount());
            fyOrderItem.setCount(f.getCount());
            fyOrderItem.setUserId(shopUser.getId());
            fyOrderItem.setCreateTime(new Date());
            //1表示未删除
            fyOrderItem.setIsdelete(1);
            //是否申请退款  1 表示否
            fyOrderItem.setIsrefound(1);
            //是否退款成功  1 表示否
            fyOrderItem.setRefoundState(1);
                fyOrderItem.setPayPrice(f.getPrice().multiply(new BigDecimal(f.getCount())).subtract(f.getDiscount()));
            fyOrderItemList.add(fyOrderItem);
            //订单详情存入数据库
            fyOrderItemService.save(fyOrderItem);
            //1表示添加购物车购买  2表示不添加购物车直接购买
            if(1==fygoodsCartVo.getByCar()){
                //删除购物车里面的数据
                hashOperations.delete("userCart:"+shopUser.getId(),f.getFyGoodsId());
            }
            //记录订单号，延迟消息队列要用到
            f.setOrderNum(key);
        }
        Result<FyOrder> orderByAll = createOrderByAll(fygoodsCartVo, key, shopUser, totalPrice, discount, goodsInfoStr,fyOrderItemList);
        return orderByAll;
    }

    private Result<FyOrder> createOrderByAll(FygoodsCartVo fygoodsCartVo, String key, ShopUser shopUser, BigDecimal totalPrice, BigDecimal discount, String goodsInfoStr, List<FyOrderItem>  fyOrderItemList) {
        FyOrder fyOrder=new FyOrder();
        //实体类封装
        fyOrder.setId(IdGenerator.uuid());
        fyOrder.setOrderNum(key);
        fyOrder.setFyCode(fygoodsCartVo.getFyCode());
        fyOrder.setUserName(shopUser.getNickname());
        fyOrder.setUserId(shopUser.getId());
        fyOrder.setPhoneNum(shopUser.getPhone());
        fyOrder.setAllPrice(totalPrice);
        fyOrder.setDiscount(discount);
        //实际支付金额，总金额减去折扣
        fyOrder.setPayPrice(totalPrice.subtract(discount));
        fyOrder.setGoodsBody(goodsInfoStr);
        fyOrder.setOrderTime(new Date());
        fyOrder.setPayType(1);
        fyOrder.setCouponsId(fygoodsCartVo.getCouponsId());
        //未删除状态
        fyOrder.setIsdelete(1);
        //下单成功，是未支付状态
        fyOrder.setState(2);
        //是否申请退款 1 表示没有  2 表示有
        fyOrder.setIsrefound(1);
        fyOrder.setDeliver(1);
        fyOrder.setPayTime(new Date());
        //退款金额默认为0
        fyOrder.setRefoundPrice(new BigDecimal(0));

        //订单信息存入数据库
        fyOrderService.save(fyOrder);
        //订单信息存一份到redis，支付页面获取数据直接从redis中拿，减轻数据库的压力   订单基本信息+订单详情
        FyOrderResultVo  fyOrderResultVo  = new FyOrderResultVo();
        fyOrderResultVo.setFyOrder(fyOrder);
        fyOrderResultVo.setFyOrderItemList(fyOrderItemList);
        redisTemplate.opsForValue().set("order:"+fyOrder.getOrderNum(), JsonUtil.object2JsonStr(fyOrderResultVo ));
        redisTemplate.expire("order:"+fyOrder.getOrderNum(),120, TimeUnit.SECONDS);
        // sendOrderMessage(fyOrder.getOrderNum());
        //发送信息至延迟 消息队列，主要用来做订单回滚
        sendDelayMessage(fygoodsCartVo.getList());
        return   Result.ok(fyOrder);
    }

    /**
     * @Author: jick
     * @Date: 2019/12/14 19:58
     * 查询订单类表
     * 二期没用
     */
    @RequestMapping("/getOrderList")
    @PermissionMode(PermissionMode.Mode.White)
     public  Result<?>  getOrderList(@RequestBody(required = false)  Map<String,Object> param){
         List<FyOrder> orderList  = new ArrayList<>();

         HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
         //查询
         Map<String, String> resultMap = hashOperations.entries("order:"+param.get("phoneNum"));

    //    Set<String> s  =hashOperations.keys("order*");


         // 判断map是否为空
         if (!resultMap.isEmpty() && resultMap.size() > 0) {
             //遍历
             for(Map.Entry<String, String> map : resultMap.entrySet()){
                 FyOrder  fyOrder  =  JsonUtil.jsonStr2Object(map.getValue(),  FyOrder.class);
                 orderList.add(fyOrder);

             }

         }

        return   Result.ok(orderList);

     }



    /**
     * @Author: jick
     * @Date: 2019/12/14 19:40
     *确认支付状态，订单信息存入关系型数据库，同时删除缓存数据库里面的记录
     * 将订单号和·手机号码传过来
     * 二期没用
     */
    @RequestMapping("/payOrder")
    //@PermissionMode(PermissionMode.Mode.White)
      public  Result<?>  PayController(@RequestBody Map<String,String> param,@Session(SessionKeys.USER) User user){
          HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();

         String  orderStr =   hashOperations.get("order:"+param.get("phoneNum"),param.get("orderNum"));
           //查询订单基本信息，转换成对应实体类
           FyOrder fyOrder = JsonUtil.jsonStr2Object(orderStr,FyOrder.class);

           //存放至数据库
             fyOrder.setState(1);
             fyOrder.setDistributorName(user.getUsername());
             fyOrder.setDistributorNickName(user.getNickname());
            // fyOrder.setId(IdGenerator.uuid());
            fyOrderService.save(fyOrder);
          //删除redis中的记录
         hashOperations.delete("order:"+param.get("phoneNum"),param.get("orderNum"));

        //生成订单详情
        //查询
        List<FyOrderItem> list  = new ArrayList<>();
        Map<String, String> resultMap = hashOperations.entries("item:"+param.get("orderNum"));
        // 判断map是否为空
        if (!resultMap.isEmpty() && resultMap.size() > 0) {
            //遍历
            for(Map.Entry<String, String> map : resultMap.entrySet()){
                FyOrderItem  fyOrderItem  =  JsonUtil.jsonStr2Object(map.getValue(),  FyOrderItem.class);
                list.add(fyOrderItem);
            }
            fyOrderItemService.save(list);
            //清除redis中的缓存
            redisTemplate.delete("item:"+param.get("orderNum"));
        }



          return   Result.ok();
      }


/*userCardsInfo/getMyCoupons*/


      /**
       * @Author: jick
       * @Date: 2019/12/15 13:30
       * 后台查看订单基本信息信息
       *
       */
      @RequestMapping("/getManagerOrderList")
      public   Result<?>  getFyOrderList(Pagination pagination,@RequestBody(required = false)  Map<String,Object>  param ){
          if (StringUtils.isNotBlank((String) param.get("endTime"))) {
              param.put("endTime", (String) param.get("endTime") + " 23:59:59");
          }
          if (StringUtils.isNotBlank((String) param.get("startTime"))) {
              param.put("startTime", (String) param.get("startTime") + " 00:00:00");
          }
          param.put("isdelete",1);
          Pageable<FyOrder> list = fyOrderService.queryOrderList(param,pagination);
          return  Result.ok(list);
      }


      /**
       * @Author: jick
       * @Date: 2020/1/10 10:26
       * 获取退款订单信息
       */
      @RequestMapping("/getRefoundOrderList")
      public   Result<?>   getRefoudOrderList(Pagination pagination,@RequestBody(required = false)  Map<String,Object>  param){
          param.put("isdelete",1);
          param.put("ishasRefound",1);
          Pageable<FyOrder> list = fyOrderService.refoundOrderList(param,pagination);
          return  Result.ok(list);
      }

      /**
       * @Author: jick
       * @Date: 2020/1/13 16:03
       * 获取退款订单详情
       */
      @RequestMapping("/getRefoundOrderDetail")
      public   Result<?>   getRefoudOrderDetail(Pagination pagination,@RequestBody(required = false)  Map<String,Object>  param){
          param.put("refoundState",1);
          Pageable<FyOrderItem> list = fyOrderItemService.queryOrderItemByNum(param,pagination);
          return  Result.ok(list);
      }



     // '/runtime/config


      /**
       * @Author: jick
       * @Date: 2019/12/17 18:03
       * 后台更具id查看商品详情
       */
      @RequestMapping("/getManagerOrderItem")
      @PermissionMode(PermissionMode.Mode.White)
      public   Result<?>  getFyOrderItemList(@RequestBody(required = false)  Map<String,Object>  param ,Pagination pagination ){

          Pageable<FyOrderItem> list =  fyOrderItemService.queryOrderItemByNum(param,pagination);

          return  Result.ok(list);
      }


     /* @RequestMapping("/getManagerOrderItem")
      @PermissionMode(PermissionMode.Mode.White)
      public   Result<?>  getFyOrderItemList(@RequestBody(required = false)  Map<String,Object>  param ){
            List<FyOrderItem>   list =  fyOrderItemService.list(param);
          return  Result.ok(list);
      }*/




      /**
       * @Author: jick
       * @Date: 2019/12/16 11:44
       * 前台获取订单信息
       */
      @RequestMapping("/portalGetOrderList")
       public  Result<?>   getPortalOrderList(@Session("ShopUser") ShopUser shopUser, @RequestBody Map<String,Object> params){
          List<FyOrderResultVo> fyOrderResultVoList  = new ArrayList<>();

          Map<String,Object> map  = new HashMap<>(5);
          Map<String,Object> map2  = new HashMap<>(5);
          //查询已支付订单,已经支付的订单和部分退款的订单
              //包含已支付订单和部分商品已退款的订单
              map.put("state",params.get("state"));
          List<FyOrder>  orderList=new ArrayList<>(100);
          Integer states = (Integer) params.get("state");
          map.put("userId",shopUser.getId());
          map.put("isdelete",1);
          int parseInt = Integer.valueOf(states);
          if (parseInt==3){
              map.put("state2",4);
              map.put("state",3);
              orderList  =   fyOrderService.queryPayOrderList2(map);
          }else {
              orderList  =   fyOrderService.queryPayOrderList(map);
          }
              for(FyOrder  f  : orderList){
                  FyOrderResultVo fyOrderResultVo  = new FyOrderResultVo();
                  fyOrderResultVo.setFyOrder(f);
                  //查询未退款订单
                  map2.put("orderNum",f.getOrderNum());
                  map2.put("isdelete",1);
                  List<FyOrderItem> fyOrderItemList = fyOrderItemService.list(map2);
                  for (FyOrderItem fyOrderItem : fyOrderItemList) {
                      if (fyOrderItem.getType()==1||fyOrderItem.getType()==2){
                          Goods goods1 = fygoodsService.get(fyOrderItem.getGoodsId());
                          GoodsBase goodsBase = goodsBaseService.get(goods1.getGoodsId());
                          fyOrderItem.setGoodsCover(goodsBase.getGoodCover());
                      }
                      if (fyOrderItem.getType()==3){
                              Fygoods fygoods = goodsService.get(fyOrderItem.getGoodsId());
                              Goods goods=fygoodsService.get(fygoods.getGoodsId());
                              GoodsBase goodsBase = goodsBaseService.get(goods.getGoodsId());
                              fyOrderItem.setGoodsCover(goodsBase.getGoodCover());
                          }
                          if (fyOrderItem.getType()==4){
                              GrouponGoods fygoods = grouponGoodsService.get(fyOrderItem.getGoodsId());
                              Goods goods=fygoodsService.get(fygoods.getGoodsId());
                              GoodsBase goodsBase = goodsBaseService.get(goods.getGoodsId());
                              fyOrderItem.setGoodsCover(goodsBase.getGoodCover());
                          }
                          if (fyOrderItem.getType()==5){
                              ShareGoods fygoods = shareGoodsService.get(fyOrderItem.getGoodsId());
                              Goods goods=fygoodsService.get(fygoods.getGoodsId());
                              GoodsBase goodsBase = goodsBaseService.get(goods.getGoodsId());
                              fyOrderItem.setGoodsCover(goodsBase.getGoodCover());
                          }
                          if (fyOrderItem.getType()==6){
                              BargainGoods fygoods = bargainGoodsService.get(fyOrderItem.getGoodsId());
                              Goods goods=fygoodsService.get(fygoods.getGoodsId());
                              GoodsBase goodsBase = goodsBaseService.get(goods.getGoodsId());
                              fyOrderItem.setGoodsCover(goodsBase.getGoodCover());
                          }
                  }
                  fyOrderResultVo.setFyOrderItemList(fyOrderItemList);
                  fyOrderResultVoList.add(fyOrderResultVo);
              }
          return Result.ok(fyOrderResultVoList);
       }









       /**
        * @Author: jick
        * @Date: 2019/12/16 15:31
        * 获取未支付的订单信息,从redis中拿
        * 二期用不到
        */
       @RequestMapping("/portalGetUnPayOrder")
    //   @PermissionMode(PermissionMode.Mode.White)
       public   Result<?>  getUpPayOrderList(@Session("ShopUser") ShopUser shopUser){
  //     public   Result<?>  getUpPayOrderList(){

     //      ShopUser shopUser  = new ShopUser();

     //      shopUser.setPhone("13215657368");


           List<FyOrderResultVo> fyOrderResultVoList = new ArrayList<>();

           HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
           //查询
           Map<String, String> resultMap = hashOperations.entries("order:"+shopUser.getPhone());


           List<FyOrder> list = new ArrayList<>();

           // 判断map是否为空
           if (!resultMap.isEmpty() && resultMap.size() > 0) {
               //遍历
               for(Map.Entry<String, String> map : resultMap.entrySet()){
                   FyOrder  fyOrder  =  JsonUtil.jsonStr2Object(map.getValue(),  FyOrder.class);
                       list.add(fyOrder);
               }

           }

           //排序
          // Collections.sort(list);








           // 判断map是否为空
           if (list.size() > 0) {
               //遍历
               for(FyOrder fyOrder : list){

                   //获取单个订单商品详情
                   List<FyOrderItem>  fyOrderItemList =  getUpPayOrderDetail(fyOrder.getOrderNum());

                  // Collections.sort(fyOrderItemList);

                   //封装  一个订单几本信息+订单商品详情集合
                   FyOrderResultVo  fyOrderResultVo  = new FyOrderResultVo();
                   fyOrderResultVo.setFyOrder(fyOrder);
                   fyOrderResultVo.setFyOrderItemList(fyOrderItemList);
                   fyOrderResultVoList.add(fyOrderResultVo);

               }

           }
           return   Result.ok(fyOrderResultVoList);

       }




       /**
        * @Author: jick
        * @Date: 2019/12/16 15:37
        * 获取未支付订单的详细信息，从redis中拿
        */
       public    List<FyOrderItem>  getUpPayOrderDetail(String orderNum){

           List<FyOrderItem>  orderItemList  = new ArrayList<>();

           Map<String,Object> param  = new HashMap<>();
            param.put("orderNum",orderNum);

           HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
           //查询redis
           Map<String, String> resultMap = hashOperations.entries("item:"+param.get("orderNum"));

           // 判断map是否为空
           if (!resultMap.isEmpty() && resultMap.size() > 0) {
               //遍历
               for(Map.Entry<String, String> map : resultMap.entrySet()){
                   FyOrderItem  fyOrderItem  =  JsonUtil.jsonStr2Object(map.getValue(),  FyOrderItem.class);
                   orderItemList.add(fyOrderItem);
               }

           }
           return    orderItemList;
       }






       /**
        * @Author: jick
        * @Date: 2019/12/16 13:55
        *  退款  一期
        */
       @RequestMapping("/returnOrder")
       @PermissionMode(PermissionMode.Mode.White)
       public   Result<?>   ReturnOrderController(@RequestBody FyOrderItem fyOrderItem) {


           //改变商品基本表信息
           Map<String,Object> param = new HashMap<>();
           param.put("orderNum",fyOrderItem.getOrderNum());
           FyOrder  fyOrder  =    fyOrderService.getOne(param);//根据订单号查询基本订单信息

           fyOrder.setState(3);//三表示已有商品确认退款
           fyOrder.setRefoundPrice(fyOrder.getRefoundPrice().add(fyOrderItem.getPayPrice()));//退款金额更新
           fyOrderService.update(fyOrder);


           //更新订单基本表信息
           fyOrderItem.setRefoundState(1);//已确认退款
           fyOrderItemService.update(fyOrderItem);

         //  fyOrder.set
          // fyOrderService.updateSelective();
           return  Result.ok();

       }


       /**
        * @Author: jick
        * @Date: 2019/12/16 14:49
        * 删除订单
        */
       @RequestMapping("/deleteOrder")
       public   Result<?>   DeleteOrderController(@RequestBody FyOrder fyOrder) {
           fyOrder.setIsdelete(2);//2表示订单删除
           fyOrderService.update(fyOrder);
           return  Result.ok();
       }
       





       /**
        * @description:
         导出表单信息
        * @return:
        * @author: jick
        * @date: 2020-02-21 05:32
        */
       @RequestMapping("/export")
       public void export( HttpServletRequest req,HttpServletResponse rsp, @Session(SessionKeys.USER) User user) throws IOException {
            Map<String,Object> params=new HashMap<>(10);
           params.put("startTime", req.getParameter("startTime"));
           params.put("endTime", req.getParameter("endTime"));
           params.put("isdelete",1);
           if (StringUtils.isNotBlank((String) params.get("endTime"))) {
               params.put("endTime", (String) params.get("endTime") + " 23:59:59");
           }
           if (StringUtils.isNotBlank((String) params.get("startTime"))) {
               params.put("startTime", (String) params.get("startTime") +" 00:00:00");
           }


           List<FyOrderVo> result = fyOrderService.queryExportOrderList(params);
           // 导出Excel
           OutputStream os = rsp.getOutputStream();
           rsp.setContentType("application/x-download");
           // 设置导出文件名称
           rsp.setHeader("Content-Disposition",
                   "attachment;filename=" + DateFormatUtils.format(new Date(), "yyyyMMddHHmmssSSS") + ".xls");
           ExportParams param = new ExportParams("订单相关信息表", "订单相关信息", "订单信息");
           param.setAddIndex(true);
           Workbook excel = ExcelExportUtil.exportExcel(param, FyOrderVo.class, result);
           excel.write(os);
           os.flush();
           os.close();
       }

















    /**
     * @Author: jick
     * @Date: 2020/1/6 23:14
     * 发送订单消息至消息队列
     */
    public void sendOrderMessage(String  orderNum){
        rabbitTemplate.convertAndSend(
                "exchange.order.done",
                "orderNum",
                orderNum,
                new MessagePostProcessor() {
                    @Override
                    public Message postProcessMessage(Message message) throws AmqpException {
                        //消息有效期30分钟
                        //message.getMessageProperties().setExpiration(String.valueOf(1800000));
                        message.getMessageProperties();
                        return message;
                    }
                });
    }



    /**
     * @Author:
     * @Date: 2020/1/7 11:00
     * 发送延迟消息消息队列。主要用来做订单回滚
     */

    public void   sendDelayMessage(List<FygoodsCart> fygoodsCarts){

        rabbitTemplate.convertAndSend(
                "exchange.delay.order.begin",
                "delay",
                JSON.toJSONString(fygoodsCarts),
                new MessagePostProcessor() {
                    @Override
                    public Message postProcessMessage(Message message) throws AmqpException {
                        //消息有效期30分钟
                        //message.getMessageProperties().setExpiration(String.valueOf(1800000));
                        //  message.getMessageProperties();
                        message.getMessageProperties().setExpiration(String.valueOf(1800000));
                        return message;
                    }
                });
    }














}
