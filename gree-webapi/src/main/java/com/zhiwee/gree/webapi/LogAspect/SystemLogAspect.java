package com.zhiwee.gree.webapi.LogAspect;

import com.zhiwee.gree.model.Log.Log;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.User;
import com.zhiwee.gree.service.log.LogService;
import com.zhiwee.gree.webapi.util.MAPIHttpServletRequestWrapper;
import com.zhiwee.gree.webapi.util.SystemControllerLog;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.NamedThreadLocal;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;
import xyz.icrab.common.util.KeyUtils;
import xyz.icrab.common.web.session.Session;
import xyz.icrab.common.web.util.SessionUtils;
import xyz.icrab.ums.util.consts.SessionKeys;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Executable;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;

/**
 * @author sun on 2019/3/28
 */
@Aspect
@Component
public class SystemLogAspect {
    private  static  final Logger logger = LoggerFactory.getLogger(SystemLogAspect.class);

    private static final ThreadLocal<Date> beginTimeThreadLocal =
            new NamedThreadLocal<Date>("ThreadLocal beginTime");
    private static final ThreadLocal<Log> logThreadLocal =
            new NamedThreadLocal<Log>("ThreadLocal log");

    private static final ThreadLocal<User> currentUser=new NamedThreadLocal<>("ThreadLocal user");

    private static final ThreadLocal<ShopUser> currentShopUser=new NamedThreadLocal<>("ThreadLocal user");

    @Autowired(required=false)
    private HttpServletRequest request;

    @Autowired
    private com.zhiwee.gree.webapi.LogAspect.ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Autowired
    private LogService logService;





    /**
     * Controller层切点 注解拦截
     */
    @Pointcut("@annotation(com.zhiwee.gree.webapi.util.SystemControllerLog)")
    public void controllerAspect(){}

    /**
     * 前置通知 用于拦截Controller层记录用户的操作的开始时间
     * @param joinPoint 切点
     * @throws InterruptedException
     */
    @Before("controllerAspect()")
    public void doBefore(JoinPoint joinPoint) throws InterruptedException{
            MAPIHttpServletRequestWrapper args = null;
            Log log=new Log();
            String param="";
        for (Object o  :joinPoint.getArgs()){
            if (o instanceof MAPIHttpServletRequestWrapper){
                args=(MAPIHttpServletRequestWrapper)o;
            }
        }
        if (args!=null){
            byte[] body1 = args.getBody();
            try {
                param=    new String(body1, 0, body1.length, "utf-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        log.setParams(param);

        Date beginTime=new Date();
        //线程绑定变量（该数据只有当前请求的线程可见）
        beginTimeThreadLocal.set(beginTime);
        //这里日志级别为debug
        if (logger.isDebugEnabled()){
            logger.debug("开始计时: {}  URI: {}", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS")
                    .format(beginTime), request.getRequestURI());
        }

        logThreadLocal.set(log);
        //读取session中的用户
        Session session = SessionUtils.get(request);
        User user=null;
        ShopUser  shopUser=null;
        if(session != null){
            user=(User)session.getAttribute(SessionKeys.USER);
            shopUser=(ShopUser)session.getAttribute("ShopUser");
        }
       if(user!=null){
       currentUser.set(user);

     }
     if(shopUser != null){
         currentShopUser.set(shopUser);
     }


    }

    /**
     * 后置通知 用于拦截Controller层记录用户的操作
     * @param joinPoint 切点
     */
    @SuppressWarnings("unchecked")
    @After("controllerAspect()")
    public void doAfter(JoinPoint joinPoint) {

        User user = currentUser.get();
        ShopUser shopUser = currentShopUser.get();

            String title="";
        //日志类型(info:入库,error:错误)
            String type="info";
        //请求的IP
            String remoteAddr=request.getRemoteAddr();
        //请求的Uri
            String requestUri=request.getRequestURI();
        //请求的方法类型(post/get)
            String method=request.getMethod();
        //请求提交的参数
//            Map<String,String[]> params=request.getParameterMap();

            try {
                title=getControllerMethodDescription(joinPoint);
            } catch (Exception e) {
                e.printStackTrace();
            }
            // 打印JVM信息。
        //得到线程绑定的局部变量（开始时间）
            long beginTime = beginTimeThreadLocal.get().getTime();
        //2、结束时间
            long endTime = System.currentTimeMillis();
            Log log = logThreadLocal.get();
            log.setLogId(KeyUtils.getKey());
            log.setTitle(title);
            log.setType(type);
            log.setRemoteAddr(remoteAddr);
            log.setRequestUri(requestUri);
            log.setMethod(method);
            if(user!=null){
                log.setUserId(user.getId());
                log.setNickname(user.getNickname());
                log.setState(1);
            }
            if(shopUser!=null){
                log.setShopUserId(shopUser.getId());
                log.setNickname(shopUser.getUsername());
                log.setState(2);
            }
            if("/shopUser/login".equals(requestUri) || "/shopUser/wxLogin".equals(requestUri) || "/shopUser/wxScanLogin".equals(requestUri)){
                log.setRequestMethod(1);
            }else{
                log.setRequestMethod(2);
            }
            Date operateDate=beginTimeThreadLocal.get();
            log.setOperateDate(operateDate);
            log.setTimeout(String.valueOf(endTime - beginTime));
            //消息队列发送日志
//            final String msg = JSON.toJSONString(log);
//            MessageCreator messageCreator = new MessageCreator() {
//                @Override
//                public Message createMessage(javax.jms.Session session) throws JMSException {
//                    return session.createTextMessage(msg);
//                }
//            };
//            try {
//                jmsTemplate.send(activeMQDestination.getCompositeDestinations()[0], messageCreator);
//
//            } catch (Exception ex) {
//                logger.error("向队列{}发送消息失败，消息为：{}", destination, msg);
//            }
                    //线程发送日志
        threadPoolTaskExecutor.execute(new SaveLogThread(log,logService));
//                Thread us=new Thread(new SaveLogThread(log,logService));
//                us.start();
        }



    /**
     *  异常通知 记录操作报错日志
     * @param joinPoint
     * @param e
     */
    @AfterThrowing(pointcut = "controllerAspect()", throwing = "e")
    public  void doAfterThrowing(JoinPoint joinPoint, Throwable e) {
        Log log = logThreadLocal.get();
        log.setType("error");
        log.setException(e.toString());
        new UpdateLogThread(log, logService).start();
    }

    /**
     * 获取注解中对方法的描述信息 用于service层注解
     * @param
     * @return discription
     */
    public static String getServiceMthodDescription(JoinPoint joinPoint) {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        SystemControllerLog serviceLog = method
                .getAnnotation(SystemControllerLog.class);
        String discription = serviceLog.description();
        return discription;
    }

    /**
     * 获取注解中对方法的描述信息 用于Controller层注解
     *
     * @param joinPoint 切点
     * @return discription
     */
    public static String getControllerMethodDescription(JoinPoint joinPoint) {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        SystemControllerLog controllerLog = method
                .getAnnotation(SystemControllerLog.class);
        String discription = controllerLog.description();
        return discription;
    }

    /**
     * 保存日志线程
     */
    private static class SaveLogThread implements Runnable {
        private Log log;
        private LogService logService;

        public SaveLogThread(Log log, LogService logService) {
            this.log = log;
            this.logService = logService;
        }

        @Override
        public void run() {
            logService.save(log);
        }
    }

    /**
     * 日志更新线程
     */
    private static class UpdateLogThread extends Thread {
        private Log log;
        private LogService logService;

        public UpdateLogThread(Log log, LogService logService) {
            super(UpdateLogThread.class.getSimpleName());
            this.log = log;
            this.logService = logService;
        }

        @Override
        public void run() {
            this.logService.update(log);
        }
    }
}
