package com.zhiwee.gree.webapi.Controller.Wxpay;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.wxpay.sdk.WXPay;
import com.zhiwee.gree.model.BaseInfo.BaseInfo;
import com.zhiwee.gree.model.FyGoods.FyOrder;
import com.zhiwee.gree.model.OrderInfo.Order;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.WxPay.WeChatConfig;
import com.zhiwee.gree.model.WxPay.WeChatParams;
import com.zhiwee.gree.model.WxPay.wxPayFuYang;
import com.zhiwee.gree.service.BaseInfo.BaseInfoService;
import com.zhiwee.gree.service.FyGoods.FyOrderService;
import com.zhiwee.gree.service.OrderInfo.OrderService;
import com.zhiwee.gree.service.ShopDistributor.DistributorOrderService;



import com.zhiwee.gree.webapi.util.*;

import org.apache.commons.collections.bag.SynchronizedSortedBag;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.annotation.SuppressAjWarnings;
import org.jdom.JDOMException;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.util.HttpUtils;
import xyz.icrab.common.util.KeyUtils;
import xyz.icrab.common.web.annotation.PermissionMode;
import org.slf4j.Logger;
import xyz.icrab.common.web.annotation.Session;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.apache.commons.httpclient.HttpConstants.getContentBytes;


/**
 * @author sun on 2019/5/15
 */
@RequestMapping("/wxPay")
@Controller
public class WxpayController {

    private static final Logger logger = LoggerFactory.getLogger(WxpayController.class);
    @Autowired
    private OrderService orderService;

    @Autowired
    FyOrderService fyOrderService;

    @Autowired
    private DistributorOrderService distributorOrderService;

    @Autowired
    private BaseInfoService baseInfoService;

/**
 * Description: pc 端返回二维码下单流程
 * @author: sun
 * @Date 上午10:42 2019/6/25
 * @param:
 * @return:
 */
    @RequestMapping("/pay")
    @SuppressWarnings("all")
    @ResponseBody
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> wxPay(@RequestBody WeChatParams ps) throws Exception {
        Order order = orderService.get(ps.getOut_trade_no());
        ps.setBody("安徽格力商品");
        ps.setTotal_fee(String.valueOf((int) (order.getRealPrice() * 100)));
        ps.setOut_trade_no(order.getId());
        ps.setAttach("xiner");
        ps.setMemberid("888");
        String   trade_type = WeChatConfig.scanPay;
        Map map = WeixinPay.getCodeUrl(ps,WeChatConfig.WECHAT_NOTIFYURL,trade_type,null);
        if(!"SUCCESS".equals((String) map.get("return_code"))){
            return Result.of(Status.ClientError.BAD_REQUEST, (String) map.get("return_msg"));
        }
        String urlCode = (String) map.get("code_url");
//        String generate = CodesUtils.generate(urlCode);
        System.out.println(urlCode);
        return Result.ok(urlCode);
    }


    /**
     * pc端微信支付之后的回调方法
     * @param request
     * @param response
     * @throws Exception
     */
    @PermissionMode(PermissionMode.Mode.White)
    @SuppressWarnings("all")
    @RequestMapping(value = "wechat_notifurl", method = RequestMethod.POST)
    public void wechat_notify_url_pc(HttpServletRequest request, HttpServletResponse response) throws Exception {

        //读取参数
        InputStream inputStream;
        StringBuffer sb = new StringBuffer();
        inputStream = request.getInputStream();
        String s;
        BufferedReader in = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
        while ((s = in.readLine()) != null) {
            sb.append(s);
        }
        in.close();
        inputStream.close();

        //解析xml成map
        Map<String, String> m = new HashMap<String, String>();
        m = XMLUtil.doXMLParse(sb.toString());

        //过滤空 设置 TreeMap
        SortedMap<Object, Object> packageParams = new TreeMap<Object, Object>();
        Iterator<String> it = m.keySet().iterator();
        while (it.hasNext()) {
            String parameter = it.next();
            String parameterValue = m.get(parameter);

            String v = "";
            if (null != parameterValue) {
                v = parameterValue.trim();
            }
            packageParams.put(parameter, v);
        }
        // 微信支付的API密钥
        String key = WeChatConfig.APIKEY; // key

//        lg.info("微信支付返回回来的参数："+packageParams);
        //判断签名是否正确
        if (PayForUtil.isTenpaySign("UTF-8", packageParams, key)) {
            //------------------------------
            //处理业务开始
            //------------------------------
            String resXml = "";
            if ("SUCCESS".equals((String) packageParams.get("result_code"))) {

                // 这里是支付成功
                //执行自己的业务逻辑开始
//                String app_id = (String)packageParams.get("appid");
//
//                String mch_id = (String)packageParams.get("mch_id");
//
//                String openid = (String)packageParams.get("openid");
//
//                String is_subscribe = (String)packageParams.get("is_subscribe");//是否关注公众号
//
//                //附加参数【商标申请_0bda32824db44d6f9611f1047829fa3b_15460】--【业务类型_会员ID_订单号】
//                String attach = (String)packageParams.get("attach");
                //商户订单号
                String out_trade_no = (String) packageParams.get("out_trade_no");
                //付款金额【以分为单位】
//                String total_fee = (String)packageParams.get("total_fee");
//                //微信生成的交易订单号
//                String transaction_id = (String)packageParams.get("transaction_id");//微信支付订单号
//                //支付完成时间
//                String time_end=(String)packageParams.get("time_end");

                Order order = orderService.get(out_trade_no);
                order.setPayTime(new Date());
                order.setOrderState(2);
                order.setPayType(2);
//                orderService.update(order);

//                System.out.println(app_id+""+mch_id+""+openid+""+is_subscribe+""+attach+""+out_trade_no+""+total_fee);
//                System.out.println(transaction_id+""+time_end);
//                System.out.println("支付成功");
//                lg.info("支付成功");
                //通知微信.异步确认成功.必写.不然会一直通知后台.八次之后就认为交易失败了.
                resXml = "<xml>" + "<return_code><![CDATA[SUCCESS]]></return_code>"
                        + "<return_msg><![CDATA[OK]]></return_msg>" + "</xml> ";

            } else {
//                lg.info("支付失败,错误信息：" + packageParams.get("err_code"));
                resXml = "<xml>" + "<return_code><![CDATA[FAIL]]></return_code>"
                        + "<return_msg><![CDATA[报文为空]]></return_msg>" + "</xml> ";
            }
            //------------------------------
            //处理业务完毕
            //------------------------------
            BufferedOutputStream out = new BufferedOutputStream(
                    response.getOutputStream());
            out.write(resXml.getBytes());
            out.flush();
            out.close();
        } else {
            System.out.println("支付失败");
        }

    }


    /**
     * Description: 微信订单退款的接口
     *
     * @author: sun
     * @Date 下午1:56 2019/5/17
     * @param:
     * @return:
     */
    @RequestMapping("/refund")
    @SuppressWarnings("all")
    @ResponseBody
    public Result<?> wxPayRefund(@RequestBody WeChatParams ps) {
      List<BaseInfo>  baseInfos  =  baseInfoService.list();
      if(!baseInfos.get(0).getRefundPassword().equals(ps.getRefundPassword())){
          return Result.of(Status.ClientError.BAD_REQUEST, "退款密码不正确");
      }
        String id = ps.getOut_trade_no();
        Order order = orderService.get(id);
        if (order.getOrderState() != 2 || order.getPayType() != 2 || (order.getRefundState() != 2 && order.getRefundState() != 8)) {
            return Result.of(Status.ClientError.BAD_REQUEST, "该订单未付款或者该订单不是微信支付或者改订单没有申请无法退款");
        }
        Double refundMoney = Double.valueOf(ps.getTotal_fee());
        if(refundMoney > order.getRealPrice()*100){
            return Result.of(Status.ClientError.BAD_REQUEST, "退款金额超过订单的金额");
        }
        if(refundMoney <= 0){
            return Result.of(Status.ClientError.BAD_REQUEST, "退款金额不能小于等于0");
        }
        Map map = ClientCustomSSL.setUrl(order.getId(),order.getRealPrice(), "",refundMoney,WeChatConfig.REFUND_URL);
        if (map.get("status").equals("SUCCESS")) {
            Map<String,Object> params = new HashMap<>();
            params.put("out_trade_no",(String) map.get("out_trade_no"));
            params.put("refundMoney",refundMoney);
//            Order orders = orderService.get((String) map.get("out_trade_no"));
//            orders.setOrderState(4);
//            orderService.updateOrders(params);
            return Result.ok("申请退款成功");
        } else {
            return Result.of(Status.ClientError.BAD_REQUEST, map.get("msg"));
        }

    }


    /**
     * Description: 微信支申请退款后的回调方法
     *
     * @author: sun
     * @Date 下午1:56 2019/5/17
     * @param:
     * @return:
     */
    @RequestMapping("/refundUrl")
    @SuppressWarnings("all")
    @ResponseBody
    public Result<?> wxPayRefundUrl(HttpServletRequest req) throws Exception {
        InputStream inputStream;
        StringBuffer sb = new StringBuffer();
        inputStream = req.getInputStream();
        String s;
        BufferedReader in = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
        while ((s = in.readLine()) != null) {
            sb.append(s);
        }
        in.close();
        inputStream.close();

        //解析xml成map
        Map<String, String> param = new HashMap<String, String>();
        param = XMLUtil.doXMLParse(sb.toString());
        System.out.println(param + "微信退款");
        if (param != null) {
            if (param.get("return_code").equals("SUCCESS")) {
                //退款成功，修改订单账号
                Order order = orderService.get(param.get("out_trade_no"));
                order.setOrderState(4);
                orderService.update(order);
                return Result.ok("申请退款成功");
            } else {
                return Result.of(Status.ClientError.BAD_REQUEST, param.get("return_msg"));
            }
        }

        return Result.of(Status.ClientError.BAD_REQUEST, "退款失败");

    }

    /**
     * Description: 微信发红包 根据code获取openId
     * @author: sun
     * @Date 下午1:25 2019/5/30
     * @param:
     * @return:
     */
    @RequestMapping("/cashVoucher")
    @PermissionMode(PermissionMode.Mode.White)
    @SuppressWarnings("all")
    @ResponseBody
    public Result<?> cashVoucher(@RequestBody Map<String, Object> param) throws Exception {
        String url = WeChatConfig.access_tokenUrL;
//        String appid = WeChatConfig.HNAPPID;
        String appid = WeChatConfig.APPID;
//        String secret = WeChatConfig.hnappSecret;
        String secret = WeChatConfig.appSecret;
        String code = (String) param.get("code");
        System.out.println("code:" + code);
        url = url.replace("AppId", appid).replace("AppSecret", secret).replace("CODE", code);
        System.out.println("url:" + url);
        String openIdText = HttpUtil.sendGet(url);
        ObjectMapper objectMapper = new ObjectMapper();
        Map map = objectMapper.readValue(openIdText, Map.class);
//        System.out.println(openIdText);
        String openid = "";
        if (map.get("openid") != null) {
            openid = map.get("openid").toString();
            System.out.println("孙凯wudi"+openid);
        }else{
            return Result.of(Status.ClientError.BAD_REQUEST, map.get("errmsg"));
        }
        Map params = ClientCustomSSL.cashVoucher("XXEFWRGSDCSDW12312DFEWFEWF", openid, 1.0);
        if(params.get("status").equals("SUCCESS")){
            return Result.ok(params.get("msg"));
        }else{
            return Result.of(Status.ClientError.BAD_REQUEST, params.get("msg"));
        }

    }







    /**
     * @author tzj
     * @description 阜阳小程序支付
     * @date 2019/12/25 12:17
    */
//    @PermissionMode(PermissionMode.Mode.White)
    @RequestMapping("/payFy")
    @ResponseBody
    @SuppressWarnings("all")
    public Result<?> payFy(HttpServletRequest  request, @RequestBody wxPayFuYang fuYang,@Session("ShopUser") ShopUser shopUser) throws Exception {
                    String openid =   shopUser.getOpenId();
                        FyOrder fyOrder=new FyOrder();
                        int money=1;
                        fyOrder.setId(KeyUtils.getKey());
                            //生成的随机字符串
                            String nonce_str = getRandomString(32);
                            //商品名称
                            String body = "test";
            //               = fuYang.getOpenId();
                            //获取客户端的ip地址
                            String spbill_create_ip = "127.0.0.1";
                            //用来测试标记
                 String today = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());



                 //组和生成签名
              SortedMap<String,String> packageParams = new TreeMap<>();


                //组装参数，用户生成统一下单接口的签名
                packageParams.put("appid", WeChatConfig.APPIDFuYang);
                packageParams.put("mch_id", WeChatConfig.MCHIDFuYang);
                packageParams.put("nonce_str", nonce_str);
                packageParams.put("body", body);
                //商户订单号,自己的订单ID
                packageParams.put("out_trade_no", "1504031011");//测试
              //  packageParams.put("out_trade_no", fyOrder.getId());
                //支付金额，这边需要转成字符串类型，否则后面的签名会失败
                packageParams.put("total_fee",String.valueOf(money));
                packageParams.put("spbill_create_ip", "127.0.0.1");
                //支付成功后的回调地址
                packageParams.put("notify_url", WeChatConfig.WECHAT_NOTIFYURLFuYang);
                //支付方式
                packageParams.put("trade_type", WeChatConfig.Pay);
                //用户的openID，自己获取
                packageParams.put("openid", openid);
                //加密类型
   //             packageParams.put("sign_type", "MD5");


        //sign签名
        String sign = WXPayUtil.createSign(packageParams, WeChatConfig.APIKEYFuYang);
        packageParams.put("sign",sign);



        //map转xml
        String payXml = WXPayUtil.mapToXml(packageParams);





        System.out.println(payXml);



        //统一下单
        String orderStr = HttpUtil.doPost(WeChatConfig.pay_url,payXml,4000);



        if(null == orderStr) {
            return null;
        }

        Map<String, String> unifiedOrderMap =  WXPayUtil.xmlToMap(orderStr);
        System.out.println(unifiedOrderMap.toString());

        Long timeStamp = System.currentTimeMillis()/ 1000;


      //  String stringSignTemp ="appId="+ WeChatConfig.APPIDFuYang +"&nonceStr="+ nonce_str +"&package=prepay_id="+unifiedOrderMap.get("prepay_id")+"&signType="+ WeChatConfig.SIGNTYPE +"&timeStamp="+ timeStamp;
        //再次签名，这个签名用于小程序端调用wx.requesetPayment方法
      //  String paySign2 = PayUtil.sign(stringSignTemp,WeChatConfig.APIKEYFuYang,"utf-8").toUpperCase();

        SortedMap<String,String> packageParams2 = new TreeMap<>();
        packageParams2.put("appId", WeChatConfig.APPIDFuYang);
        packageParams2.put("nonceStr", nonce_str);
        packageParams2.put("package","prepay_id="+unifiedOrderMap.get("prepay_id"));
        packageParams2.put("signType", WeChatConfig.SIGNTYPE);
        packageParams2.put("timeStamp",String.valueOf(timeStamp));
        String paySign2 = WXPayUtil.createSign(packageParams2, WeChatConfig.APIKEYFuYang);

        System.out.println("测试  "+paySign2);


       // signType="+ WeChatConfig.SIGNTYPE +"&timeStamp="+ timeStamp;


        //返回给移动端需要的参数
        Map <String,Object> response = new HashMap <String,Object>();

        response.put("nonceStr",nonce_str);
        response.put("package","prepay_id="+unifiedOrderMap.get("prepay_id"));
        response.put("timeStamp",timeStamp);
        response.put("paySign",paySign2);


        return  Result.ok(response);

        //
    }

    /*
    *
    *
    *
    *   String openid =   shopUser.getOpenId();
                        FyOrder fyOrder=new FyOrder();
                        int money=1;
                        fyOrder.setId(KeyUtils.getKey());
                            //生成的随机字符串
                            String nonce_str = getRandomString(32);
                            //商品名称
                            String body = "test";
            //               = fuYang.getOpenId();
                            //获取客户端的ip地址
                            String spbill_create_ip = "127.0.0.1";
                            //用来测试标记
                 String today = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());


                //组装参数，用户生成统一下单接口的签名

                Map <String, Object> packageParams = new HashMap<>();
                packageParams.put("appid", WeChatConfig.APPIDFuYang);
                packageParams.put("mch_id", WeChatConfig.MCHIDFuYang);
                packageParams.put("nonce_str", nonce_str);
                packageParams.put("body", body);
                //商户订单号,自己的订单ID
                packageParams.put("out_trade_no", fyOrder.getId());
                //支付金额，这边需要转成字符串类型，否则后面的签名会失败
                packageParams.put("total_fee", money);
                packageParams.put("spbill_create_ip", "127.0.0.1");
                //支付成功后的回调地址
                packageParams.put("notify_url", WeChatConfig.WECHAT_NOTIFYURLFuYang);
                //支付方式
                packageParams.put("trade_type", WeChatConfig.Pay);
                //用户的openID，自己获取
                packageParams.put("openid", openid);
                //加密类型
//                packageParams.put("sign_type", "MD5");




        packageParams = PayUtil.paraFilter(packageParams);
                // 把数组所有元素，按照“参数=参数值”的模式用“&”字符拼接成字符串 \
                String prestr = PayUtil.createLinkString(packageParams);
        logger.info("=======================第一次sTRING："+ prestr +"============ ======");
                //MD5运算生成签名，这里是第一次签名，用于调用统一下单接口
               String mysign = PayUtil.sign(prestr, WeChatConfig.APIKEYFuYang, "utf-8").toUpperCase();
               logger.info("=======================第一次签名："+ mysign +"============ ======");

                //拼接统一下单接口使用的xml数据，要将上一步生成的签名一起拼接进去
                String xml = "<xml>" +
                        "<appid>" + WeChatConfig.APPIDFuYang + "</appid>"
                        + "<body><![CDATA[" + body + "]]></body>"
                        + "<mch_id>" + WeChatConfig.MCHIDFuYang + "</mch_id>"
                        + "<nonce_str>" + nonce_str + "</nonce_str>"
                        + "<notify_url>" + WeChatConfig.WECHAT_NOTIFYURLFuYang +"</notify_url>"
                        + "<openid>" + openid +"</openid>"
                        + "<out_trade_no>" + fyOrder.getId() + "</out_trade_no>"
                        + "<spbill_create_ip>" + "127.0.0.1" + "</spbill_create_ip>"
                        + "<total_fee>" + money + "</total_fee>"
                        + "<trade_type>" + WeChatConfig.Pay +  "</trade_type>"
                        + "<sign>" + mysign + "</sign>"
                        + "</xml>";

                        logger.info("调试模式_统一下单接口 请求XML数据：" + xml);
                        //调用统一下单接口，并接受返回的结果
                        String res = PayUtil.httpRequest(WeChatConfig.pay_url, "POST", xml);
                        // 将解析结果存储在HashMap中
                        logger.info("返回数据为"+res);
                            Map map = XMLUtil.doXMLParse(res);
                        //返回状态码
                     String return_code = (String) map.get("return_code");

                    //返回给移动端需要的参数
                    Map <String,Object> response = new HashMap <String,Object>();
                    if("SUCCESS".equals(return_code)&& return_code.equals(return_code)){
                        //业务结果
                        //返回的预付单信息
                        String prepay_id =(String)map.get("prepay_id");
                        response.put("nonceStr",nonce_str);
                        //
                        response.put("package","prepay_id="+prepay_id);
                        Long timeStamp = System.currentTimeMillis()/ 1000;
                        response.put("timeStamp",timeStamp); //这边要将返回的时间戳转化成字符串，不然小程序端调用wx.requestPayment方法会报签名错误
                        String stringSignTemp ="appId="+ WeChatConfig.APPIDFuYang +"&nonceStr="+ nonce_str +"&package=prepay_id="+ prepay_id +"&signType="+ WeChatConfig.SIGNTYPE +"&timeStamp="+ timeStamp;
                        //再次签名，这个签名用于小程序端调用wx.requesetPayment方法
                        String paySign2 = PayUtil.sign(stringSignTemp,WeChatConfig.APIKEYFuYang,"utf-8").toUpperCase();
                        System.out.println("第三次签名字符串"+stringSignTemp);
                        logger.info("=======================第三次签名："+ paySign2 +"============ ======");
                        response.put("paySign",paySign2);
                        //更新订单信息
                        //业务逻辑代码
                            return Result.ok(response);
                    }

        return null;
    *
    *
    *
    *
    *
    *
    *
    *
    *
    *
    *
    * */





    /**
     * @author tzj
     * @description 小程序回调接口
     * @date 2019/12/25 20:10
     *
    */
    @SuppressWarnings("all")
    @RequestMapping(value="/wxNotify")
    @PermissionMode(PermissionMode.Mode.White)
    public synchronized void wxNotify(HttpServletRequest request,HttpServletResponse response) throws Exception{

        String orderId = null;

        InputStream inputStream =  request.getInputStream();

        try{
            BufferedReader br =  new BufferedReader(new InputStreamReader(inputStream,"UTF-8"));
            String line = null;
            StringBuilder sb = new StringBuilder();
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            br.close();
            inputStream.close();
            //sb为微信返回的xml
            String notityXml = sb.toString();
            String resXml = "";
            logger.info("接收到的报文：" + notityXml);
       // Map map = PayUtil.doXMLParse(notityXml);

        Map<String,String>  map = WXPayUtil.xmlToMap(notityXml);
            System.out.println("map信息"+map.toString());

            SortedMap<String,String> sortedMap = WXPayUtil.getSortedMap(map);
            System.out.println(sortedMap.toString());



         //   String returnCode = (String) map.get("return_code");

            //判断签名是否正确
            if(WXPayUtil.isCorrectSign(sortedMap,WeChatConfig.APIKEYFuYang)){
                if("SUCCESS".equals(sortedMap.get("result_code"))){
                      //添加自己的业务逻辑代码
                    response.setContentType("text/xml");
                    response.getWriter().println("success");
                    System.out.println("支付成功");
                    return;
                }
            }
            response.setContentType("text/xml");
            response.getWriter().println("fail");
            System.out.println("支付失败");
            logger.info("微信支付回调数据结束");

    } catch (Exception e) {
       logger.error("错误");
    }


    }




    /*String orderId = null;

        try{
        BufferedReader br = new BufferedReader(new InputStreamReader((ServletInputStream) request.getInputStream()));
        String line = null;
        StringBuilder sb = new StringBuilder();
        while ((line = br.readLine()) != null) {
            sb.append(line);
        }
        br.close();
        //sb为微信返回的xml
        String notityXml = sb.toString();
        String resXml = "";
        logger.info("接收到的报文：" + notityXml);
        Map map = PayUtil.doXMLParse(notityXml);

        String returnCode = (String) map.get("return_code");
        if("SUCCESS".equals(returnCode)){
            //验证签名是否正确
            Map<String, Object> validParams = PayUtil.paraFilter(map);  //回调验签时需要去除sign和空值参数
            String validStr = PayUtil.createLinkString(validParams);//把数组所有元素，按照“参数=参数值”的模式用“&”字符拼接成字符串
            String sign = PayUtil.sign(validStr, WeChatConfig.APIKEYFuYang, "utf-8").toUpperCase();//拼装生成服务器端验证的签名
            //根据微信官网的介绍，此处不仅对回调的参数进行验签，还需要对返回的金额与系统订单的金额进行比对等
            if(sign.equals(map.get("sign"))){
                *//**此处添加自己的业务逻辑代码start**//*
                //TODO

                *//**此处添加自己的业务逻辑代码end**//*
                //通知微信服务器已经支付成功
//                resXml = this.getXml();
            }

        }else{
//            resXml = this.getFailXml();
        }
        logger.info(resXml);
        logger.info("微信支付回调数据结束");


        BufferedOutputStream out = new BufferedOutputStream(
                response.getOutputStream());
        out.write(resXml.getBytes());
        out.flush();
        out.close();
    } catch (Exception e) {
        logger.error("错误");
    }*/




    /**
     * @Author: jick
     * @Date: 2020/1/3 14:24
     * 微信退款接口
     */
    @RequestMapping("/programRefund")
    @PermissionMode(PermissionMode.Mode.White)
    @ResponseBody
    public Result<?> wxPayRefu() {

        String nonce_str = getRandomString(32);

        //生成sign签名
        SortedMap<String,String>  parameters = new TreeMap<>();
        parameters.put("appid", WeChatConfig.APPIDFuYang);
        parameters.put("mch_id", WeChatConfig.MCHIDFuYang);
        parameters.put("nonce_str",nonce_str);
        parameters.put("sign_type","MD5");

        //这个是你要退款的流水号，你支付的时候微信返还给你的
      //  parameters.put("transaction_id", data.getTransaction_id());
        parameters.put("out_refund_no","1504031011");
        parameters.put("total_fee", "1");
        parameters.put("refund_fee","1");
        parameters.put("out_trade_no","1504031011");
       // parameters.put("refund_fee_type", data.getRefund_fee_type());
        //参数不知道什么意思的，对着官方给的API文档看看就清楚了

        String sign = WXPayUtil.createSign(parameters, WeChatConfig.APIKEYFuYang);  //工具类的方法,key就是平台上获取的

        parameters.put("sign", sign);

        RefundWxPayConfig refundWxPayConfig = new RefundWxPayConfig();
        Map<String, String>  returnMap  = null;
        WXPay2 wxPay = new WXPay2(refundWxPayConfig);
        try {
           returnMap = wxPay.refund(parameters);
            System.out.println(returnMap);


            //业务逻辑代码
            if("SUCCESS".equals(returnMap.get("result_code"))){
                programRefundDetai();
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

        return  Result.ok(returnMap);


    }


    /**
     * @Author: jick
     * @Date: 2020/1/3 18:11
     * 退款的业务逻辑代码
     */
    public  void    programRefundDetai(){
        /**
         * 退款的一些逻辑处理
         */

    }


    /*
    *
    *
    *
    *   FyOrder  fyOrder  = new FyOrder();
        fyOrder.setId("1504031011");
      //  fyOrder.setPayPrice(new);
        int monery =1;


        Map map = ClientCustomSSL.setUrl(fyOrder.getId(),monery, "",monery,WeChatConfig.REFUND_URL);
        if (map.get("status").equals("SUCCESS")) {
          //  Map<String,Object> params = new HashMap<>();
         //   params.put("out_trade_no",(String) map.get("out_trade_no"));
         //   params.put("refundMoney",refundMoney);
//            Order orders = orderService.get((String) map.get("out_trade_no"));
//            orders.setOrderState(4);
//            orderService.updateOrders(params);
            return Result.ok("申请退款成功");
        } else {
            return Result.of(Status.ClientError.BAD_REQUEST, map.get("msg"));
        }
    *
    *
    * */



    @RequestMapping("/refUrl")
    @ResponseBody
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> wxPayRefUrl(HttpServletRequest req) throws Exception {
        InputStream inputStream;
        StringBuffer sb = new StringBuffer();
        inputStream = req.getInputStream();
        String s;
        BufferedReader in = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
        while ((s = in.readLine()) != null) {
            sb.append(s);
        }
        in.close();
        inputStream.close();

        //解析xml成map
        Map<String, String> param = new HashMap<String, String>();
        param = XMLUtil.doXMLParse(sb.toString());
        System.out.println(param + "微信退款");
        if (param != null) {
            if (param.get("return_code").equals("SUCCESS")) {
                //退款成功，修改订单账号
               // Order order = orderService.get(param.get("out_trade_no"));
             //   order.setOrderState(4);
           //     orderService.update(order);
                return Result.ok("申请退款成功");
            } else {
                return Result.of(Status.ClientError.BAD_REQUEST, param.get("return_msg"));
            }
        }
        return Result.of(Status.ClientError.BAD_REQUEST, "退款失败");

    }






    /**
     * @author tzj
     * @description 获取用户IP地址
     * @date 2019/12/25 11:21
    */
    public static String getIpAddress(HttpServletRequest request) {
        // 避免反向代理不能获取真实地址, 取X-Forwarded-For中第一个非unknown的有效IP字符串
        String ip = request.getHeader("x-forwarded-for");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        return ip;
    }
    /**
     * 解析xml，取出其中的result_code，和prepay_id
     */
    public HashMap<String ,String> doXMLToMap(String results) {
        HashMap<String ,String> result = new HashMap<String ,String>();
        String return_code = results.substring(results.indexOf("<return_code><![CDATA[")+22, results.indexOf("]]></return_code>"));
        result.put("return_code", return_code);
        if(return_code.equals("SUCCESS")) {
            String prepay_id = results.substring(results.indexOf("<prepay_id><![CDATA[")+20, results.indexOf("]]></prepay_id>"));
            result.put("prepay_id", prepay_id);
        }
        return result;

    }


    /**
     *
     * requestUrl请求地址
     *  requestMethod请求方法
     *  outputStr参数
     */
    public static String httpRequest(String requestUrl, String requestMethod, String outputStr) {
        // 创建SSLContext
        StringBuffer buffer = null;
        try {
            URL url = new URL(requestUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod(requestMethod);
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.connect();
            // 往服务器端写内容
            if (null != outputStr) {
                OutputStream os = conn.getOutputStream();
                os.write(outputStr.getBytes("utf-8"));
                os.close();
            }
            // 读取服务器端返回的内容
            InputStream is = conn.getInputStream();
            InputStreamReader isr = new InputStreamReader(is, "utf-8");
            BufferedReader br = new BufferedReader(isr);
            buffer = new StringBuffer();
            String line = null;
            while ((line = br.readLine()) != null) {
                buffer.append(line);
            }
            br.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return buffer.toString();
    }





    //length用户要求产生字符串的长度
    public static String getRandomString(int length){
        String str="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        Random random=new Random();
        StringBuffer sb=new StringBuffer();
        for(int i=0;i<length;i++){
            int number=random.nextInt(62);
            sb.append(str.charAt(number));
        }
        return sb.toString();
    }


    /**
     * 签名字符串
     * @param
     * @param key 密钥
     * @return 签名结果
     */
    public static String sign(String text, String key, String input_charset) {
        text = text + "&key=" + key;
        return DigestUtils.md5DigestAsHex(getContentBytes(text, input_charset));
    }










}