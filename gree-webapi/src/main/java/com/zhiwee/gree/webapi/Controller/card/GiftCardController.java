package com.zhiwee.gree.webapi.Controller.card;


import com.zhiwee.gree.model.User;
import com.zhiwee.gree.model.card.BookingCard;
import com.zhiwee.gree.model.card.GiftCard;
import com.zhiwee.gree.service.card.BookingCardService;
import com.zhiwee.gree.service.card.BookingCardsService;
import com.zhiwee.gree.service.card.GiftCardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.util.KeyUtils;
import xyz.icrab.common.web.annotation.EnableGetRequest;
import xyz.icrab.common.web.annotation.EnableListRequest;
import xyz.icrab.common.web.annotation.Session;
import xyz.icrab.ums.util.consts.SessionKeys;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author DELL
 */
@RequestMapping("/giftCard")
@Controller
@ResponseBody
@EnableListRequest
@EnableGetRequest
public class GiftCardController {
    @Resource
    private GiftCardService giftCardService;

    /**
     * @return
     * @author cs
     * @description 预售卡页面
     * @date 2020/02/13 12:55
     */
    @RequestMapping(value = "/infoPage")
    public Result<?> infoPage(@RequestBody Map<String, Object> params, Pagination pagination) {
        if (params == null) {
            params = new HashMap<>();
        }
        Pageable<GiftCard> page = giftCardService.pageInfo(params, pagination);
        return Result.ok(page);
    }

    @RequestMapping("/delete")
    @ResponseBody
    public Result<?> delete(@RequestBody GiftCard bookingCard) {
        bookingCard.setState(0);
        giftCardService.update(bookingCard);
        return Result.ok();
    }

    @RequestMapping(value = "/add")
    public Result<?> add(@Session(SessionKeys.USER) User user, @RequestBody(required = false) GiftCard cardInfo) {
        Integer numbers = cardInfo.getNumbers();
        if (numbers!=1) {
            if (numbers <= 0 || (numbers % 2 != 0)) {
                return Result.of(Status.ClientError.BAD_REQUEST, "可以购买次数必须是正整数!");
            }
        }
        cardInfo.setId(KeyUtils.getUUID());
        cardInfo.setCreatetime(new Date());
        cardInfo.setCreateman(user.getId());
        cardInfo.setState(1);
        giftCardService.save(cardInfo);
        return Result.ok();
    }

    @RequestMapping(value = "/get")
    public Result<?> get(@Session(SessionKeys.USER) User user, @RequestBody(required = false) GiftCard cardInfo) {
        GiftCard one = giftCardService.getInfo(cardInfo);
        return Result.ok(one);
    }

    @RequestMapping(value = "/update")
    @SuppressWarnings("all")
    public Result<?> update(@Session(SessionKeys.USER) User user, @RequestBody(required = false) GiftCard cardInfo) {
        cardInfo.setUpdatetime(new Date());
        cardInfo.setUpdateman(user.getId());
        giftCardService.update(cardInfo);
        return Result.ok();
    }

    /**
     * @author cs
     * @description 小程序获取预售卡信息
     * @date 2020/02/14 13:24
     */
    @RequestMapping(value = "/cardlist")
    public Result<?> cardList() {
        Map<String, Object> map = new HashMap<>();
        map.put("state",1);
        return Result.ok(giftCardService.list(map));
    }

}
