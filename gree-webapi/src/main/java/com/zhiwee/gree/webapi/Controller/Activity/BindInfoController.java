package com.zhiwee.gree.webapi.Controller.Activity;

import com.zhiwee.gree.model.Activity.BindInfo;
import com.zhiwee.gree.service.Activity.ActivityInfoService;
import com.zhiwee.gree.service.Activity.BindInfoService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.util.KeyUtils;
import xyz.icrab.common.web.annotation.EnableListRequest;
import xyz.icrab.common.web.annotation.EnablePageRequest;
import xyz.icrab.common.web.annotation.PermissionMode;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author DELL
 */
@Controller
@RequestMapping("/bindInfo")
@EnablePageRequest
@EnableListRequest
public class BindInfoController {

    @Autowired
    private BindInfoService bindInfoService;

    @Autowired
    private ActivityInfoService activityInfoService;

    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * @author tzj
     * @description 信息绑定
     * @date 2020/1/15 17:05
     */
    @RequestMapping("/add")
    @ResponseBody
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> add(@RequestBody BindInfo bindInfo) {
        //校验手机验证码
        String key = getCaptchaKey(bindInfo.getPhone());
        String captcha = String.valueOf(redisTemplate.opsForValue().get(key));
        if (StringUtils.isBlank(captcha) || !StringUtils.equals(captcha, bindInfo.getYzm())) {
            return Result.of(Status.ClientError.FORBIDDEN, "验证码错误或者失效");
        }

        //校验当前活动是否绑定过信息
        String phone = bindInfo.getPhone();
        Map<String, Object> param = new HashMap<>(10);
        param.put("phone", phone);
        List<BindInfo> list = bindInfoService.list(param);
        if (!list.isEmpty()) {
            return Result.of(Status.ClientError.BAD_REQUEST, "该用户在当前活动中已经绑定了推荐人!");
        }
        bindInfo.setId(KeyUtils.getKey());
        bindInfo.setCreatetime(new Date());
        bindInfoService.save(bindInfo);
        return Result.ok();
    }
    private String getCaptchaKey(String mobile) {
        return "captcha:phone:active:" + mobile;
    }


    /**
     * @author tzj
     * @description 找到绑定的卡号
     * @date 2020/1/16 9:21
     */
    @PostMapping("/getCardCode")
    @ResponseBody
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> getCardCode(HttpServletRequest request, @RequestBody Map<String, Object> params) {
        //获取电话号码
        String phone = (String) params.get("phone");
        if (StringUtils.isBlank(phone)) {
            return Result.of(Status.ClientError.BAD_REQUEST, "客户号码为空");
        }
        //找到当前活动
//        Map<String, Object> param = new HashMap<>(10);
//        param.put("time", new Date());
//        ActivityInfo activityInfo;
//        activityInfo = activityInfoService.getCurrent(param);
//        if (activityInfo == null) {
//            return Result.of(Status.ClientError.BAD_REQUEST, "目前无活动信息");
//        }
        //扎到绑定信息返货推荐号
        BindInfo bindInfo = new BindInfo();
//        bindInfo.setActivityId(activityInfo.getId());
        bindInfo.setPhone(phone);
        bindInfo = bindInfoService.getOne(bindInfo);
        if (bindInfo != null) {
            return Result.ok(bindInfo);
        } else {
            return Result.ok();
        }
    }


}
