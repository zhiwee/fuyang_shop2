package com.zhiwee.gree.webapi.Controller.HomePage;

import com.google.common.io.Files;
import com.zhiwee.gree.model.HomePage.HomeContent;
import com.zhiwee.gree.model.HomePage.HomePage;
import com.zhiwee.gree.model.HomePage.PcPlanmap;
import com.zhiwee.gree.model.User;
import com.zhiwee.gree.service.HomePage.HomePageService;
import com.zhiwee.gree.service.HomePage.PcPlanmapService;
import com.zhiwee.gree.webapi.util.SystemControllerLog;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.*;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.util.IdGenerator;
import xyz.icrab.common.util.KeyUtils;
import xyz.icrab.common.web.annotation.*;
import net.coobird.thumbnailator.Thumbnails;
import xyz.icrab.common.web.param.IdParam;
import org.springframework.web.multipart.MultipartFile;
import xyz.icrab.common.web.param.IdsParam;
import xyz.icrab.common.web.util.RequestUtils;
import xyz.icrab.common.web.util.Validator;
import xyz.icrab.ums.util.consts.SessionKeys;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author sun on 2019/5/6
 */
@RestController
@RequestMapping("/homePage")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class HomePageController {
       @Autowired
       private HomePageService homePageService;

       @Autowired
       private PcPlanmapService pcPlanmapService;

    @Autowired
    private RedisTemplate redisTemplate;
    @Value("HOMEPAGE_CONTENT")
    private String homePage_content;

    @Value("HOMEPHONEPAGE_CONTENT")
    private String homePhonePage_content;

    @Value("${file.upload.dir}")
    private String fileUploadDir;

    @Value("${HTTP_PICTUREURL}")
    private String httpurl;


    /**
     * Description: 获取pc首页的信息
     * @author: sun
     * @Date 下午12:51 2019/5/6
     * @param:
     * @return:
     */
    @PermissionMode(PermissionMode.Mode.White)
    @RequestMapping(value = "/homePage")
    public Result<?> searchHomePage(){
        List<HomePage> homePage = (List)redisTemplate.opsForValue().get(homePage_content);
         if(homePage != null && homePage.size() > 0 ){
             return Result.ok(homePage);
         }
         Map<String,Object> params = new HashMap<>();
        params.put("type",1);
        params.put("state",1);
        List<HomePage> homePageList = homePageService.list(params);
       redisTemplate.opsForValue().set(homePage_content,homePageList);
        return Result.ok(homePage);
    }



/**
 * Description: 获取手机端的轮播图
 * @author: sun
 * @Date 下午12:46 2019/6/20
 * @param:
 * @return:
 */
    @PermissionMode(PermissionMode.Mode.White)
    @RequestMapping(value = "/homePhonePage")
    public Result<?> homePhonePage(){

        List<HomePage> homePage = (List)redisTemplate.opsForValue().get(homePhonePage_content);
        if(homePage != null){
            return Result.ok(homePage);
        }
        Map<String,Object> params = new HashMap<>();
        params.put("type",2);
        params.put("state",1);
        List<HomePage> homePageList = homePageService.list(params);

        redisTemplate.opsForValue().set(homePhonePage_content,homePageList);
        return Result.ok(homePageList);
    }
    /**
     * Description: 添加首页信息
     * @author: sun
     * @Date 下午12:51 2019/5/6
     * @param:
     * @return:
     */

    @RequestMapping(value = "/addHomePage")
    public Result<?> addHomePage( @RequestBody HomePage homePage, @Session(SessionKeys.USER) User user)  {
        Validator validator = new Validator();
        validator.notEmpty(homePage.getName(), "名称不能为空");
        validator.notNull(homePage.getType(), "类型不能为空");
        validator.notNull(homePage.getPicture(), "请上传图片不能为空");
        validator.notNull(homePage.getSort(), "排序不能为空");
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }
        homePage.setId(IdGenerator.objectId());
        homePage.setState(1);
        homePage.setCreateMan(user.getId());
        homePage.setCreatetime(new Date());
        homePageService.save(homePage);
        redisTemplate.delete(homePage_content);
        redisTemplate.delete(homePhonePage_content);
        return Result.ok();
    }



    /**
     * Description: 更新首页信息
     * @author: sun
     * @Date 下午12:51 2019/5/6
     * @param:
     * @return:
     */
    @RequestMapping(value = "/updateHomePage")
    public Result<?> updateHomePage(@RequestBody HomePage homePage, @Session(SessionKeys.USER) User user)  {
        Validator validator = new Validator();
        validator.notEmpty(homePage.getId(), "请重新选择");
        validator.notEmpty(homePage.getName(), "名称不能为空");
        validator.notNull(homePage.getType(), "类型不能为空");
        validator.notNull(homePage.getPicture(), "请上传图片不能为空");
        validator.notNull(homePage.getSort(), "排序不能为空");
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }
        homePage.setUpdateMan(user.getId());
        homePage.setUpdatetime(new Date());
        homePageService.update(homePage);
        redisTemplate.delete(homePage_content);
        redisTemplate.delete(homePhonePage_content);
        return Result.ok();
    }



    /**
     * Description: 后台查询商城首页信息
     * @author: sun
     * @Date 下午12:51 2019/5/6
     * @param:
     * @return:
     */

    @RequestMapping(value = "/achieveHomePage")
    public Result<?> achieveHomePage(@RequestBody(required = false) Map<String, Object> params,Pagination pagination){
        Pageable<HomePage> homePageList = homePageService.achieveHomePage(params, pagination);
        return Result.ok(homePageList);
    }
    /**
     * Description: PC轮播图  周广
     * @author: sun
     * @Date 下午12:51 2019/5/6
     * @param:
     * @return:
     */
    @PermissionMode(PermissionMode.Mode.White)
    @RequestMapping(value = "/getPlanmapList")
    public Result<?> searchPcPlanmap(){
        Map<String,Object> params = new HashMap<>();
        params.put("state",1);
        SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String time=sdf.format(new Date());
        params.put("time",time);
        List<PcPlanmap> homePage = pcPlanmapService.getList(params);
        return Result.ok(homePage);
    }

    /**
     * Description: 轮播图  分页  周广
     * @param:
     * @return:
     */

    @RequestMapping(value = "/RplanMapPage")
    public Result<?> RplanMapPage(@RequestBody(required = false) Map<String, Object> params,Pagination pagination){
        if (params == null) {
            params = new HashMap<>();
        }

       // Pageable<PcPlanmap> pageable = homePageService.getPage(params, pagination);
        Pageable<PcPlanmap> pcPage = pcPlanmapService.getPage(params, pagination);
        return Result.ok(pcPage);
    }

    /**
     * 轮播图  添加 周广
     * @param pcPlanmap
     * @return
     */
    @RequestMapping(value = "/addPlanmap")
    public Result<?> addPlanmap(@RequestBody PcPlanmap pcPlanmap){
        pcPlanmap.setId(KeyUtils.getKey());
        pcPlanmap.setType(1);
        pcPlanmapService.save(pcPlanmap);
        return Result.ok();
    }

    /**
     * 轮播图  修改  周广
     * @param pcPlanmap
     * @return
     */
    @RequestMapping(value = "/updatePlanmap")
    public Result<?> updatePlanmap(@RequestBody PcPlanmap pcPlanmap){
        pcPlanmapService.update(pcPlanmap);
        return Result.ok();
    }


    /***
     * 轮播图 查询单个  周广
     * @param param
     * @return
     */
    @RequestMapping("/selectPlanmap")
    @ResponseBody
    public Result<?> selectPlanmap(@RequestBody IdParam param){
        return Result.ok(pcPlanmapService.getById(param.getId()));
    }



    /***
     * 轮播图  启用  周广
     * @param param
     * @return
     */
    @RequestMapping("/enablePlanmap")
    @ResponseBody
    public Result<?> enablePlanmap(@RequestBody @Valid IdsParam param){
        for (String id : param.getIds()) {
            PcPlanmap pcPlanmap=new PcPlanmap();
            pcPlanmap.setId(id);
            pcPlanmap.setState(1);
            pcPlanmapService.update(pcPlanmap);
        }
        return Result.ok();
    }

    /***
     * 轮播图  禁用  周广
     * @param param
     * @return
     */
    @RequestMapping("/disablePlanmap")
    @ResponseBody
    public Result<?> disablePlanmap(@RequestBody @Valid IdsParam param){
        for (String id : param.getIds()) {
            PcPlanmap pcPlanmap=new PcPlanmap();
            pcPlanmap.setId(id);
            pcPlanmap.setState(2);
            pcPlanmapService.update(pcPlanmap);
        }
        return Result.ok();
    }


    /***
     * 轮播图  删除 周广
     * @param param
     * @return
     */
    @RequestMapping("/deletePlanmap")
    @ResponseBody
    public Result<?> deletePlanmap(@RequestBody @Valid IdsParam param){
        for (String id : param.getIds()) {
            PcPlanmap pcPlanmap=new PcPlanmap();
            pcPlanmap.setId(id);
            pcPlanmapService.delete(pcPlanmap);
        }
        return Result.ok();
    }




/**
 * Description: 启用首页信息
 * @author: sun
 * @Date 下午2:00 2019/5/7
 * @param:
 * @return:
 */

    @RequestMapping("/open")
    public  Result<?> open(@RequestBody @Valid IdParam map) {
        HomePage homePage = new HomePage();
        homePage.setId(map.getId());
        homePage.setState(1);
        homePageService.update(homePage);
        redisTemplate.delete(homePage_content);
        redisTemplate.delete(homePhonePage_content);
        return Result.ok();
    }

    /**
     * Description: 禁用首页信息
     * @author: sun
     * @Date 下午2:00 2019/5/7
     * @param:
     * @return:
     */

    @RequestMapping("/stop")
    public  Result<?> stop(@RequestBody @Valid IdParam map) {
        HomePage homePage = new HomePage();
        homePage.setId(map.getId());
        homePage.setState(2);
        homePageService.update(homePage);
        redisTemplate.delete(homePage_content);
        redisTemplate.delete(homePhonePage_content);
        return Result.ok();
    }


    /**
     * Description: 显示首页详情
     * @author: sun
     * @Date 下午2:00 2019/5/7
     * @param:
     * @return:
     */

    @RequestMapping("/selectOne")
    public  Result<?> selectOne(@RequestBody @Valid IdParam map) {
        HomePage homePage = homePageService.get(map.getId());
        return Result.ok(homePage);
    }



/**
 * Description: 添加首页信息
 * @author: sun
 * @Date 下午3:55 2019/5/7
 * @param:
 * @return:
 */
    @PostMapping("/upload")
    public Result<?> upload(@RequestPart("file") MultipartFile file, HttpServletRequest req) throws Exception {
        Map param = RequestUtils.getParameters(req);
        String fileId = (String) param.get("uid");
        // 获取文件名
        String suffix = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".") + 1);
        String id = KeyUtils.getKey();
        // 获取项目的路径 + 拼接得到文件要保存的位置
        String filePath = fileUploadDir+"homepage/" + id+ "." + suffix;
        // 创建一个文件的对象
        File file1 = new File(filePath);
        // 创建父文件夹

        Files.createParentDirs(file1);

        try {
            // 先尝试压缩并保存图片
            Thumbnails.of(file.getInputStream()).scale(1f).outputQuality(0.25f).toFile(file1);
        } catch (IOException e) {
            try {
                // 失败了再用springmvc自带的方式
                file.transferTo(file1);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
        Runtime.getRuntime().exec("chmod 777 " + filePath);
//        file.transferTo(file1);
        return Result.ok(httpurl+"homepage/" + id+ "." + suffix);
    }
}
