package com.zhiwee.gree.webapi.Controller.fySkill;

import com.zhiwee.gree.model.FyGoods.FyOrder;
import com.zhiwee.gree.model.FyGoods.FyOrderItem;
import com.zhiwee.gree.model.FyGoods.Fygoods;
import com.zhiwee.gree.model.FyGoods.FygoodsCart;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.resultBase.BaseResult;
import com.zhiwee.gree.service.FyGoods.FyOrderItemService;
import com.zhiwee.gree.service.FyGoods.FyOrderService;
import com.zhiwee.gree.service.FyGoods.FygoodsService;
import com.zhiwee.gree.webapi.util.DateUtil;
import com.zhiwee.gree.webapi.util.JsonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import sun.security.pkcs11.P11Util;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.util.IdGenerator;
import xyz.icrab.common.web.annotation.*;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@RestController
@RequestMapping("/luckDraw")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class LuckDrawControler {


    @Autowired
    private FygoodsService    fygoodsService;


    @Autowired
    private  FyOrderItemService   fyOrderItemService;


    @Autowired
    private FyOrderService  fyOrderService;


    @Autowired
    private RedisTemplate<String,String > redisTemplate;





    /**
     * @Author: jick
     * @Date: 2019/12/22 15:28
     * 抽奖，抽取秒杀商品，同时生成订单信息
     */
     @RequestMapping("/getLuckGoods")
   //  @PermissionMode(PermissionMode.Mode.White)
    public   Result<?>  getLuckGoodsController(@Session("ShopUser") ShopUser shopUser){

    /* public   Result<?>  getLuckGoodsController(){

         ShopUser shopUser  = new ShopUser();
         shopUser.setId("1504031011");
         shopUser.setPhone("18726053785");
         shopUser.setNickname("张三");*/

         Lock  lock = new ReentrantLock();

         try {

             //加锁
             lock.lock();

             //查询库存大于0的秒杀商品
             Map<String,Object> param =  new HashMap<>();
             param.put("storeCount",0);
             param.put("iskill",1);
             param.put("state",1);
             List<Fygoods> list  =    fygoodsService.queryList(param);

             //如果库存为0
             if(list.size()==0){
                 return  Result.of("211","",null);
             }


             //生成随机数，领取对应索引的商品
             Random  r  = new Random();
            // int  i = r.nextInt(list.size());
             Fygoods  fygoods =  list.get(r.nextInt(list.size()));



             HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();

             FyOrder fyOrder =    new FyOrder();

             String  goodsInfoStr = "获奖商品：";

             Date date  = new Date();

             if(null ==shopUser.getPhone() || shopUser.getPhone().trim() ==""){
                 return   Result.of("207","请填写手机号等基本信息",null);
             }




                     //生成订单信息
                 //将商品详情拼接成一个字符串
                 goodsInfoStr +=fygoods.getGoodsName()+":"+fygoods.getKillPrice()+"*"+1+",";

                 FyOrderItem fyOrderItem  =  new FyOrderItem();

                 fyOrderItem.setId(IdGenerator.uuid());
                 fyOrderItem.setGoodsName(fygoods.getGoodsName());
                 fyOrderItem.setGoodsCover(fygoods.getCover());
                 fyOrderItem.setOrderNum(DateUtil.date2Str(date));
                 fyOrderItem.setSinglePrice(fygoods.getKillPrice());
                 fyOrderItem.setCount(1);
                 fyOrderItem.setDraw("抽奖订单");
                 fyOrderItem.setPayPrice(fygoods.getKillPrice().multiply(new BigDecimal(1)));

                /* //存入redis
                 Map<String,String> itemMap  = new HashMap<>();
                 itemMap.put(fyOrderItem.getId(), JsonUtil.object2JsonStr(fyOrderItem));
                 hashOperations.putAll("item:"+ DateUtil.date2Str(date),itemMap);
                 //设置时间订单有效时间为一天
                 redisTemplate.expire("item:"+ DateUtil.date2Str(date),7, TimeUnit.DAYS);
*/
                //订单信息信息存入数据库
                  fyOrderItemService.save(fyOrderItem);
                 //改变库存数目
                 fygoods.setStoreCount(fygoods.getStoreCount()-1);
                 fygoodsService.update(fygoods);



             //订单基本信息

             //实体类封装
             fyOrder.setId(IdGenerator.uuid());
             fyOrder.setOrderNum(DateUtil.date2Str(date));
             fyOrder.setUserName(shopUser.getNickname());
             fyOrder.setUserId(shopUser.getId());
             fyOrder.setPhoneNum(shopUser.getPhone());
             fyOrder.setAllPrice(fygoods.getKillPrice().multiply(new BigDecimal(1)));
             fyOrder.setDiscount(new BigDecimal(0));
             //实际支付金额，总金额减去折扣
             fyOrder.setPayPrice(fyOrder.getAllPrice().subtract(fyOrder.getDiscount()));
             fyOrder.setGoodsBody(goodsInfoStr);
             fyOrder.setOrderTime(date);
             fyOrder.setIsdelete(1);//未删除状态

             //下单成功，是未支付状态
             fyOrder.setState(2);

             /*//订单信息存入redis；
             Map<String,String>  orderMap  = new HashMap<>();
             orderMap.put(fyOrder.getOrderNum(), JsonUtil.object2JsonStr(fyOrder));


             hashOperations.putAll("order:"+shopUser.getPhone(),orderMap);
             //设置未支付订单存活时间为7天
             redisTemplate.expire("order:"+shopUser.getPhone(),7,TimeUnit.DAYS);
             */

             //订单信息存入数据库
              fyOrderService.save(fyOrder);


             return  Result.of("200","恭喜你抽取到奖品："+fygoods.getGoodsName()+"，请找导购员结算",null);

         }finally {
             //释放锁
             lock.unlock();
         }


    }


}
