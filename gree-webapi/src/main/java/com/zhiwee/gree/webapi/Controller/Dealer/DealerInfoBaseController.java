package com.zhiwee.gree.webapi.Controller.Dealer;


import com.zhiwee.gree.model.Dealer.DealerInfoBase;
import com.zhiwee.gree.model.ShopDistributor.ShopDistributor;
import com.zhiwee.gree.service.Dealer.DealerInfoBaseService;

import com.zhiwee.gree.service.ShopDistributor.DistributorGradeService;
import com.zhiwee.gree.service.ShopDistributor.ShopDistributorService;
import com.zhiwee.gree.webapi.util.SystemControllerLog;
import org.apache.commons.collections4.CollectionUtils;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import xyz.icrab.common.model.*;
import xyz.icrab.common.model.enums.YesOrNo;
import xyz.icrab.common.util.IdGenerator;

import xyz.icrab.common.web.annotation.EnableGetRequest;
import xyz.icrab.common.web.annotation.EnableListRequest;
import xyz.icrab.common.web.annotation.PermissionMode;

import xyz.icrab.common.web.param.IdParam;
import xyz.icrab.common.web.param.IdsParam;


import javax.annotation.Resource;

import javax.validation.Valid;

import java.util.*;

@RequestMapping("/dealerInfoBase")
@Controller
@ResponseBody
@EnableListRequest
@EnableGetRequest
public class DealerInfoBaseController {
    @Resource
    private DealerInfoBaseService dealerInfoBaseService;

    @Autowired
    private ShopDistributorService shopDistributorService;

    @Autowired
    private DistributorGradeService distributorGradeService;

    /**
     * Description: 获取所有活动系统的门店信息
     * @author: sun
     * @Date 下午3:19 2019/5/21
     * @param:
     * @return:
     */
    @RequestMapping("/infoPage")
    public Result<?> pageDealer(@RequestBody Map<String, Object> param, Pagination pagination) {
        if (param == null) {
            param = new HashMap<>();
        }
        Pageable<DealerInfoBase> pageable = dealerInfoBaseService.infoPage(param, pagination);
        return Result.ok(pageable);
    }


/**
 * Description: 获取活动系统所有门店的树形结构
 * @author: sun
 * @Date 下午3:18 2019/5/21
 * @param:
 * @return:
 */

    @RequestMapping("/simpleTree")
    public Result<?> simpleTree(@RequestBody(required = false) Map<String, Object> param) {
        if (param == null) {
            param = new HashMap<>();
        }
//        param.put("state", new Object[]{1, 2});
        List<DealerInfoBase> list = dealerInfoBaseService.simpleList(param);
        List<Tree<DealerInfoBase>> trees = new ArrayList<>(list.size());
        for (DealerInfoBase record : list) {
            DealerInfoBase rs = new DealerInfoBase();
            rs.setIdPath(record.getIdPath());
            Tree<DealerInfoBase> tree = new Tree<>(record.getId(), record.getName(), record.getParentId(), rs);
            trees.add(tree);
        }
        if (trees.size() == 0) {
            Tree<DealerInfoBase> tree = new Tree<>("", "全部门店", "", null);
            trees.add(tree);
        }
        return Result.ok(trees);
    }



    /**
     * 获取子级（不包括自身）
     * @param param
     * @return
     */

    @RequestMapping("/getChildren")
    public Result getChildren(@RequestBody Map<String, Object> param ){
    	String parentid = (String)param.get("id");
        param.clear();
        param.put("parentId", parentid);
//        param.put("state", YesOrNo.YES.value());
        List<DealerInfoBase> childrenList = dealerInfoBaseService.list(param);
        return Result.ok(childrenList);
    }

    /**
     * 获取自身以及自身的子级
     *
     * @param param
     * @return
     */
    @RequestMapping("/getWithChildren")
    @PermissionMode(PermissionMode.Mode.White)
    public Result getWithChildren(@RequestBody @Valid IdParam param) {
        DealerInfoBase dealerInfo = dealerInfoBaseService.get(param.getId());
        if (dealerInfo != null && dealerInfo.getState() == YesOrNo.YES) {
            //查询直接子级
            Map<String, Object> params = new HashMap<>();
            params.put("idPath", dealerInfo.getId());
//            params.put("state", YesOrNo.YES.value());
            List<DealerInfoBase> childrenList = dealerInfoBaseService.list(params);
            if (CollectionUtils.isNotEmpty(childrenList)) {
                Iterator<DealerInfoBase> iter = childrenList.iterator();
                Map<String, DealerInfoBase> map = new LinkedHashMap<>();
                Set<String> forDeletedIds = new HashSet<>();
                while (iter.hasNext()) {
                    DealerInfoBase next = iter.next();
                    if (dealerInfo.getId().equals(next.getId())) {
                        iter.remove();
                    } else {
                        map.put(next.getId(), next);
                        if (!dealerInfo.getId().equals(next.getParentId())) {
                            forDeletedIds.add(next.getParentId());
                        }
                    }
                }
                if (forDeletedIds.size() > 0) {
                    for (String id : forDeletedIds) {
                        map.remove(id);
                    }
                }
                dealerInfo.setChildrenList(new ArrayList<>(map.values()));
            } else {
                dealerInfo.setChildrenList(childrenList);
            }
        }
        return Result.ok(dealerInfo);
    }



    /**
     * Description:获取所有门店
     * @author: sun
     * @Date 下午3:20 2019/5/21
     * @param:
     * @return:
     */
    @RequestMapping("/page")
    public Result<?> page(@RequestBody Map<String, Object> param, Pagination pagination) {
        if (param == null) {
            param = new HashMap<>();
        }
        Pageable<DealerInfoBase> pageable = dealerInfoBaseService.infoPage(param, pagination);
        return Result.ok(pageable);
    }

    /**
     * Description:将销售系统的门店加入配送商
     * @author: sun
     * @Date 下午3:20 2019/5/21
     * @param:
     * @return:
     */
    @RequestMapping("/disable")
    public Result<?> disable(@RequestBody IdsParam param) {
        List<String> list = param.getIds();
        ShopDistributor shopDistributor = new ShopDistributor();
        DealerInfoBase dealer = null;
        for(String id : list){
            dealer = dealerInfoBaseService.get(id);
            shopDistributor.setId(IdGenerator.objectId());
            shopDistributor.setDealerId(dealer.getId());
            shopDistributor.setDealerName(dealer.getName());
            shopDistributor.setPhone(dealer.getMobile());
            shopDistributor.setManagerName(dealer.getName());
            //1 门店配送商 2 非门店配送商
            shopDistributor.setType(1);
            //1 启用 2 禁用
            shopDistributor.setState(1);

            shopDistributor.setDefaultDistributor(2);
            shopDistributor.setCreateTime(new Date());
            DealerInfoBase dealerInfo = new DealerInfoBase();
            dealerInfo.setId(id);
            dealerInfo.setIsShop(1);
            shopDistributorService.saveAndUpdate(shopDistributor,dealerInfo);

        }
        return Result.ok();
    }
/**
 * Description: 将门店从配送商中去除
 * @author: sun
 * @Date 下午3:26 2019/5/21
 * @param:
 * @return:
 */
    @SystemControllerLog(description = "将门店从配送商中去除")
    @RequestMapping("/enable")
    public Result<?> enable(@RequestBody IdsParam param) {
        List<String> list = param.getIds();
        for(String id : list){
            Map<String,Object> params = new HashMap<>();
            params.put("dealerId",id);
            ShopDistributor shopDistributor = shopDistributorService.getOne(params);
            shopDistributor.setUpdateTime(new Date());
            shopDistributor.setState(2);
            DealerInfoBase dealerInfo = new DealerInfoBase();
            dealerInfo.setId(id);
            dealerInfo.setIsShop(2);
            shopDistributorService.updateDealerInfo(shopDistributor,dealerInfo);
        }
        return Result.ok();
    }


}
