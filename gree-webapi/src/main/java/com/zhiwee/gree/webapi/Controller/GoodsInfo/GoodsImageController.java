package com.zhiwee.gree.webapi.Controller.GoodsInfo;

import com.zhiwee.gree.model.GoodsInfo.GoodsBand;
import com.zhiwee.gree.model.GoodsInfo.GoodsImage;
import com.zhiwee.gree.model.resultBase.FileResult;
import com.zhiwee.gree.service.GoodsInfo.GoodsImageService;
import com.zhiwee.gree.service.support.FileUpload.FileUploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.util.KeyUtils;
import xyz.icrab.common.web.param.IdParam;
import xyz.icrab.common.web.param.IdsParam;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/goodsImage")
public class GoodsImageController {

    @Autowired
    private GoodsImageService goodsImageService;

    @Value("${HTTP_PICTUREURL}")
    private String httpurl;

    @Autowired
    private FileUploadService fileUploadService;
    /***
     * 分页查询，周广
     * @param param
     * @param pagination
     * @return
     */
    @RequestMapping("/page")
    public Result<?> page(@RequestBody Map<String, Object> param, Pagination pagination) {
        if (param == null) {
            param = new HashMap<>();
        }
        Pageable<GoodsImage> pageable = goodsImageService.getPage(param, pagination);
        return Result.ok(pageable);
    }

    /**
     * list  集合，，周广
     * @param param
     * @return
     */
    @RequestMapping("/list")
    public Result<?> list(@RequestBody Map<String, Object> param){
        List<GoodsImage> goodsImageList =goodsImageService.getWebList(param);
        return Result.ok(goodsImageList);
    }


    /***
     * 删除 周广
     * @param param
     * @return
     */
    @RequestMapping("/delete")
    @ResponseBody
    public Result<?> delete(@RequestBody @Valid IdsParam param){
        for (String id : param.getIds()) {
            GoodsImage goodsImage=new GoodsImage();
            goodsImage.setId(id);
            goodsImageService.delete(goodsImage);
        }
        return Result.ok();
    }

    @RequestMapping("/selectOne")
    public Result<?> getOne(@RequestBody IdParam param){
        return Result.ok(goodsImageService.get(param.getId()));
    }

    /**
     * 更新  周广
     * @param goodsImage
     * @return
     */
    @RequestMapping("/update")
    public Result<?> update(@RequestBody GoodsImage goodsImage){
        goodsImageService.update(goodsImage);
        return Result.ok();
    }

    /**
     * 品牌增加  周广
     * @param goodsImage
     * @return
     */
    @RequestMapping("/add")
    public Result<?> add(@RequestBody GoodsImage goodsImage){
        goodsImage.setId(KeyUtils.getKey());
        goodsImageService.save(goodsImage);
        return Result.ok();
    }

    /**
     * Description: 图标 图片上传，周广
     * @param:
     * @return:
     */
    @PostMapping("/uploadBackPicture")
    public Result<?> uploadBackPicture(@RequestPart("file") MultipartFile file, HttpServletRequest req) throws Exception {
        FileResult result= fileUploadService.fileUpload(file.getOriginalFilename(),file.getInputStream());
        return Result.ok(result.getFileUrl());
    }

}
