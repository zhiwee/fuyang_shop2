package com.zhiwee.gree.webapi.Controller.GoodsInfo;

import com.zhiwee.gree.model.GoodsInfo.GoodsBand;
import com.zhiwee.gree.model.GoodsInfo.GoodsCategoryImg;
import com.zhiwee.gree.model.GoodsInfo.GoodsSpecsInfo;
import com.zhiwee.gree.service.GoodsInfo.GoodsSpecsInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.util.KeyUtils;
import xyz.icrab.common.web.param.IdParam;
import xyz.icrab.common.web.param.IdsParam;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

/**
 * 商品规格  周广
 */
@ResponseBody
@RequestMapping("/goodsSpecsInfo")
@RestController
public class GoodsSpecsInfoController {

    @Autowired
    private GoodsSpecsInfoService goodsSpecsInfoService;


    /***
     * 分页查询，周广
     * @param param
     * @param pagination
     * @return
     */
    @RequestMapping("/page")
    public Result<?> parentPage(@RequestBody Map<String, Object> param, Pagination pagination) {
        if (param == null) {
            param = new HashMap<>();
        }
        Pageable<GoodsSpecsInfo> pageable = goodsSpecsInfoService.getPage(param, pagination);
        return Result.ok(pageable);
    }
    /***
     * 分页查询，周广
     * @param param
     * @param pagination
     * @return
     */
    @RequestMapping("/itemPage")
    public Result<?> itemPage(@RequestBody Map<String, Object> param, Pagination pagination) {
        if (param == null) {
            param = new HashMap<>();
        }
        Pageable<GoodsSpecsInfo> pageable = goodsSpecsInfoService.getItemPage(param, pagination);
        return Result.ok(pageable);
    }

    /**
     * 增加 周广
     * @param goodsSpecsInfo
     * @return
     */
    @RequestMapping("/add")
    public Result<?> add(@RequestBody GoodsSpecsInfo goodsSpecsInfo){
        goodsSpecsInfo.setId(KeyUtils.getKey());
        goodsSpecsInfoService.save(goodsSpecsInfo);
        return Result.ok();
    }

    /**
     * 编辑  周广
     * @param goodsSpecsInfo
     * @return
     */
    @RequestMapping("/update")
    public Result<?> update(@RequestBody GoodsSpecsInfo goodsSpecsInfo){
        goodsSpecsInfoService.update(goodsSpecsInfo);
        return Result.ok();
    }

    /***
     * 查询单个  周广
     * @param param
     * @return
     */
    @RequestMapping("/selectOne")
    @ResponseBody
    public Result<?> selectOne(@RequestBody IdParam param){
        return Result.ok(goodsSpecsInfoService.get(param.getId()));
    }

    /***
     * 删除 周广
     * @param param
     * @return
     */
    @RequestMapping("/delete")
    @ResponseBody
    public Result<?> delete(@RequestBody @Valid IdsParam param){
        for (String id : param.getIds()) {
            GoodsSpecsInfo goodsSpecsInfo=new GoodsSpecsInfo();
            goodsSpecsInfo.setId(id);
            goodsSpecsInfoService.delete(goodsSpecsInfo);
        }
        return Result.ok();
    }

}
