package com.zhiwee.gree.webapi.Controller.shopBase;

import com.zhiwee.gree.model.IdentifiedUser;
import com.zhiwee.gree.model.Interface;
import com.zhiwee.gree.model.Menu;
import com.zhiwee.gree.model.User;
import com.zhiwee.gree.model.enums.UserType;
import com.zhiwee.gree.model.ret.LoginReturnMsg;
import com.zhiwee.gree.service.shopBase.InterfaceService;
import com.zhiwee.gree.service.shopBase.MenuService;
import com.zhiwee.gree.service.shopBase.UserService;
import com.zhiwee.gree.webapi.Controller.shopBase.param.ExistsMobileParam;
import com.zhiwee.gree.webapi.Controller.shopBase.param.LoginParam;
import com.zhiwee.gree.webapi.Controller.shopBase.vo.UserProfile;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.model.enums.YesOrNo;
import xyz.icrab.common.web.annotation.PermissionMode;
import xyz.icrab.common.web.consts.HeaderNames;
import xyz.icrab.common.web.session.Session;
import xyz.icrab.common.web.session.SessionService;
import xyz.icrab.common.web.support.session.DefaultSession;
import xyz.icrab.common.web.util.KeyUtils;

import xyz.icrab.ums.util.consts.SessionKeys;


import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author Knight
 * @since 2018/4/12 17:12
 */
@RestController
@RequestMapping("/account")
public class AccountController {

    @Autowired
    private UserService userService;

    @Autowired
    private MenuService menuService;

    @Autowired
    private InterfaceService interfaceService;

    @Autowired
    private SessionService sessionService;

    @RequestMapping("/login")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<UserProfile> login(@RequestBody @Valid LoginParam param, HttpServletResponse res){
        return doLogin(param, res, UserType.Super, UserType.System);
    }

    @RequestMapping("/dlogin")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<UserProfile> dlogin(@RequestBody @Valid LoginParam param, HttpServletResponse res){
        return doLogin(param, res, UserType.Super, UserType.System);
    }

    /**
     * 判断是否存在该用户名
     * @param param
     * @return
     */
    @RequestMapping("/exists")
    public Result<Boolean> existsMobile(@RequestBody ExistsMobileParam param) {
        if(param.getType() == null) {
            return Result.of(Status.ClientError.NOT_FOUND, false);
        }
        User userParam = new User();
        userParam.setUsername(param.getUsername());
//        userParam.setType(param.getType());
        //周广改
        userParam.setType(UserType.Super);
        User result = userService.getOne(userParam);
        if(result == null) {
            return Result.of(Status.ClientError.NOT_FOUND, false);
        }
        return Result.ok(true);
    }

    @RequestMapping("/menus")
    public Result<List<Menu>> getMenus(@xyz.icrab.common.web.annotation.Session(SessionKeys.MENUS) List<Menu> menus){
        if(CollectionUtils.isEmpty(menus)) {
            return Result.ok(Collections.emptyList());
        }

        Map<String, Menu> menuIndex = new HashMap<>();
        for(Menu menu : menus){
            menuIndex.put(menu.getId(), menu);
        }
        Iterator<Menu> iter = menus.iterator();
        while (iter.hasNext()) {
            Menu menu = iter.next();
            if(!"ROOT".equals(menu.getParentid())){
                Menu parent = menuIndex.get(menu.getParentid());
                if(parent != null){
                    parent.getChildren().add(menu);
                }
                iter.remove();
            }
        }
        return Result.ok(menus);
    }

    @RequestMapping("/delivery/login")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> deliveryLogin(@RequestBody @Valid LoginParam param, HttpServletResponse res) {
        return Result.of(Status.ClientError.NOT_FOUND);
    }

    private Result<UserProfile> doLogin(LoginParam param, HttpServletResponse res, UserType...userTypes) {
        IdentifiedUser user = userService.login(param.getUsername(), param.getPassword(), userTypes);
        if(user == null){
            return Result.of(Status.ClientError.NOT_FOUND, LoginReturnMsg.FAILED);
        }
        String token = KeyUtils.getKey();
        //存储至session中
        putToSession(user, token);

        res.setHeader(HeaderNames.TOKEN, token);
        //cookie
        Cookie cookie = new Cookie(HeaderNames.TOKEN, token);
        cookie.setPath("/");
        res.addCookie(cookie);

        UserProfile profile = new UserProfile();
        profile.setName(user.getUser().getNickname());
        profile.setAvatar(user.getUser().getAvatar());
        profile.setGender(user.getUser().getGender());
        //周广改
        //profile.setType(user.getUser().getType());
        profile.setType(1);
        profile.setToken(token);

        return Result.ok(profile);
    }

    private void putToSession(IdentifiedUser user, String token) {
        Session session = new DefaultSession(token);
        session.setAttribute(SessionKeys.USER, user.getUser());
        session.setAttribute(SessionKeys.ROLES, Optional.ofNullable(user.getRoles()).orElse(Collections.emptyList()) );
        session.setAttribute(SessionKeys.INTERFACES, Optional.ofNullable(user.getInterfaces()).orElse(Collections.emptyList()));
        session.setAttribute(SessionKeys.MENUS, Optional.ofNullable(user.getMenus()).orElse(Collections.emptyList()));
        session.setAttribute(SessionKeys.BUTTONS, Optional.ofNullable(user.getButtons()).orElse(Collections.emptyList()));
        session.setAttribute(SessionKeys.AUTH, user);

        //获取所有的菜单和接口，存入Session中
        Menu menuParam = new Menu();
        menuParam.setState(YesOrNo.YES.value());

        List<Menu> menus = menuService.list(menuParam);
        Map<String, Menu> menuIndex = new HashMap<>();
        if(CollectionUtils.isNotEmpty(menus)){
            for(Menu menu : menus){
                if(StringUtils.isNotBlank(menu.getUrl())){
                    menuIndex.put(menu.getUrl(), menu);
                }
            }
        }
        session.setAttribute(SessionKeys.ALL_MENUS, menuIndex);

        List<Interface> interfaces = interfaceService.list();
        Map<String, Interface> interfaceIndex = new HashMap<>();
        if(CollectionUtils.isNotEmpty(interfaces)){
            for(Interface api : interfaces){
                if(StringUtils.isNotBlank(api.getUrl())) {
                    interfaceIndex.put(api.getUrl(), api);
                }
            }
        }
        session.setAttribute(SessionKeys.ALL_INTERFACES, interfaceIndex);

        sessionService.set(session);
    }

}
