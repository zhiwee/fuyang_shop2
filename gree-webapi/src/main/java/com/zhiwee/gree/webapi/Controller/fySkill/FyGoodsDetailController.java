package com.zhiwee.gree.webapi.Controller.fySkill;

import com.zhiwee.gree.model.FyGoods.FyGoodsDetail;
import com.zhiwee.gree.model.resultBase.BaseResult;
import com.zhiwee.gree.service.FyGoods.FyGoodsDetailService;
import com.zhiwee.gree.service.support.FileUpload.FileUploadService;
import org.apache.poi.util.LittleEndianInputStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.util.IdGenerator;
import xyz.icrab.common.web.annotation.EnableDeleteRequest;
import xyz.icrab.common.web.annotation.EnableGetRequest;
import xyz.icrab.common.web.annotation.EnableListRequest;
import xyz.icrab.common.web.annotation.EnablePageRequest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/skillgoodsDetail")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class FyGoodsDetailController {


    @Autowired
    private FyGoodsDetailService   fyGoodsDetailService;


    @Autowired
    private FileUploadService  fileUploadService;

    /**
     * @Author: jick
     * @Date: 2019/12/11 10:31
     * 详情图片存入数据库
     */
    @RequestMapping("/save")
    public Result<?>  goodsItemImgSave(@RequestBody FyGoodsDetail fyGoodsDetail){


        //url重新处理
        String[]  urls = fyGoodsDetail.getUrl().split(",");

        for(String url : urls){
            fyGoodsDetail.setId(IdGenerator.uuid());
            fyGoodsDetail.setUrl(url);
            fyGoodsDetailService.save(fyGoodsDetail);
        }
        return     Result.ok();
    }


    /**
     * @Author: jick
     * @Date: 2019/12/11 10:39
     */
    @RequestMapping("/delete")
    public   Result<?>   goodsItemImgdel(String url){

        //删除服务器上的图片
        fileUploadService.fileDelete(url);

        //删除数据库里面的记录
        FyGoodsDetail  fyGoodsDetail  =  new FyGoodsDetail();
        fyGoodsDetail.setUrl(url);
        fyGoodsDetailService.delete(fyGoodsDetail);

        return   Result.ok();



    }


    /**
     * @Author: jick
     * @Date: 2019/12/11 11:07
     * 更具goodsId获取详情图片
     */
    @RequestMapping("/goodsItem")
     public  Result<?>   getGoodsItemImg(String goodsId){


        Map<String,Object> param =  new HashMap<>();

        param.put("goodsId",goodsId);

        List<FyGoodsDetail>  list =  fyGoodsDetailService.queryByGoodsId(param);

        return   Result.ok(list);

     }





}
