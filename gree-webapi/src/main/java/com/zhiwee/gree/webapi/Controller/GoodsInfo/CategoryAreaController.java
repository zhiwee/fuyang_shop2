package com.zhiwee.gree.webapi.Controller.GoodsInfo;


import com.zhiwee.gree.model.GoodsInfo.CategoryArea;
import com.zhiwee.gree.model.GoodsInfo.GoodsClassify;
import com.zhiwee.gree.model.GoodsLabel.*;
import com.zhiwee.gree.service.GoodsInfo.CategoryAreaService;
import com.zhiwee.gree.service.GoodsInfo.GoodsClassifyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.*;
import xyz.icrab.common.util.IdGenerator;
import xyz.icrab.common.web.annotation.*;
import xyz.icrab.common.web.util.Validator;

import javax.annotation.Resource;
import java.util.*;

@RequestMapping("/categoryArea")
@RestController
@ResponseBody
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class CategoryAreaController {

   @Autowired
    private CategoryAreaService categoryAreaService;



  /**
   * Description: 添加商品小类的区域
   * @author: sun
   * @Date 上午9:53 2019/7/26
   * @param:
   * @return:
   */
    @RequestMapping("/addCategoryArea")
    @ResponseBody
    public Result<Pageable<CategoryArea>> addCategoryArea(@RequestBody CategoryArea categoryArea) {
        Validator validator = new Validator();
        validator.notEmpty(categoryArea.getAreaId(),"区域不能为空");
        validator.notEmpty(categoryArea.getCategoryId(),"商品分类不能为空");
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }
        String[] areas = categoryArea.getAreaId().split(",");
        List<CategoryArea> categoryAreas = new ArrayList<>();
    for(String  areaId:areas){
        CategoryArea ca = new CategoryArea();
        ca.setAreaId(areaId);
        ca.setId(IdGenerator.objectId());
        ca.setCategoryId(categoryArea.getCategoryId());
        ca.setCreateTime( new Date());
        categoryAreas.add(ca);
    }
        categoryAreaService.save(categoryAreas);
        return Result.ok();
    }


}
