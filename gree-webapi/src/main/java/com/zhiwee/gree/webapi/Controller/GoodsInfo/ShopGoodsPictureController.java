package com.zhiwee.gree.webapi.Controller.GoodsInfo;

import com.zhiwee.gree.model.GoodsInfo.GoodsInfo;
import com.zhiwee.gree.model.GoodsInfo.ShopGoodsInfo;
import com.zhiwee.gree.model.GoodsInfo.ShopGoodsPicture;
import com.zhiwee.gree.service.GoodsInfo.GoodsInfoService;
import com.zhiwee.gree.service.GoodsInfo.ShopGoodsInfoService;
import com.zhiwee.gree.service.GoodsInfo.ShopGoodsPictureService;
import com.zhiwee.gree.webapi.util.SystemControllerLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.util.IdGenerator;
import xyz.icrab.common.web.annotation.EnableDeleteRequest;
import xyz.icrab.common.web.annotation.EnableGetRequest;
import xyz.icrab.common.web.annotation.EnableListRequest;
import xyz.icrab.common.web.annotation.EnablePageRequest;
import xyz.icrab.common.web.param.IdParam;
import xyz.icrab.common.web.param.IdsParam;

import javax.validation.Valid;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author sun on 2019/4/30
 */
@RestController
@RequestMapping("/shopGoodsPicture")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class ShopGoodsPictureController {


@Autowired
    private ShopGoodsPictureService shopGoodsPictureService;

  /**
   * Description: 更具商品id获取商品图片
   * @author: sun
   * @Date 下午6:31 2019/7/4
   * @param:
   * @return:
   */
    @RequestMapping("/getPicture")
    @ResponseBody
    public Result<?> openUseCoupon(@RequestBody @Valid IdParam map) {
        Map<String, Object> params = new HashMap<>();
        params.put("goodId",map.getId());
       List<ShopGoodsPicture>  shopGoodsPictures = shopGoodsPictureService.list(params);
        return Result.ok(shopGoodsPictures);
    }



    /**
     * Description: 根据商品id获取商品图片
     * @author: sun
     * @Date 下午6:31 2019/7/4
     * @param:
     * @return:
     */
    @RequestMapping("/deletePicture")
    @ResponseBody
    public Result<?> deletePicture(@RequestBody @Valid IdParam map) {
        ShopGoodsPicture shopGoodsPicture = new ShopGoodsPicture();
        shopGoodsPicture.setId(map.getId());
        shopGoodsPictureService.delete(shopGoodsPicture);
        return Result.ok();
    }
}
