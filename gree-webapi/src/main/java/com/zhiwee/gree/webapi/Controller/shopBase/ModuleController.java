package com.zhiwee.gree.webapi.Controller.shopBase;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.zhiwee.gree.model.Module;
import com.zhiwee.gree.model.ret.ModuleReturnMsg;
import com.zhiwee.gree.service.shopBase.ModuleService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.model.Tree;
import xyz.icrab.common.web.annotation.EnableDeleteRequest;
import xyz.icrab.common.web.annotation.EnableGetRequest;
import xyz.icrab.common.web.annotation.EnableListRequest;
import xyz.icrab.common.web.annotation.EnablePageRequest;
import xyz.icrab.common.web.message.ValidatedMessage;
import xyz.icrab.common.web.util.KeyUtils;
import xyz.icrab.common.web.util.Validator;


/**
 * @author gll
 * @since 2018/4/21 17:11
 */
@RestController
@RequestMapping("/module")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class ModuleController {

    @Autowired
    private ModuleService moduleService;

    @RequestMapping("/add")
    @ResponseBody
    public <T> Result<?> add(@RequestBody(required = false) Map<String, Object> param){
    	String name = (String)param.get("name");
    	String parentid = (String)param.get("parentid");
    	String parentids = (String)param.get("parentids");
    	Validator validator = new Validator();
    	validator.notEmpty(name, ModuleReturnMsg.NAME_EMPTY.message());
    	if(validator.isError()){
    		return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
    	}
    	if(StringUtils.isEmpty(parentid)){
    		parentid = "ROOT";
    		parentids = "ROOT";
    	}
    	Module module = new Module();
    	String id = KeyUtils.getKey();
    	parentids = parentids + "," + id ;
    	module.setId(id);
    	module.setName(name);
    	module.setParentid(parentid);
    	module.setParentids(parentids);
    	moduleService.save(module);
        return Result.ok();
    }

    @RequestMapping("/update")
    @ResponseBody
    public <T> Result<?> update(@RequestBody(required = false) Map<String, Object> param){
    	String id = (String)param.get("id");
    	String name = (String)param.get("name");
    	String parentid = (String)param.get("parentid");
    	String parentids = (String)param.get("parentids");
    	Validator validator = new Validator();
    	validator.notEmpty(id, ValidatedMessage.EMPTY_ID);
    	validator.notEmpty(name, ModuleReturnMsg.NAME_EMPTY.message());
    	if(validator.isError()){
    		return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
    	}
    	if(StringUtils.isEmpty(name)){
    		parentid = "ROOT";
    		parentids = "ROOT";
    	}
    	Module module = new Module();
    	module.setId(id);
    	module.setName(name);
    	module.setParentid(parentid);
    	module.setParentids(parentids);
    	moduleService.update(module);
        return Result.ok();
    }

    @RequestMapping("/getTree")
    @ResponseBody
    public <T> Result<List<Tree<Module>>> getTree(@RequestBody(required = false) Map<String, Object> param){
    	List<Module> list = moduleService.list();
    	List<Tree<Module>> trees = new ArrayList<>();
		for (Module record : list) {
			Tree<Module> tree = new Tree<Module>(record.getId(), record.getName(), record.getParentid(), record);
			trees.add(tree);
		}
        return Result.ok(trees);
    }


}
