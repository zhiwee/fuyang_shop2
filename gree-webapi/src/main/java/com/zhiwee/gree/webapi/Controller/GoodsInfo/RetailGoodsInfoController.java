package com.zhiwee.gree.webapi.Controller.GoodsInfo;


import com.zhiwee.gree.model.GoodsInfo.RetailGoodsPicture;
import com.zhiwee.gree.model.GoodsInfo.ShopGoodsInfo;
import com.zhiwee.gree.model.GoodsInfo.ShopGoodsPicture;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.resultBase.FileResult;
import com.zhiwee.gree.service.GoodsInfo.RetailGoodsPictureService;
import com.zhiwee.gree.service.GoodsInfo.ShopGoodsInfoService;
import com.zhiwee.gree.service.GoodsInfo.ShopGoodsPictureService;
import com.zhiwee.gree.service.ShopUser.ShopUserService;
import com.zhiwee.gree.service.support.FileUpload.FileUploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import sun.misc.BASE64Decoder;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.util.IdGenerator;
import xyz.icrab.common.web.annotation.*;
import xyz.icrab.common.web.util.Validator;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;


/**
 * @author sun on 2019/4/30
 */
@RestController
@RequestMapping("/retailGoodsInfo")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class RetailGoodsInfoController {


    @Autowired
    private RetailGoodsPictureService retailGoodsPictureService;


    @Autowired
    private ShopUserService shopUserService;

    @Autowired
    private ShopGoodsInfoService shopGoodsInfoService;

    @Autowired
    private ShopGoodsPictureService shopGoodsPictureService;

    @Value("${file.upload.dir}")
    private String fileUploadDir;


    @Value("${RETAIL_QCODE_URL}")
    private String RETAIL_QCODE_URL;




    @Value("${HTTP_PICTUREURL}")
    private String httpurl;

    @Autowired
    private FileUploadService  fileUploadService;



    /**
     * Description: 生成分销员的个人商品海报
     *
     * @author: sun
     * @Date 下午12:56 2019/6/14
     * @param:
     * @return:
     */
    @RequestMapping(value = "/createBill")
    @ResponseBody
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> achievePicture(@RequestBody Map<String, Object> params, @Session("ShopUser") ShopUser user) {
        ShopUser shopUser = shopUserService.get(user.getId());
        if (shopUser.getType() == 1) {
            return Result.of(Status.ClientError.BAD_REQUEST, "该用户不是分销员，无法制作自己的商品海报");
        }
        if (params.get("goodId") == null || ((String) params.get("goodId")).equals("")) {
            return Result.of(Status.ClientError.BAD_REQUEST, "商品的标识不能为空");
        }

        ShopGoodsInfo shopGoodsInfo = shopGoodsInfoService.get((String) params.get("goodId"));
        if (shopGoodsInfo == null || shopGoodsInfo.getRetail() != 1) {
            return Result.of(Status.ClientError.BAD_REQUEST, "该商品不是分销商品");
        }
        Map<String, Object> param = new HashMap<>();
        param.put("goodId", params.get("goodId"));
        RetailGoodsPicture retailGoodsPicture = retailGoodsPictureService.getOne(param);
        if (retailGoodsPicture == null) {
            return Result.of(Status.ClientError.BAD_REQUEST, "未获取到商品的分销图");
        }
        String redirectUrl = String.format(RETAIL_QCODE_URL, (String) params.get("goodId"), user.getId(), System.currentTimeMillis());
//        res.sendRedirect(redirectUrl);
        retailGoodsPicture.setRedirectUrl(redirectUrl);
        return Result.ok(retailGoodsPicture);
    }


    /**
     * Description:通过商品id获取商品的背景图
     *
     * @author: sun
     * @Date 上午10:31 2019/6/13
     * @param:
     * @return:
     */
    @RequestMapping(value = "/achievePicture", method = RequestMethod.POST)
    @ResponseBody
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> achievePicture(@RequestBody Map<String, Object> params) {
        RetailGoodsPicture retailGoodsPicture = retailGoodsPictureService.getOne(params);
        return Result.ok(retailGoodsPicture);
    }


    /**
     * Description: 分销商品的海报成品上传 base64后台解析成图片
     *
     * @author: sun
     * @Date 上午10:31 2019/6/13
     * @param:
     * @return:
     */
    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    @ResponseBody
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> upload(@RequestBody RetailGoodsPicture retailGoodsPicture) {
        String uuid = UUID.randomUUID().toString();
        base64CodeToimage(retailGoodsPicture.getLastPicture(), fileUploadDir+"retail/" + uuid + ".jpg");
       // base64CodeToimage(retailGoodsPicture.getLastPicture(),  "D\\"+ uuid + ".jpg");
        retailGoodsPicture.setLastPicture(httpurl+"retail/"  + uuid + ".jpg");
        retailGoodsPictureService.update(retailGoodsPicture);
        return Result.ok();
    }

    /**
     *      * base64字符串转换为图片，并保存
     *      * @Description
     *      * @Param
     *      * @Return
     *     
     */
    public void base64CodeToimage(String basee64code, String url) {
        BASE64Decoder bd = new BASE64Decoder();
        try {
            File file = new File(url);
           // File  file  = new File("D:\\ka");
            FileOutputStream fo = new FileOutputStream(file);
            fo.write(bd.decodeBuffer(basee64code));
            fo.flush();
            fo.close();
            Runtime.getRuntime().exec("chmod 777 " + url);
        } catch (IOException e) {
            String code = UUID.randomUUID().toString();
            System.out.println("保存失败");

        }
    }


    /**
     * Description: 获取分销商品 不分页
     *
     * @author: sun
     * @Date 下午12:56 2019/6/14
     * @param:
     * @return:
     */
    @RequestMapping(value = "/achieveRetailGoods")
    @ResponseBody
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> achieveRetailGoods() {
        Map<String, Object> params = new HashMap<>();
        params.put("retail", 1);
        params.put("state", 1);
        List<ShopGoodsInfo> list = shopGoodsInfoService.list(params);
        if (!list.isEmpty() && list.size() > 0) {
            for (ShopGoodsInfo shopGoodsInfo : list) {
                params.clear();
                params.put("goodId", shopGoodsInfo.getId());
                List<ShopGoodsPicture> goodPictures = shopGoodsPictureService.list(params);
                if (!goodPictures.isEmpty() && goodPictures.size() > 0) {
                    shopGoodsInfo.setPicture(goodPictures.get(0).getPicture());
                }

            }
        }

        return Result.ok(list);
    }


    /**
     * Description: 海报背景图上传
     *
     * @author: jick
     * @Date 下午11:04 2019/8/10
     * @param:
     * @return:
     */
    @PostMapping("/uploadBackPicture")
    public Result<?> uploadBackPicture(@RequestPart("file") MultipartFile file, HttpServletRequest req) throws Exception {
      /*  Map param = RequestUtils.getParameters(req);
        // 获取文件名
        String suffix = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".") + 1);
        String id = KeyUtils.getKey();
        // 获取项目的路径 + 拼接得到文件要保存的位置
        String filePath = fileUploadDir+"retail/" +id+"."+ suffix;
        // 创建一个文件的对象
        File file1 = new File(filePath);
        // 创建父文件夹
        Files.createParentDirs(file1);
        try {
            // 先尝试压缩并保存图片
            Thumbnails.of(file.getInputStream()).scale(1f).outputQuality(0.25f).toFile(file1);
        } catch (IOException e) {
            try {
                // 失败了再用springmvc自带的方式
                file.transferTo(file1);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }

           Runtime.getRuntime().exec("chmod 777 " + filePath);
          //Runtime.getRuntime().exec(filePath);
        return Result.ok(httpurl+"retail/" +id+"."+ suffix);*/

         FileResult result= fileUploadService.fileUpload(file.getOriginalFilename(),file.getInputStream());
        return Result.ok(result.getFileUrl());

    }


    /**
     * Description: 海报背景图添加
     *
     * @author: sun
     * @Date 下午11:04 2019/6/19
     * @param:
     * @return:
     */
    @PostMapping("/addBackPicture")
    public Result<?> addBackPicture(@RequestBody RetailGoodsPicture retailGoodsPicture) throws Exception {
        Validator validator = new Validator();
        validator.notNull(retailGoodsPicture.getGoodId(), "商品id不能为空");
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }
        Map<String, Object> pramas = new HashMap<>();
        pramas.put("goodId", retailGoodsPicture.getGoodId());
        RetailGoodsPicture one = retailGoodsPictureService.getOne(pramas);
        if(one != null){
            retailGoodsPictureService.delete(one);
            retailGoodsPicture.setId(IdGenerator.objectId());
            retailGoodsPictureService.save(retailGoodsPicture);
        }else{
            retailGoodsPicture.setId(IdGenerator.objectId());
            retailGoodsPictureService.save(retailGoodsPicture);
        }

        return Result.ok();
    }
}