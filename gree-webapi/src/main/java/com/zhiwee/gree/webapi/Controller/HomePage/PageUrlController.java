package com.zhiwee.gree.webapi.Controller.HomePage;


import com.zhiwee.gree.model.HomePage.PageNav;
import com.zhiwee.gree.model.HomePage.PageSearch;
import com.zhiwee.gree.model.HomePage.PageUrl;
import com.zhiwee.gree.service.HomePage.PageUrlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.util.KeyUtils;
import xyz.icrab.common.web.param.IdsParam;

import javax.net.ssl.SSLEngineResult;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/pageUrl")
public class PageUrlController {

    @Autowired
    private PageUrlService pageUrlService;

    /***
     * 分页查询，周广
     * @param param
     * @param pagination
     * @return
     */
    @RequestMapping("/page")
    public Result<?> page(@RequestBody Map<String, Object> param, Pagination pagination) {
        if (param == null) {
            param = new HashMap<>();
        }
        Pageable<PageUrl> pageable = pageUrlService.getPage(param, pagination);
        return Result.ok(pageable);
    }


    /**
     * 新增
     * @param pageUrl
     * @return
     */
    @RequestMapping("/add")
    public Result<?> add(@RequestBody PageUrl pageUrl){
        //跳转商品   类型  -2
        if(pageUrl.getType()==1){

        }
        else if(pageUrl.getType()==2){
            pageUrl.setUrl("./pages/goods/goodsinfo.html");
        //跳转分类   类型  -3
        }else if(pageUrl.getType()==3){
            pageUrl.setUrl("./pages/category/category.html");
        }
        //跳转分类and 规格   类型  -2
        else if(pageUrl.getType()==4){
            pageUrl.setUrl("./pages/category/category.html");
        }else{
            return Result.of(Status.ClientError.BAD_REQUEST,"类型错误");
        }
        pageUrl.setId(KeyUtils.getKey());
        pageUrlService.save(pageUrl);
        return Result.ok();
    }

    /***
     * 启用  周广
     * @param param
     * @return
     */
    @RequestMapping("/enable")
    @ResponseBody
    public Result<?> enable(@RequestBody @Valid IdsParam param){
        for (String id : param.getIds()) {
            PageUrl pageUrl=new PageUrl();
            pageUrl.setId(id);
            pageUrl.setState(1);
            pageUrlService.update(pageUrl);
        }
        return Result.ok();
    }

    /***
     * 禁用  周广
     * @param param
     * @return
     */
    @RequestMapping("/disable")
    @ResponseBody
    public Result<?> disable(@RequestBody @Valid IdsParam param){
        for (String id : param.getIds()) {
            PageUrl pageUrl=new PageUrl();
            pageUrl.setId(id);
            pageUrl.setState(2);
            pageUrlService.update(pageUrl);
        }
        return Result.ok();
    }


    /***
     * 删除 周广
     * @param param
     * @return
     */
    @RequestMapping("/delete")
    @ResponseBody
    public Result<?> delete(@RequestBody @Valid IdsParam param){
        for (String id : param.getIds()) {
            PageUrl pageUrl=new PageUrl();
            pageUrl.setId(id);
            pageUrlService.delete(pageUrl);
        }
        return Result.ok();
    }


}
