package com.zhiwee.gree.webapi.Controller.GoodsInfo;

import com.zhiwee.gree.model.FyGoods.Fygoods;
import com.zhiwee.gree.model.GoodsInfo.*;
import com.zhiwee.gree.service.GoodsInfo.*;
import com.zhiwee.gree.service.support.FileUpload.FileUploadService;
import com.zhiwee.gree.service.tgoods.GoodsLabelService;
import com.zhiwee.gree.webapi.util.JsonUtil;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.solr.core.SolrTemplate;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.*;
import xyz.icrab.common.util.KeyUtils;
import xyz.icrab.common.web.annotation.*;
import xyz.icrab.common.web.param.IdsParam;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.*;


/**
 * @author jick
 * @date 2019/8/30
 */
@RestController
@RequestMapping("/goodsBase")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class GoodsBaseController {
    @Resource
    private GoodsBaseService  goodsBaseService;
    @Resource
    private CategorysService categorysService;
    @Resource
    private GoodsLabelService goodsLabelService;
    @Resource
    private GoodsCategoryService goodsCategoryService;
    @Resource
    private FileUploadService    fileUploadService;
    @Resource
    private LActivityGoodsService   lActivityGoodsService;
    @Resource
    private SolrTemplate solrTemplate;
    @Resource
    private RedisTemplate<String, String> redisTemplate;
    @Resource
    private GoodsInfoPictureService goodsInfoPictureService;
    @Resource
    private GoodsService  goodsService;

    /**
     * 商品信息入库
     * @param goods
     * @return
     */

    @RequestMapping(value = "/saveOrupdate")
    @ResponseBody
    @SuppressWarnings("all")
    public Result<?> productInfoSave(@RequestBody GoodsBase goods){
      /*  if(goods.getFloor()==null || "".equals(goods.getFloor())){
            return Result.of(Status.ClientError.BAD_REQUEST,"请选择楼层");
        }*/

        //如果主键为空
        if(goods.getId()==null || goods.getId().trim()==""){
            //设置主键
            String key = KeyUtils.getKey();
            goods.setState(1);
            goods.setId(key);
            goods.setIsdept(2);
            goods.setLiveUrl("pages/detail/detail.html?goodsid="+key+"&goodstype=2");
            String categoryId = goods.getCategoryId();
            Map<String ,Object> param=new HashMap<>();
            param.put("parentId",categoryId);
            Categorys goodsCategory=new Categorys();
            goodsCategory.setId(categoryId);
            goodsCategory=     categorysService.getOne(goodsCategory);
            List<Categorys> list = categorysService.list(param);
            if (!list.isEmpty()){
                return Result.of(Status.ClientError.BAD_REQUEST,"商品分类：只能选择末级");
            }
            //存入数据库
            goodsBaseService.save(goods);
        }else{
            //否则是更新操作
            goods.setState(1);
            String categoryId = goods.getCategoryId();
            Map<String ,Object> param=new HashMap<>();
            goods.setLiveUrl("pages/detail/detail.html?goodsid="+goods.getId()+"&goodstype=2");
            param.put("parentId",categoryId);
            Categorys goodsCategory=new Categorys();
            goodsCategory.setId(categoryId);
            goodsCategory=     categorysService.getOne(goodsCategory);
            List<Categorys> list = categorysService.list(param);
            if (!list.isEmpty()){
                return Result.of(Status.ClientError.BAD_REQUEST,"商品分类：只能选择末级");
            }
            goodsBaseService.update(goods);
        }
        return   Result.ok(goods);

    }
            /**
             * @author tzj
             * @description 循环扎到所有他所在的二级
             * @date 2019/12/27 17:12
            */
    private GoodsCategory getFather(GoodsCategory goodsCategory) {
        GoodsCategory goodsCategory1=new GoodsCategory();
        if (goodsCategory.getRank()==2){
            return goodsCategory;
        }else {
            goodsCategory1.setId(goodsCategory.getParentId());
            goodsCategory1= goodsCategoryService.getOne(goodsCategory1);
            getFather(goodsCategory1);
            return goodsCategory1;
        }
    }

        /**
         * 列表展示
         */
    @RequestMapping(value = "searchAll")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?>  searchAll(@RequestBody(required = false) Map<String, Object> params, Pagination pagination) {
        Pageable<GoodsBase> list =   goodsBaseService.queryInfoList(params,pagination);
        return Result.ok(list);
    }

    /**
     * 是否推荐
     */
    @RequestMapping(value = "isRecommendOpen")
    public  Result<?> recommendOpenController(@RequestBody @Valid IdsParam map){
        List<String> ids = map.getIds();
        for(String id:ids){
            GoodsBase  goods = new GoodsBase();
            goods.setId(id);
            goods.setIsRecommend(1);
            goodsBaseService.update(goods);
        }
        return   Result.ok();

    }
    @RequestMapping(value = "isRecommendStop")
    public  Result<?> recommendStopController(@RequestBody @Valid IdsParam map){
        List<String> ids = map.getIds();
        for(String id:ids){
            GoodsBase  goods = new GoodsBase();
            goods.setId(id);
            goods.setIsRecommend(2);
            goodsBaseService.update(goods);
        }
        return   Result.ok();

    }

    /**
     * 是否为最新商品
     */
    @RequestMapping(value = "isNewOpen")
    public  Result<?> isNewOpenController(@RequestBody @Valid IdsParam map){
        List<String> ids = map.getIds();
        for(String id:ids){
            GoodsBase  goods = new GoodsBase();
            goods.setId(id);
            goods.setIsNew(1);
            goodsBaseService.update(goods);
        }
        return   Result.ok();
    }
    @RequestMapping(value = "isNewStop")
    public  Result<?> isNewStopController(@RequestBody @Valid IdsParam map){
        List<String> ids = map.getIds();
        for(String id:ids){
            GoodsBase  goods = new GoodsBase();
            goods.setId(id);
            goods.setIsNew(2);
            goodsBaseService.update(goods);
        }
        return   Result.ok();
    }

    /**
     * 是否热卖
     */
    @RequestMapping(value = "isHotOpen")
    public  Result<?> isHotOpenController(@RequestBody @Valid IdsParam map){
        List<String> ids = map.getIds();
        for(String id:ids){
            GoodsBase  goods = new GoodsBase();
            goods.setId(id);
            goods.setIsHot(1);
            goodsBaseService.update(goods);
        }
        return   Result.ok();
    }
    @RequestMapping(value = "isHotStop")
    public  Result<?> isHotStopController(@RequestBody @Valid IdsParam map){
        List<String> ids = map.getIds();
        for(String id:ids){
            GoodsBase  goods = new GoodsBase();
            goods.setId(id);
            goods.setIsHot(2);
            goodsBaseService.update(goods);
        }
        return   Result.ok();
    }

    /**
     * 是否下架
     */
    @RequestMapping(value = "isDownOpen")
    public  Result<?> isDownOpenController(@RequestBody @Valid IdsParam map){
        List<String> ids = map.getIds();
        for(String id:ids){
            GoodsBase  goods = new GoodsBase();
            goods.setId(id);
            goods.setIsdown(1);
            goodsBaseService.update(goods);
        }
        return   Result.ok();
    }

    @RequestMapping(value = "isDownStop")
    public  Result<?> isDownCloseController(@RequestBody @Valid IdsParam map){
        List<String> ids = map.getIds();
        for(String id:ids){
            GoodsBase  goods = new GoodsBase();
            goods.setId(id);
            goods.setIsdown(2);
            goodsBaseService.update(goods);
        }
        return   Result.ok();
    }

    /**
     * 删除操作
      */
    @RequestMapping("/delete")
    public   Result<?>  goodsInfoDel(@RequestBody @Valid IdsParam map){
        List<String>  ids = map.getIds();
        for(String id : ids){
            GoodsBase  goods = new GoodsBase();
            goods.setId(id);
            goods.setState(2);
            goodsBaseService.update(goods);
        }
        return   Result.ok();

    }

    /**
     * @author tzj
     * @description  秒杀商品
     * @date 2019/9/24 19:03
    */
    @RequestMapping("/getInfo")
    public   Result<?>  getInfo(@RequestBody(required = false) Map<String, Object> params){
        LAGoodsInfo  laGoodsInfo  =  lActivityGoodsService.queryKillInfoById(params);
        return  Result.ok(laGoodsInfo);
    }

    /**
     * @author tzj
     * @description 秒杀商品表
     * @date 2019/9/24 14:44
     * @return
    */
    @RequestMapping(value = "secKillGoods")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?>  secKillGoods(@RequestBody(required = false) Map<String, Object> params, Pagination pagination) {
                params.put("activityType",1);
        Pageable<LAGoodsInfo> list =    lActivityGoodsService.secKillGoods(params,pagination);

        List<LAGoodsInfo> list2 = lActivityGoodsService.secKillGoods2(params);

        HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();

        // 先查询
        Map<String, String> resultMap = hashOperations.entries("SeckillGoods:001");

        //循环将值插入到redis
        if(resultMap.isEmpty()){
            for(LAGoodsInfo lag : list2){
                resultMap.put(lag.getActivityId()+":"+lag.getGoodsId(), JsonUtil.object2JsonStr(lag));
            }
            hashOperations.putAll( "SeckillGoods:001", resultMap);
        }

        return Result.ok(list);
    }

    /**
     * @Author: jick
     * @Date: 2019/10/19 17:48
     * 将缓存数据库里面的数据同步至数据库
     */
    @RequestMapping(value = "redisToDatabase")
    @PermissionMode(PermissionMode.Mode.White)
    public  Result<?>   redisSysDatabase(){

        HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
        // 先查询
        Map<String, String> resultMap = hashOperations.entries("SeckillGoods:001");

        if(!resultMap.isEmpty()){
            for(Map.Entry<String,String> map : resultMap.entrySet()){
                LAGoodsInfo  laGoodsInfo  = JsonUtil.jsonStr2Object(map.getValue(),  LAGoodsInfo.class);
                lActivityGoodsService.update(laGoodsInfo);
            }
            //删除缓存数据库数据。

            }

        return   Result.ok();
    }


    /**
     * @author tzj
     * @description 秒杀商品表更新价格
     * @date 2019/9/24 14:44
     * @return
     */

    @RequestMapping( "/upSecKillGoods")
    public Result<?>  upSecKillGoods(@RequestBody(required = false) Map<String, Object> params) {
        LAGoodsInfo   lActivityGoods  = new LAGoodsInfo();
        lActivityGoods.setSecKillPrice(new BigDecimal((String)params.get("secKillPrice")));
        lActivityGoods.setId((String)params.get("id"));
        lActivityGoods.setStockNum(Integer.valueOf((String) params.get("stockNum")));
         lActivityGoodsService.update(lActivityGoods);
        return Result.ok();
    }

    /**
     * @author tzj
     * @description 获取基础商品信息
     * @date 2020/4/8 9:43
     */
    @RequestMapping("/getGoodsBase")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> getGoodsBase(@RequestBody(required = false) Map<String, Object> params) {
        if (params==null){
            params=new HashMap<>(5);
        }
        params.put("state",1);
        params.put("isdown",1);
        List<GoodsBase> list = goodsBaseService.listAll(params);
        return Result.ok(list);
    }

    /**
     * 通过id查询商品的详细信息
     */
    @ResponseBody
    @RequestMapping("/getDetails")
    @PermissionMode(PermissionMode.Mode.White)
    public   Result<?>  getMe(@RequestBody(required = false) Map<String, Object> params){
        GoodsBase  goods  =  goodsBaseService.queryInfoListById(params);
        if (goods!=null) {
            List<Goods>  list2= goodsService.getCount(goods.getId());
            Integer i=0;
            for (Goods goods1 : list2) {
                if (goods1.getSaleCount()!=null) {
                    i = i + goods1.getSaleCount();
                }
            }
            //获取详情
            goods.setSaleCount(i);
            Map<String, Object> param = new HashMap<>();
            param.put("goodsId", goods.getId());
            List<GoodsInfoPicture> list = goodsInfoPictureService.listAll(param);
            if (list.size() > 0) {
                List<String> urlList = new ArrayList<>(10);
                for (GoodsInfoPicture goodsInfoPicture : list) {
                    urlList.add(goodsInfoPicture.getImgUrl());
                }
                goods.setUrlList(urlList);
            }
            return Result.ok(goods);
        }else {
            return Result.of(Status.ClientError.BAD_REQUEST,"未找到该商品");
        }

    }


    @RequestMapping("/tree")
    public Result<?> tree(@RequestBody(required = false) Map<String, Object> params) {
        if (params == null) {
            params = new HashMap<>();
        }
        List<GoodsBase> list = goodsBaseService.list(params);
        List<Tree<Categorys>> trees = new ArrayList<>(list.size());
        for (GoodsBase goodsBase : list) {
            Tree<Categorys> tree = new Tree<>(goodsBase.getId(), goodsBase.getName(), null, null);
            trees.add(tree);
        }
        return Result.ok(trees);
    }



}
