package com.zhiwee.gree.webapi.Controller.QuartzInfo;

import com.zhiwee.gree.model.Partement.Partement;
import com.zhiwee.gree.model.Quartz.QuartzInfo;
import com.zhiwee.gree.service.Coupon.CouponService;
import com.zhiwee.gree.service.Partement.PartementService;
import com.zhiwee.gree.service.QuartzInfo.QuartzInfoService;
import com.zhiwee.gree.webapi.QuartzUtil.QuartzJob;
import com.zhiwee.gree.webapi.QuartzUtil.QuartzManager;
import org.quartz.Scheduler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.util.IdGenerator;
import xyz.icrab.common.web.annotation.EnableDeleteRequest;
import xyz.icrab.common.web.annotation.EnableGetRequest;
import xyz.icrab.common.web.annotation.EnableListRequest;
import xyz.icrab.common.web.annotation.EnablePageRequest;
import xyz.icrab.common.web.param.IdParam;
import xyz.icrab.common.web.param.IdsParam;
import xyz.icrab.common.web.util.Validator;

import javax.validation.Valid;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * @author sun on 2019/6/16
 */
@RestController
@RequestMapping("/quartzInfo")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class QuartzInfoController {


    @Autowired
    private SchedulerFactoryBean schedulerFactoryBean;


    @Autowired
    private CouponService couponService;

   @Autowired
   private QuartzInfoService quartzInfoService;

    private static  String  time=  "0 z x * * ?";
    /**
     * Description: 通过id获取信息
     *
     * @author: sun
     * @Date 下午3:20 2019/6/13
     * @param:
     * @return:
     */
    @RequestMapping("/getOne")
    public Result<?> getOne(@RequestBody @Valid IdParam map) {
     String id = map.getId();
        QuartzInfo quartzInfo = quartzInfoService.get(id);
        return Result.ok(quartzInfo);
    }

    /**
     * Description: 获取所有信息
     *
     * @author: sun
     * @Date 下午3:20 2019/6/13
     * @param:
     * @return:
     */
    @RequestMapping("/pageInfo")
    public Result<?> pageInfo(@RequestBody Map<String, Object> params,  Pagination pagination) {
        if (params == null) {
            params = new HashMap<>();
        }
        Pageable<QuartzInfo> page = quartzInfoService.pageInfo(params, pagination);
        return Result.ok(page);
    }

    /**
     * Description: 更新触发器的时间
     * @author: sun 现在的实现方式时先删除再添加
     * @Date 下午4:26 2019/7/3
     * @param:
     * @return:
     */
    @RequestMapping("/updateQuartz")
    public Result<?> updateQuartz(@RequestBody QuartzInfo quartzInfo) {
        Validator validator = new Validator();
        validator.notEmpty(quartzInfo.getId(), "任务的id不能为空");
        validator.notNull(quartzInfo.getHour(), "任务的时间不能为空");
        validator.notNull(quartzInfo.getMin(), "任务的时间不能为空");
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }

        QuartzInfo qi = quartzInfoService.get(quartzInfo.getId());
        /**
         * 要想不删除的前提下修改时间必须的修改trriger的下一次执行的时间NEXT_FIRE_TIME和上一次执行的时间PREV_FIRE_TIME值改为0
         */
        if(qi.getState() == 1){
            Scheduler sche = schedulerFactoryBean.getScheduler();
            QuartzManager.removeJob(sche,qi.getJobName(),qi.getJobGroupName(),qi.getTriggerName(),qi.getTriggerGroupName());
            Pattern p = Pattern.compile("x");
            Matcher m = p.matcher(time);
            String hour = m.replaceAll(quartzInfo.getHour().toString());

            p = Pattern.compile("z");
            m = p.matcher(hour);
            String min = m.replaceAll(quartzInfo.getMin().toString());

            QuartzManager.addJob(sche, qi.getJobName(),qi.getJobGroupName(),qi.getTriggerName(),
                    qi.getTriggerGroupName(), QuartzJob.class, min);
//            QuartzManager.modifyJobTime(sche,qi.getTriggerName(),qi.getTriggerGroupName(),min);
        }
        quartzInfo.setUpdateTime(new Date());
        quartzInfoService.update(quartzInfo);
                return Result.ok();
    }




    /**
     * Description: 启用 添加任务
     *
     * @author: sun
     * @Date 下午3:20 2019/6/13
     * @param:
     * @return:
     */
    @RequestMapping("/open")
    public Result<?> open(@RequestBody @Valid IdsParam map) {
        List<String> ids = map.getIds();

        Scheduler sche = schedulerFactoryBean.getScheduler();
        for (String id : ids) {
            QuartzInfo quartzInfo = quartzInfoService.get(id);
            Pattern p = Pattern.compile("x");
            Matcher m = p.matcher(time);
            String hour = m.replaceAll(quartzInfo.getHour().toString());

            p = Pattern.compile("z");
            m = p.matcher(hour);
            String min = m.replaceAll(quartzInfo.getMin().toString());

            QuartzManager.addJob(sche, quartzInfo.getJobName(),quartzInfo.getJobGroupName(),quartzInfo.getTriggerName(),
                    quartzInfo.getTriggerGroupName(), QuartzJob.class, min);
            quartzInfo.setId(id);
            quartzInfo.setState(1);
            quartzInfo.setUpdateTime(new Date());
            quartzInfoService.update(quartzInfo);
        }

        return Result.ok();
    }

    /**
     * Description: 禁用 移除任务
     *
     * @author: sun
     * @Date 下午3:20 2019/6/13
     * @param:
     * @return:
     */
    @RequestMapping("/stop")
    public Result<?> stop(@RequestBody @Valid IdsParam map) {
        List<String> ids = map.getIds();
        Scheduler sche = schedulerFactoryBean.getScheduler();
        for (String id : ids) {
            QuartzInfo quartzInfo = quartzInfoService.get(id);
            QuartzManager.removeJob(sche,quartzInfo.getJobName(),quartzInfo.getJobGroupName(),quartzInfo.getTriggerName(),quartzInfo.getTriggerGroupName());
            quartzInfo.setState(2);
            quartzInfo.setUpdateTime(new Date());
            quartzInfoService.update(quartzInfo);

        }

        return Result.ok();
    }

}




