package com.zhiwee.gree.webapi.Controller.AirCondition;

import com.zhiwee.gree.model.AchieveGift.GiftInfo;
import com.zhiwee.gree.model.AirCondition.AirCondition;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.service.AchieveGift.GiftInfoService;
import com.zhiwee.gree.service.AirCondition.AirConditionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.util.IdGenerator;
import xyz.icrab.common.web.annotation.*;
import xyz.icrab.common.web.param.IdParam;
import xyz.icrab.common.web.param.IdsParam;
import xyz.icrab.common.web.util.Validator;

import javax.validation.Valid;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author sun on 2019/7/15
 */
@RestController
@RequestMapping("/airCondition")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class AirConditionController {
    @Autowired
    private AirConditionService airConditionService;

/**
 * Description: 查询所有的中央空调等级记录
 * @author: sun
 * @Date 下午9:46 2019/7/15
 * @param:
 * @return:
 */
    @RequestMapping(value = "/pageInfo")
    public Result<?>  pageInfo(@RequestBody(required = false) Map<String, Object> params,Pagination pagination){
        Pageable<AirCondition> achieveGifts = airConditionService.pageInfo(params, pagination);
        return Result.ok(achieveGifts);
    }




    /**
     * Description: 通过手机号和劵号领取礼品
     * @author: sun
     * @Date 下午12:51 2019/5/6
     * @param:  //1 家装  2 工装
     * @return:
     */
    @RequestMapping(value = "/addAirCondition")
    public Result<?> achieveGift(@RequestBody AirCondition airCondition, @Session("ShopUser") ShopUser shopUser){
        Validator validator = new Validator();
        validator.notNull(airCondition.getType(), "安装类型不能为空");
        validator.notEmpty(airCondition.getSquare(), "面积不能为空");
        validator.notEmpty(airCondition.getUsername(), "联系人不能为空");
        validator.notEmpty(airCondition.getUserPhone(), "联系电话不能为空");
        validator.notEmpty(airCondition.getRequirement(), "意向不能为空");
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }

        if(airCondition.getType() == 2){
            validator.notEmpty(airCondition.getDealerName(), "单位名称不能为空");
            validator.notEmpty(airCondition.getInstallAddress(), "安装地址不能为空");
            if (validator.isError()) {
                return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
            }
        }


        if(airCondition.getType() == 1){
            validator.notEmpty(airCondition.getCity(), "市不能为空");
            validator.notEmpty(airCondition.getArea(), "县/区不能为空");
            validator.notEmpty(airCondition.getVillage(), "小区不能为空");
            validator.notEmpty(airCondition.getQi(), "期不能为空");
            validator.notEmpty(airCondition.getDong(), "栋不能为空");
            validator.notEmpty(airCondition.getDy(), "单元不能为空");
            validator.notEmpty(airCondition.getRoomNo(), "房号不能为空");
            if (validator.isError()) {
                return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
            }
        }
        airCondition.setId(IdGenerator.objectId());
        airCondition.setCreateTime( new Date());
        airCondition.setUserId(shopUser.getId());
        airConditionService.save(airCondition);
        return Result.ok();
    }



    /**
     * Description: 获取个人中央空调登记记录
     * @author: sun
     * @Date 下午12:51 2019/5/6
     * @param:
     * @return:
     */
    @RequestMapping(value = "/achieveReword")
    public Result<?> achieveReword( @Session("ShopUser") ShopUser shopUser){
        Map<String,Object> params  = new HashMap<>();
        params.put("userId",shopUser.getId());
        List<AirCondition> list = airConditionService.list(params);
        return Result.ok(list);
    }

}
