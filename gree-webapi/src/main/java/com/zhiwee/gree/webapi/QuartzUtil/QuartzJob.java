package com.zhiwee.gree.webapi.QuartzUtil;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.zhiwee.gree.model.Coupon.Coupon;
import com.zhiwee.gree.service.Coupon.CouponService;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author sun on 2019/7/3
 */

public class QuartzJob implements Job  {

    /**
     * 但是遇到了在任务类中需要引用注入类，但是注入类对象zhuangbeietongService为空，
     *     故查询原因得知：Quartz初始化是自己的JobContext，不同于Spring的ApplicationContext，
     *     所以无法直接注入，导致使用时产生空指针异常！
     *
     *
     *     job对象在spring容器加载时候，能够注入bean，但是调度时，job对象会重新创建
     *     ，此时就是导致已经注入的对象丢失，因此报空指针异常。
     *
     *
     *     重写jobFactory在重建job重新使用spring的注入类将属性注入到job中
     * @param jobExecutionContext
     * @throws JobExecutionException
     */



    @Autowired
    private CouponService couponService;


    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
//        JobDataMap dataMap = jobExecutionContext.getJobDetail().getJobDataMap();
//
//
//        String couponMessage = dataMap.getString("couponMessage");
//
//        JSONObject jsonObject = JSON.parseObject(couponMessage);
//        CouponService couponService = jsonObject.toJavaObject(CouponService.class);


        Map<String,Object> params = new HashMap<>();
        params.put("couponState","1");
        List<Coupon> couponList = couponService.list(params);
        for(Coupon coupon : couponList){
            if(new Date().after(coupon.getEndTime())){
                coupon.setCouponState(3);
                couponService.update(coupon);
            }
        }

//        String jobName = jobExecutionContext.getJobDetail().getKey().getName();
//        String jobGroup = jobExecutionContext.getJobDetail().getKey().getGroup();
//        String triggerName = jobExecutionContext.getTrigger().getKey().getName();
//        String triggerGroup = jobExecutionContext.getTrigger().getKey().getGroup();

//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");
//        System.err.println("触发器Key:" + triggerName + ".." + triggerGroup + " 正在执行...");
//        System.err.println("任务Key:" + jobName + ".." + jobGroup + " 正在执行，执行时间: "
//                + dateFormat.format(Calendar.getInstance().getTime()));



    }

//    @Override
//    public void afterPropertiesSet() throws Exception {
//
//    }
}
