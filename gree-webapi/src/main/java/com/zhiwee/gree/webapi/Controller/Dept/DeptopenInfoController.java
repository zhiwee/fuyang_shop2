package com.zhiwee.gree.webapi.Controller.Dept;

import com.zhiwee.gree.model.DeptInfo.DeptopenInfo;
import com.zhiwee.gree.model.User;
import com.zhiwee.gree.service.Dept.DeptopenInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.web.annotation.*;
import xyz.icrab.common.web.param.IdParam;
import xyz.icrab.common.web.param.IdsParam;
import xyz.icrab.ums.util.consts.SessionKeys;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

/**
 * Created by jick on 2019/8/23.
 */
@RestController
@RequestMapping("/deptOpenInfo")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class DeptopenInfoController {


    @Autowired
    private DeptopenInfoService   deptopenInfoService;


    /**
     * @author tzj
     * @description 信息入库
     * @date 2019/12/14 0:43
    */
    @RequestMapping(value = "/save")
    public Result<?> deptOpenInfoAdd(@Session(SessionKeys.USER) User user, @RequestBody(required = false) DeptopenInfo deptopenInfo){

        /**
         * 非空判断
         */
        if(null==deptopenInfo.getDeptName()||null==deptopenInfo.getIsopenShop()||null==deptopenInfo.getIsopenCity()
                ||null==deptopenInfo.getIsopenName()||null==deptopenInfo.getIsopenTel()||null==deptopenInfo.getIsopenInstall()
                ||null==deptopenInfo.getIsopenBranch()||null==deptopenInfo.getIsopenFactory()){
            return  Result.of(Status.ClientError.FORBIDDEN, "基本信息不完整,请检查");
        }

        deptopenInfoService.save(deptopenInfo);
        return   Result.ok();




    }

    /**
     * 更新操作
     */
   // @PermissionMode(PermissionMode.Mode.White)
    @RequestMapping(value = "/update")
    public Result<?> deptOpenInfoUpdate(@Session(SessionKeys.USER) User user, @RequestBody(required = false) DeptopenInfo deptopenInfo) {

        /**
         * 非空判断
         */
        if(null==deptopenInfo.getDeptName()||null==deptopenInfo.getIsopenShop()||null==deptopenInfo.getIsopenCity()
                ||null==deptopenInfo.getIsopenName()||null==deptopenInfo.getIsopenTel()||null==deptopenInfo.getIsopenInstall()
                ||null==deptopenInfo.getIsopenBranch()||null==deptopenInfo.getIsopenFactory()){
            return  Result.of(Status.ClientError.FORBIDDEN, "基本信息不完整,请检查");
        }

        deptopenInfoService.update(deptopenInfo);
        return   Result.ok();

    }




        /**
         * 类表展示
         */

    @RequestMapping("/list")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?>   deptOpenInfoList(Pagination pagination, @RequestBody(required =false)Map<String,Object> map){

        // Pageable<Information> list =  informationService.queryInfoList(pagination,map);
        Pageable<DeptopenInfo>  list  =  deptopenInfoService.queryDeptOpenInfo(map,pagination);
        return Result.ok(list);

    }


    /**
     *通过id查询商品的信息，用来做后台信息回显
     */
    @RequestMapping("/get")
    @PermissionMode(PermissionMode.Mode.White)
    @ResponseBody
    public Result<?> getMe(@RequestBody @Valid IdParam map){
        //  ActivityPage activityPage = activityPageService.get(map.getId());
        DeptopenInfo   deptopenInfo = deptopenInfoService.get(map.getId());
        return Result.ok(deptopenInfo);
    }

    /**
     * 删除操作
     */
    @RequestMapping("/delete")
    @ResponseBody
    public Result<?> open(@RequestBody @Valid IdsParam map) {
        List<String> ids = map.getIds();
         DeptopenInfo deptopenInfo = new DeptopenInfo();
        for (String id : ids) {
            deptopenInfo.setId(Integer.parseInt(id));
            //activityPageService.update(activityPage);
            deptopenInfoService.delete(deptopenInfo);
        }
        return Result.ok();
    }



}
