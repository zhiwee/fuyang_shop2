package com.zhiwee.gree.webapi.Controller.GoodsInfo;

import com.zhiwee.gree.model.GoodsInfo.GoodVo.SpecsInfoOrderVO;
import com.zhiwee.gree.model.GoodsInfo.GoodsSpecsInfoOrder;
import com.zhiwee.gree.model.GoodsInfo.GoodsSpecsItem;
import com.zhiwee.gree.model.GoodsInfo.GoodsSpecsItemOrder;
import com.zhiwee.gree.service.GoodsInfo.GoodsSpecsInfoOrderService;
import org.apache.commons.collections.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.util.KeyUtils;
import xyz.icrab.common.web.param.IdParam;
import xyz.icrab.common.web.param.IdsParam;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RequestMapping("/goodsSpecsInfoOrder")
@RestController
public class GoodsSpecsInfoOrderController {

    @Autowired
    private GoodsSpecsInfoOrderService goodsSpecsInfoOrderService;

    /**
     * 增加 周广
     * @param goodsSpecsInfoOrder
     * @return
     */
    @RequestMapping("/add")
    public Result<?> add(@RequestBody GoodsSpecsInfoOrder goodsSpecsInfoOrder){
        goodsSpecsInfoOrder.setId(KeyUtils.getKey());
        goodsSpecsInfoOrderService.save(goodsSpecsInfoOrder);
        return Result.ok(goodsSpecsInfoOrder.getId());
    }


    /***
     * 查询单个  周广
     * @param param
     * @return
     */
    @RequestMapping("/selectOne")
    @ResponseBody
    public Result<?> selectOne(@RequestBody IdParam param){
        Map<String, Object> params  = new HashedMap();
        params.put("goodsId",param.getId());
        List<SpecsInfoOrderVO> specsInfoOrderVOList=goodsSpecsInfoOrderService.selectOneList(params);
        return Result.ok(specsInfoOrderVOList);
    }


    /***
     * 删除 周广
     * @param param
     * @return
     */
    @RequestMapping("/delete")
    @ResponseBody
    public Result<?> delete(@RequestBody @Valid IdsParam param){
        for (String id : param.getIds()) {
            GoodsSpecsInfoOrder goodsSpecsInfoOrder=new GoodsSpecsInfoOrder();
            goodsSpecsInfoOrder.setId(id);
            goodsSpecsInfoOrderService.delete(goodsSpecsInfoOrder);
        }
        return Result.ok();
    }

}
