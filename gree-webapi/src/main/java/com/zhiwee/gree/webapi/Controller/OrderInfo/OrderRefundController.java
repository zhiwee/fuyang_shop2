package com.zhiwee.gree.webapi.Controller.OrderInfo;

import com.zhiwee.gree.model.OrderInfo.OrderRefund;
import com.zhiwee.gree.service.OrderInfo.OrderRefundService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.web.annotation.EnableDeleteRequest;
import xyz.icrab.common.web.annotation.EnableGetRequest;
import xyz.icrab.common.web.annotation.EnableListRequest;
import xyz.icrab.common.web.annotation.EnablePageRequest;

import java.util.HashMap;
import java.util.Map;


/**
 * @author sun on 2019/4/30
 */
@RestController
@RequestMapping("/orderRefund")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class OrderRefundController {


    @Autowired
    private OrderRefundService orderRefundService;


  /**
   * Description: 退款记录
   * @author: sun
   * @Date 下午5:42 2019/6/30
   * @param:
   * @return:
   */
    @RequestMapping("/pageInfo")
    public Object orderList(@RequestBody Map<String, Object> params, Pagination pagination) {
        if (params == null) {
            params = new HashMap<>();
        }
        Pageable<OrderRefund> page = orderRefundService.pageInfo(params, pagination);
        return Result.ok(page);
    }

}
