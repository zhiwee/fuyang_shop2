package com.zhiwee.gree.webapi.Controller.card;

import com.zhiwee.gree.model.card.*;
import com.zhiwee.gree.service.card.DepartmentInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import xyz.icrab.common.model.*;
import xyz.icrab.common.util.KeyUtils;
import xyz.icrab.common.web.annotation.EnableGetRequest;
import xyz.icrab.common.web.annotation.EnableListRequest;
import xyz.icrab.common.web.annotation.PermissionMode;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

@RequestMapping("/department")
@Controller
@ResponseBody
@EnableListRequest
@EnableGetRequest

/**
 * @author tzj
 * @description
 * @return
*/
public class DepartmentInfoController {

    @Autowired
    private DepartmentInfoService departmentInfoService;




    /**
     * @author tzj
     * @description 页面
     * @date 2019/12/11 12:55
     * @return
    */
    @RequestMapping(value = "/infoPage")
    @SuppressWarnings("all")
    public Result<?> infoPage(@RequestBody Map<String, Object> params, Pagination pagination) {
        if (params == null) {
            params = new HashMap<>();
        }
        if(params.get("parentids") != null && !("".equals((String)params.get("parentids")))){
            params.put("parentids",params.get("parentids"));
        }
        Pageable<DepartmentInfo> page = departmentInfoService.pageInfo(params, pagination);
        return Result.ok(page);
    }



    @RequestMapping("/addDepartment")
    @PermissionMode(PermissionMode.Mode.White)
    @ResponseBody
    @SuppressWarnings("all")
    public Result<?> addDepartment(@RequestBody Map<String, Object> param,HttpServletRequest req,HttpServletResponse res) throws Exception {
        if ((String) param.get("code") == null || "".equals ((String) param.get("code"))){
            return Result.of(Status.ClientError.BAD_REQUEST,"编码为空");
        }
        if ((String) param.get("departmentName") == null || "".equals ((String) param.get("departmentName")) ) {
            return Result.of(Status.ClientError.BAD_REQUEST,"名称为空");
        }
        if ((String) param.get("parentCode") == null || "".equals ((String) param.get("parentCode")) ) {
            return Result.of(Status.ClientError.BAD_REQUEST,"父类编码为空");
        }

        String code = (String) param.get("code");
        String parentCode = (String) param.get("parentCode");
        String name = (String) param.get("departmentName");

            //根据code查找是否含有该部门
        DepartmentInfo departmentInfo=new DepartmentInfo();
         departmentInfo.setCode(code);
        departmentInfo= departmentInfoService.getOne(departmentInfo);
        if (departmentInfo!=null){
            return Result.of(Status.ClientError.BAD_REQUEST,"编码重复");
        }
         //根据父类编码查找父类是否存在
        DepartmentInfo departmentInfo1=new DepartmentInfo();
        departmentInfo1.setCode(parentCode);
        departmentInfo1= departmentInfoService.getOne(departmentInfo1);
        if (departmentInfo1 == null){
            return Result.of(Status.ClientError.BAD_REQUEST,"父类编码未找到对应的部门");
        }
            DepartmentInfo commitD=new DepartmentInfo();
        commitD.setId(KeyUtils.getKey());
        commitD.setRank(departmentInfo1.getRank()+1);
        commitD.setCode(code);
        commitD.setParentid(departmentInfo1.getId());
        commitD.setName(name);
        commitD.setCreatetime(new Date());
        departmentInfoService.save(commitD);
        return Result.ok("成功");
    }

 /*   private Boolean checkCode(DepartmentInfo departmentInfo) {
        DepartmentInfo one = departmentInfoService.getOne(departmentInfo);
        return one==null;
    }*/

    @RequestMapping("/updateDepartment")
    @PermissionMode(PermissionMode.Mode.White)
    @ResponseBody
    @SuppressWarnings("all")
    public Result<?> updateDepartment(@RequestBody Map<String, Object> param,HttpServletRequest req,HttpServletResponse res) throws Exception {
        String newCode =null;
        String parentCode =null;
        if ((String) param.get("code") == null || "".equals ((String) param.get("code"))){
            return Result.of(Status.ClientError.BAD_REQUEST,"编码为空");
        }
        if ((String) param.get("departmentName") == null || "".equals ((String) param.get("departmentName")) ) {
            return Result.of(Status.ClientError.BAD_REQUEST,"名称为空");
        }
        if ((String) param.get("newCode") != null || !"".equals ((String) param.get("newCode")) ) {
             newCode = (String) param.get("newCode");
        }
        if ((String) param.get("parentCode") != null || !"".equals ((String) param.get("parentCode")) ) {
            parentCode = (String) param.get("parentCode");
        }
        String code = (String) param.get("code");
        String name = (String) param.get("departmentName");



        Map<String ,Object> param2=new HashMap<>();
        param2.put("code",code);
        List<DepartmentInfo> list = departmentInfoService.list(param2);
        if (list.size()>1){
            return Result.of(Status.ClientError.BAD_REQUEST,"编码重复，无法更新");
        }
        if (list.isEmpty()){
            return Result.of(Status.ClientError.BAD_REQUEST,"未找到该编码对应的部门");
        }
        if (parentCode!=null){
        if (list.get(0).getCode().equals(parentCode)){
            return Result.of(Status.ClientError.BAD_REQUEST,"编码不能自身相同");
        }
        }
        DepartmentInfo departmentInfo=new DepartmentInfo();
        if (parentCode!=null) {
            departmentInfo.setCode(parentCode);
            departmentInfo= departmentInfoService.getOne(departmentInfo);
            if (departmentInfo==null){
                return Result.of(Status.ClientError.BAD_REQUEST,"未找到父类编码对应的部门");
            }else {
                list.get(0).setParentid(departmentInfo.getId());
            }
        }

        if (newCode != null) {
            list.get(0).setCode(newCode);
        }
        list.get(0).setName(name);
        list.get(0).setUpdatetime(new Date());

        departmentInfoService.update( list.get(0));
        return Result.ok("成功");

    }


    @RequestMapping("/deleteDepartment")
    @PermissionMode(PermissionMode.Mode.White)
    @ResponseBody
    @SuppressWarnings("all")
    public Result<?> deleteDepartment(@RequestBody Map<String, Object> param,HttpServletRequest req,HttpServletResponse res) throws Exception {
        if ((String) param.get("code") != null && ! "".equals ((String) param.get("code")) ) {
            String code = (String) param.get("code");
            Map<String ,Object> param2=new HashMap<>();
            param2.put("code",code);
            List<DepartmentInfo> list = departmentInfoService.list(param2);
            if (list.isEmpty()){
                return Result.of(Status.ClientError.BAD_REQUEST,"未找到该编码对应的部门");
            }
            DepartmentInfo departmentInfo = new DepartmentInfo();
            departmentInfo.setCode(code);
            departmentInfoService.delete(departmentInfo);
            return Result.ok("成功");
        }else {
            return Result.of(Status.ClientError.BAD_REQUEST,"编码为空,删除失败");
        }
    }


        /**
         * @author tzj
         * @description 树形结构
         * @date 2019/12/12 12:46
        */
    @RequestMapping("/tree")
    public Result<?> tree(@RequestBody(required = false) Map<String, Object> params) {
        if(params==null){
            params= new HashMap<>(10);
        }
        List<DepartmentInfo> list = departmentInfoService.list(params);
        List<Tree<DepartmentInfo>> trees = new ArrayList<>(list.size());
        for (DepartmentInfo departmentInfo : list) {
            Tree<DepartmentInfo> tree = new Tree<>(departmentInfo.getId(), departmentInfo.getName(), departmentInfo.getParentid(), departmentInfo);
            trees.add(tree);
        }
        return Result.ok(trees);
    }

    @RequestMapping("/getAll")
    @PermissionMode(PermissionMode.Mode.White)
    @ResponseBody
    public Result<?> getAll() {
        List<DepartmentInfoCY> goodsCategories= departmentInfoService.listAll();
        return Result.ok(goodsCategories);
    }




}
