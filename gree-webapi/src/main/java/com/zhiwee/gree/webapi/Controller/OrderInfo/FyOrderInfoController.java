package com.zhiwee.gree.webapi.Controller.OrderInfo;


import com.zhiwee.gree.model.OrderInfo.Order;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.service.GoodsInfo.GoodsService;
import com.zhiwee.gree.service.OrderInfo.OrderService;
import com.zhiwee.gree.service.ShopUser.ShopUserService;
import com.zhiwee.gree.util.SequenceUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.util.IdGenerator;
import xyz.icrab.common.web.annotation.EnableGetRequest;
import xyz.icrab.common.web.annotation.EnableListRequest;
import xyz.icrab.common.web.annotation.EnablePageRequest;
import xyz.icrab.common.web.annotation.Session;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RequestMapping("/fyOrder")
@RestController
@ResponseBody
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
public class FyOrderInfoController {
    @Autowired
    private ShopUserService shopUserService;

    @Autowired
    private OrderService orderService;
    @Autowired
    private SequenceUtils sequenceUtils;

    /**
     * @author tzj
     * @description 测试订单
     * @date 2020/1/3 10:48
    */
    @RequestMapping("/createOrder")
    public Result<?> createOrder(@RequestBody(required = false) Order order, @Session("ShopUser") ShopUser user) {
        order.setId(IdGenerator.objectId());
        ShopUser shopUser = shopUserService.get(user.getId());
        if("".equals(shopUser.getUsername())){
            return Result.of(Status.ClientError.BAD_REQUEST, "请先绑定手机号");
        }
        if (order.getOrderItems().isEmpty()) {
            return Result.of(Status.ClientError.BAD_REQUEST, "请选择商品");
        }
        orderService.createFyOrder(order,shopUser);
        return Result.ok(order.getId());
    }
    @RequestMapping("/pageInfo")
    public Result<?> pageInfo(@RequestBody Map<String, Object> params,  Pagination pagination) {
        if (params == null) {
            params = new HashMap<>();
        }
        Pageable<Order> page = orderService.pageInfo(params, pagination);
        return Result.ok(page);
    }


}
