package com.zhiwee.gree.webapi.Controller.fySkill;


import com.alipay.api.domain.Data;
import com.alipay.api.domain.UboVO;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.User;
import com.zhiwee.gree.model.skill.HbUser;
import com.zhiwee.gree.model.skill.Skillhb;
import com.zhiwee.gree.service.skill.HbUserService;
import com.zhiwee.gree.service.skill.SkillhbService;
import com.zhiwee.gree.webapi.util.DateUtil;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.util.IdGenerator;
import xyz.icrab.common.web.annotation.*;
import xyz.icrab.common.web.param.IdParam;
import xyz.icrab.common.web.param.IdsParam;
import xyz.icrab.ums.util.consts.SessionKeys;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@RestController
@RequestMapping("/skillhb")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class SkillhbController {


    @Autowired
    private SkillhbService skillhbService;

    @Autowired
    private HbUserService hbUserService;


    /**
     * @Author: jick
     * @Date: 2019/12/9 21:31
     * 制作秒杀红包
     */
    @RequestMapping("/createOrUpdate")
    @ResponseBody
    public Result<T> madeSkillController(@RequestBody Skillhb skillhb) {


        //如果id就是更新操作，否则是添加操作

        if (skillhb.getId() == "" || null == skillhb.getId() || skillhb.getId().trim().length() == 0) {
            //时间格式化
            Date data = new Date();
            //    SimpleDateFormat format = new SimpleDateFormat("yyyyMMddhhmmss");
            //封装实体类
            skillhb.setId(DateUtil.date2Str(data));
            skillhb.setCreatTime(data);
            skillhb.setState(1);

            skillhbService.save(skillhb);
        } else {//更新操作
            skillhb.setState(1);
            skillhbService.update(skillhb);
        }


        return Result.ok();


    }


    /**
     * @Author: jick
     * @Date: 2019/12/9 22:56
     * 查询红包池列表
     */
    @RequestMapping("/list")
    @ResponseBody
    public Result<?> queryListService(@RequestBody(required = false) Map<String, Object> params, Pagination pagination) {
        Pageable<Skillhb> list = skillhbService.queryList(params, pagination);
        return Result.ok(list);
    }


    /**
     * @Author: jick
     * @Date: 2019/12/10 0:47
     * 启用与禁用
     */
    @RequestMapping("/openOrstop")
    @ResponseBody
    public Result<?> openOrStopController(@RequestBody Skillhb skillhb) {
        skillhbService.update(skillhb);
        return Result.ok();
    }


    /**
     * @Author: jick
     * @Date: 2019/12/10 9:49
     * 删除红包
     */
    @RequestMapping("/delete")
    @ResponseBody
    public Result<?> deleteController(@RequestBody @Valid IdsParam map) {

        List<String> ids = map.getIds();
        Skillhb skillhb = new Skillhb();
        for (String id : ids) {
            skillhb.setId(id);
            //activityPageService.update(activityPage);
            skillhbService.delete(skillhb);
        }
        return Result.ok();

    }

    /**
     * @Author: jick
     * @Date: 2019/12/10 9:58
     * 根据id跳转到商品的详情页
     */
    @RequestMapping("/get")
    @ResponseBody
    public Result<?> queryByIdController(@RequestBody @Valid IdParam map) {
        Skillhb skillhb = skillhbService.get(map.getId());
        return Result.ok(skillhb);
    }


    /**
     * @Author: jick
     * @Date: 2019/12/11 20:46
     * 前台展示红包（更具时间检索，展示的仅仅是红包池）
     */
    @RequestMapping("/getHbPoll")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> queryHbPollByTime(@RequestBody(required = false) Map<String, Object> param) {
        param.put("state", 1);
        List<Skillhb> list = skillhbService.queryHbByTime(param);
        return Result.ok(list);
    }


    /**
     * @Author: jick
     * @Date: 2019/12/11 21:13
     * 抢红包
     */
    @RequestMapping("/getHb")
    @ResponseBody
    public Result<?> getHb(@Session("ShopUser") ShopUser shopUser, @RequestBody Skillhb skillhb) {

        Lock lock = new ReentrantLock();

        lock.lock();//加锁
        try {
            //查询数据库，判断红包数目
            Skillhb gethb = skillhbService.get(skillhb.getId());

            if(null ==shopUser.getPhone()  || shopUser.getPhone().trim()==""){
                return Result.of("207", "请先填写手机号等基本信息", null);
            }

            if (gethb.getCount() == 0) {
                return Result.of("208", "红包以抢完,择日再抢", null);
            } else {


                //查询领取表，看看该用户是否已经领取过了
                HbUser h = new HbUser();

                h.setPhoneNum(shopUser.getPhone());
                HbUser gethbUser = hbUserService.getOne(h);
                if (null != gethbUser) {
                    return Result.of("209", "红包已领取过", null);
                }


                //否则领取红包
                //红包数目减1
                gethb.setCount(gethb.getCount() - 1);
                skillhbService.update(gethb);
                //记录领取记录
                HbUser hbUser = new HbUser();
                hbUser.setStartTime(gethb.getStartTime());
                hbUser.setEndTime(gethb.getEndTime());
                hbUser.setPrice(gethb.getPrice());
                SignGetHbInfo(hbUser, shopUser);
                return Result.of("200", "秒杀成功，请12月31日晚8:00之后凭秒杀成功的截图或者姓名/手机号码到阜阳商厦现场领取电子券",null);
            }
        } finally {
            lock.unlock();//解锁

        }

    }


    /**
     * @Author: jick
     * @Date: 2019/12/12 10:44
     * 记录红包领取信息
     */
    public void SignGetHbInfo(HbUser hbUser, ShopUser shopUser) {
        Date date = new Date();
        hbUser.setId(IdGenerator.uuid());
        hbUser.setHbNumber(DateUtil.date2Str(date));
        hbUser.setUserId(shopUser.getId());
        hbUser.setNickName(shopUser.getNickname());
        hbUser.setPhoneNum(shopUser.getPhone());
        hbUser.setGetTime(date);
        hbUser.setIsUsed(2);
        //存入到数据库
        hbUserService.save(hbUser);
    }
    
    
    
    /**
     * @Author: jick
     * @Date: 2019/12/12 19:26
     * 查询用户领取红包的记录
     */

    @RequestMapping("/getInfo")
    @PermissionMode(PermissionMode.Mode.White)
    public  Result<?>  getUserHbInfo(@RequestBody(required = false)  Map<String,Object> param){

        List<HbUser>  list = hbUserService.list(param);
        return   Result.ok(list);

    }



    /**
     * @Author: jick
     * @Date: 2019/12/13 10:07
     * 确认红包是否已经被使用
     * 需要穿穿id过来
     */
    @RequestMapping("/useHb")
   // @PermissionMode(PermissionMode.Mode.White)
    public  Result<?>  UseHb(@RequestBody  HbUser hbUser,@Session(SessionKeys.USER) User user){
         hbUser.setIsUsed(1);
         //记录核销员的信息
         hbUser.setDistributorName(user.getUsername());
         hbUser.setDistributorNickName(user.getNickname());
         hbUserService.update(hbUser);
        return   Result.ok();
    }
    
    
    /**
     * @Author: jick
     * @Date: 2019/12/13 10:24
     * 用户领取的红包记录,微信小程序展示
     */
    @RequestMapping("/getUseHbList")
  //  @PermissionMode(PermissionMode.Mode.White)
    public  Result<?> getHbInfo(@Session("ShopUser") ShopUser shopUser){
      //  ShopUser  shopUser  = new ShopUser();
       // shopUser.setId("5def804ce6abd93a6c1ac41f");
         //skillhbService.
        Map<String,Object> param = new HashMap<>();
        param.put("userId",shopUser.getId());
         List<HbUser>  hbUserList  =  hbUserService.queryGetHbList(param);
         return   Result.ok(hbUserList);
    }


    /**
     * @Author: jick
     * @Date: 2019/12/17 12:49
     * 后台显示红包领取详情
     */

    @RequestMapping("/portalGetUseHbList")
    @PermissionMode(PermissionMode.Mode.White)
    public  Result<?> portalGetHbInfo(@RequestBody Map<String,Object> param,Pagination pagination){
        Pageable<HbUser>  hbUserList  =  hbUserService.queryList(param,pagination);
        return   Result.ok(hbUserList);
    }

    
    
    
    
    /**
     * @Author: jick
     * @Date: 2019/12/19 10:59
     * 每个用户查询红包领取金额
     */
    @RequestMapping("/getHbPrice")
      //@PermissionMode(PermissionMode.Mode.White)
    public  Result<?> getHbInfo2(@Session("ShopUser") ShopUser shopUser){
    //public  Result<?> getHbInfo2(){
        //ShopUser shopUser  = new ShopUser();
        //shopUser.setId("5dfae12e155f8249aa187e64");
        Map<String,Object> param = new HashMap<>();
        param.put("userId",shopUser.getId());
        HbUser hbUser  =    hbUserService.getOne(param);
        return   Result.ok(hbUser);
    }










}
