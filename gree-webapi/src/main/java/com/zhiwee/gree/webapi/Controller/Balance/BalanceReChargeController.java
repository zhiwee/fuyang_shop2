package com.zhiwee.gree.webapi.Controller.Balance;

import com.alipay.api.AlipayApiException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zhiwee.gree.model.Balance.BalanceReCharge;
import com.zhiwee.gree.model.BaseInfo.BaseInfo;
import com.zhiwee.gree.model.FyGoods.vo.FygoodsCartVo;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.WxPay.WeChatConfig;
import com.zhiwee.gree.model.WxPay.WeChatParams;
import com.zhiwee.gree.service.Balance.BalanceReChargeService;
import com.zhiwee.gree.service.BaseInfo.BaseInfoService;
import com.zhiwee.gree.service.ShopUser.ShopUserService;
import com.zhiwee.gree.webapi.util.HttpUtil;
import com.zhiwee.gree.webapi.util.PayForUtil;
import com.zhiwee.gree.webapi.util.WeixinPay;
import com.zhiwee.gree.webapi.util.XMLUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.util.IdGenerator;
import xyz.icrab.common.util.KeyUtils;
import xyz.icrab.common.web.annotation.*;
import xyz.icrab.common.web.param.IdParam;
import xyz.icrab.common.web.util.Validator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;


/**
 * @author sun on 2019/6/24
 */
@RestController
@RequestMapping("/balanceReCharge")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class BalanceReChargeController {

    @Autowired
    private BalanceReChargeService balanceReChargeService;

    @Autowired
    private ShopUserService shopUserService;
    @Autowired
    private BaseInfoService baseInfoService;

    /**
     * Description: 查询所有的记录
     *
     * @author: sun
     * @Date 下午11:19 2019/6/24
     * @param:
     * @return:
     */
    @RequestMapping({"/page"})
    public Result<Pageable<BalanceReCharge>> page(@RequestBody(required = false) Map<String, Object> param, Pagination pagination) {
        return Result.ok(balanceReChargeService.pageInfo(param, pagination));
    }


    /**
     * @author tzj
     * @description 增加佣金
     * @date 2020/4/1 9:30
     */
    @RequestMapping("/addBalance")
   @PermissionMode(PermissionMode.Mode.White)
    public Result<?> addBalance(HttpServletRequest request, HttpServletResponse response,
                                @RequestBody  ShopUser shopUser) {
        shopUser= shopUserService.getOne(shopUser);
        List<BaseInfo> list = baseInfoService.list();
        Double sendBalance = list.get(0).getSendBalance();
        if (sendBalance<0){
            return null;
        }
        BalanceReCharge balanceReCharge = new BalanceReCharge();
        balanceReCharge.setId(KeyUtils.getKey());
        balanceReCharge.setAmount(sendBalance);
        balanceReCharge.setCreateTime(new Date());
        balanceReCharge.setUserId(shopUser.getId());
        balanceReCharge.setOldBalance(shopUser.getBalance());
        balanceReCharge.setNewBalance(shopUser.getBalance()+sendBalance);
        //分享获得积分
        balanceReCharge.setType(3);
        balanceReChargeService.save(balanceReCharge);

        shopUser.setBalance(sendBalance+shopUser.getBalance());
        shopUserService.update(shopUser);

        return Result.ok();
    }


}
