package com.zhiwee.gree.webapi.Controller.fyGoods;


import com.zhiwee.gree.model.FyGoods.BargainGoods;
import com.zhiwee.gree.model.FyGoods.BargainOrder;
import com.zhiwee.gree.model.FyGoods.FyOrder;
import com.zhiwee.gree.model.FyGoods.FyOrderItem;
import com.zhiwee.gree.model.GoodsInfo.Goods;
import com.zhiwee.gree.model.GoodsInfo.GoodsBase;
import com.zhiwee.gree.model.GoodsInfo.GoodsInfoPicture;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.WxPay.WeChatConfig;
import com.zhiwee.gree.service.FyGoods.*;
import com.zhiwee.gree.service.GoodsInfo.GoodsBaseService;
import com.zhiwee.gree.service.GoodsInfo.GoodsInfoPictureService;
import com.zhiwee.gree.service.GoodsInfo.GoodsService;
import com.zhiwee.gree.service.ShopUser.ShopUserService;
import com.zhiwee.gree.webapi.util.HttpUtil;
import net.sf.json.JSONObject;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.util.KeyUtils;
import xyz.icrab.common.web.annotation.*;
import xyz.icrab.common.web.param.IdParam;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.rmi.MarshalledObject;
import java.util.*;

/**
 * @author DELL
 */
@RestController
@RequestMapping("/bargainGoods")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class BargainGoodsController {
    @Resource
    private FyOrderService fyOrderService;
    @Resource
    private GoodsBaseService goodsBaseService;
    @Resource
    private FyOrderItemService fyOrderItemService;
    @Resource
    private GoodsService goodsService;
    @Resource
    private BargainGoodsService bargainGoodsService;
    @Resource
    private GoodsInfoPictureService goodsInfoPictureService;
    @Resource
    private BargainOrderService bargainOrderService;
    @Resource
    private ShopUserService shopUserService;
    /**
     * @author tzj
     * @description 获取列表
     * @date 2020/4/26 8:56
     */
    @RequestMapping("/list")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> getFyGoodsList(Pagination pagination) {
        Map<String, Object> param = new HashMap<>(10);
        param.put("state",1);
        List<BargainGoods> list = bargainGoodsService.queryGoodsList(param,pagination);
        if (CollectionUtils.isNotEmpty(list)){
            for (BargainGoods next : list) {
                Goods goods = goodsService.get(next.getGoodsId());
                if (goods!=null){
                    Map<String, Object> params=new HashMap<>(5);
                    params.put("goodsId",goods.getGoodsId());
                    List<String> list1=new ArrayList<>(10);
                    List<GoodsInfoPicture> goodsInfoPictures = goodsInfoPictureService.listAll(params);
                    if (CollectionUtils.isNotEmpty(goodsInfoPictures)){
                        for (GoodsInfoPicture goodsInfoPicture : goodsInfoPictures) {
                            list1.add(goodsInfoPicture.getImgUrl());
                        }
                    }
                    GoodsBase goodsBase = goodsBaseService.get(goods.getGoodsId());
                    goods.setGoodsBase(goodsBase);
                    goods.setUrlList(list1);
                    next.setGoods(goods);
                    next.setGoodCover(goodsBase.getGoodCover());
                }
            }
        }
        return Result.ok(list);
    }

    @RequestMapping("/list2")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> getFyGoodsList2(Pagination pagination) {
        Map<String, Object> param = new HashMap<>(10);
        Pageable<BargainGoods> list = bargainGoodsService.page(param, pagination);
        return Result.ok(list);
    }

    /**
     * @author tzj
     * @description 新增编辑商品
     * @date 2020/4/26 8:57
     */
    @RequestMapping("/saveOrUpdate")
    public Result<?> skillGoodsSaveController(@RequestBody BargainGoods grouponGoods) {
        if (null == grouponGoods.getLimitCount()) {
            grouponGoods.setLimitCount(1);
        }
        //如果id为空，则是添加操作
        if (grouponGoods.getId() == null || "".equals(grouponGoods.getId().trim()) || grouponGoods.getId().trim().length() == 0) {
            //确保同一时间段只有一个商品
            Boolean result = checkOnly(grouponGoods);
            if (!result){
                return Result.of(Status.ClientError.BAD_REQUEST,"与已经存在的拼团商品冲突,请检查");
            }
            grouponGoods.setId(KeyUtils.getKey());
            grouponGoods.setState(1);
            bargainGoodsService.save(grouponGoods);
        } else {
            //如果id不为空则是更新操作
            //确保同一时间段只有一个商品
            Boolean result = checkOnly(grouponGoods);
            if (!result){
                return Result.of(Status.ClientError.BAD_REQUEST,"与已经存在的拼团商品冲突,请检查");
            }
            grouponGoods.setState(1);
            bargainGoodsService.update(grouponGoods);
        }
        return Result.ok(grouponGoods);
    }

    /**
     * @author tzj
     * @description
     * @date 2020/4/26 15:25
     */
    private Boolean checkOnly(BargainGoods grouponGoods) {
        Map<String,Object> param=new HashMap<>(6);
        param.put("goodsId",grouponGoods.getGoodsId());
        List<BargainGoods> list = bargainGoodsService.list(param);
        //是否为空
        if (CollectionUtils.isNotEmpty(list)) {
            //大于1直接返回
            if (list.size()>1){
                return false;
            }
            BargainGoods groupGoods1 = list.get(0);
            //id相同
            //如果开始时间在他们中间则不行
            return grouponGoods.getId().equals(groupGoods1.getId());

        }else {
            return true;
        }
    }


    /**
     * @author tzj
     * @description 通过ID查询信息
     * @date 2020/4/26 8:58
     */
    @RequestMapping("/get")
    public Result<?> get(@RequestBody IdParam param) {
        BargainGoods grouponGoods = bargainGoodsService.get(param.getId());
        return Result.ok(grouponGoods);
    }

    /**
     * @author tzj
     * @description 删除
     * @date 2020/4/26 13:20
     */
    @RequestMapping("/delete")
    public Result<?> fyGoodsDelete(@RequestBody IdParam idParam) {
        //删除商品表的数据
        BargainGoods grouponGoods = new BargainGoods();
        grouponGoods.setId(idParam.getId());
        bargainGoodsService.delete(grouponGoods);
        return Result.ok();
    }



    /**
     * @author tzj
     * @description 发送订阅消息
     * @date 2020/4/27 10:12
    */
    private    void   getMsg( String id,String orderNum ){
        ShopUser shopUser = shopUserService.get(id);
        String grant_type="client_credential";
        String appid= WeChatConfig.APPIDFuYang;
        String secret=  WeChatConfig.APIKEYFuYang;
        String url="https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&&appid=wx575dba0ef8ff034f&&secret=7f7f89ad5023ce314a4ba5b6496201b8";
        String resultMap = HttpUtil.sendGet(url);
        if (null == resultMap) {
           return;
        }
        Map<Object, Object> result1 = jsonToMap(resultMap);
        String access_token= (String) result1.get("access_token");
        String data="{  \"touser\": \""+shopUser.getOpenId()+"\",\n" +
                "\"page\": \"pages/bargainshare/bargainshare?kjshare=1&cgtype=1&orderNum="+orderNum+"\",\n" +
                "  \"template_id\": \"gUjzbZvamMbCySz9fLAtCp7ybVJkOn2q6KcEy6r0XhI\",\n" +
                "  \"miniprogram_state\":\"developer\",\n" +
                "  \"lang\":\"zh_CN\",\n" +
                "  \"data\": {\n" +
                "      \"thing2\": {\n" +
                "          \"value\": \"恭喜您在阜阳商厦砍价成功\"\n" +
                "      },\n" +
                "      \"thing7\": {\n" +
                "          \"value\": \"请前往砍价列表支付订单\"\n" +
                "      }\n" +
                "  }}";
        String url2= "https://api.weixin.qq.com/cgi-bin/message/subscribe/send?access_token="+access_token;
        System.out.println(access_token);
        String s = HttpUtil.doPost(url2, data, 4000);
        System.out.println(s);
    }

    /**
     * json string 转换为 map 对象
     * @param jsonObj
     * @return
     */
    public static Map<Object, Object> jsonToMap(Object jsonObj) {
        JSONObject jsonObject = JSONObject.fromObject(jsonObj);
        Map<Object, Object> map = (Map)jsonObject;
        return map;
    }

    /**
     * @author tzj
     * @description
     * @date 2020/4/10 11:41
     */
    @RequestMapping("/bargainOrder")
    public Result<?> bargainOrder(@Session("ShopUser") ShopUser shopUser, @RequestBody(required = false) Map<String, Object> param) {
        String orderNum = (String) param.get("orderNum");
        if (StringUtils.isEmpty(orderNum)) {
            return Result.of(Status.ClientError.BAD_REQUEST, "参数错误，请重新打开链接");
        }
        shopUser=  shopUserService.get(shopUser.getId());
        List<BargainOrder> list = bargainOrderService.list(param);
       Boolean flg= checkOnlyOnce(shopUser,list);
       if (flg){
           return Result.of(Status.ClientError.BAD_REQUEST, "您已经参加过本次砍价了");
       }
        if (CollectionUtils.isEmpty(list) || list.size() < 3) {
            FyOrder fyOrder = new FyOrder();
            fyOrder.setOrderNum(orderNum);
            fyOrder = fyOrderService.getOne(fyOrder);
            //找到订单
            if (fyOrder != null) {
                //找商品
                FyOrderItem fyOrderItem = new FyOrderItem();
                fyOrderItem.setOrderNum(orderNum);
                fyOrderItem = fyOrderItemService.getOne(fyOrderItem);
                Integer state=0;
                if (fyOrderItem != null) {
                    BargainGoods bargainGoods = bargainGoodsService.get(fyOrderItem.getGoodsId());
                    //商品最低价
                    BigDecimal lowPrice = bargainGoods.getLowPrice();
                    //本次砍价金额
                    BigDecimal  subtract=new BigDecimal(0);
                    //订单价格
                    BigDecimal  payPrice=new BigDecimal(0);
                    //已经砍价金额
                    BigDecimal hasBargain=new BigDecimal(0);
                    //如果是第三个人
                    if (list.size() == 2) {
                        for (BargainOrder bargainOrder : list) {
                            hasBargain=hasBargain.add(bargainOrder.getPrice());
                        }
                        state=2;
                        ShopUser shopUser1 = shopUserService.get(fyOrder.getUserId());
                        //发消息通知
                        getMsg(shopUser1.getId(),fyOrder.getOrderNum());
                        //得出本次砍价的价格
                         subtract = bargainGoods.getPrice().subtract(lowPrice).subtract(hasBargain);
                        payPrice=lowPrice;

                    }else if (list.size()==1){
                        state=1;
                      BigDecimal  canPrice = bargainGoods.getPrice().subtract(list.get(0).getPrice());
                      //取随机数
                        payPrice  = getSubtract(canPrice,lowPrice);
                        subtract=bargainGoods.getPrice().subtract(list.get(0).getPrice()).subtract(payPrice);
                    }else {
                        ShopUser shopUser1 = shopUserService.get(fyOrder.getUserId());
                        getMsg(shopUser1.getId(),fyOrder.getOrderNum());
                        state=1;
                        payPrice= getSubtract(bargainGoods.getPrice(),lowPrice);
                        subtract =bargainGoods.getPrice().subtract(payPrice);
                    }
                    BargainOrder bargainOrder=new BargainOrder();
                    bargainOrder.setCreateTime(new Date());
                    bargainOrder.setId(KeyUtils.getKey());
                    bargainOrder.setOrderNum(orderNum);
                    bargainOrder.setPrice(subtract);
                    bargainOrder.setUserId(shopUser.getId());
                    bargainOrder.setState(state);
                    bargainOrder.setPassword(shopUser.getPassword());
                    bargainOrder.setNickname(shopUser.getNickname());
                    bargainOrderService.save(bargainOrder);
                    fyOrder.setPayPrice(payPrice);
                    fyOrderItem.setPayPrice(payPrice);
                    fyOrderService.update(fyOrder);
                    fyOrderItemService.update(fyOrderItem);
                    return Result.ok(bargainOrder);
                }
            }
        }
        return Result.of(Status.ClientError.BAD_REQUEST, "未找到当前砍价记录");
    }

    private Boolean checkOnlyOnce(ShopUser shopUser, List<BargainOrder> list) {
        for (BargainOrder bargainOrder : list) {
            if (bargainOrder.getUserId().equals(shopUser.getId())){
                return true;
            }
        }
        return false;
    }

    private BigDecimal getSubtract(BigDecimal canPrice,BigDecimal lowPrice) {
        float minF = lowPrice.floatValue();
        float maxF = canPrice.floatValue();
        //生成随机数
        BigDecimal db = new BigDecimal(Math.random() * (maxF - minF) + minF);
        //返回保留两位小数的随机数。不进行四舍五入
        return db.setScale(2,BigDecimal.ROUND_DOWN);
    }


    /**
     * @author tzj
     * @description 前端获取
     * @date 2020-05-22 16:52
    */
    @RequestMapping("/getList")
    public Result<?> getList(@Session("ShopUser") ShopUser shopUser, @RequestBody(required = false) Map<String, Object> param) {
        if (param==null){
            param=new HashMap<>(10);
        }
            param.put("type",6);
        Integer giveType= (Integer) param.get("giveType");
        if (giveType==1) {
            param.put("userId", shopUser.getId());
        }

        List<FyOrderItem> list = fyOrderItemService.list(param);
        if (CollectionUtils.isNotEmpty(list)) {
            for (FyOrderItem fyOrderItem : list) {
                BargainGoods bargainGoods = bargainGoodsService.get(fyOrderItem.getGoodsId());
                Goods goods = goodsService.get(bargainGoods.getGoodsId());
                GoodsBase goodsBase = goodsBaseService.get(goods.getGoodsId());
                Map<String, Object> params = new HashMap<>(5);
                params.put("orderNum", fyOrderItem.getOrderNum());
                List<BargainOrder> list1 = bargainOrderService.list(params);
                fyOrderItem.setCover(goods.getGoodCover());
                fyOrderItem.setBargainGoods(bargainGoods);
                fyOrderItem.setBargainOrders(list1);
                fyOrderItem.setSendType(goodsBase.getSendType());
                fyOrderItem.setLowPrice(bargainGoods.getLowPrice());
            }
        }
        return Result.ok(list);
    }


    @RequestMapping("/getBargain")
    public Result<?> getBargain( @RequestBody(required = false) Map<String, Object> param) {
        List<BargainOrder> list = bargainOrderService.list(param);
        return Result.ok(list);
    }





}
