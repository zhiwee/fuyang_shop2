package com.zhiwee.gree.webapi.Controller.Log;

import com.zhiwee.gree.model.Log.Log;
import com.zhiwee.gree.service.log.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.web.annotation.*;

import java.util.HashMap;
import java.util.Map;


/**
 * @author sun
 * @since 2019/4/28 17:11
 */
@RestController
@RequestMapping("/log")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class LogController {

    @Autowired
    private LogService logService;


    /**
     * Description: 查询日志
     * @author: sun
     * @Date 上午11:36 2018/12/18
     * @param:
     * @return:
     */
    @RequestMapping({"/page"})
    @ResponseBody
    public Result<Pageable<Log>> page(@RequestBody(required = false) Map<String, Object> param, Pagination pagination) {
        return Result.ok(logService.page(param, pagination));
    }


    /**
     * Description: 查询商城前台日志
     * @author: sun
     * @Date 上午11:36 2018/12/18
     * @param:
     * @return:
     */
    @RequestMapping({"/loginLog"})
    @ResponseBody
    public Result<Pageable<Log>> loginLog(@RequestBody(required = false) Map<String, Object> param, Pagination pagination) {
        if(param == null){
            param = new HashMap<>();
        }
        param.put("requestMethod","2");
        return Result.ok(logService.page(param, pagination));
    }



}
