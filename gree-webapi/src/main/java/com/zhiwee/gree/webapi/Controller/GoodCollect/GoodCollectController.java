package com.zhiwee.gree.webapi.Controller.GoodCollect;


import com.zhiwee.gree.model.Dealer.DealerInfoBase;
import com.zhiwee.gree.model.GoodCollect.GoodCollect;
import com.zhiwee.gree.model.ShopDistributor.ShopDistributor;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.service.Dealer.DealerInfoBaseService;
import com.zhiwee.gree.service.GoodCollect.GoodCollectService;
import com.zhiwee.gree.service.ShopDistributor.DistributorGradeService;
import com.zhiwee.gree.service.ShopDistributor.ShopDistributorService;
import com.zhiwee.gree.webapi.util.SystemControllerLog;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import xyz.icrab.common.model.*;
import xyz.icrab.common.model.enums.YesOrNo;
import xyz.icrab.common.util.IdGenerator;
import xyz.icrab.common.web.annotation.EnableGetRequest;
import xyz.icrab.common.web.annotation.EnableListRequest;
import xyz.icrab.common.web.annotation.PermissionMode;
import xyz.icrab.common.web.annotation.Session;
import xyz.icrab.common.web.param.IdParam;
import xyz.icrab.common.web.param.IdsParam;
import xyz.icrab.common.web.util.Validator;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.*;

@RequestMapping("/goodCollect")
@Controller
@ResponseBody
@EnableListRequest
@EnableGetRequest
public class GoodCollectController {
    @Resource
    private GoodCollectService goodCollectService;


/**
 * Description: 获取个人收藏
 * @author: sun
 * @Date 下午6:59 2019/7/3
 * @param:
 * @return:
 */
    @RequestMapping("/infoPage")
    public Result<?> pageDealer( Pagination pagination,@Session("ShopUser") ShopUser user) {
    Map<String, Object> param = new HashMap<>();
        param.put("userId",user.getId());
        Pageable<GoodCollect> pageable = goodCollectService.infoPage(param, pagination);
        return Result.ok(pageable);
    }




    /**
     * Description: 加入收藏
     * @author: sun
     * @Date 下午6:59 2019/7/3
     * @param:
     * @return:
     */
    @RequestMapping("/addGoodCollect")
    public Result<?> addGoodCollect(@RequestBody GoodCollect goodCollect ,@Session("ShopUser") ShopUser user) {
        Validator validator = new Validator();
        validator.notNull(goodCollect.getGoodId(), "商品id不能为空");
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }
        goodCollect.setId(IdGenerator.objectId());
        goodCollect.setUserId(user.getId());
        goodCollect.setCreateTime(new Date());
        goodCollectService.save(goodCollect);
        return Result.ok();
    }


    /**
     * Description: 删除收藏
     * @author: sun
     * @Date 下午6:59 2019/7/3
     * @param:
     * @return:
     */
    @RequestMapping("/deleteGoodCollect")
    public Result<?> deleteGoodCollect(@RequestBody GoodCollect goodCollect ,@Session("ShopUser") ShopUser user) {
        Validator validator = new Validator();
        validator.notNull(goodCollect.getId(), "id不能为空");
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }
        goodCollectService.delete(goodCollect);
        return Result.ok();
    }






}
