package com.zhiwee.gree.webapi.Controller.Shop;

import com.zhiwee.gree.model.Shop.ShopArea;
import com.zhiwee.gree.service.Shop.ShopAreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.web.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author jiake
 * @date 2020-02-19 13:15
 */
@RestController
@RequestMapping("/shopArea")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class ShopAreaController {


    @Autowired
    private ShopAreaService shopAreaService;
    
    
    /** 
     * @description:  
     获取地址
     * @return:  
     * @author: jick
     * @date: 2020-02-19 16:43 
     */
    @RequestMapping("/list")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> getAreaController (Integer parentId){

        Map<String,Object>  param = new HashMap<>();

        param.put("parentId",parentId);

        return   Result.ok(shopAreaService.list(param));


    }
    
    
    
}
