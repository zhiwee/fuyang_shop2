package com.zhiwee.gree.webapi.Controller.User;

import com.zhiwee.gree.model.*;
import com.zhiwee.gree.model.consts.Constant;
import com.zhiwee.gree.model.enums.UserType;
import com.zhiwee.gree.model.ret.LoginReturnMsg;
import com.zhiwee.gree.model.ret.UserReturnMsg;
import com.zhiwee.gree.service.shopBase.InterfaceService;
import com.zhiwee.gree.service.shopBase.MenuService;
import com.zhiwee.gree.service.shopBase.RoleService;
import com.zhiwee.gree.service.shopBase.UserService;
import com.zhiwee.gree.webapi.Controller.shopBase.param.LoginParam;
import com.zhiwee.gree.webapi.Controller.shopBase.vo.UserProfile;
import com.zhiwee.gree.webapi.util.SystemControllerLog;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.jeecgframework.poi.excel.ExcelExportUtil;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.enmus.ExcelType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.model.enums.Gender;
import xyz.icrab.common.model.enums.YesOrNo;
import xyz.icrab.common.web.annotation.*;
import xyz.icrab.common.web.consts.HeaderNames;
import xyz.icrab.common.web.message.ValidatedMessage;
import xyz.icrab.common.web.param.IdParam;
import xyz.icrab.common.web.param.IdsParam;
import xyz.icrab.common.web.session.SessionService;
import xyz.icrab.common.web.support.session.DefaultSession;
import xyz.icrab.common.web.util.KeyUtils;
import xyz.icrab.common.web.util.Validator;

import xyz.icrab.ums.util.consts.SessionKeys;

import java.io.IOException;
import java.io.OutputStream;
import java.util.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

/**
 * @author Knight
 * @since 2018/4/12 17:11
 */
@RestController
@RequestMapping("/user")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private SessionService sessionService;

    @Autowired
    private MenuService menuService;

    @Autowired
    private InterfaceService interfaceService;


    @Autowired
    private RoleService roleService;



    /**
     * Description: 获取一个用户信息
     * @author: sun
     * @Date 上午10:32 2019/4/29
     * @param:
     * @return:
     */
    @RequestMapping({"/searchOne"})
    public Result<?> selectOne(@RequestBody(required = false) Map<String, Object> param) {
        return Result.ok(userService.searchOne(param));
    }


/**
 * Description: 获取所有用户的角色
 * @author: sun
 * @Date 上午10:32 2019/4/29
 * @param:
 * @return:
 */

    @RequestMapping({"/page"})
    public Result<Pageable<User>> page(@RequestBody(required = false) Map<String, Object> param, Pagination pagination,
                                       @Session(SessionKeys.USER) User user) {
        return Result.ok(userService.page(param, pagination));
    }
    @RequestMapping({"/dealerPage"})
    public Result<Pageable<User>> dealerPage(@RequestBody(required = false) Map<String, Object> param, Pagination pagination,
                                       @Session(SessionKeys.USER) User user) {
        param.put("type",3);
        return Result.ok(userService.page(param, pagination));
    }
    /**
     * Description:  根据当前用户查角色
     * @author: sun
     * @Date 上午11:22 2019/4/4
     * @param:
     * @return:
     */

    @RequestMapping("/getMyRoleName")
    public Result<?> getMyRoleName(@Session(SessionKeys.USER) com.zhiwee.gree.model.User user) {
        Map<String, Object> param = new HashMap<>();
        param.put("sid",user.getId());
        com.zhiwee.gree.model.User roleUser =  userService.getMyRoleName(param);
        return Result.ok(roleUser);
    }

    @RequestMapping("/add")
    public Result<?> add(@RequestBody User user) {
        Validator validator = new Validator();
        validator.notEmpty(user.getUsername(), UserReturnMsg.EMPTY_USERNAME.message());
        validator.notEmpty(user.getPassword(), UserReturnMsg.EMPTY_PASSWORD.message());
        validator.notEmpty(user.getAddRoleIds(), UserReturnMsg.EMPTY_ROLE.message());
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }

        user.setId(KeyUtils.getKey());
        user.setState(YesOrNo.YES);
        if(user.getDistributorId() == null   || user.getDistributorId().equals("") ){
            user.setType(UserType.System);
        }else{
            user.setType(UserType.Retailer);
        }
        user.setPassword(Constant.DEFAULT_PASSWORD);
        if (user.getGender() == null) {
            user.setGender(Gender.Unknown);
        }

        userService.save(user);
         List<String> roleList = Arrays.asList(user.getAddRoleIds().split(","));
        roleService.resetUserRole(user.getId(),roleList);
        return Result.ok();
    }


    @RequestMapping("/update")
    public Result<?> update(@RequestBody User user) {
        Validator validator = new Validator();
        validator.notEmpty(user.getId(), ValidatedMessage.EMPTY_ID);
        validator.notEmpty(user.getUsername(), UserReturnMsg.EMPTY_USERNAME.message());
        validator.notEmpty(user.getPassword(), UserReturnMsg.EMPTY_PASSWORD.message());
        validator.notEmpty(user.getAddRoleIds(), UserReturnMsg.EMPTY_ROLE.message());
       // validator.in(user.getType(), Arrays.asList(UserType.System.value(), UserType.Retailer.value(), UserType.Super.value()), UserReturnMsg.VALID_USERTYPE.message());

        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }
        if(user.getDistributorId() == null   || user.getDistributorId().equals("") ){
            user.setType(UserType.System);
        }else{
            user.setType(UserType.Retailer);
        }
        if (user.getGender() == null) {
            user.setGender(Gender.Unknown);
        }

        userService.updateAndDelete(user);
        List<String> roleList = Arrays.asList(user.getAddRoleIds().split(","));
        roleService.resetUserRole(user.getId(),roleList);
        return Result.ok();
    }


    @RequestMapping("/open")
    @ResponseBody
    public <T> Result<?> open(@RequestBody @Valid IdParam map) {
        User user = new User();
        user.setId(map.getId());
        user.setState(YesOrNo.YES);
        userService.update(user);
        return Result.ok();
    }


    @RequestMapping("/stop")
    @ResponseBody
    public <T> Result<?> stop(@RequestBody @Valid IdParam map) {
        User user = new User();
        user.setId(map.getId());
        user.setState(YesOrNo.NO);
        userService.update(user);
        return Result.ok();
    }


    @RequestMapping("/modifyPwd")
    public Result<?> modifyPwd(@RequestBody Map<String, Object> params, @Session(SessionKeys.USER) User user) {
        Validator validator = new Validator();
        validator.notEmpty((String) params.get("password"), UserReturnMsg.EMPTY_USERNAME.message());
        validator.notEmpty((String) params.get("checkPwd"), UserReturnMsg.EMPTY_USERNAME.message());
        validator.minLength((String) params.get("password"),6, UserReturnMsg.EMPTY_USERNAME.message());
        validator.maxLength((String) params.get("password"),20, UserReturnMsg.EMPTY_USERNAME.message());
        validator.equals((String) params.get("password"), (String) params.get("checkPwd"), UserReturnMsg.EMPTY_USERNAME.message());
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }
        User loadUser = userService.get(user.getId());
        if (!loadUser.getPassword().equals((String) params.get("oldPwd"))) {
            return Result.of(Status.ClientError.BAD_REQUEST,"密码不正确");
        }
        User newUser = new User();
        newUser.setId(user.getId());
        newUser.setPassword((String)params.get("password"));
        userService.update(newUser);
        return Result.ok("修改密码成功");
    }


    @RequestMapping("/getMe")
    public Result<?> getMe(@Session(SessionKeys.USER) User user){
        return Result.ok(user);
    }






    @SystemControllerLog(description = "系统管理员登录")
    @RequestMapping("/login")

    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> login(HttpServletRequest request, HttpServletResponse res){
        String userName = request.getParameter("userName");
        String password = request.getParameter("password");

        if(StringUtils.isBlank(userName)) {
            return Result.of(Status.ClientError.BAD_REQUEST, LoginReturnMsg.EMPTY_USERNAME);
        }
        if(StringUtils.isBlank(password)) {
            return Result.of(Status.ClientError.BAD_REQUEST, LoginReturnMsg.EMPTY_PASSWORD);
        }
        LoginParam param = new LoginParam();
        param.setUsername(userName);
        param.setPassword(password);
        return doLogin(param, res, UserType.Super, UserType.System);
    }

    /**
     * Description: 商城后台登录接口
     * @author: sun
     * @Date 下午2:18 2019/5/26
     * @param:
     * @return:
     */

    @RequestMapping("/account/login")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> accountLogin(@RequestBody @Valid LoginParam param, HttpServletResponse res){
        return doLogin(param, res, UserType.Super, UserType.System);
    }

    private Result<UserProfile> doLogin(LoginParam param, HttpServletResponse res, UserType...userTypes) {
//        IdentifiedUser user = userService.login(param.getUsername(), param.getPassword(), userTypes);
        IdentifiedUser user = userService.login(param.getUsername(), param.getPassword(), userTypes);
        if(user == null){
            return Result.of(Status.ClientError.NOT_FOUND, LoginReturnMsg.FAILED);
        }
        if(user.getUser().getState() == YesOrNo.NO){
            return Result.of(Status.ClientError.NOT_ACCEPTABLE, "该账号被禁用了");
        }
        String token = KeyUtils.getKey();


        //获取用户角色
//        user.setRoles(user.getRoles());
//        user.setUser(greeUser);
        //存储至session中
        putToSession(user, token);

        res.setHeader(HeaderNames.TOKEN, token);
        //cookie

        Cookie cookie = new Cookie(HeaderNames.TOKEN, token);
        cookie.setPath("/");
        res.addCookie(cookie);

        UserProfile profile = new UserProfile();
        profile.setName(user.getUser().getNickname());
        profile.setAvatar(user.getUser().getAvatar());
        profile.setGender(user.getUser().getGender());
        profile.setType(user.getUser().getType().value());
        profile.setToken(token);
//        profile.setRoles(user.getRoles());
        return Result.ok(profile);
    }




    private void putToSession(IdentifiedUser user, String token) {
        //以token作为id
        xyz.icrab.common.web.session.Session session = new DefaultSession(token);

        session.setAttribute(SessionKeys.USER, user.getUser());
        session.setAttribute(SessionKeys.ROLES, Optional.ofNullable(user.getRoles()).orElse(Collections.emptyList()) );
        session.setAttribute(SessionKeys.INTERFACES, Optional.ofNullable(user.getInterfaces()).orElse(Collections.emptyList()));
        session.setAttribute(SessionKeys.MENUS, Optional.ofNullable(user.getMenus()).orElse(Collections.emptyList()));
        session.setAttribute(SessionKeys.BUTTONS, Optional.ofNullable(user.getButtons()).orElse(Collections.emptyList()));
        session.setAttribute(SessionKeys.AUTH, user);

//            session.setAttribute("dealer", dealerInfo);

        //获取所有的菜单和接口，存入Session中
        Menu menuParam = new Menu();
        menuParam.setState(YesOrNo.YES.value());

        List<Menu> menus = menuService.list(menuParam);
        Map<String, Menu> menuIndex = new HashMap<>();
        if(CollectionUtils.isNotEmpty(menus)){
            for(Menu menu : menus){
                if(StringUtils.isNotBlank(menu.getUrl())){
                    menuIndex.put(menu.getUrl(), menu);
                }
            }
        }
        session.setAttribute(SessionKeys.ALL_MENUS, menuIndex);
        List<Interface> interfaces = interfaceService.list();
        Map<String, Interface> interfaceIndex = new HashMap<>();
        if(CollectionUtils.isNotEmpty(interfaces)){
            for(Interface api : interfaces){
                if(StringUtils.isNotBlank(api.getUrl())) {
                    interfaceIndex.put(api.getUrl(), api);
                }
            }
        }
        session.setAttribute(SessionKeys.ALL_INTERFACES, interfaceIndex);

        sessionService.set(session);
    }
    /**
     * Description: 导出用户信息
     * @author: sun
     * @Date 下午11:36 2019/3/7
     * @param:
     * @return:
     */

    @RequestMapping({"/exportUser"})
    public void exportUser(HttpServletRequest req, HttpServletResponse rsp) throws IOException {
        Map<String,Object> param = new HashMap<>();
        param.put("dealerId",req.getParameter("dealerId"));
        List<UserExport> result = userService.exportUser(param);
        OutputStream os = rsp.getOutputStream();
        rsp.setContentType("application/x-download");
        // 设置导出文件名称

        rsp.setHeader("Content-Disposition",
                "attachment;filename=" + DateFormatUtils.format(new Date(), "yyyyMMddHHmmssSSS") + ".xlsx");
        ExportParams params = new ExportParams("用户信息表", "用户信息表");
        params.setAddIndex(true);
        params.setType(ExcelType.XSSF);

        Workbook excel = ExcelExportUtil.exportExcel(params, UserExport.class, result);
        excel.write(os);
        os.flush();
        os.close();
    }

/**
 * Description: 重置密码
 * @author: sun
 * @Date 下午1:38 2019/4/29
 * @param:
 * @return:
 */
    @RequestMapping({"/resetPassword"})
    public Result<?> page(@RequestBody @Valid IdParam param) {
        User user = new User();
        user.setId(param.getId());
        user.setPassword("123456");
        userService.update(user);
        return Result.ok();
    }
    /**
     * Description: 删除用户
     * @author: sun
     * @Date 下午1:38 2019/4/29
     * @param:
     * @return:
     */
    @RequestMapping({"/deleteUser"})
    public Result<?> deleteUser(@RequestBody @Valid IdsParam param) {
        userService.deleteUser(param);
        return Result.ok();
    }



    /**
     * Description: 系统用户数量
     * @author: sun
     * @Date 上午10:54 2019/7/1
     * @param:
     * @return:
     */
    @RequestMapping({"/systemUserAmount"})
    public Result<?> systemUserAmount(@Session(SessionKeys.USER) User user) {
        if (!"1a".equals(user.getId())){
            return null;
        }
        return Result.ok(userService.count());
    }
}
