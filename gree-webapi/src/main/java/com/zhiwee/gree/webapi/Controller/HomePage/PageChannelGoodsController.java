package com.zhiwee.gree.webapi.Controller.HomePage;

import com.zhiwee.gree.model.GoodsInfo.GoodsBand;
import com.zhiwee.gree.model.HomePage.PageChannel;
import com.zhiwee.gree.model.HomePage.PageChannelGoods;
import com.zhiwee.gree.service.HomePage.PageChannelGoodsService;
import com.zhiwee.gree.service.HomePage.PageChannelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.util.KeyUtils;
import xyz.icrab.common.web.param.IdParam;
import xyz.icrab.common.web.param.IdsParam;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/pageChannelGoods")
public class PageChannelGoodsController {

    @Autowired
    private PageChannelGoodsService pageChannelGoodsService;

    @Autowired
    private PageChannelService pageChannelService;

    /***
     * 分页查询，周广
     * @param param
     * @param pagination
     * @return
     */
    @RequestMapping("/page")
    public Result<?> page(@RequestBody Map<String, Object> param, Pagination pagination) {
        if (param == null) {
            param = new HashMap<>();
        }
        Pageable<PageChannelGoods> pageable = pageChannelGoodsService.getPage(param, pagination);
        return Result.ok(pageable);
    }

    /**
     * 增加  周广
     * @param pageChannelGoods
     * @return
     */
    @RequestMapping("/add")
    public Result<?> add(@RequestBody PageChannelGoods pageChannelGoods){

        PageChannel pageChannel=pageChannelService.get(pageChannelGoods.getChannelId());
        Map<String,Object> param=new HashMap<>();
        param.put("channelId",pageChannelGoods.getChannelId());
        List<PageChannelGoods> pageChannelGoodsList=pageChannelGoodsService.list(param);
        if(pageChannel.getType()==2 && pageChannelGoodsList.size()==6){
            return Result.of(Status.ClientError.BAD_REQUEST, "超值优惠，最多支持6件商品展示");
        }
        if(pageChannel.getType()==4 && pageChannelGoodsList.size()==8){
            return Result.of(Status.ClientError.BAD_REQUEST, "频道广场，最多支持8件商品展示");
        }
        if(pageChannel.getType()==5 && pageChannelGoodsList.size()==10){
            return Result.of(Status.ClientError.BAD_REQUEST, "精品推荐，最多支持10件商品展示");
        }
        pageChannelGoods.setId(KeyUtils.getKey());
        pageChannelGoodsService.save(pageChannelGoods);
        return Result.ok();
    }
    /**
     * 更新  周广
     * @param pageChannelGoods
     * @return
     */
    @RequestMapping("/update")
    public Result<?> update(@RequestBody PageChannelGoods pageChannelGoods){
        pageChannelGoodsService.update(pageChannelGoods);
        return Result.ok();
    }

    /***
     * 查询单个  周广
     * @param param
     * @return
     */
    @RequestMapping("/selectOne")
    @ResponseBody
    public Result<?> selectOne(@RequestBody IdParam param){
        return Result.ok(pageChannelGoodsService.get(param.getId()));
    }
    /***
     * 删除 周广
     * @param param
     * @return
     */
    @RequestMapping("/delete")
    @ResponseBody
    public Result<?> delete(@RequestBody @Valid IdsParam param){
        for (String id : param.getIds()) {
            PageChannelGoods pageChannelGoods=new PageChannelGoods();
            pageChannelGoods.setId(id);
            pageChannelGoodsService.delete(pageChannelGoods);
        }
        return Result.ok();
    }
}
