package com.zhiwee.gree.webapi.Controller.shopBase.param;

/**
 * @author Knight
 * @since 2018/7/9 10:38
 */
public class ExistsMobileParam {

    private String username;

    private Integer type;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}
