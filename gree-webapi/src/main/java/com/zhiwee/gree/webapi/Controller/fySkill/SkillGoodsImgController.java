package com.zhiwee.gree.webapi.Controller.fySkill;

import com.zhiwee.gree.model.FyGoods.FyGoodsImg;
import com.zhiwee.gree.service.FyGoods.FygoodsImgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.util.IdGenerator;
import xyz.icrab.common.web.annotation.EnableDeleteRequest;
import xyz.icrab.common.web.annotation.EnableGetRequest;
import xyz.icrab.common.web.annotation.EnableListRequest;
import xyz.icrab.common.web.annotation.EnablePageRequest;
import xyz.icrab.common.web.param.IdParam;

import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/skillgoodsImg")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class SkillGoodsImgController {

    @Autowired
    private FygoodsImgService fygoodsImgService;


    /**
     * @Author: jick
     * @Date: 2019/12/10 19:10
     * 将秒杀商品图片的信息存入到数据库中
     */
    @RequestMapping("/save")
    public Result<?> goodsImgSaveController(@RequestBody FyGoodsImg  fyGoodsImg){

            fyGoodsImg.setId(IdGenerator.uuid());

            fygoodsImgService.save(fyGoodsImg);

            return    Result.ok();

    }



    /**
     * @Author: jick
     * @Date: 2019/12/10 19:10
     * 更新秒杀商品的图片信息存入到数据库中
     */
    @RequestMapping("/update")
    public Result<?> goodsImgUpdateController(@RequestBody FyGoodsImg  fyGoodsImg){

        fygoodsImgService.update(fyGoodsImg);

        return    Result.ok();

    }



    /**
     * @Author: jick
     * @Date: 2019/12/10 19:20
     * 更具获取单张图片的信息
     */

    @RequestMapping("/selectOne")
    public Result<?> getOne(@RequestBody IdParam param){
        return Result.ok(fygoodsImgService.get(param.getId()));
    }


    /**
     * @Author: jick
     * @Date: 2019/12/10 20:45
     * 更具goodsid获取商品相册图片图片
     */
    @RequestMapping("/list")
    public Result<?> goodsImglist(@RequestBody Map<String, Object> param){
        List<FyGoodsImg>  list =  fygoodsImgService.queryImgList(param);
        return Result.ok(list);
    }


    /**
     * @Author: jick
     * @Date: 2019/12/11 0:09
     * 根据id删除图片
     */
    @RequestMapping("/delete")
    public    Result<?>  deleteFygoodsImg(@RequestBody IdParam param){
        FyGoodsImg fyGoodsImg  = new FyGoodsImg();
        fyGoodsImg.setId(param.getId());
        fygoodsImgService.delete(fyGoodsImg);

        return  Result.ok();

    }














}
