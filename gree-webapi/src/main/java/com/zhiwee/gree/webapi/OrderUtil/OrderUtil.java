package com.zhiwee.gree.webapi.OrderUtil;

import com.zhiwee.gree.model.Integral.Integral;
import com.zhiwee.gree.model.MemberGrade.MemberGrade;
import com.zhiwee.gree.model.MemberGrade.MemberGradeChange;
import com.zhiwee.gree.model.OrderInfo.Order;
import com.zhiwee.gree.model.OrderInfo.OrderAddress;
import com.zhiwee.gree.model.OrderInfo.OrderItem;
import com.zhiwee.gree.model.OrderInfo.OrderRefund;
import com.zhiwee.gree.model.ShopDistributor.DistributorArea;
import com.zhiwee.gree.model.ShopDistributor.DistributorOrder;
import com.zhiwee.gree.model.ShopDistributor.ShopDistributor;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.service.Integral.IntegralService;
import com.zhiwee.gree.service.MemberGrade.MemberGradeChangeService;
import com.zhiwee.gree.service.MemberGrade.MemberGradeService;
import com.zhiwee.gree.service.OrderInfo.OrderAddressService;
import com.zhiwee.gree.service.OrderInfo.OrderItemService;
import com.zhiwee.gree.service.OrderInfo.OrderRefundService;
import com.zhiwee.gree.service.OrderInfo.OrderService;
import com.zhiwee.gree.service.ShopDistributor.DistributorAreaService;
import com.zhiwee.gree.service.ShopDistributor.DistributorOrderService;
import com.zhiwee.gree.service.ShopDistributor.ShopDistributorService;
import com.zhiwee.gree.service.ShopUser.ShopUserService;
import com.zhiwee.gree.util.SequenceUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import xyz.icrab.common.util.IdGenerator;

import java.util.*;

/**
 * @author sun on 2019/6/19
 */
@Component
public class OrderUtil {

    @Autowired
    private OrderService orderService;

    @Autowired
    private SequenceUtils sequenceUtils;

    @Autowired
    private OrderItemService orderItemService;
    @Autowired
    private OrderAddressService orderAddressService;

    @Autowired
    private DistributorAreaService distributorAreaService;

    @Autowired
    private ShopDistributorService shopDistributorService;


    @Autowired
    private ShopUserService shopUserService;


    @Autowired
    private IntegralService integralService;



    @Autowired
    private MemberGradeService memberGradeService;

    @Autowired
    private DistributorOrderService distributorOrderService;


    @Autowired
    private MemberGradeChangeService memberGradeChangeService;

    @Autowired
    private OrderRefundService orderRefundService;
    /**
     * Description: 商城订单以及分销订单订单配送工具方法
     * @author: sun
     * @Date 下午8:51 2019/6/19
     * @param:
     * @return:
     */
    public  List<DistributorOrder> sendOrder(Order order) {
        Map<String,Object> mapkey = new HashMap<>();
        mapkey.put("defaultDistributor",1);
        ShopDistributor shopDistributor = shopDistributorService.defaultDistributor(mapkey);
        Map<String,Object> param = new HashMap<>();
        param.put("orderId",order.getId());
        OrderAddress orderAddress = orderAddressService.getOne(param);
        List<OrderItem> orderItemList = orderItemService.list(param);
        DistributorOrder distributorOrder = null;
        List<DistributorOrder> distributorOrders = new ArrayList<>();
        if(!orderItemList.isEmpty() && orderItemList.size() > 0){
            for(OrderItem orderItem :orderItemList){
                if(shopDistributor != null && shopDistributor.getCategoryIdPath() != null && !"".equals(shopDistributor.getCategoryIdPath())){
                    if(StringUtils.contains(shopDistributor.getCategoryIdPath(), orderItem.getCategoryId())){
                        distributorOrder =  new DistributorOrder();
                        distributorOrder.setId(IdGenerator.objectId());
                        distributorOrder.setDistributorId(shopDistributor.getId());
                        distributorOrder.setOrderId(order.getId());
                        distributorOrder.setDeliveryState(2);
                        distributorOrder.setOrderItemId(orderItem.getId());
                        distributorOrder.setState(1);
                        distributorOrder.setCommentState(1);
                        distributorOrder.setSendNum(sequenceUtils.sendNumByDay());
                        distributorOrder.setCreateTime(new Date());
                        distributorOrders.add(distributorOrder);
                    }else{
                        Map<String,Object> params = new HashMap<>();
                        params.put("category",orderItem.getCategoryId());
                        params.put("areaId",orderAddress.getAreaCode());
                        params.put("state",1);
                        List<DistributorArea> shopDistributors = distributorAreaService.searchByCategory(params);
                        if(shopDistributors != null && shopDistributors.size() > 0){
                            distributorOrder =  new DistributorOrder();
                            distributorOrder.setId(IdGenerator.objectId());
                            distributorOrder.setDistributorId(shopDistributors.get(0).getDistributorId());
                            distributorOrder.setDeliveryState(2);
                            distributorOrder.setOrderId(order.getId());
                            distributorOrder.setOrderItemId(orderItem.getId());
                            distributorOrder.setState(1);
                            distributorOrder.setCommentState(1);
                            distributorOrder.setSendNum(sequenceUtils.sendNumByDay());
                            distributorOrder.setCreateTime(new Date());
                            distributorOrders.add(distributorOrder);
                        }else{
                            distributorOrder =  new DistributorOrder();
                            distributorOrder.setId(IdGenerator.objectId());
                            distributorOrder.setDistributorId(shopDistributor.getId());
                            distributorOrder.setOrderId(order.getId());
                            distributorOrder.setDeliveryState(2);
                            distributorOrder.setCommentState(1);
                            distributorOrder.setOrderItemId(orderItem.getId());
                            distributorOrder.setSendNum(sequenceUtils.sendNumByDay());
                            distributorOrder.setState(1);

                            distributorOrder.setCreateTime(new Date());
                            distributorOrders.add(distributorOrder);
                        }
                    }
                }else{
                    Map<String,Object> params = new HashMap<>();
                    params.put("category",orderItem.getCategoryId());
                    params.put("areaId",orderAddress.getAreaCode());
                    params.put("state",1);
                    List<DistributorArea> shopDistributors = distributorAreaService.searchByCategory(params);
                    if(shopDistributors != null && shopDistributors.size() > 0){

                        distributorOrder =  new DistributorOrder();
                        distributorOrder.setId(IdGenerator.objectId());

                        distributorOrder.setDistributorId(shopDistributors.get(0).getDistributorId());
                        distributorOrder.setOrderId(order.getId());
                        distributorOrder.setDeliveryState(2);
                        distributorOrder.setOrderItemId(orderItem.getId());
                        distributorOrder.setState(1);
                        distributorOrder.setCommentState(1);
                        distributorOrder.setSendNum(sequenceUtils.sendNumByDay());
                        distributorOrders.add(distributorOrder);
                        distributorOrder.setCreateTime(new Date());

                    }else{
                        distributorOrder =  new DistributorOrder();
                        distributorOrder.setId(IdGenerator.objectId());
                        distributorOrder.setDistributorId(shopDistributor.getId());
                        distributorOrder.setOrderId(order.getId());
                        distributorOrder.setDeliveryState(2);
                        distributorOrder.setOrderItemId(orderItem.getId());
                        distributorOrder.setState(1);
                        distributorOrder.setCommentState(1);
                        distributorOrder.setSendNum(sequenceUtils.sendNumByDay());
                        distributorOrder.setCreateTime(new Date());
                        distributorOrders.add(distributorOrder);
                    }
                }

            }
        }
     return distributorOrders;
    }



    /**
     * Description: 商城订单以及分销订单退款工具方法
     * @author: sun
     * @Date 下午8:55 2019/6/19
     * @param:
     * @return:
     */
    //申请退款状态  1 未申请退换货  2 申请退货中  3已允许退货，4拒绝退货 5 申请换货中 6允许换货 7 拒绝换货  8申请退款中  9 同意退款 10 拒绝退款
    public  void refundOrder(Map map,Order orders) {
        ShopUser shopUser = shopUserService.get(orders.getPayId());
        Map<String, Object> parmas = new HashMap<>();
        parmas.put("state", 1);
        Integral integral = integralService.getOne(parmas);
        MemberGradeChange memberGradeChange = null;
        if (shopUser != null) {
            memberGradeChange = new MemberGradeChange();
            memberGradeChange.setId(IdGenerator.objectId());
            memberGradeChange.setUserId(shopUser.getId());
            memberGradeChange.setCreateTime(new Date());
            if (integral != null) {
                //处理积分
                double ceil = Math.ceil(orders.getRealPrice() / (integral.getDividend()) * integral.getDivisor());
                Double nowIntegral =shopUser.getIntegral() - ceil;
                memberGradeChange.setOldIntegral(shopUser.getIntegral());
                memberGradeChange.setNowIntegral(nowIntegral);
                shopUser.setIntegral(nowIntegral);
            }
            double nowAmount = shopUser.getAmount() - orders.getRealPrice();
            memberGradeChange.setOldGrade(shopUser.getGrade());
            memberGradeChange.setOldAmount(shopUser.getAmount());
            memberGradeChange.setNowAmount(nowAmount);
            shopUser.setAmount(nowAmount);

            Map<String, Object> params = new HashMap<>();
            params.put("amount", shopUser.getAmount());
            MemberGrade memberGrade = memberGradeService.selectMax(params);
            if (memberGrade == null) {
                Map<String, Object> max = new HashMap<>();
                max.put("max", 1);
                max.put("state", 1);
                MemberGrade maxMember = memberGradeService.selectMax(max);
                if (shopUser.getAmount() >= maxMember.getEndAmount()) {
                    shopUser.setGrade(maxMember.getId());
                    memberGradeChange.setNowGrade(maxMember.getId());
                } else {
                    Map<String, Object> min = new HashMap<>();
                    min.put("min", 1);
                    min.put("state", 1);
                    MemberGrade minMember = memberGradeService.selectMax(min);
                    if (shopUser.getAmount() <= minMember.getStartAmount()) {
                        shopUser.setGrade(minMember.getId());
                        memberGradeChange.setNowGrade(minMember.getId());
                    }
                }

            } else {
                shopUser.setGrade(memberGrade.getId());
                memberGradeChange.setNowGrade(memberGrade.getId());
            }

        }
        Double refundMoney = (Double) map.get("refundMoney");
        OrderRefund orderRefund = new OrderRefund();
        orderRefund.setId(IdGenerator.objectId());
        orderRefund.setCreateTime(new Date());
        orderRefund.setOrderId(orders.getId());
        orderRefund.setRefundPrice(refundMoney);
        orderRefund.setTotalPrice(orders.getRealPrice());
        orderRefund.setRefundNum(sequenceUtils.refundNumByDay());

        Map<String, Object> params = new HashMap<>();
        params.put("orderId", orders.getId());
        List<DistributorOrder> distributorOrders = distributorOrderService.list(params);
        for (DistributorOrder distributorOrder : distributorOrders) {
            if (orders.getRefundState() == 2) {
                distributorOrder.setState(2);
            } else if (orders.getRefundState() == 8) {
                distributorOrder.setState(4);
            }
            distributorOrderService.update(distributorOrder);
        }
        orders.setOrderState(4);
        if (orders.getRefundState() == 2) {
            orders.setRefundState(3);
            orderRefund.setState(1);
        } else if (orders.getRefundState() == 8) {
            orders.setRefundState(9);
            orderRefund.setState(3);
        }

        if (memberGradeChange != null) {
            memberGradeChangeService.save(memberGradeChange);
        }
        orderService.update(orders);
        orderRefundService.save(orderRefund);
    }
}