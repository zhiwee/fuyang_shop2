package com.zhiwee.gree.webapi.Controller.AchieveGift;

import com.zhiwee.gree.model.AchieveGift.AchieveGift;
import com.zhiwee.gree.model.ActivityTicketOrder.ActivityTicketOrder;
import com.zhiwee.gree.service.AchieveGift.AchieveGiftService;
import com.zhiwee.gree.service.ActivityTicketOrder.ActivityTicketOrderService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.util.IdGenerator;
import xyz.icrab.common.web.annotation.*;
import xyz.icrab.common.web.param.IdParam;
import xyz.icrab.common.web.param.IdsParam;
import xyz.icrab.common.web.util.Validator;
import javax.validation.Valid;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author sun on 2019/5/6
 */
@RestController
@RequestMapping("/achieveGift")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class AchieveGiftController {


    @Autowired
    private AchieveGiftService achieveGiftService;

    @Autowired
    private ActivityTicketOrderService activityTicketOrderService;

    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * Description: 获取所有领取礼品的记录
     * @author: sun
     * @Date 下午12:51 2019/5/6
     * @param:
     * @return:
     */
    @PermissionMode(PermissionMode.Mode.White)
    @RequestMapping(value = "/searchGiftInfo")
    public Object searchGiftInfo(@RequestBody(required = false) Map<String, Object> params,Pagination pagination){
        Pageable<AchieveGift> achieveGifts = achieveGiftService.searchGiftInfo(params, pagination);
        return Result.ok(achieveGifts);
    }




    /**
     * Description: 通过手机号和劵号领取礼品
     * @author: sun
     * @Date 下午12:51 2019/5/6
     * @param:
     * @return:
     */
    @PermissionMode(PermissionMode.Mode.White)
    @RequestMapping(value = "/achieveGift")
    public Object achieveGift(@RequestBody AchieveGift achieveGift){
        Validator validator = new Validator();
        validator.notEmpty(achieveGift.getMobile(), "激活手机号不能为空");
        validator.notNull(achieveGift.getTicketSeqno(), "劵号不能为空");
        validator.notNull(achieveGift.getCaptcha(), "验证码不能为空");

        validator.notEmpty(achieveGift.getDeliveryPhone(), "收货人手机号不能为空");
        validator.notNull(achieveGift.getDeliveryUser(), "收货人姓名不能为空");

        validator.notEmpty(achieveGift.getGiftId(), "礼品不能为空");
        validator.notNull(achieveGift.getGiftNum(), "礼品数量不能为空");
        validator.notNull(achieveGift.getProvince(), "省不能为空");


        validator.notEmpty(achieveGift.getCity(), "市不能为空");
        validator.notNull(achieveGift.getArea(), "区/县不能为空");
        validator.notNull(achieveGift.getProvinceCode(), "省编码不能为空");


        validator.notEmpty(achieveGift.getCityCode(), "市编码不能为空");
        validator.notNull(achieveGift.getAreaCode(), "县编码不能为空");

        validator.notNull(achieveGift.getDetailAddress(), "详细地址不能为空");
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }
        String key = getCaptchaKey(achieveGift.getMobile());
        String captcha = String.valueOf(redisTemplate.opsForValue().get(key));
        if (StringUtils.isBlank(captcha) || !StringUtils.equals(captcha,achieveGift.getCaptcha())) {
            return Result.of(Status.ClientError.FORBIDDEN, "验证码错误或者失效");
        }
          Map<String, Object> param = new HashMap<>();
         param.put("ticketSeqno",achieveGift.getTicketSeqno());
        List<AchieveGift> achieveGifts = achieveGiftService.list(param);
        if(achieveGifts.size()>0){
             return Result.of(Status.ClientError.FORBIDDEN, "当前认筹劵号已经领取过礼品");
        }
        param.put("deliveryMobile",achieveGift.getMobile());
        ActivityTicketOrder one = activityTicketOrderService.getOne(param);
        if(one == null){
            return Result.of(Status.ClientError.FORBIDDEN, "该劵号或激活手机号错误");
        }

        if(one.getState() != 2){
            return Result.of(Status.ClientError.FORBIDDEN, "该劵号未支付或者已经退款");
        }
        achieveGift.setId(IdGenerator.objectId());
        achieveGift.setCreateTime(new Date());
        achieveGift.setIsSend(1);
        achieveGiftService.save(achieveGift);
        return Result.ok();
    }

    private String getCaptchaKey(String mobile) {
        return "captcha:phone:active:" + mobile;
    }




/**
 * Description: 通过劵号查询礼品领取记录
 * @author: sun
 * @Date 下午4:22 2019/7/1
 * @param:
 * @return:
 */
    @PermissionMode(PermissionMode.Mode.White)
    @RequestMapping(value = "/searchGift")
    public Result<?> searchGift(@RequestBody AchieveGift achieveGift){
        Validator validator = new Validator();
        validator.notNull(achieveGift.getTicketSeqno(), "劵号不能为空");
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }
        Map<String, Object> param = new HashMap<>();
        param.put("ticketSeqno",achieveGift.getTicketSeqno());
        ActivityTicketOrder one = activityTicketOrderService.getOne(param);
        if(one == null){
            return Result.of(Status.ClientError.FORBIDDEN, "该劵号未认筹");
        }
        if(one.getState() != 2){
            return Result.of(Status.ClientError.FORBIDDEN, "该劵号未支付或者已经退款");
        }
        List<AchieveGift> achieveGifts = achieveGiftService.searchGift(param);
        if(achieveGifts.isEmpty()){
            return Result.ok();

        }else{
            return Result.ok(achieveGifts.get(0));
        }

    }




    /**
     * Description: 更新物流信息
     * @author: sun
     * @Date 下午4:22 2019/7/1
     * @param:
     * @return:
     */
    @PermissionMode(PermissionMode.Mode.White)
    @RequestMapping(value = "/updateSend")
    public Result<?> updateSend(@RequestBody AchieveGift achieveGift){
        Validator validator = new Validator();
        validator.notNull(achieveGift.getId(), "标识不能为空");
        validator.notNull(achieveGift.getSendCompnany(), "物流公司不能为空");
        validator.notNull(achieveGift.getOrderNum(), "快递单号不能为空");
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }
        achieveGiftService.update(achieveGift);
 return Result.ok();
    }



    /**
 * Description: 发货
 * @author: sun
 * @Date 上午11:14 2019/7/2
 * @param:
 * @return:
 */
    @RequestMapping("/open")
    @ResponseBody
    public Result<?> stopUseCoupon(@RequestBody @Valid IdsParam map) {
        List<String> ids = map.getIds();
        AchieveGift achieveGift = new AchieveGift();
        for (String id : ids) {
            achieveGift.setId(id);
            achieveGift.setIsSend(2);
            achieveGiftService.update(achieveGift);

        }
        return Result.ok();
    }




    /**
     * Description: 通过记录id获取信息
     * @author: sun
     * @Date 上午11:14 2019/7/2
     * @param:
     * @return:
     */
    @RequestMapping("/getById")
    @ResponseBody
    public Result<?> stopUseCoupon(@RequestBody @Valid IdParam map) {
        AchieveGift achieveGift = achieveGiftService.get(map.getId());
        return Result.ok(achieveGift);
    }

    /**
     * Description: 礼品领取的统计报表
     * @author: sun
     * @Date 上午9:43 2019/7/5
     * @param:
     * @return:
     */
    @RequestMapping("/giftCount")
    @ResponseBody
    public Result<?> categoryGoodsNameNum(@RequestBody(required = false) Map<String, Object> param) {
        return Result.ok(achieveGiftService.giftCount(param));
    }
}
