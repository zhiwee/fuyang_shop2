package com.zhiwee.gree.webapi.Controller.Dept;

import com.zhiwee.gree.model.DeptInfo.DeptInfo;
import com.zhiwee.gree.model.Partement.Partement;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.User;
import com.zhiwee.gree.service.Dept.DeptInfoService;
import com.zhiwee.gree.service.ShopUser.ShopUserService;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Tree;
import xyz.icrab.common.util.IdGenerator;
import xyz.icrab.common.web.annotation.*;
import xyz.icrab.common.web.param.IdParam;
import xyz.icrab.ums.util.consts.SessionKeys;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jick on 2019/8/26.
 */

@RestController
@RequestMapping("/deptInfo")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class DeptInfoController {

    @Autowired
    private DeptInfoService   deptInfoService;

    @Autowired
    private ShopUserService shopUserService;

  /**
   * @author tzj
   * @description 注册成为分销员
   * @date 2019/10/22 16:11
  */

    @RequestMapping(value = "/save")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> deptInfoAdd(@Session("ShopUser") ShopUser shopUser, @RequestBody(required = false) DeptInfo deptInfo){
        deptInfo.setId(IdGenerator.objectId());
        deptInfo.setState(1);
        deptInfo.setIntegral(0.00);
        deptInfo.setRank(4);
        deptInfo.setUserId(shopUser.getId());

        shopUser.setIsRetail(1);

        shopUserService.update(shopUser);
        deptInfoService.save(deptInfo);
        return   Result.ok();

    }


    /**
     * 启用
     */
    @RequestMapping("/open")
    @PermissionMode(PermissionMode.Mode.White)
    @ResponseBody
    public Result<?>  openController(@RequestBody(required =false)  @Valid IdParam map){
        DeptInfo deptInfo =  new DeptInfo();
        deptInfo.setId(map.getId());
        deptInfo.setState(1);
        deptInfoService.update(deptInfo);
        return  Result.ok();

    }



    /**
     * 禁用
     */
    @RequestMapping("/stop")
    @ResponseBody
    public Result<?>  stopController( @RequestBody  @Valid IdParam map){
        DeptInfo deptInfo =  new DeptInfo();
        deptInfo.setId(map.getId());
        deptInfo.setState(2);
        deptInfoService.update(deptInfo);
        return  Result.ok();

    }


    @RequestMapping("/tree")
    @ResponseBody
    public Result<?> tree(@RequestBody(required =false)Map<String,Object> param) {
        if (param == null) {
            param = new HashMap<>(10);
        }
        param.put("state", new Object[]{1, 2});
        param.put("idPath", ",ROOT,1,");
        List<DeptInfo> list = deptInfoService.list(param);
        List<Tree<DeptInfo>> trees = new ArrayList<>(list.size());
        for (DeptInfo record : list) {
            Tree<DeptInfo> tree = new Tree<>(record.getId(), record.getName(), record.getParentId(), record);
            trees.add(tree);
        }
        if (trees.size() == 0) {
            Tree<DeptInfo> tree = new Tree<>("", "所有部门", "", null);
            trees.add(tree);
        }
        return Result.ok(trees);
    }





    @RequestMapping("/infoPage")
    public Result<?> infoPage(@RequestBody Map<String, Object> param, Pagination pagination) {
        if (param == null) {
            param = new HashMap<>(30);
        }
        Pageable<DeptInfo> pageable = deptInfoService.pageInfo(param, pagination);
        return Result.ok(pageable);
    }



}
