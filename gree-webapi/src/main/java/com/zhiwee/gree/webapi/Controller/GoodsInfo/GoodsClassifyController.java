package com.zhiwee.gree.webapi.Controller.GoodsInfo;


import com.zhiwee.gree.model.GoodsInfo.GoodsClassify;
import com.zhiwee.gree.model.GoodsLabel.*;
import com.zhiwee.gree.service.GoodsInfo.GoodsClassifyService;
import com.zhiwee.gree.webapi.util.SystemControllerLog;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.*;
import xyz.icrab.common.web.annotation.*;


import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.*;

@RequestMapping("/goodsClassify")
@RestController
@ResponseBody
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class GoodsClassifyController {
    @Resource
    private GoodsClassifyService goodsClassifyService;
/**
 * Description: 后台商品分类查询
 * @author: sun
 * @Date 下午12:49 2019/5/6
 * @param:
 * @return:
 */
    @RequestMapping("/tree")
    @PermissionMode(PermissionMode.Mode.White)
    public Object tree(@RequestBody(required = false) Map<String, Object> param) {
        if (param == null) {
            param = new HashMap<>();
        }

        param.put("state",1);
        List<GoodsClassify> list = goodsClassifyService.list(param);

        List<Tree<GoodsClassify>> trees = new ArrayList<>(list.size());
        for (GoodsClassify record : list) {
            Tree<GoodsClassify> tree = new Tree<>(record.getId(), record.getName(), record.getParentID(), record);
            trees.add(tree);
        }
        if (trees.size() == 0) {
            Tree<GoodsClassify> tree = new Tree<>("", "全部菜单", "", null);
            trees.add(tree);
        }

        return Result.ok(trees);

    }


    /**
     * 分页查询所有商品分类
     * @param param
     * @param pagination
     * @return
     */
    @RequestMapping("/page")
    public Result<?> page(@RequestBody Map<String, Object> param, Pagination pagination) {
        if (param == null) {
            param = new HashMap<>();
        }
        param.put("state", new Object[]{1, 2});
        Pageable<GoodsClassify> pageable = goodsClassifyService.smallGoodsClassify(param, pagination);
        return Result.ok(pageable);
    }




    /**
 * Description: 商城商品分类查询
 * @author: sun
 * @Date 下午12:50 2019/5/6
 * @param:
 * @return:
 */

    @RequestMapping("/shoptree")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> shoptree(@RequestBody(required = false) Map<String, Object> param) {
        if (param == null) {
            param = new HashMap<>();
        }
        param.put("state", new Object[]{1, 2});
        List<GoodsClassify> list = goodsClassifyService.list(param);
        List<GoodsClassify> goodsClassifies = new ArrayList<>();
        if(!list.isEmpty()){
            for(GoodsClassify  goodsClassify: list){
                if(goodsClassify.getParentID().equals("ROOT")){
                    goodsClassifies.add(goodsClassify);
                    Iterator<GoodsClassify> goodsClassifyIterator = list.iterator();
                    while(goodsClassifyIterator.hasNext()){
                        GoodsClassify gc = goodsClassifyIterator.next();
                        if(gc.getParentID().equals(goodsClassify.getId())){
                            goodsClassify.getChildren().add(gc);
//                            goodsClassifyIterator.remove();
                        }

                    }
                }

            }
        }
        return Result.ok(goodsClassifies);


    }





    /**
     * Description: 获取商品分类对应的标签
     * @author: sun
     * @Date 下午12:50 2019/5/6
     * @param:
     * @return:
     */

    @RequestMapping("/classifyLable")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> classifyLable(@RequestBody Map<String, Object> param) {
        if (param == null) {
            param = new HashMap<>();
        }
         List<Ps> ps = goodsClassifyService.getPsList(param);
        List<Space> spaces = goodsClassifyService.getSpaceList(param);
        List<Gb> gbs= goodsClassifyService.getGbList(param);
        List<Bp> bps = goodsClassifyService.getBpsList(param);
        List<Efficiency> efficiencies = goodsClassifyService.getEfficienciesList(param);


       Map<String,Object>   goodLable = new HashMap<>();
        goodLable.put("空调匹数,ps",ps);
        goodLable.put("适用面积,space",spaces);
        goodLable.put(" 挂壁/立柜,gb",gbs);
        goodLable.put("能效等级,energyEfficiencyGrade",efficiencies);
        goodLable.put("变频/定频,bp",bps);
        return Result.ok(goodLable);

    }

    /**
     * Description: 获取商品小类
     * @author: sun
     * @Date 下午12:50 2019/5/6
     * @param:
     * @return:
     */

    @RequestMapping("/smallCategory")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> smallCategory(@RequestBody Map<String, Object> param,Pagination pagination) {
        if (param == null) {
            param = new HashMap<>();
        }
        Pageable<GoodsClassify> goodsClassifyPageable = goodsClassifyService.smallCategory(param, pagination);
        return Result.ok(goodsClassifyPageable);

    }

/**
 * Description: 获取商品大类
 * @author: sun
 * @Date 下午1:25 2019/7/18
 * @param:
 * @return:
 */
    @RequestMapping("/queryByRoot")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> queryListByRoot() {
      Map<String, Object> param = new HashMap<>();
      param.put("state",1);
        List<GoodsClassify>  list =  goodsClassifyService.queryListByRoot(param);
        return  Result.ok(list);

    }

    /**
     * Description: 通过商品大类id查询商品小类
     * @author: sun
     * @Date 下午1:25 2019/7/18
     * @param:
     * @return:
     */
    @RequestMapping("/queryById")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> queryListById(@RequestBody Map<String ,Object> param) {
        List<GoodsClassify>  list =  goodsClassifyService.queryListById(param);
        return  Result.ok(list);


    }





}
