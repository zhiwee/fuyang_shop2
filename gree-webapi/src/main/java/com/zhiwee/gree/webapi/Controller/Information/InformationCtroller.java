package com.zhiwee.gree.webapi.Controller.Information;

import com.zhiwee.gree.model.Information.Information;
import com.zhiwee.gree.model.User;
import com.zhiwee.gree.service.Information.InformationService;
import org.apache.commons.collections.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.util.IdGenerator;
import xyz.icrab.common.web.annotation.*;
import xyz.icrab.common.web.param.IdParam;
import xyz.icrab.common.web.param.IdsParam;
import xyz.icrab.common.web.util.Validator;
import xyz.icrab.ums.util.consts.SessionKeys;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by jick on 2019/7/18.
 */
@RestController
@RequestMapping("/information")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class InformationCtroller {

    @Autowired
    private InformationService  informationService;



    /**
     * 关键信息录入数据库
     */
    @PermissionMode(PermissionMode.Mode.White)
    @RequestMapping(value = "/save")
   public Result<?> informationAdd(@Session(SessionKeys.USER) User user, @RequestBody(required = false)Information information) {
        //判断非空
      if(null==information.getTitle()||""==information.getTitle().trim()||null==information.getContent()||""==information.getContent().trim()
              ||0==information.getType()||0==information.getState()){
          return  Result.of(Status.ClientError.FORBIDDEN, "基本信息不完整,请检查");
      }

        information.setId(IdGenerator.objectId());
        information.setCreateTime(new Date());
        //information.setFounder("0001");
        information.setFounder(user.getUsername());
        informationService.save(information);
        return  Result.ok();
        }

    /**
     * 查询消息列表
     */
    //@PermissionMode(PermissionMode.Mode.White)
    @RequestMapping(value = "/list")
    public Result<?>  informationList(Pagination pagination, @RequestBody(required =false)Map<String,Object> map){
       // List<Information>  list =  informationService.queryInfoList();
        //  Pageable<Invoice>  invoices = invoiceService.queryBaseInvoice(param,pagination);
        Pageable<Information> list =  informationService.queryInfoList(pagination,map);
        return Result.ok(list);

    }

    /**
     * 更新操作
     */
    @RequestMapping("/update")
    @ResponseBody
    public Result<?> informationUpdate(@RequestBody(required = false)Information information) {

        //判断非空
        if(null==information.getId()||""==information.getId().trim()||null==information.getTitle()||""==information.getTitle().trim()||null==information.getContent()||""==information.getContent().trim()
                ||0==information.getType()|| 0==information.getState()){
            return  Result.of(Status.ClientError.FORBIDDEN, "基本信息不完整,请检查");
        }

        information.setUpdateTime(new Date());
        informationService.update(information);
        return  Result.ok();

    }

    /**
     *通过id查询商品的信息，用来做后台信息回显
     */
    @RequestMapping("/get")
    @ResponseBody
    public Result<?> getMe(@RequestBody @Valid IdParam map){
        //  ActivityPage activityPage = activityPageService.get(map.getId());
        Information   information = informationService.get(map.getId());
        return Result.ok(information);
    }


    /**
     * 删除操作
     */

    /**
     * Description: 启用
     * @author: sun
     * @Date 上午10:37 2019/7/8
     * @param:
     * @return:
     */
    @RequestMapping("/delete")
    @ResponseBody
    public Result<?> open(@RequestBody @Valid IdsParam map) {
        List<String> ids = map.getIds();
        Information info = new Information();
        for (String id : ids) {
            info.setId(id);
            //activityPageService.update(activityPage);
            informationService.delete(info);
        }
        return Result.ok();
    }

    /**
     * 消息列表，前台页面使用
     */
    @PermissionMode(PermissionMode.Mode.White)
    @RequestMapping(value = "/portalInfoList")
    public Result<?>  portalInformationList(@RequestBody(required = false) Information information){
        // List<Information>  list =  informationService.queryInfoList();
        //  Pageable<Invoice>  invoices = invoiceService.queryBaseInvoice(param,pagination);
       // Pageable<Information> list =  informationService.queryInfoList(pagination,map);
        Map<String,Object> map = new HashedMap();
        if(null != information && null != information.getType()){
            map.put("type",information.getType());
        }
        map.put("state",1);
        List<Information> list = informationService.list(map);
        return Result.ok(list);

    }






}
