package com.zhiwee.gree.webapi.Controller.GoodsInfo;

import com.zhiwee.gree.model.GoodsInfo.GoodsBand;
import com.zhiwee.gree.model.GoodsInfo.GoodsInfoPicture;
import com.zhiwee.gree.service.GoodsInfo.GoodsBaseService;
import com.zhiwee.gree.service.GoodsInfo.GoodsInfoPictureService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.web.annotation.*;
import xyz.icrab.common.web.param.IdParam;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author DELL
 */
@RestController
@RequestMapping("/goodsInfoPicture")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class GoodsInfoPictureController {
    @Resource
    private GoodsInfoPictureService goodsInfoPictureService;
    @Resource
    private GoodsBaseService goodsBaseService;
    @RequestMapping(value = "searchAll")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> searchAll(@RequestBody(required = false) Map<String, Object> params, Pagination pagination) {
        Pageable<GoodsInfoPicture> list =   goodsInfoPictureService.pageInfo(params,pagination);
        return Result.ok(list);
    }

    @RequestMapping("/selectOne")
    @ResponseBody
    public Result<?> selectOne(@RequestBody IdParam param){
        GoodsInfoPicture info =  goodsInfoPictureService.getInfo(param.getId());
        return Result.ok(info);
    }
    @RequestMapping("/update")
    public Result<?> update(@RequestBody GoodsInfoPicture goodsBand){
        goodsInfoPictureService.update(goodsBand);
        return Result.ok();
    }

}
