package com.zhiwee.gree.webapi.framework;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.util.JsonUtils;
import xyz.icrab.common.web.exception.NoLoginException;
import xyz.icrab.common.web.exception.PermissionAuthException;
import xyz.icrab.common.web.exception.SessionAuthException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Description: 全局异常处理
 * @author: sun
 * @Date 下午12:50 2019/5/6
 * @param:
 * @return:
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(com.zhiwee.gree.webapi.framework.GlobalExceptionHandler.class);

    @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Object handleHttpMediaTypeNotSupportedException(HttpMediaTypeNotSupportedException ex, HttpServletRequest req, HttpServletResponse res){
        logger.error(ex.getMessage(), ex);
        return resultOf(req, res, Result.of(Status.ClientError.BAD_REQUEST), viewOf("403"));
    }

    @ExceptionHandler(NoLoginException.class)
    public Object handleNoLoginException(NoLoginException ex, HttpServletRequest req, HttpServletResponse res){
        return resultOf(req, res, Result.of(Status.ClientError.UNAUTHORIZED, "用户未登录或登录已过期"), viewOf("redirect:/login"));
    }

    @ExceptionHandler(HandleNoSessionException.class)
    public Object handleNoSessionException(HandleNoSessionException ex, HttpServletRequest req, HttpServletResponse res){
        return resultOf(req, res, Result.of(Status.ClientError.UNAUTHORIZED, "用户未登录或登录已过期"), viewOf("redirect:/login"));
    }


    @ExceptionHandler(NoHandlerFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Object handleNoHandlerFoundException(NoHandlerFoundException ex, HttpServletRequest req, HttpServletResponse res){
        logger.error(ex.getMessage(), ex);
        return resultOf(req, res, Result.of(Status.ClientError.NOT_FOUND), viewOf("404"));
    }

    @ExceptionHandler(SessionAuthException.class)
    @ResponseStatus(HttpStatus.OK)
    public Object handleSessionAuthException(SessionAuthException ex, HttpServletRequest req, HttpServletResponse res){
        logger.error(ex.getMessage(), ex);
        return resultOf(req, res, Result.of(Status.ClientError.UNAUTHORIZED, ex.getMessage()), viewOf("401"));
    }

    @ExceptionHandler(PermissionAuthException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public Object handlePermissionAuthException(PermissionAuthException ex, HttpServletRequest req, HttpServletResponse res){
        logger.error(ex.getMessage(), ex);
        return resultOf(req, res, Result.of(Status.ClientError.FORBIDDEN, ex.getMessage()), viewOf("403"));
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Object handleHttpMessageNotReadableException(HttpMessageNotReadableException ex, HttpServletRequest req, HttpServletResponse res){
        logger.error(ex.getMessage(), ex);
        return resultOf(req, res, Result.of(Status.ClientError.BAD_REQUEST, "Request body is missing"), viewOf("403"));
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Object handleMethodArgumentNotValidException(MethodArgumentNotValidException ex, HttpServletRequest req, HttpServletResponse res){
        logger.error(ex.getMessage(), ex);
        String errorMsg = ex.getBindingResult().getFieldErrors().stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .findFirst()
                .orElse(ex.getMessage());
        return resultOf(req, res, Result.of(Status.ClientError.BAD_REQUEST, errorMsg), viewOf("403"));
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public Object handleException(Exception ex, HttpServletRequest req, HttpServletResponse res){
        logger.error(ex.getMessage(), ex);
        return resultOf(req, res, Result.of(Status.ServerError.INTERNAL_SERVER_ERROR, ex.getMessage()), viewOf("500"));
    }

    @ExceptionHandler(Throwable.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public Object handleThrowable(Throwable ex, HttpServletRequest req, HttpServletResponse res){
        logger.error(ex.getMessage(), ex);
        return resultOf(req, res, Result.of(Status.ServerError.INTERNAL_SERVER_ERROR), viewOf("500"));
    }

    private Boolean isJson(HttpServletRequest request){
        String header = request.getHeader("content-type");
        return header != null && header.contains("json");
    }

    private ModelAndView viewOf(String viewName){
        return new ModelAndView(viewName);
    }

    private Object resultOf(HttpServletRequest request, HttpServletResponse res, Result<?> json, ModelAndView view) {
        if(isJson(request)) {
            try {
                res.setContentType("application/json;charset=utf-8");
                res.getWriter().write(JsonUtils.toJsonQuietly(json));
            } catch (IOException e) {
                e.printStackTrace();
            }
            return new ModelAndView();
        } else {
            return view;
        }
    }
}
