package com.zhiwee.gree.webapi.Controller.livePlay;

import com.zhiwee.gree.model.WxPay.WeChatConfig;
import com.zhiwee.gree.model.livePlay.LivePlay;
import com.zhiwee.gree.model.livePlay.LivePlayStr;
import com.zhiwee.gree.webapi.util.HttpUtil;
import com.zhiwee.gree.webapi.util.JsonUtil;
import net.sf.json.JSONObject;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.web.annotation.EnableGetRequest;
import xyz.icrab.common.web.annotation.EnableListRequest;
import xyz.icrab.common.web.annotation.PermissionMode;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author DELL
 */
@RequestMapping("/livePlay")
@Controller
@ResponseBody
@EnableListRequest
@EnableGetRequest
public class LivePlayController {
    @Resource
    private RedisTemplate redisTemplate;


    /**
     * @author tzj
     * @description 获取直播间
     * @date 2020-04-28 18:06
     */
    @Scheduled(cron = "0 */15 * * * ?")
    @RequestMapping(value = "/getWxListToRedis")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> getWxListToRedis() {
        System.out.println("刷新列表" + new Date());
        String grant_type = "client_credential";
        String appid = WeChatConfig.APPIDFuYang;
        String secret = WeChatConfig.APIKEYFuYang;
        String url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&&appid=wx575dba0ef8ff034f&&secret=7f7f89ad5023ce314a4ba5b6496201b8";
        String resultMap = HttpUtil.sendGet(url);
        if (null == resultMap) {
            return Result.of(Status.ClientError.BAD_REQUEST, "请求失败，请重试");
        }
        Map<Object, Object> result1 = jsonToMap(resultMap);
        String access_token = (String) result1.get("access_token");
        String data = "{\"start\":0,\"limit\":\"100\"}";
        String url2 = "http://api.weixin.qq.com/wxa/business/getliveinfo?access_token=" + access_token;
        String s = HttpUtil.doPost(url2, data, 4000);
        System.out.println(s);
        Map<String, String> result = new HashMap<>(100);
        Map<String, String> result2 = new HashMap<>(100);
        LivePlayStr livePlayStr = JsonUtil.jsonStr2Object(s, LivePlayStr.class);
        assert livePlayStr != null;
        List<LivePlay> roomInfo = livePlayStr.getRoom_info();
        List<LivePlay> liveNew = new ArrayList<>(100);
        List<LivePlay> liveAgain = new ArrayList<>(100);
        for (LivePlay livePlay : roomInfo) {
            if (103==livePlay.getLive_status()){
                liveAgain.add(livePlay);
            }else {
                liveNew.add(livePlay);
            }
        }
        //回访
        checkLivePlay(liveAgain);
        //存redis
        HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
        //livePlayList
        result.put("info", JsonUtil.object2JsonStr(liveNew));
        result2.put("info", JsonUtil.object2JsonStr(liveAgain));
        hashOperations.putAll("livePlayNew", result);
        hashOperations.putAll("livePlayAgain", result2);
        System.out.println("刷新直播间列表成功");
        return Result.ok(result);
    }

    /**
     * @author tzj
     * @description 生成回放
     * @date 2020-04-28 20:07
    */
    private void checkLivePlay(List<LivePlay> liveAgain) {
        //找到需要生成回放的
        List<LivePlay> needLive=new ArrayList<>(100);
        HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
        //循环
        for (LivePlay livePlay : liveAgain) {
            Map<String, String> entries = hashOperations.entries("livePlay_" + livePlay.getRoomid());
            //查不到记录
            if (entries.size()==0){
                needLive.add(livePlay);
            }
        }
        if (CollectionUtils.isEmpty(needLive)){
            return;
        }
        //去到access_token
        String grant_type = "client_credential";
        String appid = WeChatConfig.APPIDFuYang;
        String secret = WeChatConfig.APIKEYFuYang;
        String url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&&appid=wx575dba0ef8ff034f&&secret=7f7f89ad5023ce314a4ba5b6496201b8";
        String resultMap = HttpUtil.sendGet(url);
        Map<Object, Object> result1 = jsonToMap(resultMap);
        String access_token = (String) result1.get("access_token");
        for (LivePlay livePlay : needLive) {
            getWxListToRedis2(livePlay.getRoomid(),access_token);
        }
    }


    /**
     * @author tzj
     * @description 会放列表
     * @date 2020-04-28 20:08
    */
    @RequestMapping(value = "/livePlayAgain")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> livePlayAgain() {
        HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
        Map<String, String> resultMap = hashOperations.entries("livePlayAgain");
        List<LivePlay> list = new ArrayList<>();
        for (Map.Entry<String, String> map : resultMap.entrySet()) {
            list = JsonUtil.jsonToList(map.getValue(), LivePlay.class);
        }
        if (CollectionUtils.isNotEmpty(list)){
            for (LivePlay livePlay : list) {
                Long start_time = livePlay.getStart_time();
                Long end_time = livePlay.getEnd_time();
                SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String et = sdf.format(new Date(start_time*1000));
                String en = sdf.format(new Date(end_time*1000));
                livePlay.setStart_timeStr(et);
                livePlay.setEnd_timeStr(en);
            }
        }
        return Result.ok(list);
    }
    /**
     * @author tzj
     * @description 直播列表
     * @date 2020-04-28 20:08
     */
    @RequestMapping(value = "/livePlayNew")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> livePlayNew() {
        HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
        Map<String, String> resultMap = hashOperations.entries("livePlayNew");
        List<LivePlay> list = new ArrayList<>();
        for (Map.Entry<String, String> map : resultMap.entrySet()) {
            list = JsonUtil.jsonToList(map.getValue(), LivePlay.class);
        }
        if (CollectionUtils.isNotEmpty(list)){
            for (LivePlay livePlay : list) {
                Long start_time = livePlay.getStart_time();
                Long end_time = livePlay.getEnd_time();
                SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String et = sdf.format(new Date(start_time*1000));
                String en = sdf.format(new Date(end_time*1000));
                livePlay.setStart_timeStr(et);
                livePlay.setEnd_timeStr(en);
            }
        }
        return Result.ok(list);
    }


    /**
     * json string 转换为 map 对象
     *
     * @param jsonObj
     * @return
     */
    public static Map<Object, Object> jsonToMap(Object jsonObj) {
        JSONObject jsonObject = JSONObject.fromObject(jsonObj);
        Map<Object, Object> map = (Map) jsonObject;
        return map;
    }


    /**
     * @author tzj
     * @description 清零
     * @date 2020/4/14 12:50
     */
    @RequestMapping(value = "/clean")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> clean() throws Exception {
        String grant_type = "client_credential";
        String appid = WeChatConfig.APPIDFuYang;
        String secret = WeChatConfig.APIKEYFuYang;
        String url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&&appid=wx575dba0ef8ff034f&&secret=7f7f89ad5023ce314a4ba5b6496201b8";
        String resultMap = HttpUtil.sendGet(url);
        if (null == resultMap) {
            return Result.of(Status.ClientError.BAD_REQUEST, "请求失败，请重试");
        }
        String data = "{\"appid\": \"wx575dba0ef8ff034f\"}";
        Map<Object, Object> result1 = jsonToMap(resultMap);
        String access_token = (String) result1.get("access_token");
        String url2 = "https://api.weixin.qq.com/cgi-bin/clear_quota?access_token=?" + access_token;
        String s = HttpUtil.doPost(url2, data, 4000);
        System.out.println("清零成功");
        return Result.ok();
    }


    /**
     * @author tzj
     * @description 获取回访
     * @date 2020-04-28 18:06
     */

    private void getWxListToRedis2(Integer room_id,String access_token) {
        String data = "{\n" +
                "        \"action\": \"get_replay\", \n" +
                "        \"room_id\": " + room_id + ",\n" +
                "        \"start\": 0, \n" +
                "        \"limit\": 100 \n" +
                "    }";
        String url2 = "http://api.weixin.qq.com/wxa/business/getliveinfo?access_token=" + access_token;
        String s = HttpUtil.doPost(url2, data, 4000);
        Map<String, String> result = new HashMap<>();
        Map<Object, Object> getInfo = jsonToMap(s);
        //存redis
        HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
        //livePlayList
        result.put("info", JsonUtil.object2JsonStr(getInfo.get("room_info")));
        hashOperations.putAll("livePlay_" + room_id, result);
    }


}
