package com.zhiwee.gree.webapi.Controller.draw;

import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.User;
import com.zhiwee.gree.model.card.LShopUserCards;
import com.zhiwee.gree.model.draw.CanDraws;
import com.zhiwee.gree.model.draw.DrawInfo;
import com.zhiwee.gree.model.draw.DrawUseInfo;
import com.zhiwee.gree.service.card.LShopUserCardsService;
import com.zhiwee.gree.service.draw.CanDrawsService;
import com.zhiwee.gree.service.draw.DrawInfoService;
import com.zhiwee.gree.service.draw.DrawUseInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.util.IdGenerator;
import xyz.icrab.common.util.KeyUtils;
import xyz.icrab.common.web.annotation.EnableGetRequest;
import xyz.icrab.common.web.annotation.EnableListRequest;
import xyz.icrab.common.web.annotation.Session;
import xyz.icrab.ums.util.consts.SessionKeys;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequestMapping("/drawInfo")
@Controller
@ResponseBody
@EnableListRequest
@EnableGetRequest

/**
 * @author tzj
 * @description
 * @date 2019/12/14 0:34
 * @return
*/
public class DrawInfoController {

    @Resource
    private DrawInfoService drawInfoService;
    @Resource
    private CanDrawsService canDrawsService;
    @Resource
    private DrawUseInfoService drawUseInfoService;
    @Resource
    private LShopUserCardsService lShopUserCardsService;
    /**
     * @author tzj
     * @description 奖池新增
     * @date 2019/12/8 19:33
     */

    @RequestMapping(value = "/add")
    public Result<?> deptInfoAdd(@Session(SessionKeys.USER) User user, @RequestBody(required = false) DrawInfo drawInfo) {

        Boolean canUse = checkTime(drawInfo);
        if (!canUse) {
            return Result.of(Status.ClientError.BAD_REQUEST, "与已有抽奖时间冲突");
        }
        Boolean num = checkNum(drawInfo);
        if (!num) {
            return Result.of(Status.ClientError.BAD_REQUEST, "已经抽奖数目不能大于总数");
        }
        Boolean time = checkTime2(drawInfo);
        if (!time) {
            return Result.of(Status.ClientError.BAD_REQUEST, "结束时间不能在开始时间之前");
        }
        drawInfo.setId(IdGenerator.objectId());
        drawInfo.setState(1);
        drawInfo.setCreatetime(new Date());
        drawInfo.setCreateman(user.getId());
        drawInfoService.save(drawInfo);
        return Result.ok();
    }


    /**
     * @return
     * @author tzj
     * @description 奖池更新
     * @date 2019/12/8 19:32
     */

    @RequestMapping(value = "/update")
    @SuppressWarnings("all")
    public Result<?> update(@Session(SessionKeys.USER) User user, @RequestBody(required = false) DrawInfo drawInfo) {
        Boolean canUse = checkTime(drawInfo);
        if (!canUse) {
            return Result.of(Status.ClientError.BAD_REQUEST, "与已有抽奖时间冲突");
        }
        Boolean num = checkNum(drawInfo);
        if (!num) {
            return Result.of(Status.ClientError.BAD_REQUEST, "已经抽奖数目不能大于总数");
        }
        Boolean time = checkTime2(drawInfo);
        if (!time) {
            return Result.of(Status.ClientError.BAD_REQUEST, "结束时间不能在开始时间之前");
        }
        drawInfo.setUpdateman(user.getId());
        drawInfo.setUpdatetime(new Date());
        drawInfoService.update(drawInfo);
        return Result.ok();
    }


    /**
     * @return
     * @author tzj
     * @description 奖池页面
     * @date 2019/12/8 19:32
     */
    @RequestMapping(value = "/infoPage")
    @SuppressWarnings("all")
    public Result<?> infoPage(@RequestBody Map<String, Object> params, Pagination pagination) {
        if (params == null) {
            params = new HashMap<>();
        }

        Pageable<DrawInfo> page = drawInfoService.pageInfo(params, pagination);
        return Result.ok(page);
    }


    /**
     * @return
     * @author tzj
     * @description 删除抽奖
     * @date 2019/12/8 19:52
     */

    @RequestMapping("/delete")
    @ResponseBody
    @SuppressWarnings("all")
    public Result<?> delete(@RequestBody DrawInfo drawInfo) {
        drawInfoService.delete(drawInfo);
        return Result.ok();
    }


    /**
     * @author tzj
     * @description 校验时间是否冲突
     * @date 2019/12/8 19:27
     */
    private Boolean checkTime(DrawInfo drawInfo) {
        Map<String, Object> param = new HashMap<>(10);
        param.put("endTime", drawInfo.getEndTime());
        param.put("beginTime", drawInfo.getBeginTime());
        param.put("state", 1);
        List<DrawInfo> list = drawInfoService.checkTime(param);
        if (list.size() > 0) {
            return list.size() == 1 && drawInfo.getId().equals(list.get(0).getId());
        } else {
            return true;
        }
    }

    private Boolean checkNum(DrawInfo drawInfo) {
        return drawInfo.getUseSum() <= drawInfo.getAllSum();
    }

    private Boolean checkTime2(DrawInfo drawInfo) {
        return drawInfo.getBeginTime().before(drawInfo.getEndTime());
    }


        /**
         * @author tzj
         * @description 查看当前是否有抽奖活动
         * @date 2020/1/2 17:08
        */
    @RequestMapping("/getDraw")
    @ResponseBody
    public Result<?> getDraw(@Session("ShopUser") ShopUser user) {
        //通过时间去查是否有抽奖
        List<DrawInfo> currentDraw = drawInfoService.getCurrent(new Date());
            if (!currentDraw.isEmpty()){

                //如果是所有都可参与直接返回
                if (currentDraw.get(0).getType()==1){
                    canDrawForUser(user,currentDraw.get(0));
                    return Result.ok(currentDraw.get(0));
                }else {
                    //判断是否购买珍藏卡
                    LShopUserCards lShopUserCards=new LShopUserCards();
                    lShopUserCards.setShopUserId(user.getId());
                    lShopUserCards.setState(2);
                    lShopUserCards=lShopUserCardsService.getOne(lShopUserCards);
                    if (lShopUserCards==null){
                        return Result.of(Status.ClientError.BAD_REQUEST,"无抽奖活动");
                    }else {
                        return Result.ok(currentDraw.get(0));

                    }
                }
            }else {
                return Result.of(Status.ClientError.BAD_REQUEST,"无抽奖活动");
            }
        }

    /**
     * @author tzj
     * @description 查看用户是否有抽奖记录
     * @date 2020/1/3 9:43
    */
    private void canDrawForUser(ShopUser user,DrawInfo drawInfo) {
        //通过用户查找是否有抽奖记录
        CanDraws canDraws=new CanDraws();
        canDraws.setShopUserId(user.getId());
        canDraws= canDrawsService.getOne(canDraws);
        Map<String,Object> param=new HashMap<>();
        param.put("drawId",drawInfo.getId());
        param.put("customerid",user.getId());
        List<DrawUseInfo> list = drawUseInfoService.list(param);


        //如果有以前的资料
        if (canDraws!=null){
            if (list.isEmpty()) {
                canDraws.setDrawsSum(drawInfo.getOneSum());
            }else {
                canDraws.setDrawsSum(drawInfo.getOneSum()-list.size());
            }
            canDrawsService.update(canDraws);
        }else{
            //如果没有
            CanDraws canDraws1=new CanDraws();
            canDraws1.setShopUserId(user.getId());
            canDraws1.setId(KeyUtils.getKey());
            canDraws1.setDrawsSum(drawInfo.getOneSum());
            canDraws1.setCreatetime(new Date());
            canDrawsService.save(canDraws1);
        }
    }
}
