package com.zhiwee.gree.webapi.Controller.SecKill;


import com.alibaba.fastjson.JSON;
import com.zhiwee.gree.model.GoodsInfo.LAGoodsInfo;
import com.zhiwee.gree.model.OrderInfo.Order;
import com.zhiwee.gree.model.OrderInfo.UsersOrder;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.User;
import com.zhiwee.gree.model.resultBase.BaseResult;
import com.zhiwee.gree.webapi.util.DateUtil;
import com.zhiwee.gree.webapi.util.JsonUtil;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessagePostProcessor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.util.IdGenerator;
import xyz.icrab.common.web.annotation.*;
import xyz.icrab.common.web.session.Session;
import xyz.icrab.common.web.util.SessionUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author: jick
 * @Date: 2019/10/19 20:26
 */
@RestController
@RequestMapping("/secKill")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class SeckillController {

    @Resource(name ="redisTemplate")
    private   RedisTemplate<String,String> redisTemplate;





   /* @Autowired
    private RabbitTemplate   rabbitTemplate;

*/







    /**
     * @Author: jick
     * @Date: 2019/10/25 13:27
     *
     */
  /*  @RequestMapping("/createrOrder")
    @PermissionMode(PermissionMode.Mode.White)
    public BaseResult createOrder(@RequestBody(required = false) Map<String, Object> param, HttpServletResponse res,
                                  HttpServletRequest req){

        Session session = SessionUtils.get(req);
         ShopUser user = null;


        if(session !=null){

            user = (ShopUser) session.getAttribute("user");

            HashOperations<String,String,String> hashOperations =  redisTemplate.opsForHash();

           // HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();


            //首先查询缓存数据库，看看该人是否已经抢购抢购在排队了
              //String  ticket  =   redisTemplate.opsForValue().get("line-"+user.getId()+"-"+(String) param.get("goodsId"));
            String  ticket  =   redisTemplate.opsForValue().get("line-"+user.getId()+"-"+(String) param.get("goodsId"));
              if(null !=ticket && ticket.length()>0){
                  return    new BaseResult(201,"已经有商品抢购，请支付");
              }else{

                  return    creaderOrder(hashOperations,param,user);

              }
              *//**//*







        }


        return   new BaseResult(204,"请登录");

    }*/




    //多线程创建订单
   /* @Async
    public    BaseResult   creaderOrder(HashOperations<String,String,String> hashOperations,
                                        Map<String, Object> param,
                                        ShopUser user){
        try {


            //判断该商品库存·是否大于0
            //getActivityId()+":"+lag.getGoodsId()

            // 先查询
            Map<String, String> resultMap = hashOperations.entries("SeckillGoods:001");

            // 根据map的key获取value
            String  skillGoods = resultMap.get((String) param.get("activityId")+":"+(String) param.get("goodsId"));

            if(null !=skillGoods && skillGoods.length()>0){
                // ShopCartGoods  shopCartGoods  =   JsonUtil.jsonStr2Object(shopGoods, ShopCartGoods.class);
                LAGoodsInfo   laGoodsInfo  = JsonUtil.jsonStr2Object(skillGoods,LAGoodsInfo.class);

                //如果库存大于0
                if(laGoodsInfo.getStockNum()>0){

                    Date  date  = new Date();
                    //更具时间和用户id创建排队票据
                    redisTemplate.opsForValue().set("line-"+user.getId()+"-"+(String) param.get("goodsId"),user.getId()+":"+DateUtil.date2Str(date));


                    UsersOrder order  = new UsersOrder();
                    order.setId(IdGenerator.uuid());
                    order.setOrderNum(DateUtil.date2Str(date));
                    order.setState(1);//1表示未支付
                    order.setPayId(user.getId());
                    order.setActivityId((String) param.get("activityId"));
                    order.setGoodsId((String) param.get("goodsId"));
                    order.setCreateTime(date);
                    order.setTotalPrice(BigDecimal.valueOf(500.0));
                    order.setRealPrice(BigDecimal.valueOf(500.0));
                    //创建订单
                    Map<String, String> orderMap =  new HashMap<>();
                    orderMap.put(user.getId()+":"+DateUtil.date2Str(date), JsonUtil.object2JsonStr(order));

                    // 添加至redis
                    hashOperations.putAll("order:"+user.getId() ,orderMap);
                     //发送信息至消息队列
                    sendDelayMessage(order);
                    //库存减1
                    laGoodsInfo.setStockNum(laGoodsInfo.getStockNum()-1);
                    //库存信息更新至redis
                    resultMap.put((String) param.get("activityId")+":"+(String) param.get("goodsId"),JsonUtil.object2JsonStr(laGoodsInfo));
                    hashOperations.putAll( "SeckillGoods:001", resultMap);

                    //删除排队票卷
                    // redisTemplate.delete("line："+user.getId());



                    return    new BaseResult(200,"抢购成功");

                }
                return  new BaseResult(203,"商品已售罄，抢购失败");

            }
            return  new BaseResult(203,"商品已售罄，抢购失败");



        }catch (Exception e){
            e.printStackTrace();
        }
        return   null;
    }*/



    //发送订单信息到消息队列
   /* public void sendDelayMessage(UsersOrder  order){
        rabbitTemplate.convertAndSend(
                "exchange.delay.secOrder.begin",
                "delay",
                JSON.toJSONString(order),
                new MessagePostProcessor() {
                    @Override
                    public Message postProcessMessage(Message message) throws AmqpException {
              //消息有效期30分钟
              //message.getMessageProperties().setExpiration(String.valueOf(1800000));
                        message.getMessageProperties().setExpiration(String.valueOf(180000));
                        return message;
                    }
                });
    }*/













}
