package com.zhiwee.gree.webapi.util;

import com.zhiwee.gree.model.WxPay.WeChatConfig;
import org.apache.commons.lang3.StringUtils;

import java.util.*;

/**
 * @author sun on 2019/5/16
 */
public class WeixinRefundUtil {


    /**
     *      * @Author: sun
     *      * @Description:微信退款
     *      * @param out_trade_no 商户订单号
     *      * @param transaction_id 微信订单号
     *      * @param total_fee 订单总金额
     *      * @Date: 2019-5-17
     *      * @return:
     *      
     */
    public static String wxPayRefund(String out_trade_no,String total_fee, String transaction_id, String refundMoney, String notifyUrl) {
//        StringBuffer xml = new StringBuffer();
        String data = null;
        try {
            String nonceStr =PayForUtil.buildRandom(4) + "";//生成32位随机字符串
//            xml.append("</xml>");
            SortedMap<Object, Object> parameters = new TreeMap<Object, Object>();
            parameters.put("appid", WeChatConfig.APPID);
            parameters.put("mch_id", WeChatConfig.MCHID);
            parameters.put("nonce_str", nonceStr);
            parameters.put("out_trade_no", out_trade_no);
            //有订单号可以没有这个
//            parameters.put("transaction_id", transaction_id);
            parameters.put("out_refund_no", nonceStr);
            parameters.put("fee_type", "CNY");
            parameters.put("total_fee", total_fee);
            parameters.put("refund_fee", refundMoney);
            parameters.put("notify_url", notifyUrl);
//            parameters.put("op_user_id", WXPayConstants.MCH_ID);
            parameters.put("sign", PayForUtil.createSign("UTF-8", parameters,WeChatConfig.APIKEY));
            data = PayForUtil.getRequestXml(parameters);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return null;
        }
        return data;
    }








    /**
     *      * @Author: sun
     *      * @Description:微信发红包
     *      * @param re_openid 用户微信id
     *      * @param orderNum 订单号
     *      * @param total_fee 订单总金额
     *      * @Date: 2019-5-17
     *      * @return:
     *      
     */
    public static String cashVoucher(String orderNum, String re_openid, String total_fee) {
//        StringBuffer xml = new StringBuffer();
        String data = null;
        try {
            String nonceStr =PayForUtil.buildRandom(4) + "";//生成32位随机字符串
//            xml.append("</xml>");
            SortedMap<Object, Object> parameters = new TreeMap<Object, Object>();
            parameters.put("nonce_str", nonceStr);

            parameters.put("mch_billno", orderNum);

//            parameters.put("appid", WeChatConfig.HNAPPID);

            parameters.put("mch_id", WeChatConfig.HNMCHID);

            parameters.put("wxappid", WeChatConfig.HNAPPID);

            parameters.put("send_name", WeChatConfig.HNMCHIDNAME);

            parameters.put("re_openid", re_openid);

            parameters.put("total_amount", total_fee);

            parameters.put("total_num", "1");

            parameters.put("wishing", "恭喜");

            parameters.put("client_ip", PayForUtil.localIp());
            System.out.println(PayForUtil.localIp());

            parameters.put("act_name", "活动名称");

            parameters.put("remark", "备注");

            parameters.put("sign", PayForUtil.createSign("ISO-8859-1", parameters,WeChatConfig.HNAPIKEY));
            System.out.println("sign"+parameters.get("sign").toString());
            data = PayForUtil.getXml(parameters);
            System.out.println("data"+data);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return null;
        }
        return data;
    }


    /**
     *      * @Author: sun
     *      * @Description:  企业微信支付
     *      * @param re_openid 用户微信openid
     *      * @param orderNum 体现订单号
     *      * @param total_fee 体现金额
     *      * @Date: 2019-6-14
     *      * @return:
     *      
     */
    public static String withdrawal(String orderNum, String re_openid, String total_fee) {
//        StringBuffer xml = new StringBuffer();
        String data = null;
        try {
            String nonceStr =PayForUtil.buildRandom(4) + "";//生成32位随机字符串
//            xml.append("</xml>");
            SortedMap<Object, Object> parameters = new TreeMap<Object, Object>();

            parameters.put("mchid", WeChatConfig.MCHID);

            parameters.put("mch_appid", WeChatConfig.APPID);

            parameters.put("nonce_str", nonceStr);

            parameters.put("partner_trade_no", orderNum);

            parameters.put("openid", re_openid);

            parameters.put("check_name", "NO_CHECK");

            parameters.put("amount", total_fee);

//            parameters.put("total_num", "1");

            parameters.put("desc", total_fee);

            parameters.put("spbill_create_ip", PayForUtil.localIp());

            parameters.put("sign", PayForUtil.createSign("UTF-8", parameters,WeChatConfig.APIKEY));
            System.out.println("sign"+parameters.get("sign").toString());
            data = PayForUtil.getyiyeXml(parameters);
            System.out.println("data"+data);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return null;
        }
        return data;
    }


}
