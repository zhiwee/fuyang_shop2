//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.zhiwee.gree.webapi.Controller.shopBase;

import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.servlet.ModelAndView;
import xyz.icrab.common.web.annotation.PermissionMode;
import xyz.icrab.common.web.annotation.PermissionMode.Mode;
import xyz.icrab.common.web.session.Session;
import xyz.icrab.common.web.util.RequestUtils;
import xyz.icrab.common.web.util.RuntimeConfig;
import xyz.icrab.common.web.util.SessionUtils;

@RequestMapping({"/"})
public class ViewController {
    @Autowired(
        required = false
    )
    private RuntimeConfig runtimeConfig;
    private Map<String, String> errorPages;

    public ViewController() {
    }

    @GetMapping({"/view/**"})
    public ModelAndView view(HttpServletRequest req, HttpServletResponse res) {
        String view = extractPathFromPattern(req);
        return this.toView(view, this.getModel(req));
    }

    @PermissionMode(Mode.White)
    @GetMapping({"/login"})
    public ModelAndView login(HttpServletRequest req, HttpServletResponse res) {
        return this.toView("login", this.getModel(req));
    }

    @GetMapping({"/", "/index"})
    public ModelAndView index(HttpServletRequest req, HttpServletResponse res) {
        return this.toView("index", this.getModel(req));
    }

    @GetMapping({"/logout"})
    public ModelAndView logout(HttpServletRequest req, HttpServletResponse res) {
        SessionUtils.remove(req);
        return this.toView("redirect:/login", this.getModel(req));
    }

    @GetMapping({"/timeout"})
    public ModelAndView timeout(HttpServletRequest req, HttpServletResponse res) {
        return this.toView("redirect:/login", this.getModel(req));
    }

    @RequestMapping({"/notfound"})
    public ModelAndView notfound(HttpServletRequest req, HttpServletResponse res) {
        return this.toView("notfound", this.getModel(req));
    }

    private Map<String, Object> getModel(HttpServletRequest req) {
        Map<String, Object> model = new HashMap();
        model.put("param", RequestUtils.getParameters(req));
        Session session = SessionUtils.get();
        Map<String, Object> map = SessionUtils.getAttributes(session);
        map.remove("all:menus");
        map.remove("all:interfaces");
        model.put("session", map);
        return model;
    }

    public Map<String, String> getErrorPages() {
        return this.errorPages;
    }

    public void setErrorPages(Map<String, String> errorPages) {
        this.errorPages = errorPages;
    }

    private ModelAndView toView(String viewName, Map<String, Object> model) {
        ModelAndView mv;
        if (StringUtils.isBlank(viewName)) {
            mv = new ModelAndView();
            mv.setViewName("redirect:/notfound");
            return mv;
        } else {
            mv = new ModelAndView();
            mv.setViewName(viewName);
            mv.addAllObjects(model);
            return mv;
        }
    }

    private static String extractPathFromPattern(HttpServletRequest request) {
        String path = (String)request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);
        String bestMatchPattern = (String)request.getAttribute(HandlerMapping.BEST_MATCHING_PATTERN_ATTRIBUTE);
        return (new AntPathMatcher()).extractPathWithinPattern(bestMatchPattern, path);
    }







}
