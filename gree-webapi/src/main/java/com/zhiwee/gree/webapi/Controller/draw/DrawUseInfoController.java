package com.zhiwee.gree.webapi.Controller.draw;

import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.User;
import com.zhiwee.gree.model.card.LShopUserCards;
import com.zhiwee.gree.model.card.UserCardsInfo;
import com.zhiwee.gree.model.draw.*;
import com.zhiwee.gree.service.ShopUser.ShopUserService;
import com.zhiwee.gree.service.draw.CanDrawsService;
import com.zhiwee.gree.service.draw.DrawInfoService;
import com.zhiwee.gree.service.draw.DrawUseInfoService;
import com.zhiwee.gree.service.draw.DrawsInfoService;
import com.zhiwee.gree.webapi.util.SystemControllerLog;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.jeecgframework.poi.excel.ExcelExportUtil;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.enmus.ExcelType;
import xyz.icrab.common.model.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.util.KeyUtils;
import xyz.icrab.common.web.annotation.EnableGetRequest;
import xyz.icrab.common.web.annotation.EnableListRequest;
import xyz.icrab.common.web.annotation.PermissionMode;
import xyz.icrab.common.web.annotation.Session;
import xyz.icrab.ums.util.consts.SessionKeys;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.time.LocalDate;
import java.util.*;

/**
 * @author tzj
 */
@RequestMapping("/drawUseInfo")
@Controller
@ResponseBody
@EnableListRequest
@EnableGetRequest
public class DrawUseInfoController {
    @Autowired
    private DrawUseInfoService drawUseInfoService;
    @Autowired
    private CanDrawsService canDrawsService;
    @Autowired
    private DrawInfoService drawInfoService;
    @Autowired
    private DrawsInfoService drawsInfoService;

    @Autowired
    private ShopUserService shopUserService;

    /**
     * @author tzj
     * @description 抽奖
     * @date 2020/1/3 9:29
    */
    @RequestMapping(value = "/draw")
    @ResponseBody
    public Result<?> add(@Session("ShopUser") ShopUser user,@RequestBody Map<String,Object> param ) {

        user=  shopUserService.get(user.getId());
        if (user.getPhone()==null||"".equals(user.getPhone())){
            return Result.of(Status.ClientError.UNAUTHORIZED,"请完善手机号信息");
        }
        //判断用户是否有抽奖次数
        Boolean hasNum = checkShopUser(user);
        if (!hasNum) {
            return Result.of(Status.ClientError.PAYMENT_REQUIRED, "该用户抽奖次数已经用尽");
        }
        //查询当前抽奖
        List<DrawInfo> list = checkCurrentDraw();
        if (list.isEmpty()) {
            return Result.of(Status.ClientError.BAD_REQUEST, "当前没有进行的抽奖");
        }
        //结束
        if (list.get(0).getEndTime().before(new Date())){
            return Result.of(Status.ClientError.BAD_REQUEST, "当前抽奖已经结束了");
        }
            //总的中奖次数已经用完
        if (list.get(0).getUseSum().equals(list.get(0).getAllSum())) {
            return Result.of(Status.ClientError.BAD_REQUEST, "很遗憾，您未中奖");
        }

        //抽到什么
        DrawsInfo drawsInfo = drawUser(list.get(0));

        //更新个人抽奖次数
        CanDraws canDraws=new CanDraws();
        canDraws.setShopUserId(user.getId());
        canDraws=canDrawsService.getOne(canDraws);
        canDraws.setDrawsSum(canDraws.getDrawsSum()-1);
        canDrawsService.update(canDraws);
        if (drawsInfo == null) {
            //更新总数
            DrawInfo drawInfo = list.get(0);
            drawInfo.setUseSum(drawInfo.getUseSum()+1);
            drawInfoService.update(drawInfo);

            return Result.of(Status.ClientError.BAD_REQUEST, "很遗憾，您未中奖");
        } else {
            //更新中奖信息
            synchronized ("s2d") {
                List<DrawInfo> listConfirm = checkCurrentDraw();
                if (listConfirm.isEmpty()) {
                    return Result.of(Status.ClientError.BAD_REQUEST, "当前没有进行的抽奖");
                }
                if (listConfirm.get(0).getUseSum().equals(listConfirm.get(0).getAllSum())) {
                    return Result.of(Status.ClientError.BAD_REQUEST, "当前抽奖已经结束了");
                }
                DrawsInfo confirm = new DrawsInfo();
                confirm.setId(drawsInfo.getId());
                confirm = drawsInfoService.getOne(confirm);
                if (confirm.getUseDrawSum().equals(confirm.getDrawSum())) {
                    return null;
                }

                DrawUseInfo drawUseInfo = new DrawUseInfo();
                drawUseInfo.setId(KeyUtils.getKey());
                drawUseInfo.setCustomer(user.getNickname());
                drawUseInfo.setCustomerPhone(user.getPhone());
                drawUseInfo.setPrize(confirm.getPrize());
                drawUseInfo.setDrawId(confirm.getDrawId());
                drawUseInfo.setDrawName(listConfirm.get(0).getName());
                drawUseInfo.setDrawsId(confirm.getId());
                drawUseInfo.setDrawsName(confirm.getName());
                drawUseInfo.setCreatetime(new Date());
                drawUseInfo.setCustomerid(user.getId());
                drawUseInfo.setState(1);
                //更新奖项次数
                confirm.setUseDrawSum(confirm.getUseDrawSum() + 1);
                //更新奖池次数
                listConfirm.get(0).setUseSum(listConfirm.get(0).getUseSum() + 1);

                drawInfoService.update(listConfirm.get(0));

                drawsInfoService.update(confirm);

                drawUseInfoService.save(drawUseInfo);
                return Result.ok(drawUseInfo);
            }

        }
    }


    /**
     * @author tzj
     * @description 抽奖公共方法tzj
     * @date 2019/12/14 0:41
     */
    private DrawsInfo drawUser(DrawInfo drawInfo) {
        Map<String, Object> param = new HashMap<>(10);
        param.put("drawId", drawInfo.getId());
        List<DrawsInfo> list = drawsInfoService.listAll(param);

        if (list.isEmpty()) {
            return null;
        }
        //找到不限量的
        List<DrawsInfo> unLimit = checkSome(list);

        int prize = prize(list);
        //返回是否抽中
        if (prize != -1) {
            //改奖项还有剩余
            if ((list.get(prize - 1).getDrawSum() - list.get(prize - 1).getUseDrawSum()) > 0) {
                return list.get(prize - 1);
            } else {
                //没有剩余  随机返回不限量  或者空
                if (!unLimit.isEmpty()) {
                    return getSomeOne(unLimit);
                } else {
                    return null;
                }
            }
        } else {
            if (!unLimit.isEmpty()) {
                return getSomeOne(unLimit);
            } else {
                return null;
            }
        }
    }

    /**
     * @author tzj
     * @description 找到不限量的奖品
     * @date 2019/12/14 0:41
     */
    private List<DrawsInfo> checkSome(List<DrawsInfo> list) {
        List<DrawsInfo> unLimit = new ArrayList<>();
        for (DrawsInfo drawsInfo : list) {
            if (drawsInfo.getState() == 2) {
                unLimit.add(drawsInfo);
            }
        }
        return unLimit;
    }


    /**
     * @author tzj
     * @description 随机一个不限量的
     * @date 2019/12/14 0:42
     */
    private DrawsInfo getSomeOne(List<DrawsInfo> unLimit) {
        //产生一个1以内的随机数
        double mathNum = Math.random();
        //把不限量的划分成等分
        double publicNum = 1.0 / unLimit.size();
        //遍历去到随机数所在
        for (int i = 1; i < unLimit.size(); i++) {
            if (mathNum < publicNum) {
                return unLimit.get(0);
            } else {
                if (mathNum > i * publicNum && mathNum < publicNum * (i + 1)) {
                    return unLimit.get(i);
                }
            }
        }
        return null;
    }

    /**
     * 几等奖
     * @param list
     * @return
     */
    private int prize(List<DrawsInfo> list) {
        //用来累加概率
        double allChance = list.get(0).getChance();
        //抽奖
        int prize = 0;
        //中奖随机数
        double mathNum = Math.random();
        //遍历是否中奖
        for (int i = 1; i < list.size(); i++) {
            if (mathNum < list.get(0).getChance()) {
                prize = 1;
                break;
            }
            double probability = list.get(i).getChance();
            if (mathNum < (probability + allChance) && mathNum > allChance) {
                prize = i + 1;
                break;
            } else {
                allChance = allChance + list.get(i).getChance();
            }
        }
        //未中奖
        if (prize == 0) {
            return -1;
        } else {
            return prize;
        }
    }
    /**
     * 当前是否有抽奖
     * @return
     */
    private List<DrawInfo> checkCurrentDraw() {
        Date time = new Date();
        return drawInfoService.getCurrent(time);
    }

    /**
     * 用户是否可抽奖
     * @param shopUser
     * @return
     */
    private Boolean checkShopUser(ShopUser shopUser ) {
        CanDraws canDraws = new CanDraws();
        canDraws.setShopUserId(shopUser.getId());
        canDraws = canDrawsService.getOne(canDraws);
        if (canDraws != null) {
            return canDraws.getDrawsSum() > 0;
        } else {
            return false;
        }

    }

    @RequestMapping(value = "/infoPage")
    public Result<?> infoPage(@RequestBody Map<String, Object> params, Pagination pagination) {
        if (params == null) {
            params = new HashMap<>(10);
        }
        if (StringUtils.isNotBlank((String) params.get("endCreateTime"))) {
            params.put("endCreateTime", (String) params.get("endCreateTime") + " 23:59:59");
        }
        if (StringUtils.isNotBlank((String) params.get("startCreateTime"))) {
            params.put("endCreateTime", (String) params.get("endCreateTime") + " 23:59:59");
        }
        Pageable<DrawUseInfo> page = drawUseInfoService.pageInfo(params, pagination);
        return Result.ok(page);
    }

    @RequestMapping(value = "/getDrawInfo")
    public Result<?> getDrawInfo(@Session("ShopUser") ShopUser user) {
        Map<String, Object> map = new HashMap<>(10);
        map.put("customerid", user.getId());
        List<DrawUseInfo> list = drawUseInfoService.list(map);
        if (!list.isEmpty()) {
            return  Result.ok(list);
        }else {
            return  Result.of(Status.ClientError.BAD_REQUEST,"未找到中奖信息");
        }
    }

    /**
     * Description: 中奖导出
     *
     * @author: sun
     * @Date 下午4:36 2019/12/12
     * @param:
     * @return:
     */
    @RequestMapping("/export")
    public void export(HttpServletRequest req, HttpServletResponse rsp) throws IOException {
        Map<String, Object> params = new HashMap<>(10);
        params.put("startCreateTime", req.getParameter("startCreateTime"));
        params.put("endCreateTime", req.getParameter("endCreateTime"));
        params.put("drawId", req.getParameter("drawId"));
        params.put("prize", req.getParameter("prize"));

        if (StringUtils.isNotBlank((String) params.get("startCreateTime"))) {
            params.put("startCreateTime", (String) params.get("startCreateTime") + " 00:00:00");
        }
        if (StringUtils.isNotBlank((String) params.get("endCreateTime"))) {
            params.put("endCreateTime", (String) params.get("endCreateTime") + " 23:59:59");
        }
        List<DrawUseInfoExrt> result;
        result = drawUseInfoService.queryDrawUseInfoExport(params);

        // 导出Excel
        OutputStream os = rsp.getOutputStream();

        rsp.setContentType("application/x-download");
        // 设置导出文件名称
        rsp.setHeader("Content-Disposition",
                "attachment;filename=" + DateFormatUtils.format(new Date(), "yyyyMMddHHmmssSSS") + ".xlsx");
        ExportParams param = new ExportParams("中将名单", "中将人员相关信息", "中将信息表");
        param.setAddIndex(true);
        param.setType(ExcelType.XSSF);
        Workbook excel = ExcelExportUtil.exportExcel(param, DrawUseInfoExrt.class, result);
        excel.write(os);
        os.flush();
        os.close();
    }


    @SystemControllerLog(description = "手机号校验礼品")
    @RequestMapping("/checkUserDrawInfo")
    @PermissionMode(PermissionMode.Mode.White)
    @ResponseBody
    public Result<?> checkUserDrawInfo(HttpServletRequest request, @RequestBody Map<String, Object> param) {
        List<DrawUseInfo> list = drawUseInfoService.list(param);
        if (!list.isEmpty()) {
            return Result.ok(list.get(0));
        } else {
            return Result.of(Status.ClientError.BAD_REQUEST, "未查到相关礼品！");
        }
    }


    @SystemControllerLog(description = "核销礼品")
    @RequestMapping("/clearUserDraw")
    @PermissionMode(PermissionMode.Mode.White)
    @ResponseBody
    public Result<?> clearUserDraw(@Session(SessionKeys.USER) User user,
                                   @RequestBody Map<String, Object> param) {
        DrawUseInfo drawUseInfo = drawUseInfoService.getOne(param);
        if (drawUseInfo != null) {
            drawUseInfo.setState(2);
            drawUseInfo.setUpdatetime(new Date());
            drawUseInfo.setSysuserid(user.getId());
            drawUseInfo.setSysusername(user.getNickname());

            drawUseInfoService.update(drawUseInfo);
            return Result.of(Status.Success.OK, "领取成功");
        } else {
            return Result.of(Status.ClientError.BAD_REQUEST, "您尚未获得该奖品");
        }
    }


    /**
     * @author tzj
     * @description 领取礼品
     * @date 2020/4/13 17:03
    */
    @RequestMapping(value = "/getGift")
    public Result<?> getGift(@Session("ShopUser") ShopUser user,@RequestBody Map<String,Object> param) {
       String id= (String) param.get("keyId");
        DrawUseInfo drawUseInfo = drawUseInfoService.get(id);
        if (drawUseInfo!=null) {
            if(drawUseInfo.getState()==2){
                return  Result.of(Status.ClientError.BAD_REQUEST,"您已经领取成功，无需点击！");
            }
            drawUseInfo.setState(2);
            drawUseInfo.setUpdatetime(new Date());
          drawUseInfoService.update(drawUseInfo);
            return  Result.ok("领取成功");
        }else {
            return  Result.of(Status.ClientError.BAD_REQUEST,"未找到中奖信息！");
        }
    }

}
