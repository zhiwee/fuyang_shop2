package com.zhiwee.gree.webapi.Controller.fyGoods;


import com.zhiwee.gree.model.GoodsInfo.GoodsDealer;
import com.zhiwee.gree.service.GoodsInfo.GoodsDealerService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.util.KeyUtils;
import xyz.icrab.common.web.annotation.*;
import xyz.icrab.common.web.param.IdParam;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author DELL
 */
@RestController
@RequestMapping("/goodsDealer")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class GoodsDealerController {
    @Resource
    private GoodsDealerService goodsDealerService;

    /**
     * @author tzj
     * @description 页面
     * @date 2020/4/2 10:14
     */
    @RequestMapping("/page")
    public Result<?> page(@RequestBody Map<String, Object> param, Pagination pagination) {
        Pageable<GoodsDealer> pageable = goodsDealerService.page(param, pagination);
        return Result.ok(pageable);
    }

    /**
     * @author tzj
     * @description 新增
     * @date 2020/4/2 10:14
     */
    @RequestMapping("/add")
    public Result<?> add(@RequestBody GoodsDealer goodsLabel) {
        goodsLabel.setId(KeyUtils.getKey());
        goodsLabel.setCreateTime(new Date());
        goodsDealerService.save(goodsLabel);
        return Result.ok();
    }


    @RequestMapping("/selectOne")
    @ResponseBody
    public Result<?> selectOne(@RequestBody IdParam param) {
        GoodsDealer goodsCategory = goodsDealerService.get(param.getId());
        return Result.ok(goodsCategory);
    }


    @RequestMapping("/update")
    public Result<?> update(@RequestBody GoodsDealer goodsLabel) {
        goodsLabel.setUpdateTime(new Date());
        goodsDealerService.update(goodsLabel);
        return Result.ok();
    }

    /**
     * @author tzj
     * @description 家电卖场列表
     * @date 2020/4/20 16:51
    */
    @RequestMapping("/list")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> list(@RequestBody Map<String, Object> param) {
        List<GoodsDealer> list = goodsDealerService.list(param);
        return Result.ok(list);
    }






}
