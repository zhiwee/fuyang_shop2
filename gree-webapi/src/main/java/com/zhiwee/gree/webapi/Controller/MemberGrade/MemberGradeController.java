package com.zhiwee.gree.webapi.Controller.MemberGrade;

import com.zhiwee.gree.model.MemberGrade.MemberGrade;
import com.zhiwee.gree.service.MemberGrade.MemberGradeService;
import com.zhiwee.gree.webapi.util.SystemControllerLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.web.annotation.*;
import xyz.icrab.common.web.param.IdParam;
import xyz.icrab.common.web.util.Validator;

import javax.validation.Valid;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author sun on 2019/4/30
 */
@RestController
@RequestMapping("/memberGrade")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class MemberGradeController {
    @Autowired
    private MemberGradeService memberGradeService;
    /**
     * Description:  查询所有客户等级
     * @author: sun
     * @Date 下午10:40 2019/3/27
     * @param:
     * @return:
     */
    @SystemControllerLog(description="查询所有客户等级")
    @RequestMapping(value = "searchAll")
    public Result<?>  searchAll(@RequestBody(required = false) Map<String, Object> params,Pagination pagination) {
        Pageable<MemberGrade> memberGradePageable = memberGradeService.searchAll(params, pagination);
        return Result.ok(memberGradePageable);
    }



 /**
  * Description: 添加会员等级
  * @author: sun
  * @Date 上午10:21 2019/6/19
  * @param:
  * @return:
  */
    @RequestMapping(value = "/addMemberGrade")
    public Result<?> addHomePage( @RequestBody MemberGrade memberGrade)  {
        Validator validator = new Validator();
        validator.notEmpty(memberGrade.getName(), "等级名称不能为空");
//        validator.notNull(homePage.getType(), "类型不能为空");
        validator.notNull(memberGrade.getEndAmount(), "结束金额为空");
        validator.notNull(memberGrade.getStartAmount(), "开始金额不能为空");
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }
        if(memberGrade.getStartAmount()>= memberGrade.getEndAmount()){
            return Result.of(Status.ClientError.BAD_REQUEST, "开始金额不能大于结束金额");
        }
        Map<String, Object> params = new HashMap<>();
        params.put("max",1);
       MemberGrade member =  memberGradeService.selectMax(params);
        if(member != null) {
            if (member.getEndAmount() >= memberGrade.getStartAmount()) {
                return Result.of(Status.ClientError.BAD_REQUEST, "和"+member.getName()+"存在区间重复");
            }
        }
        memberGrade.setState(1);
        memberGrade.setIsdefault(2);
        memberGrade.setCreateTime(new Date());
        memberGradeService.save(memberGrade);
        return Result.ok();
    }



    /**
     * Description: 更新会员等级
     * @author: sun
     * @Date 上午10:21 2019/6/19
     * @param:
     * @return:
     */
    @RequestMapping(value = "/updateMemberGrade")
    public Result<?> updateMemberGrade( @RequestBody MemberGrade memberGrade)  {
        Validator validator = new Validator();
        validator.notEmpty(memberGrade.getName(), "等级名称不能为空");
//        validator.notNull(homePage.getType(), "类型不能为空");
        validator.notNull(memberGrade.getEndAmount(), "结束金额为空");
        validator.notNull(memberGrade.getStartAmount(), "开始金额不能为空");
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }
        if(memberGrade.getStartAmount()>= memberGrade.getEndAmount()){
            return Result.of(Status.ClientError.BAD_REQUEST, "开始金额不能大于结束金额");
        }
        Map<String, Object> params = new HashMap<>();
        params.put("maxId",memberGrade.getId());
        //比他大的最小的
        MemberGrade maxmember =  memberGradeService.selectMax(params);
        if(maxmember != null) {
            if (maxmember.getStartAmount() <= memberGrade.getEndAmount()) {
                return Result.of(Status.ClientError.BAD_REQUEST, "和"+maxmember.getName()+"存在区间重复");
            }
        }
        Map<String, Object> param = new HashMap<>();
        param.put("minId",memberGrade.getId());
        //比他小的最大的
        MemberGrade minmember =  memberGradeService.selectMax(param);
        if(minmember != null) {
            if (minmember.getEndAmount() >= memberGrade.getStartAmount()) {
                return Result.of(Status.ClientError.BAD_REQUEST, "和"+minmember.getName()+"存在区间重复");
            }
        }
        memberGrade.setUpdateTime(new Date());
        memberGradeService.update(memberGrade);
        return Result.ok();
    }



    /**
     * Description: 获取会员等级
     * @author: sun
     * @Date 上午10:21 2019/6/19
     * @param:
     * @return:
     */
    @RequestMapping(value = "/achieveMemberGrade")
    public Result<?> achieveMemberGrade( @RequestBody MemberGrade memberGrade)  {
        MemberGrade member = memberGradeService.getOne(memberGrade);
        return Result.ok(member);
    }



/**
 * Description: 等级启用
 * @author: sun
 * @Date 上午11:35 2019/6/19
 * @param:
 * @return:
 */
    @RequestMapping("/open")
    @ResponseBody
    public  Result<?> open(@RequestBody MemberGrade memberGrade ){
        memberGrade.setState(1);
        memberGradeService.update(memberGrade);
        return Result.ok();
    }


    /**
     * Description: 等级禁用
     * @author: sun
     * @Date 上午11:35 2019/6/19
     * @param:
     * @return:
     */
    @RequestMapping("/stop")
    @ResponseBody
    public  Result<?> stop(@RequestBody MemberGrade memberGrade ){
        MemberGrade member = memberGradeService.getOne(memberGrade);
        if(member.getIsdefault() == 1){
            return Result.of(Status.ClientError.BAD_REQUEST, "这个是默认等级不能禁用");
        }
        memberGrade.setState(2);
        memberGradeService.update(memberGrade);
        return Result.ok();
    }



    /**
     * Description: 等级设置默认，只会传过来不是默认的等级的id
     * @author: sun
     * @Date 上午11:35 2019/6/19
     * @param:
     * @return:
     */
    @RequestMapping("/openDefault")
    @ResponseBody
    public  Result<?> openDefault(@RequestBody MemberGrade memberGrade ){
        Map<String ,Object> param = new HashMap<>();
        param.put("isdefault",1);
        List<MemberGrade> list = memberGradeService.list(param);
        if(!list.isEmpty() && list.size() > 0){
            return Result.of(Status.ClientError.BAD_REQUEST, "存在默认等级，无法设置");
        }

        memberGrade.setIsdefault(1);
        memberGradeService.update(memberGrade);
        return Result.ok();
    }

    /**
     * Description: 等级取消默认
     * @author: sun
     * @Date 上午11:35 2019/6/19
     * @param:
     * @return:
     */
    @RequestMapping("/stopDefault")
    @ResponseBody
    public  Result<?> stopDefault(@RequestBody MemberGrade memberGrade ){
        memberGrade.setIsdefault(2);
        memberGradeService.update(memberGrade);
        return Result.ok();
    }
}
