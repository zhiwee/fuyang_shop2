package com.zhiwee.gree.webapi.Controller.MemberGrade;

import com.zhiwee.gree.model.MemberGrade.MemberGrade;
import com.zhiwee.gree.model.MemberGrade.MemberGradeChange;
import com.zhiwee.gree.service.MemberGrade.MemberGradeChangeService;
import com.zhiwee.gree.service.MemberGrade.MemberGradeService;
import com.zhiwee.gree.webapi.util.SystemControllerLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.web.annotation.EnableDeleteRequest;
import xyz.icrab.common.web.annotation.EnableGetRequest;
import xyz.icrab.common.web.annotation.EnableListRequest;
import xyz.icrab.common.web.annotation.EnablePageRequest;
import xyz.icrab.common.web.util.Validator;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author sun on 2019/4/30
 */
@RestController
@RequestMapping("/memberGradeChange")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class MemberGradeChangeController {
    @Autowired
    private MemberGradeChangeService memberGradeChangeService;

/**
 * Description: 查询所有会员基础信息改变记录
 * @author: sun
 * @Date 下午4:52 2019/6/19
 * @param:
 * @return:
 */
    @RequestMapping(value = "searchAll")
    public Result<?>  searchAll(@RequestBody(required = false) Map<String, Object> params,Pagination pagination) {
        Pageable<MemberGradeChange> memberGradeChangePageable = memberGradeChangeService.searchAll(params, pagination);
        return Result.ok(memberGradeChangePageable);
    }



}
