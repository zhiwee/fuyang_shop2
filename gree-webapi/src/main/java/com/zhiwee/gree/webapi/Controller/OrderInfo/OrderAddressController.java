package com.zhiwee.gree.webapi.Controller.OrderInfo;

import com.zhiwee.gree.model.GoodsInfo.ShopGoodsInfo;
import com.zhiwee.gree.model.OrderInfo.Order;
import com.zhiwee.gree.model.OrderInfo.OrderAddress;
import com.zhiwee.gree.model.OrderInfo.OrderItem;
import com.zhiwee.gree.model.User;
import com.zhiwee.gree.service.GoodsInfo.GoodsInfoService;
import com.zhiwee.gree.service.GoodsInfo.ShopGoodsInfoService;
import com.zhiwee.gree.service.OrderInfo.OrderAddressService;
import com.zhiwee.gree.service.OrderInfo.OrderItemService;
import com.zhiwee.gree.service.OrderInfo.OrderService;
import com.zhiwee.gree.webapi.util.SystemControllerLog;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.web.annotation.*;
import xyz.icrab.common.web.param.IdParam;
import xyz.icrab.common.web.util.Validator;
import xyz.icrab.ums.util.consts.SessionKeys;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * @author sun on 2019/4/30
 */
@RestController
@RequestMapping("/orderAddress")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class OrderAddressController {

    @Autowired
    private OrderAddressService orderAddressService;


    @Autowired
    private OrderService orderService;

/**
 * Description: 获取订单订单
 * @author: sun
 * @Date 下午10:48 2019/6/1
 * @param:
 * @return:
 */
    @RequestMapping("/achieveOrderAddress")
    public Result<?> achieveOrderAddress(@RequestBody OrderAddress orderAddress) {
        Map<String,Object> param = new HashMap<>();
        param.put("orderId",orderAddress.getOrderId());
        OrderAddress address = orderAddressService.getOne(param);
        return Result.ok(address);
    }



    /**
     * Description: 修改订单的收货人信息
     * @author: sun
     * @Date 下午10:48 2019/6/1
     * @param:
     * @return:
     */
    @RequestMapping("/updateOrderAddress")
    public Result<?> updateOrderAddress(@RequestBody OrderAddress orderAddress) {
        Validator validator = new Validator();
        validator.notEmpty(orderAddress.getProvinceCode(), "省不能为空");
        validator.notEmpty(orderAddress.getCityCode(), "市不能为空");
        validator.notEmpty(orderAddress.getAreaCode(), " 区不能为空");
        validator.notEmpty(orderAddress.getDeliveryPhone(), "收货号码不能为空");
        validator.notEmpty(orderAddress.getDeliveryUser(), "收货人不能为空");
        validator.notEmpty(orderAddress.getDetailAddress(), "详细地址不能为空");
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }
        Map<String,Object> param = new HashMap<>();
        param.put("orderId",orderAddress.getOrderId());
        Order order = orderService.get(orderAddress.getOrderId());

        OrderAddress address = orderAddressService.getOne(param);
        Boolean flag = false;
        if(!orderAddress.getAreaCode().equals(orderAddress.getAreaCode()) && (order.getIsOffline() == 1 || order.getIsOffline() == 3)){
            flag = true;
        }
        orderAddressService.updateOrders(orderAddress,address,flag);
        return Result.ok(address);
    }




}
