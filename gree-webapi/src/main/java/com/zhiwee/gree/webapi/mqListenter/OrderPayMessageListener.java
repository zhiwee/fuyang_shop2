package com.zhiwee.gree.webapi.mqListenter;

import com.zhiwee.gree.model.FyGoods.FyOrder;
import com.zhiwee.gree.model.FyGoods.FyOrderItem;
import com.zhiwee.gree.model.FyGoods.Fygoods;
import com.zhiwee.gree.model.FyGoods.GrouponGoods;
import com.zhiwee.gree.model.GoodsInfo.Goods;
import com.zhiwee.gree.service.FyGoods.FyOrderItemService;
import com.zhiwee.gree.service.FyGoods.FyOrderService;
import com.zhiwee.gree.service.FyGoods.FygoodsService;
import com.zhiwee.gree.service.FyGoods.GrouponGoodsService;
import com.zhiwee.gree.service.GoodsInfo.GoodsService;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class OrderPayMessageListener  implements MessageListener {

    @Resource
    private FyOrderService  fyOrderService;
    @Resource
    private FyOrderItemService   fyOrderItemService;
    //@Autowired
   //// private RabbitTemplate rabbitTemplate;

    @Resource
    private GoodsService  goodsService;

    @Resource
    private FygoodsService  fygoodsService;
    @Resource
    private GrouponGoodsService grouponGoodsService;

    @Override
    public void onMessage(Message message) {
        System.out.println("2执行了");
        System.out.println("2执行了");
        System.out.println("2执行了");
        System.out.println("2执行了");
        String   orderNum = new String(message.getBody());

        Map<String,Object> params  =  new HashMap<>();

         //进行更新操作
        params.put("orderNum",orderNum);
        params.put("state",2);//1表示已支付
        fyOrderService.updateByOrderNum(params);


        //根据订单号查询订单详情
        Map<String,Object>  map2 =  new HashMap<>();
        map2.put("orderNum",orderNum);
        List<FyOrderItem> fyOrderItemList  =  fyOrderItemService.list(map2);
        //循环更新库存
//        for(FyOrderItem  item : fyOrderItemList){
//
//            Goods  goods  = goodsService.get(item.getFyGoodsId());//长益商品
//            Fygoods   fygoods  = fygoodsService.get(item.getFyGoodsId());//秒杀商品
//            GrouponGoods grouponGoods  = grouponGoodsService.get(item.getFyGoodsId());//拼团商品
//
//            if(null != goods){
//                goods.setStoreCount(goods.getStoreCount()-item.getCount());
//                goodsService.update(goods);
//            }
//            if(null != grouponGoods){
//                grouponGoods.setStoreCount(grouponGoods.getStoreCount()-item.getCount());
//                grouponGoodsService.update(grouponGoods);
//            }
//            if(null != fygoods){
//                fygoods.setStoreCount(fygoods.getStoreCount()-item.getCount());
//                fygoodsService.update(fygoods);
//            }
//
//        }




        //更新数据库里面商品的库存
        // goods.setStoreCount(goods.getStoreCount()-count);//减库存
        // goodsService.update(goods);//更新操作



        //消费做延迟的消息队列里面的消息
        //rabbitTemplate.receive("queue.delay.order.begin");
    }
}
