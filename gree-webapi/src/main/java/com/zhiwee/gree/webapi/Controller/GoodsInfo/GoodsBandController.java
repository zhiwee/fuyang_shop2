package com.zhiwee.gree.webapi.Controller.GoodsInfo;

import com.zhiwee.gree.model.GoodsInfo.GoodsBand;
import com.zhiwee.gree.model.GoodsInfo.GoodsClassify;
import com.zhiwee.gree.model.Menu;
import com.zhiwee.gree.service.GoodsInfo.GoodsBandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.enums.StatusEnum;
import xyz.icrab.common.util.KeyUtils;
import xyz.icrab.common.web.annotation.PermissionMode;
import xyz.icrab.common.web.param.IdParam;
import xyz.icrab.common.web.param.IdsParam;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 品牌
 */
@ResponseBody
@RequestMapping("/goodsBand")
@RestController
public class GoodsBandController {

    @Autowired
    private GoodsBandService goodsBandService;

    /***
     * 分页查询，周广
     * @param param
     * @param pagination
     * @return
     */
    @RequestMapping("/page")
    public Result<?> page(@RequestBody Map<String, Object> param, Pagination pagination) {
        if (param == null) {
            param = new HashMap<>();
        }
        param.put("state", new Object[]{1, 2});
        Pageable<GoodsBand> pageable = goodsBandService.getPage(param, pagination);
        return Result.ok(pageable);
    }


    /**
     * @author tzj
     * @description 根据楼层获取品牌
     * @date 2020/4/26 17:31
    */
    @RequestMapping("/getBrandByFloor")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> getBrandByFloor( @RequestBody(required = false)  Map<String,Object> param){
        List<GoodsBand> list = goodsBandService.list(param);
        return Result.ok(list);
    }
    /**
     * 品牌增加  周广
     * @param goodsBand
     * @return
     */
    @RequestMapping("/add")
    public Result<?> add(@RequestBody GoodsBand goodsBand){
        goodsBand.setId(KeyUtils.getKey());
        goodsBandService.save(goodsBand);
        return Result.ok();
    }
    /**
     * 品牌更新  周广
     * @param goodsBand
     * @return
     */
    @RequestMapping("/update")
    public Result<?> update(@RequestBody GoodsBand goodsBand){
        goodsBandService.update(goodsBand);
        return Result.ok();
    }

    /***
     * 查询单个  周广
     * @param param
     * @return
     */
    @RequestMapping("/selectOne")
    @ResponseBody
    public Result<?> selectOne(@RequestBody IdParam param){
        return Result.ok(goodsBandService.get(param.getId()));
    }

    /***
     * 启用  周广
     * @param param
     * @return
     */
    @RequestMapping("/enable")
    @ResponseBody
    public Result<?> enable(@RequestBody @Valid IdsParam param){
        for (String id : param.getIds()) {
            GoodsBand goodsBand=new GoodsBand();
            goodsBand.setId(id);
            goodsBand.setState(1);
            goodsBandService.update(goodsBand);
        }
        return Result.ok();
    }

    /***
     * 禁用  周广
     * @param param
     * @return
     */
    @RequestMapping("/disable")
    @ResponseBody
    public Result<?> disable(@RequestBody @Valid IdsParam param){
        for (String id : param.getIds()) {
            GoodsBand goodsBand=new GoodsBand();
            goodsBand.setId(id);
            goodsBand.setState(2);
            goodsBandService.update(goodsBand);
        }
        return Result.ok();
    }


    /***
     * 删除 周广
     * @param param
     * @return
     */
    @RequestMapping("/delete")
    @ResponseBody
    public Result<?> delete(@RequestBody @Valid IdsParam param){
        for (String id : param.getIds()) {
            GoodsBand goodsBand=new GoodsBand();
            goodsBand.setId(id);
            goodsBandService.delete(goodsBand);
        }
        return Result.ok();
    }


}
