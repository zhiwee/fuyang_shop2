package com.zhiwee.gree.webapi.Controller.OnlineActivity;

import com.zhiwee.gree.model.Activity.ActivityInfo;
import com.zhiwee.gree.model.Dealer.DealerInfo;
import com.zhiwee.gree.model.GoodsInfo.GoodVo.PeopleExr;
import com.zhiwee.gree.model.GoodsInfo.Goods;
import com.zhiwee.gree.model.GoodsInfo.LAGoodsInfo;
import com.zhiwee.gree.model.GoodsInfo.LAPeople;
import com.zhiwee.gree.model.OnlineActivity.OnlineActivityInfo;
import com.zhiwee.gree.model.OnlineActivity.OnlineActivityInfoExe;
import com.zhiwee.gree.model.User;
import com.zhiwee.gree.model.enums.OnlineActivityType;
import com.zhiwee.gree.service.Activity.ActivityInfoService;
import com.zhiwee.gree.service.GoodsInfo.GoodsInfoService;
import com.zhiwee.gree.service.OnlineActivity.OnlineActivityInfoService;
import com.zhiwee.gree.webapi.util.SystemControllerLog;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jeecgframework.poi.excel.ExcelExportUtil;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.entity.enmus.ExcelType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.util.IdGenerator;
import xyz.icrab.common.util.KeyUtils;
import xyz.icrab.common.web.annotation.EnableListRequest;
import xyz.icrab.common.web.annotation.EnablePageRequest;
import xyz.icrab.common.web.annotation.PermissionMode;
import xyz.icrab.common.web.annotation.Session;
import xyz.icrab.common.web.param.IdParam;
import xyz.icrab.common.web.param.IdsParam;
import xyz.icrab.common.web.util.Validator;
import xyz.icrab.ums.util.consts.SessionKeys;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.security.Key;
import java.text.NumberFormat;
import java.util.*;

import static com.zhiwee.gree.util.ExcelUtil.getValue;

/**
 * @author SUN
 * @since 2018/7/6 14:22
 */
@Controller
@RequestMapping("/onlineActivityInfo")
@EnablePageRequest
@EnableListRequest
public class OnlineActivityInfoController {

   
    @Autowired
    private OnlineActivityInfoService onlineActivityInfoService;

    @Autowired
    private  ActivityInfoService activityInfoService;

    @Autowired

    private GoodsInfoService goodsInfoService;

    /**
     * Description: 获取线上正在进行的活动
     * @author: sun
     * @Date 上午12:55 2019/5/16
     * @param:
     * @return:
     */
    @RequestMapping("/onlineCurrent")
    @ResponseBody
    public Result<?> onlineCurrent(@RequestBody OnlineActivityInfo info, @Session(SessionKeys.USER) User user) {


        List<OnlineActivityInfo> list = onlineActivityInfoService.onlineCurrent();

        return Result.ok(list);
    }



    /**
  * Description: 添加活动
  * @author: sun
  * @Date 上午12:55 2019/5/16
  * @param:
  * @return:
  */
    @RequestMapping("/add")
    @ResponseBody
    public Result<?> add(@RequestBody OnlineActivityInfo info, @Session(SessionKeys.USER) User user) {


        Validator validator = new Validator();
        validator.notEmpty(info.getName(), "活动名称不能为空");
        validator.notNull(info.getBeginTime(), "开始时间不能为空");
        validator.notNull(info.getEndTime(), "结束时间不能为空");
        validator.isTrue(info.getBeginTime().before(info.getEndTime()), "开始时间必须小于结束时间");

        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }


        Map<String, Object> param = new HashMap<>();
        param.put("activityType", info.getActivityType());
        param.put("state", 1);
        List<OnlineActivityInfo> list = onlineActivityInfoService.list(param);
        if(!list.isEmpty()){
            for (OnlineActivityInfo onlineActivityInfo : list) {
                if ( (info.getBeginTime().before(onlineActivityInfo.getBeginTime()) && info.getEndTime().after(onlineActivityInfo.getBeginTime()))
                || (info.getBeginTime().after(onlineActivityInfo.getBeginTime()) && info.getBeginTime().before(onlineActivityInfo.getEndTime()))
                ) {
                    return Result.of(Status.ClientError.BAD_REQUEST, "已有同类型活动在当前时间段:"+onlineActivityInfo.getName());
                }
            }
        }
            if (info.getActivityId() !=null && !info.getActivityId().equals("")){
                ActivityInfo activityInfo=new ActivityInfo();
                activityInfo.setId(info.getActivityId());
                activityInfo= activityInfoService.getOne(activityInfo);
                info.setActivityName(activityInfo.getName());
            }
        info.setState(1);
        info.setId(IdGenerator.objectId());
        info.setCreatetime(new Date());
        info.setCreateman(user.getId());


        //添加行活动信息
        onlineActivityInfoService.save(info);

        return Result.ok();
    }

/**
 * Description: 更新活动
 * @author: sun
 * @Date 上午12:55 2019/5/16
 * @param:
 * @return:
 */
    @RequestMapping("/update")
    @ResponseBody
    public Result<?> update(@RequestBody OnlineActivityInfo info, @Session(SessionKeys.USER) User user) {
        Validator validator = new Validator();
        validator.notEmpty(info.getName(), "活动名称不能为空");
        validator.notNull(info.getBeginTime(), "开始时间不能为空");
        validator.notNull(info.getEndTime(), "结束时间不能为空");
        validator.isTrue(info.getBeginTime().before(info.getEndTime()), "开始时间必须小于结束时间");
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }
        Map<String, Object> param = new HashMap<>();
        param.put("activityType", info.getActivityType());
        param.put("state", 1);
        List<OnlineActivityInfo> list = onlineActivityInfoService.list(param);
        if(!list.isEmpty()){
            for (OnlineActivityInfo onlineActivityInfo : list) {
                if ( (info.getBeginTime().before(onlineActivityInfo.getBeginTime()) && info.getEndTime().after(onlineActivityInfo.getBeginTime()))
                        || (info.getBeginTime().after(onlineActivityInfo.getBeginTime()) && info.getBeginTime().before(onlineActivityInfo.getEndTime()))){
                    return Result.of(Status.ClientError.BAD_REQUEST, "已有同类型活动在当前时间段:"+onlineActivityInfo.getName());
                }
            }
        }
        if (info.getActivityId() ==null || info.getActivityId().equals("")){
            info.setActivityName("");
            info.setActivityId("");
        }else {
            ActivityInfo activityInfo=new ActivityInfo();
            activityInfo.setId(info.getActivityId());
            activityInfo= activityInfoService.getOne(activityInfo);
            info.setActivityName(activityInfo.getName());
        }

        info.setUpdatetime(new Date());
        info.setUpdateman(user.getId());
        onlineActivityInfoService.update(info);
        return Result.ok();
    }

    /**
     * Description: 根据活动id获取活动信息
     * @author: sun
     * @Date 下午11:45 2019/5/15
     * @param:
     * @return:
     */
    @RequestMapping("/achieveOnlineActivity")
    @ResponseBody
    public Result<OnlineActivityInfo> achieveOnlineActivity(@Session(SessionKeys.USER) User user, @RequestBody @Valid IdParam param) {
        OnlineActivityInfo onlineActivityInfo = onlineActivityInfoService.get(param.getId());
        return Result.ok(onlineActivityInfo);
    }

/**
 * Description: 获取线上活动
 * @author: sun
 * @Date 下午11:45 2019/5/15
 * @param:
 * @return:
 */
    @RequestMapping("/page")
    @ResponseBody
    public Result<Pageable<OnlineActivityInfo>> page(@Session(SessionKeys.USER) User user, @RequestBody(required = false) Map<String, Object> param, Pagination pagination) {
        if (param == null) {
            param = new HashMap<>();
        }
        Pageable<OnlineActivityInfo> pageable = onlineActivityInfoService.page(param, pagination);
        return Result.ok(pageable);
    }

    /**
     * Description: 启用活动
     * @author: sun
     * @Date 上午12:51 2019/5/16
     * @param:
     */
    @RequestMapping("/enable")
    @ResponseBody
    public Result<?> enable(@RequestBody @Valid IdsParam param, @Session(SessionKeys.USER) User user) {
        OnlineActivityInfo info = new OnlineActivityInfo();
        Map<String,Object> params = new HashMap<>();
        params.put("id",param.getIds().get(0));
        List<OnlineActivityInfo> list = onlineActivityInfoService.list(params);
        if(!list.isEmpty()){
            info=list.get(0);
            Map<String,Object> paramS = new HashMap<>();
            params.put("state",1);
            params.put("activityType",list.get(0).getActivityType());
            List<OnlineActivityInfo> listState = onlineActivityInfoService.list(paramS);
            for (OnlineActivityInfo onlineActivityInfo:listState){
                if ( (info.getBeginTime().before(onlineActivityInfo.getBeginTime()) && info.getEndTime().after(onlineActivityInfo.getBeginTime()))
                        || (info.getBeginTime().after(onlineActivityInfo.getBeginTime()) && info.getBeginTime().before(onlineActivityInfo.getEndTime())) ){
                    return Result.of(Status.ClientError.BAD_REQUEST, "已有同类型活动在当前时间段:"+onlineActivityInfo.getName());
                }
            }



        }

        for (String id : param.getIds()) {
            info.setId(id);
            info.setState(1);
            info.setUpdatetime(new Date());
            info.setCreateman(user.getId());
            onlineActivityInfoService.update(info);
        }
        return Result.ok();
    }
/**
 * Description: 禁用活动
 * @author: sun
 * @Date 上午12:51 2019/5/16
 * @param:
 * @return:
 */
    @RequestMapping("/disable")
    @ResponseBody
    public Result<?> disable(@RequestBody @Valid IdsParam param, @Session(SessionKeys.USER) User user) {
        OnlineActivityInfo info = new OnlineActivityInfo();
        for (String id : param.getIds()) {
            info.setId(id);
            info.setState(2);
            info.setUpdatetime(new Date());
            info.setCreateman(user.getId());
            onlineActivityInfoService.update(info);
        }
        return Result.ok();
    }

 /**
  * Description: 获取线上所有活动
  * @author: sun
  * @Date 上午1:51 2019/5/16
  * @param:
  * @return:
  */
    @SystemControllerLog(description = "获取线上所有活动")
    @RequestMapping("/list")
    @ResponseBody
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> list(@RequestBody(required = false) Map<String, Object> param) {
        List<OnlineActivityInfo> list = onlineActivityInfoService.list(param);
        return Result.ok(list);
    }


    @RequestMapping("/get")
    @ResponseBody
    @PermissionMode(PermissionMode.Mode.White)
    public Result<OnlineActivityInfo> get(@RequestBody @Valid IdParam param) {
        return Result.ok(onlineActivityInfoService.get(param.getId()));
    }







    public static char[] generate() {
        char[] letters = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
                'K', 'L', 'M', 'N', 'O', 'P','Q', 'R', 'S', 'T', 'U', 'V',
                'W', 'X', 'Y', 'Z','0','1','2','3','4','5','6','7','8','9'};
        boolean[] flags = new boolean[letters.length];
        char[] chs = new char[6];
        for (int i = 0; i < chs.length; i++) {
            int index;
            do {
                index = (int) (Math.random() * (letters.length));
            } while (flags[index]);// 判断生成的字符是否重复
            chs[i] = letters[index];
            flags[index] = true;
        }
        return chs;
    }





    /**
     * @author tzj
     * @description  导出活动定性
     * @date 2019/9/24 22:04
     * @return
    */
    @RequestMapping("/exportActivityInfo")
    @SuppressWarnings("all")
    public void exportTicket(HttpServletRequest req, HttpServletResponse rep) throws IOException {
        Map<String, Object> params = new HashMap<>();
        if (req.getParameter("state") != null && req.getParameter("state") != "") {
            params.put("state", req.getParameter("state"));
        }
        List<OnlineActivityInfoExe> result = onlineActivityInfoService.exportActivityInfo(params);

        OutputStream os = rep.getOutputStream();

        rep.setContentType("application/x-download");
        rep.setHeader("Content-Disposition",
                "attachment;filename=" + DateFormatUtils.format(new Date(), "yyyyMMddHHmmssSSS") + ".xlsx");
        ExportParams param = new ExportParams("活动信息", "活动信息");
        param.setAddIndex(true);
        param.setType(ExcelType.XSSF);
        Workbook excel = ExcelExportUtil.exportExcel(param, OnlineActivityInfoExe.class, result);
        excel.write(os);
        os.flush();
        os.close();
    }











}
