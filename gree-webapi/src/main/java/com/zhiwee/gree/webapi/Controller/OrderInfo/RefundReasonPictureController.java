package com.zhiwee.gree.webapi.Controller.OrderInfo;

import com.zhiwee.gree.model.OrderInfo.Order;
import com.zhiwee.gree.model.OrderInfo.OrderRefundReason;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.service.OrderInfo.OrderRefundReasonService;
import com.zhiwee.gree.service.OrderInfo.OrderService;
import com.zhiwee.gree.service.OrderInfo.RefundReasonPictureService;
import com.zhiwee.gree.webapi.util.SystemControllerLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.util.KeyUtils;
import xyz.icrab.common.web.annotation.*;
import xyz.icrab.common.web.param.IdParam;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;


/**
 * @author sun on 2019/4/30
 */
@RestController
@RequestMapping("/refundReasonPicture")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class RefundReasonPictureController {


    @Autowired
    private RefundReasonPictureService refundReasonPictureService;




    /**
     * Description: 通过申请id获取申请的图片
     * @author: sun
     * @Date 上午11:08 2019/7/7
     * @param:
     * @return:
     */
    @RequestMapping({"/achieveRefundPicture"})
    public Result<?> achieveCommentPicture(@RequestBody @Valid IdParam map) {
        Map<String,Object> params = new HashMap<>();
        params.put("refundReasonId",map.getId());
        return Result.ok(refundReasonPictureService.list(params));
    }




}
