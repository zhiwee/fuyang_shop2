package com.zhiwee.gree.webapi.Controller.Shop;

import com.alipay.api.request.KoubeiRetailExtitemShopextitemQueryRequest;
import com.zhiwee.gree.model.FyGoods.Fygoods;
import com.zhiwee.gree.model.Shop.Shop;
import com.zhiwee.gree.service.Shop.ShopService;
import net.sf.jsqlparser.expression.StringValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.bind.annotation.*;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.util.IdGenerator;
import xyz.icrab.common.web.annotation.*;
import xyz.icrab.common.web.param.IdParam;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

/**
 * @author jiake
 * @date 2020-02-19 15:06
 */
@RestController
@RequestMapping("/shop")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class ShopController {


    @Autowired
    private ShopService shopService;

    /**
     * @description:
       店铺信息存入数据库
     * @return:
     * @author: jick
     * @date: 2020-02-19 13:00
     */

     @RequestMapping("/saveOrUpdate")
     public Result<?> shopSave(@RequestBody Shop shop){

         //添加操作
         if(null ==shop.getId() || ""==shop.getId()){
             shop.setId(IdGenerator.uuid());
             shop.setIsvalid(1);//1表示可用
             shopService.save(shop);
         }else{
             //更新操作
             shop.setIsvalid(1);//1表示可用
             shopService.update(shop);
         }





         return   Result.ok();

     }
     

     
     
     
     
     

     /**
      * @description:
       获取店铺列表
      * @return:
      * @author: jick
      * @date: 2020-02-19 00:26
      */
     @RequestMapping("/list")
     public Result<?> getshopList(@RequestBody(required = false) Map<String,Object> param, Pagination pagination){

         Pageable<Shop> list  = shopService.queryList(param,pagination);
         return  Result.ok(list);
     }


     /**
      * @description:
        通过id查询店铺信息
      * @return:
      * @author: jick
      * @date: 2020-02-19 54:37
      */
     @RequestMapping("/get")
     public Result<?> getInfo(@RequestBody Map<String,String> map){
         
         Shop shop =  shopService.get(map.get("id"));
         return Result.ok(shop);
         

     }


     /**
      * @description:
        删除操作
      * @return:
      * @author: jick
      * @date: 2020-02-19 35:45
      */
     @RequestMapping("/delete")
     public Result<?> deleteShop(@RequestBody Map<String,String> map){

         Shop shop = new Shop();
         shop.setId(map.get("id"));
         shopService.delete(shop);
         return Result.ok();

     }






     /**
      * @description:
        前段页面获取店铺信息
      * @return:
      * @author: jick
      * @date: 2020-02-19 03:52
      */
     @RequestMapping("/portal/list")
     @PermissionMode(PermissionMode.Mode.White)
     public Result<?> getShopList(@RequestBody(required = false) Map<String,Object> params){
         List<Shop> list  =  shopService.list(params);
         return Result.ok(list);
     }









}
