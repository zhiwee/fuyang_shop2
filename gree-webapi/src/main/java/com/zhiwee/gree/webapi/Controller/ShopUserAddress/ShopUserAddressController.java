package com.zhiwee.gree.webapi.Controller.ShopUserAddress;

import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.ShopUser.ShopUserExport;
import com.zhiwee.gree.model.ShopUserAddress.ShopUserAddress;
import com.zhiwee.gree.service.ShopUser.ShopUserService;
import com.zhiwee.gree.service.ShopUserAddreee.ShopUserAddressService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.*;

import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.util.IdGenerator;
import xyz.icrab.common.util.KeyUtils;
import xyz.icrab.common.web.annotation.*;

import xyz.icrab.common.web.util.Validator;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author sun on 2019/4/30
 */
@RestController
@RequestMapping("/shopUserAddress")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class ShopUserAddressController {

    @Autowired
    private ShopUserAddressService shopUserAddressService;

/**
 * Description: 添加地址
 * @author: sun
 * @Date 下午11:26 2019/5/9
 * @param:
 * @return:
 *
 */
    @RequestMapping(value = "/addShopUserAddress")
    public Result<?> addShopUserAddress(@RequestBody ShopUserAddress shopUserAddress,@Session("ShopUser") ShopUser shopUser){
        Validator validator = new Validator();
        validator.notEmpty(shopUserAddress.getProvince(), "省不能为空");
        validator.notEmpty(shopUserAddress.getCity(), "市不能为空");
        validator.notEmpty(shopUserAddress.getArea(), "区/县不能为空");
        validator.notEmpty(shopUserAddress.getAddress(), "详细地址不能为空");
        validator.notEmpty(shopUserAddress.getProvinceCode(), "省编码不能为空");
        validator.notEmpty(shopUserAddress.getCityCode(), "市编码不能为空");
        validator.notEmpty(shopUserAddress.getAreaCode(), "区/县编码不能为空");
        validator.notEmpty(shopUserAddress.getDeliveryPhone(), "收货号码不能为空");
        validator.notEmpty(shopUserAddress.getDeliveryUser(), "收货人不能为空");

        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }
       if(shopUserAddress.getIsDefault() == null || shopUserAddress.getIsDefault() != 1){
           shopUserAddress.setIsDefault(2);
       }else{
           shopUserAddress.setIsDefault(1);
       }
        Map<String,Object> param = new HashMap<>();
        param.put("userId",shopUser.getId());
        List<ShopUserAddress> shopUserAddresses = shopUserAddressService.list(param);
        if(shopUserAddresses != null && shopUserAddresses.size() >= 10){
            return Result.of(Status.ClientError.BAD_REQUEST, "地址最多只能添加10个");
        }
        Map<String,Object> params = new HashMap<>();
        params.put("userId",shopUser.getId());
        params.put("isDefault",1);
        ShopUserAddress defaultAddress = shopUserAddressService.getOne(params);
        shopUserAddress.setCreateTime(new Date());
        shopUserAddress.setUserId(shopUser.getId());
        shopUserAddress.setId(IdGenerator.objectId());
        shopUserAddressService.saveAndUpdate(shopUserAddress,defaultAddress);
        return Result.ok();
    }



    /**
     * Description: 更新地址
     * @author: sun
     * @Date 下午11:26 2019/5/9
     * @param:
     * @return:
     *
     */
    @RequestMapping(value = "/updateShopUserAddress")
    public Result<?> updateShopUserAddress(@RequestBody ShopUserAddress shopUserAddress,@Session("ShopUser") ShopUser shopUser){
        Validator validator = new Validator();
        validator.notEmpty(shopUserAddress.getId(), "地址Id不能为空");
        validator.notEmpty(shopUserAddress.getProvince(), "省不能为空");
        validator.notEmpty(shopUserAddress.getCity(), "市不能为空");
        validator.notEmpty(shopUserAddress.getArea(), "区/县不能为空");
        validator.notEmpty(shopUserAddress.getAddress(), "详细地址不能为空");
        validator.notEmpty(shopUserAddress.getProvinceCode(), "省编码不能为空");
        validator.notEmpty(shopUserAddress.getCityCode(), "市编码不能为空");
        validator.notEmpty(shopUserAddress.getAreaCode(), "区/县编码不能为空");
        validator.notEmpty(shopUserAddress.getDeliveryPhone(), "收货号码不能为空");
        validator.notEmpty(shopUserAddress.getDeliveryUser(), "收货人不能为空");

        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }
        if(shopUserAddress.getIsDefault() == null || shopUserAddress.getIsDefault() != 1){
            shopUserAddress.setIsDefault(2);
        }else{
            shopUserAddress.setIsDefault(1);
        }
        shopUserAddress.setUpdateTime(new Date());
        shopUserAddressService.updateAddress(shopUserAddress,shopUser);
        return Result.ok();
    }




    /**
     * Description: 更新地址
     * @author: sun
     * @Date 下午11:26 2019/5/9
     * @param:
     * @return:
     *
     */
    @RequestMapping(value = "/deleteShopUserAddress")
    public Result<?> deleteShopUserAddress(@RequestBody ShopUserAddress shopUserAddress){
        Validator validator = new Validator();
        validator.notEmpty(shopUserAddress.getId(), "地址Id不能为空");
        shopUserAddressService.delete(shopUserAddress);
        return Result.ok();
    }





    /**
     * Description: 获取地址
     * @author: sun
     * @Date 下午11:26 2019/5/9
     * @param:
     * @return:
     */
    @RequestMapping(value = "/achieveShopUserrAddress")
    public Result<?> achieveShopUserrAddress( @Session("ShopUser") ShopUser shopUser){
        Map<String,Object> param =  new HashMap<>();
        param.put("userId",shopUser.getId());
        List<ShopUserAddress> list = shopUserAddressService.list(param);
        return Result.ok(list);
    }


}
