package com.zhiwee.gree.webapi.Controller.HomePage;

import com.zhiwee.gree.model.HomePage.PageChannelGoods;
import com.zhiwee.gree.model.HomePage.PageChannelKeyword;
import com.zhiwee.gree.service.HomePage.PageChannelKeywordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.util.KeyUtils;
import xyz.icrab.common.web.param.IdParam;
import xyz.icrab.common.web.param.IdsParam;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/pageChannelKeyword")
public class PageChannelKeywordController {

    @Autowired
    private PageChannelKeywordService pageChannelKeywordService;

    /***
     * 分页查询，周广
     * @param param
     * @param pagination
     * @return
     */
    @RequestMapping("/page")
    public Result<?> page(@RequestBody Map<String, Object> param, Pagination pagination) {
        if (param == null) {
            param = new HashMap<>();
        }
        Pageable<PageChannelKeyword> pageable = pageChannelKeywordService.getPage(param, pagination);
        return Result.ok(pageable);
    }

    /**
     * 增加  周广
     * @param pageChannelKeyword
     * @return
     */
    @RequestMapping("/add")
    public Result<?> add(@RequestBody PageChannelKeyword pageChannelKeyword){
        Map<String,Object> pream=new HashMap<>();
        pream.put("channelId",pageChannelKeyword.getChannelId());
        List<PageChannelKeyword> pageChannelKeywords=pageChannelKeywordService.list(pream);
        if(pageChannelKeywords.size()==8){
            return Result.of(Status.ClientError.BAD_REQUEST, "同一板块，关键字最多支持8个");
        }
        pageChannelKeyword.setId(KeyUtils.getKey());
        pageChannelKeywordService.save(pageChannelKeyword);
        return Result.ok();
    }
    /**
     * 更新  周广
     * @param pageChannelKeyword
     * @return
     */
    @RequestMapping("/update")
    public Result<?> update(@RequestBody PageChannelKeyword pageChannelKeyword){
        pageChannelKeywordService.update(pageChannelKeyword);
        return Result.ok();
    }

    /***
     * 查询单个  周广
     * @param param
     * @return
     */
    @RequestMapping("/selectOne")
    @ResponseBody
    public Result<?> selectOne(@RequestBody IdParam param){
        return Result.ok(pageChannelKeywordService.get(param.getId()));
    }
    /***
     * 删除 周广
     * @param param
     * @return
     */
    @RequestMapping("/delete")
    @ResponseBody
    public Result<?> delete(@RequestBody @Valid IdsParam param){
        for (String id : param.getIds()) {
            PageChannelKeyword pageChannelKeyword=new PageChannelKeyword();
            pageChannelKeyword.setId(id);
            pageChannelKeywordService.delete(pageChannelKeyword);
        }
        return Result.ok();
    }
}
