package com.zhiwee.gree.webapi.Controller.GoodsInfo;


import com.zhiwee.gree.model.GoodsInfo.GoodsBand;
import com.zhiwee.gree.model.GoodsInfo.GoodsCategory;
import com.zhiwee.gree.model.GoodsInfo.GoodsCategoryImg;
import com.zhiwee.gree.model.resultBase.FileResult;
import com.zhiwee.gree.service.GoodsInfo.GoodsCategoryImgService;
import com.zhiwee.gree.service.support.FileUpload.FileUploadService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.web.param.IdParam;
import xyz.icrab.common.web.param.IdsParam;
import xyz.icrab.common.web.util.KeyUtils;
import xyz.icrab.common.web.util.Validator;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

/**
 * 商品分类  周广
 */
@ResponseBody
@RequestMapping("/goodsCategoryImg")
@RestController
public class GoodsCategoryImgController {

    @Autowired
    private GoodsCategoryImgService goodsCategoryImgService;

    @Value("${HTTP_PICTUREURL}")
    private String httpurl;

    @Autowired
    private FileUploadService fileUploadService;
    /***
     * 分页查询，周广
     * @param param
     * @param pagination
     * @return
     */
    @RequestMapping("/parentPage")
    public Result<?> parentPage(@RequestBody Map<String, Object> param, Pagination pagination) {
        if (param == null) {
            param = new HashMap<>();
        }
        Pageable<GoodsCategoryImg> pageable = goodsCategoryImgService.getPage(param, pagination);
        return Result.ok(pageable);
    }

    /**
     * Description: 图标 图片上传，周广
     * @param:
     * @return:
     */
    @PostMapping("/uploadBackPicture")
    public Result<?> uploadBackPicture(@RequestPart("file") MultipartFile file, HttpServletRequest req) throws Exception {
        FileResult result= fileUploadService.fileUpload(file.getOriginalFilename(),file.getInputStream());
        return Result.ok(result.getFileUrl());
    }

    /**
     * 添加  周广
     */
    @RequestMapping("/add")
    public Result<?> add(@RequestBody GoodsCategoryImg goodsCategoryImg) {
        Validator validator = new Validator();
        validator.notEmpty(goodsCategoryImg.getCategoryId(), "请选择分类");
        validator.notEmpty(goodsCategoryImg.getImgTopUrl(), "请上传点击之前的图片");
        validator.notEmpty(goodsCategoryImg.getImgBomUrl(), "请上传点击之后的图片");
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }
        GoodsCategoryImg goodsCategoryImg1=goodsCategoryImgService.getOneByCategoryId(goodsCategoryImg.getCategoryId());
        if(goodsCategoryImg1==null){
            goodsCategoryImg.setId(KeyUtils.getKey());
            goodsCategoryImgService.save(goodsCategoryImg);
            return Result.ok();
        }else{
            return Result.of(Status.ClientError.BAD_REQUEST,"一个分类，只能添加一个图标");
        }

    }
    /**
     * 添加  周广
     */
    @RequestMapping("/update")
    public Result<?> update(@RequestBody GoodsCategoryImg goodsCategoryImg) {
        Validator validator = new Validator();
        validator.notEmpty(goodsCategoryImg.getId(), "请合理选择修改的分类图标");
        validator.notEmpty(goodsCategoryImg.getCategoryId(), "请选择修改的，分类");
        validator.notEmpty(goodsCategoryImg.getImgTopUrl(), "请上传修改的，点击之前的图片");
        validator.notEmpty(goodsCategoryImg.getImgBomUrl(), "请上传修改的，点击之后的图片");
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }
        goodsCategoryImgService.update(goodsCategoryImg);
        return Result.ok();
    }

    /**
     * 添加  周广
     */
    @RequestMapping("/selectOne")
    @ResponseBody
    public Result<?> selectOne(@RequestBody Map<String, Object> param) {
        if (param == null) {
            param = new HashMap<>();
        }
        if(StringUtils.isBlank((String) param.get("id"))){
            return Result.of(Status.ClientError.BAD_REQUEST, "请合理选择数据！");
        }
        GoodsCategoryImg goodsCategoryImg=goodsCategoryImgService.selectById((String) param.get("id"));
        return Result.ok(goodsCategoryImg);
    }
    /***
     * 删除  周广
     * @param param
     * @return
     */
    @RequestMapping("/delete")
    @ResponseBody
    public Result<?> delete(@RequestBody @Valid IdsParam param){
        for (String id : param.getIds()) {
            GoodsCategoryImg goodsCategoryImg=new GoodsCategoryImg();
            goodsCategoryImg.setId(id);
            goodsCategoryImgService.delete(goodsCategoryImg);
        }
        return Result.ok();
    }
}
