package com.zhiwee.gree.webapi.Controller.GoodsInfo;


import com.zhiwee.gree.model.GoodsInfo.GoodsCategoryAdImg;
import com.zhiwee.gree.model.GoodsInfo.GoodsCategoryImg;
import com.zhiwee.gree.model.resultBase.FileResult;
import com.zhiwee.gree.service.GoodsInfo.GoodsCategoryAdImgService;
import com.zhiwee.gree.service.support.FileUpload.FileUploadService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.web.param.IdsParam;
import xyz.icrab.common.web.util.KeyUtils;
import xyz.icrab.common.web.util.Validator;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

/**
 * 商品分类  周广
 */
@ResponseBody
@RequestMapping("/goodsCategoryAdImg")
@RestController
public class GoodsCategoryAdImgController {

    @Autowired
    private GoodsCategoryAdImgService goodsCategoryAdImgService;


    @Value("${HTTP_PICTUREURL}")
    private String httpurl;

    @Autowired
    private FileUploadService fileUploadService;
    /***
     * 分页查询，周广
     * @param param
     * @param pagination
     * @return
     */
    @RequestMapping("/parentPage")
    public Result<?> parentPage(@RequestBody Map<String, Object> param, Pagination pagination) {
        if (param == null) {
            param = new HashMap<>();
        }
        Pageable<GoodsCategoryAdImg> pageable = goodsCategoryAdImgService.getPage(param, pagination);
        return Result.ok(pageable);
    }

    /**
     * Description: 广告 图片上传，周广
     * @param:
     * @return:
     */
    @PostMapping("/uploadBackPicture")
    public Result<?> uploadBackPicture(@RequestPart("file") MultipartFile file, HttpServletRequest req) throws Exception {
        FileResult result= fileUploadService.fileUpload(file.getOriginalFilename(),file.getInputStream());
        return Result.ok(result.getFileUrl());
    }

    /**
     * 广告 添加  周广
     */
    @RequestMapping("/add")
    public Result<?> add(@RequestBody GoodsCategoryAdImg goodsCategoryAdImg) {
        if (StringUtils.isBlank(goodsCategoryAdImg.getCategoryId())) {
            return Result.of(Status.ClientError.BAD_REQUEST,"请选择分类");
        }
        if (StringUtils.isBlank(goodsCategoryAdImg.getUrlId())) {
            return Result.of(Status.ClientError.BAD_REQUEST,"请选择指向链接");
        }
        if (StringUtils.isBlank(goodsCategoryAdImg.getImgUrl())) {
            return Result.of(Status.ClientError.BAD_REQUEST,"请上传广告图");
        }
        goodsCategoryAdImg.setId(KeyUtils.getKey());
        goodsCategoryAdImgService.save(goodsCategoryAdImg);
        return Result.ok();
    }
    /**
     * 广告 更新  周广
     */
    @RequestMapping("/update")
    public Result<?> update(@RequestBody GoodsCategoryAdImg goodsCategoryAdImg) {
        if (StringUtils.isBlank(goodsCategoryAdImg.getCategoryId())) {
            return Result.of(Status.ClientError.BAD_REQUEST,"请选择修改分类");
        }
        if (StringUtils.isBlank(goodsCategoryAdImg.getUrlId())) {
            return Result.of(Status.ClientError.BAD_REQUEST,"请选择修改指向链接");
        }
        if (StringUtils.isBlank(goodsCategoryAdImg.getImgUrl())) {
            return Result.of(Status.ClientError.BAD_REQUEST,"请上传修改广告图");
        }
        goodsCategoryAdImgService.update(goodsCategoryAdImg);
        return Result.ok();
    }

    /**
     * 添加  周广
     */
    @RequestMapping("/selectOne")
    @ResponseBody
    public Result<?> selectOne(@RequestBody Map<String, Object> param) {
        if (param == null) {
            param = new HashMap<>();
        }
        if(StringUtils.isBlank((String) param.get("id"))){
            return Result.of(Status.ClientError.BAD_REQUEST, "请合理选择数据！");
        }
        GoodsCategoryAdImg goodsCategoryAdImg=goodsCategoryAdImgService.selectById((String) param.get("id"));
        return Result.ok(goodsCategoryAdImg);
    }
    /***
     * 删除  周广
     * @param param
     * @return
     */
    @RequestMapping("/delete")
    @ResponseBody
    public Result<?> delete(@RequestBody @Valid IdsParam param){
        for (String id : param.getIds()) {
            GoodsCategoryAdImg goodsCategoryAdImg=new GoodsCategoryAdImg();
            goodsCategoryAdImg.setId(id);
            goodsCategoryAdImgService.delete(goodsCategoryAdImg);
        }
        return Result.ok();
    }
}
