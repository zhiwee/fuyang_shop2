package com.zhiwee.gree.webapi.Controller.GoodsInfo;

import com.zhiwee.gree.model.GoodsInfo.GoodVo.GategoryVo;
import com.zhiwee.gree.model.GoodsInfo.GoodVo.GoodsCategoryCY;
import com.zhiwee.gree.model.GoodsInfo.GoodsCategory;
import com.zhiwee.gree.model.HomePage.PageV0.PageChannelGoodsVO;
import com.zhiwee.gree.service.GoodsInfo.GoodsCategoryAdImgService;
import com.zhiwee.gree.service.GoodsInfo.GoodsCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.*;
import xyz.icrab.common.util.KeyUtils;
import xyz.icrab.common.web.annotation.PermissionMode;
import xyz.icrab.common.web.param.IdParam;
import xyz.icrab.common.web.param.IdsParam;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.*;

/**
 * 商品分类  周广
 */
@ResponseBody
    @RequestMapping("/goodsCategory")
@RestController
public class GoodsCategoryController {

    @Autowired
    private GoodsCategoryService goodsCategoryService;

    @Autowired
    private GoodsCategoryAdImgService goodsCategoryAdImgService;

    @PermissionMode(PermissionMode.Mode.White)
    @RequestMapping("/getList")
    @ResponseBody
    public Result<?> getList() {
        Map<String, Object> params = new HashMap<>();
        params.put("state", 1);
        params.put("isLook", 1);
        //全部分类
        List<GategoryVo> gategoryVoList = goodsCategoryService.getVoList(params);

        List<GategoryVo> oneCategoryList = new ArrayList<>();


        List<GategoryVo> twoCategoryList = new ArrayList<>();

        List<GategoryVo> sreeCategoryList = new ArrayList<>();

        for (GategoryVo gategoryVo : gategoryVoList) {
            if (gategoryVo.getType() == 1) {
                oneCategoryList.add(gategoryVo);
            } else if (gategoryVo.getType() == 2) {
                twoCategoryList.add(gategoryVo);
            } else if (gategoryVo.getType() == 3) {
                sreeCategoryList.add(gategoryVo);
            }
        }
        for (GategoryVo twogategoryVo : twoCategoryList) {
            for (GategoryVo sreegategoryVo : sreeCategoryList) {

                if (twogategoryVo.getId().equals(sreegategoryVo.getParentsId())) {
                    if (twogategoryVo.getChildren() == null || twogategoryVo.getChildren().size() == 0) {
                        List<GategoryVo> nullsreeList = new ArrayList<>();
                        nullsreeList.add(sreegategoryVo);
                        twogategoryVo.setChildren(nullsreeList);
                    } else {
                        twogategoryVo.getChildren().add(sreegategoryVo);
                    }
                }
            }
        }
        for (GategoryVo onegategoryVo : oneCategoryList) {
            for (GategoryVo twogategoryVo : twoCategoryList) {
                if (onegategoryVo.getId().equals(twogategoryVo.getParentsId())) {
                    if (onegategoryVo.getChildren() == null || onegategoryVo.getChildren().size() == 0) {
                        List<GategoryVo> nulltwoList = new ArrayList<>();
                        nulltwoList.add(twogategoryVo);
                        onegategoryVo.setChildren(nulltwoList);
                    } else {
                        onegategoryVo.getChildren().add(twogategoryVo);
                    }
                }

            }
        }
        return Result.ok(oneCategoryList);
    }

    /**
     * 根据分类获取商品，分页，前台使用
     */
    @RequestMapping("/getCategoryGoods")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> getCategoryGoods(@RequestBody Map<String, Object> params, HttpServletRequest req, HttpServletResponse res) {
        if (params == null) {
            params = new HashMap<>();
        }
        if (params.get("categoryId") == null) {
            return Result.of(Status.ClientError.BAD_REQUEST, "系统未检测到该分类");
        }
        List<PageChannelGoodsVO> pageChannelGoodsVOS = goodsCategoryService.getCategoryGoods(params);
        return Result.ok(pageChannelGoodsVOS);
    }

    /***
     * 分页查询，周广
     * @param param
     * @param pagination
     */
    @RequestMapping("/page")
    public Result<?> page(@RequestBody Map<String, Object> param, Pagination pagination) {
        if (param == null) {
            param = new HashMap<>();
        }
        Pageable<GoodsCategory> pageable = goodsCategoryService.getPage(param, pagination);
        return Result.ok(pageable);
    }

    /***
     * 分页查询一级，周广
     * @param param
     * @param pagination
     * @return
     */
    @RequestMapping("/parentPage")
    public Result<?> parentPage(@RequestBody Map<String, Object> param, Pagination pagination) {
        if (param == null) {
            param = new HashMap<>();
        }
        param.put("type", 1);
        Pageable<GoodsCategory> pageable = goodsCategoryService.getPage(param, pagination);
        return Result.ok(pageable);
    }


    /**
     * 分类增加  周广
     *
     * @param goodsCategory
     * @return
     */
    @RequestMapping("/add")
    public Result<?> add(@RequestBody GoodsCategory goodsCategory) {
        goodsCategory.setId(KeyUtils.getKey());
        if (goodsCategory.getParentId() == null || goodsCategory.getParentId().equals("")) {
            goodsCategory.setType(1);
        } else {
            GoodsCategory goodsCategory1 = goodsCategoryService.get(goodsCategory.getParentId());
            goodsCategory.setType(goodsCategory1.getType().intValue() + 1);
        }
        goodsCategoryService.save(goodsCategory);
        return Result.ok();
    }

    /**
     * 分类更新  周广
     *
     * @param goodsCategory
     * @return
     */
    @RequestMapping("/update")
    public Result<?> update(@RequestBody GoodsCategory goodsCategory) {
        if (goodsCategory.getParentId() == null || goodsCategory.getParentId().equals("")) {
            goodsCategory.setType(1);
        } else {
            GoodsCategory goodsCategory1 = goodsCategoryService.get(goodsCategory.getParentId());
            goodsCategory.setType(goodsCategory1.getType().intValue() + 1);
        }
        goodsCategoryService.update(goodsCategory);
        return Result.ok();
    }

    /***
     * 查询单个  周广
     * @param param
     * @return
     */
    @RequestMapping("/selectOne")
    @ResponseBody
    public Result<?> selectOne(@RequestBody IdParam param) {
        GoodsCategory goodsCategory = goodsCategoryService.selectByid(param.getId());
        return Result.ok(goodsCategory);
    }

    /***
     * 启用  周广
     * @param param
     * @return
     */
    @RequestMapping("/enable")
    @ResponseBody
    public Result<?> enable(@RequestBody @Valid IdsParam param) {
        for (String id : param.getIds()) {
            GoodsCategory goodsCategory = new GoodsCategory();
            goodsCategory.setId(id);
            goodsCategory.setState(1);
            goodsCategoryService.update(goodsCategory);
        }
        return Result.ok();
    }

    /***
     * 禁用  周广
     * @param param
     * @return
     */
    @RequestMapping("/disable")
    @ResponseBody
    public Result<?> disable(@RequestBody @Valid IdsParam param) {
        for (String id : param.getIds()) {
            GoodsCategory goodsCategory = new GoodsCategory();
            goodsCategory.setId(id);
            goodsCategory.setState(2);
            goodsCategoryService.update(goodsCategory);
        }
        return Result.ok();
    }

    /***
     * 显示  周广
     * @param param
     * @return
     */
    @RequestMapping("/lookYes")
    @ResponseBody
    public Result<?> lookYes(@RequestBody @Valid IdsParam param) {
        for (String id : param.getIds()) {
            GoodsCategory goodsCategory = new GoodsCategory();
            goodsCategory.setId(id);
            goodsCategory.setIsLook(1);
            goodsCategoryService.update(goodsCategory);
        }
        return Result.ok();
    }

    /***
     * 不显示  周广
     * @param param
     * @return
     */
    @RequestMapping("/lookNo")
    @ResponseBody
    public Result<?> lookNo(@RequestBody @Valid IdsParam param) {
        for (String id : param.getIds()) {
            GoodsCategory goodsCategory = new GoodsCategory();
            goodsCategory.setId(id);
            goodsCategory.setIsLook(2);
            goodsCategoryService.update(goodsCategory);
        }
        return Result.ok();
    }

    /***
     * 安装  周广
     * @param param
     * @return
     */
    @RequestMapping("/afterYes")
    @ResponseBody
    public Result<?> afterYes(@RequestBody @Valid IdsParam param) {
        for (String id : param.getIds()) {
            GoodsCategory goodsCategory = new GoodsCategory();
            goodsCategory.setId(id);
            goodsCategory.setIsAfter(1);
            goodsCategoryService.update(goodsCategory);
        }
        return Result.ok();
    }

    /***
     * 不安装  周广
     * @param param
     * @return
     */
    @RequestMapping("/afterNo")
    @ResponseBody
    public Result<?> afterNo(@RequestBody @Valid IdsParam param) {
        for (String id : param.getIds()) {
            GoodsCategory goodsCategory = new GoodsCategory();
            goodsCategory.setId(id);
            goodsCategory.setIsAfter(2);
            goodsCategoryService.update(goodsCategory);
        }
        return Result.ok();
    }


    /***
     * 删除单个  周广
     * @param param
     * @return
     */
    @RequestMapping("/delete")
    @ResponseBody
    public Result<?> delete(@RequestBody @Valid IdsParam param) {
        for (String id : param.getIds()) {
            GoodsCategory goodsCategory = new GoodsCategory();
            goodsCategory.setId(id);
            goodsCategoryService.delete(goodsCategory);
        }
        return Result.ok();
    }


    @RequestMapping("/addCategory")
    @PermissionMode(PermissionMode.Mode.White)
    @ResponseBody
    public Result<?> addCategory(@RequestBody Map<String, Object> param, HttpServletRequest req, HttpServletResponse res) throws Exception {
        if ((String) param.get("code") == null || "".equals((String) param.get("code"))) {
            return Result.of(Status.ClientError.BAD_REQUEST, "编码为空");
        }
        if ((String) param.get("categoryName") == null || "".equals((String) param.get("categoryName"))) {
            return Result.of(Status.ClientError.BAD_REQUEST, "名称为空");
        }
        if ((String) param.get("parentCode") == null || "".equals((String) param.get("parentCode"))) {
            return Result.of(Status.ClientError.BAD_REQUEST, "父类code为空");
        }
        String code = (String) param.get("code");
        String parentCode = (String) param.get("parentCode");

        GoodsCategory goodsCategory = new GoodsCategory();
        goodsCategory.setCode(code);
        //查看编码是否存在
        Boolean canUse = checkCode(goodsCategory);
        if (!canUse) {
            return Result.of(Status.ClientError.BAD_REQUEST, "编码重复");
        }
        //查看父类是否存在
        Boolean checkF = checkFather(parentCode);
        if (checkF) {
            return Result.of(Status.ClientError.BAD_REQUEST, "父类code未找到对应的分类");
        }
        GoodsCategory goodsCategory1 = new GoodsCategory();
        goodsCategory1.setCode(parentCode);
        goodsCategory1 = goodsCategoryService.getOne(goodsCategory1);

        String name = (String) param.get("categoryName");

        String id = KeyUtils.getKey();
        goodsCategory.setId(id);
        goodsCategory.setParentName(goodsCategory1.getName());
        goodsCategory.setRank(goodsCategory1.getRank() + 1);
        goodsCategory.setParentId(goodsCategory1.getId());
        goodsCategory.setName(name);
        goodsCategory.setState(1);
        goodsCategoryService.save(goodsCategory);
        return Result.ok("成功");
    }

    private Boolean checkFather(String parentCode) {
        GoodsCategory category = new GoodsCategory();
        category.setCode(parentCode);
        category = goodsCategoryService.getOne(category);
        return category == null;
    }

    private Boolean checkCode(GoodsCategory goodsCategory) {
        GoodsCategory one = goodsCategoryService.getOne(goodsCategory);
        return one == null;
    }

    @RequestMapping("/updateCategory")
    @PermissionMode(PermissionMode.Mode.White)
    @ResponseBody
    public Result<?> updateCategory(@RequestBody Map<String, Object> param, HttpServletRequest req, HttpServletResponse res) throws Exception {
        String newCode = null;
        String parentCode = null;
        if ((String) param.get("code") == null || "".equals((String) param.get("code"))) {
            return Result.of(Status.ClientError.BAD_REQUEST, "编码为空");
        }
        if ((String) param.get("categoryName") == null || "".equals((String) param.get("categoryName"))) {
            return Result.of(Status.ClientError.BAD_REQUEST, "名称为空");
        }

        if ((String) param.get("parentCode") != null || !"".equals((String) param.get("parentCode"))) {
            parentCode = (String) param.get("parentCode");
        }
        if ((String) param.get("newCode") != null || !"".equals((String) param.get("newCode"))) {
            newCode = (String) param.get("newCode");
        }
        GoodsCategory goodsCategory = new GoodsCategory();
        String code = (String) param.get("code");
        //查看父类是否存在
        if (parentCode != null) {
            Boolean checkF = checkFather(parentCode);
            if (checkF) {
                return Result.of(Status.ClientError.BAD_REQUEST, "父类编码未找到对应的分类");
            }
            goodsCategory.setCode(parentCode);
            goodsCategory = goodsCategoryService.getOne(goodsCategory);
            if (goodsCategory.getCode().equals(code) || goodsCategory.getCode().equals(newCode)) {
                return Result.of(Status.ClientError.BAD_REQUEST, "父类编码不能自身编码相同");
            }


        }


        String name = (String) param.get("categoryName");
        Map<String, Object> param2 = new HashMap<>();
        param2.put("code", code);
        List<GoodsCategory> list = goodsCategoryService.list(param2);
        if (list.size() > 1) {
            return Result.of(Status.ClientError.BAD_REQUEST, "编码重复，无法更新");
        }
        if (list.isEmpty()) {
            return Result.of(Status.ClientError.BAD_REQUEST, "未找到该编码对应的分类");
        }


        if (newCode != null) {
            list.get(0).setCode(newCode);
        }

        if (parentCode != null) {
            list.get(0).setParentId(goodsCategory.getId());
        }
        list.get(0).setName(name);
        goodsCategoryService.update(list.get(0));
        return Result.ok("成功");

    }


    @RequestMapping("/deleteCategory")
    @PermissionMode(PermissionMode.Mode.White)
    @ResponseBody
    public Result<?> deleteCategory(@RequestBody Map<String, Object> param, HttpServletRequest req, HttpServletResponse res) throws Exception {
        if ((String) param.get("code") != null && !"".equals((String) param.get("code"))) {
            String code = (String) param.get("code");
            Map<String, Object> param2 = new HashMap<>();
            param2.put("code", code);
            List<GoodsCategory> list = goodsCategoryService.list(param2);
            if (list.isEmpty()) {
                return Result.of(Status.ClientError.BAD_REQUEST, "未找到该编码对应的分类");
            }
            GoodsCategory goodsCategory = new GoodsCategory();
            goodsCategory.setCode(code);
            goodsCategoryService.delete(goodsCategory);
            return Result.ok("成功");
        } else {
            return Result.of(Status.ClientError.BAD_REQUEST, "编码为空,删除失败");
        }
    }

    /**
     * @return
     * @author tzj
     * @description 长益查看所有分类
     * @date 2019/12/12 14:10
     */
    @RequestMapping("/getAll")
    @PermissionMode(PermissionMode.Mode.White)
    @ResponseBody
    public Result<?> getAll(@RequestBody Map<String, Object> param, HttpServletRequest req, HttpServletResponse res) throws Exception {
        List<GoodsCategoryCY> goodsCategories = goodsCategoryService.listInfo();
        return Result.ok(goodsCategories);
    }


    /**
     * @return
     * @author tzj
     * @description 树形结构
     * @date 2019/12/12 12:46
     */
    @RequestMapping("/tree")
    public Result<?> tree(@RequestBody(required = false) Map<String, Object> params) {
        if (params == null) {
            params = new HashMap<>();
        }
        List<GoodsCategory> list = goodsCategoryService.list(params);
        List<Tree<GoodsCategory>> trees = new ArrayList<>(list.size());
        for (GoodsCategory goodsCategory : list) {
            Tree<GoodsCategory> tree = new Tree<>(goodsCategory.getId(), goodsCategory.getName(), goodsCategory.getParentId(), goodsCategory);
            trees.add(tree);
        }
        return Result.ok(trees);
    }


    /**
     * @author tzj
     * @description 小程序获取分类接口
     * @date 2019/12/27 15:22
     */
    @RequestMapping("/listAll")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> listAll(@RequestBody(required = false) Map<String, Object> params) {
        if (params == null) {
            params = new HashMap<>();
        }
//        else {
        params.put("rank", 1);
        List<GoodsCategory> lists;
        lists = goodsCategoryService.list(params);

        Map<String, Object> param = new HashMap<>(5);
        for (GoodsCategory next : lists) {
            param.put("id", next.getId());
            List<GoodsCategory> goodsCategories = goodsCategoryService.listChildren(param);
            for (GoodsCategory goodsCategory : goodsCategories) {
                Map<String, Object> param2 = new HashMap<>();
                param2.put("id", goodsCategory.getId());
                List<GoodsCategory> goodsCategories2 = goodsCategoryService.listChildren(param2);
                goodsCategory.setList(goodsCategories2);
            }
            next.setList(goodsCategories);
        }
        return Result.ok(lists);
    }



}