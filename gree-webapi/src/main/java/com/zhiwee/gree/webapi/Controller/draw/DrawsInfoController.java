package com.zhiwee.gree.webapi.Controller.draw;

import com.zhiwee.gree.model.User;
import com.zhiwee.gree.model.draw.DrawInfo;
import com.zhiwee.gree.model.draw.DrawsInfo;
import com.zhiwee.gree.service.draw.DrawInfoService;
import com.zhiwee.gree.service.draw.DrawsInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.util.IdGenerator;
import xyz.icrab.common.web.annotation.EnableGetRequest;
import xyz.icrab.common.web.annotation.EnableListRequest;
import xyz.icrab.common.web.annotation.Session;
import xyz.icrab.ums.util.consts.SessionKeys;

import java.text.DecimalFormat;
import java.util.*;


@RequestMapping("/drawsInfo")
@Controller
@ResponseBody
@EnableListRequest
@EnableGetRequest
public class DrawsInfoController {
    @Autowired
    private DrawsInfoService drawsInfoService;
    @Autowired
    private DrawInfoService drawInfoService;

    /**
     * @return
     * @author tzj
     * @description 新增奖品
     * @date 2019/12/8 20:30
     */
    @RequestMapping(value = "/add")
    @SuppressWarnings("all")
    public Result<?> deptInfoAdd(@Session(SessionKeys.USER) User user, @RequestBody(required = false) DrawsInfo drawsInfo) {
            if (drawsInfo.getDrawId()==null|| "".equals(drawsInfo.getDrawId())){
                return Result.of(Status.ClientError.BAD_REQUEST, "请选择相应奖池");
            }
        Boolean checkAllNum = checkAllNum(drawsInfo);
        if (!checkAllNum) {
            return Result.of(Status.ClientError.BAD_REQUEST, "与已有奖项冲突或总数大于奖池可抽次数!");
        }
        Boolean checkUseNum = checkUseNum(drawsInfo);
        if (!checkUseNum) {
            return Result.of(Status.ClientError.BAD_REQUEST, "已中数量必须小于可中数量!");
        }
//        if (drawsInfo.getState()==2){
//            Boolean CheckState = checkState(drawsInfo.getDrawId());
//            if (!CheckState){
//                return Result.of(Status.ClientError.BAD_REQUEST, "只能有一个不限量的奖项!");
//            }
//        }
        DrawInfo drawInfo=new DrawInfo();
        drawInfo.setId(drawsInfo.getDrawId());
        drawInfo=drawInfoService.getOne(drawInfo);
        double chance= drawsInfo.getDrawSum()*1.0/drawInfo.getAllSum();


        drawsInfo.setId(IdGenerator.objectId());
        drawsInfo.setCreateman(user.getId());
        drawsInfo.setCreatetime(new Date());
        drawsInfo.setChance(chance);
        drawsInfoService.save(drawsInfo);
        return Result.ok();
    }

    /**
     * @return
     * @author tzj
     * @description 奖品更新
     * @date 2019/12/8 19:32
     */
        @RequestMapping(value = "/update")
    @SuppressWarnings("all")
    public Result<?> update(@Session(SessionKeys.USER) User user, @RequestBody(required = false) DrawsInfo drawsInfo) {
        if (drawsInfo.getDrawId()==null|| "".equals(drawsInfo.getDrawId())){
            return Result.of(Status.ClientError.BAD_REQUEST, "请选择相应奖池");
        }
        Boolean checkAllNum = checkAllNum2(drawsInfo);
        if (!checkAllNum) {
            return Result.of(Status.ClientError.BAD_REQUEST, "与已有奖项冲突或总数大于奖池可抽次数!");
        }
        Boolean checkUseNum = checkUseNum(drawsInfo);
        if (!checkUseNum) {
            return Result.of(Status.ClientError.BAD_REQUEST, "已中数量必须小于可中数量!");
        }
//        if (drawsInfo.getState()==2){
//            Boolean CheckState = checkState2(drawsInfo);
//            if (CheckState){
//                return Result.of(Status.ClientError.BAD_REQUEST, "只能有一个不限量的奖项!");
//            }
//        }
            DrawInfo drawInfo=new DrawInfo();
            drawInfo.setId(drawsInfo.getDrawId());
            drawInfo=drawInfoService.getOne(drawInfo);
            double chance= drawsInfo.getDrawSum()*1.0/drawInfo.getAllSum();

            drawsInfo.setChance(chance);
            drawsInfo.setUpdateman(user.getId());
            drawsInfo.setUpdatetime(new Date());
            drawsInfoService.update(drawsInfo);
            return Result.ok();
    }

    /**
     * @return
     * @author tzj
     * @description 奖品页面
     * @date 2019/12/8 19:32
     */
    @RequestMapping(value = "/infoPage")
    @SuppressWarnings("all")
    public Result<?> infoPage(@RequestBody Map<String, Object> params, Pagination pagination) {
        if (params == null) {
            params = new HashMap<>();
        }
        Pageable<DrawsInfo> page = drawsInfoService.pageInfo(params, pagination);
        return Result.ok(page);
    }



    /**
     * @author tzj
     * @description 奖品删除
     * @date 2019/12/9 15:54
     * @return
    */

    @RequestMapping("/delete")
    @ResponseBody
    @SuppressWarnings("all")
    public Result<?> delete(@RequestBody DrawsInfo drawsInfo) {
        drawsInfoService.delete(drawsInfo);
        return Result.ok();
    }


        //奖品的可抽次数总数小于奖池的可抽总数
        @SuppressWarnings("all")
    private Boolean checkAllNum(DrawsInfo drawsInfo) {
        //找到奖池
        DrawInfo drawInfo=new DrawInfo();
        drawInfo.setId(drawsInfo.getDrawId());
        drawInfo= drawInfoService.getOne(drawInfo);

        //找到所有奖项
        Map<String, Object> param = new HashMap<>();
        param.put("drawId", drawsInfo.getDrawId());
        List<DrawsInfo> list = drawsInfoService.checkAllNum(param);

        //接收所有奖项奏疏
        int currentSum=0;
        for (DrawsInfo info : list) {
            if(info.getPrize().equals(drawsInfo.getPrize())){
                return false;
            }
            currentSum=currentSum+info.getDrawSum();
        }
        return (currentSum+drawsInfo.getDrawSum()) <= (drawInfo.getAllSum()-drawInfo.getUseSum());
    }

    /**
     * @author tzj
     * @description 奖品的可抽次数总数小于奖池的可抽总数
     * @date 2019/12/14 0:36
    */
    private Boolean checkAllNum2(DrawsInfo drawsInfo) {
        //找到奖池
        DrawInfo drawInfo=new DrawInfo();
        drawInfo.setId(drawsInfo.getDrawId());
        drawInfo= drawInfoService.getOne(drawInfo);

        //找到所有奖项
        Map<String, Object> param = new HashMap<>(10);
        param.put("drawId", drawsInfo.getDrawId());
        List<DrawsInfo> list = drawsInfoService.checkAllNum(param);

        //接收所有奖项奏疏
        int currentSum=0;
        for (DrawsInfo info : list) {
            if (!drawsInfo.getId().equals(info.getId())) {
                currentSum = currentSum + info.getDrawSum();
            }
        }
        return (currentSum+drawsInfo.getDrawSum()) <= (drawInfo.getAllSum()-drawInfo.getUseSum());
    }


    /**
     * @author tzj
     * @description 奖品的已中数量小于奖品的可中数量
     * @date 2019/12/14 0:36
    */
    private Boolean checkUseNum(DrawsInfo drawsInfo) {
        return drawsInfo.getUseDrawSum()<=drawsInfo.getDrawSum();
    }

    /*
      @author tzj
     * @description 不想奖品只能有一个\
     * @date 2019/12/14 0:36
    */
   /* private Boolean checkState(String drawId) {
        Map<String,Object> param=new HashMap<>();
        param.put("drawId",drawId);
        List<DrawsInfo> list = drawsInfoService.list(param);
        for (DrawsInfo drawsInfo : list) {
            if (drawsInfo.getState()==2){
                return false;
            }
        }
        return true;
    }*/

  /*  private Boolean checkState2(DrawsInfo drawsInfo) {
        Map<String,Object> param=new HashMap<>();
        param.put("drawId",drawsInfo.getDrawId());
        List<DrawsInfo> list = drawsInfoService.list(param);
        for (DrawsInfo drawsInfo1 : list) {
            if (drawsInfo1.getState()==2){
                return drawsInfo1.getId().equals(drawsInfo.getId());
            }
        }
        return false;
    }*/

}
