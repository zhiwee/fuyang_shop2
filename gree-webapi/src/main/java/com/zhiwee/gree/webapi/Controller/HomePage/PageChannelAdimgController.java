package com.zhiwee.gree.webapi.Controller.HomePage;


import com.zhiwee.gree.model.HomePage.PageChannelAdimg;
import com.zhiwee.gree.model.HomePage.PageChannelKeyword;
import com.zhiwee.gree.service.HomePage.PageChannelAdimgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.util.KeyUtils;
import xyz.icrab.common.web.param.IdParam;
import xyz.icrab.common.web.param.IdsParam;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/pageChannelAdimg")
public class PageChannelAdimgController {

    @Autowired
    private PageChannelAdimgService pageChannelAdimgService;

    /***
     * 分页查询，周广
     * @param param
     * @param pagination
     * @return
     */
    @RequestMapping("/page")
    public Result<?> page(@RequestBody Map<String, Object> param, Pagination pagination) {
        if (param == null) {
            param = new HashMap<>();
        }
        Pageable<PageChannelAdimg> pageable = pageChannelAdimgService.getPage(param, pagination);
        return Result.ok(pageable);
    }

    /**
     * 增加  周广
     * @param pageChannelAdimg
     * @return
     */
    @RequestMapping("/add")
    public Result<?> add(@RequestBody PageChannelAdimg pageChannelAdimg){
        Map<String,Object> pream=new HashMap<>();
        pream.put("channelId",pageChannelAdimg.getChannelId());
        pream.put("type",pageChannelAdimg.getType());
        List<PageChannelAdimg>  pageChannelAdimgs=pageChannelAdimgService.list(pream);
        if(pageChannelAdimgs.size()!=0){
            if(pageChannelAdimg.getType()==1){
                return Result.of(Status.ClientError.BAD_REQUEST, "该频道左侧广告图已存在");
            }else{
                if(pageChannelAdimgs.size()==2){
                    return Result.of(Status.ClientError.BAD_REQUEST, "该频道右侧两张广告图已存在");
                }
            }
        }
        pageChannelAdimg.setId(KeyUtils.getKey());
        pageChannelAdimgService.save(pageChannelAdimg);
        return Result.ok();

    }
    /**
     * 更新  周广
     * @param pageChannelAdimg
     * @return
     */
    @RequestMapping("/update")
    public Result<?> update(@RequestBody PageChannelAdimg pageChannelAdimg){
        pageChannelAdimgService.update(pageChannelAdimg);
        return Result.ok();
    }

    /***
     * 查询单个  周广
     * @param param
     * @return
     */
    @RequestMapping("/selectOne")
    @ResponseBody
    public Result<?> selectOne(@RequestBody IdParam param){
        return Result.ok(pageChannelAdimgService.get(param.getId()));
    }
    /***
     * 删除 周广
     * @param param
     * @return
     */
    @RequestMapping("/delete")
    @ResponseBody
    public Result<?> delete(@RequestBody @Valid IdsParam param){
        for (String id : param.getIds()) {
            PageChannelAdimg pageChannelAdimg=new PageChannelAdimg();
            pageChannelAdimg.setId(id);
            pageChannelAdimgService.delete(pageChannelAdimg);
        }
        return Result.ok();
    }

}
