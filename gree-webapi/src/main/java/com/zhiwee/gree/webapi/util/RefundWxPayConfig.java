package com.zhiwee.gree.webapi.util;

import com.github.wxpay.sdk.WXPayConfig;
import com.zhiwee.gree.model.WxPay.WeChatConfig;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class RefundWxPayConfig implements WXPayConfig {

    @Override
    public String getAppID() {
        return WeChatConfig.APPIDFuYang;
    }

    @Override
    public String getMchID() {
        return  WeChatConfig.MCHIDFuYang;
    }

    @Override
    public String getKey() {
        return  WeChatConfig.APIKEYFuYang;
    }

    @Override
    public InputStream getCertStream() {
        try {
            InputStream inputStream = new FileInputStream (new File("/home/cert/WXCertUtil/cert/1232511302_20200103_cert/apiclient_cert.p12"));
//            InputStream inputStream = new FileInputStream (new File("F:/1232511302_20200103_cert/apiclient_cert.p12"));

            // InputStream inputStream = new FileInputStream
            //        (new
            //               File("/Users/jiake/Desktop/WXCertUtil/cert/1232511302_20200103_cert/apiclient_cert.p12"));
            return inputStream;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public int getHttpConnectTimeoutMs() {
        return 6*1000;
    }

    @Override
    public int getHttpReadTimeoutMs() {
        return 8*1000;
    }
}

