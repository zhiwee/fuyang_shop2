package com.zhiwee.gree.webapi.Controller.ShopDistributor;


import com.zhiwee.gree.model.OrderInfo.Order;
import com.zhiwee.gree.model.ShopDistributor.DistributorArea;
import com.zhiwee.gree.model.ShopDistributor.DistributorOrder;
import com.zhiwee.gree.model.User;
import com.zhiwee.gree.service.OrderInfo.OrderService;
import com.zhiwee.gree.service.ShopDistributor.DistributorAreaService;
import com.zhiwee.gree.service.ShopDistributor.DistributorOrderService;
import com.zhiwee.gree.webapi.util.SystemControllerLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.util.IdGenerator;
import xyz.icrab.common.web.annotation.EnableGetRequest;
import xyz.icrab.common.web.annotation.EnableListRequest;
import xyz.icrab.common.web.annotation.Session;
import xyz.icrab.common.web.param.IdsParam;
import xyz.icrab.common.web.util.Validator;
import xyz.icrab.ums.util.consts.SessionKeys;

import java.util.*;

@RequestMapping("/distributorOrder")
@Controller
@ResponseBody
@EnableListRequest
@EnableGetRequest
public class DistributorOrderController {


    @Autowired
    private DistributorOrderService distributorOrderService;

    @Autowired
    private OrderService orderService;

    /**
     * Description: 获取所有属于自己的配送订单
     * @author: sun
     * @Date 下午3:19 2019/5/21
     * @param:
     * @return:
     */
//    @SystemControllerLog(description = "获取所有属于自己的配送订单")
    @RequestMapping("/infoPage")
    @ResponseBody
    public Result<?> infoPage(@RequestBody Map<String, Object> param, Pagination pagination,@Session(SessionKeys.USER) User user) {
        if (param == null) {
            param = new HashMap<>();
        }
        if(user.getDistributorId() != null && !"".equals(user.getDistributorId())){
            param.put("distributorId",user.getDistributorId());
        }
        Pageable<DistributorOrder> pageable = distributorOrderService.infoPage(param, pagination);
        return Result.ok(pageable);
    }



    /**
     * Description:发货
     * @author: sun
     * @Date 下午3:20 2019/5/21
     * 申请退款状态  1 未申请退换货  2 申请退货中  3已允许退货，4拒绝退货 5 申请换货中
     * 6允许换货 7 拒绝换货  8申请退款中  9 同意退款 10 拒绝退款
     *      *    1 退货  2 换货，3退款
     *
     *      是否需要限制到父订单的状态来决定是否是否可以发货
     * @param:
     * @return:
     */
//    @SystemControllerLog(description = "发货")
    @RequestMapping("/open")
    @ResponseBody
    public Result<?> open(@RequestBody IdsParam param) {
        List<String> list = param.getIds();
        for(String id : list){
            DistributorOrder  distributorOrder= distributorOrderService.get(id);
            Order order = orderService.get(distributorOrder.getOrderId());
            if(order == null || order.getRefundState() == 8){
                return Result.of(Status.ClientError.UPGRADE_REQUIRED, " 该订单申请退款中无法发货");
            }
            if(distributorOrder.getState() != 1){
                return Result.of(Status.ClientError.UPGRADE_REQUIRED, "存在不正常的订单无法发货");
            }
            distributorOrder.setId(id);
            distributorOrder.setDeliveryState(1);
            distributorOrder.setUpdateTime(new Date());
            distributorOrder.setDeliveryTime(new Date());
//            distributorOrderService.updateOrders(distributorOrder);
        }
        return Result.ok();
    }
    /**
     * Description: 取消发货
     * @author: sun
     * @Date 下午3:26 2019/5/21
     * @param:
     * @return:
     */
//    @SystemControllerLog(description = "取消发货")
    @RequestMapping("/stop")
    @ResponseBody
    public Result<?> stop(@RequestBody IdsParam param) {
        List<String> list = param.getIds();
        for(String id : list){
            DistributorOrder  distributorOrder= distributorOrderService.get(id);
            if(distributorOrder.getDeliveryState()  != 2){
                return Result.of(Status.ClientError.UPGRADE_REQUIRED, "存在不是已经发货的订单无法取消");
            }

            distributorOrder.setId(id);
            distributorOrder.setDeliveryState(2);
            distributorOrderService.updateDistributorOrder(distributorOrder);
        }
        return Result.ok();
    }


    /**
     * Description: 改派订单
     * @author: sun
     * @Date 下午3:20 2019/5/21
     * @param:
     * @return:
     */
//    @SystemControllerLog(description = "改派订单")
    @RequestMapping("/changeOrder")
    @ResponseBody
    public Result<?> changeOrder(@RequestBody Map<String, Object> param) {
        DistributorOrder distributorOrder = distributorOrderService.get((String) param.get("orderId"));
      if( distributorOrder.getDeliveryState() != 2 || distributorOrder.getState() != 1) {
          return Result.of(Status.ClientError.UPGRADE_REQUIRED, "该订单已经发货或者该订单已经被取消无法改派");
      }
        distributorOrder.setDistributorId((String) param.get("dealerId"));
        distributorOrder.setUpdateTime(new Date());
        distributorOrderService.update(distributorOrder);
        return Result.ok();
    }


}
