package com.zhiwee.gree.webapi.Controller.card;


import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.card.CellGetValue;
import com.zhiwee.gree.model.card.WelfareCard;
import com.zhiwee.gree.model.card.vo.WelfareCardExrt;
import com.zhiwee.gree.service.ShopUser.ShopUserService;
import com.zhiwee.gree.service.card.WelfareCardService;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jeecgframework.poi.excel.ExcelExportUtil;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.enmus.ExcelType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.util.KeyUtils;
import xyz.icrab.common.web.annotation.EnableGetRequest;
import xyz.icrab.common.web.annotation.EnableListRequest;
import xyz.icrab.common.web.annotation.Session;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.*;

import static xyz.icrab.common.model.Status.ClientError.BAD_REQUEST;

@RequestMapping("/welfareCard")
@Controller
@ResponseBody
@EnableListRequest
@EnableGetRequest
public class WelfareCardController {

    @Autowired
    private WelfareCardService welfareCardService;
    @Autowired
    private ShopUserService shopUserService;

    /**
     * @return
     * @author cs
     * @description 页面
     * @date 2020/02/14 15:55
     */
    @RequestMapping(value = "/infoPage")
    public Result<?> infoPage(@RequestBody Map<String, Object> params, Pagination pagination) {
        if (params == null) {
            params = new HashMap<>();
        }
        Pageable<WelfareCard> page = welfareCardService.pageInfo(params, pagination);
        return Result.ok(page);
    }



    /**
     * @author tzj
     * @description 领取公益卡
     * @date 2020/4/1 19:39
    */
    @RequestMapping(value = "/getWelfare")
    @ResponseBody
    public Result<?> getWelfare(@RequestBody(required = false) Map<String, Object> param, @Session("ShopUser")ShopUser shopUser) {
      String cardCode= (String) param.get("cardCode");
      String fyCode= (String) param.get("fyCode");

        WelfareCard welfareCard=new WelfareCard();
        welfareCard.setCardCode(cardCode);
        welfareCard=welfareCardService.getOne(welfareCard);
        if (welfareCard==null||welfareCard.getState()==1){
            return Result.of(BAD_REQUEST, "该卡号不存在或已经被领取");
        }
        Map<String, Object> params=new HashMap<>(5);
        params.put("userId",shopUser.getId());
        List<WelfareCard> list = welfareCardService.list(params);
        if (list.size()>0){
            return Result.of(BAD_REQUEST, "您已经领取过该卡");
        }
        welfareCard.setUserId(shopUser.getId());
        welfareCard.setState(1);
        welfareCard.setUpdateTime(new Date());
        welfareCard.setFyCode(fyCode);
        welfareCard.setPhone(shopUser.getPhone());
        welfareCard.setUserName(shopUser.getNickname());
        welfareCardService.update(welfareCard);
        return Result.ok("领取成功");
    }

    /**
     * @author tzj
     * @description 获取个人卡信息
     * @date 2020/4/1 20:10
    */

    @RequestMapping(value = "/getInfo")
    @ResponseBody
    public Result<?> getWelfare( @Session("ShopUser")ShopUser shopUser) {
        WelfareCard welfareCard=new WelfareCard();
        welfareCard.setUserId(shopUser.getId());
        welfareCard=welfareCardService.getOne(welfareCard);
        if (welfareCard==null){
            return Result.of(BAD_REQUEST, "未找到信息");
        }
        return Result.ok(welfareCard);
    }

    /**
     * @author tzj
     * @description 导入卡号
     * @date 2020/4/1 19:38
    */
    @RequestMapping("/import")
    public Result<?> dealerUserImport(@RequestParam("file") MultipartFile file) {
        InputStream is = null;
        XSSFWorkbook hssfWorkbook = null;
        try {
            is = file.getInputStream();
            hssfWorkbook = new XSSFWorkbook(is);
        } catch (IOException e) {
            e.printStackTrace();
        }
        WelfareCard go;
        Map<String, Object> param = new HashMap<>();
        // 循环工作表Sheet
        for (int numSheet = 0; numSheet < hssfWorkbook.getNumberOfSheets(); numSheet++) {
            XSSFSheet hssfSheet = hssfWorkbook.getSheetAt(numSheet);
            if (hssfSheet == null) {
                continue;
            }
            // 循环行Row
            for (int rowNum = 1; rowNum <= hssfSheet.getLastRowNum(); rowNum++) {
                XSSFRow hssfRow = hssfSheet.getRow(rowNum);
                if (hssfRow != null) {
                    go = new WelfareCard();
                    XSSFCell cardCode = hssfRow.getCell(0);
                    if (cardCode == null) {
                        return Result.of(BAD_REQUEST, "工作表" + numSheet + "的第" + rowNum + "行的卡号为空");
                    }
                    cardCode.setCellType(cardCode.CELL_TYPE_STRING);
                    String value = CellGetValue.getValue(cardCode);
                    param.put("cardCode",value);
                    List<WelfareCard> list = welfareCardService.list(param);
                    if (list.size()>0){
                        return Result.of(BAD_REQUEST, "工作表" + numSheet + "的第" + rowNum + "行的卡号在系统已经存在");
                    }
                    // 虽然excel中设置的都是文本，但是数字文本还被读错，如“1”取成“1.0”
                    // 加上下面这句，临时把它当做文本来读取
                    //   goodsId.setCellType(userid.CELL_TYPE_STRING);
                    //System.out.println(getValue(activityPrice));

                    go.setId(KeyUtils.getKey());
                    go.setCardCode(value);
                    go.setCreateTime(new Date());
                    go.setState(0);
                    welfareCardService.save(go);
                }
            }
        }
        return Result.ok();
    }




    /**
     * @author tzj
     * @description 导出
     * @date 2020/4/1 19:55
    */
    @RequestMapping("/exportUserCards")
    public void export(HttpServletRequest req, HttpServletResponse rsp) throws IOException {
        Map<String, Object> params = new HashMap<>(10);
        params.put("startCreateTime", req.getParameter("startCreateTime"));
        params.put("endCreateTime", req.getParameter("endCreateTime"));
        params.put("userName", req.getParameter("userName"));
        params.put("phone", req.getParameter("phone"));
        params.put("state", req.getParameter("state"));

        List<WelfareCardExrt> result;
        result = welfareCardService.listAll(params);

        // 导出Excel
        OutputStream os = rsp.getOutputStream();

        rsp.setContentType("application/x-download");
        // 设置导出文件名称
        rsp.setHeader("Content-Disposition",
                "attachment;filename=" + DateFormatUtils.format(new Date(), "yyyyMMddHHmmssSSS") + ".xlsx");
        ExportParams param = new ExportParams("领卡信息", "领卡信息", "领卡信息表");
        param.setAddIndex(true);
        param.setType(ExcelType.XSSF);
        Workbook excel = ExcelExportUtil.exportExcel(param, WelfareCardExrt.class, result);
        excel.write(os);
        os.flush();
        os.close();
    }


    /**
     * @author tzj
     * @description  判断是否领取
     * @date 2020/4/13 17:33
    */

    @RequestMapping(value = "/yesOrNo")
    @ResponseBody
    public Result<?> yesOrNo( @Session("ShopUser")ShopUser shopUser) {
        WelfareCard welfareCard=new WelfareCard();
        welfareCard.setUserId(shopUser.getId());
        welfareCard=welfareCardService.getOne(welfareCard);
        if (welfareCard==null){
            return Result.ok("1");
        }
        return Result.ok("2");
    }



}
