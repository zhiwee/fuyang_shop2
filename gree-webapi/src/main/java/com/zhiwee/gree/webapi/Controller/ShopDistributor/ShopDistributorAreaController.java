package com.zhiwee.gree.webapi.Controller.ShopDistributor;



import com.zhiwee.gree.model.ShopDistributor.DistributorArea;

import com.zhiwee.gree.model.User;

import com.zhiwee.gree.service.ShopDistributor.DistributorAreaService;

import com.zhiwee.gree.webapi.util.SystemControllerLog;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.util.IdGenerator;
import xyz.icrab.common.util.KeyUtils;
import xyz.icrab.common.web.annotation.EnableGetRequest;
import xyz.icrab.common.web.annotation.EnableListRequest;
import xyz.icrab.common.web.annotation.Session;
import xyz.icrab.common.web.param.IdParam;
import xyz.icrab.common.web.param.IdsParam;
import xyz.icrab.common.web.util.Validator;
import xyz.icrab.ums.util.consts.SessionKeys;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.*;

@RequestMapping("/distributorArea")
@Controller
@ResponseBody
@EnableListRequest
@EnableGetRequest
public class ShopDistributorAreaController {


    @Autowired
    private DistributorAreaService distributorAreaService;





    /**
     * Description: 添加配送商的负责区域
     * @author: sun
     * @Date 下午11:52 2019/5/21
     * @param:
     * @return:
     */
    @SystemControllerLog(description="添加配送商的负责区域")
    @RequestMapping(value = "/add")
    @ResponseBody
    public Result<?> addShopDistributor(@RequestBody DistributorArea distributorArea, @Session(SessionKeys.USER) User user)  {
        Validator validator = new Validator();
        validator.notEmpty(distributorArea.getAreaId(), "区域不能为空");
        validator.notEmpty(distributorArea.getDistributorId(), "配送商不能为空");
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }
        String[] areaId =  distributorArea.getAreaId().split(",");
        List<DistributorArea> distributorAreas = new ArrayList<>();
        DistributorArea darea = null;
        Map<String,Object> params = new HashMap();


        for(String id  : areaId ){
            darea = new DistributorArea();
            params.put("areaId",id);
            params.put("distributorId",distributorArea.getDistributorId());

            List<DistributorArea> list = distributorAreaService.list(params);
            if(!list.isEmpty() && list.size() > 0){
                return Result.of(Status.ClientError.BAD_REQUEST, "存在添加的区域冲突了");
            }
            darea.setId(IdGenerator.objectId());
            darea.setDistributorId(distributorArea.getDistributorId());
            darea.setAreaId(id);
            darea.setState(1);
            distributorAreas.add(darea);
        }
        distributorAreaService.save(distributorAreas);
        return Result.ok();
    }







    /**
     * Description:启用配送商区域
     * @author: sun
     * @Date 下午3:20 2019/5/21
     * @param:
     * @return:
     */
    @SystemControllerLog(description = "启用配送商区域")
    @RequestMapping("/open")
    @ResponseBody
    public Result<?> open(@RequestBody IdsParam param) {
        List<String> list = param.getIds();
        for(String id : list){
            DistributorArea distributorArea = new DistributorArea();
            distributorArea.setId(id);
            distributorArea.setState(1);
            distributorAreaService.update(distributorArea);
        }
        return Result.ok();
    }
    /**
     * Description: 禁用配送商区域
     * @author: sun
     * @Date 下午3:26 2019/5/21
     * @param:
     * @return:
     */
    @SystemControllerLog(description = "禁用配送商区域")
    @RequestMapping("/stop")
    @ResponseBody
    public Result<?> stop(@RequestBody IdsParam param) {
        List<String> list = param.getIds();
        for(String id : list){
            DistributorArea distributorArea = new DistributorArea();
            distributorArea.setId(id);
            distributorArea.setState(2);
            distributorAreaService.update(distributorArea);
        }
        return Result.ok();
    }


    /**
     * Description: 删除配送区域
     * @author: sun
     * @Date 下午3:26 2019/5/21
     * @param:
     * @return:
     */
    @SystemControllerLog(description = "禁用配送商区域")
    @RequestMapping("/delete")
    @ResponseBody
    public Result<?> delete(@RequestBody IdsParam param) {
        List<String> list = param.getIds();
        for(String id : list){
            DistributorArea distributorArea = new DistributorArea();
            distributorArea.setId(id);
            distributorAreaService.delete(distributorArea);
        }
        return Result.ok();
    }


    /**
     * Description: 获取所有配送商信息
     * @author: sun
     * @Date 下午3:19 2019/5/21
     * @param:
     * @return:
     */
    @SystemControllerLog(description = "获取所有配送商区域信息")
    @RequestMapping("/pageArea")
    @ResponseBody
    public Result<?> PageArea(@RequestBody Map<String, Object> param, Pagination pagination) {
        if (param == null) {
            param = new HashMap<>();
        }
        Pageable<DistributorArea> pageable = distributorAreaService.pageArea(param, pagination);
        return Result.ok(pageable);
    }






}
