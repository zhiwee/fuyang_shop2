package com.zhiwee.gree.webapi.Controller.GoodsInfo;
import com.zhiwee.gree.model.GoodsInfo.Goods;
import com.zhiwee.gree.model.GoodsInfo.GoodsSpecsItemOrder;
import com.zhiwee.gree.service.GoodsInfo.GoodsService;
import com.zhiwee.gree.service.GoodsInfo.GoodsSpecsItemOrderService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.util.KeyUtils;
import xyz.icrab.common.web.param.IdParam;
import xyz.icrab.common.web.param.IdsParam;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequestMapping("/goodsSpecsItemOrder")
@RestController
public class GoodsSpecsItemOrderController {

    @Autowired
    private GoodsSpecsItemOrderService goodsSpecsItemOrderService;


    @Autowired
    private GoodsService goodsService;


    /**
     * 增加 周广
     * @param param
     * @return
     */
    @RequestMapping("/add")
    public Result<?> add(@RequestBody Map<String, Object> param){
        if (param == null) {
            param = new HashMap<>();
        }
        GoodsSpecsItemOrder goodsSpecsItemOrder=new GoodsSpecsItemOrder();
        Goods goods=goodsService.get((String)param.get("goodsId"));
        if(goods==null){
            return Result.of(Status.ClientError.BAD_REQUEST,"请先完成商品基础信息的填写");
        }
        BeanUtils.copyProperties(goods,goodsSpecsItemOrder);
        goodsSpecsItemOrder.setId(KeyUtils.getKey());
        goodsSpecsItemOrder.setGoodsId((String)param.get("goodsId"));
        goodsSpecsItemOrder.setName(goodsSpecsItemOrder.getName()+(String)param.get("name"));
        goodsSpecsItemOrderService.save(goodsSpecsItemOrder);
        return Result.ok(goodsSpecsItemOrder);
    }
    /**
     * 确认会销商品 周广
     * @param param
     * @return
     */
    @RequestMapping("/updateItem")
    public Result<?> updateItem(@RequestBody Map<String, Object> param){
        if (param == null) {
            param = new HashMap<>();
        }
        if(param.get("id")==null){
            return Result.of(Status.ClientError.BAD_REQUEST,"未选中详情，请检查");
        }
        GoodsSpecsItemOrder goodsSpecsItemOrder=new GoodsSpecsItemOrder();
        goodsSpecsItemOrder.setId((String)param.get("id"));
        goodsSpecsItemOrder.setDealerGoodsId((String)param.get("goodsId"));
        goodsSpecsItemOrderService.update(goodsSpecsItemOrder);
        return Result.ok();
    }
    /**
     * 修改保存 周广
     * @param goodsSpecsItemOrder
     * @return
     */
    @RequestMapping("/updateAll")
    public Result<?> updateAll(@RequestBody GoodsSpecsItemOrder goodsSpecsItemOrder){
        if(goodsSpecsItemOrder.getId()==null){
            return Result.of(Status.ClientError.BAD_REQUEST,"数据有误");
        }
        goodsSpecsItemOrderService.update(goodsSpecsItemOrder);
        return Result.ok();
    }

    /***
     * 查询单个  周广
     * @param param
     * @return
     */
    @RequestMapping("/selectOne")
    @ResponseBody
    public Result<?> selectOne(@RequestBody Map<String, Object> param){
        List<GoodsSpecsItemOrder> goodsSpecsItemOrderList=goodsSpecsItemOrderService.getListBYGoodsId(param);
        return Result.ok(goodsSpecsItemOrderList);
    }


    /***
     * 删除 周广
     * @param param
     * @return
     */
    @RequestMapping("/delete")
    @ResponseBody
    public Result<?> delete(@RequestBody @Valid IdsParam param){
        for (String id : param.getIds()) {
            GoodsSpecsItemOrder goodsSpecsItemOrder=new GoodsSpecsItemOrder();
            goodsSpecsItemOrder.setId(id);
            goodsSpecsItemOrderService.delete(goodsSpecsItemOrder);
        }
        return Result.ok();
    }
}
