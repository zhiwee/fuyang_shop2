package com.zhiwee.gree.webapi.Controller.GoodsLabel;

import com.zhiwee.gree.model.GoodsLabel.Space;
import com.zhiwee.gree.service.GoodsLabel.SpaceService;
import com.zhiwee.gree.webapi.util.SystemControllerLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.util.IdGenerator;
import xyz.icrab.common.web.annotation.EnableDeleteRequest;
import xyz.icrab.common.web.annotation.EnableGetRequest;
import xyz.icrab.common.web.annotation.EnableListRequest;
import xyz.icrab.common.web.annotation.EnablePageRequest;
import xyz.icrab.common.web.util.Validator;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author sun on 2019/5/28
 */
@RestController
@RequestMapping("/space")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class SpaceController {


    @Autowired
    private SpaceService spaceService;
    /**
     * Description:  查询所有分页
     * @author: sun
     * @Date 下午10:40 2019/3/27
     * @param:
     * @return:
     */
    @SystemControllerLog(description="查询所有分页")
    @RequestMapping(value = "infoPage")
    public Result<?> searchAll(@RequestBody(required = false) Map<String, Object> params, Pagination pagination) {
        Pageable<Space> GoodsInfoList = spaceService.page(params, pagination);
        return Result.ok(GoodsInfoList);
    }



    /**
     * Description:  查询不分页
     * @author: sun
     * @Date 下午10:40 2019/3/27
     * @param:
     * @return:
     */
    @SystemControllerLog(description="查询所有数据不分页")
    @RequestMapping(value = "searchList")
    public Result<?> searchList() {
        List<Space> bps = spaceService.list();
        return Result.ok(bps);
    }



    /**
     * Description:  添加标签
     * @author: sun
     * @Date 下午10:40 2019/3/27
     * @param:
     * @return:
     */
    @SystemControllerLog(description="全品类优惠券的添加")
    @RequestMapping(value = "add")
    public Result<?>  addCoupons(@RequestBody Space space ) {
        Validator validator = new Validator();
        validator.notNull(space.getName(), "名称不能为空");
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }
        space.setId(IdGenerator.objectId());
        space.setCreateTime(new Date());
        spaceService.save(space);
        return Result.ok();

    }



    /**
     * Description: 更新标签
     * @author: sun
     * @Date 下午9:06 2019/5/8
     * @param:
     * @return:
     */
    @SystemControllerLog(description="全品类优惠劵更新")
    @RequestMapping("/update")
    @ResponseBody
    public  Result<?> updateCoupon(@RequestBody Space space) {
        Validator validator = new Validator();
        validator.notNull(space.getName(), "名称不能为空");
        validator.notNull(space.getId(), "未获取到标签");
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }
        spaceService.update(space);
        return Result.ok();
    }


    /**
     * Description: 通过id查找标签
     *
     * @author: sun
     * @Date 下午10:40 2019/3/27
     * @param:
     * @return:
     */
    @SystemControllerLog(description = "通过id查找标签")
    @RequestMapping(value = "/getById")
    public Result<?> achieveGoodsById(@RequestBody Map<String, Object> params) {
        if (params == null) {
            params = new HashMap<>();
        }
        Space ps= spaceService.get((String)params.get("id"));
        return Result.ok(ps);
    }

}
