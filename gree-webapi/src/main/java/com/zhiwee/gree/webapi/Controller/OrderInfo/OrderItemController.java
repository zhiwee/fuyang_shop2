package com.zhiwee.gree.webapi.Controller.OrderInfo;

import com.zhiwee.gree.model.Coupon.Coupon;
import com.zhiwee.gree.model.GoodsInfo.GoodsInfo;
import com.zhiwee.gree.model.GoodsInfo.ShopGoodsInfo;
import com.zhiwee.gree.model.OrderInfo.Order;
import com.zhiwee.gree.model.OrderInfo.OrderChange;
import com.zhiwee.gree.model.OrderInfo.OrderItem;
import com.zhiwee.gree.model.OrderInfo.OrderRecord;
import com.zhiwee.gree.model.OrderInfo.vo.GoodNameTopTen;
import com.zhiwee.gree.model.User;
import com.zhiwee.gree.service.Coupon.CouponService;
import com.zhiwee.gree.service.GoodsInfo.GoodsInfoService;
import com.zhiwee.gree.service.GoodsInfo.ShopGoodsInfoService;
import com.zhiwee.gree.service.OrderInfo.OrderItemService;
import com.zhiwee.gree.service.OrderInfo.OrderService;
import com.zhiwee.gree.webapi.util.SystemControllerLog;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.web.annotation.*;
import xyz.icrab.common.web.param.IdParam;
import xyz.icrab.ums.util.consts.SessionKeys;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.*;


/**
 * @author sun on 2019/4/30
 */
@RestController
@RequestMapping("/orderItem")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class OrderItemController {
    @Autowired
    private OrderItemService orderItemService;

    @Autowired
    private OrderService orderService;

    @Autowired
    private GoodsInfoService goodsInfoService;
    @Autowired
    private ShopGoodsInfoService shopGoodsInfoService;

    @Autowired
    private CouponService couponService;



    /**
   * Description: 订单明细
   * @author: sun
   * @Date 下午1:16 2019/5/12
   * @param:
   * @return:
   */
    @RequestMapping("/orderItemList")
    @PermissionMode(PermissionMode.Mode.White)
    public Object orderList(@RequestBody Map<String, Object> params, Pagination pagination) {
        if (params == null) {
            params = new HashMap<>();
        }
        Pageable<OrderItem> page = orderItemService.orderItemList(params, pagination);
        return Result.ok(page);
    }

    /**
     * Description: 商品明细
     * @author: sun
     * @Date 下午1:16 2019/5/12
     * @param:
     * @return:
     */
//    @SystemControllerLog(description="商品明细")
    @RequestMapping("/orderItem")
    public Result<?> orderItem(@RequestBody @Valid IdParam map) {
        OrderItem orderItem = orderItemService.get(map.getId());
        return Result.ok(orderItem);
    }


    /**
     * Description: 更新订单商品明细
     * @author: sun
     * @Date 下午1:16 2019/5/12
     * @param:换货处理包括价格的处理，优惠券的处理，数量的处理
     * @return:
     */
//    @SystemControllerLog(description="更新订单商品明细")
    @RequestMapping("/updateOrderItem")
    public Result<?> orderList(@RequestBody OrderItem orderItem,@Session(SessionKeys.USER) User user) {
        if(orderItem.getUnitPrice() <= 0 || orderItem.getBuyBun()<=0){
            return Result.of(Status.ClientError.UPGRADE_REQUIRED, "请填写正确的商品价格或者数量");
        }
        OrderItem item = orderItemService.get(orderItem.getId());
        Order order = orderService.get(item.getOrderId());
//        申请退款状态  1 未申请退换货  2 申请退货中  3已允许退货，4拒绝退货 5 申请换货中 6允许换货 7 拒绝换货  8申请退款中  9 同意退款 10 拒绝退款
        if( order.getOrderState() == 2 && order.getRefundState() != 5){
            return Result.of(Status.ClientError.UPGRADE_REQUIRED, "用户未申请换货");
        }
        OrderChange orderChange =  new OrderChange();
         Boolean flag = false;
        order.setUpdateMan(user.getId());
        order.setUpdateTime(new Date());
        //保存原来订单的价格以及订单和订单明细的id
        // 设置原来的数量以及现在数量
        //设置现在的商品id以及原来的商品id
        orderChange.setOldRealPrice(order.getRealPrice());
        orderChange.setOrderId(order.getId());
        orderChange.setOrderItemId(item.getId());
        orderChange.setOrderState(1);
        orderChange.setOldGood(item.getGoodsId());
        orderChange.setNewGood(orderItem.getGoodsId());
        orderChange.setOldNum(item.getBuyBun());
        orderChange.setNewNum(orderItem.getBuyBun());

        order.setTotalPrice(BigDecimal.valueOf(order.getTotalPrice()).subtract(BigDecimal.valueOf(item.getAmount())).doubleValue());
        order.setRealPrice(BigDecimal.valueOf(order.getRealPrice()).subtract(BigDecimal.valueOf(item.getRealAmount())).doubleValue());
        item.setUpdateMan(user.getId());
        item.setUpdateTime(new Date());
        if(orderItem.getGoodsId().equals(item.getGoodsId())){



            item.setBuyBun(orderItem.getBuyBun());
            item.setSubtractPrice(orderItem.getSubtractPrice());
            //item.setPromotionCosts(BigDecimal.valueOf(item.getActivityCost()).multiply(BigDecimal.valueOf(orderItem.getBuyBun())).doubleValue());
            //调货商品以前端穿过来的价格为准
            item.setAmount(BigDecimal.valueOf(orderItem.getUnitPrice()).multiply(new BigDecimal(orderItem.getBuyBun())).doubleValue());
            //减去调价的金额
                Double price = BigDecimal.valueOf(orderItem.getUnitPrice()).multiply(new BigDecimal(orderItem.getBuyBun())).subtract(BigDecimal.valueOf(orderItem.getSubtractPrice())).doubleValue();
                item.setRealAmount(price < 0 ? 0:price );


                //同一个商品优惠金额照常使用
            if(item.getCouponMoney() != null){
                Double realPrice = BigDecimal.valueOf(item.getRealAmount()).subtract(BigDecimal.valueOf(item.getCouponMoney())).doubleValue();
                item.setRealAmount(realPrice < 0 ? 0:realPrice );
            }

        }

        if(!orderItem.getGoodsId().equals(item.getGoodsId())){
            //只有分销订单以及商城订单才需要改派
            if(order.getIsOffline() == 1 || order.getIsOffline() == 3){
                flag = true;
            }
            ShopGoodsInfo goodsInfo = shopGoodsInfoService.get(orderItem.getGoodsId());
            item.setBuyBun(orderItem.getBuyBun());
            item.setUnitPrice(orderItem.getUnitPrice());
            item.setGoodsId(goodsInfo.getId());
            item.setGoodsName(goodsInfo.getName());
            //item.setActivityCost(goodsInfo.getActivityCost());
            item.setCategoryId(goodsInfo.getCategory());
            item.setSpecId(goodsInfo.getSpecId());
//            item.setSubtractPrice(orderItem.getSubtractPrice());
            item.setSubtractPrice(orderItem.getSubtractPrice()== null ?0:orderItem.getSubtractPrice());
          //  item.setPromotionCosts(BigDecimal.valueOf(goodsInfo.getActivityCost()).multiply(BigDecimal.valueOf(orderItem.getBuyBun())).doubleValue());
            item.setAmount(BigDecimal.valueOf(orderItem.getUnitPrice()).multiply(new BigDecimal(orderItem.getBuyBun())).doubleValue());
            Double price = BigDecimal.valueOf(orderItem.getUnitPrice()).multiply(new BigDecimal(orderItem.getBuyBun())).subtract(BigDecimal.valueOf(orderItem.getSubtractPrice())).doubleValue();
            item.setRealAmount(price < 0 ? 0:price );
            if(item.getCouponMoney() != null){
                Coupon coupon = couponService.get(item.getCouponId());
                if(goodsInfo.getUseCoupon()== 1){
                    //全品类代金券以及优惠券
                    if(coupon.getType() == 1 || coupon.getType() == 4){
                        Double realPrice = BigDecimal.valueOf(item.getRealAmount()).subtract(BigDecimal.valueOf(item.getCouponMoney())).doubleValue();
                        item.setRealAmount(realPrice < 0 ? 0:realPrice );
                        //非全品类的优惠券或者代金券，判断是否在这个类型中
                    }else if((coupon.getType() == 2 || coupon.getType() == 5) && StringUtils.contains(coupon.getIdPath(), item.getCategoryId()) ){
                        Double realPrice = BigDecimal.valueOf(item.getRealAmount()).subtract(BigDecimal.valueOf(item.getCouponMoney())).doubleValue();
                        item.setRealAmount(realPrice < 0 ? 0:realPrice );
                        //指定商品优惠券或者代金券 判断是否包含商品
                    }else if((coupon.getType() == 3 || coupon.getType() == 6) && StringUtils.contains(coupon.getIdPath(), item.getGoodsId())){
                        Double realPrice = BigDecimal.valueOf(item.getRealAmount()).subtract(BigDecimal.valueOf(item.getCouponMoney())).doubleValue();
                        item.setRealAmount(realPrice < 0 ? 0:realPrice );
                    }else{
                        item.setCouponMoney(0.0);
                    }
                }


            }
        }
        order.setTotalPrice(BigDecimal.valueOf(order.getTotalPrice()).add(BigDecimal.valueOf(item.getAmount())).doubleValue());
        order.setRealPrice(BigDecimal.valueOf(order.getRealPrice()).add(BigDecimal.valueOf(item.getRealAmount())).doubleValue());
        orderChange.setNewRealPrice(order.getRealPrice());
        orderItemService.updateOrderAndItem(item,order,flag,orderChange);
        return Result.ok(orderItem);
    }




    /**
     * Description: 获取商品的购买记录
     * @author: sun
     * @Date 上午11:40 2019/6/26
     * @param:
     * @return:
     */
    @PermissionMode(PermissionMode.Mode.White)
    @RequestMapping("/achieveOrderRecord")
    public Object achieveOrderRecord(@RequestBody @Valid IdParam map, Pagination pagination, HttpServletRequest req, HttpServletResponse res) {
        List<String> ids =Arrays.asList(map.getId().split(","));
        Pageable<OrderRecord> shopUserComments=orderItemService.achieveOrderRecord(ids,pagination);
        return Result.ok(shopUserComments);
    }

    /**
     * Description: 销售前10商品
     * @author: sun
     * @Date 上午11:15 2019/7/1
     * @param:
     * @return:
     */

    @RequestMapping("/topTen")
    public Result<?> topTen(@Session(SessionKeys.USER) User user){
     if (!"1a".equals(user.getId())){
        return null;
    }
        List<GoodNameTopTen> goodNameTopTens = orderItemService.topTen();
        return Result.ok(goodNameTopTens);
    }








    /**
     * Description: 工程订单删除
     * @author: sun
     * @Date 下午3:00 2019/7/11
     * @param:
     * @return:
     */
    @RequestMapping("deleteOrderItem")
    public Result<?> deleteOrderItem(@RequestBody Map<String, Object> param) {
        if(param.get("orderItemId") == null || ((String)param.get("orderItemId")).equals("")){
            return Result.of(Status.ClientError.BAD_REQUEST, "订单明细标识不能为空");
        }
        OrderItem orderItem = orderItemService.get((String)param.get("orderItemId"));
        orderItemService.delete(orderItem);
        Map<String,Object> params = new HashMap<>();
        params.put("orderId",orderItem.getOrderId());
        List<OrderItem> orderItemList = orderItemService.achieveItemsByOrderId(param);
        if(orderItemList.isEmpty() || orderItemList.size() <= 0){
            orderItemService.deleteAll(params);
        }
        return Result.ok();
    }
}
