package com.zhiwee.gree.webapi.Controller.KillGoods;


import com.zhiwee.gree.model.OnlineActivity.vo.OnlineActivityInfoVo;
import com.zhiwee.gree.service.GoodsInfo.GoodsService;
import com.zhiwee.gree.webapi.util.JsonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.web.annotation.EnableDeleteRequest;
import xyz.icrab.common.web.annotation.EnableGetRequest;
import xyz.icrab.common.web.annotation.EnableListRequest;
import xyz.icrab.common.web.annotation.EnablePageRequest;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/kill")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class KillGoodsController {


    @Autowired
    private GoodsService goodsService;

    @Autowired
    private RedisTemplate<String,String>  redisTemplate;



    /**
     * @Author: jick
     * @Date: 2019/10/16 17:06
     * 秒杀
     */

       public Result<?>  getKillGoods(@RequestBody Map<String,Object> params){

           params.put("activityType",1);
           params.put("state",1);
           List<OnlineActivityInfoVo>  l1 =  goodsService.getCurrentSceKillGoods(params);

           //查询redis数据
            String  str =  redisTemplate.opsForValue().get("killGoods");
            List<OnlineActivityInfoVo>  l2 = null;
             if(null !=str && str.length()>0){
                 // gcList1 = JsonUtil.jsonToList(levelStr1, GoodsCategory.class);
                 l2 = JsonUtil.jsonToList(str, OnlineActivityInfoVo.class);
             }

             //如果缓存数据库里面的数据与
           return null;



       }







}
