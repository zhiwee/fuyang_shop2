package com.zhiwee.gree.webapi.Controller.share;


import com.zhiwee.gree.model.Coupon.Coupon;
import com.zhiwee.gree.model.Coupon.LShareSubject;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.draw.CanDraws;
import com.zhiwee.gree.model.draw.ShareInfo;
import com.zhiwee.gree.service.Coupon.CouponService;
import com.zhiwee.gree.service.Coupon.LShareSubjectService;
import com.zhiwee.gree.service.ShopUser.ShopUserService;
import com.zhiwee.gree.service.draw.CanDrawsService;
import com.zhiwee.gree.service.draw.ShareInfoService;
import com.zhiwee.gree.util.ExecutorUtils;
import com.zhiwee.gree.util.RedisLock;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.util.KeyUtils;
import xyz.icrab.common.web.annotation.EnableGetRequest;
import xyz.icrab.common.web.annotation.EnableListRequest;
import xyz.icrab.common.web.annotation.PermissionMode;
import xyz.icrab.common.web.annotation.Session;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.zhiwee.gree.webapi.Controller.Coupon.CouponController.ACHIEVE_WORKING_FLAG;

@RequestMapping("/shareInfo")
@Controller
@ResponseBody
@EnableListRequest
@EnableGetRequest

/*
 * @author tzj
 * @description
 * @date 2019/12/14 0:34
 * @return
 */
public class ShareInfoController {
    @Resource
    private ShareInfoService shareInfoService;
    @Resource
    private ShopUserService shopUserService;
    @Resource
    private LShareSubjectService lShareSubjectService;
    @Resource
    private CouponService couponService;
    @Resource
    private RedisTemplate redisTemplate;
    /**
     * @author tzj
     * @description 直播分享
     * @date 2020/4/9 15:25
     */
    @RequestMapping("/add")
    @ResponseBody
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> add(@RequestBody Map<String, Object> param) {
        String shareOpenId = (String) param.get("shareOpenId");
        String acceptOpenId = (String) param.get("acceptOpenId");
        String lockKey = (String) param.get("lockKey");
        if (StringUtils.isEmpty(shareOpenId) || StringUtils.isEmpty(acceptOpenId) || StringUtils.isEmpty(lockKey)) {
            return Result.of(Status.ClientError.BAD_REQUEST, "参数获取失败，请重试");
        }
        ShopUser shopUser = new ShopUser();
        shopUser.setOpenId(shareOpenId);
        shopUser = shopUserService.getOne(shopUser);
        if (shopUser == null) {
            return Result.of(Status.ClientError.BAD_REQUEST, "用户信息未获取到添加分享失败");
        }
        ShareInfo shareInfo = new ShareInfo();
        shareInfo.setId(KeyUtils.getKey());
        shareInfo.setLockKey(lockKey);
        shareInfo.setAcceptOpenId(acceptOpenId);
        shareInfo.setCreatetime(new Date());
        shareInfo.setShopUserId(shopUser.getId());
        shareInfoService.save(shareInfo);
        //分享成功去计数看看是否领券
//        checkNum(shopUser);
        return Result.ok("成功");

    }

    /**
     * @author tzj
     * @description 分享成功去计数看看是否领券
     * @date 2020/4/24 14:19
    */
    private void checkNum(ShopUser shopUser) {
        double v=0.0;
        Map<String,Object> param=new HashMap<>(5);
        param.put("shopUserId",shopUser.getId());
        param.put("keyWord","livePlay");
        //找到此人所有分享记录
        List<ShareInfo> list = shareInfoService.list(param);
        if (!list.isEmpty()) {
            //找到分享直播可领券
            List<LShareSubject> shareList = lShareSubjectService.list();
            if (!shareList.isEmpty()){
                LShareSubject lShareSubject = shareList.get(0);
                //查看此人是否已经领取足够的优惠券
                Map<String,Object> checkCoupon=new HashMap<>(5);
                checkCoupon.put("userId",shopUser.getId());
                checkCoupon.put("subjectId",lShareSubject.getSubjectId());
                List<Coupon> coupons = couponService.list(checkCoupon);
                if (!coupons.isEmpty()&&coupons.size()>lShareSubject.getUserNum()){
                    return;
                }
                //如果分享次数达到要求
                if (list.size()>=lShareSubject.getShareNum()){
                    //找到优惠券
                    Coupon coupon=new Coupon();
                    coupon.setType(1);
                    //类似于Java的可重入锁
                    RedisLock redisLock = new RedisLock(redisTemplate);
                    boolean canWork = redisLock.lock(ACHIEVE_WORKING_FLAG);
                    if (!canWork) {
                       return;
                    }
//接口new的操作，会默认生成一个实现这个接口和方法的对象，实质就是new了一个实现这个接口的对象的
                    ExecutorUtils.execute(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                redisLock.lock(ACHIEVE_WORKING_FLAG);
                                couponService.getCoupon(shopUser, coupon, v);
                            } finally {
                                redisLock.deleteKey(ACHIEVE_WORKING_FLAG);
                            }
                        }
                    });

                }


            }
        }
    }


}
