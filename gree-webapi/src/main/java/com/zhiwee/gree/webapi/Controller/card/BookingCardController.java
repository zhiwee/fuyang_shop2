package com.zhiwee.gree.webapi.Controller.card;


import com.zhiwee.gree.model.User;
import com.zhiwee.gree.model.card.BookingCard;
import com.zhiwee.gree.service.card.BookingCardService;
import com.zhiwee.gree.service.card.BookingCardsService;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.util.KeyUtils;
import xyz.icrab.common.web.annotation.EnableGetRequest;
import xyz.icrab.common.web.annotation.EnableListRequest;
import xyz.icrab.common.web.annotation.Session;
import xyz.icrab.ums.util.consts.SessionKeys;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author DELL
 */
@RequestMapping("/bookingCard")
@Controller
@ResponseBody
@EnableListRequest
@EnableGetRequest
public class BookingCardController {
    @Resource
    private BookingCardService bookingCardService;
    @Resource
    private BookingCardsService bookingCardsService;

    /**
     * @return
     * @author cs
     * @description 预售卡页面
     * @date 2020/02/13 12:55
     */
    @RequestMapping(value = "/infoPage")
    public Result<?> infoPage(@RequestBody Map<String, Object> params, Pagination pagination) {
        if (params == null) {
            params = new HashMap<>();
        }
        Pageable<BookingCard> page = bookingCardService.pageInfo(params, pagination);
        return Result.ok(page);
    }

    @RequestMapping("/delete")
    @ResponseBody
    public Result<?> delete(@RequestBody BookingCard bookingCard) {
        bookingCard.setState(0);
        bookingCardService.update(bookingCard);
        return Result.ok();
    }

    @RequestMapping(value = "/add")
    public Result<?> add(@Session(SessionKeys.USER) User user, @RequestBody(required = false) BookingCard cardInfo) {
        Integer numbers = cardInfo.getNumbers();
        if (numbers!=1) {
            if (numbers <= 0 || (numbers % 2 != 0)) {
                return Result.of(Status.ClientError.BAD_REQUEST, "可以购买次数必须是正整数!");
            }
        }
        cardInfo.setId(KeyUtils.getUUID());
        cardInfo.setCreatetime(new Date());
        cardInfo.setCreateman(user.getId());
        cardInfo.setState(1);
        bookingCardService.save(cardInfo);
        return Result.ok();
    }

    @RequestMapping(value = "/get")
    public Result<?> get(@Session(SessionKeys.USER) User user, @RequestBody(required = false) BookingCard cardInfo) {
        BookingCard one = bookingCardService.getInfo(cardInfo);
        return Result.ok(one);
    }

    @RequestMapping(value = "/update")
    @SuppressWarnings("all")
    public Result<?> update(@Session(SessionKeys.USER) User user, @RequestBody(required = false) BookingCard cardInfo) {
        cardInfo.setUpdatetime(new Date());
        cardInfo.setUpdateman(user.getId());
        bookingCardService.update(cardInfo);
        return Result.ok();
    }

    /**
     * @author cs
     * @description 小程序获取预售卡信息
     * @date 2020/02/14 13:24
     */
    @RequestMapping(value = "/cardlist")
    public Result<?> cardList() {
        Map<String, Object> map = new HashMap<>();
        map.put("state",1);
        return Result.ok(bookingCardService.list(map));
    }

    /**
     * @author tzj
     * @description 增加次数
     * @date 2020-05-20 9:28
    */
//    @Scheduled(cron = "0 0 9 * * ?")
    private void addNum() {
        List<BookingCard> list = bookingCardService.list();
        if (CollectionUtils.isNotEmpty(list)){
            for (BookingCard bookingCard : list) {
                bookingCard.setLimitNum(bookingCard.getLimitNum()+650);
                bookingCardService.update(bookingCard);
            }
        }

    }

}
