package com.zhiwee.gree.webapi.Controller.Coupon;

import com.zhiwee.gree.model.BaseInfo.BaseInfo;
import com.zhiwee.gree.model.Coupon.Coupon;
import com.zhiwee.gree.model.Coupon.CouponExport;
import com.zhiwee.gree.model.Coupon.CouponSubject;
import com.zhiwee.gree.model.Coupon.LShareSubject;
import com.zhiwee.gree.model.GoodsInfo.GoodsClassify;
import com.zhiwee.gree.model.GoodsInfo.ShopGoodsInfo;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.User;
import com.zhiwee.gree.model.card.UserCardsInfo;
import com.zhiwee.gree.service.BaseInfo.BaseInfoService;
import com.zhiwee.gree.service.Coupon.CouponService;
import com.zhiwee.gree.service.Coupon.CouponSubjectService;
import com.zhiwee.gree.service.Coupon.LShareSubjectService;
import com.zhiwee.gree.service.GoodsInfo.GoodsClassifyService;
import com.zhiwee.gree.service.GoodsInfo.ShopGoodsInfoService;
import com.zhiwee.gree.service.ShopUser.ShopUserService;
import com.zhiwee.gree.util.ExecutorUtils;
import com.zhiwee.gree.util.RedisLock;
import com.zhiwee.gree.webapi.util.SystemControllerLog;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.jeecgframework.poi.excel.ExcelExportUtil;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.enmus.ExcelType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.util.IdGenerator;
import xyz.icrab.common.web.annotation.*;
import xyz.icrab.common.web.param.IdParam;
import xyz.icrab.common.web.util.Validator;
import xyz.icrab.ums.util.consts.SessionKeys;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.io.OutputStream;
import java.util.*;


/**
 * @author sun on 2019/4/30
 */
@RestController
@RequestMapping("/Coupon")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class CouponController {

    @Resource
    private CouponSubjectService couponSubjectService;

    @Resource
    private RedisTemplate redisTemplate;
    @Resource
    private ShopUserService shopUserService;
    @Resource
    private CouponService couponService;

    @Resource
    private GoodsClassifyService goodsClassifyService;

    @Resource
    private ShopGoodsInfoService shopGoodsInfoService;

    @Resource
    private LShareSubjectService lShareSubjectService;


    @Autowired
    private BaseInfoService baseInfoService;

    public static final String GENERATE_WORKING_FLAG = "couponLock:coupon";

    public static final String ACHIEVE_WORKING_FLAG = "achieveLock:coupon";


    /**
     * Description: 查询所有优惠券
     * @author: sun
     * @Date 上午10:32 2019/4/29
     * @param:
     * @return:
     */
    @PermissionMode(PermissionMode.Mode.White)
    @RequestMapping({"/page"})
    public Result<Pageable<Coupon>> page(@RequestBody(required = false) Map<String, Object> param, Pagination pagination) {
        return Result.ok(couponService.pageInfo(param, pagination));
    }

    /**
     * @author tzj
     * @description 找到领取分享优惠券的
     * @date 2020/4/24 11:54
    */
    @RequestMapping({"/sharePage"})
    public Result<Pageable<Coupon>> sharePage(@RequestBody(required = false) Map<String, Object> param, Pagination pagination) {
        List<LShareSubject> list = lShareSubjectService.list();
            if (list!=null&&list.size()>0){
                String subjectId = list.get(0).getSubjectId();
                    param.put("sendState",2);
                    param.put("subjectId",subjectId);
                return Result.ok(couponService.pageInfo(param, pagination));
            }else {
                return Result.ok();
            }
    }

    /**
     * Description:  全品类优惠券或者代金券的的添加
     * @author: sun
     * @Date 下午10:40 2019/3/27
     * @param:
     * @return:
     */
    @RequestMapping(value = "addCoupons")
    public Result<?> addCoupons(@RequestBody CouponSubject couponSubject, @Session(SessionKeys.USER) User user) {
        Validator validator = new Validator();
        validator.notNull(couponSubject.getForMoney(), "请输入抵扣金额");
        validator.notNull(couponSubject.getAllNum(), "请输入制作的数量");
        validator.notNull(couponSubject.getEndTime(), "结束时间不能为空");
        validator.notNull(couponSubject.getBeginTime(), "开始时间不能为空");
//        validator.notNull(coupon.getStartCouponSeqno(), "请输入开始序号");
        validator.notNull(couponSubject.getLowMoney(), "请输入最低使用标准");
        validator.isTrue(couponSubject.getBeginTime().before(couponSubject.getEndTime()), "开始时间必须小于结束时间");
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }
        couponSubject.setId(IdGenerator.objectId());
        couponSubject.setState(1);
        couponSubject.setCreateTime(new Date());
        couponSubject.setCreateMan(user.getNickname());
        Coupon coupon=new Coupon();
        coupon.setReachMoney(couponSubject.getLowMoney());
        coupon.setStartTime(couponSubject.getBeginTime());
        coupon.setEndTime(couponSubject.getEndTime());
        coupon.setPriceType(couponSubject.getTypePrice());
        coupon.setCouponMoney(couponSubject.getForMoney());
        coupon.setType(couponSubject.getType());
        coupon.setNum(couponSubject.getAllNum());

        couponService.generate(coupon, user,couponSubject);
        return Result.ok();

    }


    /**
     * Description: 全品类优惠劵更新
     *
     * @author: sun
     * @Date 下午9:06 2019/5/8
     * @param:
     * @return:
     */
    @RequestMapping("/updateCoupon")
    @ResponseBody
    public Result<?> updateCoupon(@RequestBody Coupon coupon) {
        Validator validator = new Validator();
        validator.notNull(coupon.getPriceType(), "请选择价格使用类型");
        validator.notNull(coupon.getCouponMoney(), "请输入抵扣金额");
        validator.notNull(coupon.getReachMoney(), "请输入最低使用标准");
        validator.notNull(coupon.getEndTime(), "结束时间不能为空");
        validator.notNull(coupon.getStartTime(), "开始时间不能为空");
        validator.isTrue(coupon.getStartTime().before(coupon.getEndTime()), "开始时间必须小于结束时间");
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }

        Coupon coupons = couponService.get(coupon.getId());
        if (coupons == null) {
            return Result.of(Status.ClientError.FORBIDDEN, "未找到改优惠券");
        }

        if (coupons.getCouponState() == 2) {
            return Result.of(Status.ClientError.FORBIDDEN, "该优惠券已经使用不能更新");
        }
        coupons.setCouponMoney(coupon.getCouponMoney());
        coupons.setReachMoney(coupon.getReachMoney());
        coupons.setPriceType(coupon.getPriceType());
        coupons.setUpdateTime(new Date());
        couponService.update(coupons);
        return Result.ok(coupons);
    }


    /**
     * Description:  非全品类优惠券的添加
     * @author: sun
     * @Date 下午10:40 2019/3/27
     * @param:
     * @return:
     */
    @RequestMapping(value = "addNoAllCoupons")
    public Result<?> addNoAllCoupons(@RequestBody Coupon coupon, @Session(SessionKeys.USER) User user) {
        Validator validator = new Validator();
        validator.notNull(coupon.getPriceType(), "请选择价格使用类型");
        validator.notEmpty(coupon.getIdPath(), "请选择商品分类");
        validator.notNull(coupon.getCouponMoney(), "请输入抵扣金额");
        validator.notNull(coupon.getNum(), "请输入制作的数量");
//        validator.notNull(coupon.getStartCouponSeqno(), "请输入开始序号");
        validator.notNull(coupon.getReachMoney(), "请输入最低使用标准");
        validator.notNull(coupon.getEndTime(), "结束时间不能为空");
        validator.notNull(coupon.getStartTime(), "开始时间不能为空");
        validator.isTrue(coupon.getStartTime().before(coupon.getEndTime()), "开始时间必须小于结束时间");
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }

        CouponSubject couponSubject=new CouponSubject();
        couponSubject.setState(1);
        couponSubject.setType(coupon.getType());
        couponSubject.setForMoney(coupon.getCouponMoney());
        couponSubject.setId(IdGenerator.objectId());
        couponSubject.setName(coupon.getSubjectName());
        couponSubject.setCreateTime(new Date());
        couponSubject.setCreateMan(user.getNickname());
        couponSubject.setTypePrice(coupon.getPriceType());
        couponSubject.setAllNum(coupon.getNum());
        couponSubject.setBeginTime(coupon.getStartTime());
        couponSubject.setEndTime(coupon.getEndTime());
        couponSubject.setLowMoney(coupon.getReachMoney());
        couponService.generate(coupon, user,couponSubject);
        return Result.ok();

    }


    /**
     * Description: 非全品类优惠劵更新
     *
     * @author: sun
     * @Date 下午9:06 2019/5/8
     * @param:
     * @return:
     */
    @RequestMapping("/updateNoAllCoupon")
    @ResponseBody
    public Result<?> updateNoAllCoupon(@RequestBody Coupon coupon) {
        Validator validator = new Validator();
        validator.notNull(coupon.getPriceType(), "请选择价格使用类型");
        validator.notNull(coupon.getCouponMoney(), "请输入抵扣金额");
        validator.notNull(coupon.getReachMoney(), "请输入最低使用标准");
        validator.notNull(coupon.getEndTime(), "结束时间不能为空");
        validator.notNull(coupon.getStartTime(), "开始时间不能为空");
        validator.isTrue(coupon.getStartTime().before(coupon.getEndTime()), "开始时间必须小于结束时间");
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }


        Coupon coupons = couponService.get(coupon.getId());
        if (coupons == null) {
            return Result.of(Status.ClientError.FORBIDDEN, "未找到改优惠券");
        }

        if (coupons.getCouponState() == 2) {
            return Result.of(Status.ClientError.FORBIDDEN, "该优惠券已经使用不能更新");
        }
        coupons.setCouponMoney(coupon.getCouponMoney());

        coupons.setReachMoney(coupon.getReachMoney());
        coupons.setPriceType(coupon.getPriceType());
        coupons.setUpdateTime(new Date());
        couponService.update(coupons);
        return Result.ok(coupons);
    }


    /**
     * Description:  指定商品优惠券的添加
     *
     * @author: sun
     * @Date 下午10:40 2019/3/27
     * @param:
     * @return:
     */

    @RequestMapping(value = "addGoodsCoupons")
    public Result<?> addGoodsCoupons(@RequestBody Coupon coupon, @Session(SessionKeys.USER) User user) {
        Validator validator = new Validator();
        validator.notNull(coupon.getPriceType(), "请选择价格使用类型");
        validator.notNull(coupon.getCouponMoney(), "请输入抵扣金额");
        validator.notNull(coupon.getNum(), "请输入制作的数量");
//        validator.notNull(coupon.getStartCouponSeqno(), "请输入开始序号");
        validator.notNull(coupon.getReachMoney(), "请输入最低使用标准");
        validator.notNull(coupon.getIdPath(), "请选择商品");
        validator.notNull(coupon.getEndTime(), "结束时间不能为空");
        validator.notNull(coupon.getStartTime(), "开始时间不能为空");
        validator.isTrue(coupon.getStartTime().before(coupon.getEndTime()), "开始时间必须小于结束时间");
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }

        CouponSubject couponSubject=new CouponSubject();
        couponSubject.setState(1);
        couponSubject.setType(coupon.getType());
        couponSubject.setForMoney(coupon.getCouponMoney());
        couponSubject.setId(IdGenerator.objectId());
        couponSubject.setName(coupon.getSubjectName());
        couponSubject.setCreateTime(new Date());
        couponSubject.setCreateMan(user.getNickname());
        couponSubject.setTypePrice(coupon.getPriceType());
        couponSubject.setAllNum(coupon.getNum());
        couponSubject.setBeginTime(coupon.getStartTime());
        couponSubject.setEndTime(coupon.getEndTime());
        couponSubject.setLowMoney(coupon.getReachMoney());

        couponService.generate(coupon, user,couponSubject);
        return Result.ok();

    }


    /**
     * Description: 指定商品优惠劵更新
     *
     * @author: sun
     * @Date 下午9:06 2019/5/8
     * @param:
     * @return:
     */

    @RequestMapping("/updateGoodsCoupon")
    @ResponseBody
    public Result<?> updateGoodsCoupon(@RequestBody CouponSubject couponSubject,@Session(SessionKeys.USER) User user) {
        Validator validator = new Validator();
        validator.notNull(couponSubject.getTypePrice(), "请选择价格使用类型");
        validator.notNull(couponSubject.getForMoney(), "请输入抵扣金额");
        validator.notNull(couponSubject.getLowMoney(), "请输入最低使用标准");
        validator.notNull(couponSubject.getEndTime(), "结束时间不能为空");
        validator.notNull(couponSubject.getBeginTime(), "开始时间不能为空");
        validator.isTrue(couponSubject.getBeginTime().before(couponSubject.getEndTime()), "开始时间必须小于结束时间");
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }
        Map<String,Object> param=new HashMap<>();
        param.put("subjectId",couponSubject.getId());
        List<Coupon> list=couponService.list(param);
                for (Coupon c:list){
                    if (c.getSendState()==1) {
                        c.setStartTime(couponSubject.getBeginTime());
                        c.setEndTime(couponSubject.getEndTime());
                        c.setPriceType(couponSubject.getTypePrice());
                        c.setReachMoney(couponSubject.getLowMoney());
                        c.setCouponMoney(couponSubject.getForMoney());
                        c.setUpdateMan(user.getNickname());
                        c.setUpdateTime(new Date());
                        couponService.update(c);
                    }
                }
                couponSubjectService.update(couponSubject);
        return Result.ok();
    }


    /**
     * Description: 获取单个优惠劵
     *
     * @author: sun
     * @Date 下午9:06 2019/5/8
     * @param:
     * @return:
     */

    @RequestMapping("/achieveCoupon")
    @ResponseBody
    public Result<?> achieveCoupon(@RequestBody Coupon coupon) {
        Coupon coupons = couponService.getCouponById(coupon.getId());
        if (coupons == null) {
            return Result.of(Status.ClientError.FORBIDDEN, "未获取到对应的优惠券");
        }
        return Result.ok(coupons);
    }

    /**
     * Description: 优惠券的启用
     *
     * @author: sun
     * @Date 下午9:06 2019/5/8
     * @param:
     * @return:
     */

    @RequestMapping("/open")
    @ResponseBody
    public Result<?> open(@RequestBody @Valid IdParam map) {
        Coupon coupon = new Coupon();
        coupon.setId(map.getId());
        coupon.setState(1);
        couponService.update(coupon);
        return Result.ok();
    }

    /**
     * Description: 优惠券的禁用
     *
     * @author: sun
     * @Date 下午9:07 2019/5/8
     * @param:
     * @return:
     */

    @RequestMapping("/stop")
    @ResponseBody
    public Result<?> stop(@RequestBody @Valid IdParam map) {
        Coupon coupon = new Coupon();
        coupon.setId(map.getId());
        coupon.setState(2);
        couponService.update(coupon);
        return Result.ok();
    }


    /**
     * Description: 删除优惠券
     *
     * @author: sun
     * @Date 下午12:46 2019/3/3
     * @param:
     * @return:
     */

    @RequestMapping({"/deleteCoupon"})
    @ResponseBody
    public Result<?> deleteCoupon(@RequestBody @Valid Map<String, String> param) {
        String id = param.get("orderId");
        List<String> ids = Arrays.asList(id.split(","));
        couponService.delete(ids);
        return Result.ok();
    }


    /**
     * Description: 优惠券信息导出
     *
     * @author: sun
     * @Date 下午12:46 2019/3/3
     * @param:
     * @return:
     */
    @RequestMapping("/exportCoupon")
    public void exportTicket(HttpServletRequest req, HttpServletResponse rep) throws IOException {
        Map<String, Object> params = new HashMap<>();
//        params.put("startCreateTime", (String) req.getParameter("startCreateTime"));
        if (req.getParameter("state") != null && req.getParameter("state") != "") {
            params.put("state", req.getParameter("state"));
        }
        if (req.getParameter("type") != null && req.getParameter("type") != "") {
            params.put("type", req.getParameter("type"));
        }
        if (req.getParameter("subjectId") != null && req.getParameter("subjectId") != "") {
            params.put("subjectId", req.getParameter("subjectId"));
        }

        List<CouponExport> result = couponService.exportCoupon(params);

        OutputStream os = rep.getOutputStream();

        rep.setContentType("application/x-download");
        rep.setHeader("Content-Disposition",
                "attachment;filename=" + DateFormatUtils.format(new Date(), "yyyyMMddHHmmssSSS") + ".xlsx");
        ExportParams param = new ExportParams("优惠劵信息", "优惠劵信息表");
        param.setAddIndex(true);
        param.setType(ExcelType.XSSF);
        Workbook excel = ExcelExportUtil.exportExcel(param, CouponExport.class, result);
        excel.write(os);
        os.flush();
        os.close();
    }


    /**
     * Description:  优惠券领取接口
     * @author: sun
     * @Date 下午10:40 2019/3/27
     * @param:
     * @return:
     */
    @RequestMapping(value = "getCoupon")
    public Result<?> getCoupon(@RequestBody Coupon coupon, @Session("ShopUser") ShopUser user) {
        ShopUser shopUser = shopUserService.get(user.getId());
        if (shopUser==null){
            return Result.of(Status.ClientError.FORBIDDEN, "该类型优惠券已经被兑换完，请等待下次机会");
        }
        //找到可领取的优惠券
        Map<String, Object> param = new HashMap<>(10);
        param.put("couponState", new Object[]{1});
        param.put("type", 1);
        param.put("sendState", "1");
        param.put("state", "1");
        List<Coupon> userCoupons = couponService.list(param);
        if (userCoupons == null || userCoupons.size() == 0) {
            return Result.of(Status.ClientError.FORBIDDEN, "该类型优惠券已经被兑换完，请等待下次机会");
        }
        //判断积分
        CouponSubject couponSubject = couponSubjectService.get(userCoupons.get(0).getSubjectId());
        if (couponSubject==null){
            return Result.of(Status.ClientError.FORBIDDEN, "该类型优惠券已经被兑换完，请等待下次机会");
        }
        if (shopUser.getIntegral()<couponSubject.getIntegral()){
            return Result.of(Status.ClientError.FORBIDDEN, "用户积分不足,无法兑换");
        }
        //类似于Java的可重入锁
        RedisLock redisLock = new RedisLock(redisTemplate);
        boolean canWork = redisLock.lock(ACHIEVE_WORKING_FLAG);
        if (!canWork) {
            return Result.of(Status.ClientError.FORBIDDEN, "请稍后再试");
        }
//接口new的操作，会默认生成一个实现这个接口和方法的对象，实质就是new了一个实现这个接口的对象的
        ExecutorUtils.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    redisLock.lock(ACHIEVE_WORKING_FLAG);
                    couponService.getCoupon(shopUser, coupon,couponSubject.getIntegral());
                } finally {
                    redisLock.deleteKey(ACHIEVE_WORKING_FLAG);
                }
            }
        });
        return Result.ok();
    }


    /**
     * Description: 获取个人优惠券
     *
     * @author: sun
     * @Date 下午10:40 2019/3/27
     * @param:
     * @return:
     */
    @RequestMapping(value = "/achieveCoupons")
    public Result<?> achieveCoupons(@Session("ShopUser") ShopUser user) {
        Map<String, Object> params = new HashMap<>(5);
        params.put("userId", user.getId());
        List<Coupon> list = couponService.listAll(params);
        if (CollectionUtils.isNotEmpty(list)) {
            Iterator<Coupon> iterator = list.iterator();
            Date date = new Date();
            while (iterator.hasNext()) {
                Coupon next = iterator.next();
                if (next.getEndTime().before(date)) {
                    next.setCouponState(3);
                    couponService.update(next);
                }
            }
        }
        return Result.ok(list);
    }


    /**
     * Description: 展示优惠券或者代金券的商品分类
     * @author: sun
     * @Date 下午11:52 2019/5/21
     * @param:
     * @return:
     */
    @RequestMapping(value = "/achieveCategory")
    @ResponseBody
    public Result<?> achieveCategory(@RequestBody Map<String, Object> param, Pagination pagination) {
        Map<String,Object> params=new HashMap<>();
        params.put("subjectId",(String) param.get("id"));
        List<Coupon> list = couponService.list(params);
        if (list.size()<=0){
            return Result.of(Status.ClientError.BAD_REQUEST, "未获取到分类");
        }
        param.put("categoryIds", list.get(0).getIdPath().split(","));
        if (list.get(0).getIdPath() == null || list.get(0).getIdPath().equals("")) {
            return Result.ok(null);
        }
        Pageable<GoodsClassify> classifies = goodsClassifyService.achieveSmallCategory(param, pagination);
        return Result.ok(classifies);
    }


    /**
     * Description: 展示优惠券或者代金券的商品
     *
     * @author: sun
     * @Date 下午11:52 2019/5/21
     * @param:
     * @return:
     */
    @RequestMapping(value = "/achieveGoods")
    @ResponseBody
    public Result<?> achieveGoods(@RequestBody Map<String, Object> param, Pagination pagination) {
        Map<String,Object> params=new HashMap<>();
        params.put("subjectId",(String) param.get("id"));
        List<Coupon> list = couponService.list(params);
        param.put("goodIds", list.get(0).getIdPath().split(","));
        if (list.get(0).getIdPath() == null || "".equals(list.get(0).getIdPath())) {
            return Result.of(Status.ClientError.BAD_REQUEST, "未获取到商品");
        }
        Pageable<ShopGoodsInfo> goods = shopGoodsInfoService.achieveSmallGoods(param, pagination);
        return Result.ok(goods);
    }


    /**
     * Description: 检测代金券是否符合要求
     * @author: sun
     * @Date 下午1:48 2019/7/7
     * @param:
     * @return:
     */
    @RequestMapping("/checkVouchers")
    public Result<?> checkVouchers(@RequestBody Coupon coupon, @Session("ShopUser") ShopUser user) {
        if (coupon.getOrderItems().isEmpty()) {
            return Result.of(Status.ClientError.BAD_REQUEST, "请选择商品");
        }
        Coupon vouchers = null;
        if (coupon.getCouponSeqno() != null && !coupon.getCouponSeqno().equals("")) {
            if (coupon.getPassword() == null || coupon.getPassword().equals("")) {
                return Result.of(Status.ClientError.BAD_REQUEST, "请输入密码");
            }
            Map<String, Object> params = new HashMap<>();
            params.put("couponSeqno", coupon.getCouponSeqno());
            params.put("password", coupon.getPassword());
            vouchers = couponService.getOne(params);
            if (vouchers == null) {
                return Result.of(Status.ClientError.BAD_REQUEST, "代金券劵号或密码错误");
            } else {
                if (new Date().before(vouchers.getStartTime())) {
                    return Result.of(Status.ClientError.BAD_REQUEST, "代金券使用时间还未开始");
                }
                if (new Date().after(vouchers.getEndTime())) {
                    return Result.of(Status.ClientError.BAD_REQUEST, "代金券使用时间已过");
                }
                if (vouchers.getState() == 2) {
                    return Result.of(Status.ClientError.BAD_REQUEST, "代金券被禁用");
                }
                if (vouchers.getCouponState() == null || vouchers.getCouponState() == 2) {

                    return Result.of(Status.ClientError.BAD_REQUEST, "代金券已被使用");
                }
                if (vouchers.getCouponState() == null || vouchers.getCouponState() == 3) {

                    return Result.of(Status.ClientError.BAD_REQUEST, "代金券已过期");
                }
            }
        }

        if (user.getRegisterType() == 2) {
            return Result.of(Status.ClientError.BAD_REQUEST, "经销商会员无需使用代金券");
        }


            return Result.of(Status.ClientError.BAD_REQUEST, "不符合要求");


    }


    /**
     * Description: 检测优惠券是否符合要求
     * @Date 下午1:48 2019/7/7
     * @param:
     * @return:
     */
    @RequestMapping("/checkCoupons")
    public Result<?> createOrder(@RequestBody Map<String,Object> params, @Session("ShopUser") ShopUser user) {
      String  orderMoney= (String) params.get("orderMoney");
        if (StringUtils.isEmpty(orderMoney)) {
            return Result.of(Status.ClientError.BAD_REQUEST, "订单总价为空不能使用优惠券");
        }
        Double allMoney = Double.valueOf(orderMoney);
        //找到个人所有可用优惠券
        Map<String,Object> param=new HashMap<>(5);
        param.put("userId",user.getId());
        param.put("couponState",1);
        List<Coupon> list = couponService.listAll(param);
        List<Coupon> resultList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(list)){
            Iterator<Coupon> iterator = list.iterator();
            Date date = new Date();
            //遍历
            while (iterator.hasNext()){
                Coupon next = iterator.next();
                //找到金额合适的
                if (next.getReachMoney()<=allMoney){
                    if (next.getStartTime().before(date)&& next.getEndTime().after(date)){
                        resultList.add(next);
                    }
                }
            }
        }
        return Result.ok(resultList);
    }
















}