package com.zhiwee.gree.webapi.Controller.ShopUser;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import com.zhiwee.gree.model.BaseInfo.BaseInfo;
import com.zhiwee.gree.model.MemberGrade.MemberGrade;
import com.zhiwee.gree.model.ShopDistributor.DistributorGrade;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.ShopUser.ShopUserCy;
import com.zhiwee.gree.model.ShopUser.ShopUserExport;
import com.zhiwee.gree.model.ShopUser.ShopUserSignLog;
import com.zhiwee.gree.model.User;
import com.zhiwee.gree.model.WxPay.WeChatConfig;
import com.zhiwee.gree.model.card.BookingCards;
import com.zhiwee.gree.model.card.LShopUserCards;
import com.zhiwee.gree.service.BaseInfo.BaseInfoService;
import com.zhiwee.gree.service.MemberGrade.MemberGradeService;
import com.zhiwee.gree.service.ShopDistributor.DistributorGradeService;
import com.zhiwee.gree.service.ShopUser.ShopUserService;
import com.zhiwee.gree.service.ShopUser.ShopUserSignLogService;
import com.zhiwee.gree.service.card.BookingCardsService;
import com.zhiwee.gree.service.card.LShopUserCardsService;
import com.zhiwee.gree.util.JudgeRequestDeviceUtil;
import com.zhiwee.gree.webapi.util.HttpUtil;
import com.zhiwee.gree.webapi.util.SystemControllerLog;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.jeecgframework.poi.excel.ExcelExportUtil;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.enmus.ExcelType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import sun.misc.BASE64Decoder;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.util.IdGenerator;
import xyz.icrab.common.util.KeyUtils;
import xyz.icrab.common.web.annotation.*;
import xyz.icrab.common.web.consts.HeaderNames;
import xyz.icrab.common.web.param.IdParam;
import xyz.icrab.common.web.param.IdsParam;
import xyz.icrab.common.web.session.SessionService;
import xyz.icrab.common.web.support.session.DefaultSession;
import xyz.icrab.common.web.util.SessionUtils;
import xyz.icrab.common.web.util.Validator;
import xyz.icrab.ums.util.consts.SessionKeys;

import javax.annotation.Resource;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.AlgorithmParameters;
import java.security.Security;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * @author sun on 2019/4/30
 */
@RestController
@RequestMapping("/shopUser")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class ShopUserController {
    @Resource
    private LShopUserCardsService lShopUserCardsService;
    @Resource
    private ShopUserService shopUserService;
    @Resource
    private ShopUserSignLogService shopUserSignLogService;
    @Resource
    private RedisTemplate redisTemplate;
    @Resource
    private SessionService sessionService;
    @Resource
    private BaseInfoService baseInfoService;
    @Resource
    private MemberGradeService memberGradeService;
    @Resource
    private BookingCardsService bookingCardsService;
    @Resource
    private DistributorGradeService distributorGradeService;
    @Value("${ZHENCANGKA_URL}")
    private String cardUrl;


    private RestTemplate restTemplate = new RestTemplate();

    private String wxApiCode2Session = "https://api.weixin.qq.com/sns/jscode2session?appid=wx80d7680e6e49f944&secret=dad47801cc15d26b269b6114c029efed&js_code={1}&grant_type=authorization_code";


    @Value("${file.upload.dir}")
    private String fileUploadDir;

    /**
     * Description: 查询所有商城用户
     *
     * @author: sun
     * @Date 上午10:32 2019/4/29
     * @param:
     * @return:
     */
    @RequestMapping({"/page"})
    public Result<Pageable<ShopUser>> page(@RequestBody(required = false) Map<String, Object> param, Pagination pagination) {

        return Result.ok(shopUserService.pageInfo(param, pagination));
    }


    /**
     * Description: 导出商城用户信息
     *
     * @author: sun
     * @Date 下午9:26 2019/5/8
     * @param:
     * @return:
     */
    @RequestMapping({"/exportShopUser"})
    public void exportUser(HttpServletRequest req, HttpServletResponse rsp) throws IOException {
        Map<String, Object> param = new HashMap<>();
        param.put("state", (String) req.getParameter("state"));


        List<ShopUserExport> result = shopUserService.exportShopUser(param);
        OutputStream os = rsp.getOutputStream();
        rsp.setContentType("application/x-download");
        // 设置导出文件名称
        rsp.setHeader("Content-Disposition",
                "attachment;filename=" + DateFormatUtils.format(new Date(), "yyyyMMddHHmmssSSS") + ".xlsx");
        ExportParams params = new ExportParams("微信用户信息表", "微信用户信息表");
        params.setAddIndex(true);
        params.setType(ExcelType.XSSF);
        Workbook excel = ExcelExportUtil.exportExcel(params, ShopUserExport.class, result);
        excel.write(os);
        os.flush();
        os.close();
    }

    /**
     * Description: 商城用户数量的查询
     *
     * @author: sun
     * @Date 上午10:09 2019/7/1
     * @param:
     * @return:
     */
    @RequestMapping({"/shopUserAmount"})
    public Result<?> shopUserAmount(@Session(SessionKeys.USER) User user, @RequestBody(required = false) Map<String, Object> param, Pagination pagination) {
        if (!"1a".equals(user.getId())){
            return null;
        }
        return Result.ok(shopUserService.count());
    }


    /**
     * Description: 今日注册
     *
     * @author: sun
     * @Date 上午10:09 2019/7/1
     * @param:
     * @return:
     */
    @RequestMapping({"/registerAmount"})
    public Result<?> registerAmount(@Session(SessionKeys.USER) User user) {
        if (!"1a".equals(user.getId())){
            return null;
        }
        String fmt = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(fmt);
        String startTime = sdf.format(new Date());
        String endTime = startTime + "23:59:59";
        Map<String, Object> params = new HashMap<>();
        params.put("startTime", startTime);
        params.put("endTime", endTime);
        return Result.ok(shopUserService.registerAmount(params));
    }


    /**
     * Description: 更具用户id获取用户信息
     *
     * @author: sun
     * @Date 上午1:06 2019/5/27
     * @param:
     * @return:
     */
    @RequestMapping("/get")
    public Result<?> getMe(@RequestBody ShopUser shopUser) {
        ShopUser user = shopUserService.achieveByUser(shopUser);
        return Result.ok(user);
    }


    /**
     * Description: 同意用户的申请成为分销员
     *
     * @author: sun
     * @Date 上午1:06 2019/5/27
     * @param:1 申请门店推销员  2 申请公司推销员 3 申请格力工厂推销员 4 未申请
     * 1 商城正常的用户  2 门店分销员  3 格力员工分销员  4 格力工厂分销员
     * @return:
     */
    @RequestMapping("/updateRetail")
    public Result<?> updateRetail(@RequestBody ShopUser shopUser) {
        ShopUser user = shopUserService.get(shopUser.getId());
        if (user.getApplyRetail() == 4) {
            return Result.of(Status.ClientError.BAD_REQUEST, "用户未申请成为分销员");
        }
        if (user.getApplyRetail() == 1) {
            user.setType(2);
        } else if (user.getApplyRetail() == 2) {
            user.setType(3);
        } else if (user.getApplyRetail() == 3) {
            user.setType(4);
        }
        user.setUpdateTime(new Date());
        shopUserService.update(user);
        return Result.ok();
    }


    /**
     * Description: 申请成为分销员接口
     *
     * @author: sun
     * @Date 上午1:06 2019/5/27
     * @param:1 申请门店推销员  2 申请公司推销员 3 申请格力工厂推销员 4 未申请
     * @return:
     */
    @RequestMapping("/applyRetail")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> applyRetail(@RequestBody ShopUser shopUser, @Session("ShopUser") ShopUser user) {
        ShopUser userInfo = shopUserService.get(user.getId());
        userInfo.setApplyRetail(shopUser.getApplyRetail());
        userInfo.setDealerId(shopUser.getDealerId());
        userInfo.setDealerName(shopUser.getDealerName());
        userInfo.setCityCode(shopUser.getCityCode());
        userInfo.setCityName(shopUser.getCityName());
        userInfo.setDealerId(shopUser.getDealerId());
        userInfo.setPartementId(shopUser.getPartementId());
        userInfo.setPhone(shopUser.getPhone());
        userInfo.setUpdateTime(new Date());
        shopUserService.update(userInfo);
        return Result.ok();
    }


    /**
     * Description: 获取当前商城登录用户
     *
     * @author: sun
     * @Date 上午1:06 2019/5/27
     * @param:
     * @return:
     */
    @RequestMapping("/getMe")
//    @PermissionMode(PermissionMode.Mode.White)


    public Result<?> getMe(@Session("ShopUser") ShopUser user, HttpServletRequest request, HttpServletResponse response) {
        ShopUser shopUser = shopUserService.achieveByUser(user);
        return Result.ok(shopUser);
    }


    /**
     * Description: 用户注册
     *
     * @author: sun
     * @Date 下午12:51 2019/5/6
     * @param:
     * @return:
     */
    @PermissionMode(PermissionMode.Mode.White)
    @RequestMapping(value = "/shopUserRegister")
    public Result<?> shopUserRegister(@RequestBody ShopUser shopUser) {
        Validator validator = new Validator();
        validator.notEmpty(shopUser.getUsername(), "用户名不能为空");
        validator.notEmpty(shopUser.getPassword(), "密码不能为空");
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }

        List<BaseInfo> baseInfos = baseInfoService.list();

        Map<String, Object> map = new HashMap<>();
        map.put("username", shopUser.getUsername());
        ShopUser one = shopUserService.getOne(map);
        if (one != null) {
            return Result.of(Status.ClientError.BAD_REQUEST, "用户名已经被使用");
        }
        shopUser.setState(1);
        shopUser.setGender(1);
        shopUser.setCreateTime(new Date());
        shopUser.setApplyRetail(4);
        shopUser.setType(1);
        shopUser.setBalance(0.0);
        shopUser.setRegisterType(1);
        shopUser.setDistributorAmount(0.0);
        //注册默认佣金为0
        shopUser.setActivityCost(0.0);
        shopUser.setAmount(0.0);
        if (baseInfos.isEmpty() || baseInfos.size() > 0) {
            shopUser.setIntegral(0.0);
        } else {
            shopUser.setIntegral(0.0);
        }
        Map<String, Object> params = new HashMap<>();
        params.put("isdefault", 1);
        MemberGrade memberGrade = memberGradeService.getOne(params);
        if (memberGrade != null) {
            shopUser.setGrade(memberGrade.getId());
        } else {
            Map<String, Object> min = new HashMap<>();
            min.put("min", 1);
            min.put("state", 1);
            MemberGrade minMember = memberGradeService.selectMax(min);
            shopUser.setGrade(minMember.getId());
        }
        shopUser.setNickname(shopUser.getUsername());
        shopUser.setId(IdGenerator.objectId());
        shopUserService.save(shopUser);
        return Result.ok();
    }

    /**
     * Description: 后台添加经销商用户
     *
     * @author: sun
     * @Date 下午2:22 2019/6/23
     * @param:
     * @return:
     */
    @RequestMapping(value = "/shopUserAdd")
    public Result<?> shopUserAdd(@RequestBody ShopUser shopUser) {
        Validator validator = new Validator();
        validator.notEmpty(shopUser.getUsername(), "用户名不能为空");
        validator.notEmpty(shopUser.getPassword(), "密码不能为空");
        validator.notNull(shopUser.getDistributorGrade(), "经销商会员等级不能为空");
        validator.notNull(shopUser.getWan(), "区域不能为空");
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }

        List<BaseInfo> baseInfos = baseInfoService.list();

        Map<String, Object> map = new HashMap<>();
        map.put("username", shopUser.getUsername());
        ShopUser one = shopUserService.getOne(map);
        if (one != null) {
            return Result.of(Status.ClientError.BAD_REQUEST, "用户名已经被使用");
        }
        shopUser.setState(1);
        shopUser.setGender(1);
        shopUser.setCreateTime(new Date());
        shopUser.setApplyRetail(4);
        shopUser.setType(1);
        shopUser.setBalance(0.0);
        //经销商类型用户
        shopUser.setRegisterType(2);
//        shopUser.setDistributorAmount(0.0);
        //注册默认佣金为0
        shopUser.setActivityCost(0.0);
        shopUser.setAmount(0.0);
        if (baseInfos.isEmpty() || baseInfos.size() > 0) {
            shopUser.setIntegral(0.0);
        } else {
            shopUser.setIntegral(0.0);
        }

        DistributorGrade distributorGrade = distributorGradeService.get(shopUser.getDistributorGrade().toString());
        shopUser.setDistributorAmount(distributorGrade.getStartAmount());
        shopUser.setNickname(shopUser.getUsername());
        shopUser.setId(IdGenerator.objectId());
        shopUserService.save(shopUser);
        return Result.ok();
    }


    /**
     * Description: 后台更新经销商用户
     *
     * @author: sun
     * @Date 下午2:22 2019/6/23
     * @param:
     * @return:
     */
    @RequestMapping(value = "/distributorUserUpdate")
    public Result<?> distributorUserUpdate(@RequestBody ShopUser shopUser) {
        Validator validator = new Validator();
        validator.notEmpty(shopUser.getId(), "用户id不能为空");
        validator.notEmpty(shopUser.getUsername(), "用户名不能为空");
        validator.notEmpty(shopUser.getPassword(), "密码不能为空");
        validator.notNull(shopUser.getDistributorGrade(), "经销商会员等级不能为空");
        validator.notNull(shopUser.getWan(), "区域不能为空");
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }
        DistributorGrade distributorGrade = distributorGradeService.get(shopUser.getDistributorGrade().toString());
        shopUser.setDistributorAmount(distributorGrade.getStartAmount());
        shopUserService.update(shopUser);
        return Result.ok();
    }


    /**
     * Description:修改用户信息,前台使用
     *
     * @author: sun
     * @Date 下午11:24 2019/6/4
     * @param:
     * @return:
     */
    @PermissionMode(PermissionMode.Mode.White)
    @RequestMapping(value = "/updateShopUser")
    public Result<?> updateShopUser(@RequestBody ShopUser shopUser) {
        Validator validator = new Validator();
        validator.notEmpty(shopUser.getId(), "用户id不能为空");
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }

        shopUserService.update(shopUser);
        return Result.ok();
    }


    /**
     * Description:修改用户名信息
     *
     * @author: sun
     * @Date 下午11:24 2019/6/4
     * @param:
     * @return:
     */
    @PermissionMode(PermissionMode.Mode.White)
    @RequestMapping(value = "/updateShopUserName")
    public Object updateShopUserName(@RequestBody ShopUser shopUser) {
        Validator validator = new Validator();
        validator.notEmpty(shopUser.getUsername(), "用户名不能为空");
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }

        shopUserService.update(shopUser);
        return Result.ok();
    }


    /**
     * Description: 修改密码
     *
     * @author: sun
     * @Date 下午12:51 2019/5/6
     * @param:
     * @return:
     */
    @PermissionMode(PermissionMode.Mode.White)
    @RequestMapping("/modifyPwd")
    public Object modifyPwd(@RequestBody Map<String, Object> params, @Session("ShopUser") ShopUser user) {
        Validator validator = new Validator();
        validator.notEmpty((String) params.get("oldPassword"), "原密码不能为空");
        validator.notEmpty((String) params.get("password"), "新密码不能为空");
        validator.notEmpty((String) params.get("confirmPassword"), "确认密码不能为空");
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }
        ShopUser loadUser = shopUserService.get(user.getId());
        if (!((String) params.get("password")).equals((String) params.get("confirmPassword"))) {
            return Result.of(Status.ClientError.BAD_REQUEST, "俩次密码不一致");
        }
        if (!loadUser.getPassword().equals((String) params.get("oldPassword"))) {
            return Result.of(Status.ClientError.BAD_REQUEST, "原密码不正确");
        }
        ShopUser newUser = new ShopUser();
        newUser.setId(user.getId());
        newUser.setPassword((String) params.get("password"));
        shopUserService.update(newUser);
        return Result.ok("修改密码成功");
    }


    /**
     * Description: pc商城用户账号密码登录
     * 或者手机端的商城登录接口
     *
     * @author: sun
     * @Date 下午5:11 2019/5/8
     * @param:
     * @return:
     */
    @SystemControllerLog(description = "商城用户登录")
    @PermissionMode(PermissionMode.Mode.White)
    @RequestMapping("/login")
    public Result<?> login(@RequestBody(required = false) ShopUser shopUser, HttpServletRequest request, HttpServletResponse response, String callback) {
        Validator validator = new Validator();
        validator.notEmpty(shopUser.getUsername(), "用户名不能为空");
        validator.notEmpty(shopUser.getPassword(), "密码不能为空");
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }
        Map<String, Object> map = new HashMap<>();
        map.put("username", shopUser.getUsername());
        map.put("password", shopUser.getPassword());
        ShopUser user = shopUserService.getOne(map);
        if (user == null) {
            return Result.of(Status.ClientError.BAD_REQUEST, "用户名或密码不正确");
        }
        String token = KeyUtils.getKey();
        xyz.icrab.common.web.session.Session session = new DefaultSession(token);
        session.setAttribute("ShopUser", user);
        sessionService.set(session);


        response.setHeader(HeaderNames.TOKEN, token);
        //cookie

        Cookie cookie = new Cookie(HeaderNames.TOKEN, token);
        cookie.setPath("/");
        response.addCookie(cookie);

        String requestHeader = request.getHeader("user-agent");
        if (JudgeRequestDeviceUtil.isMobileDevice(requestHeader)) {
            System.out.println("使用手机浏览器");
        } else {
            System.out.println("使用web浏览器");
        }
        response.setHeader("refresh", "3;url='http://dbgree.com:8010'");

        return Result.ok(token);


    }


    /**
     * Description: 商城用户手机号密码登录
     *
     * @author: sun
     * @Date 下午5:11 2019/5/8
     * @param:
     * @return:
     */
    @SystemControllerLog(description = "商城用户登录")
    @PermissionMode(PermissionMode.Mode.White)
    @RequestMapping("/phoneLogin")
    public Object phoneLogin(@RequestBody(required = false) ShopUser shopUser, HttpServletRequest request, HttpServletResponse response, String callback) {
        Validator validator = new Validator();
        validator.notEmpty(shopUser.getPhone(), "手机号不能为空");
        validator.notEmpty(shopUser.getCaptcha(), "验证码不能为空");
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }

        String key = getCaptchaKey(shopUser.getPhone());
        String captcha = String.valueOf(redisTemplate.opsForValue().get(key));
        if (StringUtils.isBlank(captcha) || !StringUtils.equals(captcha, shopUser.getCaptcha())) {
            return Result.of(Status.ClientError.FORBIDDEN, "验证码错误或者失效");
        }
        Map<String, Object> map = new HashMap<>();
        map.put("phone", shopUser.getUsername());
        ShopUser user = shopUserService.getOne(map);
        if (user == null) {
            return Result.of(Status.ClientError.BAD_REQUEST, "用户名或密码不正确");


        }
        String token = KeyUtils.getKey();
        xyz.icrab.common.web.session.Session session = new DefaultSession(token);
        session.setAttribute("ShopUser", user);

        String requestHeader = request.getHeader("user-agent");
        if (JudgeRequestDeviceUtil.isMobileDevice(requestHeader)) {
            response.setHeader("refresh", "0;url='http://dbgree.com:8080'");
            System.out.println("使用手机浏览器");
        } else {
            response.setHeader("refresh", "0;url='http://zxx.gxgree.com:8280'");
            System.out.println("使用web浏览器");
        }

        return Result.ok();

    }


    private String getCaptchaKey(String mobile) {
        return "captcha:phone:active:" + mobile;
    }


    /**
     * Description: 商城用户的启用
     *
     * @author: sun
     * @Date 下午9:06 2019/5/8
     * @param:
     * @return:
     */
    @RequestMapping("/open")
    @ResponseBody
    public Result<?> open(@RequestBody @Valid IdParam map) {
        ShopUser user = new ShopUser();
        user.setId(map.getId());
        user.setState(1);
        shopUserService.update(user);
        return Result.ok();
    }

    /**
     * Description: 商城用户的禁用
     *
     * @author: sun
     * @Date 下午9:07 2019/5/8
     * @param:
     * @return:
     */

    @RequestMapping("/stop")
    @ResponseBody
    public Result<?> stop(@RequestBody @Valid IdParam map) {
        ShopUser user = new ShopUser();
        user.setId(map.getId());
        user.setState(2);
        shopUserService.update(user);
        return Result.ok();
    }


    /**
     * Description: 删除用户
     *
     * @author: sun
     * @Date 下午1:38 2019/4/29
     * @param:
     * @return:
     */
    @RequestMapping({"/deleteShopUser"})
    public Result<?> deleteUser(@RequestBody @Valid IdsParam param) {
        shopUserService.deleteUser(param);
        return Result.ok();
    }


    /**
     * Description: 用户头像上传
     *
     * @author: sun
     * @Date 上午10:26 2019/5/28
     * @param:
     * @return:
     */
    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    @ResponseBody
    @PermissionMode(PermissionMode.Mode.White)
    public Result<List> upload(HttpServletRequest request) {
        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver(request.getSession().getServletContext());
        if (multipartResolver.isMultipart(request)) {
            List<Map<String, String>> result = new ArrayList<>();
            MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
            Iterator iter = multiRequest.getFileNames();
            while (iter.hasNext()) {
                List<MultipartFile> files = multiRequest.getFiles(iter.next().toString());
                for (MultipartFile file : files) {
                    if (file != null) {
                        Map<String, String> map = new HashMap<>();
                        String id = KeyUtils.getKey();

                        String suffix = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".") + 1);
                        try {
                            String path = "shopuser" + "/" + KeyUtils.getKey() + "." + suffix;

                            Path target = Paths.get(fileUploadDir, path).normalize();
                            if (!Files.exists(target)) {
                                Files.createDirectories(target);
                            }
                            file.transferTo(target.toFile());
                            map.put("path", path);
                            result.add(map);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            return Result.ok(result);
        } else {
            return Result.of(Status.ClientError.UPGRADE_REQUIRED, "无图片");
        }
    }


    /**
     * Description: 微信公众号登录，获取用户的openID 以及个人信息包括（昵称。性别，省市区，头像地址，unionid（这个信息暂时用户对所有的同一公众号下的产品是一致，需要配置），
     * privilege（用户特权信息，json 数组，如微信沃卡用户为（chinaunicom））
     *
     * @author: sun
     * @Date 上午10:05 2019/6/14
     * @param:
     * @return:
     */
//    @SystemControllerLog(description = "商城用户登录")
    @RequestMapping("/wxLogin")
    @PermissionMode(PermissionMode.Mode.White)
    @ResponseBody
    public Result<?> wxLogin(@RequestBody Map<String, Object> param, HttpServletRequest req, HttpServletResponse res) throws Exception {
        ShopUser user = null;
        //通过code换取access_token 以及openId
        String url = WeChatConfig.access_tokenUrL;
        String nickname = (String) param.get("nickname");
        String appid = WeChatConfig.APPID;
        String secret = WeChatConfig.appSecret;
        String code = (String) param.get("code");
        url = url.replace("AppId", appid).replace("AppSecret", secret).replace("CODE", code);
        String token = loginUtil(url, res, nickname);
        if (token == null) {
            return Result.of(Status.ClientError.BAD_REQUEST, "登录失败请重试");
        }
        return Result.ok(token);
    }


    /**
     * Description: 微信k开放平台扫码登录，获取用户的openID 以及个人信息包括（昵称。性别，省市区，头像地址，unionid（这个信息暂时用户对所有的同一公众号下的产品是一致，需要配置），
     * privilege（用户特权信息，json 数组，如微信沃卡用户为（chinaunicom））
     *
     * @author: sun
     * @Date 上午10:05 2019/6/14
     * @param:
     * @return:
     */
//    @SystemControllerLog(description = "商城用户登录")
    @RequestMapping("/wxScanLogin")
    @PermissionMode(PermissionMode.Mode.White)
    @ResponseBody
    public Result<?> wxScanLogin(@RequestBody Map<String, Object> param, HttpServletRequest req, HttpServletResponse res) throws Exception {
        ShopUser user = null;
        String url = WeChatConfig.wxApiCode2Session;
        String appid = WeChatConfig.MINAPPID;
        String secret = WeChatConfig.AppSecret;
        String code = (String) param.get("code");
        String nickname = (String) param.get("nickname");
        url = url.replace("AppId", appid).replace("AppSecret", secret).replace("CODE", code);
        System.out.println("url:" + url);
        String token = loginUtil(url, res, nickname);
        if (token == null) {
            return Result.of(Status.ClientError.UPGRADE_REQUIRED, "登录失败请重试");
        }
        String[] split = token.split("!");

        Map<String, Object> map = new HashMap<>();
        map.put("token", split[0]);
        map.put("type", split[1]);
        return Result.ok(map);
    }


    /**
     * @author tzj
     * @description 小程序登录获取用户信息
     * @date 2019/12/27 9:17
     */
    @RequestMapping(value = "/userInfo")
    public Result<?> getUserInfo(@Session("ShopUser") ShopUser user) {
        user = shopUserService.achieveByUser(user);
        if (null != user && !"".equals(user.getPhone()) && null != user.getPhone()) {
            return Result.ok(user);
        } else {
            return Result.of(Status.ClientError.BAD_REQUEST, "用户信息待完善");
        }
    }

    @RequestMapping(value = "/userSign")
    public Result<?> wxSign(@Session("ShopUser") ShopUser user) {
        //查询今日是否签到
        String fmt = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(fmt);

        Date today = new Date();

        String todayStr = sdf.format(today);

        Map<String, Object> params = new HashMap<>();
        params.put("userId", user.getId());
        params.put("startTime", todayStr + " 00:00:00");
        params.put("endTime", todayStr + " 23:59:59");
        int count;
        count = shopUserSignLogService.signAmount(params);
        if (count > 0) {
            return Result.of(Status.ClientError.FORBIDDEN, "今日已签到");
        } else {
            //获取连续签到的天数
            Calendar calendar = new GregorianCalendar();
            calendar.setTime(today);
            calendar.add(Calendar.DATE, -1);

            Date yesterday = calendar.getTime();
            String yesterdayStr = sdf.format(yesterday);

            params.clear();
            params.put("userId", user.getId());
            params.put("startTime", yesterdayStr + " 00:00:00");
            params.put("endTime", yesterdayStr + " 23:59:59");
            ShopUserSignLog yestShopUserSignLog = shopUserSignLogService.getOneLog(params);
            int days;
            if (yestShopUserSignLog != null) {
                days = yestShopUserSignLog.getSignDays() + 1;
            } else {
                days = 1;
            }
            ShopUserSignLog shopUserSignLog = new ShopUserSignLog(
                    KeyUtils.getUUID(),
                    user.getId(),
                    today);
            shopUserSignLog.setSignDays(days);
            //奖励积分
            String msg = "签到成功！";
            if (days > 1) {
                msg += "已连续签到" + days + "天。";
            }

            //添加签到记录
            shopUserSignLogService.save(shopUserSignLog);

            return Result.ok(msg);
        }
    }


    /**
     * @author tzj
     * @description 登录工具
     * @date 2019/12/27 14:07
     */
    public String loginUtil(String url, HttpServletResponse res, String nickname) throws Exception {
        ShopUser user = null;
        String openIdText = HttpUtil.sendGet(url);
        ObjectMapper objectMapper = new ObjectMapper();
        Map map = objectMapper.readValue(openIdText, Map.class);
        String openid = "";
        String access_token = "";
        if (map.get("openid") != null) {
            Map<String, Object> params = new HashMap<>();
            openid = map.get("openid").toString();
            params.put("openId", openid);
            user = shopUserService.getOne(params);
            //加入通过openId 查到用户直接登录
            if (user != null) {
                String token = null;
                if (user.getState() == 2) {
                    return null;
                }
                token = KeyUtils.getKey() + "!" + user.getType();
                xyz.icrab.common.web.session.Session session = new DefaultSession(token);
                session.setAttribute("ShopUser", user);
                sessionService.set(session);
                res.setHeader(HeaderNames.TOKEN, token);
                //cookie
                Cookie cookie = new Cookie(HeaderNames.TOKEN, token);
                cookie.setPath("/");
                res.addCookie(cookie);
                return token;
            } else {
                //创建新用户
                user = new ShopUser();
                user.setId(IdGenerator.objectId());
                user.setNickname("系统用户");
                user.setOpenId((String) map.get("openid"));
                user.setDealerId("1");
                user.setCreateTime(new Date());
                user.setApplyRetail(4);
                List<BaseInfo> baseInfos = baseInfoService.list();
                //注册默认佣金为0
                user.setActivityCost(0.0);
                user.setRegisterType(1);
                user.setBalance(0.0);
                user.setDistributorAmount(0.0);
                user.setAmount(0.0);
                user.setType(1);
                user.setState(1);
                if (baseInfos.size() != 0) {
                    user.setIntegral(0.0);
                } else {
                    user.setIntegral(0.0);
                }
                Map<String, Object> params1 = new HashMap<>();
                params1.put("isdefault", 1);
                MemberGrade memberGrade = memberGradeService.getOne(params1);
                if (memberGrade != null) {
                    user.setGrade(memberGrade.getId());
                } else {
                    Map<String, Object> min = new HashMap<>();
                    min.put("min", 1);
                    min.put("state", 1);
                    MemberGrade minMember = memberGradeService.selectMax(min);
                    user.setGrade(minMember.getId());
                }
                shopUserService.save(user);
                String token = KeyUtils.getKey() + "!" + user.getType();
                xyz.icrab.common.web.session.Session session = new DefaultSession(token);
                session.setAttribute("ShopUser", user);
                sessionService.set(session);
                res.setHeader(HeaderNames.TOKEN, token);
                //cookie
                Cookie cookie = new Cookie(HeaderNames.TOKEN, token);
                cookie.setPath("/");
                res.addCookie(cookie);
                return token;
            }
        } else {
            return null;
        }
    }


    /**
     * Description: 绑定手机号
     *
     * @author: sun
     * @Date 下午8:49 2019/6/23
     * @param:
     * @return:
     */
    @PermissionMode(PermissionMode.Mode.White)
    @RequestMapping("/setPhone")
    public Result<?> setPhone(@RequestBody Map<String, Object> params, @Session("ShopUser") ShopUser user, HttpServletRequest req, HttpServletResponse res) {
        String key = getCaptchaKey(params.get("phone").toString());
        String captcha = String.valueOf(redisTemplate.opsForValue().get(key));
        if (StringUtils.isBlank(captcha) || !StringUtils.equals(captcha, params.get("captcha").toString())) {
            return Result.of(Status.ClientError.FORBIDDEN, "验证码错误或者失效");
        }

        Map<String, Object> param = new HashMap<>();
        param.put("phone", params.get("phone"));
        xyz.icrab.common.web.session.Session session = SessionUtils.get(req);
        ShopUser shopUser = shopUserService.getOne(param);
        ShopUser su = shopUserService.get(user.getId());

        if (shopUser == null) {
            su.setPhone((String) params.get("phone"));
            su.setNickname((String) params.get("username"));
            su.setUsername((String) params.get("username"));
        } else {
            if (shopUser.getId().equals(su.getId())) {
                su.setNickname((String) params.get("username"));
                su.setUsername((String) params.get("username"));
            } else {
                return Result.of(Status.ClientError.FORBIDDEN, "该手机号已被其他用户绑定");
            }
        }
        su.setType(2);
        session.setAttribute("ShopUser", su);
        sessionService.set(session);
        shopUserService.update(su);
        return Result.ok();
    }


    /**
     * Description: 修改密码
     *
     * @author: sun
     * @Date 下午12:51 2019/5/6
     * @param:
     * @return:
     */
    @PermissionMode(PermissionMode.Mode.White)
    @RequestMapping("/findPassword")
    public Result<?> findPassword(@RequestBody Map<String, Object> params) {
        Validator validator = new Validator();
        validator.notEmpty((String) params.get("username"), "用户名不能为空");
        validator.notEmpty((String) params.get("password"), "新密码不能为空");
        validator.notEmpty((String) params.get("confirmPassword"), "确认密码不能为空");
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }
        if (!((String) params.get("password")).equals((String) params.get("confirmPassword"))) {
            return Result.of(Status.ClientError.BAD_REQUEST, "俩次密码不一致");
        }
        Map<String, Object> param = new HashMap<>();
        param.put("username", params.get("username"));
        ShopUser one = shopUserService.getOne(param);
        if (one == null) {
            return Result.of(Status.ClientError.BAD_REQUEST, "该用户不存在");
        }
        one.setPassword((String) params.get("password"));
        shopUserService.update(one);
        return Result.ok("修改密码成功");
    }


    /**
     * TODO【微信】登录凭证校验
     *
     * @param code 临时登录凭证
     * @return
     */
    @RequestMapping(value = "/member/weixin")
    @PermissionMode(PermissionMode.Mode.White)
    @ResponseBody
    public String wxCode2Session(String code) {
        System.out.println("wxCode2Session");
        String res = restTemplate.getForObject(wxApiCode2Session, String.class, code);
        System.out.println(res);
        return res;
    }


    /**
     * @return @Session("ShopUer")
     * @author tzj
     * @description 用户展示个人信息二维码
     * @date 2019/12/13 9:27
     */
    @RequestMapping(value = "getUrl")
    public Result<?> getUrl(@Session("ShopUser") ShopUser shopUser) {
        //更新时间
        shopUser.setCheckTime(new Date());
        shopUserService.update(shopUser);

        LShopUserCards lShopUserCards=new LShopUserCards();
        lShopUserCards.setShopUserId(shopUser.getId());
         lShopUserCards = lShopUserCardsService.getOne(lShopUserCards);
        if (lShopUserCards==null){
            return Result.of(Status.ClientError.BAD_REQUEST,"您未购买珍藏卡");
        }else {
            String url = shopUserService.refreshUrl(shopUser.getId(), cardUrl);
            return Result.ok(url);
        }
    }

    /**
     * @author tzj
     * @description 用户展示个人信息二维码
     * @date 2019/12/13 9:27
     */
    @RequestMapping(value = "getUrl2")
    public Result<?> getUrl2(@Session("ShopUser") ShopUser shopUser1) {
        //更新时间
        shopUser1.setCheckTime(new Date());
        shopUserService.update(shopUser1);

        LShopUserCards lShopUserCards=new LShopUserCards();
        lShopUserCards.setShopUserId(shopUser1.getId());
        lShopUserCards = lShopUserCardsService.getOne(lShopUserCards);
        Map<String,Object> param=new HashMap<>(5);
        param.put("userId",shopUser1.getId());
        List<BookingCards> list = bookingCardsService.list(param);
        if (lShopUserCards==null&& CollectionUtils.isEmpty(list)) {
                return Result.of(Status.ClientError.BAD_REQUEST,"未找到相关记录");
        }else {

            String phone = shopUser1.getPhone();
            if (StringUtils.isBlank(phone)) {
                return Result.of(Status.ClientError.BAD_REQUEST, "手机号不能为空");
            }
            ShopUser shopUser = new ShopUser();
            shopUser.setPhone(phone);
            shopUser = shopUserService.getOne(shopUser);
            if (shopUser == null) {
                return Result.of(Status.ClientError.FORBIDDEN, "无此用户");
            }
            if ("1".equals(shopUser.getDealerId())) {
                shopUserService.generateImg(shopUser);
                shopUser.setDealerId("2");
                shopUserService.update(shopUser);
            }
            return Result.ok(shopUser.getId());
        }
    }


    /**
     * @return @Session("ShopUer")
     * @author tzj
     * @description 用户展示个人信息二维码
     * @date 2019/12/13 9:27
     */
    @RequestMapping("/{id}/info")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> dispatchDealer(@PathVariable("id") String id, HttpServletResponse res) throws IOException {
        if (id != null && !"".equals(id)) {
            ShopUser shopUser = new ShopUser();
            shopUser.setId(id);
            shopUser = shopUserService.getOne(shopUser);
            if (shopUser != null) {
                long now2 = System.currentTimeMillis();
                ShopUserCy shopUserCy = new ShopUserCy();
                shopUserCy.setId(id);
                shopUserCy.setTimes(now2);
                return Result.ok(shopUserCy);
            } else {
                return Result.of(Status.ClientError.BAD_REQUEST, "未获取到客户信息");
            }
        } else {
            return Result.of(Status.ClientError.BAD_REQUEST, "未获取到客户信息");
        }
    }


    /**
     * @author tzj
     * @description 微信官方获取手机号更新信息
     * @date 17:34
     */
    @RequestMapping(value = "/updateInfo")
    @ResponseBody
    public Result<?> updateInfo(@Session("ShopUser") ShopUser shopUser, @RequestBody(required = false) Map<String, Object> param) {
        shopUser = shopUserService.get(shopUser.getId());
        if (shopUser == null) {
            return Result.of(Status.ClientError.BAD_REQUEST, "请先授权成为小程序用户");
        }
//            String nickname = (String) param.get("nickname");
            String password = (String) param.get("password");
            String nickname = "系统用户";
            shopUser.setNickname(nickname);
            shopUser.setPassword(password);
            shopUserService.update(shopUser);
            return Result.ok();

    }

    @RequestMapping(value = "/updateNickName")
    @ResponseBody
    public Result<?> updateNickName(@Session("ShopUser") ShopUser shopUser, @RequestBody(required = false) Map<String, Object> param) {
        shopUser = shopUserService.get(shopUser.getId());
        if (shopUser == null) {
            return Result.of(Status.ClientError.BAD_REQUEST, "请先授权成为小程序用户");
        }
            String nickname = (String) param.get("nickname");
        shopUser.setNickname(nickname);
        shopUserService.update(shopUser);
        return Result.ok(shopUser);

    }

    private static boolean isUTF8(String key){
                try {
                        key.getBytes("utf-8");
                         return true;
                     } catch (UnsupportedEncodingException e) {
                         return false;
                    }
             }

    /**
     * @author tzj
     * @description 微信官方获取手机号
     * @date 17:34
     */
    @RequestMapping(value = "/getPhone")
    @ResponseBody
    public Result<?> getPhone(@Session("ShopUser") ShopUser shopUser, HttpServletRequest request, @RequestBody(required = false) Map<String, Object> param) throws Exception {
//        shopUser= shopUserService.getOne(shopUser);
        if (!"".equals(shopUser.getPhone()) && shopUser.getPhone() != null) {
            return Result.of(Status.ClientError.BAD_REQUEST, "用户手机号已经获取到请勿点击");
        }
        String nickname = (String) param.get("nickname");
        String url = WeChatConfig.wxApiCode2Session;
        String appid = WeChatConfig.APPIDFuYang;
        String secret = WeChatConfig.APIKEYFuYang2;
        String code = (String) param.get("loginCode");
        url = url.replace("AppId", appid).replace("AppSecret", secret).replace("CODE", code);
        System.out.println("url:" + url);
        String openIdText = HttpUtil.sendGet(url);
        System.out.println(openIdText);
        ObjectMapper objectMapper = new ObjectMapper();
        Map map = objectMapper.readValue(openIdText, Map.class);
        String session_key = (String) map.get("session_key");
        String openId = (String) map.get("openid");

        byte[] dataByte = com.sun.org.apache.xerces.internal.impl.dv.util.Base64.decode((String) param.get("encryptedData"));
        byte[] keyByte = com.sun.org.apache.xerces.internal.impl.dv.util.Base64.decode(session_key);
        byte[] ivByte = Base64.decode((String) param.get("iv"));

        try {
            int base = 16;
            if (keyByte.length % base != 0) {
                int groups = keyByte.length / base + (keyByte.length % base != 0 ? 1 : 0);
                byte[] temp = new byte[groups * base];
                Arrays.fill(temp, (byte) 0);
                System.arraycopy(keyByte, 0, temp, 0, keyByte.length);
                keyByte = temp;
            }

            // 初始化
            Security.addProvider(new BouncyCastleProvider());
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
//            Cipher cipher = Cipher.getInstance("DES/ECB/NoPadding");
            SecretKeySpec spec = new SecretKeySpec(keyByte, "AES");
            AlgorithmParameters parameters = AlgorithmParameters.getInstance("AES");

            parameters.init(new IvParameterSpec(ivByte));
            cipher.init(Cipher.DECRYPT_MODE, spec, parameters);
            byte[] resultByte = cipher.doFinal(dataByte);
            if (null != resultByte && resultByte.length > 0) {
                String result = new String(resultByte, "utf-8");
                JSONObject jsonObject = JSONObject.parseObject(result);
                String sphone = jsonObject.get("phoneNumber").toString();
                if (sphone != null && !"".equals(sphone)) {
                    shopUser.setPhone(sphone);
                    shopUser.setNickname(nickname);
                    shopUser.setType(2);
                    shopUserService.update(shopUser);
                }
                return Result.ok(sphone);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.of(Status.ClientError.BAD_REQUEST, "请重试");
    }


    @RequestMapping(value = "/getPhone2")
    @ResponseBody
    public Result<?> getPhone2(@Session("ShopUser") ShopUser shopUser, HttpServletRequest request, @RequestBody(required = false) Map<String, Object> param) throws Exception {
        String decryptData = (String) param.get("decryptData");
        String keyInfo = (String) param.get("keyInfo");
        String ivInfo = (String) param.get("ivInfo");
        String s = decryptS5(decryptData, "UTF-8", keyInfo, ivInfo);
        System.out.println(s);
        return null;
    }

    public String decryptS5(String sSrc, String encodingFormat, String sKey, String ivParameter) throws Exception {
        try {
            BASE64Decoder decoder = new BASE64Decoder();
            byte[] raw = decoder.decodeBuffer(sKey);
            SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
            IvParameterSpec iv = new IvParameterSpec(decoder.decodeBuffer(ivParameter));
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
            byte[] myendicod = decoder.decodeBuffer(sSrc);
            byte[] original = cipher.doFinal(myendicod);
            String originalString = new String(original, encodingFormat);
            return originalString;
        } catch (Exception ex) {
            return null;
        }
    }


}
