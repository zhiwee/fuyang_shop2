package com.zhiwee.gree.webapi.Controller.HomePage;
import com.zhiwee.gree.model.GoodsInfo.GoodsBand;
import com.zhiwee.gree.model.HomePage.PageNav;
import com.zhiwee.gree.service.HomePage.PageNavService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.util.KeyUtils;
import xyz.icrab.common.web.annotation.PermissionMode;
import xyz.icrab.common.web.param.IdParam;
import xyz.icrab.common.web.param.IdsParam;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;


/**
 * 导航栏  周广
 */
@RestController
@RequestMapping("/pageNav")
@ResponseBody
public class PageNavController {

    @Autowired
    private PageNavService pageNavService;

    /**
     * 前端使用 接口  周广
     * @return
     */
    @RequestMapping("/getList")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> getList(){
        return  Result.ok(pageNavService.getlist());
    }

    /***
     * 分页查询，周广
     * @param param
     * @param pagination
     * @return
     */
    @RequestMapping("/page")
    public Result<?> page(@RequestBody Map<String, Object> param, Pagination pagination) {
        if (param == null) {
            param = new HashMap<>();
        }
//        param.put("state", new Object[]{1, 2});
        Pageable<PageNav> pageable = pageNavService.getPage(param, pagination);
        return Result.ok(pageable);
    }

    /**
     * 增加  周广
     * @param pageNav
     * @return
     */
    @RequestMapping("/add")
    public Result<?> add(@RequestBody PageNav pageNav){
        pageNav.setId(KeyUtils.getKey());
        pageNavService.save(pageNav);
        return Result.ok();
    }
    /**
     * 更新  周广
     * @param pageNav
     * @return
     */
    @RequestMapping("/update")
    public Result<?> update(@RequestBody PageNav pageNav){
        pageNavService.update(pageNav);
        return Result.ok();
    }

    /***
     * 查询单个  周广
     * @param param
     * @return
     */
    @RequestMapping("/selectOne")
    @ResponseBody
    public Result<?> selectOne(@RequestBody IdParam param){
        return Result.ok(pageNavService.getById(param.getId()));
    }

    /***
     * 启用  周广
     * @param param
     * @return
     */
    @RequestMapping("/enable")
    @ResponseBody
    public Result<?> enable(@RequestBody @Valid IdsParam param){
        for (String id : param.getIds()) {
            PageNav pageNav=new PageNav();
            pageNav.setId(id);
            pageNav.setState(1);
            pageNavService.update(pageNav);
        }
        return Result.ok();
    }

    /***
     * 禁用  周广
     * @param param
     * @return
     */
    @RequestMapping("/disable")
    @ResponseBody
    public Result<?> disable(@RequestBody @Valid IdsParam param){
        for (String id : param.getIds()) {
            PageNav pageNav=new PageNav();
            pageNav.setId(id);
            pageNav.setState(2);
            pageNavService.update(pageNav);
        }
        return Result.ok();
    }


    /***
     * 删除 周广
     * @param param
     * @return
     */
    @RequestMapping("/delete")
    @ResponseBody
    public Result<?> delete(@RequestBody @Valid IdsParam param){
        for (String id : param.getIds()) {
            PageNav pageNav=new PageNav();
            pageNav.setId(id);
            pageNavService.delete(pageNav);
        }
        return Result.ok();
    }

}
