package com.zhiwee.gree.webapi.Controller.fyGoods;


import com.zhiwee.gree.model.FyGoods.PictureInfo;
import com.zhiwee.gree.service.FyGoods.PictureInfoService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.util.KeyUtils;
import xyz.icrab.common.web.annotation.*;
import xyz.icrab.common.web.param.IdParam;
import xyz.icrab.common.web.param.IdsParam;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

/**
 * @author DELL
 */
@RestController
@RequestMapping("/pictureInfo")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class PictureInfoController {
    @Resource
    private PictureInfoService pictureInfoService;

    /**
     * @author tzj
     * @description 获取列表
     * @date 2020/4/26 8:56
     */
    @RequestMapping("/list")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> getFyGoodsList(Pagination pagination) {
        Map<String, Object> param = new HashMap<>(10);
        param.put("state",1);
        Pageable<PictureInfo> list = pictureInfoService.queryGoodsList(param, pagination);
        return Result.ok(list);

    }


    @RequestMapping("/list2")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> getFyGoodsList2(Pagination pagination) {
        Map<String, Object> param = new HashMap<>(10);
        Pageable<PictureInfo> list = pictureInfoService.queryGoodsList(param, pagination);
        return Result.ok(list);

    }
    /**
     * @author tzj
     * @description 新增
     * @date 2020/4/27 9:19
    */
    @RequestMapping("/add")
    public Result<?> add(@RequestBody PictureInfo pictureInfo){
        pictureInfo.setId(KeyUtils.getKey());
        pictureInfoService.save(pictureInfo);
        return Result.ok();
    }

    /**
     * @author tzj
     * @description 修改
     * @date 2020/4/27 9:20
    */
    @RequestMapping("/update")
    public Result<?> update(@RequestBody PictureInfo pictureInfo){
        pictureInfoService.update(pictureInfo);
        return Result.ok();
    }

    /**
     * @author tzj
     * @description 查
     * @date 2020/4/27 9:21
    */
    @RequestMapping("/selectOne")
    @ResponseBody
    public Result<?> selectOne(@RequestBody IdParam param){
        return Result.ok(pictureInfoService.get(param.getId()));
    }

    /**
     * @author tzj
     * @description 用
     * @date 2020/4/27 9:21
    */
    @RequestMapping("/enable")
    @ResponseBody
    public Result<?> enable(@RequestBody @Valid IdsParam param){
        for (String id : param.getIds()) {
            PictureInfo pictureInfo=new PictureInfo();
            pictureInfo.setId(id);
            pictureInfo.setState(1);
            pictureInfoService.update(pictureInfo);
        }
        return Result.ok();
    }

    /**
     * @author tzj
     * @description 否
     * @date 2020/4/27 9:23
    */
    @RequestMapping("/disable")
    @ResponseBody
    public Result<?> disable(@RequestBody @Valid IdsParam param){
        for (String id : param.getIds()) {
            PictureInfo pictureInfo=new PictureInfo();
            pictureInfo.setId(id);
            pictureInfo.setState(2);
            pictureInfoService.update(pictureInfo);
        }
        return Result.ok();
    }







}
