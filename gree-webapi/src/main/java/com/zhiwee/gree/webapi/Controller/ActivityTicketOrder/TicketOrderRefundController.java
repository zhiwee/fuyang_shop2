package com.zhiwee.gree.webapi.Controller.ActivityTicketOrder;


import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.internal.util.AlipaySignature;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.alipay.api.request.AlipayTradeRefundRequest;
import com.alipay.api.response.AlipayTradeRefundResponse;
import com.zhiwee.gree.model.ActivityTicketOrder.TicketOrder;
import com.zhiwee.gree.model.ActivityTicketOrder.TicketOrderExport;
import com.zhiwee.gree.model.ActivityTicketOrder.TicketOrderRefund;
import com.zhiwee.gree.model.Alipay.AliRefund;
import com.zhiwee.gree.model.Alipay.AlipayVo;
import com.zhiwee.gree.model.BaseInfo.BaseInfo;
import com.zhiwee.gree.model.Dealer.DealerInfo;
import com.zhiwee.gree.model.OrderInfo.OrderRefundReason;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.User;
import com.zhiwee.gree.model.WxPay.WeChatConfig;
import com.zhiwee.gree.model.WxPay.WeChatParams;
import com.zhiwee.gree.service.ActivityTicketOrder.ActivityTicketOrderService;
import com.zhiwee.gree.service.ActivityTicketOrder.TicketOrderRefundService;
import com.zhiwee.gree.service.ActivityTicketOrder.TicketOrderService;
import com.zhiwee.gree.service.BaseInfo.BaseInfoService;
import com.zhiwee.gree.service.Dealer.DealerInfoService;
import com.zhiwee.gree.service.ShopUser.ShopUserService;
import com.zhiwee.gree.util.SequenceUtils;
import com.zhiwee.gree.webapi.util.ClientCustomSSL;
import com.zhiwee.gree.webapi.util.PayForUtil;
import com.zhiwee.gree.webapi.util.WeixinPay;
import com.zhiwee.gree.webapi.util.XMLUtil;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.jeecgframework.poi.excel.ExcelExportUtil;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.enmus.ExcelType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import shaded.com.google.gson.Gson;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.util.IdGenerator;
import xyz.icrab.common.web.annotation.EnableGetRequest;
import xyz.icrab.common.web.annotation.EnablePageRequest;
import xyz.icrab.common.web.annotation.PermissionMode;
import xyz.icrab.common.web.annotation.Session;
import xyz.icrab.common.web.param.IdParam;
import xyz.icrab.common.web.util.Validator;
import xyz.icrab.ums.util.consts.SessionKeys;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.*;
import java.util.*;


/**
 * @author sun
 * @since 2019/6/23 15:11
 */
@RestController
@RequestMapping("/ticketOrderRefund")
@EnablePageRequest
@EnableGetRequest
public class TicketOrderRefundController {

 @Autowired
 private TicketOrderRefundService ticketOrderRefundService;

 /**
  * Description: 退款记录查询
  * @author: sun
  * @Date 下午3:50 2019/7/1
  * @param:
  * @return:
  */
    @RequestMapping({"/page"})
    public Result<Pageable<TicketOrderRefund>> page(@RequestBody(required = false) Map<String, Object> param, Pagination pagination) {
        return Result.ok(ticketOrderRefundService.pageInfo(param, pagination));
    }





}
