package com.zhiwee.gree.webapi.Controller.shopBase;

import com.zhiwee.gree.model.Menu;
import com.zhiwee.gree.model.User;
import com.zhiwee.gree.model.enums.UserType;
import com.zhiwee.gree.model.ret.MenuReturnMsg;
import com.zhiwee.gree.service.shopBase.MenuService;
import com.zhiwee.gree.webapi.util.SystemControllerLog;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import xyz.icrab.common.model.*;
import xyz.icrab.common.model.enums.StatusEnum;
import xyz.icrab.common.model.enums.YesOrNo;
import xyz.icrab.common.web.annotation.*;
import xyz.icrab.common.web.param.IdsParam;
import xyz.icrab.common.web.util.KeyUtils;
import xyz.icrab.common.web.util.Validator;
import xyz.icrab.ums.util.consts.SessionKeys;

import javax.validation.Valid;
import java.util.*;

@Controller
@ResponseBody
@RequestMapping("/menu")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class MenuController {
    @Autowired
    private MenuService menuService;

    @SystemControllerLog(description="增加菜单")
    @RequestMapping("/add")
    public Result<?> add(@RequestBody(required = false) Map<String, Object> params) {
        Validator validator = new Validator();
        validator.notEmpty((String) params.get("name"), MenuReturnMsg.MENU_NAME_EMPTY.message());
        validator.notNull(params.get("type"), MenuReturnMsg.MENU_TYPE_EMPTY.message());
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }

        Menu menu = new Menu();
        String key = KeyUtils.getKey();
        menu.setId(key);
        menu.setName((String) params.get("name"));
        menu.setCode((String) params.get("code"));
        menu.setParentname((String) params.get("parentname"));
        if (!Objects.isNull(params.get("checkauth"))) {
            int checkAuth = Objects.equals(YesOrNo.YES.value(), new Integer((String) params.get("checkauth"))) ? YesOrNo.YES.value() : YesOrNo.NO.value();
            menu.setCheckauth(checkAuth);
        }
        menu.setIcon((String) params.get("icon"));
        menu.setNum(StringUtils.isBlank((String)params.get("num")) ? 100 : new Integer((String) params.get("num")));
        menu.setState(StatusEnum.ENABLE.value());
        menu.setType(new Integer((String) params.get("type")));
        menu.setUrl((String) params.get("url"));
        if (StringUtils.isNotBlank((String) params.get("parentid"))) {
            menu.setParentid((String) params.get("parentid"));
            menu.setParentids((String) params.get("parentids") + "," + key);
        } else {
            menu.setParentid("ROOT");
            menu.setParentids("ROOT," + key);
        }

        menuService.save(menu);
        return Result.ok();
    }
    @SystemControllerLog(description="修改菜单")
    @RequestMapping("/update")
    public Result<?> update(@RequestBody(required = false) Map<String, Object> params) {
        Validator validator = new Validator();
        validator.notEmpty((String) params.get("name"), MenuReturnMsg.MENU_NAME_EMPTY.message());
        validator.notNull(params.get("type"), MenuReturnMsg.MENU_TYPE_EMPTY.message());
        validator.notEmpty((String) params.get("id"), MenuReturnMsg.MENU_ID_EMPTY.message());
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }

        Menu menu = new Menu();
        menu.setId((String) params.get("id"));
        menu.setName((String) params.get("name"));
        menu.setCode((String) params.get("code"));
        menu.setParentname((String) params.get("parentname"));
        if (!Objects.isNull(params.get("checkauth"))) {
            menu.setCheckauth(new Integer((String) params.get("checkauth")));
        }
        menu.setIcon((String) params.get("icon"));
        menu.setNum(new Integer((String) params.get("num")));
        menu.setType(new Integer((String) params.get("type")));
        menu.setUrl((String) params.get("url"));
        if (StringUtils.isNotBlank((String) params.get("parentid"))) {
            menu.setParentid((String) params.get("parentid"));
        }
        if (StringUtils.isNotBlank((String) params.get("parentids"))) {
            if(((String) params.get("parentids")).indexOf(menu.getId())<0){
                menu.setParentids((String) params.get("parentids")+","+menu.getId());
            }else {
                menu.setParentids((String) params.get("parentids"));
            }
        }

        menuService.update(menu);
        return Result.ok();
    }
    @SystemControllerLog(description="启用菜单")
    @RequestMapping("/enable")
    public <T> Result<?> enable(@RequestBody @Valid IdsParam map) {
        for (String id : map.getIds()) {
            Menu menu = new Menu();
            menu.setId(id);
            menu.setState(StatusEnum.ENABLE.value());
            menuService.update(menu);
        }
        return Result.ok();
    }
    @SystemControllerLog(description="禁用菜单")
    @RequestMapping("/disable")
    public <T> Result<?> disable(@RequestBody @Valid IdsParam map) {
        for (String id : map.getIds()) {
            Menu menu = new Menu();
            menu.setId(id);
            menu.setState(StatusEnum.DISABLE.value());
            menuService.update(menu);
        }
        return Result.ok();
    }
    @SystemControllerLog(description="查询菜单分页")
    @RequestMapping("/page")
    public Result<?> page(@RequestBody Map<String, Object> param, @Session("user") User user, Pagination pagination) {
        if (user.getType() == UserType.Super) {
            Page<Menu> result = (Page<Menu>) menuService.page(param, pagination);
            return Result.ok(result);
        }
        param.put("userId", user.getId());
        Pageable<Menu> pageable = menuService.getMenuByUser(param, pagination);
        return Result.ok(pageable);
    }
    @SystemControllerLog(description="查询菜单树")
    @RequestMapping("/tree")
    public Result<?> tree(@RequestBody(required = false) Map<String, Object> param, @Session("user") User user) {
    	if(param == null){
    		param = new HashMap<String, Object>();
    	}
        List<Menu> list = null;
        if (user.getType() == UserType.Super) {
            list = menuService.list();
        } else {
            param.put("userId", user.getId());
            list = menuService.getListMenuByUser(param);
        }
        if (list == null) {
            list = new ArrayList<>();
        }
        List<Tree<Menu>> trees = new ArrayList<>(list.size());
        for (Menu menu : list) {
            Tree<Menu> tree = new Tree<Menu>(menu.getId(), menu.getName(), menu.getParentid(), menu);
            trees.add(tree);
        }
        if (trees.size() == 0) {
            Tree<Menu> tree = new Tree<Menu>("", "全部菜单", "", null);
            trees.add(tree);
        }
        return Result.ok(trees);
    }

    /**
     * 添加角色和菜单关系
     *
     * @param user
     * @return
     */
    @SystemControllerLog(description="增加角色菜单")
    @RequestMapping("/addMenuRoleRef")
    public Result<?> addMenuRoleRef(@RequestBody(required = false) Map<String, Object> params, @Session("user") User user) {
        Validator validator = new Validator();
        validator.notEmpty((String) params.get("menuids"), MenuReturnMsg.MENU_ID_EMPTY.message());
        validator.notEmpty((String) params.get("roleid"), MenuReturnMsg.ROLE_ID_EMPTY.message());
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }

        String menuIds = (String) params.get("menuids");
        Map<String, Object> param = new HashMap<>();
        param.put("menuIds", Arrays.asList(menuIds.split(",")));
        param.put("isSuper", user.isSuperUser());
        param.put("userId", user.getId());
        param.put("roleId", (String) params.get("roleid"));

        menuService.addMenuRoleRef(param);
        return Result.ok();
    }
    @SystemControllerLog(description="增加用户菜单")
    @RequestMapping("/addMenuUserRef")
    public Result<?> addMenuUserRef(@RequestBody(required = false) Map<String, Object> params, @Session("user") User user) {
        Validator validator = new Validator();
        validator.notEmpty((String) params.get("menuids"), MenuReturnMsg.MENU_ID_EMPTY.message());
        validator.notEmpty((String) params.get("userid"), MenuReturnMsg.USER_ID_EMPTY.message());
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }

        String menuIds = (String) params.get("menuids");
        Map<String, Object> param = new HashMap<>();
        param.put("menuIds", Arrays.asList(menuIds.split(",")));
        param.put("isSuper", user.isSuperUser());
        param.put("currentUserId", user.getId());
        param.put("userId", (String) params.get("userid"));

        menuService.addMenuUserRef(param);
        return Result.ok();
    }

    /**
     * 获取用户所有菜单，包含用户对应角色菜单
     * @param params
     * @param user
     * @return
     */
    @SystemControllerLog(description="查询用户菜单列表-包含角色")
    @RequestMapping("/getMenuByUser")
    public Result<?> getMenuByUser(@RequestBody(required = false) Map<String, Object> params, @Session("user") User user) {
        String userId = (String) params.get("userid");
        Validator validator = new Validator();
        validator.notEmpty((String) params.get("userid"), MenuReturnMsg.USER_ID_EMPTY.message());
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }
        Map<String, Object> param = new HashMap<>();
        param.put("userId", userId);
        List<Menu> menus = menuService.getListMenuByUser(param);

        return Result.ok(menus);
    }

    /**
     * 获取用户对应菜单，不包含用户对应角色菜单
     * @param params
     * @param user
     * @return
     */
    @SystemControllerLog(description="查询用户菜单列表-不包含角色")
    @RequestMapping("/getMenuByUserid")
    public Result<?> getMenuByUserId(@RequestBody(required = false) Map<String, Object> params, @Session("user") User user) {
        String userId = (String) params.get("userId");
        Validator validator = new Validator();
        validator.notEmpty(userId, MenuReturnMsg.USER_ID_EMPTY.message());
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }
        List<Menu> menus = menuService.getMenuByUserId(params);
        return Result.ok(menus);
    }
    @SystemControllerLog(description="查询角色菜单列表")
    @RequestMapping("/getMenuByRoleid")
    public Result<?> getMenuByRoleId(@RequestBody(required = false) Map<String, Object> params, @Session("user") User user) {
        Validator validator = new Validator();
        validator.notEmpty((String) params.get("roleid"), MenuReturnMsg.USER_ID_EMPTY.message());
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }
        List<Menu> menus = menuService.getMenuByRoleId((String) params.get("roleid"));
        return Result.ok(menus);
    }
    @SystemControllerLog(description="查询当前用户菜单列表")
    @RequestMapping("/getSelfMenu")
    public Result<?> getSelfMenu(@Session("user") User user) {
        Map<String, Object> param = new HashMap<>();
        param.put("userId", user.getId());
        List<Menu> menus = menuService.getListMenuByUser(param);
        return Result.ok(menus);
    }


//    @SystemControllerLog(description="查询当前用户缓存菜单列表")
        @RequestMapping("/menus")
        public Result<List<Menu>> getMenus(@xyz.icrab.common.web.annotation.Session(SessionKeys.MENUS) List<Menu> menus){
        if(CollectionUtils.isEmpty(menus)) {
            return Result.ok(Collections.emptyList());
        }

        Map<String, Menu> menuIndex = new HashMap<>();
        for(Menu menu : menus){
            menuIndex.put(menu.getId(), menu);
        }
        Iterator<Menu> iter = menus.iterator();
        while (iter.hasNext()) {
            Menu menu = iter.next();
            if(!"ROOT".equals(menu.getParentid())){
                Menu parent = menuIndex.get(menu.getParentid());
                if(parent != null){
                    parent.getChildren().add(menu);
                }
                iter.remove();
            }
        }
        return Result.ok(menus);
    }
}
