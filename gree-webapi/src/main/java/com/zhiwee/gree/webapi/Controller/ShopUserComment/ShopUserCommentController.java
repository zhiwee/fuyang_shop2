package com.zhiwee.gree.webapi.Controller.ShopUserComment;


import com.zhiwee.gree.model.GoodsInfo.GoodsBand;
import com.zhiwee.gree.model.GoodsInfo.ShopGoodsInfo;
import com.zhiwee.gree.model.OrderInfo.Order;
import com.zhiwee.gree.model.ShopDistributor.DistributorOrder;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.model.ShopUserComment.CommentPicture;
import com.zhiwee.gree.model.ShopUserComment.ShopUserComment;

import com.zhiwee.gree.service.GoodsInfo.ShopGoodsInfoService;
import com.zhiwee.gree.service.OrderInfo.OrderService;
import com.zhiwee.gree.service.ShopDistributor.DistributorOrderService;
import com.zhiwee.gree.service.ShopUserComment.CommentPictureService;
import com.zhiwee.gree.service.ShopUserComment.ShopUserCommentService;
import com.zhiwee.gree.webapi.util.SystemControllerLog;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;

import xyz.icrab.common.util.IdGenerator;
import xyz.icrab.common.util.KeyUtils;

import xyz.icrab.common.web.annotation.*;
import xyz.icrab.common.web.param.IdParam;
import xyz.icrab.common.web.param.IdsParam;
import xyz.icrab.common.web.util.SessionUtils;
import xyz.icrab.common.web.util.Validator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author sun on 2019/5/6
 */
@RestController
@RequestMapping("/shopUserComment")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class ShopUserCommentController {
    @Autowired
    private ShopUserCommentService shopUserCommentService;

    @Autowired
    private CommentPictureService commentPictureService;

    @Autowired
    private ShopGoodsInfoService shopGoodsInfoService;

    @Autowired
    private OrderService orderService;


    @Autowired
    private DistributorOrderService distributorOrderService;


    @Value("${file.upload.dir}")
    private String fileUploadDir;

    @Value("${HTTP_PICTUREURL}")
    private String httpurl;

    /**
     * Description: 添加评论
     * @author: sun
     * @Date 下午11:26 2019/5/9
     * @param:
     * @return:
     */
    @RequestMapping(value = "/addComment")
    public Result<?> achieveShopUserrAddress(@RequestBody ShopUserComment shopUserComment, @Session("ShopUser") ShopUser shopUser){
        Validator validator = new Validator();
        validator.notEmpty(shopUserComment.getOrderId(), "订单id不能为空");
        validator.notEmpty(shopUserComment.getOrderItemId(), "订单明细id不能为空");
        validator.notEmpty(shopUserComment.getComment(), "评论内容不能为空");
        validator.notEmpty(shopUserComment.getGoodsId(), "商品id不能为空");
        validator.notNull(shopUserComment.getLevel(), "评论等级不能为空");
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }
        Order order = orderService.get(shopUserComment.getOrderId());


        Map<String,Object> parmas = new HashMap<>();
        parmas.put("orderId",shopUserComment.getOrderId());
        parmas.put("orderItemId",shopUserComment.getOrderItemId());
        List<DistributorOrder> distributorOrders = distributorOrderService.list(parmas);
        if(distributorOrders.isEmpty() && distributorOrders.size() > 0){
            for(DistributorOrder distributorOrder : distributorOrders){
                if(distributorOrder.getDeliveryState() != 1 || distributorOrder.getState() != 1){
                    return Result.of(Status.ClientError.BAD_REQUEST, "该订单的商品未发货不能评价或者该订单的已经无法评价");
                }
                distributorOrder.setDeliveryState(3);
                distributorOrder.setDeliveryTime(new Date());
                distributorOrder.setCommentState(2);

            }
        }

        ShopGoodsInfo shopGoodsInfo = shopGoodsInfoService.get(shopUserComment.getGoodsId());
        shopUserComment.setId(IdGenerator.objectId());
        shopUserComment.setCreateTime(new Date());
        shopUserComment.setSpecId(shopGoodsInfo.getSpecId());
        shopUserComment.setUserId(shopUser.getId());
        shopUserComment.setPhone(shopUser.getPhone());
        shopUserComment.setState(1);
        //更新为已评价
        String[] pictures = shopUserComment.getPictures().split(",");
        List<CommentPicture> commentPictures = new ArrayList<>();
        if(pictures.length>0){
            for(int i = 0;i< pictures.length;i++){
                CommentPicture commentPicture = new CommentPicture();
                commentPicture.setId(IdGenerator.objectId());
                commentPicture.setCommentId(shopUserComment.getId());
                commentPicture.setCreateTime(new Date());
                commentPicture.setPicture(pictures[i]);
                commentPictures.add(commentPicture);
            }

        }
        shopUserCommentService.saveAndUpdate(order,shopUserComment,commentPictures,shopGoodsInfo,distributorOrders);
        return Result.ok();
    }


    /**
 * Description: 获取用户所有的评论
 * @author: sun
 * @Date 下午5:31 2019/5/9
 * @param:
 * @return:
 */
    @RequestMapping({"/achieveComment"})
    public Result<Pageable<ShopUserComment>> page(@RequestBody(required = false) Map<String, Object> param, Pagination pagination) {
        return Result.ok(shopUserCommentService.achieveComment(param, pagination));
    }

    /**
     * Description: 获取商品的的评论记录
     * @author: sun
     * @Date 下午4:17 2019/5/9
     * @param:
     * @return:
     */
    @PermissionMode(PermissionMode.Mode.White)
    @RequestMapping("/achieveCommentByGoodsId")
    public Object achieveComment(@RequestBody @Valid IdParam map, Pagination pagination, HttpServletRequest req, HttpServletResponse res) {
        xyz.icrab.common.web.session.Session  session = SessionUtils.get(req);
        List<String> ids =Arrays.asList(map.getId().split(","));
        Pageable<ShopUserComment> shopUserComments=shopUserCommentService.achieveCommentByGoodsId(ids,pagination, session);
        return Result.ok(shopUserComments);
    }



    /**
     * Description: 评论的启用
     * @author: sun
     * @Date 下午9:06 2019/5/8
     * @param:
     * @return:
     */
    @SystemControllerLog(description="评论的启用")
    @RequestMapping("/open")
    @ResponseBody
    public  Result<?> open(@RequestBody @Valid IdParam map) {
        ShopUserComment shopUserComment = new ShopUserComment();
        shopUserComment.setId(map.getId());
        shopUserComment.setState(1);
        shopUserCommentService.update(shopUserComment);
        return Result.ok();
    }

    /**
     * Description: 评论的禁用
     * @author: sun
     * @Date 下午9:07 2019/5/8
     * @param:
     * @return:
     */
    @SystemControllerLog(description="评论的禁用")
    @RequestMapping("/stop")
    @ResponseBody
    public  Result<?> stop(@RequestBody @Valid IdParam map) {
        ShopUserComment shopUserComment = new ShopUserComment();
        shopUserComment.setId(map.getId());
        shopUserComment.setState(2);
        shopUserCommentService.update(shopUserComment);
        return Result.ok();
    }

    /***
     * 启用  周广
     * @param param
     * @return
     */
    @RequestMapping("/enableList")
    @ResponseBody
    public Result<?> enable(@RequestBody @Valid IdsParam param){
        for (String id : param.getIds()) {
            ShopUserComment shopUserComment=new ShopUserComment();
            shopUserComment.setId(id);
            shopUserComment.setState(1);
            shopUserCommentService.update(shopUserComment);
        }
        return Result.ok();
    }

    /***
     * 禁用  周广
     * @param param
     * @return
     */
    @RequestMapping("/disableList")
    @ResponseBody
    public Result<?> disable(@RequestBody @Valid IdsParam param){
        for (String id : param.getIds()) {
            ShopUserComment shopUserComment=new ShopUserComment();
            shopUserComment.setId(id);
            shopUserComment.setState(2);
            shopUserCommentService.update(shopUserComment);
        }
        return Result.ok();
    }


    /**
     * Description: 用户评论图片上传
     * @author: sun
     * @Date 上午10:26 2019/5/28
     * @param:
     * @return:
     */
    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    @ResponseBody
    @PermissionMode(PermissionMode.Mode.White)
    public Result<List> upload(HttpServletRequest request) throws IOException {
        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver(request.getSession().getServletContext());
        if (multipartResolver.isMultipart(request)) {
            List<String> result = new ArrayList<>();
            MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
            Iterator iter = multiRequest.getFileNames();
            while (iter.hasNext()) {
                List<MultipartFile> files = multiRequest.getFiles(iter.next().toString());
                for (MultipartFile file : files) {
                    if (file != null) {
                        Map<String, String> map = new HashMap<>();
                        String id = KeyUtils.getKey();

                        String suffix = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".") + 1);
                        try {
                            String path = "shopcomment" + "/" + KeyUtils.getKey() + "." + suffix;

                            Path target = Paths.get(fileUploadDir, path).normalize();
                            if (!Files.exists(target)) {
                                Files.createDirectories(target);
                            }
                            file.transferTo(target.toFile());
                            Runtime.getRuntime().exec("chmod 777 " + fileUploadDir+path);
                            result.add(httpurl+path);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            return Result.ok(result);
        } else {
            return Result.of(Status.ClientError.UPGRADE_REQUIRED, "无图片");
        }
    }



/**
 * Description: 评论总数
 * @author: sun
 * @Date 下午4:53 2019/7/5
 * @param:
 * @return:
 */
    @RequestMapping({"/commentAmount"})
    public Result<?> commentAmount(@RequestBody(required = false) Map<String, Object> param) {
        return Result.ok(shopUserCommentService.count());
    }





/**
 * Description: 今日添加评论数
 * @author: sun
 * @Date 下午4:56 2019/7/5
 * @param:
 * @return:
 */
    @RequestMapping({"/todayAmount"})
    public Result<?> todayAmount() {
        String fmt = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(fmt);
        String startTime = sdf.format(new Date());
        String endTime = startTime+"23:59:59";
        Map<String,Object> params = new HashMap<>();
        params.put("startTime",startTime);
        params.put("endTime",endTime);
        return Result.ok(shopUserCommentService.todayAmount(params));
    }





    /**
     * Description: 获取地址
     * @author: sun
     * @Date 下午11:26 2019/5/9
     * @param:
     * @return:
     */
    @RequestMapping(value = "/achieveMyComment")
    public Result<?> achieveComment( @Session("ShopUser") ShopUser shopUser,Pagination pagination){
        Map<String,Object> param =  new HashMap<>();
        param.put("userId",shopUser.getId());
        Pageable<ShopUserComment>  shopUserCommentPageable  = shopUserCommentService.achieveMyComment(param,pagination);
        return Result.ok(shopUserCommentPageable);
    }



    /**
     * Description: 修改评论
     * @author: sun
     * @Date 下午11:26 2019/5/9
     * @param:
     * @return:
     */
    @RequestMapping(value = "/updateMyComment")
    public Result<?> achieveComment(@RequestBody ShopUserComment shopUserComment){
        Validator validator = new Validator();
        validator.notEmpty(shopUserComment.getId(), "标识不能为哦空");
        validator.notEmpty(shopUserComment.getComment(), "评论内容不能为空");
        validator.notNull(shopUserComment.getLevel(), "评论等级不能为空");
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());

        }
        shopUserComment.setUpdateTime(new Date());
        shopUserCommentService.update(shopUserComment);
        return Result.ok();

    }





}