package com.zhiwee.gree.webapi.Controller.Invoice;

import com.zhiwee.gree.model.Invoice.Invoice;
import com.zhiwee.gree.model.OrderInfo.Order;
import com.zhiwee.gree.model.ShopUser.ShopUser;
import com.zhiwee.gree.service.Invoice.InvoiceService;
import com.zhiwee.gree.service.OrderInfo.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.solr.core.query.Update;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.model.Status;
import xyz.icrab.common.util.IdGenerator;
import xyz.icrab.common.web.annotation.*;
import xyz.icrab.common.web.param.IdParam;

import javax.validation.Valid;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import xyz.icrab.common.web.util.Validator;

/**
 * Created by jick on 2019/7/16.
 */
@RestController
@RequestMapping("/invoice")
@EnableListRequest
@EnablePageRequest
@EnableGetRequest
@EnableDeleteRequest
public class InvoiceController {

    @Autowired
    private InvoiceService invoiceService;
    @Autowired
    private  OrderService orderService;

    /**
     * 发票基本信息录入
     * @param
     * @return
     */
   // @PermissionMode(PermissionMode.Mode.White)
    @RequestMapping(value = "/save")
    public Result<?> invoiceInfoAdd(@Session("ShopUser") ShopUser user,@RequestBody(required = false)  Invoice invoice  ){
   // public Result<?> invoiceInfoAdd(@RequestBody(required = false)  Invoice invoice  ){
     //@Session("ShopUser") ShopUser user,
        //通过orderNum查询订单，判读是否是登陆状态，同时获取订单号
        if(null == invoice.getIdentifyNum()||"" == invoice.getIdentifyNum()||null == invoice.getCompanyName()||""== invoice.getCompanyName()||
              null == invoice.getIdentifyNum()||"" == invoice.getIdentifyNum()|| null == invoice.getBankName()||""== invoice.getBankName()
           || null == invoice.getAccount()||"" == invoice.getAccount()){
            return Result.of(Status.ClientError.FORBIDDEN, "基本信息不完整");
        }


        Map<String, Object> param = new HashMap<>();
        param.put("orderNum",invoice.getOrderNum());

        //判断发票信息是否已经生成了
        Invoice invoice2 = invoiceService.getOne(param);
        if(null != invoice2){
            return Result.of(Status.ClientError.FORBIDDEN, "发票已经生成过");
        }


        //Order  orderInfo =  orderService.getOne(param);
         List<Order> orderInfo = orderService.list(param);
        if(orderInfo.size() == 0){
            return Result.of(Status.ClientError.FORBIDDEN, "订单流水号错误");
        }
        if(2!=orderInfo.get(0).getOrderState()){
            return Result.of(Status.ClientError.FORBIDDEN, "订单未付款");
        }
        Date date = new Date();

        Invoice in = new Invoice();
        in.setUserId(user.getId());
        in.setId(IdGenerator.objectId());
        //in.setUserId("01010101");
        in.setCompanyName(invoice.getCompanyName());
        in.setIdentifyNum(invoice.getIdentifyNum());
        in.setBankName(invoice.getBankName());
        in.setAccount(invoice.getAccount());
        in.setOrderId(orderInfo.get(0).getId());
        in.setType(invoice.getType());
        in.setCreateTime(date);
        in.setOrderNum(invoice.getOrderNum());
        invoiceService.save(in);
         return  Result.ok();
    }

    /**
     * 发票信息查询，前台页面展示
     * @param
     * @return
     */

   // @PermissionMode(PermissionMode.Mode.White)
    @RequestMapping("/serchInvoice")
    public  Result<?> invoiceInfo(@Session("ShopUser") ShopUser user){
        // public  Result<?> invoiceInfo(){
        Map<String, Object> param = new HashMap<>();
         param.put("userId",user.getId());
        // param.put("userId","01010101");
        List<Invoice>  invoices = invoiceService.queryInvoice(param);
        return  Result.ok(invoices);
    }


    /**
     * 查询发票表中的基本信息，提供给后台使用
     * @return
     */
   // @PermissionMode(PermissionMode.Mode.White)
    @RequestMapping("/invoiceBaseInfo")
    public  Result<?> invoiceBaseInfo(Pagination pagination, @RequestBody(required =false)Map<String,Object> map){
        // public  Result<?> invoiceInfo(){
       // param.put("userId",user.getId());
         //param.put("userId","01010101");
        Pageable<Invoice>  invoices = invoiceService.queryBaseInvoice(map,pagination);
        return  Result.ok(invoices);
    }


    /**
     * 发票基本信息回显，
     */
 //   @PermissionMode(PermissionMode.Mode.White)
    @RequestMapping("/BaseInfoById")
    // public  Result<?> invoiceBaseInfo(@Session("ShopUser") ShopUser user){
    public  Result<?> invoiceBaseInfoById(@RequestBody Invoice invoice){
        // public  Result<?> invoiceInfo(){
        Map<String, Object> param = new HashMap<>();
         param.put("id",invoice.getId());
        //param.put("userId","01010101");
        Invoice  in = invoiceService.queryBaseInvoiceById(param);
        return  Result.ok(in);
    }

    /**
     *根据id查询发票信息，用于数据回显
     */
    @RequestMapping("/get")
    @ResponseBody
    public Result<?> getMe(@RequestBody @Valid IdParam map){
      //  ActivityPage activityPage = activityPageService.get(map.getId());
        Invoice  invoice = invoiceService.get(map.getId());
        return Result.ok(invoice);
    }


    /**
     * 发票信息更新
     */
   @RequestMapping("/updateInvoice")
    @ResponseBody
    public Result<?> updateActivityPage(@RequestBody Invoice invoice) {
        Validator validator = new Validator();
       // validator.notNull(invoice.getId(), "表示不能为空");
        //validator.notNull(invoice.getUserId(), "用户不能为空");
        validator.notNull(invoice.getCompanyName(), "公司名称不能为空");
        validator.notNull(invoice.getIdentifyNum(), "公司识别号不能为空");
       validator.notNull(invoice.getBankName(), "开户银行名称不能为空");
       validator.notNull(invoice.getAccount(), "银行帐号不能为空");
      // validator.notNull(invoice.getOrderId(), "不能为空");
       validator.notNull(invoice.getType(), "类型不能为空");
      // validator.notNull(invoice.getCreateTime(), "名称不能为空");
      // validator.notNull(invoice.getOrderNum(), "不能为空");
       //validator.notNull(invoice.getType(), "类型不能为空");
       invoice.setUpdateTime(new Date());
        if (validator.isError()) {
            return Result.of(Status.ClientError.BAD_REQUEST, validator.getMessage());
        }
      //  activityPageService.update(activityPage);
       invoiceService.update(invoice);
        return Result.ok();
    }


















}
