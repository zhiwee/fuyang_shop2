package com.zhiwee.gree.webapi.Controller.HomePage;
import com.zhiwee.gree.model.HomePage.PageSearch;
import com.zhiwee.gree.service.HomePage.PageSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import xyz.icrab.common.model.Pageable;
import xyz.icrab.common.model.Pagination;
import xyz.icrab.common.model.Result;
import xyz.icrab.common.util.KeyUtils;
import xyz.icrab.common.web.annotation.PermissionMode;
import xyz.icrab.common.web.param.IdParam;
import xyz.icrab.common.web.param.IdsParam;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

/**
 * pc  搜索关键字  周广
 */
@RestController
@RequestMapping("/pageSearch")
@ResponseBody
public class PageSearchController {

    @Autowired
    private PageSearchService pageSearchService;

    /**
     * 前端使用  list  周广
     * @returns
     */
    @RequestMapping("/getList")
    @PermissionMode(PermissionMode.Mode.White)
    public Result<?> getList(){
        return  Result.ok(pageSearchService.getList());
    }


    /***
     * 分页查询，周广
     * @param param
     * @param pagination
     * @return
     */
    @RequestMapping("/page")
    public Result<?> page(@RequestBody Map<String, Object> param, Pagination pagination) {
        if (param == null) {
            param = new HashMap<>();
        }
//        param.put("state", new Object[]{1, 2});
        Pageable<PageSearch> pageable = pageSearchService.getPage(param, pagination);
        return Result.ok(pageable);
    }

    /**
     * 增加  周广
     * @param pageSearch
     * @return
     */
    @RequestMapping("/add")
    public Result<?> add(@RequestBody PageSearch pageSearch){
        pageSearch.setId(KeyUtils.getKey());
        pageSearchService.save(pageSearch);
        return Result.ok();
    }
    /**
     * 更新  周广
     * @param pageSearch
     * @return
     */
    @RequestMapping("/update")
    public Result<?> update(@RequestBody PageSearch pageSearch){
        pageSearchService.update(pageSearch);
        return Result.ok();
    }

    /***
     * 查询单个  周广
     * @param param
     * @return
     */
    @RequestMapping("/selectOne")
    @ResponseBody
    public Result<?> selectOne(@RequestBody IdParam param){

        return Result.ok(pageSearchService.getById(param.getId()));
    }

    /***
     * 启用  周广
     * @param param
     * @return
     */
    @RequestMapping("/enable")
    @ResponseBody
    public Result<?> enable(@RequestBody @Valid IdsParam param){
        for (String id : param.getIds()) {
            PageSearch pageSearch=new PageSearch();
            pageSearch.setId(id);
            pageSearch.setState(1);
            pageSearchService.update(pageSearch);
        }
        return Result.ok();
    }

    /***
     * 禁用  周广
     * @param param
     * @return
     */
    @RequestMapping("/disable")
    @ResponseBody
    public Result<?> disable(@RequestBody @Valid IdsParam param){
        for (String id : param.getIds()) {
            PageSearch pageSearch=new PageSearch();
            pageSearch.setId(id);
            pageSearch.setState(2);
            pageSearchService.update(pageSearch);
        }
        return Result.ok();
    }


    /***
     * 删除 周广
     * @param param
     * @return
     */
    @RequestMapping("/delete")
    @ResponseBody
    public Result<?> delete(@RequestBody @Valid IdsParam param){
        for (String id : param.getIds()) {
            PageSearch pageSearch=new PageSearch();
            pageSearch.setId(id);
            pageSearchService.delete(pageSearch);
        }
        return Result.ok();
    }
}
