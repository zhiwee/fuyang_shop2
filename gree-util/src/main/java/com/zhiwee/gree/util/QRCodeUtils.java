package com.zhiwee.gree.util;

import com.google.zxing.*;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import org.springframework.util.Base64Utils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Hashtable;

public class QRCodeUtils {
    public static void encodeToFile(String content, String path,int width,int height) throws IOException, WriterException {
        Hashtable hints = new Hashtable();
        hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.M);
        hints.put(EncodeHintType.MARGIN, 2);
        BitMatrix bitMatrix = new MultiFormatWriter().encode(content, BarcodeFormat.QR_CODE, width, height, hints);
        File file = new java.io.File(path);
        MatrixToImageWriter.writeToPath(bitMatrix, "png", file.toPath());
    }

    /**
     * 生成二维码base64加密字符串
     * @param content
     * @return
     * @throws IOException
     * @throws WriterException
     */
    public static String encodeToString(String content, int width, int height) throws IOException, WriterException {
        byte[] bytes = encodeToBytes(content,width,height);
        return Base64Utils.encodeToString(bytes);
    }

    public static byte[] encodeToBytes(String content, int width, int height) throws IOException, WriterException {
        ByteArrayOutputStream outputStream = null;
        try {
            BufferedImage image = encode(content,width,height);
            outputStream = new ByteArrayOutputStream();
            ImageIO.write(image, "png", outputStream);
            byte[] bytes = outputStream.toByteArray();
            return bytes;
        } finally {
            if (outputStream != null) {
                outputStream.close();
            }
        }
    }

    public static BufferedImage encode(String content,int width,int height) throws IOException, WriterException {
        String format = "png";
        Hashtable hints = new Hashtable();
        hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.M);
        hints.put(EncodeHintType.MARGIN, 2);
        BitMatrix bitMatrix = new MultiFormatWriter().encode(content, BarcodeFormat.QR_CODE, width, height, hints);
        return MatrixToImageWriter.toBufferedImage(bitMatrix);
    }

    public static void encodeToStream(String content, OutputStream outputStream,int width,int height) throws IOException, WriterException {
        String format = "png";
        Hashtable hints = new Hashtable();
        hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.M);
        hints.put(EncodeHintType.MARGIN, 2);
        BitMatrix bitMatrix = new MultiFormatWriter().encode(content, BarcodeFormat.QR_CODE, width, height, hints);
        MatrixToImageWriter.writeToStream(bitMatrix, format, outputStream);
    }

    public static String decode(String pathName) throws IOException, NotFoundException {
        MultiFormatReader formatReader = new MultiFormatReader();
        File file = new File(pathName);
        BufferedImage image = ImageIO.read(file);
        BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(new BufferedImageLuminanceSource(image)));
        Hashtable hints = new Hashtable();
        hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
        Result result = formatReader.decode(binaryBitmap, hints);
        return result.getText();
    }

    public static void main(String[] args) throws IOException, WriterException {
        FileOutputStream fos = new FileOutputStream(String.format("%s.jpg",  "/Users/mac/Desktop/erweima/", "aaaa"));
//        QRCodeUtils.encodeToStream("iVBORw0KGgoAAAANSUhEUgAAAMgAAADIAQAAAACFI5MzAAABuElEQVR42u2YQW6DMBBFJ2LB0kfwTcLFIoHExeAmHIElC8T0/7GTJlW77Keq4oUV87wYeeZ/j2P+07A3+etkMLO0ZyxGb+e1WbBupGR0P5J7O9nNtmvyJfZIyWLXNOTtas3is/uyWTqFuK9mW4flOQSpifwc1p9AmJ8hR3F8kzkBiRod+bFOr9UrIDEG8wMVwgC/KFhAGAxUsiEkq7ElLRmyXRjbjD2IrV98qhWiIjslauYTJ+jUseylBFOo88KwZt8N56QlLI4UEr1lOoVzj5TgODpE1HJPmDb2NFJSHcq61XhxhWf0UlLMaSlnMlEgR1WJiuwZZ2KUCqs1hWhGKSn5yWGZoVNsbMSEJUGX7nkwYdouJSOLAxL10CnKZHt0TyLCZgXacAoEnsH8rFpCq2ZY1S6enVxEIiGIqFtLftzuXqUj9OtSHChPRmliAn+gV2GETu2RHxUpvUuOpqF1fOQvKYn+zfmM4b1d20gpuacmPIM2pSflJcE7y6zk5/U1pSJoW3Bv+3rL9uRiUoLigEmwlT5eT+f3SX3TzuwhoBf/7K9FpL5l2okP2/Dr7tHZacj7X5x/Rj4AWbXvRmXQ11EAAAAASUVORK5CYII=",
//                fos,200,200);
//        fos.flush();
//        fos.close();

        BufferedImage image =encode("iVBORw0KGgoAAAANSUhEUgAAAMgAAADIAQAAAACFI5MzAAABuElEQVR42u2YQW6DMBBFJ2LB0kfwTcLFIoHExeAmHIElC8T0/7GTJlW77Keq4oUV87wYeeZ/j2P+07A3+etkMLO0ZyxGb+e1WbBupGR0P5J7O9nNtmvyJfZIyWLXNOTtas3is/uyWTqFuK9mW4flOQSpifwc1p9AmJ8hR3F8kzkBiRod+bFOr9UrIDEG8wMVwgC/KFhAGAxUsiEkq7ElLRmyXRjbjD2IrV98qhWiIjslauYTJ+jUseylBFOo88KwZt8N56QlLI4UEr1lOoVzj5TgODpE1HJPmDb2NFJSHcq61XhxhWf0UlLMaSlnMlEgR1WJiuwZZ2KUCqs1hWhGKSn5yWGZoVNsbMSEJUGX7nkwYdouJSOLAxL10CnKZHt0TyLCZgXacAoEnsH8rFpCq2ZY1S6enVxEIiGIqFtLftzuXqUj9OtSHChPRmliAn+gV2GETu2RHxUpvUuOpqF1fOQvKYn+zfmM4b1d20gpuacmPIM2pSflJcE7y6zk5/U1pSJoW3Bv+3rL9uRiUoLigEmwlT5eT+f3SX3TzuwhoBf/7K9FpL5l2okP2/Dr7tHZacj7X5x/Rj4AWbXvRmXQ11EAAAAASUVORK5CYII=",200,200);
        ImageIO.write(image, "png", fos);
        fos.flush();
        fos.close();
    }
}
