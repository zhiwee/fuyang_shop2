package com.zhiwee.gree.util;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.text.DecimalFormat;
import java.util.Date;

@Component
public class SequenceUtils {
    @Autowired
    private RedisTemplate redisTemplate;

    public String sequenceByDay(){
        String day = DateFormatUtils.format(new Date(),"yyyyMMdd");
        String key  = "OrderNum:SEQ" + day;
        Long num = redisTemplate.opsForValue().increment(key,1);
        if(num.toString().length()>5){
            return day + num.toString();
        }
        DecimalFormat decimalFormat = new DecimalFormat("000000");
        return day+decimalFormat.format(num);
    }

    //配送单号
    public String sendNumByDay(){
        String day = DateFormatUtils.format(new Date(),"yyyyMMdd");
        String key  = "sendNum:SEQ" + day;
        Long num = redisTemplate.opsForValue().increment(key,1);
        if(num.toString().length()>5){
            return day + num.toString();
        }
        DecimalFormat decimalFormat = new DecimalFormat("000000");
        return day+decimalFormat.format(num);
    }
    //退单的单号
    public String refundNumByDay(){
        String day = DateFormatUtils.format(new Date(),"yyyyMMdd");
        String key  = "refundNum:SEQ" + day;
        Long num = redisTemplate.opsForValue().increment(key,1);
        if(num.toString().length()>5){
            return day + num.toString();
        }
        DecimalFormat decimalFormat = new DecimalFormat("000000");
        return day+decimalFormat.format(num);
    }

    //认筹劵的单号
    public String ticketOrderByDay(){
        String day = DateFormatUtils.format(new Date(),"yyyyMMdd");
        String key  = "ticketOrder:SEQ" + day;
        Long num = redisTemplate.opsForValue().increment(key,1);
        if(num.toString().length()>5){
            return day + num.toString();
        }
        DecimalFormat decimalFormat = new DecimalFormat("000000");
        return day+decimalFormat.format(num);
    }



    public String addTicketByDay(){
        String day = DateFormatUtils.format(new Date(),"yyyyMMdd");
        String key  = "TICKET:SEQ" + day;
        Long num = redisTemplate.opsForValue().increment(key,1);
        if(num.toString().length()>5){
            return day + num.toString();
        }
        DecimalFormat decimalFormat = new DecimalFormat("000000");
        return "s"+day+decimalFormat.format(num);
    }



    public String orderChangeByDay(){
        String day = DateFormatUtils.format(new Date(),"yyyyMMdd");
        String key  = "orderChange:SEQ" + day;
        Long num = redisTemplate.opsForValue().increment(key,1);
        if(num.toString().length()>5){
            return day + num.toString();
        }
        DecimalFormat decimalFormat = new DecimalFormat("000000");
        return day+decimalFormat.format(num);
    }
}
