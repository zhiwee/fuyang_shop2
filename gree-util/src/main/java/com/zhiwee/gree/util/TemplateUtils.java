package com.zhiwee.gree.util;

import freemarker.template.Configuration;
import freemarker.template.Template;

import java.io.StringWriter;
import java.text.MessageFormat;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Knight
 * @since 2018/7/9 15:43
 */
public class TemplateUtils {

    public static Configuration cfg;

    static {
        cfg = new Configuration(Configuration.VERSION_2_3_0);
    }

    /**
     * String.format渲染模板
     * @param template 模版
     * @param params   参数
     * @return
     */
    public static String processFormat(String template, Object... params) {
        if (template == null || params == null)
            return null;
        return String.format(template, params);
    }

    /**
     * MessageFormat渲染模板
     * @param template 模版
     * @param params   参数
     * @return
     */
    public static String processMessage(String template, Object... params) {
        if (template == null || params == null)
            return null;
        return MessageFormat.format(template, params);
    }

    /**
     * 自定义渲染模板
     * @param template 模版
     * @param params   参数
     * @return
     */
    public static String processTemplate(String template, Map<String, Object> params) {
        if (template == null || params == null)
            return null;
        StringBuffer sb = new StringBuffer();
        Matcher m = Pattern.compile("\\$\\{\\w+\\}").matcher(template);
        while (m.find()) {
            String param = m.group();
            Object value = params.get(param.substring(2, param.length() - 1));
            m.appendReplacement(sb, value == null ? "" : value.toString());
        }
        m.appendTail(sb);
        return sb.toString();
    }

    /**
     * Freemarker渲染模板
     * @param template 模版
     * @param params   参数
     * @return
     */
    public static String processFreemarker(String template, Map<String, Object> params) {
        if (template == null || params == null) {
            return null;
        }
        try {
            StringWriter result = new StringWriter();
            Template tpl = new Template("strTpl", template, cfg);
            tpl.process(params, result);
            return result.toString();
        } catch (Exception e) {
            return null;
        }
    }

    public static void main(String[] args) {
        String p = "^(13[0-9]|14[579]|15[0-3,5-9]|16[6]|17[0135678]|18[0-9]|19[89])\\d{8}$";
        Pattern pattern = Pattern.compile(p);
        Matcher matcher = pattern.matcher("19812345678");
        System.out.println(matcher.matches());
    }

}
