package com.zhiwee.gree.util;

/**
 * created by
 */

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecutorUtils {
     //获取当前系统中可用cpu的数量
    private static final ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() * 2);

    public static void execute(Runnable runnable){
        executor.execute(runnable);
    }

}
