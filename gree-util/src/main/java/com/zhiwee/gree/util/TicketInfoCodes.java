package com.zhiwee.gree.util;


import org.apache.commons.lang3.math.NumberUtils;
/**
 * created by
 */
public class TicketInfoCodes {

    private static final long BASE_LINE = 1483200000000L; // 2017-01-01 00:00:00

    private static final String CODE_FORMAT = "%d%09d%02d"; //{seqlength}{seqno}{ts}

    public static final String toCode(int seqno){
        return toCode(seqno, BASE_LINE + seqno);
    }

    public static final String toCode(int seqno, long ts){
        return RadixUtils.decimalTo62(String.format(CODE_FORMAT, seqno, ts, String.valueOf(seqno).length()));
    }

    public static final int fromCode(String code){
        String encoded = RadixUtils.t62ToDecimal(code);
        int seqLength = NumberUtils.toInt(encoded.substring(encoded.length() - 2));
        return NumberUtils.toInt(encoded.substring(0, seqLength));
    }
}
