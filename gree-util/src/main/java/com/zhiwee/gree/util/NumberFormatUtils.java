package com.zhiwee.gree.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;

public class NumberFormatUtils {
    public static String format(double num) {
        BigDecimal bd = new BigDecimal(num);
        DecimalFormat format = new DecimalFormat("##########0.00");
        return format.format(bd.setScale(2, BigDecimal.ROUND_DOWN));
    }
}
