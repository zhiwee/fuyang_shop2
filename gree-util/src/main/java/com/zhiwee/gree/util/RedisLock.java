package  com.zhiwee.gree.util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.concurrent.TimeUnit;

public class RedisLock{

     private RedisTemplate redisTemplate;

private final String LOCKVALUE = "lockvalue";

private boolean locked = false;

public RedisLock (RedisTemplate redisTemplate){
    this.redisTemplate=redisTemplate;
}

    /**
     * 加锁
     * @param lockKey
     * @return
     */
    public  boolean lock(String lockKey){
        /**
        * 该方法会在没有key时，设置key;存在key时返回false；因此可以通过该方法及设置key的有效期，判断是否有其它线程持有锁
         * */
        Boolean success = redisTemplate.opsForValue().setIfAbsent(lockKey,LOCKVALUE);
        if(success != null && success){
        redisTemplate.expire(lockKey,60,TimeUnit.SECONDS);
        locked = true;
        }else{
        locked = false;
        }
        return locked;
        }

    /**
     * 释放锁
     * @param lockKey
     */
    public  void deleteKey(String lockKey){
          redisTemplate.delete(lockKey);
        }
}


