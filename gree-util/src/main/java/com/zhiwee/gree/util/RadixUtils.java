package com.zhiwee.gree.util;


import org.apache.commons.lang3.ArrayUtils;

import java.math.BigInteger;

public class RadixUtils {

    private static final String STRING = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    private static final char[] CHARS = STRING.toCharArray();

    private static final BigInteger SCALE_62 = BigInteger.valueOf(62L);
    private static final BigInteger KEEPER_62 = BigInteger.valueOf(61L);

    public static final String decimalTo62(String num){

        StringBuilder sb = new StringBuilder();
        BigInteger remainder = null;
        BigInteger i = new BigInteger(num);
        do {
            remainder = i.mod(SCALE_62);
            sb.append(CHARS[remainder.intValue()]);
            i = i.divide(SCALE_62);
        } while (i.compareTo(KEEPER_62) > 0);
        sb.append(CHARS[i.intValue()]);

        char[] chars = sb.toString().toCharArray();
        ArrayUtils.reverse(chars);
        return new String(chars);
    }

    public static final String t62ToDecimal(String num){
        // Trim the leading zeros first
        num = num.replace("^0*", "");

        BigInteger result = BigInteger.valueOf(0L);
        int index = 0;
        for(int i = 0; i < num.length(); i++) {
            index = STRING.indexOf(num.charAt(i));
            result = result.add(SCALE_62.pow(num.length() - i - 1).multiply(BigInteger.valueOf(index)));
        }

        return result.toString();
    }
}
